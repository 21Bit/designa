<?php
return [
    'google_place_api' => env('GOOGLE_PLACE_API'),

    'country' => 'country:au',

    'autocomplete_endpoint' => 'https://maps.googleapis.com/maps/api/place/autocomplete/json',

    'geocode_endpoint' => "https://maps.googleapis.com/maps/api/geocode/json",
    
    'details_endpoint' => "https://maps.googleapis.com/maps/api/place/details/json"
];