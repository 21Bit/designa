<?php

return [
    'url' => env('FIREBASE_URL'),   
    'key' => env('FIREBASE_KEY')
];