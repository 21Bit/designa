<?php
return [
    // product
    'product_limit' => 10,
    'product_limit_excluded_plan' => ['Pro'],

    // venue
    'venue_limit' => 2,
    'venue_limit_excluded_plan' => ['Pro'],
    'free_functionspaces_count' => 2,

    'september_promo' => 1,
    'september_promo_days_trial' => 180,

    'free_trial_access' => [
        'your-products-in-3d-10-is-pro',
        'create-your-own-event-3d-using-template',
        'transform-a-room-with-custom-3d-builds'
    ]

];
