<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Designa Studio</title>       
</head>

<body style="margin: 0; padding: 0; min-width: 100%!important; font-family: Verdana,Geneva,sans-serif; color: #3b3b3b; font-size: 14px;">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
  <td>        
    <table bgcolor="#ffffff" style="width: auto; max-width: 600px; border: 1px solid #f8f8f8;" align="center" cellpadding="0" cellspacing="0" border="0">      
      <tr>
        <td style="padding: 0 0 0 25px; ">
          <table width="auto" align="left" border="0" cellpadding="0" cellspacing="0">            
            <tr>
              <td style="background: url({{ url('site/images/invite-tag-inspiration/mail-new-logo-bg2.png')}}); height: 125px; width: 600px;">
                <a href="https://designa.studio" target="_blank"><img src="{{ url('site/images/invite-tag-inspiration/mail-logo.png') }}" width="auto" height="55" border="0" alt="" />
              </td>
            </tr> 
          </table>          
        </td>        
      </tr>      
      <tr>
        <td style="padding: 30px 60px 0px 60px; ">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td style="font-size: 16px; line-height: 28px; font-weight: normal;">
                Hi There,
              </td>
            </tr>            
            <tr>
              <td style="padding: 0; font-size: 25px; line-height: 28px; text-align: left;">
                 {{ $supplierInspiration->name }}
              </td>
            </tr>
            <tr>
              <td style="font-size: 14px; line-height: 22px; padding-top: 10px;">
                <p>Congratulations, <span style="color: #ef9a18">{{ $companyInviter->name }}</span> tagged your featured product or service as <span style="color: #ef9a18">{{ optional($supplierInspiration->role)->display_name }}</span> on the Designa Platform. Here is a preview;</p>                              
              </td>
            </tr>                       
          </table>
        </td>
      </tr>                         
      <tr>
        <td style="padding: 0px 60px 30px 60px;">        
          <!--[if (gte mso 9)|(IE)]>
            <table width="380" align="left" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td>
          <![endif]-->
          <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">  
            <tr>
              <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td>
                      <table border="0" cellspacing="0" cellpadding="0" style="background: #f6f6f6; display: block; width: 304px; margin: 30px auto 0 auto;">
                        <tr>
                          <td style="padding-top: 5px; font-size: 16px; line-height: 28px; font-weight: normal; padding-left: 10px;">                            
                            {{ $inspiration->name }}
                          </td>
                        </tr>
                        <tr>
                          <td style="padding-bottom: 10px; font-size: 12px; border-bottom: 5px solid #f7941e; width: 152px; padding-left: 10px;">
                            {{ $inspiration->company->name }}
                          </td>
                        </tr>                                               
                      </table>
                    </td>
                  </tr> 
                  <tr>
                    <td>
                      <table border="0" cellspacing="0" cellpadding="0" style="margin: 0 auto;">
                        <tr>
                          <td>                           
                            <a href="{{ route('site.inspiration.show', $inspiration->slug) }}" target="_blank"><img src="{{ url($inspiration->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png')) }}" width="100%" height="auto" /></a>
                          </td>
                        </tr>                        
                      </table>
                    </td>
                  </tr>
                  <tr align="center">
                    <td>
                      <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td style="font-size: 12px; line-height: 22px; font-style: italic;" align="center">
                            <p><a href="{{ route('site.inspiration.show', $inspiration->slug) }}" style="color: #3b3b3b; text-decoration: none;">{{ route('site.inspiration.show', $inspiration->slug) }}</a></p>                            
                          </td>
                        </tr>
                        <tr>
                          <td style="font-size: 14px; line-height: 22px; padding-top: 10px;">
                            <p>To link your product or service to this inspiration you will first have to create an account. You can do this by visiting <a href="https://designa.studio" target="_blank" style="color: #ef9a18; text-decoration: none;">https://designa.studio</a> on a desktop.</p>
                            {{-- <p>
                              We can also help set up you with a profile - Just reply to this email or follow us on instagram.
                            </p> --}}
                            {{-- <p>To get started click the button below or email  and we'll create a profile for you!</p> --}}
                            <p>We can also help get you set up with a profile - Just click the button below or email <a href="mailto:hello@designa.studio" style="color: #ef9a18; text-decoration: none;">hello@designa.studio</a> and we will create a profile for you!</p>
                          </td>
                        </tr>                        
                        <tr>
                          <td>
                            <p style="text-align: center; padding-top: 10px;"><a href="mailto:hello@designa.studio" target="_blank" style="text-align: center; font-family: 'Open Sans', sans-serif; color: #f7941e; text-decoration: none; border: 2px solid #f7941e; border-radius: 30px; padding: 8px 19px; font-weight: normal; font-size: 14px;">Get Started</a></p>
                          </td>
                        </tr>                                                     
                      </table>
                    </td>
                  </tr>                                    
                </table>
              </td>
            </tr>
          </table>
          <!--[if (gte mso 9)|(IE)]>
                </td>
              </tr>
          </table>
          <![endif]-->
        </td>
      </tr> 
      <tr>
        <td style="padding: 0 60px 30px 60px;" bgcolor="#f7f7f7">        
          <!--[if (gte mso 9)|(IE)]>
            <table width="380" align="left" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td>
          <![endif]-->
          <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">  
            <tr>
              <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr align="left">
                    <td>
                      <table border="0" cellspacing="0" cellpadding="0" >                        
                        <tr>
                          <td style="font-size: 14px; line-height: 22px; padding-top: 20px; ">
                            Kind Regards,
                          </td>
                        </tr>                        
                        <tr>
                          <td style="padding: 0 0 15px 0; font-size: 30px; line-height: 28px; ">                           
                            Designa Team
                          </td>        
                        </tr>                                                
                      </table>
                    </td>
                  </tr>                                    
                </table>
              </td>
            </tr>
          </table>
          <!--[if (gte mso 9)|(IE)]>
                </td>
              </tr>
          </table>
          <![endif]-->
        </td>
      </tr>                                                
      <tr>
        <td align="center" style="font-size: 14px; line-height: 22px; padding: 30px 40px 30px 40px;">
          Contact
          <p style="padding: 0; line-height: 0; font-size: 30px;">Follow Us</p>
          <a href="https://www.instagram.com/designastudio.au/" style="text-decoration: none;" target="_blank">
              <img src="https://designa.studio/images/instagram-icon.jpg" width="auto" height="auto" alt="Instagram" border="0" style="text-decoration: none;"  />
            </a>
            <a href="https://www.facebook.com/Designa-Studio-260657538157535/" style="text-decoration: none;" target="_blank">
              <img src="https://designa.studio/images/fb-icon.jpg"width="auto" height="auto" alt="Facebook" border="0" style="text-decoration: none;" />
            </a>
            <a href="https://www.youtube.com/channel/UC2nO0C_he52F2cqLbxGyoBA" style="text-decoration: none;" target="_blank">
              <img src="https://designa.studio/images/youtube-icon.jpg" width="auto" height="auto" alt="Twitter" border="0" style="text-decoration: none;"  />
            </a>
        </td>
      </tr>      
      <tr>
        <td bgcolor="#f7941e" style="    padding: 20px 30px 15px 30px;">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 14px;
    color: #ffffff;">               
                <a href="https://designa.studio/" target="_blank" style="text-decoration: none;"><font color="#ffffff">www.designa.studio</font></a>                 
              </td>
            </tr>            
          </table>
        </td>
      </tr>
    </table>    
    </td>
  </tr>
</table>
</body>
</html>