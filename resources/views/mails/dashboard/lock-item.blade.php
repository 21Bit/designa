<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Designa Studio</title>   
</head>

<body style="margin: 0; padding: 0; min-width: 100%!important; font-family: Verdana,Geneva,sans-serif; color: #3b3b3b; font-size: 14px;">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
  <td>        
    <table bgcolor="#ffffff" style="width: auto; max-width: 600px; border: 1px solid #f8f8f8;" align="center" cellpadding="0" cellspacing="0" border="0">      
      <tr>
        <td style="padding: 0 0 0 25px; ">
          <table width="auto" align="left" border="0" cellpadding="0" cellspacing="0">            
            <tr>
              <td style="background: url({{ url('site/images/invite-tag-inspiration/mail-new-logo-bg2.png')}}); height: 125px; width: 600px;">
                <a href="https://designa.studio" target="_blank"><img src="{{ url('site/images/invite-tag-inspiration/mail-logo.png') }}" width="auto" height="55" border="0" alt="" />
              </td>
            </tr> 
          </table>          
        </td>        
      </tr>     
      <tr>
        <td style="padding: 30px 60px 30px 60px; ">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr>
              <td style="font-size: 16px; line-height: 28px; font-weight: normal;">
                Hi There,
              </td>
            </tr>            
            <tr>
              <td style="padding: 0; font-size: 25px; line-height: 28px; text-align: left;">
                {{ $name }} <!-- you can change the name here -->
              </td>
            </tr>
            <tr>
              <td style="font-size: 14px; line-height: 22px; padding-top: 10px;">
                <p>A quick notification to let you know we have unpublished your post <span style="color: #ef9a18;">{{ $item }}</span> as it does not meet our image or post guidelines. The <span style="color: #ef9a18;">{{ $item }}</span> is <span style="color: #ef9a18;">{{ $reason }}</span>. It is now in draft form for you to edit in your profile.</p>
                <p>If you'd like to learn more about our posting guidelines, please check <a href="#" style="color: #ef9a18; text-decoration: none;">&lt;here&gt;</a>.</p>   
                <p>You can also reach out to us anytime for help and support with loading information by replying to this email.</p>                           
              </td>
            </tr>                       
          </table>
        </td>
      </tr>                         
      <tr>
        <td style="padding: 0 60px 30px 60px;" bgcolor="#f7f7f7">        
          <!--[if (gte mso 9)|(IE)]>
            <table width="380" align="left" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td>
          <![endif]-->
          <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">  
            <tr>
              <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr align="center">
                    <td>
                      <table border="0" cellspacing="0" cellpadding="0" >                        
                        <tr>
                          <td style="font-size: 14px; line-height: 22px; padding-top: 20px;" align="center">
                            Kind Regards,
                          </td>
                        </tr>                        
                        <tr>
                          <td style="padding: 0 0 15px 0; font-size: 30px; line-height: 28px;" align="center">                           
                            Team at Designa
                          </td>        
                        </tr>                                                 
                      </table>
                    </td>
                  </tr>                                    
                </table>
              </td>
            </tr>
          </table>
          <!--[if (gte mso 9)|(IE)]>
                </td>
              </tr>
          </table>
          <![endif]-->
        </td>
      </tr>                                          
      <tr>
        <td align="center" style="font-size: 14px; line-height: 22px; padding: 30px 40px 30px 40px;">
          Contact
          <p style="padding: 0; line-height: 0; font-size: 30px;">Follow Us</p>
          <a href="https://www.instagram.com/designastudio.au/" style="text-decoration: none;" target="_blank">
              <img src="https://designa.studio/images/instagram-icon.jpg" width="auto" height="auto" alt="Instagram" border="0" style="text-decoration: none;"  />
            </a>
            <a href="https://www.facebook.com/Designa-Studio-260657538157535/" style="text-decoration: none;" target="_blank">
              <img src="https://designa.studio/images/fb-icon.jpg"width="auto" height="auto" alt="Facebook" border="0" style="text-decoration: none;" />
            </a>
            <a href="https://www.youtube.com/channel/UC2nO0C_he52F2cqLbxGyoBA" style="text-decoration: none;" target="_blank">
              <img src="https://designa.studio/images/youtube-icon.jpg" width="auto" height="auto" alt="Twitter" border="0" style="text-decoration: none;"  />
            </a>
        </td>
      </tr>      
      <tr>
        <td bgcolor="#f7941e" style="    padding: 20px 30px 15px 30px;">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 14px;
    color: #ffffff;">               
                <a href="https://designa.studio/" target="_blank" style="text-decoration: none;"><font color="#ffffff">www.designa.studio</font></a>                 
              </td>
            </tr>            
          </table>
        </td>
      </tr>
    </table>    
    </td>
  </tr>
</table>
</body>
</html>