<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Designa Studio</title>       
</head>

<body style="margin: 0; padding: 0; min-width: 100%!important; font-family: Verdana,Geneva,sans-serif; color: #3b3b3b; font-size: 14px;">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
  <td>        
    <table bgcolor="#ffffff" style="width: auto; max-width: 600px; border: 1px solid #f8f8f8;" align="center" cellpadding="0" cellspacing="0" border="0">      
      <tr>
        <td style="padding: 0 0 0 25px; ">
          <table width="auto" align="left" border="0" cellpadding="0" cellspacing="0">            
            <tr>
              <td style="background: url({{ url('site/images/invite-tag-inspiration/mail-new-logo-bg2.png')}}); height: 125px; width: 600px;">
                <a href="https://designa.studio" target="_blank"><img src="{{ url('site/images/invite-tag-inspiration/mail-logo.png') }}" width="auto" height="55" border="0" alt="" />
              </td>
            </tr> 
          </table>          
        </td>        
      </tr>      
      <tr>
        <td style="padding: 30px 60px 30px 60px; ">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr>
              <td style="font-size: 16px; line-height: 28px; font-weight: normal;">
                Hi From
              </td>
            </tr>            
            <tr>
              <td style="padding: 0; font-size: 25px; line-height: 28px; text-align: left;">
                Designa <!-- you can change the name here -->
              </td>
            </tr>
            <tr>
              <td style="font-size: 14px; line-height: 22px; padding-top: 10px;">
                <p>We hope you are now enjoying the experience of using the Designa platform.</p>
                <p>It looks like your profile is still in draft and not published or available for others to view. If you would like us to publish it please click here;</p>
              </td>
            </tr>
            <tr>
              <td>
                <p style="text-align: center; padding: 18px 0;"><a href="{{ $link }}" target="_blank" style="text-align: center; font-family: 'Open Sans', sans-serif; color: #fff; text-decoration: none; border: 2px solid #f7941e; border-radius: 30px; padding: 8px 19px; font-weight: normal; font-size: 14px; background: #f7941e">Make Me Live</a></p>
              </td>
            </tr>
            <tr>
              <td style="font-size: 14px; line-height: 22px; padding-top: 10px;">
                <p>If you're a bit too busy at the moment, we will publish your profile in 5 days.</p>
                <p>If you haven't had a chance to load products, there is some help available here;</p>
              </td>
            </tr>
            <tr>
              <td>
                <p style="text-align: center; padding-top: 10px;"><a href="#" target="_blank" style="text-align: center; font-family: 'Open Sans', sans-serif; color: #f7941e; text-decoration: none; border: 2px solid #f7941e; border-radius: 30px; padding: 8px 19px; font-weight: normal; font-size: 14px;">Get Help</a></p>
              </td>
            </tr>
            <tr>
              <td style="font-size: 14px; line-height: 22px; padding-top: 10px;">
                <p>For those of you who are new we have resources available and are more than happy to facilitate group webinars - a great opportunity to meet potential collaborators and see what's possible in Designa.</p>
              </td>
            </tr>                       
          </table>
        </td>
      </tr>                         
      <tr>
        <td style="padding: 0 60px 30px 60px;" bgcolor="#f7f7f7">        
          <!--[if (gte mso 9)|(IE)]>
            <table width="380" align="left" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td>
          <![endif]-->
          <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">  
            <tr>
              <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr align="center">
                    <td>
                      <table border="0" cellspacing="0" cellpadding="0" >                        
                        <tr>
                          <td style="font-size: 14px; line-height: 22px; padding-top: 20px;" align="center">
                            Kind Regards,
                          </td>
                        </tr>                        
                        <tr>
                          <td style="padding: 0 0 15px 0; font-size: 30px; line-height: 28px;" align="center">                           
                            Team at Designa
                          </td>        
                        </tr>
                                                
                      </table>
                    </td>
                  </tr>                                    
                </table>
              </td>
            </tr>
          </table>
          <!--[if (gte mso 9)|(IE)]>
                </td>
              </tr>
          </table>
          <![endif]-->
        </td>
      </tr>                                          
      <tr>
        <td align="center" style="font-size: 14px; line-height: 22px; padding: 30px 40px 30px 40px;">
          Contact
          <p style="padding: 0; line-height: 0; font-size: 30px;">Follow Us</p>
          <a href="https://www.instagram.com/designastudio.au/" style="text-decoration: none;" target="_blank">
              <img src="https://designa.studio/images/instagram-icon.jpg" width="auto" height="auto" alt="Instagram" border="0" style="text-decoration: none;"  />
            </a>
            <a href="https://www.facebook.com/Designa-Studio-260657538157535/" style="text-decoration: none;" target="_blank">
              <img src="https://designa.studio/images/fb-icon.jpg"width="auto" height="auto" alt="Facebook" border="0" style="text-decoration: none;" />
            </a>
            <a href="https://www.youtube.com/channel/UC2nO0C_he52F2cqLbxGyoBA" style="text-decoration: none;" target="_blank">
              <img src="https://designa.studio/images/youtube-icon.jpg" width="auto" height="auto" alt="Twitter" border="0" style="text-decoration: none;"  />
            </a>
        </td>
      </tr>      
      <tr>
        <td bgcolor="#f7941e" style="    padding: 20px 30px 15px 30px;">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 14px;
    color: #ffffff;">               
                <a href="https://designa.studio/" target="_blank" style="text-decoration: none;"><font color="#ffffff">www.designa.studio</font></a>                 
              </td>
            </tr>            
          </table>
        </td>
      </tr>
    </table>    
    </td>
  </tr>
</table>
</body>
</html>