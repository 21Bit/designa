<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Inquiry</title>
</head>
<body>
    <h1> New Designa Inquiry</h1>
    <table>
        <p>
            Sender: <strong>{{ $data->name }}</strong>
        </p>
        <p>
            Email: <strong>{{ $data->email }}</strong>
        </p>
        <p>
            Contact Number: <strong>{{ $data->contact_number }}</strong>
        </p>
        <br>
        <div>
            {!! nl2br($data->message) !!}
        </div>
    </table>
</body>
</html>