<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Designa Studio</title>   
</head>

<body style="margin: 0; padding: 0; min-width: 100%!important; font-family: Verdana,Geneva,sans-serif; color: #3b3b3b; font-size: 14px;">
<table bgcolor="#fff" style="width: 100%; max-width: 600px; border: 1px solid #ccc;" align="center" cellpadding="0" cellspacing="0" border="0">
  <tr>
    <td style="padding: 40px 0 0 25px; background: url('https://designa.studio/site/images/event-registration/email-ticket-header.png'); background-size: cover; background-position: top center;">
      <table width="auto" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td height="55" style="padding-top: 50px;">
            <a href="https://designa.studio" target="_blank"><img src="{{ asset("site/images/designa-logo.png") }}" width="auto" height="55" border="0" alt="" style="padding-right: 20px;" /></a> 
          </td>
          <td style="padding-top: 50px; color: #f7941e; ">
           <span style="font-size: 46px; border-left: 1px solid #f7941e;"></span> <span style="font-size: 24px; padding-left: 20px;"> E-Ticket </span>
          </td>
        </tr>
      </table> 
    </td>
  </tr>
  @for($i = 0; count($data['name']) > $i; $i++)
    <tr>
      <td align="right" style="padding-top: 60px; padding-bottom: 30px;">
        <div style="border-top: 1px dotted #58584a; padding-top: 10px; margin-bottom: -18px;"></div>
        <span style="background: #bbbbbb; border-radius: 41px; padding: 5px 15px; color: #fff; margin-top:-20px; margin-right:20px">Ticket {{ $i + 1 }} of {{ count($data['name']) }} </span>
      </td>
    </tr>
    <tr>
      <td style="padding: 0 30px;">
        <h1 style="color: #f7941e; font-size: 26px; padding-top: 0; padding-bottom: 5px; margin-bottom: 0; margin-top: 0;">The Designa Exclusive Preview</h1>
        <h3 style="padding-top: 0; padding-bottom: 0; margin-bottom: 0; margin-top: 0; color: #58585a">November 04, 2020 @ 6:00PM</h3>
      </td>
    </tr>
    <tr>
      <td style="padding: 0 30px;">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="50%" valign="top">
              <p style="color: #9e9e9e; font-size: 13px; font-weight: bold; padding-top: 25px; padding-bottom: 0; margin-bottom: 0;">Guest Name</p>
              <h5 style="margin-bottom: 0; margin-top: 6px; font-size: 18px; color: #58585a; font-weight: normal;">{{ $data['name'][$i] }}</h5>
            </td>
            <td width="50%" valign="top">
              <p style="color: #9e9e9e; font-size: 13px; font-weight: bold; padding-top: 25px; padding-bottom: 0; margin-bottom: 0;">Email Address</p>
              <h5 style="margin-bottom: 0; margin-top: 6px; font-size: 18px; color: #58585a; font-weight: normal;">{{ $data['email'][$i] }}</h5>
            </td>
          </tr>
        </table>  
      </td>
    </tr>

    <tr>
      <td style="padding: 0 30px;">
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td valign="top">
              <p style="color: #9e9e9e; font-size: 13px; font-weight: bold; padding-top: 25px; padding-bottom: 0; margin-bottom: 0;">Registration Date</p>
            <h5 style="margin-bottom: 0; margin-top: 6px; font-size: 18px; color: #58585a; font-weight: normal;">{{ now()->format('F d, Y | h:i A')  }}</h5>
            </td>          
          </tr>
        </table>  
      </td>
    </tr>
    <tr>
      <td style="padding: 30px 30px 10px 30px;">
        <hr style="border-top: 1px solid  #fcd5a8;">
      </td>
    </tr>
    <tr>
      <td style="background: url('{{ asset("site/images/event-registration/email-ticket-footer.png") }}'); background-size: cover; height: 100px; background-position: top center; padding: 0 30px 30px;">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="50%" valign="top">
              <p style="color: #9e9e9e; font-size: 13px; font-weight: bold; padding-bottom: 0; margin-bottom: 0;">Venue</p>
              <h5 style="margin-bottom: 0; margin-top: 6px; font-size: 18px; color: #58585a; font-weight: normal; ">Hyatt Regency, 161 Sussex St, Sydney NSW 200</h5>
            </td>
            <td width="50%" valign="top">
              <p style="color: #9e9e9e; font-size: 13px; font-weight: bold; padding-bottom: 0; margin-bottom: 0;">Dress Code</p>
              <h5 style="margin-bottom: 0; margin-top: 6px; font-size: 18px; color: #58585a; font-weight: normal;">Dress to impress</h5>
            </td>
          </tr>
        </table> 
      </td>
    </tr>
  @endfor


</table>


</body>
</html>