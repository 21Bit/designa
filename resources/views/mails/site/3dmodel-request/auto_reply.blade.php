<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Designa Studio</title>   
</head>

<body style="margin: 0; padding: 0; min-width: 100%!important; font-family: Verdana,Geneva,sans-serif; color: #3b3b3b; font-size: 14px;">

<div style="width: 640px; padding: 0; margin: 0 auto;">  
  <table bgcolor="#fff" style="width: 550px; margin-bottom: -35px; z-index: 100; position: relative; " align="center" cellpadding="0" cellspacing="0" border="0">
  <tr>
    <td style="padding: 30px 0 0 0;">
      <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 30px;">
        <tr>
          <td height="55" style="padding-top: 0px;">
            <a href="https://designa.studio" target="_blank"><img src="https://designa.studio/site/images/logo.svg" width="auto" height="55" border="0" alt="" /></a> 
          </td>
          <td height="55" align="right" style="padding-top: 0px;">
            <a href="https://www.facebook.com/Designa-Studio-260657538157535/" target="_blank"><img src="{{ url('/site/images/fb-orange.png')}}" width="auto" height="40" border="0" alt="" style="padding-right: 20px;" /></a> 
            <a href="https://www.instagram.com/designastudio.au/" target="_blank"><img src="{{ url("/site/images/ig-orange.png")}}" width="auto" height="40" border="0" alt="" style="padding-right: 20px;" /></a> 
            <a href="https://www.youtube.com/channel/UC2nO0C_he52F2cqLbxGyoBA" target="_blank"><img src="{{ url("/site/images/youtube-orange.png")}}" width="auto" height="40" border="0" alt="" style="padding-right: 0px; " /></a> 
          </td>          
        </tr>
      </table> 
    </td>
  </tr>  
  <tr>
    <td style="padding: 30px; border: 1px dotted #58584a;">
      <h1 style="color: #58585a; font-size: 26px; padding-top: 0; padding-bottom: 5px; margin-bottom: 0; margin-top: 0; font-weight: bold;">Yes!</h1>
      <h3 style="padding-top: 0; padding-bottom: 0; margin-bottom: 0; margin-top: 0; color: #58585a; font-weight: normal;">This is an auto-generated reply, however, rest assured we'll be in touch shortly.</h3>
      <h3 style="padding-top: 50px; padding-bottom: 0; margin-bottom: 0; margin-top: 0; color: #58585a; font-weight: normal;">Regards,</h3>
      <h2 style="color: #58585a; font-size: 20px; padding-top: 0; padding-bottom: 5px; margin-bottom: 0; margin-top: 0; font-weight: bold;">Designa Team</h2>      
    </td>
  </tr>    
</table>
<div style="background-color: #ff9a1e; width: 100%; height: 100px;">
  <a href="https://designa.studio/" target="_blank" style="text-decoration: none; color: #fff; text-align: center; margin: 0 auto; display: block; padding-top: 63px;">https://designa.studio/</a>
</div>
</div>
</body>
</html>