<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Designa_First</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="assets/css/reset.css">
    <link rel="stylesheet" href="assets/css/style.css">

    <script src="assets/js/jquery-3.4.1.min.js"></script>

    <!-- unity resources -->
    <script src="%UNITY_WEBGL_LOADER_URL%"></script>

    <script>
      // instantiate webgl only if we're not on a touch device

      var isTouch = ('ontouchstart' in document.documentElement);

      if ( !isTouch ) {
        var unityInstance = UnityLoader.instantiate("unityContainer", "%UNITY_WEBGL_BUILD_URL%");
      }
    </script>


    <script src="/Build/UnityLoader.js"></script>
    <script>
      UnityLoader.instantiate("unityContainer", "Build/base.json");
    </script>
  </head>
  <body>
  
  <div class="webgl-wrapper">
        <div class="webgl-content">
          <div id="unityContainer" style="width:%UNITY_WIDTH%px; height: %UNITY_HEIGHT%px;"></div>
        </div>
    </div>


    <!--<div id="unityContainer" style="width: 2560px; height: 1440px; margin: auto"></div>
  -->


     <script src="assets/js/app.js"></script>
  


  </body>
</html>
