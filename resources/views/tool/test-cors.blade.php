@extends('site.includes.layouts.no-menu')

@section('page-title', 'Designa Request 3D')

@section('page-meta')
    <meta name="description" content="Some suppliers that have already joined the Designa Studio Community">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Designa Request 3D">
    <meta property="og:description" content="Start your 3D model for your venue and products">
    {{-- <meta property="og:image" content="{{ url('/site/images/support.png') }}"> --}}
    <meta property="og:url" content="{{ url('//request-3d') }}">
@endsection

@section('content')
    <div class="m-2 mt-5" id="app">
        <h3 class="text-center pb-2">3D Request Form</h3>
        <div class="container bg-white ">
            <div class="p-sm-5 pt-2 pb-2">
                @include('site.includes.alerts.message')
                <test-cors></test-cors>
            </div>
        </div>
    </div>
@endsection