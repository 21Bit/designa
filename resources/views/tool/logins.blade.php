<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Logins</title>
  </head>
  <body>
    <div class="container">
        <form class="mt-3">
                <div class="row mb-3">
                        <div class="col-sm-3">
                                Type
                                <select name="type" id="log-type" class="form-control">
                                        <option value="login">Logins</option>
                                        <option value="registrations">Registrations</option>
                                </select>
                        </div>
                        <div class="col-sm-3">
                                Supplier
                                <select name="supplier" id="" class="form-control">
                                        <option value="">- All Suppliers -</option>
                                        @foreach($companies as $company)
                                                <option value="{{ $company->id }}" @if(Request::get('supplier') == $company->id) selected @endif>{{ $company->name }}</option>
                                        @endforeach
                                </select>
                        </div>
                        <div class="col-sm-3">
                                Date
                                <input type="date" name="date" value="{{ Request::get('date') }}" class="form-control">
                        </div>
                        <div class="col-sm-3">
                                <button class="btn btn-primary mt-4">Filter</button>
                                <button id="export" class="btn btn-primary mt-4">Export CSV</button>
                        </div>
                </div>
        </form>
        <table class="table">
                <thead>
                        <tr>
                                <th>Client Number</th>
                                <th>Client Name</th>
                                <th>Date Registered</th>
                                <th>Last Loging Date</th>
                                <th>Platform Last Used</th>
                                <th>No. of Logins</th>
                        </tr>
                </thead>
                <tbody>
                        @foreach($suppliers as $supplier)
                        <tr>
                                <td>{{ $supplier->id }}</td>
                                <td>{{ optional($supplier->company)->name  }}</td>
                                <td>{{ $supplier->created_at->format('d/m/Y')  }}</td>
                                <td>{{ optional($supplier->activities()->orderBy('created_at', 'DESC')->first())->created_at  }}</td>
                         
                                @if(Request::get('date'))
                                        <td>{{ ucfirst(optional($supplier->activities()->whereDate('created_at', Request::get('date'))->whereType('LOGIN')->first())->origin)  }}</td>
                                @else
                                        <td>{{ ucfirst(optional($supplier->activities()->whereType('LOGIN')->first())->origin)  }}</td>
                                @endif
                                @if(Request::get('date'))
                                        <td>{{ $supplier->activities()->whereDate('created_at', Request::get('date'))->whereType('LOGIN')->count()  }}</td>
                                @else
                                        <td>{{ $supplier->activities()->whereType('LOGIN')->count()  }}</td>
                                @endif
                        </tr>
                        @endforeach
                </tbody>
        </table>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script>
                function download_csv(csv, filename) {
                        var csvFile;
                        var downloadLink;

                        // CSV FILE
                        csvFile = new Blob([csv], {type: "text/csv"});

                        // Download link
                        downloadLink = document.createElement("a");

                        // File name
                        downloadLink.download = filename;

                        // We have to create a link to the file
                        downloadLink.href = window.URL.createObjectURL(csvFile);

                        // Make sure that the link is not displayed
                        downloadLink.style.display = "none";

                        // Add the link to your DOM
                        document.body.appendChild(downloadLink);

                        // Lanzamos
                        downloadLink.click();
                }

                function export_table_to_csv(html, filename) {
                        var csv = [];
                        var rows = document.querySelectorAll("table tr");
                        
                        for (var i = 0; i < rows.length; i++) {
                                        var row = [], cols = rows[i].querySelectorAll("td, th");
                                        
                                for (var j = 0; j < cols.length; j++) 
                                row.push(cols[j].innerText);
                                
                                        csv.push(row.join(","));		
                                }

                        // Download CSV
                        download_csv(csv.join("\n"), filename);
                }

                document.getElementById("export").addEventListener("click", function () {
                var html = document.querySelector("table").outerHTML;
                        export_table_to_csv(html, "table.csv");
                });

                $('#log-type').change(function(){
                        $('form').submit();
                })

        </script>
  </body>
</html>