@extends('site.includes.layouts.full')

@section('page-meta')
    <meta name="description" content="{{ Str::limit($inspiration->description, 150) }}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="{{ $inspiration->name }}">
    <meta property="og:description" content="{{ Str::limit($inspiration->description, 50) }}">
    <meta property="og:image" content="{{ url($inspiration->image('cover')->path('covers', '/images/placeholders/placeholder.png')) }}">
    <meta property="og:url" content="{{ route('site.inspiration.show', $inspiration->slug) }}">
@endsection

@section('content')
    
    <div class="inspiration-header" style="background-image:url('{{ $inspiration->image('cover')->path('covers', 'https://via.placeholder.com/1920x450') }}')">
        <div class="header-box-inspiration">
            <h1>{{ $inspiration->name }}</h1>
            <b for="">Setting</b>
                : {{ optional($inspiration->categories()->whereType('setting')->first())->name }}
            <br>
            <b for="">Color</b> :
                @foreach($colors as $color)
                    <div class="color-div">
                        <span style="background-color:{{ $color->description }}">&nbsp;</span>
                    </div>
                @endforeach
            <br>
            <b for=""><i class="fa fa-map-marker"></i></b> 
                {{ $inspiration->location }}
            <br>
            
            <b for=""><i class="fa fa-camera"></i></b>
                {{ count($gallery) + 1 }} photos
        
        </div>
        <img class="bottom-pattern" src="/site/images/bottom-pattern.png" alt="">
    </div>
    <div class="container-fluid inspiration-page  mt-5">
            {{-- <div class="pull-right">
                    @auth
                      <heart-component :default="{{ $inspiration->hearts()->whereUserId(Auth::user()->id)->first() ? 1 : 0  }}" :id="{{ $inspiration->id }}" type="/inspiration" styleclass="btn btn-sm btn-outline-secondary btn-lg mt-4"> Save Inspiration</heart-component>
                      @endif

              </div> --}}
            <div class="row mt-2" style="position: relative">
                <div class="col-sm-2 pl-4 pr-3 ">
                    <div class="box bg-white p-2 mb-1">
                        <small class="d-block text-center text-muted"><i>Posted by</i></small>
                        <div class="text-center">
                            <div class="p-4">
                                <img src="{{ $inspiration->company->image('logo')->path('logos', '/images/placeholders/placeholder.png') }}" class="w-100" alt="">
                            </div>
                            <h5>
                                <a href="{{ route('site.supplier.show', optional($inspiration->company)->slug) }}" class="text-orange">
                                    {{ optional($inspiration->company)->name }}
                                </a>
                            </h5>
                            <small class="line-height-1">{{ $inspiration->company->roleList() }}</small>
                            <div class="d-flex">
                                <div class="w-50 p-1">
                                    <button class="btn btn-default btn-sm btn-block">Message</button>
                                </div>
                                <div class="w-50 p-1">
                                    <button class="btn btn-default btn-sm btn-block"><i class="fa fa-heart"></i></button>
                                </div>
                            </div>
                        </div>
                    
                    </div>
                    <div class="box bg-white  p-2 ">
                            <div class="text-muted d-block mb-3 text-center supplier-divider mb-2">Suppliers</div>
                            @foreach($inspiration->supplierRoles()->where('status', 'confirmed')->has('role')->get() as $supplier)
                                <div class="inspiration-suppliers">
                                    @if($supplier->company)
                                    <div class="buttons">
                                        <div class="d-flex">
                                            <div class="w-50 p-1">
                                                <a class="btn btn-default btn-sm"><i class="fa fa-envelope"></i></a>
                                            </div>
                                            <div class="w-50 p-1">
                                                <a class="btn btn-default btn-sm"><i class="fa fa-heart"></i></a>
                                            </div>
                                        </div>     
                                    </div>
                                        @if($supplier->role->name == "decor_hire")
                                            <a href="{{ route('site.supplier.show', $supplier->company->slug) }}"  data-target="">
                                        @else
                                            <a href="{{ route('site.supplier.show', $supplier->company->slug) }}" target="_blank">
                                        @endif
                                            <h4>
                                                {{ $supplier->company->name }}
                                            </h4>
                                            <small>{{ $supplier->role->display_name }}</small>
                                        @if($supplier->company)
                                            </a>
                                            
                                        @endif
                                    @else
                                        <a href="#" onclick="return false">
                                         <h4>
                                            {{ $supplier->name }}
                                        </h4>
                                        <small>{{ $supplier->role->display_name }}</small>
                                        </a>
                                    @endif
                                </div>
                            @endforeach
                        </div>
                </div>
                <div class="col-sm-8 pb-3">

                    {{-- <div class="pull-right">
                        <a href="" class="mr-2"><i class="fa fa-image"></i></a>
                        <a href=""><i class="fa fa-play-circle"></i></a>
                    </div> --}}
                    <div class="inspiration-grid">
                        <div class="inspiration-item">
                            <div class="image-gallery">
                                <a href="{{ $inspiration->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png') }}" data-lightbox="PORTFOLIO">
                                    <img src="{{ $inspiration->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png') }}" alt="gallery-image" class="margin">
                                </a>
                            </div>
                        </div>
                        @foreach($videos as $video)
                               <div class="inspiration-item">
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <video controls title="{{ $video->title }}">
                                            <source src="/videos/{{ $video->path }}" type="video/mp4">
                                            Your browser does not support HTML5 video.
                                        </video>
                                    </div>
                                </div>
                        @endforeach
                        @foreach($gallery as $image)
                            <div class="inspiration-item">
                                <div class="image-gallery">
                                    <a href="/images/gallery/{{ $image->path }}" data-lightbox="PORTFOLIO">
                                        <img src="/images/gallery/{{ $image->path }}"  alt="gallery-image" class="margin">
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-sm-2">
                    <h5 class="text-center text-muted decor-supplier-title mb-3">Decor Supplied by</h5>
                    @foreach($inspiration->supplierRoles()->where('status', 'confirmed')->whereHas('role', function($q){ $q->where('name', 'decor_hire'); })->get() as $supplier)
                        <div class="bg-white p-3 m-2">

                            <div class="company-box">
                                @if($supplier->company)
                                    <div class="mb-3">
                                        <div>
                                            <b><small class="text-muted">{{ $supplier->role->display_name }}</small> </b><br>
                                            <a href="{{ route('site.supplier.show', $supplier->company->slug) }}" class="name text-orange">{{ optional($supplier->company)->name }}</a> <br>
                                        </div>
                                    </div>
                                @else
                                    <div class="mb-3">
                                        <b><small class="text-muted">{{ $supplier->role->display_name }}</small></b><br>
                                        <strong for="">{{ $supplier->name }}</strong> <br>
                                    </div>
                                @endif
                            </div>
                            
                            @forelse($inspiration->products()->whereCompanyId($supplier->company_id)->get() as $product)
                                <div class="tagged-decors mb-3">
                                    <div class="d-flex">
                                        <div class="w-50  align-self-center">
                                            <div class="body">
                                                <!-- <small>{{ optional($product->categories()->whereType('product_type')->first())->name }}</small> -->
                                                <div>
                                                    {{ $product->name }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="w-50 p-2">
                                            <img src="{{ $product->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png') }}" class="mw-100" style="width:100%" alt="">
                                        </div>
                                    </div>
                                    <hr>
                                  
                                </div>
                            @empty
                                <h6 class="text-center text-muted mt-5"> No Decor Found</h6>
                            @endforelse
                        
                            
                        </div>
                    @endforeach
                </div>
            </div>  
    </div>


@endsection