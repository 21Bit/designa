@extends('site.includes.layouts.full')

@section('page-title', 'Pricing')

@section('page-meta')
    <meta name="description" content="Designa pricing for subscription.">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Pricing | Designa">
    <meta property="og:description" content="Designa pricing for subscription.">
    <meta property="og:image" content="">
    <meta property="og:url" content="{{ route('site.pricing.index') }}">
@endsection


@section('content')
    <div class="container pt-4 pb-4">
        <!-- Table -->
        <div class="subs-content subs-desktop">
            <div class="container">
                <div class="row">
                    <table class="table  table-borderless table-responsive-sm">
                        <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                            <th scope="col" >
                                <h5 class="subs-header-popular rounded-top">Most Popular</h5>
                            </th>
                            <th scope="col"></th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col"></th>
                            <th scope="col" class="subs-header-lg">
                                <h5 >Beginner</h5>
                                <h4>$50.00</h4>
                                <h5>Per Month</h5>
                            </th>
                            <th scope="col"></th>
                            <th scope="col" class="subs-header-sm">
                                <h5>Intermediate</h5>
                                <h4>$100.00</h4>
                                <h5>Per Month</h5>
                            </th>
                            <th scope="col"></th>
                            <th scope="col" class="subs-header-sm">
                                <h5>Advance</h5>
                                <h4>$150.00</h4>
                                <h5>Per Month</h5>
                            </th>
                            <th scope="col"></th>
                            <th scope="col" class="subs-header-sm">
                                <h5>Pro</h5>
                                <h4>$250.00</h4>
                                <h5>Per Month</h5>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="shadow p-3 mb-5 bg-white rounded">
                                <h6>Online Presence</h6>
                            </td>
                            <td ></td>
                            <td class="shadow p-3 mb-5 bg-white rounded"></td>
                            <td></td>
                            <td class="shadow p-3 mb-5 bg-white rounded"></td>
                            <td></td>
                            <td class="shadow p-3 mb-5 bg-white rounded"></td>
                            <td></td>
                            <td class="shadow p-3 mb-5 bg-white rounded"></td>
                        </tr>
                        {{-- <tr>
                            <td class="shadow p-3 mb-5 bg-white rounded">
                                <div class="pl-3">Create an Account</div>
                            </td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                        </tr> --}}
                        <tr>
                            <td class="shadow p-3 mb-5 bg-white rounded">
                                <div class="pl-3">Load Product</div>
                            </td>
                            <td></td>
                            <td class="shadow p-3 mb-5 bg-white rounded"></td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                        </tr>
                        <tr>
                            <td class="shadow p-3 mb-5 bg-white rounded">
                                <div class="pl-3">Digital Catalogue / Look Book</div>
                            </td>
                            <td></td>
                            <td class="shadow p-3 mb-5 bg-white rounded"></td>
                            <td></td>
                            <td class="shadow p-3 mb-5 bg-white rounded"></td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                        </tr>
                        <tr>
                            <td class="shadow p-3 mb-5 bg-white rounded">
                            <div class="pl-3"> Your Products in 3D / 10+ is Pro</div>
                            </td>
                            <td></td>
                            <td class="shadow p-3 mb-5 bg-white rounded"></td>
                            <td></td>
                            <td class="shadow p-3 mb-5 bg-white rounded"></td>
                            <td></td>
                            <td class="shadow p-3 mb-5 bg-white rounded"></td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                        </tr>
                        <tr>
                            <td class="shadow p-3 mb-5 bg-white rounded2">
                                <div class="pl-3">Immersive 3D customised room styling using template</div>
                            </td>
                            <td></td>
                            <td class="shadow p-3 mb-5 bg-white rounded2"></td>
                            <td></td>
                            <td class="shadow p-3 mb-5 bg-white rounded2"></td>
                            <td></td>
                            <td class="shadow p-3 mb-5 bg-white rounded2"></td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded2"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                        </tr>

                        <tr>
                            <td class="shadow p-3 mb-5 bg-white rounded">
                                <h6>Showcasing yourself</h6>
                            </td>
                            <td></td>
                            <td class="shadow p-3 mb-5 bg-white rounded"></td>
                            <td></td>
                            <td class="shadow p-3 mb-5 bg-white rounded"></td>
                            <td></td>
                            <td class="shadow p-3 mb-5 bg-white rounded"></td>
                            <td></td>
                            <td class="shadow p-3 mb-5 bg-white rounded"></td>
                        </tr>
                        <tr>
                            <td class="shadow p-3 mb-5 bg-white rounded">
                                <div class="pl-3">Load Inspiration</div>
                            </td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                        </tr>
                        <tr>
                            <td class="shadow p-3 mb-5 bg-white rounded">
                                <div class="pl-3">Tag and be tagged</div>
                            </td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                            <td ></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                        </tr>
                        <tr>
                            <td class="shadow p-3 mb-5 bg-white rounded">
                                <div class="pl-3">Communicate with customers</div>
                            </td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                        </tr>
                        <tr>
                            <td class="shadow p-3 mb-5 bg-white rounded">
                                <div class="pl-3">Promote using featured marketing</div>
                            </td>
                            <td></td>
                            <td class="shadow p-3 mb-5 bg-white rounded"></td>
                            <td></td>
                            <td class="shadow p-3 mb-5 bg-white rounded"></td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                        </tr>
                        <tr>
                            <td class="shadow p-3 mb-5 bg-white rounded2">
                                <div class="pl-3">Register for the Designa VR Expo</div>
                            </td>
                            <td></td>
                            <td class="shadow p-3 mb-5 bg-white rounded2"></td>
                            <td></td>
                            <td class="shadow p-3 mb-5 bg-white rounded2"></td>
                            <td></td>
                            <td class="shadow p-3 mb-5 bg-white rounded2"></td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded2"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                        </tr>
                        <tr>
                            <td class="shadow p-3 mb-5 bg-white rounded">
                                <h6>Build your brand</h6>
                            </td>
                            <td></td>
                            <td class="shadow p-3 mb-5 bg-white rounded"></td>
                            <td></td>
                            <td class="shadow p-3 mb-5 bg-white rounded"></td>
                            <td></td>
                            <td class="shadow p-3 mb-5 bg-white rounded"></td>
                            <td></td>
                            <td class="shadow p-3 mb-5 bg-white rounded"></td>
                        </tr>
                        <tr>
                            <td class="shadow p-3 mb-5 bg-white rounded">
                                <div class="pl-3">Receive and Fulfill Quotes</div>
                            </td>
                            <td></td>
                            <td class="shadow p-3 mb-5 bg-white rounded"></td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                        </tr>
                        <tr>
                            <td class="shadow p-3 mb-5 bg-white rounded">
                                <div class="pl-3">Create Pitch Packs in Designa</div>
                            </td>
                            <td></td>
                            <td class="shadow p-3 mb-5 bg-white rounded"></td>
                            <td></td>
                            <td class="shadow p-3 mb-5 bg-white rounded"></td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                        </tr>
                        <tr>
                            <td class="shadow p-3 mb-5 bg-white rounded">
                                <div class="pl-3">Create your own event 3D using template</div>
                            </td>
                            <td></td>
                            <td class="shadow p-3 mb-5 bg-white rounded"></td>
                            <td></td>
                            <td class="shadow p-3 mb-5 bg-white rounded"></td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                        </tr>
                        <tr>
                            <td class="shadow p-3 mb-5 bg-white rounded">
                                <div class="pl-3">Transform a room with custom 3D Builds</div>
                            </td>
                            <td></td>
                            <td class="shadow p-3 mb-5 bg-white rounded"></td>
                            <td></td>
                            <td class="shadow p-3 mb-5 bg-white rounded"></td>
                            <td></td>
                            <td class="shadow p-3 mb-5 bg-white rounded"></td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                        </tr>
                        {{-- <tr>
                            <td class="shadow p-3 mb-5 bg-white rounded"></td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded">
                                <a href="#" class="btn-buy-now">Subscribe</a>
                            </td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded">
                                <a href="#" class="btn-buy-now">Subscribe</a>
                            </td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded">
                                <a href="#" class="btn-buy-now">Subscribe</a>
                            </td>
                            <td></td>
                            <td class="text-center shadow p-3 mb-5 bg-white rounded">
                                <a href="#" class="btn-buy-now">Subscribe</a>
                            </td>
                        </tr> --}}
                    </tbody>

                    </table>
                </div>
            </div>
        </div>
        <!-- end of table -->

        <!-- Mobile -->
        <div class="container subs-mobile">
            <div id="accordion">
                <!-- 1st Card -->
                <div class="card">
                    <div class="card-header" id="headingOne" style="background: #a1a1a1;">
                        <div>
                            <h5 class="mb-0">
                                <button class="btn btn-link btn-plan-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Beginner
                                </button>
                            </h5>


                            <div class="accordion-plan-details">
                                <h5 class="mb-0">$00.00</h5>
                                <h6>Per Month</h6>
                            </div>
                        </div>
                        <div class="accordion-clear"></div>
                    </div>


                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body accordion-body-bg">
                            <ul class="accordion-list">
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Create an Account</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Load Inspiration</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Tag and be tagged</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Communicate with customers</li>
                            </ul>
                            <div class="text-center">
                                <a href="#"><button type="button" class="btn-buy-now-mobile">Buy Now</button></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- 1st Card -->

                <!-- 2nd Card -->
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background: #a1a1a1;">
                        <div>
                            <h5 class="mb-0">
                                <button class="btn btn-link btn-plan-link" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                    Intermediate
                                </button>
                            </h5>


                            <div class="accordion-plan-details">
                                <h5 class="mb-0">$00.00</h5>
                                <h6>Per Month</h6>
                            </div>
                        </div>
                        <div class="accordion-clear"></div>
                    </div>


                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body accordion-body-bg">
                            <ul class="accordion-list">
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Create an Account</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Load Product</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Load Inspiration</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Tag and be tagged</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Communicate with customers</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Receive and Fulfill Quotes</li>
                            </ul>
                            <div class="text-center">
                                <a href="#"><button type="button" class="btn-buy-now-mobile">Buy Now</button></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- 2nd Card -->

                <!--3rd Card -->
                <div class="card">
                    <div class="card-header" id="headingThree" style="background: #f29833">
                        <div>
                            <h5 class="mb-0">
                                <button class="btn btn-link btn-plan-link" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                    Advance
                                </button>
                            </h5>


                            <div class="accordion-plan-details">
                                <h5 class="mb-0">$00.00</h5>
                                <h6>Per Month</h6>
                            </div>
                        </div>
                        <div class="accordion-clear"></div>
                    </div>


                    <div id="collapseThree" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body accordion-body-bg">
                            <ul class="accordion-list">
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Create an Account</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Load Product</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Digital Catalogue / Look Book</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Your Products in 3D / 10+ is Pro</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Loadin Inspiration</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Tag an be tagged</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Communicate with customers</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Promote using featured</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Receive and Fulfill Quotes</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Create Pitch Packs in Designa</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Create you own event 3D using template</li>
                            </ul>
                            <div class="text-center">
                                <a href="#"><button type="button" class="btn-buy-now-mobile">Buy Now</button></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- 3rd Card -->

                <!--4th Card -->
                <div class="card">
                    <div class="card-header" id="headingFour" style="background: #a1a1a1;">
                        <div>
                            <h5 class="mb-0">
                                <button class="btn btn-link btn-plan-link" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                                    Pro
                                </button>
                            </h5>


                            <div class="accordion-plan-details">
                                <h5 class="mb-0">$00.00</h5>
                                <h6>Per Month</h6>
                            </div>
                        </div>
                        <div class="accordion-clear"></div>
                    </div>


                    <div id="collapseFour" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body accordion-body-bg">
                            <ul class="accordion-list">
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Create an Account</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Load Product</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Digital Catalogue / Look Book</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Your Products in 3D / 10+ is Pro</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> 3D Virtual Showroom</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Immersive 3D customised room styling using template</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Loadin Inspiration</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Tag an be tagged</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Communicate with customers</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Promote using featured</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Register for the Designa VR Expo</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Receive and Fulfill Quotes</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Create Pitch Packs in Designa</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Create you own event 3D using template</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Transform a room with custom 3D Builds</li>
                            </ul>
                            <div class="text-center">
                                <a href="#"><button type="button" class="btn-buy-now-mobile">Buy Now</button></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- 4th Card -->





            </div>
        </div>

            <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                    ...
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection


@push('styles')
<style>
        /* For Desktop */
    .subs-header-lg {
            background: #f29833;
            color: #fff;
            text-align: center;
            padding-top: 10px;
            padding-bottom: 0;
            width: 15%;
        }

        .subs-content h5 {
            font-size: 16px;
            padding: 0;
        }

        .subs-content h4 {
            margin: 0;
            font-weight: bold;
        }

        .subs-header-sm {
            background: #a1a1a1;
            color: #fff;
            text-align: center;
            width: 15%;
        }

        .subs-content h6 {
            color: #f29833;
            font-size: 14px;
            text-align: center;
            padding-top: 20px;
            padding-bottom: 0;
        }

        .subs-list {
            font-weight: normal;
            list-style: none;
            padding-left: 0;
            line-height: 40px;
            font-size: 15px;
        }

        .subs-checklist {
            font-weight: normal;
            list-style: none;
            padding-left: 0;
            line-height: 40px;
        }

        .subs-fa {
            color: #f29833;
            font-size: 21px;
            font-weight: normal;
            text-align: center;
        }

        .rounded {
            border-radius: unset !important;
        }

        .rounded2 {
            border-bottom: 1px solid #ececec !important;
        }

        .subs-content .table td, .table th {
            padding: .25rem !important;
        }

        .shadow {
            box-shadow: none !important;
        }

        span {
            padding-left: 20px;
        }

        .btn-buy-now {
            margin-bottom: 20px;
            margin-top: 50px;
            background: none;
            border: 1px solid #f29833;
            color: #f29833;
            border-radius: 26px;
            padding: 10px 20px;"
        }

        .btn-buy-now:hover {
            background: #f29833;
            color: #fff;
        }

        h5.subs-header-popular {
            background: #f29833;
            color: #fff;
            text-align: center;
            padding-top: 10px;
            padding-bottom: 10px;
            margin-bottom: -4px;
            margin-left: 15px;
            margin-right: 15px;
        }

        .subs-mobile {
            display: none;
        }

        .subs-desktop {
            display: inherit;
        }


        @media  only screen
        and (min-device-width: 375px)
        and (max-device-width: 667px)
        and (-webkit-min-device-pixel-ratio: 2)
        and (orientation: portrait) {
            .subs-desktop {
                display: none !important
            }

            .subs-mobile {
                display: inherit !important;
            }

            /* For Mobile */
        .card-header {
            padding: 0 !important;
        }

        .subs-fa-mobile {
        color: #f29833;
        font-size: 21px;
        font-weight: normal;
    padding-right: 10px;

        }

        .btn-buy-now-mobile {
            margin-bottom: 20px;
            margin-top: 20px;
            background: none;
            border: 1px solid #f29833;
            color: #f29833;
            border-radius: 26px;
            padding: 10px 20px;"
        }

        .btn-buy-now-mobile:hover {
            background: #f29833;
            color: #fff;
        }

        .subs-mobile .btn:focus {
            box-shadow: none !important;
        }

        .subs-mobile {
            display: none;
        }

        .subs-desktop {
            display: inherit;
        }


        .subs-mobile h5 {
            float: left;
            padding-top: 13px;
        }

        .btn-plan-link {
            color: #fff !important;
            text-decoration: none !important;
            font-size: 20px;
        }

        .accordion-plan-details {
            padding: 13px 0.90rem 4px .75rem;
            color: #fff;
            float: right;
        }

        .accordion-clear {
            clear: both;
        }

        .accordion-body-bg {
            background: #fff;
        }

        ul.accordion-list {
            list-style: none;
            padding-left: 0;
            line-height: 30px;
        }
        }

    </style>
@endpush
