@extends('site.includes.layouts.no-menu')

@section('page-title', 'Supplier Registration')

@section('page-meta')
    <meta name="description" content="">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Register as Supplier">
    <meta property="og:description" content="">
    <meta property="og:image" content="/site/images/logo.svg">
    <meta property="og:url" content="{{ url('/') }}">
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-8 offset-sm-2">
                <div class="bg-white p-3 rounded mt-5 mb-5 supplier-register-box">
                    <div class="mb-2 text-center">
                        <small>Tell us about your business. (Not a professional? <a href="/">Continue to Designa</a>)</small>
                    </div>
                    <div class="row">
                        <div class="col-sm-7 p-5">
                            <form action="{{ route('site.register.supplier.store') }}" method="POST">
                                @csrf
                                <h5 class="text-center mb-3">Supplier Information</h5>
                                  <div class='mb-4'>
                                    <small for="" class="d-block mb-2">Professional / Company Name *</small>
                                    <input type="text" name="company_name" value="{{ old("company_name") }}" required class="form-control">
                                </div>
                                {{-- <div class="mb-4">
                                    <small for="" class="d-block mb-2">Account Complete Name *</small>
                                    <input type="text" name="name" value="{{ old("name") }}"  required class="form-control">
                                </div> --}}
                               <div class="mb-4">
                                    <small for="" class="d-block mb-2">Email *</small>
                                    <input type="email" name="email"  value="{{ old('email') }}"   required class="form-control @error('email') is-invalid @enderror">
                                    @error('email')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                              
                                 <div class='mb-4'>
                                    <small for="" class="d-block mb-2">Category *
                                        @error('categories')
                                            <div class="text-danger">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </small>
                                    @foreach($roles as $role)
                                        <div class="custom-control custom-checkbox mb-1">
                                            <input type="checkbox" class="custom-control-input" @if(in_array($role->id, old('categories') ?? [] )) checked @endif value="{{ $role->id }}" id="role-{{ $role->id }}" name="categories[]" >
                                            <label class="custom-control-label" for="role-{{ $role->id }}">{{ $role->display_name }}</label>
                                        </div>
                                    @endforeach
                                </div>
                                <div class='mb-4'>
                                    <small for="" class="d-block mb-2">Event types *
                                        @error('event_types')
                                            <div class="text-danger">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </small>
                                    @foreach($caters as $cater)
                                        <div class="custom-control custom-checkbox mb-1">
                                            <input type="checkbox" @if(in_array($cater->id, old('event_types') ?? [] )) checked @endif class="custom-control-input" value="{{ $cater->id }}" id="cater-{{ $cater->id }}" name="event_types[]" >
                                            <label class="custom-control-label" for="cater-{{ $cater->id }}">{{ $cater->name }}</label>
                                        </div>
                                    @endforeach
                                </div>
                                {{-- <div class='mb-4'>
                                    <small for="" class="d-block mb-2">Category *</small>
                                    <select name="role"  required class="form-control">
                                        <option value=""> select </option>
                                        @foreach($roles as $role)
                                            <option value="{{ $role->id }}" @if(old('role') == $role->id) selected @endif >{{ $role->display_name }}</option>
                                        @endforeach
                                    </select>
                                </div> --}}
                                <div class='mb-4'>
                                    <small for="" class="d-block mb-2">Town *</small>
                                    <input type="text" name="town" value="{{ old("town") }}" required class="form-control">
                                </div>
                                <div class='mb-4'>
                                    <small for="" class="d-block mb-2">State *</small>
                                    <input type="text" name="state" value="{{ old("state") }}" required class="form-control">
                                </div>
                                {{-- <div class='mb-4'>
                                    <small for="" class="d-block mb-2">Country *</small>
                                    <select name="country" required class="form-control">
                                        <option value=""> select </option>
                                        <option value="australia">Australia</option>
                                    </select>
                                </div> --}}
                                <div class='mb-4'>
                                    <small for="" class="d-block mb-2">Postcode *</small>
                                    <input type="text" name="post_code" value="{{ old("post_code") }}"  required class="form-control">
                                </div>
                                <div class='mb-4'>
                                    <small for="" class="d-block mb-2">Tel. Number *</small>
                                    <input type="text" name="telephone_number" value="{{ old("telephone_number") }}"   required class="form-control">
                                </div>
                            
                                <div class='mb-4'>
                                    <small for="" class="d-block mb-2">Password *</small>
                                    <input type="password" name="password" required class="form-control">
                                </div>
                                <div class='mb-4'>
                                    <small for="" class="d-block mb-2">Re-enter Password *</small>
                                    <input type="password" name="password_confirmation" required class="form-control">
                                </div>
                                <div class='mb-4 mt-3'>
                                    <div class="custom-control custom-checkbox mb-1">
                                        <input type="checkbox"  @if(old('agree-terms')) checked @endif class="custom-control-input" required id="terms-condition"
                                            name="agree-terms">
                                        <label class="custom-control-label" for="terms-condition">I agree in <a href="{{ route('site.term-condition') }}"  target="_blank" > Terms and Conditions.</a></label>
                                    </div>
                                </div>
                                  <div class='mb-4 mt-3'>
                                    <div class="custom-control custom-checkbox mb-1">
                                        <input type="checkbox" @if(old('agree-email')) checked @endif class="custom-control-input" id="email-updates"
                                            name="agree-email">
                                        <label class="custom-control-label" for="email-updates">I agree to receive email updates from the Designa Team.</label>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-block btn-lg btn-warning text-white"> Next</button>
                                
                            </form>
                        </div>
                        <div class="col-sm-5 pt-5 pl-3 pr-5">
                            <div class="card rounded-0 mt-5">
                                <div class="card-header p-2">
                                    Complete your Registration
                                </div>
                                <div class="card-body">
                                    <div class="row mb-3">
                                        <div class="col-3 text-center">
                                            <img src="/site/images/registration/business-name.svg" alt="">
                                        </div>
                                        <div class="col-9">
                                            Create an account name. Use your business name to resonate with potential customers.
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-3">
                                            <img src="/site/images/registration/select-category.svg" alt="">
                                        </div>
                                        <div class="col-9">
                                            Select the category(s) that best represent your product or service.
                                            Choose your primary event types.
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-3">
                                            <img src="/site/images/registration/info-password.svg" alt="">
                                        </div>
                                        <div class="col-9">
                                            Provide your contact information and password to complete account set up.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    

@endsection