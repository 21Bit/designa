@extends('site.includes.layouts.no-menu')

@section('page-title', 'Supplier Registration')

@section('page-meta')
    <meta name="description" content="">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Register as Customer">
    <meta property="og:description" content="">
    <meta property="og:image" content="/site/images/logo.svg">
    <meta property="og:url" content="{{ url('/') }}">
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-8 offset-sm-2">
                <div class="bg-white p-3 rounded mt-5 mb-5 supplier-register-box">
                    {{-- <div class="mb-2 text-center">
                        <small>Tell us about your business. (Not a professional? <a href="/">Continue to Designa</a>)</small>
                    </div> --}}
                    <div class="row">
                        <div class="col-sm-6 offset-sm-3 p-5">
                            <form action="{{ route('site.register.customer.store') }}" method="POST">
                                @csrf
                                <h5 class="text-center mb-3">Customer Information</h5>
                        
                                <div class="mb-4">
                                    <small for="" class="d-block mb-2"> Complete Name *</small>
                                    <input type="text" name="name" value="{{ old('name') }}" required class="form-control @error('name')  is-invalid @enderror">
                                    @error('name')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="mb-4">
                                    <small for="" class="d-block mb-2">Email *</small>
                                    <input type="email" name="email"  value="{{ old('email') }}"   required class="form-control @error('email') is-invalid @enderror">
                                    @error('email')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class='mb-4'>
                                    <small for="" class="d-block mb-2">Password *</small>
                                    <input type="password" name="password" required class="form-control @error('password') is-invalid @enderror">
                                    @error('password')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class='mb-4'>
                                    <small for="" class="d-block mb-2">Repeat Password *</small>
                                    <input type="password" name="password_confirmation" required class="form-control">
                                </div>
                                <div class='mb-4 mt-3'>
                                    <div class="custom-control custom-checkbox mb-1">
                                        <input type="checkbox" class="custom-control-input" required id="terms-condition"
                                            name="agree">
                                        <label class="custom-control-label" for="terms-condition">I agree in <a href="{{ route('site.term-condition') }}" target="_blank"> Terms and Conditions</a></label>
                                    </div>
                                </div>
                                <div class='mb-4 mt-3'>
                                    <div class="custom-control custom-checkbox mb-1">
                                        <input type="checkbox" class="custom-control-input" id="email-updates"
                                            name="agree">
                                        <label class="custom-control-label" for="email-updates">I agree to receive email updates from the Designa Team</label>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-block btn-lg btn-warning text-white"> Next</button>
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    

@endsection