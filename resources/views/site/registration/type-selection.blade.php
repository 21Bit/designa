@extends('site.includes.layouts.no-menu')

@section('page-title', 'Select your type')

@section('content')
<div style="background-image: url('/images/bg/register-type-bg.jpg'); background-position: left; background-size:cover;">
    <div class="container pt-5 pb-5">
        <div class="row">
            <div class="col-md-6 offset-md-3 p-5">
                <div class="text-center rounded p-4 mt-5 mb-5 register-type-box">
                    <h4>Which describes you best?</h4>
                    <div class="d-flex mt-5">
                        <div class="w-50">
                            <a href="{{ route('site.register.supplier') }}" class="register-type-icon-link">
                                <div class="icon mb-3">
                                    <i class="fa fa-briefcase"></i>
                                </div>
                                <label for="">Supplier/Vendor</label>
                            </a>
                        </div>
                        <div class="w-50">
                            <a href="" class="register-type-icon-link">
                                <div class="icon mb-3">
                                    <i class="fa fa-user-circle-o"></i>
                                </div>
                                <label for="">Customer</label>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div>
</div>
@endsection