@extends('site.includes.layouts.no-menu')

@section('page-title', 'Registration success')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-6 offset-sm-3">
            <div class="bg-white p-5 rounded mt-5 mb-5 supplier-register-box">
                <h4 class="text-center">Registration Complete</h4>
                <p>
                    <small>
                        Please activate your acount using the link that we send to your email address.
                    </small>
                </p>
                @if($agent->isMobile() )
                    @if(Request::get('t') == 's')
                    <p>
                        <small>
                            Please note to access the supplier dashboard you will need to login using your desktop
                        </small>
                    </p>
                    @endif
                    <br>
                    <div class="text-center">
                          <a href="{{ $appLink }}" class="mr-3 mb-3" target="_blank">
                                <img src="/img/play.svg" alt="">
                            </a>
                    </div>
                @else
                    <div class="text-center mt-5 text-muted">
                        <small>
                            <a class="m-2 text-muted" href="/">Back to HOME</a>
                        </small>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection