@extends('site.includes.layouts.full')

@section('page-title', 'Designa for Suppliers')

@section('page-meta')
    <meta name="description" content="Some suppliers that have already joined the Designa Studio Community">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Designa for Suppliers">
    <meta property="og:description" content="Some suppliers that have already joined the Designa Studio Community">
    <meta property="og:image" content="{{ url('/site/images/support.png') }}">
    <meta property="og:url" content="{{ url('/for-supplier') }}">
@endsection

@section('content')
    <div class="page-header-img">
        <h1 class="text-orange text-center">Designa for Supplier</h1>
        <img class="bg-image" src="/site/images/support.png" alt="">
        {{-- <img class="bottom-pattern" src="/site/images/bottom-pattern.png" alt=""> --}}
    </div>

    <div style="background-color:#e6e6e6">
        <div class="container">
            <ul class="nav nav-pills justify-content-center mb-3 custom-nav-pills" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link " id="pills-supplier-tab" href="{{ route('site.for-supplier', 'supplier-hub') }}" role="tab" aria-controls="pills-supplier" aria-selected="true">Supplier Hub</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-mobile-tab"  href="{{ route('site.for-supplier', 'mobile-app') }}" role="tab" aria-controls="pills-mobile" aria-selected="true">Mobile App</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" id="pills-resources-tab" href="{{ route('site.for-supplier', 'resources') }}" role="tab" aria-controls="pills-resources" aria-selected="false">Resources</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-advertising-tab"  href="{{ route('site.for-supplier', 'advertising-on-designa') }}" role="tab" aria-controls="pills-advertising" aria-selected="false">Advertising on Designa</a>
                </li>
                </ul>
        </div>
    </div>

    <div class="container">
            <div class="tab-pane active " id="webinar" role="tabpanel" aria-labelledby="pills-resources-tab" style="padding-top: 20px;">

                <!-- second tab -->
<nav>
  <div class="nav nav-tabs" id="nav-tab" role="tablist">
    <a class="nav-item nav-link @if(Request::get('tab') == '') active @endif" id="nav-webinar-tab"  href="/for-supplier/resources?tab=" role="tab" aria-controls="nav-webinar">Webinar Registration</a>
    <a class="nav-item nav-link @if(Request::get('tab') == 'nav-videos-tab') active @endif" id="nav-videos-tab"  href="/for-supplier/resources?tab=nav-videos-tab" role="tab" aria-controls="nav-videos" aria-selected="false">User Videos</a>
    <a class="nav-item nav-link @if(Request::get('tab') == 'nav-guidelines-tab') active @endif" id="nav-guidelines-tab" href="/for-supplier/resources?tab=nav-guidelines-tab" role="tab" aria-controls="nav-guidelines" aria-selected="false">Image Guidelines</a>
    {{-- <a class="nav-item nav-link @if(Request::get('tab') == 'nav-tagging-tab') active @endif" id="nav-tagging-tab" href="/for-supplier/resources?tab=nav-tagging-tab" role="tab" aria-controls="nav-tagging" aria-selected="false">Tagging Suppliers</a> --}}
    <a class="nav-item nav-link @if(Request::get('tab') == 'nav-blogs-tab') active @endif" id="nav-blogs-tab" href="/for-supplier/resources?tab=nav-blogs-tab" role="tab" aria-controls="nav-blogs" aria-selected="false">Blogs and Articles</a>
  </div>
</nav>
<div class="tab-content" id="nav-tabContent">

  <!-- Webinar Registration -->
  <div class="tab-pane fade  @if(Request::get('tab') == '') active show @endif" id="nav-webinar" role="tabpanel" aria-labelledby="nav-webinar-tab" style="padding-top: 50px;">
        <div class="text-center pt-2">
            <h1>Webinar Registration</h1>
                <p>Register for a webinar to learn the basics of Designa to get you started:</p>
                <ul class="webinar-list">
                    <li>Create a profile</li>
                    <li>Load your products</li>
                    <li>Create Inspiration</li>
                </ul>
            <p>
                We'll also be sharing tips and tricks to optimise your profile and get the most out of Designa platform.
            </p>
        </div>
        <hr>
        <div class="webform-box">
           <webinar-registration-form></webinar-registration-form>
        </div>
  </div>

  <!-- User Videos -->
  <div class="tab-pane fade @if(Request::get('tab') == 'nav-videos-tab') active show @endif" id="nav-videos" role="tabpanel" aria-labelledby="nav-videos-tab" style="padding-top: 50px;">
    <div class="text-center mt-2 pt-2" id="videos">
        <h1>User Guide Videos</h1>
        <p>New to Designa? Here are our user videos to help you get started</p>
    </div>

    <hr>

    <div class="d-none">
        <div class="row" style="padding-bottom: 30px; padding-top: 10px">
            <div class="col-lg-6">
                <input placeholder="Search" autocomplete="off" class="form-control form-control form-input-sm search-box">
            </div>
            <div class="col-lg-6 text-right">
                <i class="fa fa-th-large" style="padding-right:  15px; color: #f39c12; font-size: 30px;"></i><i class="fa fa-list" style="font-size: 30px;"></i>
            </div>
        </div>
    </div>

    <div class="row text-center">
        <div class="col-lg-4">
            <div class='embed-container'><iframe src='https://www.youtube.com/embed/Qp9wvJYp4-4' frameborder='0' allowfullscreen></iframe></div>
            <h5 class="title-space">Designa Mobile App</h5>
        </div>
        <div class="col-lg-4">
            <div class='embed-container'><iframe src='https://www.youtube.com/embed/A_4Ewv_-I8c' frameborder='0' allowfullscreen></iframe></div>
            <h5 class="title-space">How to Register as Supplier</h5>
        </div>
        <div class="col-lg-4">
            <div class='embed-container'><iframe src='https://www.youtube.com/embed/-NEjSzubR0A' frameborder='0' allowfullscreen></iframe></div>
            <h5 class="title-space">How to Register As Customer</h5>
        </div>
    </div>

    <div class="row text-center">
        <div class="col-lg-4">
            <div class='embed-container'><iframe src='https://www.youtube.com/embed/LA7v31cUwnU' frameborder='0' allowfullscreen></iframe></div>
            <h5 class="title-space">How to Filter Decor</h5>
        </div>
        <div class="col-lg-4">
            <div class='embed-container'><iframe src='https://www.youtube.com/embed/sIeR9KrW68Q' frameborder='0' allowfullscreen></iframe></div>
            <h5 class="title-space">How to Filter Inspiration</h5>
        </div>
        <div class="col-lg-4">
            <div class='embed-container'><iframe src='https://www.youtube.com/embed/FnWdAWTJqwM' frameborder='0' allowfullscreen></iframe></div>
            <h5 class="title-space">How to Filter Supplier</h5>
        </div>
    </div>

    <div class="row text-center">
        <div class="col-lg-4">
            <div class='embed-container'><iframe src='https://www.youtube.com/embed/tcFnU4d3ZOE' frameborder='0' allowfullscreen></iframe></div>
            <h5 class="title-space">How to Filter Venue</h5>
        </div>
        <div class="col-lg-4">
            <div class='embed-container'><iframe src='https://www.youtube.com/embed/NRp5-nfyA2Y' frameborder='0' allowfullscreen></iframe></div>
            <h5 class="title-space">How Dashboard Works</h5>
        </div>
        <div class="col-lg-4">
            <div class='embed-container'><iframe src='https://www.youtube.com/embed/duHTwYGcCm0' frameborder='0' allowfullscreen></iframe></div>
            <h5 class="title-space">Chat Function 1</h5>
        </div>
    </div>

    <div class="row text-center">
        <div class="col-lg-4">
            <div class='embed-container'><iframe src='https://www.youtube.com/embed/bBAtlOyb2qU' frameborder='0' allowfullscreen></iframe></div>
            <h5 class="title-space">Chat Function 2</h5>
        </div>
        <div class="col-lg-4">
            <div class='embed-container'><iframe src='https://www.youtube.com/embed/QQv4xc1CFgs' frameborder='0' allowfullscreen></iframe></div>
            <h5 class="title-space">How to Load Inspiration</h5>
        </div>
        <div class="col-lg-4">
            <div class='embed-container'><iframe src='https://www.youtube.com/embed/7K72cKKKusU' frameborder='0' allowfullscreen></iframe></div>
            <h5 class="title-space">Quotation Function</h5>
        </div>
    </div>
  </div>

  <!-- Image Guidelines -->
  <div class="tab-pane fade @if(Request::get('tab') == 'nav-guidelines-tab') active show @endif" id="nav-guidelines" role="tabpanel" aria-labelledby="nav-guidelines-tab" style="padding-top: 50px; color: #58585a;">

    <div class="row">
        <div class="col-lg-6">
            <img src="https://designa.studio/site/images/logo.svg" class="img-fluid logo-header">
        </div>
        <div class="col-lg-6">
            <div class="guidelines-box">
                <h1>
                    Guidelines for <br>
                    Loading Inspiration<br>
                    and Products
                </h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <p class="guidelines-margin">
                Designa aims to ensure images shared are high quality and showcase the best our registered users have to offer. We understand showcasing your events and products can take many forms, which is why we encourage our users to be creative. We do have a number of guidelines to help users in curating their content. We also actively review all items loaded and will from time to time provide feedback to users that do not meet our guidelines.
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="brush-bg">
                <h4>Product Guidelines</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="guidelines-margin-intro">
                <h5>Intent of Inspiration:</h5>
                <hr class="hr-orange">
                <div class="row">
                    <div class="col-lg-6">
                        <ul class="ulguidelines">
                            <li>
                                Inspiration is a marketing tool. Treat it like a retail shopfront window for your business.
                            </li>
                            <li>
                                Show your best work only, not just sharing content to maximise visibility.
                            </li>
                            <li>Quality over quantity.</li>
                            <li>The platform is optimised to show the best content to users. Poor content will be removed from public view or demoted in listings.</li>
                            <li>Aim for between 3-5 images per inspiration.</li>
                            <li>All images should be high quality and include a mix of theme, decor, styling and mood.</li>
                        </ul>
                    </div>
                    <div class="col-lg-6">
                        <img src="/site/images/intent-inspiration-img.png" class="img-fluid guidelines-img">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="guidelines-margin2">
                <h5>Image Criteria:</h5>
                <hr class="hr-orange">
                <ul class="ulguidelines">
                    <li>
                        Name of event should be descriptive and theme oriented.
                    </li>
                    <li>
                        Do not use names of people in the events.
                    </li>
                    <li>
                        Images in a single inspiration post need to be related, i.e. the same event.
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="guidelines-margin2">
                <h5>Showcasing events - recommendation for image gallery:</h5>
                <hr class="hr-orange">
                <ul class="ulguidelines">
                    <li>
                        Images showcase a design focal point.
                    </li>
                    <li>
                        Images convey a vibe and mood.
                    </li>
                    <li>
                        Interactive or functional.
                    </li>
                    <li>
                        Images match the themes and text descriptors.
                    </li>
                    <li>
                        Thumbnail image must be in an event setting.
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="brush-bg">
                <h4>Product Guidelines</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="guidelines-margin-intro">
                <h5>Loading Products:</h5>
                <hr class="hr-orange">
                <ul class="ulguidelines">
                    <li>
                        Product images must be professionally shot.
                    </li>
                    <li>
                        Focused on the product being displayed.
                    </li>
                    <li>
                        Include images which showcase the product in an event setting.
                    </li>
                    <li>
                        Multiple images per product is encouraged.
                    </li>
                    <li>
                        Product description must watch the images and vice versa.
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="guidelines-margin2">
                <h5>Filter optimisation:</h5>
                <hr class="hr-orange">
                <ul class="ulguidelines">
                    <li>
                        Images and products are optmised for users based on specific criteria;
                        <ul style="list-style: none;">
                            <li>
                                Rating - in house rating of image quality.
                            </li>
                            <li>
                                Views and recognition (favourites).
                            </li>
                            <li>
                                Reviews.
                            </li>
                            <li>
                                Geographic proximity to users.
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="guidelines-margin2">
                <h5>Language:</h5>
                <hr class="hr-orange">
                <ul class="ulguidelines">
                    <li>
                        Use of capital letters in title and as appropriate.
                    </li>
                    <li>
                        No inappropriate language.
                    </li>
                    <li>
                        Minimal and appropriate use of emojis
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="guidelines-margin2">
                <h5>Image Guidelines:</h5>
                <hr class="hr-orange">
                <div class="row">
                    <div class="col-lg-6">
                        <ul class="ulguidelines">
                            <li>
                                Images should be high quality and clean (i.e. with no attached Instagram filters.)
                            </li>
                            <li>
                                Images should be professionally shot or high quality.
                            </li>
                            <li>
                                Images must be high-resolution and un-watermarked
                            </li>
                            <li>
                                Name, website, and social media handle of photographer must be provided.
                            </li>
                            <li>
                                Images should not be excessively bright or monochrome.
                            </li>
                            <li>
                                Whenever possible, images should show up close details of the item posted.
                            </li>
                            <li>
                                Images should be inspirational and natural, but not excessively modern or minimal.
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-6">
                        <img src="/site/images/loading-products-img.png" class="img-fluid guidelines-img">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 guidelines-margin-intro">
            <p>
                In some instances, images will be unpublished and return to draft status. Users will receive an email notification in these instances and are invited to review and adjust the images before republishing.
            </p>
            <p>
                When an image is unpublished, users will receive an email detailing why the image is unpublished. Some of the reasons that images might be unpublished include;
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="guidelines-margin2">
                <ul class="ulguidelines">
                    <li>
                        Poor image quality.
                    </li>
                    <li>
                        Incorrect image size.
                    </li>
                    <li>
                        Inappropriate image.
                    </li>
                    <li>
                        Image ownership dispute.
                    </li>
                    <li>
                        Image does not clearly represent the showcased product or service.
                    </li>
                    <li>
                        Poor resolution image, banner or logo.
                    </li>
                    <li>
                        Misleading or inaccurate information displayed.
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <p class="guidelines-margin-intro">
                We’re always here to help and can provide tips and suggestions on how to improve image presentation. You can contact us at anytime via <a href="mailto:hello@designa.studio">hello@designa.studio</a>
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="guidelines-margin2">
                <h5>Recommended image dimensions:</h5>
                <hr class="hr-orange">
                <ul style="list-style: none; padding-left: 0;">
                    <li>
                        Recommended photo quality -  1080 x 1080 px
                    </li>
                    <li>
                        Carousel 1080 x 1080 px
                    </li>
                    <li>
                        Banner Image 728 x 90 px Medium Rectangle (MREC) 300 x 250 px
                    </li>
                    <li>
                        Logo 180 x 90 px
                    </li>
                    <li>
                        Thumbnail/Feature image 926 x 260 px
                    </li>
                    <li>
                        Gallery (20 images max.) 420 x 315 px
                    </li>
                    <li>
                        Preferred file formats: jPEG, PNG
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="brush-bg">
                <h4>Community Guidelines</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 guidelines-margin-intro">
            <p>
                We want to ensure the platform is enjoyed by event suppliers and professionals and those looking to be inspired or to plan their next event. Some basic guidelines are outlined here, with further terms available on the platform for those who need the fine print.
            </p>
            <hr class="hr-orange">
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="guidelines-margin2">
                <div class="row">
                    <div class="col-lg-6">
                        <h6>Avoid using the Designa brand in a way that:</h6>
                        <ul class="ulguidelines">
                            <li>
                                Misrepresents the Designa platform.
                            </li>
                            <li>
                                Is not compliant with our terms of use.
                            </li>
                        </ul>

                        <h6>Avoid sharing content that is:</h6>
                        <ul class="ulguidelines">
                            <li>
                                Not in the spirit of our community objectives and beliefs.
                            </li>
                            <li>
                                May be hurtful or offensive.
                            </li>
                        </ul>
                        <div class="row">
                            <div class="col-lg-12"><h6>Content permissions:</h6>
                                <ul class="ulguidelines">
                                    <li>
                                        Ensure you have the right to share content you choose to publish.
                                    </li>
                                    <li>
                                        Follow the law. Do not post illegal images or nudity.
                                    </li>
                                    <li>
                                        Do not spam or engage platform users in a way that is not welcomed.
                                    </li>
                                    <li>
                                        Focus on meaningful interactions and not spam.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <img src="https://designa.studio/site/images/logo.svg" class="img-fluid logo-content">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 pcheckout">
            <p>
                Check out our knowledge articles for more information as well as a list of do’s and don’ts for the Designa platform.
            </p>
        </div>
    </div>
  </div>

  <!-- Tagging Suppliers -->
  @if(isset($get_blog))
    <div class="tab-pane fade @if(Request::get('tab') == 'nav-tagging-tab/'.$get_blog->id) active show @endif" id="nav-tagging" role="tabpanel" aria-labelledby="nav-tagging-tab" style="padding-top: 50px; color: #58585a;">
        <section class="tag-supplies">
            <div class="row flex-column-reverse flex-md-row">
                <div class="col-lg-8">
                    <div class="mr-2 individual-box">
                        <p style="color: #f39c12;">{{ $get_blog->category }}</p>
                        <h4>{{ $get_blog->title }}</h4>
                        <img class="d-block img-fluid" src="{{ $get_blog->image('thumbnail')->path('blogs','/images/placeholders/placeholder250x200.png') }}" alt="">

                        <div class="mt-3">
                            {!! $get_blog->description !!}
                        </div>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="">
                        <p style="color: #f39c12;">Recent Posts</p>
                        <div class="recent-box">
                            @forelse ($recent_blogs as $recent)
                            <div class="row mb-2 individual-box">
                                <div class="col-md-7">
                                    <a class="
                                        @if(Request::get('tab') == 'nav-tagging-tab')
                                        {{ $key == 0 ? 'active' : '' }} @endif" id="nav-tagging-tab"
                                        href="/for-supplier/resources?tab=nav-tagging-tab/{{  $recent->id }}"
                                        role="tab" aria-controls="nav-tagging"
                                        aria-selected="false"
                                    >
                                        <img src="{{ $recent->image('thumbnail')->path('blogs','/images/placeholders/placeholder250x200.png') }}"
                                            class="img-fluid" alt="{{ $recent->title }}" width="210">

                                    </a>
                                </div>
                                <div class="col-md-3">
                                    <a style="color: rgb(29, 29, 29);" class="
                                        @if(Request::get('tab') == 'nav-tagging-tab')
                                        {{ $key == 0 ? 'active' : '' }} @endif" id="nav-tagging-tab"
                                        href="/for-supplier/resources?tab=nav-tagging-tab/{{  $recent->id }}"
                                        role="tab" aria-controls="nav-tagging"
                                        aria-selected="false"
                                    >
                                        <h5>{{ $recent->title }}</h5>
                                    </a>
                                </div>
                            </div>
                            @empty
                                <div class="recent-box">
                                    <h4><i>No blog post yet.</i></h4>
                                </div>
                            @endforelse
                        </div>
                        <div style="clear: both;"></div>
                    </div>
                </div>

            </div>
        </section>
    </div>
  @endif

  <!-- Blogs and Articles -->
  <div class="tab-pane fade  @if(Request::get('tab') == 'nav-blogs-tab') active show @endif" id="nav-blogs" role="tabpanel" aria-labelledby="nav-blogs-tab" style="padding-top: 50px; color: #58585a;">
    <div class="row">
        <div class="col-lg-4"></div>
        <div class="col-lg-4">
            <img src="https://designa.studio/site/images/logo.svg" class="img-fluid blog-articles-logo">
        </div>
        <div class="col-lg-4">
            <input class="form-control pull-right blog-articles-search" type="text" placeholder="Search" aria-label="Search">
        </div>
    </div>

    <hr>

    <section class="recent-articles">
        <div class="row">
            <div class="col-lg-12">
                <!-- carousel -->
                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        @forelse ($blogs as $key => $blog)
                            <div class="carousel-item {{ $key == 0 ? 'active' : '' }}">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <a class="
                                            @if(Request::get('tab') == 'nav-tagging-tab')
                                            {{ $key == 0 ? 'active' : '' }} @endif" id="nav-tagging-tab"
                                            href="/for-supplier/resources?tab=nav-tagging-tab/{{  $blog->id }}"
                                            role="tab" aria-controls="nav-tagging"
                                            aria-selected="false"
                                        >
                                            <img class="d-block img-fluid"
                                             src="{{ $blog->image('thumbnail')->path('blogs','/images/placeholders/placeholder250x200.png') }}"
                                             alt="">
                                        </a>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="carousel-caption d-md-block">
                                            <p style="color: #f39c12;">{{ $blog->category }}</p>
                                            <h1 style="text-align: left;">{{ $blog->title }}</h1>
                                            <div>
                                                {!! substr($blog->description, 0,  230) !!}....
                                                <a class="
                                                    @if(Request::get('tab') == 'nav-tagging-tab')
                                                    {{ $key == 0 ? 'active' : '' }} @endif" id="nav-tagging-tab"
                                                    href="/for-supplier/resources?tab=nav-tagging-tab/{{  $blog->id }}"
                                                    role="tab" aria-controls="nav-tagging"
                                                    aria-selected="false"
                                                ><small>Read more</small></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        @empty
                        <div class="p-5">
                            <h4>
                                <i>No Blog Post yet.</i>
                            </h4>
                        </div>
                        @endforelse

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 recent-articles-title">
                <h5>Recent Articles</h5>
                <hr class="hr-orange">
            </div>
        </div>

        <div class="row">
            @forelse ($recent_blogs as $recent)
                <div class="col-lg-4">
                    <div class="mr-2">
                        <a class="
                            @if(Request::get('tab') == 'nav-tagging-tab')
                            {{ $key == 0 ? 'active' : '' }} @endif" id="nav-tagging-tab"
                            href="/for-supplier/resources?tab=nav-tagging-tab/{{  $blog->id }}"
                            role="tab" aria-controls="nav-tagging"
                            aria-selected="false"
                        >
                            <img src="{{ $recent->image('thumbnail')->path('blogs','/images/placeholders/placeholder250x200.png') }}" class="img-fluid">
                            <h6>{{ $recent->category }}</h6>
                        </a>
                        <p>
                            {!! substr($recent->description, 0,  200) !!}....<a class="
                            @if(Request::get('tab') == 'nav-tagging-tab')
                            {{ $key == 0 ? 'active' : '' }} @endif" id="nav-tagging-tab"
                            href="/for-supplier/resources?tab=nav-tagging-tab/{{  $blog->id }}"
                            role="tab" aria-controls="nav-tagging"
                            aria-selected="false"
                        ><small>Read more</small></a>
                        </p>
                    </div>
                </div>
            @empty
                <h4><i>No Blog Post yet.</i></h4>
            @endforelse
        </div>

        <!--
        <div class="pull-right">
            <a href="#">VIEW ALL</a>
        </div>-->

    </section>

    <section class="latest-post">
        <div class="row">
            <div class="col-lg-12 latest-post-title">
                <h5>Latest Post</h5>
                <hr class="hr-orange">

                <div class="latest-post-box">
                    <img src="/site/images/designa-sketch-logo.png" class="img-fluid designa-sketch-logo">
                    <h5>designastudio.au</h5>
                    <p>
                        We're an online marketplace for the events industry.
                    </p>
                </div>

                <!-- SnapWidget -->
                <script src="https://snapwidget.com/js/snapwidget.js"></script>
                <iframe src="https://snapwidget.com/embed/866771" class="snapwidget-widget" allowtransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden;  width:100%; "></iframe>

                <div class="text-center">
                    <a href="https://www.instagram.com/designastudio.au/" target="_blank">VIEW MORE POST</a>
                </div>

                <div class="get-newsletter">
                    <h1>GET OUR NEWSLETTER</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <div class="get-newsletter-box">
                        <input type="text" class="form-control get-email" placeholder="E-MAIL">
                        <button type="submit" class="form-control get-subscribe">SUBSCRIBE</button>
                    </div>
                </div>
            </div>
        </div>
    </section>

  </div>

</div>
<!-- second tab -->

            </div>
            <br>



        </div>
    </div>


@endsection

@push('scripts')
    <script>
       $(function() {
    $('.scroll-down').click (function() {
      $('html, body').animate({scrollTop: $('.ok').offset().top }, 'slow');
      return false;
    });
  });
    </script>

    <script>
        $(function () {
            var hash = window.location.hash;
            hash && $('ul.nav a[href="' + hash + '"]').tab('show');

            // $(document).ready(function(){
            //     window.open(window.location.href, '_self')
            // })

        });
    </script>


@endpush

@push('styles')
    <style>


    .need-box h4 {
            padding-top: 20px;
            padding-bottom: 10px;
        }

        .fa-briefcase, .fa-laptop {
            font-size: 47px;
        }

        .scroll-down {
            height: 50px;
    width: 50px;
    background-color: #f39c12;
    border-radius: 50%;
    display: inline-block;
    color: #fff;
    float: right;
    font-size: 29px;
        }

        .custom-nav-pills{
            display:flex;
        }

        .custom-nav-pills .nav-item{
            flex:1;
        }
         .custom-nav-pills .nav-link.active, .custom-nav-pills .show >.nav-link {
            border-top: 4px solid!important;
            font-weight: 500;
            background-color: #f7f7f7;
            border-color: transparent transparent #f39c12;
            color: #f39c12;
            border-radius: inherit;
        }

        .custom-nav-pills .nav-link {
            display: block;
            text-align: center;
            border-top: 4px solid #e6e6e6;
            padding:1em 0px 1em 0px;
            font-size:18px;
        }

        h1 {
            color: #343433;
            text-align: center;
            font-size: 31px;
        }

        .send-form-box {
            background: rgb(230, 230, 230);
            text-align: left;
            padding: 20px 110px;
            margin-top: 24px;
        }

        .send-row {
            display: grid;
            grid-template-columns: auto 1fr auto;
            grid-gap: 15px;
            align-items: center;
        }

        .store-icon {
            display: grid;
            grid-template-columns: 1fr 1fr;
            grid-gap: 15px;
            align-items: center;
        }

        .stores {
            padding: 0 267px;
        }

        .embed-container {
            position: relative;
            padding-bottom: 56.25%;
            height: 0;
            overflow: hidden;
            max-width: 100%;
        }

        .embed-container iframe,
        .embed-container object,
        .embed-container embed,
        .embed-container-resources iframe,
        .embed-container-resources object,
        .embed-container-resources embed {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }

        .embed-container-resources {
            position: relative;
            padding-bottom: 43.25%;
            height: 0;
            overflow: hidden;
            max-width: 70%;
            margin: 0 auto;
            text-align: center;
        }

        .agree-text {
            font-size: 12px;
            padding: 20px 125px;
        }

        .search-box {
            width: 357px;
        }

        .title-space {
            padding-top: 10px;
        }

        .benefits {
            padding-top: 77px;
            padding-bottom: 30px;
        }

        .reach-box {
            background: url(/site/images/icon/reach-new-customers.png);
            height: 246px;
            width: 356px;
        }

        .directly-box {
            background: url(/site/images/icon/directly-engage-and-inspire.png);
            height: 236px;
            width: 356px;
        }

        .instantly-box {
            background: url(/site/images/icon/instantly-notify-customers.png);
            height: 236px;
            width: 356px;
        }

        .tag-box {
            background: url(/site/images/icon/tag-share-and-promote.png);
            height: 236px;
            width: 356px;
        }

        .receive-box {
            background: url(/site/images/icon/receive-real%20time-quotes.png);
            height: 236px;
            width: 356px;
        }

        .message-box {
            background: url(/site/images/icon/message-and-engage-clients.png);
            height: 236px;
            width: 356px;
        }

        .clients-box {
            background: url(/site/images/icon/designa-studio-allows.png);
            height: 236px;
            width: 356px;
        }

        .reach-box p,
        .directly-box p,
        .instantly-box p,
        .tag-box p,
        .receive-box p,
        .message-box p,
        .clients-box p {
            position: relative;
            top: 50%;
            text-align: center;
            left: 0;
            right: 0;
            padding-left: 50px;
            padding-right: 50px;
        }

        .benefits-space {
            padding-top: 40px;
        }

        .suppliers img {
            width: 200px;
        }

        .btn-supplier {
            width: 300px;
        }

        #inquiry-box {
            position: fixed;
            bottom: 40px;
            right: 40px;
            z-index: 9999;
            width: 450px;
        }

        .need-box {
            background: #fff;
            /*padding: 57px 67px;*/
            padding: 43px 73px;
            margin-top: 57px;
            text-align: center;
        }

        .page-header-img h1 {
            display: none;
        }

        .advertising-text {
            /*font-size: 18px;
            padding: 30px;
            border: 1px solid #e6e6e6;*/
            color: #f39c12;
            font-size: 36px;
            font-weight: bold;
            font-family: 'Poppins', sans-serif;
        }

        .webform-box {
            padding-bottom: 16px;
            padding-left: 20%;
            padding-right: 20%;
        }

        .countdown li {
          display: inline-block;
          font-size: 1.5em;
          list-style-type: none;
          padding: 1em;
          text-transform: uppercase;
        }

        .countdown li span {
          display: block;
          font-size: 4.5rem;
        }

        .launching {
          text-align: center;
          padding-bottom: 26px;
          color: rgb(243, 156, 18);
          font-size: 28px;
        }

        .countdown-box {
            padding: 30px 0 0 0;
            text-align: center;
        }

        .webinar-list {
             width: 19%;
             margin: auto;
             text-align: left;
             padding-bottom: 20px;
        }


        /* For Image Guidelines */
        .logo-header {
            width: 200px;
            margin: 71px auto 0px auto;
            display: block;
        }

        .logo-content {
            width: 200px;
            margin: 109px auto 0px auto;
            display: block;
        }

        ul.ulguidelines {
            list-style: none; /* Remove default bullets */
        }

        ul.ulguidelines li::before {
          content: "\2022";  /* Add content: \2022 is the CSS Code/unicode for a bullet */
          color: rgb(247, 148, 30); /* Change the color */
          font-weight: bold; /* If you want it to be bold */
          display: inline-block; /* Needed to add space between the bullet and the text */
          width: 20px; /* Also needed for space (tweak if needed) */
          margin-left: -23px; /* Also needed for space (tweak if needed) */
          font-size: 22px;
          line-height: 20px;
        }

        .guidelines-box {
            background: #f7941e;
        }

        .guidelines-box h1 {
            text-align: left;
            color: #fff;
            padding-left: 48px;
            padding-top: 40px;
            padding-bottom: 40px;
            font-size: 38px;
        }

        .guidelines-margin {
            padding: 50px 100px 25px 100px;
        }

        .brush-bg {
            background: url(/site/images/brush-bg.png);
            height: 78px;
            background-size: contain;
            background-repeat: no-repeat;
            background-position: left center;
            margin-top: 20px;
        }

        .brush-bg h4 {
            padding-top: 26px;
            padding-left: 87px;
            font-size: 24px;
            color: #fff;
        }

        .guidelines-margin-intro {
           padding: 20px 100px 10px 100px;
        }

        .guidelines-margin2 {
            padding: 10px 100px;
        }

        .guidelines-margin2 h6 {
            font-weight: bold;
            padding-top: 10px;
        }

        .hr-orange {
            margin-top: 0;
            border-top: 1px solid #ffd19b;
        }

        .guidelines-img {
            height: 233px;
            margin: 14px auto 0 auto;
            display: block;
        }

        .pcheckout {
            padding: 0px 100px 10px 100px;
        }

        /* For Blogs and Articles */
        .recent-articles h6 {
            font-weight: bold;
            color: #f39c12;
            padding-top: 20px;
        }

        .recent-articles h5,
        .latest-post h5  {
            font-weight: bold;
        }

        .latest-post iframe {
            height: 583px !important
        }


        .blog-articles-box {
            padding-top: 50px;
            padding-bottom: 50px;
            color: #58585a;
        }

        .blog-articles-logo {
        text-align: center;
        width: 164px;
        }

        .blog-articles-search {
        width: 230px;
        }

        .recent-articles {
        padding-bottom: 50px;
        }

        .recent-articles-title {
            padding-top: 60px;
        }

        .latest-post-title {
            padding-top: 40px;
        }

        .latest-post-box {
        padding-top: 20px;
        text-align: center;
        }

        .designa-sketch-logo {
        width: 146px;
        margin: 0 auto;
        display: block;
        padding-bottom: 20px;
        }

        .get-newsletter {
        padding-top: 60px;
        text-align: center;
        padding-left: 300px;
        padding-right: 300px;
        }

        .get-newsletter h1 {
        color: #f39c12;
        font-weight: bold;
        }

        .get-newsletter-box {
        padding-left: 70px;
        padding-right: 70px;
        padding-top: 10px;
        }

        .get-email {
        width: 247px;
        float: left;
        border: none;
        background: #f0f0f0;
        }

        .get-subscribe {
        width: 123px; border: none;
        background: #f7941e;
        color: #fff;
        border-radius: initial;
        }


        /* Carousel */
        .carousel-control-prev {
            left: 70% !important;
        }

        .carousel-control-next {
            right: 26% !important;
        }

        .carousel-control-next, .carousel-control-prev {
            top: 356px !important;
            width: 20px !important;
            z-index: 100 !important;
        }

        .carousel-control-prev-icon,
        .carousel-control-next-icon {
            filter:
        /* for demonstration purposes; originals not entirely black */
        contrast(1000%)
        /* black to white */
        invert(100%)
        /* white to off-white */
        sepia(100%)
        /* off-white to yellow */
        saturate(10000%)
        /* do whatever you want with yellow */
        hue-rotate(90deg);
        }

        #carouselExampleControls {
            -webkit-box-shadow: -1px 1px 28px 3px rgba(0,0,0,0.08);
            -moz-box-shadow: -1px 1px 28px 3px rgba(0,0,0,0.08);
            box-shadow: -1px 1px 28px 3px rgba(0,0,0,0.08);
        }

        .carousel-caption {
            top: 16px !important;
            text-align: left !important;
            left: 5% !important;
            color: #000 !important;
        }

         /* Individual Blog */
        .individual-box {
            -webkit-box-shadow: -1px 1px 28px 3px rgba(0,0,0,0.08);
            -moz-box-shadow: -1px 1px 28px 3px rgba(0,0,0,0.08);
            box-shadow: -1px 1px 28px 3px rgba(0,0,0,0.08);
            padding: 20px;
        }

        @media (min-width: 1561px) {
            .carousel .carousel-item {
                height: auto !important;
            }
        }


        @media (max-width: 480px) and (min-width: 320px) {
            .carousel .carousel-item {
                height: 600px;
            }
        }



        /* Portrait */
        @media only screen
          and (min-device-width: 375px)
          and (max-device-width: 667px)
          and (-webkit-min-device-pixel-ratio: 2)
          and (orientation: portrait) {
              .custom-nav-pills {
                display: block !important;
                padding-bottom: 22px !important;
              }

              .form-space {
                 padding-bottom: 10px;
              }

              .send-row {
                 display: block !important;
              }

              .send-form-box {
                padding: 20px 42px !important;
                text-align: center !important;
              }

              .agree-text {
                padding: 13px 0px 0px 0px !important;
                text-align: center !important;
              }

              .stores {
                padding: 0 60px !important;
              }

              .search-box {
                width: 205px !important;
              }

              .title-space {
                padding-bottom: 10px;
             }

             .suppliers img {
                 float: none !important;
             }

             .btn-supplier {
                width: 257px !important;
                margin-bottom: 14px !important;
                float: none !important;
            }

            .header-search,
            .header-links,
            .sections,
            .links {
                display: none !important;
            }

            #inquiry-box {
                width: auto !important;
            }

            .need-box {
                padding: 57px 37px !important;
            }

            .embed-container-resources {
                padding-bottom: 53% !important;
                max-width: 100% !important;
            }

            .logo {
                height: auto !important;
            }

            .launching {
                padding-left: 20px;
                padding-right: 20px;
            }

            .countdown-box {
                text-align: left !important;
            }

            .webinar-list {
                width: 68% !important;
                padding-bottom: 16px !important;
            }

            .webform-box {
                padding-left: 0 !important;
                padding-right: 0 !important;
            }

            /* For guidelines */
            .logo-header {
                margin: 0px auto 44px auto !important;
            }

            .guidelines-margin {
                padding: 25px 40px 0px 40px !important;
            }

            .guidelines-margin-intro {
                padding: 20px 40px 10px 40px !important;
            }

            .guidelines-margin2 {
                padding: 10px ;
            }

            .guidelines-img {
                margin: 14px auto 17px auto !important;
            }

            .logo-content {
                margin: 30px auto !important;
            }

            .pcheckout {
                padding: 0px 40px 10px 40px !important;
            }

            .nav-tabs {
                display: block !important;
            }

            /* For Get Newsletter */
            .get-newsletter,
            .get-newsletter-box {
                padding-left: 0 !important;
                padding-right: 0 !important;
            }

            .blog-articles-search {
                width: 170px !important;
            }

            .blog-articles-logo {
                margin: 0 auto !important;
                display: block !important;
                padding-bottom: 28px !important;
            }

            .latest-post iframe {
                height: 201px !important;
            }

            .get-email {
                width: 219px !important;
            }

            /* Carouse */
            .carousel-caption {
                top: 0 !important;
                left: 14% !important
            }

            .carousel-control-next, .carousel-control-prev {
                top: 496px !important;
            }

            .carousel-control-prev {
                left: 40px !important;
            }

            .carousel-control-next {
                    right: 75% !important;
            }

            .latest-post-title {
                padding-top: 0 !important;
            }
    </style>


@endpush
