@extends('site.includes.layouts.full')

@section('page-title', 'Designa for Suppliers')

@section('page-meta')
    <meta name="description" content="Some suppliers that have already joined the Designa Studio Community">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Designa for Suppliers">
    <meta property="og:description" content="Some suppliers that have already joined the Designa Studio Community">
    <meta property="og:image" content="{{ url('/site/images/support.png') }}">
    <meta property="og:url" content="{{ url('/for-supplier') }}">
@endsection

@section('content')
    <div class="page-header-img">
        <h1 class="text-orange text-center">Designa for Supplier</h1>
        <img class="bg-image" src="/site/images/support.png" alt="">
        {{-- <img class="bottom-pattern" src="/site/images/bottom-pattern.png" alt=""> --}}
    </div>
    
    <div style="background-color:#e6e6e6">
        <div class="container">
            <ul class="nav nav-pills justify-content-center mb-3 custom-nav-pills" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link " id="pills-supplier-tab" href="{{ route('site.for-supplier', 'supplier-hub') }}" role="tab" aria-controls="pills-supplier" aria-selected="true">Supplier Hub</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-mobile-tab"  href="{{ route('site.for-supplier', 'mobile-app') }}" role="tab" aria-controls="pills-mobile" aria-selected="true">Mobile App</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-resources-tab" href="{{ route('site.for-supplier', 'resources') }}" role="tab" aria-controls="pills-resources" aria-selected="false">Resources</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" id="pills-advertising-tab"  href="{{ route('site.for-supplier', 'advertising-on-designa') }}" role="tab" aria-controls="pills-advertising" aria-selected="false">Advertising on Designa</a>
                </li>                  
                </ul>
        </div>
    </div>
    
    {{-- <div class="container countdown-box">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="launching">Designa Studio is officially launching to the public on the 1st July 2020. Are you ready?</h4>
                <ul class="countdown">
                    <li><span id="days"></span>days</li>
                    <li><span id="hours"></span>Hours</li>
                    <li><span id="minutes"></span>Minutes</li>
                    <li><span id="seconds" style="color: #f39c12;"></span>Seconds</li>
                </ul>
            </div>
        </div>    
    </div>
    
    <hr> --}}

    <div class="container">
        <div class="tab-content" id="pills-tabContent" style="padding-top: 20px; padding-bottom: 60px; ">
    
            <div class="tab-pane active" id="pills-advertising" role="tabpanel" aria-labelledby="pills-advertising-tab">
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-10 text-center advertising-text">
                        Advertising opportunities will be available on Designa shortly. Watch this space.            
                    </div>
                    <div class="col-lg-1"></div>
                </div>     
            </div>

        </div>
    </div>


@endsection

@push('scripts')
    <script>
        const second = 1000,
        minute = second * 60,
        hour = minute * 60,
        day = hour * 24;

        let countDown = new Date('Jul 01, 2020 00:00:00').getTime(),
            x = setInterval(function() {    

            let now = new Date().getTime(),
                distance = countDown - now;

            document.getElementById('days').innerText = Math.floor(distance / (day)),
                document.getElementById('hours').innerText = Math.floor((distance % (day)) / (hour)),
                document.getElementById('minutes').innerText = Math.floor((distance % (hour)) / (minute)),
                document.getElementById('seconds').innerText = Math.floor((distance % (minute)) / second);

            //do something later when date is reached
            //if (distance < 0) {
            //  clearInterval(x);
            //  'IT'S MY BIRTHDAY!;
            //}

            }, second)
    
    </script>
    
    <script>
       $(function() {
    $('.scroll-down').click (function() {
      $('html, body').animate({scrollTop: $('.ok').offset().top }, 'slow');
      return false;
    });
  });
    </script>
    
    <script>
        $(function () {
            var hash = window.location.hash;
            hash && $('ul.nav a[href="' + hash + '"]').tab('show');

            // $(document).ready(function(){
            //     window.open(window.location.href, '_self')
            // })

        });
    </script>


@endpush

@push('styles')
    <style>
    
        .need-box h4 {
            padding-top: 20px;
            padding-bottom: 10px;
        }
 
        .fa-briefcase, .fa-laptop {
            font-size: 47px;
        }
        
        .scroll-down {
            height: 50px;
    width: 50px;
    background-color: #f39c12;
    border-radius: 50%;
    display: inline-block;
    color: #fff;
    float: right;
    font-size: 29px;
        }
        
        .custom-nav-pills{
            display:flex;
        }

        .custom-nav-pills .nav-item{
            flex:1;
        }
         .custom-nav-pills .nav-link.active, .custom-nav-pills .show >.nav-link {
            border-top: 4px solid!important;
            font-weight: 500;
            background-color: #f7f7f7;
            border-color: transparent transparent #f39c12;
            color: #f39c12;
            border-radius: inherit;
        }

        .custom-nav-pills .nav-link {
            display: block;
            text-align: center;
            border-top: 4px solid #e6e6e6;
            padding:1em 0px 1em 0px;
            font-size:18px;
        }

        h1 {
            color: #343433;            
            text-align: center;
            font-size: 31px;
        }

        .send-form-box {
            background: rgb(230, 230, 230); 
            text-align: left; 
            padding: 20px 110px;
            margin-top: 24px;
        }

        .send-row {
            display: grid;
            grid-template-columns: auto 1fr auto;
            grid-gap: 15px;
            align-items: center;
        }

        .store-icon {
            display: grid;
            grid-template-columns: 1fr 1fr;
            grid-gap: 15px;
            align-items: center;
        }

        .stores {
            padding: 0 267px;    
        }

        .embed-container { 
            position: relative; 
            padding-bottom: 56.25%; 
            height: 0; 
            overflow: hidden; 
            max-width: 100%; 
        } 
        
        .embed-container iframe, 
        .embed-container object, 
        .embed-container embed,
        .embed-container-resources iframe,
        .embed-container-resources object,
        .embed-container-resources embed { 
            position: absolute; 
            top: 0; 
            left: 0; 
            width: 100%; 
            height: 100%; 
        }
        
        .embed-container-resources {
            position: relative;
            padding-bottom: 43.25%;
            height: 0;
            overflow: hidden;
            max-width: 70%;
            margin: 0 auto;
            text-align: center;
        }
        
        .agree-text {
            font-size: 12px;
            padding: 20px 125px;
        }
        
        .search-box {
            width: 357px;
        }
        
        .title-space {
            padding-top: 10px;
        }
        
        .benefits {
            padding-top: 77px;
            padding-bottom: 30px;
        }
        
        .reach-box {
            background: url(/site/images/icon/reach-new-customers.png);
            height: 246px;
            width: 356px;
        }
        
        .directly-box {
            background: url(/site/images/icon/directly-engage-and-inspire.png);
            height: 236px;
            width: 356px;
        }
        
        .instantly-box {
            background: url(/site/images/icon/instantly-notify-customers.png);
            height: 236px;
            width: 356px;
        }
        
        .tag-box {
            background: url(/site/images/icon/tag-share-and-promote.png);
            height: 236px;
            width: 356px;
        }
        
        .receive-box {
            background: url(/site/images/icon/receive-real%20time-quotes.png);
            height: 236px;
            width: 356px;
        }
        
        .message-box {
            background: url(/site/images/icon/message-and-engage-clients.png);
            height: 236px;
            width: 356px;
        }
        
        .clients-box {
            background: url(/site/images/icon/designa-studio-allows.png);
            height: 236px;
            width: 356px;
        }
        
        .reach-box p,
        .directly-box p,
        .instantly-box p, 
        .tag-box p,
        .receive-box p,
        .message-box p,
        .clients-box p {
            position: relative;
            top: 50%;
            text-align: center;
            left: 0;
            right: 0;
            padding-left: 50px;
            padding-right: 50px;
        }
        
        .benefits-space {
            padding-top: 40px;
        }
        
        .suppliers img {
            width: 200px;
        }
        
        .btn-supplier {
            width: 300px;
        }
        
        #inquiry-box {
            position: fixed;
            bottom: 40px;
            right: 40px;
            z-index: 9999;
            width: 450px;
        }
        
        .need-box {
            background: #fff;
            /*padding: 57px 67px;*/
            padding: 43px 73px;
            margin-top: 57px;
            text-align: center;
        }
        
        .page-header-img h1 {
            display: none;
        }
        
        .advertising-text {
            /*font-size: 18px;
            padding: 30px;
            border: 1px solid #e6e6e6;*/
            color: #f39c12;
            font-size: 36px;
            font-weight: bold;
            font-family: 'Poppins', sans-serif;
        }
        
        .webform-box {
            padding-bottom: 16px;
            padding-left: 20%;
            padding-right: 20%;
        }
        
        .countdown li {
          display: inline-block;
          font-size: 1.5em;
          list-style-type: none;
          padding: 1em;
          text-transform: uppercase;
        }
        
        .countdown li span {
          display: block;
          font-size: 4.5rem;
        }
        
        .launching {
          text-align: center;
          padding-bottom: 26px;
          color: rgb(243, 156, 18);
          font-size: 28px;
        }
        
        .countdown-box {
            padding: 30px 0 0 0;
            text-align: center;
        }
        
        .webinar-list {
             width: 19%; 
             margin: auto; 
             text-align: left;
             padding-bottom: 20px;
        }
        
        /* Portrait */
        @media only screen 
          and (min-device-width: 375px) 
          and (max-device-width: 667px) 
          and (-webkit-min-device-pixel-ratio: 2)
          and (orientation: portrait) { 
              .custom-nav-pills {
                display: block !important;
                padding-bottom: 22px !important;
              }
              
              .form-space {
                 padding-bottom: 10px;
              }
              
              .send-row {
                 display: block !important;
              }
              
              .send-form-box {
                padding: 20px 42px !important;
                text-align: center !important;
              }
              
              .agree-text {
                padding: 13px 0px 0px 0px !important;
                text-align: center !important;
              }
              
              .stores {
                padding: 0 60px !important;
              }
              
              .search-box {
                width: 205px !important;
              }
              
              .title-space {
                padding-bottom: 10px;
             }
             
             .suppliers img {
                 float: none !important;
             }
             
             .btn-supplier {
                width: 257px !important;
                margin-bottom: 14px !important;
                float: none !important;
            }
            
            .header-search,
            .header-links,
            .sections,
            .links {
                display: none !important;
            }
            
            #inquiry-box {
                width: auto !important;
            }
            
            .need-box {
                padding: 57px 37px !important;
            }
            
            .embed-container-resources {
                padding-bottom: 53% !important;
                max-width: 100% !important;
            }
            
            .logo {
                height: auto !important;
            }
            
            .launching {
                padding-left: 20px;
                padding-right: 20px;
            }
            
            .countdown-box {
                text-align: left !important;
            }
            
            .webinar-list {
                width: 68% !important;
                padding-bottom: 16px !important;
            }
            
            .webform-box {
                padding-left: 0 !important;
                padding-right: 0 !important;
            }
    </style>
    
    
@endpush