@extends('site.includes.layouts.full')

@section('page-title', 'Designa for Suppliers')

@section('page-meta')
    <meta name="description" content="Some suppliers that have already joined the Designa Studio Community">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Designa for Suppliers">
    <meta property="og:description" content="Some suppliers that have already joined the Designa Studio Community">
    <meta property="og:image" content="{{ url('/site/images/support.png') }}">
    <meta property="og:url" content="{{ url('/for-supplier') }}">
@endsection

@section('content')
    <div class="page-header-img">
        <h1 class="text-orange text-center">Designa for Supplier</h1>
        <img class="bg-image" src="/site/images/support.png" alt="">
        {{-- <img class="bottom-pattern" src="/site/images/bottom-pattern.png" alt=""> --}}
    </div>
    
    <div style="background-color:#e6e6e6">
        <div class="container">
            <ul class="nav nav-pills justify-content-center mb-3 custom-nav-pills" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link " id="pills-supplier-tab" href="{{ route('site.for-supplier', 'supplier-hub') }}" role="tab" aria-controls="pills-supplier" aria-selected="true">Supplier Hub</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-mobile-tab"  href="{{ route('site.for-supplier', 'mobile-app') }}" role="tab" aria-controls="pills-mobile" aria-selected="true">Mobile App</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" id="pills-resources-tab" href="{{ route('site.for-supplier', 'resources') }}" role="tab" aria-controls="pills-resources" aria-selected="false">Resources</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-advertising-tab"  href="{{ route('site.for-supplier', 'advertising-on-designa') }}" role="tab" aria-controls="pills-advertising" aria-selected="false">Advertising on Designa</a>
                </li>                  
                </ul>
        </div>
    </div>

    <div class="container">
            <div class="tab-pane active " id="webinar" role="tabpanel" aria-labelledby="pills-resources-tab" style="padding-top: 20px;">
                
                <!-- second tab -->
<nav>
  <div class="nav nav-tabs" id="nav-tab" role="tablist">
    <a class="nav-item nav-link active" id="nav-webinar-tab" data-toggle="tab" href="#nav-webinar" role="tab" aria-controls="nav-webinar" aria-selected="true">Webinar Registration</a>
    <a class="nav-item nav-link" id="nav-videos-tab" data-toggle="tab" href="#nav-videos" role="tab" aria-controls="nav-videos" aria-selected="false">User Videos</a>
    <a class="nav-item nav-link" id="nav-guidelines-tab" data-toggle="tab" href="#nav-guidelines" role="tab" aria-controls="nav-guidelines" aria-selected="false">Image Guidelines</a>
    <a class="nav-item nav-link" id="nav-tagging-tab" data-toggle="tab" href="#nav-tagging" role="tab" aria-controls="nav-tagging" aria-selected="false">Tagging Suppliers</a>
    <a class="nav-item nav-link" href="{{ route('site.blog.index') }}" role="tab" aria-controls="nav-blogs" aria-selected="false">Blogs and Articles</a>
    <a class="nav-item nav-link" id="nav-why-tab" data-toggle="tab" href="#nav-why" role="tab" aria-controls="nav-why" aria-selected="false">Why Designa</a> 
  </div>
</nav>
<div class="tab-content" id="nav-tabContent">

  <!-- Webinar Registration -->  
  <div class="tab-pane fade show active" id="nav-webinar" role="tabpanel" aria-labelledby="nav-webinar-tab" style="padding-top: 50px;">
        <div class="text-center pt-2">
            <h1>Webinar Registration</h1>
                <p>Register for a webinar to learn the basics of Designa to get you started:</p>
                <ul class="webinar-list">
                    <li>Create a profile</li>
                    <li>Load your products</li>
                    <li>Create Inspiration</li>
                </ul>
            <p>
                We'll also be sharing tips and tricks to optimise your profile and get the most out of Designa platform.
            </p>
        </div>
        <hr>
        <div class="webform-box">
           <webinar-registration-form></webinar-registration-form>
        </div>
  </div>

  <!-- User Videos -->
  <div class="tab-pane fade" id="nav-videos" role="tabpanel" aria-labelledby="nav-videos-tab" style="padding-top: 50px;">
    <div class="text-center mt-2 pt-2" id="videos">
        <h1>User Guide Videos</h1>
        <p>New to Designa? Here are our user videos to help you get started</p>
    </div>

    <hr>

    <div class="d-none">
        <div class="row" style="padding-bottom: 30px; padding-top: 10px">
            <div class="col-lg-6">
                <input placeholder="Search" autocomplete="off" class="form-control form-control form-input-sm search-box">
            </div>
            <div class="col-lg-6 text-right">
                <i class="fa fa-th-large" style="padding-right:  15px; color: #f39c12; font-size: 30px;"></i><i class="fa fa-list" style="font-size: 30px;"></i>
            </div> 
        </div>
    </div>

    <div class="row text-center">
        <div class="col-lg-4">
            <div class='embed-container'><iframe src='https://www.youtube.com/embed/Qp9wvJYp4-4' frameborder='0' allowfullscreen></iframe></div>
            <h5 class="title-space">Designa Mobile App</h5>
        </div>
        <div class="col-lg-4">
            <div class='embed-container'><iframe src='https://www.youtube.com/embed/A_4Ewv_-I8c' frameborder='0' allowfullscreen></iframe></div>
            <h5 class="title-space">How to Register as Supplier</h5>
        </div>
        <div class="col-lg-4">
            <div class='embed-container'><iframe src='https://www.youtube.com/embed/-NEjSzubR0A' frameborder='0' allowfullscreen></iframe></div>
            <h5 class="title-space">How to Register As Customer</h5>
        </div>
    </div>

    <div class="row text-center">
        <div class="col-lg-4">
            <div class='embed-container'><iframe src='https://www.youtube.com/embed/LA7v31cUwnU' frameborder='0' allowfullscreen></iframe></div>
            <h5 class="title-space">How to Filter Decor</h5>
        </div>
        <div class="col-lg-4">
            <div class='embed-container'><iframe src='https://www.youtube.com/embed/sIeR9KrW68Q' frameborder='0' allowfullscreen></iframe></div>
            <h5 class="title-space">How to Filter Inspiration</h5>
        </div>
        <div class="col-lg-4">
            <div class='embed-container'><iframe src='https://www.youtube.com/embed/FnWdAWTJqwM' frameborder='0' allowfullscreen></iframe></div>
            <h5 class="title-space">How to Filter Supplier</h5>
        </div>
    </div>

    <div class="row text-center">
        <div class="col-lg-4">
            <div class='embed-container'><iframe src='https://www.youtube.com/embed/tcFnU4d3ZOE' frameborder='0' allowfullscreen></iframe></div>
            <h5 class="title-space">How to Filter Venue</h5>
        </div>
        <div class="col-lg-4">
            <div class='embed-container'><iframe src='https://www.youtube.com/embed/NRp5-nfyA2Y' frameborder='0' allowfullscreen></iframe></div>
            <h5 class="title-space">How Dashboard Works</h5>
        </div>
        <div class="col-lg-4">
            <div class='embed-container'><iframe src='https://www.youtube.com/embed/duHTwYGcCm0' frameborder='0' allowfullscreen></iframe></div>
            <h5 class="title-space">Chat Function 1</h5>
        </div>
    </div>

    <div class="row text-center">
        <div class="col-lg-4">
            <div class='embed-container'><iframe src='https://www.youtube.com/embed/bBAtlOyb2qU' frameborder='0' allowfullscreen></iframe></div>
            <h5 class="title-space">Chat Function 2</h5>
        </div>
        <div class="col-lg-4">
            <div class='embed-container'><iframe src='https://www.youtube.com/embed/QQv4xc1CFgs' frameborder='0' allowfullscreen></iframe></div>
            <h5 class="title-space">How to Load Inspiration</h5>
        </div>
        <div class="col-lg-4">
            <div class='embed-container'><iframe src='https://www.youtube.com/embed/7K72cKKKusU' frameborder='0' allowfullscreen></iframe></div>
            <h5 class="title-space">Quotation Function</h5>
        </div>
    </div>
  </div>

  <!-- Image Guidelines -->  
  <div class="tab-pane fade" id="nav-guidelines" role="tabpanel" aria-labelledby="nav-guidelines-tab" style="padding-top: 50px; color: #58585a;">

    <div class="row">
        <div class="col-lg-6">
            <img src="https://designa.studio/site/images/logo.svg" class="img-fluid logo-header">
        </div>
        <div class="col-lg-6">
            <div class="guidelines-box">
                <h1>
                    Guidelines for <br>
                    Loading Inspiration<br>
                    and Products
                </h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <p class="guidelines-margin">
                Designa aims to ensure images shared are high quality and showcase the best our registered users have to offer. We understand showcasing your events and products can take many forms, which is why we encourage our users to be creative. We do have a number of guidelines to help users in curating their content. We also actively review all items loaded and will from time to time provide feedback to users that do not meet our guidelines.
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="brush-bg">
                <h4>Product Guidelines</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="guidelines-margin-intro">
                <h5>Intent of Inspiration:</h5>
                <hr class="hr-orange">
                <div class="row">
                    <div class="col-lg-6">
                        <ul class="ulguidelines">
                            <li>
                                Inspiration is a marketing tool. Treat it like a retail shopfront window for your business.
                            </li>
                            <li>
                                Show your best work only, not just sharing content to maximise visibility.                                
                            </li>
                            <li>Quality over quantity.</li>
                            <li>The platform is optimised to show the best content to users. Poor content will be removed from public view or demoted in listings.</li>
                            <li>Aim for between 3-5 images per inspiration.</li>
                            <li>All images should be high quality and include a mix of theme, decor, styling and mood.</li>
                        </ul>
                    </div>
                    <div class="col-lg-6">
                        <img src="/site/images/intent-inspiration-img.png" class="img-fluid guidelines-img">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="guidelines-margin2">
                <h5>Image Criteria:</h5>
                <hr class="hr-orange">
                <ul class="ulguidelines">
                    <li>
                        Name of event should be descriptive and theme oriented.
                    </li>
                    <li>
                        Do not use names of people in the events.                               
                    </li>
                    <li>
                        Images in a single inspiration post need to be related, i.e. the same event.
                    </li>
                </ul>                    
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="guidelines-margin2">
                <h5>Showcasing events - recommendation for image gallery:</h5>
                <hr class="hr-orange">                
                <ul class="ulguidelines">
                    <li>
                        Images showcase a design focal point.
                    </li>
                    <li>
                        Images convey a vibe and mood.
                    </li>
                    <li>
                        Interactive or functional.
                    </li>
                    <li>
                        Images match the themes and text descriptors.
                    </li>
                    <li>
                        Thumbnail image must be in an event setting.
                    </li>
                </ul>                    
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="brush-bg">
                <h4>Product Guidelines</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="guidelines-margin-intro">
                <h5>Loading Products:</h5>
                <hr class="hr-orange">                
                <ul class="ulguidelines">
                    <li>
                        Product images must be professionally shot.
                    </li>
                    <li>
                        Focused on the product being displayed.
                    </li>
                    <li>
                        Include images which showcase the product in an event setting.
                    </li>
                    <li>
                        Multiple images per product is encouraged.
                    </li>
                    <li>
                        Product description must watch the images and vice versa.
                    </li>
                </ul>                    
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="guidelines-margin2">
                <h5>Filter optimisation:</h5>
                <hr class="hr-orange">                
                <ul class="ulguidelines">
                    <li>
                        Images and products are optmised for users based on specific criteria;
                        <ul style="list-style: none;">
                            <li>
                                Rating - in house rating of image quality.
                            </li>
                            <li>
                                Views and recognition (favourites).
                            </li>
                            <li>
                                Reviews.
                            </li>
                            <li>
                                Geographic proximity to users.
                            </li>
                        </ul>
                    </li>                        
                </ul>                    
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="guidelines-margin2">
                <h5>Language:</h5>
                <hr class="hr-orange">                
                <ul class="ulguidelines">
                    <li>
                        Use of capital letters in title and as appropriate.
                    </li>
                    <li>
                        No inappropriate language.
                    </li>
                    <li>
                        Minimal and appropriate use of emojis
                    </li>                    
                </ul>                    
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="guidelines-margin2">
                <h5>Image Guidelines:</h5>
                <hr class="hr-orange">
                <div class="row">
                    <div class="col-lg-6">
                        <ul class="ulguidelines">
                            <li>
                                Images should be high quality and clean (i.e. with no attached Instagram filters.)
                            </li>
                            <li>
                                Images should be professionally shot or high quality.                                
                            </li>
                            <li>
                                Images must be high-resolution and un-watermarked
                            </li>
                            <li>
                                Name, website, and social media handle of photographer must be provided.
                            </li>
                            <li>
                                Images should not be excessively bright or monochrome.
                            </li>
                            <li>
                                Whenever possible, images should show up close details of the item posted.
                            </li>
                            <li>
                                Images should be inspirational and natural, but not excessively modern or minimal.
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-6">
                        <img src="/site/images/loading-products-img.png" class="img-fluid guidelines-img">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 guidelines-margin-intro">
            <p>
                In some instances, images will be unpublished and return to draft status. Users will receive an email notification in these instances and are invited to review and adjust the images before republishing.
            </p>
            <p>
                When an image is unpublished, users will receive an email detailing why the image is unpublished. Some of the reasons that images might be unpublished include;
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="guidelines-margin2">                
                <ul class="ulguidelines">
                    <li>
                        Poor image quality.
                    </li>
                    <li>
                        Incorrect image size.
                    </li>
                    <li>
                        Inappropriate image.
                    </li>                   
                    <li>
                        Image ownership dispute.
                    </li> 
                    <li>
                        Image does not clearly represent the showcased product or service.
                    </li>
                    <li>
                        Poor resolution image, banner or logo.
                    </li>
                    <li>
                        Misleading or inaccurate information displayed.
                    </li>
                </ul>                    
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <p class="guidelines-margin-intro">
                We’re always here to help and can provide tips and suggestions on how to improve image presentation. You can contact us at anytime via <a href="mailto:hello@designa.studio">hello@designa.studio</a>
            </p>            
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="guidelines-margin2">
                <h5>Recommended image dimensions:</h5>
                <hr class="hr-orange">                
                <ul style="list-style: none; padding-left: 0;">
                    <li>
                        Recommended photo quality -  1080 x 1080 px
                    </li>
                    <li>
                        Carousel 1080 x 1080 px
                    </li>
                    <li>
                        Banner Image 728 x 90 px Medium Rectangle (MREC) 300 x 250 px 
                    </li>
                    <li>
                        Logo 180 x 90 px
                    </li>
                    <li>
                        Thumbnail/Feature image 926 x 260 px
                    </li>
                    <li>
                        Gallery (20 images max.) 420 x 315 px
                    </li>
                    <li>
                        Preferred file formats: jPEG, PNG
                    </li>
                </ul>                    
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="brush-bg">
                <h4>Community Guidelines</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 guidelines-margin-intro">
            <p>
                We want to ensure the platform is enjoyed by event suppliers and professionals and those looking to be inspired or to plan their next event. Some basic guidelines are outlined here, with further terms available on the platform for those who need the fine print.
            </p>
            <hr class="hr-orange">
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="guidelines-margin2">                
                <div class="row">
                    <div class="col-lg-6">
                        <h6>Avoid using the Designa brand in a way that:</h6>
                        <ul class="ulguidelines">
                            <li>
                                Misrepresents the Designa platform.
                            </li>
                            <li>
                                Is not compliant with our terms of use.
                            </li>                
                        </ul>

                        <h6>Avoid sharing content that is:</h6>
                        <ul class="ulguidelines">
                            <li>
                                Not in the spirit of our community objectives and beliefs.
                            </li>
                            <li>
                                May be hurtful or offensive.
                            </li>                
                        </ul>
                        <div class="row">
                            <div class="col-lg-12"><h6>Content permissions:</h6>
                                <ul class="ulguidelines">
                                    <li>
                                        Ensure you have the right to share content you choose to publish.
                                    </li>
                                    <li>
                                        Follow the law. Do not post illegal images or nudity.
                                    </li>
                                    <li>
                                        Do not spam or engage platform users in a way that is not welcomed.
                                    </li>                
                                    <li>
                                        Focus on meaningful interactions and not spam.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <img src="https://designa.studio/site/images/logo.svg" class="img-fluid logo-content">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 pcheckout">
            <p>
                Check out our knowledge articles for more information as well as a list of do’s and don’ts for the Designa platform.
            </p>            
        </div>
    </div>
  </div>

  <!-- Tagging Suppliers -->  
  <div class="tab-pane fade" id="nav-tagging" role="tabpanel" aria-labelledby="nav-tagging-tab" style="padding-top: 50px; color: #58585a;">
        
      <div class="row">
        <div class="col-lg-6">
            <img src="https://designa.studio/site/images/logo.svg" class="img-fluid logo-header">
        </div>
        <div class="col-lg-6">
            <div class="guidelines-box">
                <h1>
                    The Importance of<br> Tagging
                </h1>
            </div>
        </div>
    </div>

    <div class="row" style="padding-top: 50px;">
        <div class="col-lg-12">
            <div class="guidelines-margin-intro">
                <h3 class="text-orange">Why Tagging is Important</h3>               
                <div class="row">
                    <div class="col-lg-6">
                        <p class="tagging-creates">
                            Tagging creates a connection where your products and inspiration will be showcased to potential customers or collaborators when planning an event. It links products and inspiration, letting customers see your products in an event setting, saving the ones that inspire them the most.
                        </p>
                    </div>
                    <div class="col-lg-6">
                        <img src="/site/images/tagging-image01.jpg" class="img-fluid guidelines-img">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="guidelines-margin-intro">
                <h5>When you link and tag other suppliers and professionals:</h5>
                <hr class="hr-orange">
                <div class="row">
                    <div class="col-lg-6">
                        <ul class="ulguidelines">
                            <li>
                                It increases the chance that you will be found by potential clients.
                            </li>
                            <li>
                                Improves your post visibility and ranking.                                
                            </li>
                            <li>Allows visitors to easily see who was involved at an event.</li>
                            <li>Helps others find inspiration suited to their specific location or interests.</li>
                        </ul>
                    </div>
                    <div class="col-lg-6">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="brush-bg-tagging">
                <h4>How do I tag myself and others?</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="guidelines-margin">
                <p>Tagging is flexible, it can be done on your desktop computer or from your mobile device using the Designa Studio App. By tagging yourself and collaborators you are increasing the profile and visibility of your inspiration.</p>
<hr class="hr-orange">
            </div>                       
            <p>
                <img src="/site/images/tagging-image02.jpg" alt="" class="img-fluid tagging-img">    
            </p>
            <p class="text-center"><a href="https://youtu.be/rBxwgFyiT18" target="_blank" >Tagging using Desktop</a></p>
            <p>
                <img src="/site/images/tagging-image03.jpg" alt="" class="img-fluid tagging-img">    
            </p>
            <p class="tagging-app">
                <a href="https://youtu.be/XWqX7lZp1_s"  target="_blank" >
                    Tagging using the Mobile App        
                </a>
            </p>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="brush-bg-tagging">
                <h4>How do I accept tags from others?</h4>
            </div>
        </div>
    </div>

    <div class="row" style="padding-top: 50px;">
        <div class="col-lg-12">
            <div class="guidelines-margin-intro">                             
                <div class="row">
                    <div class="col-lg-6">
                        <p>
                            Once another supplier or professional has tagged you in an inspiration you will need accept the tag.  You will receive an email where you can simply click the link to accept the tag.  If you have logged in on your desktop or app you can also confirm the tag request.
                        </p>
                    </div>
                    <div class="col-lg-6">
                        <img src="/site/images/tagging-image04.jpg" class="img-fluid guidelines-img">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="brush-bg-tagging">
                <h4 class="width">How do I link products to <br>a tagged inspiration?</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="guidelines-margin">
                <p>Once you have confirmed a tag you can then add your products linked to that event.  Tagging products allow user to easily identify which products feature at the event.  You will need to have loaded your products in first before they can be linked to an event.</p>
<hr class="hr-orange">
            </div>                       
            <p>
                <img src="/site/images/tagging-image03.jpg" alt="" class="img-fluid tagging-img02">    
            </p> 
            <p class="text-center"><a href="https://youtu.be/6sGRa7JL5Og" target="_blank" >How to Link Products To A Tagged Inspiration</a></p>           
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="brush-bg-tagging">
                <h4>How do I request to be tagged?</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 guidelines-margin">
            <p>
                If you were involved in an event but have not been tagged you can request to be tagged. A request will be sent to the original poster of that inspiration notifying them of your request. Once accepted, you can then also link your specific products in that event, putting your products at the fingertips of potential customers.</p>
                <hr class="hr-orange">
            </p>                       
            <p>
                <img src="/site/images/tagging-image05.png" alt="" class="img-fluid tagging-img01">    
            </p>            
        <p>
            Of course, if you need any help with tagging or being tag, support is only a click away by emailing us at <a href="mailto:hello@designa.studio" class="text-orange">hello@designa.studio</a>.
        </p>
        <p>
            Happy Tagging,
        </p>
        <p>
            <img src="https://designa.studio/site/images/logo.svg" class="img-fluid tagging-logo">
        </p>        
    </div>
    </div>  

  </div>
  <!-- Tagging Suppliers --> 

  <!-- Blogs and Articles -->  
  <div class="tab-pane fade blog-articles-box" id="nav-blogs" role="tabpanel" aria-labelledby="nav-blogs-tab">
    
    <div class="row">
        <div class="col-lg-4"></div>
        <div class="col-lg-4">
            <img src="https://designa.studio/site/images/logo.svg" class="img-fluid blog-articles-logo">
        </div>
        <div class="col-lg-4">
            <input class="form-control pull-right blog-articles-search" type="text" placeholder="Search" aria-label="Search">
        </div>
    </div>

    <hr>    

    <section class="recent-articles">
        <div class="row">
            <div class="col-lg-12">

                <!-- carousel -->
                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                  <div class="carousel-inner">
                    <div class="carousel-item active">
                         <div class="row">
                            <div class="col-lg-8">
                                <img class="d-block img-fluid" src="/site/images/slider-blog-img01.jpg" alt="">
                            </div>

                            <div class="col-lg-4">
                                <div class="carousel-caption d-md-block">
                                    <p style="color: #f39c12;">Suppliers</p>
                                    <h1 style="text-align: left;">Directory Listings – Are they really worth it?</h1>
                                    <p>Business Directories have historically been a way for events businesses to reach their online consumers, but are they still relevant?</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="carousel-item">
                      <div class="row">
                            <div class="col-lg-8">
                                <img class="d-block img-fluid" src="/site/images/slider-blog-img02.png" alt="">
                            </div>

                            <div class="col-lg-4">
                                <div class="carousel-caption d-md-block">
                                    <p style="color: #f39c12;">Suppliers</p>
                                    <h1 style="text-align: left;">Planning Fatigue in the Events industry</h1>
                                    <p>Evolution of the events industry has seen changes in tastes and trends, but the ability to sell the dream and the ability to inspire customers with a transaction has been left behind.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>

                  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>

                </div>

                
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 recent-articles-title">
                <h5>Recent Articles</h5>
                <hr class="hr-orange">            
            </div>
        </div>

        <div class="row">        
            <div class="col-lg-4">            
                <div class="mr-2">                    
                    <img src="/site/images/recent-articles-image01.jpg" class="img-fluid">
                    <h6>Suppliers</h6>
                    <p>
                        Directory Listings – Are they really worth it?
                    </p>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="mr-2">                    
                    <img src="/site/images/recent-articles-image02.jpg" class="img-fluid">
                    <h6>Suppliers</h6>
                    <p>
                        Planning Fatigue in the Events industry - How to create an inspired event experience, before the event.
                    </p>
                </div>
            </div>

            <div class="col-lg-4">
                
            </div>
        </div>
        
        <!--
        <div class="pull-right">
            <a href="#">VIEW ALL</a>
        </div>-->

    </section>


    <section class="latest-post">        
        <div class="row">
            <div class="col-lg-12 latest-post-title">
                <h5>Latest Post</h5>
                <hr class="hr-orange"> 

                <div class="latest-post-box">
                    <img src="/site/images/designa-sketch-logo.png" class="img-fluid designa-sketch-logo">
                    <h5>designastudio.au</h5>
                    <p>
                        We're an online marketplace for the events industry.
                    </p>
                </div>

                <!-- SnapWidget -->
               
                 <iframe src="https://snapwidget.com/embed/876885" class="snapwidget-widget" allowtransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden;  width:100%; "></iframe> 

                <div class="text-center">
                    <a href="https://www.instagram.com/designastudio.au/" target="_blank">VIEW MORE POST</a>
                </div>

                <div class="get-newsletter">
                    <h1>GET OUR NEWSLETTER</h1>
                    {{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p> --}}
                    {{-- <div class="get-newsletter-box">
                        <input type="text" class="form-control get-email" placeholder="E-MAIL">
                        <button type="submit" class="form-control get-subscribe">SUBSCRIBE</button>
                    </div> --}}
                    <newsletter-email></newsletter-email>
                </div>
            </div>
        </div>
    </section>
  </div>


<!-- Why Designa -->  
  <div class="tab-pane fade" id="nav-why" role="tabpanel" aria-labelledby="nav-why-tab" style="padding-top: 50px; color: #58585a;">
    
    <div class="row">
        <div class="col-lg-6">
            <img src="https://designa.studio/site/images/logo.svg" class="img-fluid logo-header">
        </div>
        <div class="col-lg-6">
            <div class="guidelines-box">
                <h1>
                    Why Designa:
                </h1>
                <p>Create design you've always wanted</p>
            </div>
        </div>
    </div>

    <div class="guidelines-margin">
        <div class="row">
            <div class="col-lg-12">
                <h4>The Events industry is very inefficient</h4>
                <div class="why-gradient"></div>
                <p>
                    Currently, the process of planning an event is linear. Customers and suppliers follow an difficult and time consuming process of doing research, obtaining quotes, planning/styling and after all this, take a leap of faith that their ideas come through on the day. The detailed tasks which detract from event planning include;
                </p>
            </div>
        </div>

        <div class="event-planning-list">
            <div class="row icon-space">
                <div class="col-lg-2 icon-right">
                    <img src="/site/images/icon1.png" alt="" class="img-fluid">
                </div>
                <div class="col-lg-10">
                    <p>
                        A person needs to do all their own research (Instagram, Facebook, internet, supplier websites)
                    </p>
                </div>
            </div>

            <div class="row icon-space">
                <div class="col-lg-2 icon-right">
                    <img src="/site/images/icon2.png" alt="" class="img-fluid">
                </div>
                <div class="col-lg-10">
                    <p>
                        They need to find who was involved in that inspiration and whether they service the area in mind.
                    </p>
                </div>
            </div>

            <div class="row icon-space">
                <div class="col-lg-2 icon-right">
                    <img src="/site/images/icon3.png" alt="" class="img-fluid">
                </div>
                <div class="col-lg-10">
                    <p>
                        They need to see if the venue is available for their time and date and if the suppliers they like can be at that venue.
                    </p>
                </div>
            </div>

            <div class="row icon-space">
                <div class="col-lg-2 icon-right">
                    <img src="/site/images/icon4.png" alt="" class="img-fluid">
                </div>
                <div class="col-lg-10">
                    <p>
                        They need to individually contact each supplier to see if they’re available &amp; if product/service is available for the date.
                    </p>
                </div>
            </div>

            <div class="row icon-space">
                <div class="col-lg-2 icon-right">
                    <img src="/site/images/icon5.png" alt="" class="img-fluid">
                </div>
                <div class="col-lg-10">
                    <p>
                        Countless consultations are booked to view the product or come up with the room style.
                    </p>
                </div>
            </div>

            <div class="row icon-space">
                <div class="col-lg-2 icon-right">
                    <img src="/site/images/icon6.png" alt="" class="img-fluid">
                </div>
                <div class="col-lg-10">
                    <p>
                        There needs to be meetings with the venue, stylist, décor company over multiple channels of communication.
                    </p>
                </div>
            </div>

            <div class="row icon-space">
                <div class="col-lg-2 icon-right">
                    <img src="/site/images/icon7.png" alt="" class="img-fluid">
                </div>
                <div class="col-lg-10">
                    <p>
                        Liaising on emails, messages, social media chat to coordinate many quotes can be very overwhelming.
                    </p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <p>
                    All of this whilst taking a leap of faith on what the actual event will look like on the day.
                </p>
                <p>
                    After all that legwork, people need and deserve more assurance and comfort that they can be confident to spend the money and that their dream will be brought to life.
                </p>
            </div>
        </div>

        <div class="row" style="padding-top: 30px;">
            <div class="col-lg-12">
                <h4>What people have told us;</h4>
                <div class="why-gradient"></div>
                <div class="row">
                    <div class="col-lg-6">
                        <img src="/site/images/what-people-have-told-us.png" class="img-fluid" style="width: 280px; margin: 0 auto; display: block;">
                    </div>
                    <div class="col-lg-6">
                        <ul class="ulguidelines">
                            <li>
                                Upselling and conversion can be difficult when exploring new event concepts.
                            </li>
                            <li>
                                It’s hard to differentiate yourself in the market, especially when directories and current methods of finding suppliers are so generic.
                            </li>
                            <li>
                                It’s very expensive and time consuming to build an online profile and keep it up to date.
                            </li>
                            <li>
                                Communication is difficult, especially when there are so many different things to coordinate.
                            </li>
                            <li>
                                Professionals and individuals would love to spend more time on creating amazing events and less time with logistics.
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" style="padding-top: 30px;">
            <div class="col-lg-12">
                <h4>Were here to help;</h4>
                <div class="why-gradient"></div>
                <p>
                    Fast forward to Designa.
                </p>
                <p>
                    At their fingertips, people are getting inspiration, building ideas, seeing what venues can work with their requirements and concepts.
                </p>
                <p>
                    They have direct access to professionals who have not only worked at these venues but have done similar themes/styles/events and can easily show the work.
                </p>
                <p>
                    Customers and suppliers have one single channel for communication and planning an event.
                </p>
                <p>
                    Venues/suppliers/professionals can visually show what the end product will look like, which allows them to upsell with ease.
                </p>
                <p>
                    Professionals can easily build pitch packs, immersive imagery and show inspiration to help visualise ideas and give confidence for the event wow-factor.
                </p>
                <p>
                    There is no other platform that makes it so easy for everyone involved.
                </p>
                <p>
                    Get your dream team of suppliers, venue, event professionals without even leaving the application
                </p>
                <p style="font-weight: bold;">
                    Improving the communication and making the end result real for the customer
                </p>
                <p>
                    <img src="/site/images/how-are-people-finding-me.png" class="img-fluid" alt="">
                </p>
                <p>
                    Saving inspiration in a portfolio for the event and visualising it in the planning stages can be done without all the logistics or back and forth headaches.
                </p>
            </div>
        </div>

        <div class="row" style="padding-top: 30px;">
            <div class="col-lg-12">
                <h4>Our product - How you use it</h4>
                <div class="why-gradient"></div>
                <p>
                    Direct benefits of using Designa
                </p>
                <ul class="usechecklist">
                    <li>
                        <div>
                            <img src="/site/images/check-123.png">
                        </div>
                        <div>
                            Register as a supplier, venue or event professional to increase your visibility, making it easier for customers to find you.
                        </div>
                    </li>
                    <li>
                        <div>
                            <img src="/site/images/check-123.png">
                        </div>
                        <div>
                            Connect with customers on the platform, engage and provide quotes.
                        </div>
                    </li>
                    <li>
                        <div>
                            <img src="/site/images/check-123.png">
                        </div>
                        <div>
                            Showcase your products through inspiration.
                        </div>
                    </li>
                    <li>
                        <div>
                            <img src="/site/images/check-123.png">
                        </div>
                        <div>
                            Create pitch packs to show clients how their dream will come to life.
                        </div>
                    </li>
                    <li>
                        <div>
                            <img src="/site/images/check-123.png">
                        </div>
                        <div>
                            Tag suppliers and tag yourself where you feature in the inspiration shared by others.
                        </div>
                    </li>
                    <li>
                        <div>
                            <img src="/site/images/check-123.png">
                        </div>
                        <div>
                            Showcase the end product, the event, so a customer can gain comfort on your choices.
                        </div>
                    </li>
                    <li>
                        <div>
                            <img src="/site/images/check-123.png">
                        </div>
                        <div>
                            Load your products and maintain a digital catalogue.
                        </div>
                    </li>                    
                </ul>
                <p style="padding-top: 30px;">
                    Designa gives you a set of tools and network capabilities to improve, simplify and enrich the experience of organising an event for customers, suppliers, venues and event professionals. It creates a network of proof points and connections that make it much easier to articulate event concepts and sell new ideas.
                </p>
            </div>
        </div>

        <div class="row" style="padding-top: 30px;">
            <div class="col-lg-12">
                <h4>What's next - We won't stop there...</h4>
                <div class="why-gradient"></div>
                <p>
                    We have an extensive roadmap over the next 12 months that will deliver more, including exciting new features that have never been seen before in this industry. And they’ll be available at your fingertips.
                </p>
            </div>
        </div>

        <div class="row" style="padding-top: 30px;">
            <div class="col-lg-12">
                <h4 class="text-center">The Steps to becoming a Pro on Designa</h4>
                <img src="/site/images/steps-arrow.png" class="img-fluid" alt="">
                <p>
                    We have an extensive roadmap over the next 12 months that will deliver more, including exciting new features that have never been seen before in this industry. And they’ll be available at your fingertips.
                </p>
                <div class="row">
                    <div class="col-lg-3">
                        <div class="steps-box box-sm">
                            <p>
                                Creating a profile
                            </p>
                            <ul>
                                <li>
                                    Create your account
                                </li>
                                <li>
                                    Build an online presence
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="steps-box box-md">
                            <p>
                                Enhance your profile
                            </p>
                            <ul>
                                <li>
                                    Build a digital catalogue
                                </li>
                                <li>
                                    Create a digital portfolio of your work
                                </li>
                                <li>
                                    Tag suppliers or professionals to your post to maximise reach
                                </li>
                                <li>
                                    Create your pitch packs using Designa
                                </li>
                            </ul>
                        </div>                        
                    </div>
                    <div class="col-lg-3">
                        <div class="steps-box box-lg">
                            <p>
                                Standout from others
                            </p>
                            <ul>
                                <li>
                                    Use our platform to showcase your products
                                </li>
                                <li>
                                    If you are a venue make sure you event spaces are available so customers and professionals can showcase your space
                                </li>
                            </ul>
                        </div>                                                
                    </div>
                    <div class="col-lg-3">
                        <div class="steps-box box-xl">
                            <p>
                                Be a Pro
                            </p>
                            <ul>
                                <li>
                                    Promote your product or services with the Designa featured marketing
                                </li>
                                <li>
                                    Create an immersive showroom with your products and the ability to customise room styling
                                </li>
                                <li>
                                    If you are a Stylist or Event Planner, show clients how you transform a room using venue templates
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </div>

    
  </div>
  <!-- Why Designa -->

</div>
                
            </div>
            <br>
            
           

        </div>
    </div>


@endsection

@push('scripts')
     <script src="https://snapwidget.com/js/snapwidget.js"></script>
    <script>
       $(function() {
    $('.scroll-down').click (function() {
      $('html, body').animate({scrollTop: $('.ok').offset().top }, 'slow');
      return false;
    });
  });
    </script>
    
    <script>
        $(function () {
            var hash = window.location.hash;
            hash && $('ul.nav a[href="' + hash + '"]').tab('show');

            // $(document).ready(function(){
            //     window.open(window.location.href, '_self')
            // })

        });
    </script>


@endpush

@push('styles')
    <style>
    
        .need-box h4 {
            padding-top: 20px;
            padding-bottom: 10px;
        }
 
        .fa-briefcase, .fa-laptop {
            font-size: 47px;
        }
        
        .scroll-down {
            height: 50px;
    width: 50px;
    background-color: #f39c12;
    border-radius: 50%;
    display: inline-block;
    color: #fff;
    float: right;
    font-size: 29px;
        }
        
        .custom-nav-pills{
            display:flex;
        }

        .custom-nav-pills .nav-item{
            flex:1;
        }
         .custom-nav-pills .nav-link.active, .custom-nav-pills .show >.nav-link {
            border-top: 4px solid!important;
            font-weight: 500;
            background-color: #f7f7f7;
            border-color: transparent transparent #f39c12;
            color: #f39c12;
            border-radius: inherit;
        }

        .custom-nav-pills .nav-link {
            display: block;
            text-align: center;
            border-top: 4px solid #e6e6e6;
            padding:1em 0px 1em 0px;
            font-size:18px;
        }

        h1 {
            color: #343433;            
            text-align: center;
            font-size: 31px;
        }

        .send-form-box {
            background: rgb(230, 230, 230); 
            text-align: left; 
            padding: 20px 110px;
            margin-top: 24px;
        }

        .send-row {
            display: grid;
            grid-template-columns: auto 1fr auto;
            grid-gap: 15px;
            align-items: center;
        }

        .store-icon {
            display: grid;
            grid-template-columns: 1fr 1fr;
            grid-gap: 15px;
            align-items: center;
        }

        .stores {
            padding: 0 267px;    
        }

        .embed-container { 
            position: relative; 
            padding-bottom: 56.25%; 
            height: 0; 
            overflow: hidden; 
            max-width: 100%; 
        } 
        
        .embed-container iframe, 
        .embed-container object, 
        .embed-container embed,
        .embed-container-resources iframe,
        .embed-container-resources object,
        .embed-container-resources embed { 
            position: absolute; 
            top: 0; 
            left: 0; 
            width: 100%; 
            height: 100%; 
        }
        
        .embed-container-resources {
            position: relative;
            padding-bottom: 43.25%;
            height: 0;
            overflow: hidden;
            max-width: 70%;
            margin: 0 auto;
            text-align: center;
        }
        
        .agree-text {
            font-size: 12px;
            padding: 20px 125px;
        }
        
        .search-box {
            width: 357px;
        }
        
        .title-space {
            padding-top: 10px;
        }
        
        .benefits {
            padding-top: 77px;
            padding-bottom: 30px;
        }
        
        .reach-box {
            background: url(/site/images/icon/reach-new-customers.png);
            height: 246px;
            width: 356px;
        }
        
        .directly-box {
            background: url(/site/images/icon/directly-engage-and-inspire.png);
            height: 236px;
            width: 356px;
        }
        
        .instantly-box {
            background: url(/site/images/icon/instantly-notify-customers.png);
            height: 236px;
            width: 356px;
        }
        
        .tag-box {
            background: url(/site/images/icon/tag-share-and-promote.png);
            height: 236px;
            width: 356px;
        }
        
        .receive-box {
            background: url(/site/images/icon/receive-real%20time-quotes.png);
            height: 236px;
            width: 356px;
        }
        
        .message-box {
            background: url(/site/images/icon/message-and-engage-clients.png);
            height: 236px;
            width: 356px;
        }
        
        .clients-box {
            background: url(/site/images/icon/designa-studio-allows.png);
            height: 236px;
            width: 356px;
        }
        
        .reach-box p,
        .directly-box p,
        .instantly-box p, 
        .tag-box p,
        .receive-box p,
        .message-box p,
        .clients-box p {
            position: relative;
            top: 50%;
            text-align: center;
            left: 0;
            right: 0;
            padding-left: 50px;
            padding-right: 50px;
        }
        
        .benefits-space {
            padding-top: 40px;
        }
        
        .suppliers img {
            width: 200px;
        }
        
        .btn-supplier {
            width: 300px;
        }
        
        #inquiry-box {
            position: fixed;
            bottom: 40px;
            right: 40px;
            z-index: 9999;
            width: 450px;
        }
        
        .need-box {
            background: #fff;
            /*padding: 57px 67px;*/
            padding: 43px 73px;
            margin-top: 57px;
            text-align: center;
        }
        
        .page-header-img h1 {
            display: none;
        }
        
        .advertising-text {
            /*font-size: 18px;
            padding: 30px;
            border: 1px solid #e6e6e6;*/
            color: #f39c12;
            font-size: 36px;
            font-weight: bold;
            font-family: 'Poppins', sans-serif;
        }
        
        .webform-box {
            padding-bottom: 16px;
            padding-left: 20%;
            padding-right: 20%;
        }
        
        .countdown li {
          display: inline-block;
          font-size: 1.5em;
          list-style-type: none;
          padding: 1em;
          text-transform: uppercase;
        }
        
        .countdown li span {
          display: block;
          font-size: 4.5rem;
        }
        
        .launching {
          text-align: center;
          padding-bottom: 26px;
          color: rgb(243, 156, 18);
          font-size: 28px;
        }
        
        .countdown-box {
            padding: 30px 0 0 0;
            text-align: center;
        }
        
        .webinar-list {
             width: 19%; 
             margin: auto; 
             text-align: left;
             padding-bottom: 20px;
        }

        
        /* For Image Guidelines */
        .logo-header {
            width: 200px; 
            margin: 71px auto 0px auto; 
            display: block;
        }

        .logo-content {
            width: 200px; 
            margin: 109px auto 0px auto; 
            display: block;
        }

        ul.ulguidelines {
            list-style: none; /* Remove default bullets */
        }

        ul.ulguidelines li::before {
          content: "\2022";  /* Add content: \2022 is the CSS Code/unicode for a bullet */
          color: rgb(247, 148, 30); /* Change the color */
          font-weight: bold; /* If you want it to be bold */
          display: inline-block; /* Needed to add space between the bullet and the text */
          width: 20px; /* Also needed for space (tweak if needed) */
          margin-left: -23px; /* Also needed for space (tweak if needed) */
          font-size: 22px;
          line-height: 20px;
        }

        .guidelines-box {
            background: #f7941e;            
            padding-bottom: 40px;
            padding-top: 40px;
            color: #fff;
            text-align: center;
        }

        .guidelines-box h1 {
            text-align: left;                       
            font-size: 38px;
            color: #fff;
            text-align: center;
        }

        .guidelines-margin {
            padding: 50px 100px 25px 100px;
        }

        .brush-bg {
            background: url(/site/images/brush-bg.png); 
            height: 78px; 
            background-size: contain; 
            background-repeat: no-repeat; 
            background-position: left center;
            margin-top: 20px;
        }

        .brush-bg h4 {
            padding-top: 26px; 
            padding-left: 87px; 
            font-size: 24px; 
            color: #fff;
        }

        .guidelines-margin-intro {
           padding: 20px 100px 10px 100px;
        }

        .guidelines-margin2 {
            padding: 10px 100px;
        }

        .guidelines-margin2 h6 {
            font-weight: bold;
            padding-top: 10px;
        }

        .hr-orange {
            margin-top: 0; 
            border-top: 1px solid #ffd19b;
        }

        .guidelines-img {
            height: 233px;
            margin: 14px auto 0 auto;
            display: block;
        }

        .pcheckout {
            padding: 0px 100px 10px 100px;
        }

        
        /* For Blogs and Articles */
        .recent-articles h6 {
            font-weight: bold;
            color: #f39c12;
            padding-top: 20px;
        }

        .recent-articles h5,
        .latest-post h5  {
            font-weight: bold;
        }

        .latest-post iframe {
            height: 583px !important
        }


        .blog-articles-box {
            padding-top: 50px; 
            padding-bottom: 50px; 
            color: #58585a;
        }

        .blog-articles-logo {
        text-align: center; 
        width: 164px;
        }

        .blog-articles-search {
        width: 230px;
        }

        .recent-articles {
        padding-bottom: 50px;
        }

        .recent-articles-title {
            padding-top: 60px;
        }

        .latest-post-title {
            padding-top: 40px;
        }

        .latest-post-box {
        padding-top: 20px; 
        text-align: center;
        }

        .designa-sketch-logo {
        width: 146px; 
        margin: 0 auto; 
        display: block; 
        padding-bottom: 20px;
        }

        .get-newsletter {
        padding-top: 60px;
        text-align: center;
        padding-left: 300px;
        padding-right: 300px;
        }

        .get-newsletter h1 {
        color: #f39c12;
        font-weight: bold;
        }

        .get-newsletter-box {
        padding-left: 70px;
        padding-right: 70px; 
        padding-top: 10px;
        }

        .get-email {
        width: 247px; 
        float: left; 
        border: none;
        background: #f0f0f0;
        }

        .get-subscribe {
        width: 123px; border: none;
        background: #f7941e;
        color: #fff;
        border-radius: initial;
        }


        /* Carousel */
        .carousel-control-prev {
            left: 70% !important;
        }

        .carousel-control-next {
            right: 26% !important;
        }

        .carousel-control-next, .carousel-control-prev {
            top: 356px !important;
            width: 20px !important;
            z-index: 100 !important;
        }

        .carousel-control-prev-icon,
        .carousel-control-next-icon {
            filter:
        /* for demonstration purposes; originals not entirely black */
        contrast(1000%)
        /* black to white */
        invert(100%)
        /* white to off-white */
        sepia(100%)
        /* off-white to yellow */
        saturate(10000%)
        /* do whatever you want with yellow */
        hue-rotate(90deg);
        }

        #carouselExampleControls {
            -webkit-box-shadow: -1px 1px 28px 3px rgba(0,0,0,0.08);
            -moz-box-shadow: -1px 1px 28px 3px rgba(0,0,0,0.08);
            box-shadow: -1px 1px 28px 3px rgba(0,0,0,0.08);
        }

        .carousel-caption {
            top: 16px !important; 
            text-align: left !important; 
            left: 5% !important; 
            color: #000 !important;
        }

         /* Individual Blog */
        .individual-box {
            -webkit-box-shadow: -1px 1px 28px 3px rgba(0,0,0,0.08);
            -moz-box-shadow: -1px 1px 28px 3px rgba(0,0,0,0.08);
            box-shadow: -1px 1px 28px 3px rgba(0,0,0,0.08);
            padding: 20px;
        }

        /* Why Designa */
        .event-planning-list {
            padding-top: 30px;  
        }

        .event-planning-list img {
            width: 57px;            
        }

        .icon-right {
            text-align: right;
        }

        .event-planning-list p {
            padding-top: 14px;
        }

        .icon-space {
            padding-bottom: 30px;
        }

        .steps-box {
            background: #f79624;
            color: #fff;
            padding: 15px;
        }

        .steps-box ul {
            padding-left: 17px;
            list-style: none;
            font-size: 14px;
            line-height: 25px;
        }

        .steps-box ul li::before {
          content: "\2022";  /* Add content: \2022 is the CSS Code/unicode for a bullet */
          color: #fff; /* Change the color */
          font-weight: bold; /* If you want it to be bold */
          display: inline-block; /* Needed to add space between the bullet and the text */
          width: 12px; /* Also needed for space (tweak if needed) */
          margin-left: -17px; /* Also needed for space (tweak if needed) */
          font-size: 17px;
          line-height: 20px;
        }

        .steps-box p {
            margin-bottom: 0;
        }

        .box-sm {
            margin-top: 300px
        }

        .box-md {
            margin-top: 150px;   
        }

        .box-lg {
            margin-top: 74px;
            padding-bottom: 91px;
        }

        .why-gradient {
            background: rgb(247,150,37);
            background: linear-gradient(90deg, rgba(247,150,37,1) 0%, rgba(255,255,255,1) 100%); 
            height: 5px; 
            width: 106px; 
            margin-bottom: 20px;
        }

        ul.usedesigna {
            list-style: none
        }

        
        ul.usedesigna li:before {    
            content: "\2713";
            color: white;
            font-weight: bold;
            display: inline-block;    
            margin-left: -39px;
            font-size: 28px;
            line-height: 24px;
            background: orange;
            border-radius: 50%;
            width: 50px;
            height: 50px;
            text-align: center;
            padding-top: 15px;
            margin-right: 18px;
            float: left;
        }

        .usechecklist {
            display: grid; 
            grid-template-columns: 1fr 1fr; 
            grid-gap: 20px;
        }

        .usechecklist > li {
             display: grid; 
             grid-template-columns: auto 1fr; 
             grid-gap: 15px;
        }

        .usechecklist img {
            width: 54px;
        }


        /* Tagging Suppliers */
        .brush-bg-tagging {
            background: url(/site/images/brush-bg.png); 
            height: 86px; 
            background-size: contain; 
            background-repeat: no-repeat; 
            background-position: left center;
            margin-top: 20px;
        }

        .brush-bg-tagging h4 {
            padding-top: 14px;
            padding-left: 99px;
            font-size: 24px;
            color: #fff;
            width: 302px;
        }

        h4.width {
            padding-top: 14px;
            padding-left: 99px;
            font-size: 24px;
            color: #fff;
            width: auto;            
        }

        .tagging-creates {
            padding-top: 49px;
        }

        .tagging-img {
            margin: 0 auto;
            display: block;
            width: 590px;
            padding-top: 10px;
        }

        .tagging-app {
            text-align: center; 
            padding-bottom: 30px
        }

        .tagging-img01 {
            margin: 0 auto;
            display: block;
            width: 590px;
            padding-top: 10px; 
            padding-bottom: 30px;
        }

        .tagging-img02 {
            margin: 0 auto;
            display: block;
            width: 590px;
            padding-top: 10px;             
        }

        .tagging-logo {
            width: 200px; 
            margin-left: -14px;
        }


        
        @media (max-width: 939px) {
            .usechecklist {
                grid-template-columns: 1fr; 
                padding-left: 0;
            }
        }

        @media (min-width: 1561px) {
            .carousel .carousel-item {
                height: auto !important;
            }
        }


        @media (max-width: 480px) and (min-width: 320px) {
            .carousel .carousel-item {
                height: 600px;
            }
        }        


        
        /* Portrait */
        @media only screen 
          and (min-device-width: 375px) 
          and (max-device-width: 667px) 
          and (-webkit-min-device-pixel-ratio: 2)
          and (orientation: portrait) { 
              .custom-nav-pills {
                display: block !important;
                padding-bottom: 22px !important;
              }
              
              .form-space {
                 padding-bottom: 10px;
              }
              
              .send-row {
                 display: block !important;
              }
              
              .send-form-box {
                padding: 20px 42px !important;
                text-align: center !important;
              }
              
              .agree-text {
                padding: 13px 0px 0px 0px !important;
                text-align: center !important;
              }
              
              .stores {
                padding: 0 60px !important;
              }
              
              .search-box {
                width: 205px !important;
              }
              
              .title-space {
                padding-bottom: 10px;
             }
             
             .suppliers img {
                 float: none !important;
             }
             
             .btn-supplier {
                width: 257px !important;
                margin-bottom: 14px !important;
                float: none !important;
            }
            
            .header-search,
            .header-links,
            .sections,
            .links {
                display: none !important;
            }
            
            #inquiry-box {
                width: auto !important;
            }
            
            .need-box {
                padding: 57px 37px !important;
            }
            
            .embed-container-resources {
                padding-bottom: 53% !important;
                max-width: 100% !important;
            }
            
            .logo {
                height: auto !important;
            }
            
            .launching {
                padding-left: 20px;
                padding-right: 20px;
            }
            
            .countdown-box {
                text-align: left !important;
            }
            
            .webinar-list {
                width: 68% !important;
                padding-bottom: 16px !important;
            }
            
            .webform-box {
                padding-left: 0 !important;
                padding-right: 0 !important;
            }

            /* For guidelines */
            .logo-header {
                margin: 0px auto 44px auto !important;
            }

            .guidelines-margin {
                padding: 25px 40px 0px 40px !important;
            }

            .guidelines-margin-intro {
                padding: 20px 40px 10px 40px !important;
            }

            .guidelines-margin2 {
                padding: 10px ;
            } 

            .guidelines-img {
                margin: 14px auto 17px auto !important;
            }

            .logo-content {
                margin: 30px auto !important;
            }

            .pcheckout {
                padding: 0px 40px 10px 40px !important;
            }

            .nav-tabs {
                display: block !important;
            }

            /* For Get Newsletter */
            .get-newsletter,
            .get-newsletter-box {
                padding-left: 0 !important;
                padding-right: 0 !important;
            }

            .blog-articles-search {
                width: 170px !important;
            }

            .blog-articles-logo {
                margin: 0 auto !important;
                display: block !important;
                padding-bottom: 28px !important;
            }

            .latest-post iframe {
                height: 201px !important;
            }

            .get-email {
                width: 219px !important;
            }

            /* Carouse */
            .carousel-caption {
                top: 0 !important;
                left: 14% !important
            }

            .carousel-control-next, .carousel-control-prev {
                top: 496px !important;
            }

            .carousel-control-prev {
                left: 40px !important;
            }

            .carousel-control-next {
                    right: 75% !important;
            }

            .latest-post-title {
                padding-top: 0 !important;
            }

            /* why designa */
            .icon-right,
            .check-center {
                text-align: center !important;
            }

            .box-sm, .box-md, .box-lg, .box-xl {
                margin-top: 20px !important;
            }

            .box-lg {
                padding-bottom: 9px !important;
            }

            /* Tagging Suppliers */
            .brush-bg-tagging h4 {
                padding-left: 41px !important;
            }

            .brush-bg-tagging {
                background-size: cover !important;
                background-position: -136px center !important;
            }

    </style>
    
    
@endpush