@extends('site.includes.layouts.full')

@section('page-title', 'Designa for Suppliers')

@section('page-meta')
    <meta name="description" content="Some suppliers that have already joined the Designa Studio Community">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Designa for Suppliers">
    <meta property="og:description" content="Some suppliers that have already joined the Designa Studio Community">
    <meta property="og:image" content="{{ url('/site/images/support.png') }}">
    <meta property="og:url" content="{{ url('/for-supplier') }}">
@endsection

@section('content')
    <div class="page-header-img">
        <h1 class="text-orange text-center">Designa for Supplier</h1>
        <img class="bg-image" src="/site/images/support.png" alt="">
        {{-- <img class="bottom-pattern" src="/site/images/bottom-pattern.png" alt=""> --}}
    </div>
    
    <div style="background-color:#e6e6e6">
        <div class="container">
            <ul class="nav nav-pills justify-content-center mb-3 custom-nav-pills" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-supplier-tab" data-toggle="pill" href="#pills-supplier" role="tab" aria-controls="pills-supplier" aria-selected="true">Supplier Hub</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-mobile-tab" data-toggle="pill" href="#pills-mobile" role="tab" aria-controls="pills-mobile" aria-selected="true">Mobile App</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-resources-tab" data-toggle="pill" href="#pills-resources" role="tab" aria-controls="pills-resources" aria-selected="false">Resources</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-advertising-tab" data-toggle="pill" href="#pills-advertising" role="tab" aria-controls="pills-advertising" aria-selected="false">Advertising on Designa</a>
                </li>                  
                </ul>
        </div>
    </div>
    
    <div class="container countdown-box">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="launching">Designa Studio is officially launching to the public on the 1st July 2020. Are you ready?</h4>
                <ul class="countdown">
                    <li><span id="days"></span>days</li>
                    <li><span id="hours"></span>Hours</li>
                    <li><span id="minutes"></span>Minutes</li>
                    <li><span id="seconds" style="color: #f39c12;"></span>Seconds</li>
                </ul>
            </div>
        </div>    
    </div>
    
    <hr>

    <div class="container">
        <div class="tab-content" id="pills-tabContent" style="padding-top: 20px; padding-bottom: 60px; ">

            <div class="tab-pane fade show active" id="pills-supplier" role="tabpanel" aria-labelledby="pills-supplier-tab">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class='embed-container-resources'><iframe src='https://www.youtube.com/embed/v6dsLli56Eg' frameborder='0' allowfullscreen></iframe></div>
                    </div>
                </div>
                
                <div class="row text-center">
                    <div class="col-lg-12">
                        <h1 class="benefits">Why join Designa?</h1>
                    </div>
                </div>
                
                <div class="row text-center">
                    <div class="col-lg-6">
                        <div class="reach-box pull-right">
                            <p>Reach new customers who are ready to buy</p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="directly-box">
                            <p>Directly engage and inspire active customers using relevant content</p>
                        </div>
                    </div>
                </div>
                
                <div class="row text-center benefits-space">
                    <div class="col-lg-4">
                        <div class="instantly-box">
                            <p>Instantly notify customers about new products and services</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="tag-box">
                            <p>Tag, share and promote events you have done with industry colleagues</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="receive-box">
                            <p>Receive real time quotes with all the relevant information to transact with ease</p>
                        </div>
                    </div>
                </div>
                
                <div class="row text-center benefits-space">
                    <div class="col-lg-6">
                        <div class="message-box pull-right">
                            <p>Message and engage clients through notifications to maximise conversion opportunities</p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="clients-box">
                            <p>Designa Studio allows clients to browse inspiration & products and create their dream event all online!</p>
                        </div>
                    </div>
                </div>
                
                <div class="row benefits">
                   <div class="col-lg-12">
                       <h3 class="text-center">Some suppliers that have already joined the Designa Studio Community</h3>
                    </div>
               </div>
               
               <div class="row text-center suppliers">
                   <div class="col-lg-3">
                       <img src="https://inthistogether.sydney/assets/images/john-emmanuel-colour-logo.png" class="img-fluid">
                   </div>
                   <div class="col-lg-3">
                       <img src="https://inthistogether.sydney/assets/images/starlight-colour-logo.png" class="img-fluid">
                   </div>
                   <div class="col-lg-3">
                       <img src="https://inthistogether.sydney/assets/images/white-label-hire-colour-logo.png" class="img-fluid" style="padding-top: 35px;">
                   </div>
                   <div class="col-lg-3">
                       <img src="site/images/icon/John%20Alten%20logo%20Black.png" class="img-fluid" style="padding-top: 35px;">
                   </div>
               </div>
               
               <div class="row need-box" style="text-align: center;">
                   <div class="col-lg-6">
                       <i class="fas fa-laptop"></i>
                       <h4>Request a Demo</h4>
                       <p>
                           If you need a bit more help, we would love to arrange a one on one demo.  
                           Our Team are ready to show you the Designa Studio system and take you through 
                           all the functions that are at your disposal.
                        </p>
                        <div  style="margin-bottom:1    px;">&nbsp;</div>
                       <request-form type="demo"></request-form>
                   </div>
                   <div class="col-lg-6">
                       <i class="fas fa-briefcase"></i>
                       <h4>Request White Glove Service</h4>
                       <p>
                           Designa Studio has a team ready to go to set up your account and start you off with loading your products a inspiration.
                       </p>
                       <p>
                           To register your interest in the White Glove service simply provide your email address below and we will be in contact.
                       </p>
                        <div style="margin-bottom:14px">&nbsp;</div>
                       <request-white-glove-service></request-white-glove-service>
                   </div>
               </div>
                
            </div>            
                

            <div class="tab-pane fade" id="pills-mobile" role="tabpanel" aria-labelledby="pills-mobile-tab">
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-10">                    
                        <h1>Go Mobile with the Designa Studio App</h1>
                        <p class="text-center">
                            Find and create inspiration for your event through the Designa Studio app for mobile devices. 
                            This app connects you with inspiration and suppliers that can make your next event effortlessly breathtaking.
                        </p>
                        

                        <div class="send-form-box d-none">
                            <div class="row send-row">
                                <div class="form-space">
                                    Send a download link to your device
                                </div>
                                <div class="form-space">
                                    <input type="text" class="form-control" name="phonenumber" placeholder="Phone Number">
                                </div>
                                <div>
                                    <a href="#"><button type="button" class="btn" style="background-color: #ffc107;border-color: #ffc107;">Send</button></a>
                                </div>
                            </div>
                        </div> 

                        <p class="agree-text d-none">
                            I agree to receive a one-time text (sent using an auto dialer) from Designa Studio to my mobile device, with a link to download the Designa Studio mobile app. Message and data rates may apply.
                        </p>

                        <div class="stores">
                            <div class="row store-icon">
                                <div>
                                    <a href="https://apps.apple.com/us/app/designa-studio/id1495137130" target="_blank"><img src="/site/images/app-store.png" class="img-fluid"></a>
                                </div>
                                <div>
                                    <a href="https://play.google.com/store/apps/details?id=com.globalenablement.designa&hl=en" target="_blank"><img src="/site/images/google-play.png" class="img-fluid"></a>
                                </div>
                            </div>   
                        </div> 
                    </div>
                    <div class="col-lg-1"></div>
                </div>      
            </div>

            <div class="tab-pane fade" id="pills-resources" role="tabpanel" aria-labelledby="pills-resources-tab">
                <div class="text-center">
                        <h1>Webinar Registration</h1>
                        <p>Register for a webinar to learn the basics of Designa to get you started:</p>
                        <ul class="webinar-list">
                            <li>Create a profile</li>
                            <li>Load your products</li>
                            <li>Create Inspiration</li>
                        </ul>
                        <p>We'll also be sharing tips and tricks to optimise your profile and get the most out of Designa platform.</p>
                    
                </div>   
                 <hr>
                 
                 <!--
                 <div class="text-center">
                    <a class="scroll-down" href="#">&darr;</a>
                   </div>-->
                
                <div class="webform-box">
                   <webinar-registration-form></webinar-registration-form>
                </div>
                
                
               
                <div class="text-center mt-4">
        
                        <h1>User Guide Videos</h1>
                        <p>New to Designa? Here are our user videos to help you get started</p>
                    
                </div>   
                <hr>
                
                <div class="d-none">    
                    <div class="row" style="padding-bottom: 30px; padding-top: 10px">
                        <div class="col-lg-6">
                            <input placeholder="Search" autocomplete="off" class="form-control form-control form-input-sm search-box">
                        </div>
                        <div class="col-lg-6 text-right">
                            <i class="fa fa-th-large" style="padding-right:  15px; color: #f39c12; font-size: 30px;"></i><i class="fa fa-list" style="font-size: 30px;"></i>
                        </div>                        
                    </div>
                </div>

                <div class="row text-center">
                    <div class="col-lg-4">
                        <div class='embed-container'><iframe src='https://www.youtube.com/embed/Qp9wvJYp4-4' frameborder='0' allowfullscreen></iframe></div>
                        <h5 class="title-space">Designa Mobile App</h5>
                    </div>
                    <div class="col-lg-4">
                        <div class='embed-container'><iframe src='https://www.youtube.com/embed/A_4Ewv_-I8c' frameborder='0' allowfullscreen></iframe></div>
                        <h5 class="title-space">How to Register as Supplier</h5>
                    </div>
                    <div class="col-lg-4">
                        <div class='embed-container'><iframe src='https://www.youtube.com/embed/-NEjSzubR0A' frameborder='0' allowfullscreen></iframe></div>
                        <h5 class="title-space">How to Register As Customer</h5>
                    </div>  
                </div>
                
                <div class="row text-center">
                    <div class="col-lg-4">
                        <div class='embed-container'><iframe src='https://www.youtube.com/embed/LA7v31cUwnU' frameborder='0' allowfullscreen></iframe></div>
                        <h5 class="title-space">How to Filter Decor</h5>
                    </div>
                    <div class="col-lg-4">
                        <div class='embed-container'><iframe src='https://www.youtube.com/embed/sIeR9KrW68Q' frameborder='0' allowfullscreen></iframe></div>
                        <h5 class="title-space">How to Filter Inspiration</h5>
                    </div>
                    <div class="col-lg-4">
                        <div class='embed-container'><iframe src='https://www.youtube.com/embed/FnWdAWTJqwM' frameborder='0' allowfullscreen></iframe></div>
                        <h5 class="title-space">How to Filter Supplier</h5>
                    </div>  
                </div>
                
                <div class="row text-center">
                    <div class="col-lg-4">
                        <div class='embed-container'><iframe src='https://www.youtube.com/embed/tcFnU4d3ZOE' frameborder='0' allowfullscreen></iframe></div>
                        <h5 class="title-space">How to Filter Venue</h5>
                    </div>
                    <div class="col-lg-4">
                        <div class='embed-container'><iframe src='https://www.youtube.com/embed/NRp5-nfyA2Y' frameborder='0' allowfullscreen></iframe></div>
                        <h5 class="title-space">How Dashboard Works</h5>
                    </div>
                    <div class="col-lg-4">
                        <div class='embed-container'><iframe src='https://www.youtube.com/embed/duHTwYGcCm0' frameborder='0' allowfullscreen></iframe></div>
                        <h5 class="title-space">Chat Function 1</h5>
                    </div>  
                </div>
                
                <div class="row text-center">
                    <div class="col-lg-4">
                        <div class='embed-container'><iframe src='https://www.youtube.com/embed/bBAtlOyb2qU' frameborder='0' allowfullscreen></iframe></div>
                        <h5 class="title-space">Chat Function 2</h5>
                    </div>
                    <div class="col-lg-4">
                        <div class='embed-container'><iframe src='https://www.youtube.com/embed/QQv4xc1CFgs' frameborder='0' allowfullscreen></iframe></div>
                        <h5 class="title-space">How to Load Inspiration</h5>
                    </div>
                    <div class="col-lg-4">
                        <div class='embed-container'><iframe src='https://www.youtube.com/embed/7K72cKKKusU' frameborder='0' allowfullscreen></iframe></div>
                        <h5 class="title-space">Quotation Function</h5>
                    </div>  
                </div>
                
            </div>
            
            <div class="tab-pane fade" id="pills-advertising" role="tabpanel" aria-labelledby="pills-advertising-tab">
                <div class="row">
                    <div class="col-lg-1"></div>
                    <div class="col-lg-10 text-center advertising-text">
                        Advertising opportunities will be available on Designa shortly. Watch this space.            
                    </div>
                    <div class="col-lg-1"></div>
                </div>     
            </div>

        </div>
    </div>


@endsection

@push('scripts')
    <script>
        const second = 1000,
        minute = second * 60,
        hour = minute * 60,
        day = hour * 24;

        let countDown = new Date('Jul 01, 2020 00:00:00').getTime(),
            x = setInterval(function() {    

            let now = new Date().getTime(),
                distance = countDown - now;

            document.getElementById('days').innerText = Math.floor(distance / (day)),
                document.getElementById('hours').innerText = Math.floor((distance % (day)) / (hour)),
                document.getElementById('minutes').innerText = Math.floor((distance % (hour)) / (minute)),
                document.getElementById('seconds').innerText = Math.floor((distance % (minute)) / second);

            //do something later when date is reached
            //if (distance < 0) {
            //  clearInterval(x);
            //  'IT'S MY BIRTHDAY!;
            //}

            }, second)
    
    </script>
    
    <script>
       $(function() {
    $('.scroll-down').click (function() {
      $('html, body').animate({scrollTop: $('.ok').offset().top }, 'slow');
      return false;
    });
  });
    </script>
    
    <script>
        $(function () {
            var hash = window.location.hash;
            hash && $('ul.nav a[href="' + hash + '"]').tab('show');
        });
    </script>


@endpush

@push('styles')
    <style>
    
        .need-box h4 {
            padding-top: 20px;
            padding-bottom: 10px;
        }
 
        .fa-briefcase, .fa-laptop {
            font-size: 47px;
        }
        
        .scroll-down {
            height: 50px;
    width: 50px;
    background-color: #f39c12;
    border-radius: 50%;
    display: inline-block;
    color: #fff;
    float: right;
    font-size: 29px;
        }
        
        .custom-nav-pills{
            display:flex;
        }

        .custom-nav-pills .nav-item{
            flex:1;
        }
         .custom-nav-pills .nav-link.active, .custom-nav-pills .show >.nav-link {
            border-top: 4px solid!important;
            font-weight: 500;
            background-color: #f7f7f7;
            border-color: transparent transparent #f39c12;
            color: #f39c12;
            border-radius: inherit;
        }

        .custom-nav-pills .nav-link {
            display: block;
            text-align: center;
            border-top: 4px solid #e6e6e6;
            padding:1em 0px 1em 0px;
            font-size:18px;
        }

        h1 {
            color: #343433;            
            text-align: center;
            font-size: 31px;
        }

        .send-form-box {
            background: rgb(230, 230, 230); 
            text-align: left; 
            padding: 20px 110px;
            margin-top: 24px;
        }

        .send-row {
            display: grid;
            grid-template-columns: auto 1fr auto;
            grid-gap: 15px;
            align-items: center;
        }

        .store-icon {
            display: grid;
            grid-template-columns: 1fr 1fr;
            grid-gap: 15px;
            align-items: center;
        }

        .stores {
            padding: 0 267px;    
        }

        .embed-container { 
            position: relative; 
            padding-bottom: 56.25%; 
            height: 0; 
            overflow: hidden; 
            max-width: 100%; 
        } 
        
        .embed-container iframe, 
        .embed-container object, 
        .embed-container embed,
        .embed-container-resources iframe,
        .embed-container-resources object,
        .embed-container-resources embed { 
            position: absolute; 
            top: 0; 
            left: 0; 
            width: 100%; 
            height: 100%; 
        }
        
        .embed-container-resources {
            position: relative;
            padding-bottom: 43.25%;
            height: 0;
            overflow: hidden;
            max-width: 70%;
            margin: 0 auto;
            text-align: center;
        }
        
        .agree-text {
            font-size: 12px;
            padding: 20px 125px;
        }
        
        .search-box {
            width: 357px;
        }
        
        .title-space {
            padding-top: 10px;
        }
        
        .benefits {
            padding-top: 77px;
            padding-bottom: 30px;
        }
        
        .reach-box {
            background: url(site/images/icon/reach-new-customers.png);
            height: 246px;
            width: 356px;
        }
        
        .directly-box {
            background: url(site/images/icon/directly-engage-and-inspire.png);
            height: 236px;
            width: 356px;
        }
        
        .instantly-box {
            background: url(site/images/icon/instantly-notify-customers.png);
            height: 236px;
            width: 356px;
        }
        
        .tag-box {
            background: url(site/images/icon/tag-share-and-promote.png);
            height: 236px;
            width: 356px;
        }
        
        .receive-box {
            background: url(site/images/icon/receive-real%20time-quotes.png);
            height: 236px;
            width: 356px;
        }
        
        .message-box {
            background: url(site/images/icon/message-and-engage-clients.png);
            height: 236px;
            width: 356px;
        }
        
        .clients-box {
            background: url(site/images/icon/designa-studio-allows.png);
            height: 236px;
            width: 356px;
        }
        
        .reach-box p,
        .directly-box p,
        .instantly-box p, 
        .tag-box p,
        .receive-box p,
        .message-box p,
        .clients-box p {
            position: relative;
            top: 50%;
            text-align: center;
            left: 0;
            right: 0;
            padding-left: 50px;
            padding-right: 50px;
        }
        
        .benefits-space {
            padding-top: 40px;
        }
        
        .suppliers img {
            width: 200px;
        }
        
        .btn-supplier {
            width: 300px;
        }
        
        #inquiry-box {
            position: fixed;
            bottom: 40px;
            right: 40px;
            z-index: 9999;
            width: 450px;
        }
        
        .need-box {
            background: #fff;
            /*padding: 57px 67px;*/
            padding: 43px 73px;
            margin-top: 57px;
            text-align: center;
        }
        
        .page-header-img h1 {
            display: none;
        }
        
        .advertising-text {
            /*font-size: 18px;
            padding: 30px;
            border: 1px solid #e6e6e6;*/
            color: #f39c12;
            font-size: 36px;
            font-weight: bold;
            font-family: 'Poppins', sans-serif;
        }
        
        .webform-box {
            padding-bottom: 16px;
            padding-left: 20%;
            padding-right: 20%;
        }
        
        .countdown li {
          display: inline-block;
          font-size: 1.5em;
          list-style-type: none;
          padding: 1em;
          text-transform: uppercase;
        }
        
        .countdown li span {
          display: block;
          font-size: 4.5rem;
        }
        
        .launching {
          text-align: center;
          padding-bottom: 26px;
          color: rgb(243, 156, 18);
          font-size: 28px;
        }
        
        .countdown-box {
            padding: 30px 0 0 0;
            text-align: center;
        }
        
        .webinar-list {
             width: 19%; 
             margin: auto; 
             text-align: left;
             padding-bottom: 20px;
        }
        
        /* Portrait */
        @media only screen 
          and (min-device-width: 375px) 
          and (max-device-width: 667px) 
          and (-webkit-min-device-pixel-ratio: 2)
          and (orientation: portrait) { 
              .custom-nav-pills {
                display: block !important;
                padding-bottom: 22px !important;
              }
              
              .form-space {
                 padding-bottom: 10px;
              }
              
              .send-row {
                 display: block !important;
              }
              
              .send-form-box {
                padding: 20px 42px !important;
                text-align: center !important;
              }
              
              .agree-text {
                padding: 13px 0px 0px 0px !important;
                text-align: center !important;
              }
              
              .stores {
                padding: 0 60px !important;
              }
              
              .search-box {
                width: 205px !important;
              }
              
              .title-space {
                padding-bottom: 10px;
             }
             
             .suppliers img {
                 float: none !important;
             }
             
             .btn-supplier {
                width: 257px !important;
                margin-bottom: 14px !important;
                float: none !important;
            }
            
            .header-search,
            .header-links,
            .sections,
            .links {
                display: none !important;
            }
            
            #inquiry-box {
                width: auto !important;
            }
            
            .need-box {
                padding: 57px 37px !important;
            }
            
            .embed-container-resources {
                padding-bottom: 53% !important;
                max-width: 100% !important;
            }
            
            .logo {
                height: auto !important;
                width: auto !important;
            }
            
            .launching {
                padding-left: 20px;
                padding-right: 20px;
            }
            
            .countdown-box {
                text-align: left !important;
            }
            
            .webinar-list {
                width: 68% !important;
                padding-bottom: 16px !important;
            }
            
            .webform-box {
                padding-left: 0 !important;
                padding-right: 0 !important;
            }
            
            .fa-briefcase {
                padding-top: 44px;
            }
    </style>
    
    
@endpush