@extends('site.includes.layouts.for-subscription-payment')

@section('page-title', 'Subscription')

@section('content') 
    <div class="container pt-4">

        <form action="{{ route('site.subscription.store') }}" method="POST" >
            @csrf                    
                <h4>Select Plan</h4>
                <div class="pl-3">
                    @foreach($plans as $plan)
                        <div class="alert alert-secondary">
                            <div class="box-content">
                                <input type="radio" name="plan" value="{{ $plan->id }}"> <strong>{{ $plan->name }}</strong>
                                {{ number_format($plan->price, 2) }}/month
                            </div>
                        </div>
                    @endforeach
                </div>
                <h4>Payment Method</h4>
                <div class="pl-3">
                    @foreach(Auth::user()->user_payment_methods as $paymentMethod)
                        <div>
                            <input type="radio" name="payment_method"  @if(Auth::user()->default_payment_method->id  == $paymentMethod->id)  checked @endif> 
                            <i class="fa fa-cc-{{ $paymentMethod->card->brand }}" ></i> **** {{ $paymentMethod->card->last4 }}      
                            Expires {{ date('m', ($paymentMethod->card->exp_month)) }}/{{ $paymentMethod->card->exp_year }}
                        </div>
                    @endforeach
                </div>
                <br>
                <button type="submit" class="btn btn-warning btn-warning" > Submit</button>
              
        </form>
    </div>
@endsection