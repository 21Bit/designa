@extends('site.includes.layouts.full')

@section('page-title', $venue->name)

@section('page-meta')
    <meta name="description" content="{{ Str::limit($venue->description, 50) }}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="{{ $venue->name }}">
    <meta property="og:description" content="{{ Str::limit($venue->description, 50) }}">
    <meta property="og:image" content="{{ url($venue->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png')) }}">
    <meta property="og:url" content="{{ route('site.venue.show', $venue->slug) }}">
@endsection

@section('content')
<!-- <div class="page-header-img mb-4">
    <img src="{{ $venue->company->image('thumbnail')->path('thumbnails', '')  }}" class="w-100" alt="">
    {{-- <img src="/site/images/venue-header.jpg" class="mw-100" alt=""> --}}
</div> -->

<!-- <div class="sticky-top bg-white"> -->
    <div class="inspiration-header" style="background-image:url('{{ $venue->image('thumbnail')->path('thumbnails', 'https://via.placeholder.com/1920x450') }}')">
      {{-- <img src="{{ $venue->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png') }}" class="img img-responsive img-thumbnail mb-2" alt=""> --}}
    </div>
    <div id="supplier-page">
        <div class="text-center mb-3">
            <img class="logo img img-thumbnail mb-2" src="{{ $venue->company->image('logo')->path('logos', '/images/placeholders/placeholder.png') }}" alt="">
            {{-- <img src="{{ $venue->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png') }}" class="img img-responsive img-thumbnail mb-2" alt=""> --}}
            {{-- <img src="{{ $venue->company->image('logo')->path('logos', '/images/placeholders/placeholder.png') }}" class="img img-responsive img-thumbnail mb-2" alt=""> --}}
            <h2 class="company-name" style='text-transform: none;' >{{ $venue->name }}</h2>
            <div class="text-center mb-4">
                <small class="mr-3 d-block">
                    By: <a href="{{ route('site.supplier.show', $venue->company->slug) }}">{{ optional($venue->company)->name }}</a>
                </small>
                <small class="mr-3 d-block">
                    <i class="fa fa-map-marker"></i> {{ $venue->location }}
                </small>
                @auth
                    <heart-component :default="{{ $venue->hearts()->whereUserId(Auth::user()->id)->first() ? 1 : 0  }}" :id="{{ $venue->id }}" type="/venue" styleclass="btn btn-sm btn-outline-secondary btn-lg mt-4"> Save Venue</heart-component>
                @endif
            </div>
        </div>

        <div class="container-fluid mb-5">
            <div class="line-tab">
                <div class="container">
                    <nav class="mb-3">
                        <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="inspiration-tab" data-toggle="tab" href="#nav-inspiration" role="tab" aria-controls="nav-inspiration" aria-selected="false">INSPIRATIONS</a>
                            <a class="nav-item nav-link" id="spaces-tab" data-toggle="tab" href="#nav-spaces" role="tab" aria-controls="nav-spaces" aria-selected="false">EVENT SPACES</a>
                            <a class="nav-item nav-link" id="reviews-tab" data-toggle="tab" href="#nav-reviews" role="tab" aria-controls="nav-reviews" aria-selected="false">REVIEWS</a>
                            <a class="nav-item nav-link " id="about-tab" data-toggle="tab" href="#nav-about" role="tab" aria-controls="nav-about" aria-selected="true">ABOUT</a>
                        </div>
                    </nav>
                </div>
                <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                    <div class="tab-pane fade  " id="nav-about" role="tabpanel" aria-labelledby="about-tab">
                        <div class="container">
                            <div class="box bg-white p-2 mb-1 pb-4 pt-5">
                                <div class="row">
                                    <div class="col-sm-8 offset-sm-2">
                                        <p>
                                            {!! $venue->description !!}
                                        </p>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button href="#" class="btn btn-outline-warning btn-sm rounded-0 show-more" style="display:none" data-target=".load-more-content"> SHOW MORE... </button>
                                </div>
                                <div class="load-more-content">
                                    <div class="row">
                                        <div class="col-sm-8 offset-sm-2">
                                            <table class="table mt-3 borderless" style='font-size:14px'>
                                                <tr>
                                                    <td width="150">Type</td>
                                                    <td>: {{ $venue->categoryList('venue_type') }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Events</td>
                                                    <td>: {{ $venue->categoryList('cater') }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Capacity</td>
                                                    <td>: {{ $venue->capacity }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Max Seated</td>
                                                    <td>: {{ $venue->max_seated }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Max Standing</td>
                                                    <td>: {{ $venue->max_standing }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Catering Options</td>
                                                    <td>: {{ $venue->categoryList('catering_option') }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Beverage Options</td>
                                                    <td>: {{ $venue->categoryList('beverage_option') }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Amenities</td>
                                                    <td>: {{ $venue->categoryList('amenity') }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Average Cost</td>
                                                    <td>: {{ $venue->categoryList('average_cost_per_head') }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Operation Hours</td>
                                                    <td>
                                                        @for($i = 0; $i < count($explodedTimes); $i++) {{-- {{ $loop->iteration }} --}} @if($explodedTimes[$i]
                                                            !='null' ) <div class="mb-2">
                                                            {{ getFormatedTime($explodedTimes[$i]) }}
                                                            <small class="text-muted d-block">
                                                                {{ getFormatedDay($i+1) }}
                                                            </small>
                                                            </div>
                                                            @endif
                                                            @endfor
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button href="#" class="btn btn-outline-warning btn-sm rounded-0 show-less"  data-target=".load-more-content"> SHOW LESS</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade show active" id="nav-inspiration" role="tabpanel" aria-labelledby="inspiration-tab">
                        <div class="inspiration-grid">
                            @forelse($inspirations as $inspiration)
                                <div class="inspiration-item">
                                    <a href="{{ route('site.inspiration.show', $inspiration->slug) }}">
                                        <img src="{{ $inspiration->image('thumbnail')->path('thumbnails','/images/placeholders/placeholder.png' ) }}" alt="">
                                        <h4 class="title">{{ $inspiration->name }} <br>
                                            <small><i class="fa fa-map-marker" style="font-size:11px"></i> {{ $inspiration->location }}</small>
                                        </h4>
                                        
                                        <div class="other-info text-right">
                                            <div class="text-right pt-3">
                                                <button class="btn btn-sm btn-icon">
                                                    <i class="fa fa-heart"></i>
                                                </button>
                                                <button class='btn btn-sm btn-icon'>
                                                    <i class="fa fa-share"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="shadow"></div>
                                    </a>    
                                </div>
                            @empty  
                                <section class="pt-5 pb-5 mb-5 mt-5">
                                    <div class="col-12 text-center p-5">
                                        <strong class="text-muted mt-5">No Inspiration</strong>
                                    </div>
                                </section>
                            @endforelse
                        </div>
                    </div>
                    <div class="tab-pane fade" id="nav-spaces" role="tabpanel" aria-labelledby="spaces-tab">
                        @if($functionspaces)
                            <section class="pt-2 pb-5" id="function-space">
                                <div class="container">
                                    <div class="row">
                                        @forelse($functionspaces as $functionspace)
                                           <div class="col-12 shadow-sm mb-3 bg-white ">
                                                <div class="row shadow-sm p-2">
                                                    <div class="col-md-3 shadow-sm rounded" style="background-color:#e1e1e1 ;padding-top: 20%;  background-position:center; background-repeat:no-repeat;   background-size:cover; background-image: url('{{ $functionspace->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder250x200.png') }}')">
                                                    
                                                    </div>
                                                    <div class="col-md-9">
                                                            <h4 class="mb-0 d-block mt-3 mb-2 text-bold">
                                                                {{ $functionspace->name }}
                                                            </h4>    
                                                            <span class='mr-3'>
                                                                Max Seated: {{ $functionspace->max_capacity_seated }} 
                                                            </span>
                                                            <span>
                                                                Max Standing: {{ $functionspace->max_capacity_standing }} 
                                                            </span>
                                                            <div class="pt-3">
                                                                <small class="text-muted">
                                                                    {!! nl2br($functionspace->description) !!}
                                                                </small>
                                                            </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @empty
                                            <div class="col-12 text-center p-5">
                                                <img src="/images/placeholders/no-data.png" alt="">
                                                {{-- <strong class="text-muted mt-5">No function space</strong> --}}
                                            </div>
                                        @endforelse
                                    </div>
                                </>
                            </section>
                        @endif
                    </div>
                    <div class="tab-pane fade" id="nav-reviews" role="tabpanel" aria-labelledby="reviews-tab">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-8 offset-sm-2">
                                <review-component  type="venue" :id="{{ $venue->id }}"></review-component>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if($otherVenues)
        <div class="container mt-5 mb-5">
            <h4 class="text-center mb-3 mt-5">
                Other Venues
            </h4>
            <venue-related-component uri="{{ route("site.venue.list.related") }}" 
                    current_id="{{ $venue->id }}" 
                    supplier="{{ $venue->company_id }}"
                ></venue-related-component>
        </div>
        @endif
    </div>
@endsection
