@extends('site.includes.layouts.full')

@section('page-title', 'Venues')

{{-- @section('page-meta')
    <meta name="description" content="Desif">
    <meta property="og:type" content="website">
    <meta property="og:title" content="{{ $inspiration->name }}">
    <meta property="og:description" content="{{ Str::limit($inspiration->description, 50) }}">
    <meta property="og:image" content="{{ url($inspiration->image('cover')->path('covers', '/images/placeholders/placeholder.png')) }}">
    <meta property="og:url" content="{{ route('site.inspiration.show', $inspiration->slug) }}">
@endsection --}}

@section('content')

<div class="page-header-img mb-4">
    <h1 class="text-orange">Venue</h1>
    <img src="/site/images/venue.png" class="bg-image"  alt="">
    <img class="bottom-pattern" src="/site/images/bottom-pattern.png" alt="">
</div>

<venue-filter-component default-category="{{ $defaultCategory }}"  ></venue-filter-component>

@endsection
