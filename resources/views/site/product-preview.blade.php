@extends('site.includes.layouts.full')

@section('content')
    <div class="modal" style="display: block" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-full-width" role="document">
            <div class="modal-content modal-content-full-width">
                <div class="modal-body">
                    
                    <div class="row no-gutters">
                        <div class="col-8 col-sm-9">
                            <div class="product-gallery">
                                <div class="main-image text-center align-items-center with-images">
                                    <img src="https://designa.studio/images/thumbnails/5e93d260a6e6e1586745952.png" class="mw-100 mh-100 center" alt="">
                                </div>
                                {{-- <div class="image-list p-2 d-flex justify-content-center align-items-center">
                                    <div class="other-image">
                                        <img src="https://designa.studio/images/thumbnails/5e93b4eb39ad61586738411.png" alt="">
                                    </div>
                                    <div class="other-image">
                                        <img src="https://designa.studio/images/thumbnails/5e93b4eb39ad61586738411.png" alt="">
                                    </div>
                                    <div class="other-image">
                                        <img src="https://designa.studio/images/thumbnails/5e93b0a1b03641586737313.png" alt="">
                                    </div>
                                    <div class="other-image">
                                        <img src="https://designa.studio/images/thumbnails/5e93b4eb39ad61586738411.png" alt="">
                                    </div>
                                    <div class="other-image">
                                        <img src="https://designa.studio/images/thumbnails/5e93b0a1b03641586737313.png" alt="">
                                    </div>
                                    <div class="other-image">
                                        <img src="https://designa.studio/images/thumbnails/5e93b4eb39ad61586738411.png" alt="">
                                    </div>
                                    <div class="other-image">
                                        <img src="https://designa.studio/images/thumbnails/5e93b0a1b03641586737313.png" alt="">
                                    </div>
                                    <div class="other-image">
                                        <img src="https://designa.studio/images/thumbnails/5e93b4eb39ad61586738411.png" alt="">
                                    </div>
                                    <div class="other-image">
                                        <img src="https://designa.studio/images/thumbnails/5e93b0a1b03641586737313.png" alt="">
                                    </div>
                                    <div class="other-image">
                                        <img src="https://designa.studio/images/thumbnails/5e93b1b4d59e81586737588.png" alt="">
                                    </div>
                                    <div class="other-image">
                                        <img src="https://designa.studio/images/thumbnails/5e93b4eb39ad61586738411.png" alt="">
                                    </div>
                                    <div class="other-image">
                                        <img src="https://designa.studio/images/thumbnails/5e93b0a1b03641586737313.png" alt="">
                                    </div>
                                    <div class="other-image">
                                        <img src="https://designa.studio/images/thumbnails/5e93b1b4d59e81586737588.png" alt="">
                                    </div>
                                    <div class="other-image">
                                        <img src="https://designa.studio/images/thumbnails/5e93b4eb39ad61586738411.png" alt="">
                                    </div>
                                    <div class="other-image">
                                        <img src="https://designa.studio/images/thumbnails/5e93b0a1b03641586737313.png" alt="">
                                    </div>
                                    <div class="other-image">
                                        <img src="https://designa.studio/images/thumbnails/5e93b1b4d59e81586737588.png" alt="">
                                    </div>
                                    <div class="other-image">
                                        <img src="https://designa.studio/images/thumbnails/5e93b4eb39ad61586738411.png" alt="">
                                    </div>
                                    <div class="other-image">
                                        <img src="https://designa.studio/images/thumbnails/5e93b0a1b03641586737313.png" alt="">
                                    </div>
                                    <div class="other-image">
                                        <img src="https://designa.studio/images/thumbnails/5e93b1b4d59e81586737588.png" alt="">
                                    </div>
                                    <div class="other-image">
                                        <img src="https://designa.studio/images/thumbnails/5e93b4eb39ad61586738411.png" alt="">
                                    </div>
                                    <div class="other-image">
                                        <img src="https://designa.studio/images/thumbnails/5e93b0a1b03641586737313.png" alt="">
                                    </div>
                                    <div class="other-image">
                                        <img src="https://designa.studio/images/thumbnails/5e93b1b4d59e81586737588.png" alt="">
                                    </div>
                                </div> --}}
                            </div>
                        </div>
                        <div class="col-4 col-sm-3">
                           
                            <div class="product-information">
                                <div class="supplier-box item p-3">
                                     <div class="text-right p-1 top-button-close-div">
                                        <button type="button" class='close-button' data-dismiss="modal" aria-label="Close">
                                            <i class="fa fa-remove"></i>
                                        </button>
                                    </div>
                                    <div class="d-flex align-items-center align-self-center">
                                        <div class="" style="width: 46px;">
                                            <img src="https://designa.studio/images/logos/1585180850.png" class="w-100 rounded-circle" alt="">
                                        </div>
                                        <div class="ml-2">
                                            <h5 class="m-0" style='font-size:16px'>
                                                STARLIGHT CHANDELIERS
                                            </h5>
                                            <small>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            </small>
                                        </div>
                                    </div>
                                </div>
                                <div class="side-info item p-4">
                                    <h4 class="title m-0">Liana Wedding Invitation – Digital Print & Foil</h4>
                                     <small>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            </small>
                                    <p class="description d-block mt-4">
                                        Custom designed wedding invitation digitally printed on a 600gsm premium cotton paper with foiling Production Time: +/- 2 – 3 weeks *Envelopes not included. Available in a range of foil colours. If you wish to customise design or require a smaller/larger quantity, please email info@luxeletterpress.com.au
                                    </p>
                                </div>
                                <div class="side-info item p-4">
                                    <h3 style="font-size:1.1rem;">Product Details</h3>
                                    <table class='w-100'>
                                        <tr>
                                            <td class="text-bold">Event Type</td>
                                            <td>Wedding</td>
                                        </tr>
                                        <tr>
                                            <td>Colours</td>
                                            <td>wef</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="side-info item p-4">
                                    <h3 style="font-size:1.1rem">Related Items</h3>
                                    <div class="row">
                                        <div class="col-3">
                                            <div class="related-item" style="background-image: url('https://designa.studio/images/thumbnails/5e93b4eb39ad61586738411.png')">
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="related-item" style="background-image: url('https://designa.studio/images/thumbnails/5e93b0a1b03641586737313.png')">
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="related-item" style="background-image: url('https://designa.studio/images/thumbnails/5e93b4eb39ad61586738411.png')">
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="related-item" style="background-image: url('https://designa.studio/images/thumbnails/5e93af53383051586736979.png')">
                                            </div>
                                        </div>
                                    </div>    
                                </div>

                                <div class="side-info item p-4">
                                    <h3 style="font-size:1.1rem">Reviews</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <style>

            .other-image img{
                width: 100px;
                height: 100px;
                margin:10px;

            }
            .title{
                font-size:1.3rem;
            }

            .description{
                color:#6b6b6b;
            }

            .related-item{
                background-size:cover;
                width: 100%;
                height: 100px;
                margin-bottom:2em;
            }

            .image-list{
                height: 150px;
                overflow-y: auto;
            }

            .main-image{
                height: 100vh;
                position: relative;
            }

            .with-images{
                 height: calc(100vh - 150px);
            }

            .center {
                margin: 0;
                position: absolute;
                top: 50%;
                left: 50%;
                -ms-transform: translate(-50%, -50%);
                transform: translate(-50%, -50%);
            }

            .side-info{
            }

            .item{
                border-bottom:1px solid #f1f1f1;
            }

            .supplier-box{
                position: relative;
                padding-right: 50px;
            }

            .top-button-close-div{
                height: 50px;
                position: absolute;
                top:0;
                right: 0;
            }

              .close-button{
                border:none;
                background-color:transparent;
                font-size:24px;
                cursor: pointer;
            }

            .product-information{
                height: calc(100vh);
                overflow: auto;
            }

            .no-gutters {
                margin-right: 0;
                margin-left: 0;

                > .col,
                > [class*="col-"] {
                    padding-right: 0;
                    padding-left: 0;
                }
            }

          

            .close{
                font-size:24px !important;
            }

            .product-gallery{
                height: 100vh;
                border-right:1px solid #ccc;
            }

            .modal-dialog-full-width {
                width: 100% !important;
                height: 100% !important;
                margin: 0 !important;
                padding: 0 !important;
                max-width:none !important;

            }

            .modal-body{
                padding:0px;
            }

            .modal-content-full-width  {
                height: auto !important;
                min-height: 100% !important;
                border-radius: 0 !important;
                background-color:#fff; 
            }

            .modal-header-full-width  {
                border-bottom: 1px solid #9ea2a2 !important;
            }

            .modal-footer-full-width  {
                border-top: 1px solid #9ea2a2 !important;
            }

    </style>
@endpush