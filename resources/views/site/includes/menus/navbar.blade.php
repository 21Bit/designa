<div class="top-menu">
    <nav class="navbar navbar-expand-lg p-0">
        <div class="container">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <ul class="navbar-nav mx-auto">
                    <li class="nav-item @if(Request::segment(1) == "inspiration") active @endif ">
                        <a class="nav-link" href="{{ route('site.inspiration.index') }}"><i class="fa fa-camera"></i> Inspiration <span class="sr-only">(current)</span></a>
                        <div class="mega-menu">
                            <div class="sections">
                                @foreach($data['caters'] as $cater)
                                    <div class="section">
                                        <h4 class="title mb-0">
                                            <a class="text-orange" style="text-weight:bold; font-size:16px" @if(Request::get('cat') == $cater->slug) style="color:orange" @endif  href="{{ route('site.inspiration.index', ['cat' => $cater->slug]) }}">{{ $cater->name }}</a>
                                        </h4>
                                        @foreach($cater->styles as $style)
                                            <a @if(Request::get('cat') == $style->slug) style="color:orange" @endif href="{{ route('site.inspiration.index',['cat' => $style->slug]) }}">{{ $style->name }}</a>
                                        @endforeach
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </li>
                    <li class="nav-item @if(Request::segment(1) == "venue") active @endif">
                        <a class="nav-link" href="{{ route('site.venue.index') }}"><i class="fa fa-map-marker"></i> Find Venue</a>
                        <div class="mega-menu">
                            <div class="sections">
                                @php 
                                    $venues_counter = 1;
                                    $venue_limit = 4; 
                                @endphp
                                @foreach($data['venue_types'] as $venue)
                                    @if($venues_counter == 1)
                                        <div class="section">
                                    @endif
                                        <a @if(Request::get('cat') == $style->slug) style="color:orange" @endif  href="{{ route('site.venue.index', ['cat' => $venue->slug]) }}">{{ $venue->name }}</a>
                                    
                                    @if($venues_counter == $venue_limit)
                                        </div>
                                            @php $venues_counter = 1; @endphp
                                    @else
                                        @php $venues_counter++ @endphp
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </li>
                    <li class="nav-item @if(Request::segment(1) == "supplier") active @endif">
                        <a class="nav-link" href="{{ route('site.supplier.index') }}"><i class="fa fa-truck"></i> Find Supplier</a>
                        <div class="mega-menu">
                             <div class="sections">
                                @php 
                                    $suppliers_counter = 1;
                                    $supplier_limit = 6; 
                                @endphp
                                @foreach($data['suppliers'] as $supplier)
                                    @if($suppliers_counter == 1)
                                        <div class="section">
                                    @endif
                                        <a @if(Request::get('cat') == $style->slug) style="color:orange" @endif  href="{{ route('site.supplier.index', ['cat' => $supplier->name]) }}">{{ $supplier->display_name }}</a>
                                    
                                    @if($suppliers_counter == $supplier_limit)
                                        </div>
                                            @php $suppliers_counter = 1; @endphp
                                    @else
                                        @php $suppliers_counter++ @endphp
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </li>
                    <li class="nav-item @if(Request::segment(1) == "decor") active @endif">
                        <a class="nav-link" href="{{ route('site.decor.index') }}"><i class="fa fa-search"></i> Find Décor</a>
                        <div class="mega-menu">
                             <div class="sections">
                                @php 
                                    $products_counter = 1;
                                    $product_limit = 6; 
                                @endphp
                                @foreach($data['products'] as $product)
                                    @if($products_counter == 1)
                                        <div class="section">
                                    @endif
                                    <div>
                                        @if(count($product->styles))
                                            <div>
                                                <a style="display: inline" @if(Request::get('cat') == $style->slug) style="color:orange" @endif  href="{{ route('site.decor.index',['cat' => $product->slug]) }}">{{ $product->name }}</a>
                                                <a style="display: inline" class="ml-1" data-toggle="collapse" href="#collapse-{{ $product->id }}" role="button" aria-expanded="false" aria-controls="collapse-{{ $product->id }}">
                                                    <i class="fa" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        @else
                                            <a @if(Request::get('cat') == $style->slug) style="color:orange" @endif  href="{{ route('site.decor.index',['cat' => $product->slug]) }}">{{ $product->name }}</a>
                                        @endif
                                            <div class="pl-3 collapse multi-collapse" id="collapse-{{ $product->id }}">
                                                @foreach($product->styles as $style)
                                                    <a @if(Request::get('cat') == $style->slug) style="color:orange" @endif  href="{{ route('site.decor.index',['cat' => $style->slug]) }}">{{ $style->name }}</a>
                                                @endforeach
                                            </div>
                                    </div>
                                    @if($products_counter >= $product_limit)
                                        </div>
                                            @php $products_counter = 1; @endphp
                                    @else
                                        @php $products_counter++ @endphp
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </li>
                    {{-- <li class="nav-item">
                        <a class="nav-link" href="{{ route('site.pricing.index') }}"><i class="fa fa-dollar"></i> Pricing</a>
                    </li> --}}
                    <li class="nav-item">
                        <a class="nav-link" href="#"><i class="fa fa-comment"></i> Get Help</a>
                        <div class="mega-menu">
                             <div class="sections">
                                 <div class="section">
                                    <a href="/for-supplier/supplier-hub">For Supplier</a>
                                 </div>
                             </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>



