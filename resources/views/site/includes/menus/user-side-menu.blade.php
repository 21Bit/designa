<div class="site-side-link">
    <div class="profile mb-3 text-center">
        <img src="{{ Auth::user()->getProfilePicture() }}" class="mw-100 rounded-circle mx-auto d-block mb-2" style="width:80px; height: 80px;" alt="User Image">
        <h3 class="text-orange">{{ Auth::user()->name }}</h3>
        <hr>
    </div>

    <ul class="nav flex-column">
        <li class="nav-item">
            <a class="nav-link @if(Request::segment(2) == "profile") active @endif"  href="{{ route('user.profile.index') }}"><i class="fa fa-user"></i> Profile</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if(Request::segment(2) == "saved") active @endif" href="{{ route('user.saved.inspirations') }}"><i class="fa fa-tag"></i> Saved Items</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if(Request::segment(2) == "qoute") active @endif" href="{{ route('user.qoute.index') }}"><i class="fa fa-file"></i> Quotes</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if(Request::segment(2) == "project") active @endif" href="{{ route('user.event-board.index') }}"><i class="fa fa-image"></i> Event Board</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if(Request::segment(2) == "notification") active @endif " href="{{ route('user.notification.index') }}"><i class="fa fa-bell"></i> Notification</a>
        </li>
        <li class="nav-item">
            <a class="nav-link @if(Request::segment(2) == "profile") active @endif" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i> Logout</a>
                
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>
    </ul>
</div>