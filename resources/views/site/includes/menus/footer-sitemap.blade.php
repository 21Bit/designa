<div class="site-map d-none d-sm-block">
    <div class="container">
        <div class="sections">
            <div class="section">
                <h5>Browse Photos</h5>
                @foreach($data['caters'] as $cater)
                    <a href="#">{{ $cater->name }}</a>
                @endforeach
            </div>
            <div class="section">
                <h5>Find Venues</h5>
                @foreach($data['venue_types'] as $venue)
                    <a href="{{ route('site.venue.index', ['cat' => $venue->slug]) }}">{{ $venue->name }}</a>
                @endforeach
            </div>
            <div class="section">
                <h5>Find Suppliers</h5>
                @foreach($data['suppliers'] as $supplier)
                    <a href="{{ route('site.supplier.index', ['cat' => $supplier->name]) }}">{{ $supplier->display_name }}</a>
                @endforeach
            </div>
            <div class="section">
                <h5>Find Decor</h5>
                @foreach($data['products'] as $product)
                    <a href="{{ route('site.decor.index', ['cat' => $product->slug]) }}">{{ $product->name }}</a>
                @endforeach

            </div>
            <div class="section">
                <h5>Get Help</h5>
                <a href="/for-supplier">For Supplier</a>
            </div>
        </div>

        <div class="text-center">
            
        </div>
    </div>
</div>