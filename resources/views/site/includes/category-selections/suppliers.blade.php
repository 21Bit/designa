<ul>
    @foreach($data as $data)
    <li>
        <div class="custom-control custom-checkbox mb-1">
            <input type="checkbox" class="custom-control-input" value="{{ $data->id }}" id="supplier-checkbox-{{ $data->id }}" name="datas[]" >
            <label class="custom-control-label custom-controler-warning" style="margin-top:2px" for="supplier-checkbox-{{ $data->id }}">{{ $data->display__name }}</label>
        </div>
    </li>
    @endforeach
</ul>