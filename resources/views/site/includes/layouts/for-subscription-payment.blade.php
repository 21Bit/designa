<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('/css/app.css')}}">
    <title>@yield('page-title', config('app.name')) - {{ config('app.name') }}</title>
    <link rel="icon" href="/images/logo.ico">
    @yield('page-meta')
</head>
<body>
    <div class="bg-white p-3">
        <div class="container">
            <div class="pb-2">
                <a href="/">
                    <img src="{{ asset('/images/designa-logo-sm.png') }}" class="mw-100" style="width: 120px;" style="" alt="Designa logo">
                </a>
            </div>
        </div>
    </div>
    @yield('content')
</div>
<script src='/js/app.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
@stack('scripts')

</body>
</html>