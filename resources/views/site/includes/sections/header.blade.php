<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="/images/logo.ico">
    @yield('page-meta')

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/app.css">
    <title>@yield('page-title', config('app.name')) - {{ config('app.name') }}</title>
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1722738,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-EV8CHRVEMX"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'G-EV8CHRVEMX');
    </script>

    

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window,document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');

        fbq('init', '288580559191040');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1"
        src="https://www.facebook.com/tr?id=288580559191040&ev=PageView
        &noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->

    @stack('styles')
</head>
<body>
<div id="app">
    <inquiery-box></inquiery-box>
    <div style="background-color:#fff; border-bottom: 1px solid #e6e6e6;">
        <div class="container">
            <div class="d-flex w-100 p-2" >
                <div class="w-25 text-right" >
                    <a href="/">
                    <img src="/site/images/logo.svg" style="width: 155px;height: 60px; padding: 5px 10px 5px 0px;" class="mw-100" alt="Designa logo">
                    </a>
                </div>
                <div class="w-50 p-2">
                   <main-search-component></main-search-component>
                </div>
                <div class="w-25  header-links">
                    @if(Auth::user())
                        {{-- <profile-dropdown></profile-dropdown> --}}
                        @if(Auth::user()->type == "supplier" || Auth::user()->type == "administrator")
                            <a href="{{ route('dashboard.home') }}" class="mr-2">DASHBOARD</a>
                        @else
                            <a href="{{ route('user.profile.index') }}" class="mr-2">Profile</a>
                        @endif
                    @else
                        <a href="#" class="mr-2 openModalLogin">LOGIN</a>
                        <a href="#" class="mr-2 openModalRegister" >SUPPLIER</a>
                    @endif
                </div>
            </div>
        </div>
    </div>


