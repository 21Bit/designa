<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('/css/app.css')}}">
    <title>@yield('page-title', config('app.name')) - {{ config('app.name') }}</title>
    <link rel="icon" href="/images/logo.ico">
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-EV8CHRVEMX" type="c3a6e93014f366131615fadf-text/javascript"></script>
    <script type="c3a6e93014f366131615fadf-text/javascript">
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'G-EV8CHRVEMX');
    </script>
    @yield('page-meta')
</head>
<body>
    <div class="bg-white p-3">
        <div class="container">
            <div class="pb-2 text-center">
                <a href="/">
                    <img src="{{ asset('/images/designa-logo-sm.png') }}" class="mw-100" style="width: 120px;" style="" alt="Designa logo">
                </a>
            </div>
        </div>
    </div>

