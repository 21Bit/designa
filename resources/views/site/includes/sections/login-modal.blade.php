<!-- Modal -->
<div class="modal fade"  id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="text-right pr-2">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div id="login-tab">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item mw-50">
                    <a class="nav-link active" id="login-tab" data-toggle="tab" href="#login" role="tab" aria-controls="login"
                        aria-selected="true">LOGIN</a>
                </li>
                <li class="nav-item mw-50">
                    <a class="nav-link" id="register-tab" data-toggle="tab" href="#register" role="tab" aria-controls="register"
                        aria-selected="false">REGISTER</a>
                </li>
            </ul>
            <div class="tab-content p-3 pt-4" id="myTabContent" >
                <div class="tab-pane fade show active" id="login" role="tabpanel" aria-labelledby="login-tab">
                    <div>
{{--                        <div class="socials mb-2">--}}
{{--                            <div class="d-flex">--}}
{{--                                <div class="w-50 p-1">--}}
{{--                                    <button class="btn btn-primary btn-block bg-fb"><i class="fa fa-facebook"></i> Login with Facebook</button>--}}
{{--                                </div>--}}
{{--                                <div class="w-50 p-1">--}}
{{--                                    <button class="btn btn-outline-dark btn-block"><i class="fa fa-google"></i> Login with Google</button>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="text-center p-3">--}}
{{--                            <small>- OR -</small>--}}
{{--                        </div>--}}
                        <modal-login-component route="{{ route('login.web') }}"></modal-login-component>
                    </div>
                </div>
                <div class="tab-pane fade" id="register" role="tabpanel" aria-labelledby="register-tab">
                    <div class="text-center rounded p-4 register-type-box">
                        <h4>Which describes you best?</h4>
                        <div class="d-flex mt-5">
                            <div class="w-50">
                                <a href="{{ route('site.register.supplier') }}" class="register-type-icon-link">
                                    <div class="icon mb-3">
                                        <i class="fa fa-briefcase"></i>
                                    </div>
                                    <label for="">Supplier/Vendor</label>
                                </a>
                            </div>
                            <div class="w-50">
                                <a href="{{ route('site.register.customer') }}" class="register-type-icon-link">
                                    <div class="icon mb-3">
                                        <i class="fa fa-user-circle-o"></i>
                                    </div>
                                    <label for="">Customer</label>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
@if(Request::get('intent') == 'login')
    @push('scripts')
    <script>
        $('#loginModal').modal('show')
        $('#loginModal li > a[href="#login"]').tab("show")
    </script>
    @endpush

@elseif(Request::get('intent') == 'register')
    @push('scripts')
    <script>
        $('#loginModal').modal('show')
        $('#loginModal li > a[href="#register"]').tab("show");
    </script>
    @endpush
@endif
