      <footer>
        @include('site.includes.menus.footer-sitemap')
        <div class="bottom">
          <div class="container-fluid">
            <div class="row">
              <div class="col-sm-6 text-left">
                {{ config("app.name") }}
              </div>
              <div class="col-sm-6 text-right">
                <nav class="links">
                  <a href="{{ route('site.term-condition') }}">Terms and Conditions</a>
                  <a href="{{ route('site.privacy-policy') }}">Privacy Policy</a>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </footer>
    </div>
    <script src='/js/app.js'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    @stack('scripts')

  </body>
</html>