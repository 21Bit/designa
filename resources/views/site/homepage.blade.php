@extends('site.includes.layouts.full')

@section('page-title', 'Home')

@section('page-meta')
    <meta name="description" content="Connect with the wedding and events industry. Showcase your products and services. Increase your network and find new opportunities. Designa Studio is the leading online community where inspirational images become real life events. Join today and get rewarded!">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Designa Studio">
    <meta property="og:description" content="Connect with the wedding and events industry. Showcase your products and services. Increase your network and find new opportunities. Designa Studio is the leading online community where inspirational images become real life events. Join today and get rewarded!">
    <meta property="og:image" content="{{ url('/site/images/hompage/logo.svg')}}">
    <meta property="og:url" content="{{ url('/') }}">
@endsection

@section('cover')
@endsection

@section('content')
<div class="homepage">
  <section class="home-new-banner text-center">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="home-banner-details">
                    <h2>Creating your event in 3D is almost here!</h2>
                    <h5>Is Your Busines Ready?</h5>
                    @auth
                        <button onclick="window.location.href='{{ route('dashboard.baselayer.index') }}'" class="btn-3d">Start 3D Now</button>
                    @else
                        <button class="btn-3d openModalLogin">Start 3D Now</button>
                    @endauth
                </div>
                <div class="phone-box">
                    <img src="/site/images/homepage/phone.png" alt="" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
  </section>

  <section class="new-events">
    <div class="container">
        <h2 class="new-title">Beautiful Events Designed by You</h2>
        <div class="row new-events-box">
            <div class="col-lg-3">
                <div class="image-border">
                    <img src="/site/images/homepage/get-inspired.png" alt="" class="img-fluid">
                </div>
                <h5>Get inspired and start to create event</h5>
            </div>
            <div class="col-lg-3">
                <div class="image-border">
                    <img src="/site/images/homepage/shortlist.png" alt="" class="img-fluid">
                </div>
                <h5>Shortlist your favorite venues, suppliers, and decor</h5>
            </div>
            <div class="col-lg-3">
                <div class="image-border">
                    <img src="/site/images/homepage/get-in-touch.png" alt="" class="img-fluid">
                </div>
                <h5>Get in touch or request a quote</h5>
            </div>
            <div class="col-lg-3">
                <div class="image-border">
                    <img src="/site/images/homepage/get-expert-advise.png" alt="" class="img-fluid">
                </div>
                <h5>Get expert advise from an experienced event stylist</h5>
            </div>
        </div>
    </div>
  </section>

<!-- inspiration categories -->
  <section class="inspiration-categories">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 mb-4">
                <div class="card">
                    <img src="/site/images/homepage/wedding.png" alt="" class="card-image rounded-0">
                    <div class="card-body p-3">
                        <div class="row">
                            <div class="col-sm-8">
                                <h5 class="mb-0"><a href="{{ route('site.inspiration.index', ['cat' => 'wedding']) }}"> Wedding</a></h5>
                            </div>
                            <div class="col-sm-4 text-right"><small><i class="fa fa-image"></i> {{ $wedding }}</small></div>
                        </div>
                       
                    </div>
                </div>
                <!-- <div class="inspiration-categories-link">
                    <a href="{{ route('site.inspiration.index', ['cat' => 'wedding']) }}">
                        <img src="/site/images/homepage/wedding.png" alt="" class="mw-100">
                        
                        <div class="row">
                            <div class="col-lg-5">
                            <h1>Wedding</h1>    
                            </div>
                            <div class="col-lg-5 posted-by-box">Posted by: White Label Hire</div>
                            <div class="col-lg-7 text-right posted-number"><i class="fa fa-image"></i> {{ $wedding }}</div>
                        </div>
                    </a>
                </div> -->
            </div>
            <div class="col-lg-6 mb-4">
                <div class="card">
                    <img src="/site/images/homepage/corporate.png" alt="" class="card-image rounded-0">
                    <div class="card-body p-3">
                        <div class="row">
                            <div class="col-sm-8">  <h5 class="mb-0"><a href="{{ route('site.inspiration.index', ['cat' => 'corporate']) }}">Corporate</a></h5> </div>
                            <div class="col-sm-4 text-right"><small><i class="fa fa-image"></i> {{ $corporate }}</small></div>
                        </div>
                    </div>
                </div>
<!-- 
                <div class="inspiration-categories-link">
                    <a href="{{ route('site.inspiration.index', ['cat' => 'corporate']) }}">
                        <img src="/site/images/homepage/corporate.png" alt="" class="mw-100">
                        <h4>Corporate</h4>
                        <div class="row">
                            <div class="col-lg-7 text-right posted-number"><i class="fa fa-image"></i> {{ $corporate }}</div>
                        </div>
                    </a>
                </div> -->
            </div>

            <div class="col-lg-6 mb-4">
                <div class="card">
                    <img src="/site/images/homepage/special-events.png" alt="" class="card-image rounded-0">
                    <div class="card-body p-3">
                        <div class="row">
                            <div class="col-sm-8">  <h5 class="mb-0"><a href="{{ route('site.inspiration.index', ['cat' => 'special-events']) }}">Special Events</a></h5> </div>
                            <div class="col-sm-4 text-right"><small><i class="fa fa-image"></i> {{ $special_event }}</small></div>
                        </div>
                    </div>
                </div>
<!-- 
                <div class="inspiration-categories-link">
                    <a href="{{ route('site.inspiration.index', ['cat' => 'special-events']) }}">
                        <img src="/site/images/homepage/special-events.png" alt="" class="img-fluid">
                        <h4>Special Events</h4>
                        <div class="row">
                            <div class="col-lg-7 text-right posted-number"><i class="fa fa-image"></i> {{ $special_event }}</div>
                        </div>
                    </a>
                </div> -->
            </div>
            <div class="col-lg-6 mb-4">
                <div class="card">
                    <img src="/site/images/homepage/cultural.png" alt="" class="card-image rounded-0">
                    <div class="card-body p-3">
                        <div class="row">
                            <div class="col-sm-8">  <h5 class="mb-0"><a href="{{ route('site.inspiration.index', ['cat' => 'cultural']) }}">Cultural</a></h5> </div>
                            <div class="col-sm-4 text-right"><small><i class="fa fa-image"></i> {{ $cultural }}</small></div>
                        </div>
                    </div>
                </div>
                <!-- <div class="inspiration-categories-link">
                    <a href="{{ route('site.inspiration.index', ['cat' => 'cultural']) }}">
                        <img src="/site/images/homepage/cultural.png" alt="" class="img-fluid">
                        <h4>Cultural</h4>
                        <div class="row">
                            <div class="col-lg-7 text-right posted-number"><i class="fa fa-image"></i> {{ $cultural }}</div>
                        </div>
                    </a>
                </div> -->
            </div>
        </div>
    </div>
  </section>
<!-- inspiration categories -->

<!-- venue outdoor -->
  <section class="venue-outdoor">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row venue-outdoor-box ">
                    <div class="col-lg-1 venue-outdoor-image ">
                        <img src="{{ $first_outdoor->company->logo }}" alt="" class="img-fluid">
                    </div>
                    <div class="col-lg-11 venue-outdoor-details">
                       {{  $first_outdoor->name }}
                    </div>
                </div>
                <img src="/site/images/homepage/outdoor.png" alt="" class="img-fluid">

                <div class="row venue-outdoor-count">
                    <div class="col-lg-6">
                        Outdoor
                    </div>
                    <div class="col-lg-6 text-right venue-outdoor-number">
                        <i class="fa fa-image"></i> {{ $outdoor }}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <img src="/site/images/homepage/bar-restaurant.png" alt="" class="img-fluid">
                <div class="row venue-outdoor-count">
                    <div class="col-lg-6">
                        Bar / Restaurant
                    </div>
                    <div class="col-lg-6 text-right venue-outdoor-number">
                        <i class="fa fa-image"></i> {{ $bar_restaurant }}
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <img src="/site/images/homepage/boat-cruises.png" alt="" class="img-fluid">
                <div class="row venue-outdoor-count">
                    <div class="col-lg-6">
                        Boat &amp; Cruises
                    </div>
                    <div class="col-lg-6 text-right venue-outdoor-number">
                        <i class="fa fa-image"></i> {{ $boats_cruises }}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <img src="/site/images/homepage/event-space.png" alt="" class="img-fluid">
                <div class="row venue-outdoor-count">
                    <div class="col-lg-6">
                        Event Space
                    </div>
                    <div class="col-lg-6 text-right venue-outdoor-number">
                        <i class="fa fa-image"></i> {{ $event_space }}
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <img src="/site/images/homepage/destination.png" alt="" class="img-fluid">
                <div class="row venue-outdoor-count">
                    <div class="col-lg-6">
                        Destination
                    </div>
                    <div class="col-lg-6 text-right venue-outdoor-number">
                        <i class="fa fa-image"></i> {{ $destination }}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <img src="/site/images/homepage/hotel.png" alt="" class="img-fluid">
                <div class="row venue-outdoor-count">
                    <div class="col-lg-6">
                        Hotel
                    </div>
                    <div class="col-lg-6 text-right venue-outdoor-number">
                        <i class="fa fa-image"></i> {{ $hotel }}
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <img src="/site/images/homepage/reception.png" alt="" class="img-fluid">
                <div class="row venue-outdoor-count">
                    <div class="col-lg-6">
                        Reception
                    </div>
                    <div class="col-lg-6 text-right venue-outdoor-number">
                        <i class="fa fa-image"></i> {{ $reception }}
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
<!-- venue outdoor -->

<!-- suplier slider -->
  <section class="supplier-slider">
    <div class="container">
        <h4 class="text-center">Featured</h4>
        <h5 class="text-left">Suppliers</h5>
        <div class="text-right">
            <a href="{{ route('site.supplier.index') }}">
                View More
            </a>
        </div>

        <div class="multiple-items">
            @foreach($suppliers as $supplier)
            <div>
                <div style="position: relative;">
                    <div class="row">
                        <div class="col-lg-4 supplier-slider-image">
                            <img src="{{ $supplier->logo  }}" alt="" class="img-fluid rounded-cicle">
                        </div>
                        <div class="col-lg-8 supplier-slider-details">
                            <h6>
                                <a href="{{ route('site.supplier.show', $supplier->slug) }}">
                                    {{ $supplier->name  }}
                                </a>
                            </h6>
                            <p>{{ $supplier->roleList()  }}</p>
                        </div>
                    </div>
                </div>
                <div class="supplier-slider-image2" style="padding-top:63%;background-image:url({{ $supplier->image('cover')->path('covers', 'https://via.placeholder.com/1920x450')  }}); background-size:cover; background-position:center;">
                    {{-- <img src="{{ $supplier->image('cover')->path('covers', 'https://via.placeholder.com/1920x450')  }}" alt="" class="img-fluid"> --}}
                </div>
                <div class="supplier-slider-pin">
                    <i class="fa fa-map-marker" aria-hidden="true"></i> {{ $supplier->town . ', ' . $supplier->country  }}
                </div>
            </div>
            @endforeach
        </div>
    </div>
  </section>
<!-- suplier slider -->

<!-- decor slider -->
  <section class="decor-slider">
    <div class="container">

        <h5 class="text-left">Decor</h5>

        <div class="text-right">
            <a href="{{ route('site.decor.index') }}">
                View More
            </a>
        </div>

        <div class="multiple-items">
            @foreach($decors as $decor)
                <div>
                    <div style="background-image:url('{{ $decor->thumbnail }}'); background-size:cover;background-position: center; padding-top:75%;">

                    </div>
                    {{-- <img src="{{ $decor->thumbnail  }}" alt="" class="img-fluid"> --}}
                    <div class="row decor-slider-details">
                        <div class="col-lg-8">
                            <h6>{{ $decor->name  }}</h6>
                            <p>{{ optional($decor->product_type)->name  }}</p>
                        </div>
                        <div class="col-lg-4" >
                            <img src="{{ optional($decor->company)->logo }}" alt="" style="width: 32px; height:32px;" class="rounded-circle">
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
  </section>
<!-- decor slider -->

<section class="request form" style="background: #f7f2e5; padding-top: 60px; padding-bottom: 60px; font-family: 'Nunito', sans-serif; font-weight: 300;">
  <div class="container">
      <div class="row">
          <div class="col-lg-2"></div>
          <div class="col-lg-4">
              <h4>Request a Demo</h4>
              <p>
                  If you need a bit more help, we would love to arrange a one on one demo.
              </p>
              <p>
                  Our Team are ready to show you the Designa Studio system and take you through all the functions take you through all the functions at your fingertips.
              </p>
          </div>
          <div class="col-lg-4">
            <request-form type="demo"></request-form>
          </div>
          <div class="col-lg-2"></div>
      </div>
  </div>
</section>
</div>


@endsection

@push('styles')
  <!-- Add the slick-theme.css if you want default styling -->
  <link rel="stylesheet" type="text/css" href="https://kenwheeler.github.io/slick/slick/slick.css"/>
  <!-- Add the slick-theme.css if you want default styling -->
  <link rel="stylesheet" type="text/css" href="https://kenwheeler.github.io/slick/slick/slick-theme.css"/>
@endpush

@push('scripts')
<script type="text/javascript" src="https://code.jquery.com/jquery-migrate-1.2.1.min.js
"></script>
<script type="text/javascript" src="https://kenwheeler.github.io/slick/slick/slick.js"></script>
<script type="text/javascript">
    $('.multiple-items').slick({
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 3,
      arrows: true,
      dots: true,   
      prevArrow: null,
      nextArrow: null,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            arrows: false,
            slidesToScroll: 1,
            slidesToShow: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            arrows: false,
            slidesToScroll: 1,
            slidesToShow: 1
          }
        }
      ]
    });
</script>
@endpush
