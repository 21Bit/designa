<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Designa Studio</title>   
  
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">

  <link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />

  <style type="text/css">
    header {
      background-image: url('/site/images/event-registration-banner.png');
      background-size:  cover;
      background-position: center;
      background-repeat: no-repeat;
      min-height: 213px;
    }

    .events {
      padding-top: 50px;
      text-align: center;
    }

    h1 {
      color: #ff9a1f;
      font-size: 30px;
    }

    p.event-details {      
      padding-top: 10px;
      padding-bottom: 30px;
    }

    .btn-add-details {
      padding-left: 35px;
      padding-right: 35px;
      background: #ff9a1f;
      border: 1px solid #ff9a1f;
      border-radius: 7px;
      color: #fff;      
    }

    .col-right {
      text-align: right;
    }

    .event-box {
      background: #f6f6f6; 
      padding: 30px 50px;
    }

    .list-add {
      padding-top: 30px;
    }

    .list-box {
      padding-top: 10px;
    }

    .check-circle {
      color: #ff9a1f; 
      font-size: 17px; 
      padding-right: 9px;
    }

    .submit-box {
      padding-top: 40px; 
      padding-bottom: 40px;
    }

    h2 {
       text-align: left; 
       font-size: 20px; 
       color: #ff9a1f
    }

    /* Portrait */
    @media  only screen 
      and (min-device-width: 375px) 
      and (max-device-width: 667px) 
      and (-webkit-min-device-pixel-ratio: 2)
      and (orientation: portrait) { 
      .col-right {
          text-align: center !important; 
          padding-top: 10px !important;         
      }

      .event-box input {
        margin-bottom: 13px !important;
      }  
    }

  </style>
</head>

<body>

  <main id="app">
    <header></header>
    <div class="container events">
        <h1> Event Registration Form</h1>
        <p class="event-details">
          Please register all guest details (including your own) below that will be attending the Designa Exclusive Preview.
        </p>

        <event-registration-form></event-registration-form>

    </div>
  </main>
  <script src="/js/eventregistration.js"></script>
</body>
</html>