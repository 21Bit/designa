<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Designa Studio</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <meta name="Designa Studio" content="app-id=com.globalenablement.designa, affiliate-data=designa-app, app-argument=https://apps.apple.com/us/app/designa-studio/id1495137130?ls=1">
    <script>
        // window.onload = function() {
        // <!-- Deep link URL for existing users with app already installed on their device -->
        //     window.location = "{{ $appLink }}";
        // <!-- Download URL (TUNE link) for new users to download the app -->
        //     setTimeout("window.location = '{{ $appLink }}';", 1000);
        // } 
    </script>
</head>
<body style="background-image:url(/img/landing-banner.png); background-repeat: no-repeat; background-size: cover;">
    <div class="bg-white p-3">
        <img src="/site/images/designa-logo.png" class="mw-100">
    </div>
    <div class="d-flex align-items-center">
        <div class="container mb-5 pt-3">
        
            <div class="row align-items-center">
                <div class="col-xl-7 col-md-6">
                    <div class="text-white ">
                        <h3 class="wow fadeInDown text-center mb-3 mt-2">Congratulations! </h3>
                        {{-- <p class="wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".1s">Get more ideas and inspirations for your next event.</p> --}}
                        <p class="h4 text-center mb-3">
                                Your profile is now live. To view all the content, login on desktop or view as a user on the mobile app.
                        </p>
                     
                        
                        <div class="mt-5 text-center pl-5 pr-5">
                            <a href="{{ $appLink }}" class="mr-3 mb-3" target="_blank">
                                @if($agent->is('iPhone'))
                                    <img src="/site/images/app-store.png" alt="" class="mw-100">
                                @else
                                    <img src="/site/images/google-play.png" alt="" class="mw-100">
                                @endif
                            </a>
                            {{-- <a href="https://play.google.com/store/apps/details?id=com.globalenablement.designa" class="mr-3 mb-3">
                                <img src="/img/play.svg" alt="">
                            </a> --}}
                            <!-- <a href="/home" class="btn btn-lg btn-outline-light">
                                Continue to Website 
                            </a> -->
                        </div>
                    </div>
                </div>
                <div class="col-xl-5 col-md-6 mt-2 pt-5">
                   <img src="/img/mobile.png" alt="" class="mw-100">
                </div>
            </div>
        </div>
    </div>
</body>
</html>