
@extends('site.includes.layouts.no-menu')

@section('page-title', 'Supplier Registration')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-8 offset-sm-2">
                <div class="bg-white p-3 rounded mt-5 mb-5 supplier-register-box">
                    {{-- <div class="mb-2 text-center">
                        <small>Tell us about your business. (Not a professional? <a href="/">Continue to Designa</a>)</small>
                    </div> --}}
                    <div class="row">
                        <div class="col-sm-12">
                              @if(Session::has('message'))
                                    <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-dismissible mt-1 mb-3">
                                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                          {{ Session::get('message') }}
                                    </div>
                              @endif

                              <form action="{{ route('site.supplier.assistance.form.post') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <h3 class="text-center">Business Profile</h3> 
                                    <h5>Personal Details</h5>            
                                    <div class="form-group mb-4">
                                          <label for="fname">Full Name</label>
                                          <input type="text" required class="form-control" id="full_name" name="name">
                                    </div>
                                    <div class="form-row mb-2">
                                          <div class="form-group mb-4 col-md-6">
                                                <label for="email">Email Address</label>
                                                <input type="email" required class="form-control" id="email" name="email">
                                          </div>
                                          <div class="form-group mb-4 col-md-6">
                                                <label for="mobile">Mobile Number</label>
                                                <input type="text" required class="form-control" id="mobile" name="mobile_number">
                                          </div>
                                    </div>

                                    <h5>Business Details</h5>
                                    <div class="form-group mb-4">
                                          <label for="bname">Businesss Name</label>
                                          <input type="text" required class="form-control" id="bname" name="business_name">
                                    </div>
                                    <div class="form-group mb-4">
                                          <label for="bname">Businesss Address</label>
                                          <input type="text" required class="form-control" id="bname" name="business_address">
                                    </div>
                                    <div class="form-group mb-4">
                                          <label for="bname">Brief Businesss Description. If you already have this on your website, then please provide the URL address </label>
                                          <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="3"></textarea>
                                    </div>
                                    <div class="checklist">
                                          <label>Which categories does your business fit into (it can be more than one).</label>
                                    <div class="row mb-4">
                                          <div class="col-lg-6">
                                                <div class="form-check">              
                                                      <input class="form-check-input" type="checkbox" value="Wedding" name="categories[]" id="wedding">
                                                      <label class="form-check-label" for="wedding">
                                                      Wedding
                                                      </label>
                                                </div>
                                                <div class="form-check">              
                                                      <input class="form-check-input" type="checkbox" value="Corporate"  name="categories[]"  id="Corporate">
                                                      <label class="form-check-label" for="Corporate">
                                                      Corporate
                                                      </label>
                                                </div>
                                          </div>
                                          <div class="col-lg-6">
                                                <div class="form-check">              
                                                      <input class="form-check-input" type="checkbox" value="Special Occasion"  name="categories[]"  id="Special">
                                                      <label class="form-check-label" for="Special">
                                                      Special Occasion
                                                      </label>
                                                </div>
                                                <div class="form-check">              
                                                      <input class="form-check-input" type="checkbox" value="Cultural"  name="categories[]"  id="Cultural">
                                                      <label class="form-check-label" for="Cultural">
                                                      Cultural
                                                      </label>
                                                </div>
                                          </div>
                                    </div>
                                    </div>
                                    <div class="form-group mb-4">
                                          <label for="exampleFormControlFile1">Business Profile Image</label>
                                          <input type="file" name="business_profile_image" required  class="form-control-file" id="exampleFormControlFile1">
                                    </div>
                                    <div class="form-group mb-4">
                                          <label for="exampleFormControlFile1">Business Banner Image <small><i>(recomended size is 1920x450)</i></small></label>
                                          <input type="file" class="form-control-file" required name="business_cover_image" id="exampleFormControlFile1">
                                    </div>
                                    <div class="form-group mb-4">
                                          <label for="exampleFormControlFile1">Business Logo</label>
                                          <input type="file" class="form-control-file" required name="business_logo" id="exampleFormControlFile1">
                                    </div>
                                    <div class="form-group mb-4">
                                          <label for="exampleFormControlFile1">Specific areas you service</label>
                                          <input type="text" class="form-control" required name="areas_of_services" id="exampleFormControlFile1" placeholder="Sydney, Melbourne">
                                    </div>
                                    <div class="form-group mb-4">
                                          <label for="exampleFormControlFile1">Type of service you offer</label>              
                                          <textarea class="form-control" required id="exampleFormControlTextarea1" name="offers" rows="3" placeholder="Delivery, Set Up, Pack Down, On Day Coordination, Special Event Planning"></textarea>
                                    </div>
                                    <div class="form-group mb-4">
                                          <label for="exampleFormControlFile1">Any achievements or industry recognition you may have received</label>
                                          <input type="text" class="form-control" name="achievements" id="exampleFormControlFile1">
                                    </div>            
                                    <div class="text-center button-box">
                                          <button type="submit" class="btn btn-primary btn-lg">Submit</button>       
                                    </div>

                             
                                
                              </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    

@endsection