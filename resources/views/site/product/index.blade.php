@extends('site.includes.layouts.full')

@section('page-title', 'Decors')

@section('content')
    <div class="page-header-img mb-4">
        <h1 class="text-orange">Décor</h1>
        <img class="bg-image" src="/site/images/decor.png" alt="">
        <img class="bottom-pattern" src="/site/images/bottom-pattern.png" alt="">
    </div>
    
    <product-filter-component besupplier="true" default-category="{{ $defaultCategory }}" default-category-parent="{{ $defaultCategoryParent }}" ></product-filter-component>

    <div class="mt-4"></div>
@endsection
