<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Designa Studio</title>
        <link rel="icon" type="image/x-icon" href="https://designa.studio/images/logo.ico" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link href="/landing-page01/css/styles.css" rel="stylesheet" />
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Spartan:wght@400;500;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <meta name="description" content="$1000 plus a great away against the best stylist and planners from across Australia">
        <meta property="og:type" content="website">
        <meta property="og:title" content="Exhibit your talent in a digital creative scene">
        <meta property="og:description" content="$1000 plus a great away Compete against the best stylist and planners from across Australia">
        <meta property="og:image" content="{{ url('landing-page01/img/styling-banner.png') }}">
        <meta property="og:url" content="{{ route('landing.contest-registration') }}">
        <!-- Hotjar Tracking Code For Content Registration -->
        <script>
            (function(h,o,t,j,a,r){
                h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                h._hjSettings={hjid:2559195,hjsv:6};
                a=o.getElementsByTagName('head')[0];
                r=o.createElement('script');r.async=1;
                r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                a.appendChild(r);
            })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
        </script>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-EV8CHRVEMX"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'G-EV8CHRVEMX');
        </script>
    </head>
    <body>
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-white bg-white fixed-top" id="mainNav">
            <div class="container px-4">
                <a class="navbar-brand"><img src="landing-page01/img/designa-logo.png" alt="Designa Studio" class="logo"></a>
                <button class="navbar-toggler navbar-light" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ms-auto">
                        <li class="nav-item"><a class="nav-link" href="#about3">about</a></li>
                        <li class="nav-item"><a class="nav-link" href="#winhowitworks">how it works</a></li>
                        <li class="nav-item"><a class="nav-link" href="#finerpoint">the finer point</a></li>
                        <li class="nav-item"><a class="nav-link btn-try" href="#about3">JOIN NOW</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Header-->
        <header class="sofitel-banner text-white" id="about3">
            <div class="container px-4 text-center">
                <div class="row gx-4">
                    <div class="col-lg-6 win-top">
                        <h1 class="fw-bolder">WIN</h1>
                        <div class="win-box">
                            <h2>$1000 plus a Sydney Getaway</h2>
                            <h3>Compete against the best stylist and planners from across Australia</h3>
                        </div>
                    </div>
                    <div class="col-lg-6 win-form" id="app">
                        <div class="win-form-box">
                            <h2>Exhibit your talent in a digital creative scene</h2>
                            <contest-3d-registration-form />
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <!-- About section-->
        <section class="bg-white about4">
            <div class="container px-4">
                <div class="row gx-4 text-center marketing-space">
                    <div class="col-lg-4">
                        <div class="plan-box4">
                            <img src="/landing-page01/img/be-dynamic-icon.png" alt="" class="img-fluid">
                            <h6 class="text-black">Be Dynamic</h6>
                            <ul>
                                <li>Designa & Sofitel Sydney Wentworth connects both the hotel and event professionals with unparalleled freedom in event design.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="plan-box4 about-space">
                            <img src="/landing-page01/img/be-bold-icon.png" alt="" class="img-fluid">
                            <h6 class="text-black">Be Bold</h6>
                            <ul>
                                <li>Express yourself and your creative prowess with Designa's digital event styling platform. Unleash your talent!</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="plan-box4 about-space">
                            <img src="/landing-page01/img/be-inspired-icon.png" alt="" class="img-fluid">
                            <h6 class="text-black">Be Inspired</h6>
                            <ul>
                                <li>Big ideas and real-world learning. Battle with the industry's finest event stylists and stay inspired. It all happens on one digital platform.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Benefits section-->
        <section class="win-section">
            <div class="win-banner"></div>
        </section>

        <!-- How It Works section-->
        <section id="winhowitworks" class="bg-light">
            <div class="container px-4">
                <div class="row gx-4">
                    <div class="col-lg-6">
                        <img src="landing-page01/img/win-how-it-works-banner.png" class="img-fluid" alt="">
                    </div>
                    <div class="col-lg-6 win-how-it-works-details">
                        <h2 class="text-center">How it works</h2>
                        <ul>
                            <li>
                                <i class="fa fa-check-circle fa-2x" aria-hidden="true"></i>
                                <div><span>Step 1.</span> Want to show off your styling skills? Register your interest until September 30.</div>
                            </li>
                            <li>
                                <i class="fa fa-check-circle fa-2x" aria-hidden="true"></i>
                                <div><span>Step 2.</span> Join our online webinar, where we show you the tips and tricks on using Designa to maximise your design. We will also be providing online help throughout the competition.</div>
                            </li>
                            <li>
                                <i class="fa fa-check-circle fa-2x" aria-hidden="true"></i>
                                <div><span>Step 3.</span> We send you the brief; you do the rest. Time to unbox your ideas and start designing your event. You will have 14 days to finish your 3D event.</div>
                            </li>
                            <li>
                                <i class="fa fa-check-circle fa-2x" aria-hidden="true"></i>
                                <div><span>Step 4.</span> Submit your masterpiece before October 14.  A panel of expert judges will select a winner. Good luck!</div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>


       <!-- The Finer Point section-->
        <section id="finerpoint" class="bg-white">
            <div class="container px-4">
                <div class="row gx-4">
                    <div class="col-lg-6">
                        <h2 class="text-center">The Finer Points</h2>
                        <ul>
                            <li>
                                <img src="/landing-page01/img/finer-participants-icon.png" alt="" class="img-fluid">
                                <div>Participants must pre-register via Designa website. The deadline for registration is September 30, 2021, at 11:59 PM.</div>
                            </li>
                            <li>
                                <img src="/landing-page01/img/finer-competition-icon.png" alt="" class="img-fluid">
                                <div>The competition is open to all stylists, individual or company representatives.</div>
                            </li>
                            <li>
                                <img src="/landing-page01/img/finer-plan-icon.png" alt="" class="img-fluid">
                                <div>Plan, design, and style Sofitel Sydney Wentworth newly refurbished Wentworth Ballroom using Designa's 3D platform.</div>
                            </li>
                            <li>
                                <img src="/landing-page01/img/finer-create-icon.png" alt="" class="img-fluid">
                                <div>You can create designs as many as you want, following a luxurious wedding as the theme.</div>
                            </li>
                            <li>
                                <img src="/landing-page01/img/finer-judge-icon.png" alt="" class="img-fluid">
                                <div>Sofitel Sydney Wentworth will judge entries, and the winner will be announced on October 14, 2021.</div>
                            </li>
                            <li>
                                <img src="/landing-page01/img/finer-winner-icon.png" alt="" class="img-fluid">
                                <div>The winner will receive $1,000 plus One Night Stay in a Luxury Room with Breakfast for two at Sydney's most prestigious hotel.</div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-6 text-center">
                        <div class="embed-responsive embed-responsive-16by9">
                            <video controls poster="/landing-page01/img/sofitel-video-poster.png" class="sofitel-videocol img-fluid">
                                <source src="landing-page01/img/sofitel-walkthrough-video.mp4" type="video/mp4">
                              Your browser does not support HTML5 video.
                            </video>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <!-- Benefits section-->
        <section class="styling-skills">
            <div class="styling-banner image-text-center">
                <h1>Bring out the best in your event styling skills.</h1>
                <a  href="#about3"><button type="submit" class="btn-signup2">JOIN NOW</button></a>
            </div>
        </section>

        <!-- Footer-->
        <footer class="py-5 bg-white">
            <div class="container px-4">
                <div class="row gx-4 justify-content-center">
                    <div class="col-lg-12 text-center">
                        <div class="social-circle">
                            <a href="https://www.facebook.com/Designa-Studio-260657538157535/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        </div>
                        <div class="social-circle">
                            <a href="https://www.linkedin.com/company/designastudio/" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                        </div>
                        <div class="social-circle">
                            <a href="https://www.instagram.com/designastudio.au/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        </div>
                        <p style="font-size: 14px; padding-top: 20px;">
                            Follow us to receive instant update<br>
                            <a href="mailto:hello@designa.studio" style="color: #4E4E4E;">hello@designa.studio</a>
                        </p>
                    </div>
                </div>
            </div>
        </footer>



    <script>
    var video = document.getElementById("je-video");
    var btn = document.getElementById("myBtn");

    function myFunction() {
      if (video.paused) {
        video.play();
        btn.innerHTML = "Pause";
      } else {
        video.pause();
        btn.innerHTML = "Play";
      }
    }
    </script>

    <!-- end video -->




        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="/landing-page01/js/scripts.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous" type="3fdf24a46cbb971ce9cc62d4-text/javascript"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous" type="3fdf24a46cbb971ce9cc62d4-text/javascript"></script>
        <script src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js" data-cf-settings="3fdf24a46cbb971ce9cc62d4-|49" defer=""></script>
        <script src="/js/app.js"></script>
    </body>
</html>
