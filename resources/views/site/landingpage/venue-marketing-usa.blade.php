<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Designa Studio</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="icon" type="image/x-icon" href="https://designa.studio/images/logo.ico" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link href="/landing-page01/css/styles.css" rel="stylesheet" />
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>

        <meta name="description" content="Impress clients and gain high-quality leads.">
        <meta property="og:type" content="website">
        <meta property="og:title" content="Experience the magic of 3D venue tours">
        <meta property="og:description" content="Impress clients and gain high-quality leads.">
        {{-- <meta property="og:image" content="{{ url($inspiration->thumbnail) }}"> --}}
        <meta property="og:image" content="{{ url('landing-page01/img/banner-usa01.png') }}">
        <meta property="og:url" content="{{ route('landing.venue-marketing-usa') }}">

        <link href="https://fonts.googleapis.com/css2?family=Spartan:wght@400;500;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

         <!-- Hotjar Tracking Code -->
        <script>
            (function(h,o,t,j,a,r){
                h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                h._hjSettings={hjid:1744893,hjsv:6};
                a=o.getElementsByTagName('head')[0];
                r=o.createElement('script');r.async=1;
                r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                a.appendChild(r);
            })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
        </script>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-EV8CHRVEMX"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'G-EV8CHRVEMX');
        </script>

        <script type="application/javascript">
            (function(b,o,n,g,s,r,c){if(b[s])return;b[s]={};b[s].scriptToken="XzQxNTUxODEzNw";b[s].callsQueue=[];b[s].api=function(){b[s].callsQueue.push(arguments);};r=o.createElement(n);c=o.getElementsByTagName(n)[0];r.async=1;r.src=g;r.id=s+n;c.parentNode.insertBefore(r,c);})(window,document,"script","https://cdn.oribi.io/XzQxNTUxODEzNw/oribi.js","ORIBI");
        </script>
    </head>
    <body>
      <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-white bg-white fixed-top" id="mainNav">
            <div class="container px-4">
                <a class="navbar-brand"><img src="/landing-page01/img/designa-logo.png" alt="Designa Studio" class="logo"></a>
                <button class="navbar-toggler navbar-light" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ms-auto">
                        <li class="nav-item"><a class="nav-link" href="#about3">about</a></li>
                        <li class="nav-item"><a class="nav-link" href="#benefits2">benefits</a></li>
                        <li class="nav-item"><a class="nav-link" href="#comparison">comparison</a></li>
                        <li class="nav-item"><a class="nav-link" href="#contact">contact</a></li>
                        <li class="nav-item"><a class="nav-link btn-try" href="#register">Register Now</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Header-->
        <header class="landing-page-banner1 text-white" id="about3">
            <div class="container px-4 text-center">
                <h1 class="fw-bolder header-intro">Experience the magic of 3D venue tours</h1>
                <p class="lead">Impress clients and gain high-quality leads</p>
            </div>
        </header>
        <!-- About section-->
        <section class="bg-white about">
            <div class="container px-4">
                <div class="row gx-4 justify-content-center">
                    <div class="col-lg-12">
                        <h2 class="text-center">Venue marketing made simple</h2>
                    </div>
                </div>

                <div class="row gx-4 text-center marketing-space">
                    <div class="col-lg-4">
                        <div class="plan-box3">
                            <img src="/landing-page01/img/accurate-3d.png" alt="" class="img-fluid">
                            <h6 class="text-black">Accurate 3D floor plans</h6>
                            <ul>
                                <li>Floor plans are extremely useful to event planners. A planner can begin mentally mapping their event onto your area by just presenting your floor plans.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="plan-box3 about-space">
                            <img src="/landing-page01/img/stunning-3d.png" alt="" class="img-fluid">
                            <h6 class="text-black">Stunning 3D visuals</h6>
                            <ul>
                                <li>Would you book a venue if the visuals were not compelling? Showcase your space with stunning professional 3D highlighting some unique features that make it more than just possible.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="plan-box3 about-space">
                            <img src="/landing-page01/img/3d-video.png" alt="" class="img-fluid">
                            <h6 class="text-black">3D video walkthroughs</h6>
                            <ul>
                                <li>Virtual tours allow you to show planners your space without them ever stepping foot into it, taking the experience to the next level.</li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row gx-4 text-center marketing-space">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-4">
                        <div class="plan-box3 about-space2">
                            <img src="/landing-page01/img/available-anytime.png" alt="" class="img-fluid">
                            <h6 class="text-black">Available Anytime, Anywhere</h6>
                            <ul>
                                <li>Present your venue on every device for easy access for your clients.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="plan-box3 about-space">
                            <img src="/landing-page01/img/get-more-leads.png" alt="" class="img-fluid">
                            <h6 class="text-black">Get More Leads</h6>
                            <ul>
                                <li>Get that momentum with a steady stream of quality leads and best navigate them through an innovative online hub for the event industry.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-2"></div>
                </div>
            </div>
        </section>
        <!-- Benefits section-->
        <section class="bg-light" id="benefits2">
            <div class="container px-4">
                <div class="row gx-4 justify-content-center">
                    <div class="col-lg-12 text-center">
                        <h2>Be the leader in your industry by pre-registering now.</h2>
                        <p style="font-weight: bold;">Get a 3D venue. Event professional? Then Designa is for you. Design your event in 3D.</p>
                        <p class="benefits-space"><a href="#register"><button type="button" class="btn-signup2">REGISTER NOW</button></a></p>
                    </div>
                </div>
            </div>
            <div class="benefits-banner"></div>
        </section>

        <!-- Comparison section-->
        <section class="bg-white" id="comparison">
            <div class="container px-4">
                <h2 class="text-center">What sets us apart?</h2>
                <div class="row gx-4">
                    <div class="col-lg-4">
                        <div class="comparison-box">
                            <h6 style="padding-top: 27px;">Why Choose Designa?</h6>
                            <ul>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>See your event in 3D instantly</div></li>
                                <li><i class="fa fa-check-square-o check-benefits" aria-hidden="true"></i> <div>Shortlist your favorite venues, suppliers, and planners</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Boost your brand with Designa</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>User-friendly interface</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Easy to find inspirations</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Provides ultra-realistic floorplan</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Accepts add-on services</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Create a digital catalogue</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Compatible with Web, Android and iOS mobile application</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Get expert advice from professionals</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>An inbuilt quoting system</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Live chat with prospective clients</div></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="comparison-box">
                            <h6>What do you get with online directory services?</h6>
                            <ul>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Irrelevant leads</div></li>
                                <li><i class="fa fa-check-square-o check-benefits" aria-hidden="true"></i> <div>Complex structure for users</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Expensive subscription plan</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>No control to filter trolls</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Hidden costs</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Low ROI</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Increased clutter</div></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="comparison-box">
                            <h6>What to do you get with venue finding service?</h6>
                            <ul>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Grouped with irrelevant services</div></li>
                                <li><i class="fa fa-check-square-o check-benefits" aria-hidden="true"></i> <div>Vague information</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>One size fits all offering</div></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row gx-4">
                    <div class="col-lg-12 text-center transform-box2">
                        <h2>Watch Designa transform your event in 30 seconds</h2>
                        <div class="video-section video-section-bg">
                            <div class="video-wrapper">
                                <video controls poster="/landing-page01/img/video-poster.png" id="je-video">
                                    <source src="/landing-page01/img/video-designa.mp4" type="video/mp4">
                                  Your browser does not support HTML5 video.
                                </video>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div id="app">
        <!-- Register section-->
        <section class="bg-white register-banner" id="register">
            <div class="container px-4">
                <div class="row gx-4 justify-content-center">
                    <div class="col-lg-6">
                    </div>
                    <div class="col-lg-6 text-center bg-white register-box">
                        <h2>Become a market leader by jumping in early</h2>
                        <p style="font-weight: bold; padding-top: 10px;">Create visually stunning events in minutes.</p>
                        <p style="font-size: 13px;">Register your interest today and be notified when once we start in your area. If you're an event professional then Designa is for you.</p>
                        <venue-marketing-usa-form url="{{ route('landing.venue-marketing-usa-send-email') }}"></venue-marketing-usa-form>
                    </div>
                </div>
            </div>
        </section>

        <!-- Contact section-->
        <section id="contact" class="bg-light">
            <div class="container px-4">
                <div class="row gx-4 justify-content-center">
                    <div class="col-lg-8 text-center">
                        <h2>Need more information</h2>
                        <p class="lead">Request a demo now</p>
                        <request-form-covid></request-form-covid>
                    </div>
                </div>
            </div>
        </section>
        </div>

        {{-- end of app --}}
        <!-- Footer-->
        <footer class="py-5 bg-white">
            <div class="container px-4">
                <div class="row gx-4 justify-content-center">
                    <div class="col-lg-12 text-center">
                        <div class="social-circle">
                            <a href="https://www.facebook.com/Designa-Studio-260657538157535/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        </div>
                        <div class="social-circle">
                            <a href="https://www.linkedin.com/company/designastudio/" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                        </div>
                        <div class="social-circle">
                            <a href="https://www.instagram.com/designastudio.au/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        </div>
                        <p style="font-size: 14px; padding-top: 20px;">
                            Follow us to receive instant update<br>
                            <a href="mailto:hello@designa.studio" style="color: #4E4E4E;">hello@designa.studio</a>
                        </p>
                    </div>
                </div>
            </div>
        </footer>
     <!-- Bootstrap core JS-->
     <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
     <!-- Core theme JS-->
     <script src="/landing-page01/js/scripts.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous" type="3fdf24a46cbb971ce9cc62d4-text/javascript"></script>
     <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous" type="3fdf24a46cbb971ce9cc62d4-text/javascript"></script>

     <script src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js" data-cf-settings="3fdf24a46cbb971ce9cc62d4-|49" defer=""></script>
     <script src="/js/app.js"></script>
    </body>
</html>
