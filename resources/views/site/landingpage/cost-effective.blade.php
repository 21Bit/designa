<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Cost Effective</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="icon" type="image/x-icon" href="https://designa.studio/images/logo.ico" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link href="/landing-page01/css/styles.css" rel="stylesheet" />
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>


        <meta name="description" content="Cut all those unnecessary stuff and go straight down to the nitty-gritty of your business.">
        <meta property="og:type" content="website">
        <meta property="og:title" content="Save your time and money">
        <meta property="og:description" content="Cut all those unnecessary stuff and go straight down to the nitty-gritty of your business.">
        {{-- <meta property="og:image" content="{{ url($inspiration->thumbnail) }}"> --}}
        <meta property="og:image" content="{{ url('landing-page01/img/landing-page3-banner1.png') }}">
        <meta property="og:url" content="{{ route('landingpage-3') }}">

        <!-- Hotjar Tracking Code for https://designa.studio/ -->
        <script>
            (function(h,o,t,j,a,r){
                h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                h._hjSettings={hjid:1722738,hjsv:6};
                a=o.getElementsByTagName('head')[0];
                r=o.createElement('script');r.async=1;
                r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                a.appendChild(r);
            })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
        </script>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-EV8CHRVEMX"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'G-EV8CHRVEMX');
        </script>

        <script type="application/javascript">
            (function(b,o,n,g,s,r,c){if(b[s])return;b[s]={};b[s].scriptToken="XzQxNTUxODEzNw";b[s].callsQueue=[];b[s].api=function(){b[s].callsQueue.push(arguments);};r=o.createElement(n);c=o.getElementsByTagName(n)[0];r.async=1;r.src=g;r.id=s+n;c.parentNode.insertBefore(r,c);})(window,document,"script","https://cdn.oribi.io/XzQxNTUxODEzNw/oribi.js","ORIBI");
        </script>

        <link href="https://fonts.googleapis.com/css2?family=Spartan:wght@400;500;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <script type="application/javascript">
            (function(b,o,n,g,s,r,c){if(b[s])return;b[s]={};b[s].scriptToken="XzQxNTUxODEzNw";b[s].callsQueue=[];b[s].api=function(){b[s].callsQueue.push(arguments);};r=o.createElement(n);c=o.getElementsByTagName(n)[0];r.async=1;r.src=g;r.id=s+n;c.parentNode.insertBefore(r,c);})(window,document,"script","https://cdn.oribi.io/XzQxNTUxODEzNw/oribi.js","ORIBI");
          </script>
    </head>
    <body id="page-top">
        <nav class="navbar navbar-expand-lg navbar-white bg-white fixed-top" id="mainNav">
            <div class="container px-4">
                <a class="navbar-brand"><img src="/landing-page01/img/designa-logo.png" alt="Designa Studio" class="logo"></a>
                <button class="navbar-toggler navbar-light" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ms-auto">
                        <li class="nav-item"><a class="nav-link" href="#page-top">home</a></li>
                        <li class="nav-item"><a class="nav-link" href="#about2">about</a></li>
                        <li class="nav-item"><a class="nav-link" href="#benefits">benefits</a></li>
                        <li class="nav-item"><a class="nav-link" href="#comparison">comparison</a></li>
                        <li class="nav-item"><a class="nav-link" href="#pricing">pricing</a></li>
                        <li class="nav-item"><a class="nav-link" href="#contact">contact</a></li>
                        <li class="nav-item"><a class="nav-link" href="https://designa.studio/for-supplier/supplier-hub" target="_blank">Suppliers hub</a></li>
                        <li class="nav-item"><a class="nav-link btn-try" href="https://designa.studio/for-supplier/supplier-hub/#demo" target="_blank">Try It Free </a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Header-->
        <!-- Header-->
        <header class="bg-white text-black main-banner">
            <div class="container px-4">
                <div class="row gx-4">
                    <div class="col-lg-6 banner-col-1">
                        <h1 class="fw-bolder"><span class="highlighted2">Gain exposure</span> that leads to more business</h1>
                        <p>List your venue where potential clients and event planners can easily find you.</p>
                    </div>
                    <div class="col-lg-6">
                        <img src="/landing-page01/img/landing-page3-banner3.png" class="img-fluid" alt="">
                    </div>
                </div>
            </div>
        </header>
        <!-- About section-->
        <section id="about2" class="bg-light">
            <div class="container px-4">
                <div class="row gx-4">
                    <div class="col-lg-6">
                        <img src="/landing-page01/img/landing-page3-banner2.png" class="img-fluid" alt="">
                    </div>
                    <div class="col-lg-6 plan-box2-details">
                        <h2 class="text-center" style="padding-bottom: 22px;">Venue marketing made simple</h2>
                        <div class="plan-box2 plan-box2-bottom-space">
                            <h6>Reach new clients and event planners</h6>
                            <ul>
                                <li>Flaunt your venue with the leading event and wedding planners in Australia.</li>
                            </ul>
                        </div>
                        <div class="plan-box2 plan-box2-bottom-space">
                            <h6>Get interactive</h6>
                            <ul>
                                <li>Increase chances of bookings with ultra-realistic 3D images and walkthrough of your venue.</li>
                            </ul>
                        </div>
                        <div class="plan-box2">
                            <h6>Receive technical support</h6>
                            <ul>
                                <li>Our event and tech professionals are ready to help you anytime, whenever you need assistance.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Benefits section-->
        <section class="bg-white marvelous" id="benefits">
            <div class="container px-4">
                <div class="row gx-4 justify-content-center">
                    <div class="col-lg-6 text-center marvelous-space">
                        <h2>Marvelous benefits</h2>
                        <ul class="benefits-list">
                            <li><i class="fa fa-check-circle check-benefits" aria-hidden="true"></i> Increases your venue bookings</li>
                            <li><i class="fa fa-check-circle check-benefits" aria-hidden="true"></i> Widens exposure to events organisers</li>
                            <li><i class="fa fa-check-circle check-benefits" aria-hidden="true"></i> Upsells your products and services</li>
                            <li><i class="fa fa-check-circle check-benefits" aria-hidden="true"></i> Launches anytime… anywhere</li>
                            <li><i class="fa fa-check-circle check-benefits" aria-hidden="true"></i> It’s effortless</li>
                        </ul>
                        <div class="join">Join Today, it's Free!</div>
                        <a href="https://designa.studio/register/supplier" target="_blank"><button type="button" class="btn-signup2">SIGN UP</button></a>
                    </div>
                    <div class="col-lg-6">
                       <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                          <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                          </ol>
                          <div class="carousel-inner">
                            <div class="carousel-item active">
                              <img class="d-block w-100 img-fluid" src="/landing-page01/img/slider-image01.png" alt="">
                            </div>
                            <div class="carousel-item">
                              <img class="d-block w-100 img-fluid" src="/landing-page01/img/slider-image02.png" alt="">
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Comparison section-->
        <section class="bg-light" id="comparison">
            <div class="container px-4">
                <h2 class="text-center">What sets us apart?</h2>
                <div class="row gx-4">
                    <div class="col-lg-4">
                        <div class="comparison-box">
                            <h6 style="padding-top: 27px;">Why Choose Designa?</h6>
                            <ul>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>See your event in 3D instantly</div></li>
                                <li><i class="fa fa-check-square-o check-benefits" aria-hidden="true"></i> <div>Shortlist your favorite venues, suppliers, and planners</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Boost your brand with Designa</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>User-friendly interface</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Easy to find inspirations</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Provides ultra-realistic floorplan</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Accepts add-on services</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Create a digital catalogue</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Compatible with Web, Android and iOS mobile application</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Get expert advice from professionals</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>An inbuilt quoting system</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Live chat with prospective clients</div></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="comparison-box">
                            <h6>What do you get with online directory services?</h6>
                            <ul>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Irrelevant leads</div></li>
                                <li><i class="fa fa-check-square-o check-benefits" aria-hidden="true"></i> <div>Complex structure for users</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Expensive subscription plan</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>No control to filter trolls</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Hidden costs</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Low ROI</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Increased clutter</div></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="comparison-box">
                            <h6>What to do you get with venue finding service?</h6>
                            <ul>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Grouped with irrelevant services</div></li>
                                <li><i class="fa fa-check-square-o check-benefits" aria-hidden="true"></i> <div>Vague information</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>One size fits all offering</div></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row gx-4">
                    <div class="col-lg-12 text-center transform-box4">
                        <h2>Watch Designa transform your event in 30 seconds</h2>
                        <div class="video-section video-section-bg">
                            <div class="video-wrapper">
                                <video controls poster="/landing-page01/img/video-poster.png" id="je-video">
                                    <source src="/landing-page01/img/video-designa.mp4" type="video/mp4">
                                  Your browser does not support HTML5 video.
                                </video>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Subscription section-->
        <section class="bg-white" id="pricing">
            <!-- Table -->
            <div class="subs-content subs-desktop bg-white">
                <div class="container">
                    <div class="row">
                                <div class="col-lg-12 text-center">
                                    <h2>Select Subscription Plan</h2>
                                    <p style="padding-bottom: 40px; padding-top: 0;" class="lead">Upgrade to Pro</p>
                                </div>
                            </div>
                    <div class="row">
                        <table class="table  table-borderless table-responsive-sm">
                            <thead>
                            <tr>
                                <th scope="col"></th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                                <th scope="col" >
                                    <h5 class="subs-header-popular rounded-top">Most Popular</h5>
                                </th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <thead>
                            <tr>
                                <th scope="col"></th>
                                <th scope="col"></th>
                                <th scope="col" class="subs-header-lg">
                                    <h5 >Beginner</h5>
                                    <h4>$0.00</h4>
                                    <h5>Per Month</h5>
                                </th>
                                <th scope="col"></th>
                                <th scope="col" class="subs-header-sm">
                                    <h5>Intermediate</h5>
                                    <h4>$50.00</h4>
                                    <h5>Per Month</h5>
                                </th>
                                <th scope="col"></th>
                                <th scope="col" class="subs-header-sm">
                                    <h5>Advance</h5>
                                    <h4>$150.00</h4>
                                    <h5>Per Month</h5>
                                </th>
                                <th scope="col"></th>
                                <th scope="col" class="subs-header-sm">
                                    <h5>Pro</h5>
                                    <h4>$300.00</h4>
                                    <h5>Per Month</h5>
                                </th>
                            </tr>
                          </thead>
                          <tbody class="bg-light">
                            <tr>
                                <td class="shadow p-3 mb-5 bg-white rounded">
                                    <h6>Online Presence</h6>
                                </td>
                                <td ></td>
                                <td class="shadow p-3 mb-5 bg-white rounded"></td>
                                <td></td>
                                <td class="shadow p-3 mb-5 bg-white rounded"></td>
                                <td></td>
                                <td class="shadow p-3 mb-5 bg-white rounded"></td>
                                <td></td>
                                <td class="shadow p-3 mb-5 bg-white rounded"></td>
                            </tr>
                            <tr>
                                <td class="shadow p-3 mb-5 bg-white rounded">
                                    <span>Create an account</span>
                                </td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                            </tr>
                            <tr>
                                <td class="shadow p-3 mb-5 bg-white rounded">
                                    <span>Load products</span>
                                </td>
                                <td></td>
                                <td class="shadow p-3 mb-5 bg-white rounded"></td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                            </tr>
                            <tr>
                                <td class="shadow p-3 mb-5 bg-white rounded">
                                    <span>Digital catalogue / Look book</span>
                                </td>
                                <td></td>
                                <td class="shadow p-3 mb-5 bg-white rounded"></td>
                                <td></td>
                                <td class="shadow p-3 mb-5 bg-white rounded"></td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                            </tr>
                            <tr>
                                <td class="shadow p-3 mb-5 bg-white rounded">
                                   <span>Your products in 3D</span>
                                </td>
                                <td></td>
                                <td class="shadow p-3 mb-5 bg-white rounded"></td>
                                <td></td>
                                <td class="shadow p-3 mb-5 bg-white rounded"></td>
                                <td></td>
                                <td class="shadow p-3 mb-5 bg-white rounded"></td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                            </tr>
                            <tr>
                                <td class="shadow p-3 mb-5 bg-white rounded2">
                                    <span>Immersive 3D customised room styling using template</span>
                                </td>
                                <td></td>
                                <td class="shadow p-3 mb-5 bg-white rounded2"></td>
                                <td></td>
                                <td class="shadow p-3 mb-5 bg-white rounded2"></td>
                                <td></td>
                                <td class="shadow p-3 mb-5 bg-white rounded2"></td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded2"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                            </tr>

                            <tr>
                                <td class="shadow p-3 mb-5 bg-white rounded">
                                    <h6>Showcasing yourself</h6>
                                </td>
                                <td></td>
                                <td class="shadow p-3 mb-5 bg-white rounded"></td>
                                <td></td>
                                <td class="shadow p-3 mb-5 bg-white rounded"></td>
                                <td></td>
                                <td class="shadow p-3 mb-5 bg-white rounded"></td>
                                <td></td>
                                <td class="shadow p-3 mb-5 bg-white rounded"></td>
                            </tr>
                            <tr>
                                <td class="shadow p-3 mb-5 bg-white rounded">
                                    <span>Load inspiration</span>
                                </td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                            </tr>
                            <tr>
                                <td class="shadow p-3 mb-5 bg-white rounded">
                                    <span>Tag and be tagged</span>
                                </td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                                <td ></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                            </tr>
                            <tr>
                                <td class="shadow p-3 mb-5 bg-white rounded">
                                    <span>Communicate with customers</span>
                                </td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                            </tr>
                            <tr>
                                <td class="shadow p-3 mb-5 bg-white rounded">
                                    <span>Promote using featured marketing</span>
                                </td>
                                <td></td>
                                <td class="shadow p-3 mb-5 bg-white rounded"></td>
                                <td></td>
                                <td class="shadow p-3 mb-5 bg-white rounded"></td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                            </tr>
                            <tr>
                                <td class="shadow p-3 mb-5 bg-white rounded2">
                                    <span>Register for Designa VR Expo</span>
                                </td>
                                <td></td>
                                <td class="shadow p-3 mb-5 bg-white rounded2"></td>
                                <td></td>
                                <td class="shadow p-3 mb-5 bg-white rounded2"></td>
                                <td></td>
                                <td class="shadow p-3 mb-5 bg-white rounded2"></td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded2"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                            </tr>
                            <tr>
                                <td class="shadow p-3 mb-5 bg-white rounded">
                                    <h6>Build your brand</h6>
                                </td>
                                <td></td>
                                <td class="shadow p-3 mb-5 bg-white rounded"></td>
                                <td></td>
                                <td class="shadow p-3 mb-5 bg-white rounded"></td>
                                <td></td>
                                <td class="shadow p-3 mb-5 bg-white rounded"></td>
                                <td></td>
                                <td class="shadow p-3 mb-5 bg-white rounded"></td>
                            </tr>
                            <tr>
                                <td class="shadow p-3 mb-5 bg-white rounded">
                                    <span>Receive and fulfill quotes</span>
                                </td>
                                <td></td>
                                <td class="shadow p-3 mb-5 bg-white rounded"></td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                            </tr>
                            <tr>
                                <td class="shadow p-3 mb-5 bg-white rounded">
                                    <span>Create pitch packs in Designa</span>
                                </td>
                                <td></td>
                                <td class="shadow p-3 mb-5 bg-white rounded"></td>
                                <td></td>
                                <td class="shadow p-3 mb-5 bg-white rounded"></td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                            </tr>
                            <tr>
                                <td class="shadow p-3 mb-5 bg-white rounded">
                                    <span>Create your own event 3D using template</span>
                                </td>
                                <td></td>
                                <td class="shadow p-3 mb-5 bg-white rounded"></td>
                                <td></td>
                                <td class="shadow p-3 mb-5 bg-white rounded"></td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                            </tr>
                            <tr>
                                <td class="shadow p-3 mb-5 bg-white rounded">
                                    <span>Transform a room with custom 3D Builds</span>
                                </td>
                                <td></td>
                                <td class="shadow p-3 mb-5 bg-white rounded"></td>
                                <td></td>
                                <td class="shadow p-3 mb-5 bg-white rounded"></td>
                                <td></td>
                                <td class="shadow p-3 mb-5 bg-white rounded"></td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                            </tr>
                            <tr>
                                <td class="shadow p-3 mb-5 bg-white rounded"></td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded">
                                    <a href="#"><button type="button" class="btn-buy-now">Choose this plan</button></a>
                                </td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded">
                                    <a href="#"><button type="button" class="btn-buy-now">Choose this plan</button></a>
                                </td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded">
                                    <a href="#"><button type="button" class="btn-buy-now">Choose this plan</button></a>
                                </td>
                                <td></td>
                                <td class="text-center shadow p-3 mb-5 bg-white rounded">
                                    <a href="#"><button type="button" class="btn-buy-now">Choose this plan</button></a>
                                </td>
                            </tr>
                          </tbody>

                        </table>
                    </div>
                </div>
            </div>
            <!-- end of table -->
        </section>

        <!-- Mobile -->
        <div class="container subs-mobile">
            <h2 class="text-center" style="padding-bottom: 10px;">Select Subscription Plan</h2>
            <div id="accordion">

                <!-- 1st Card -->
                <div class="card">
                    <div class="card-header" id="headingOne" style="background: #cbcbcb;">
                        <div>
                            <h5 class="mb-0">
                                <button class="btn btn-link btn-plan-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Beginner
                                </button>
                            </h5>


                            <div class="accordion-plan-details">
                                <h5 class="mb-0">$00.00</h5>
                                <h6>Per Month</h6>
                            </div>
                        </div>
                        <div class="accordion-clear"></div>
                    </div>


                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body accordion-body-bg">
                            <ul class="accordion-list">
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Create an account</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Load inspiration</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Tag and be tagged</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Communicate with customers</li>
                            </ul>
                            <div class="text-center">
                                <a href="#"><button type="button" class="btn-buy-now-mobile">Choose this plan</button></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- 1st Card -->

                <!-- 2nd Card -->
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background: #cbcbcb;">
                        <div>
                            <h5 class="mb-0">
                                <button class="btn btn-link btn-plan-link" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                    Intermediate
                                </button>
                            </h5>


                            <div class="accordion-plan-details">
                                <h5 class="mb-0">$50.00</h5>
                                <h6>Per Month</h6>
                            </div>
                        </div>
                        <div class="accordion-clear"></div>
                    </div>


                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body accordion-body-bg">
                            <ul class="accordion-list">
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Create an account</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Load products</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Load inspiration</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Tag and be tagged</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Communicate with customers</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Receive and fulfill quotes</li>
                            </ul>
                            <div class="text-center">
                                <a href="#"><button type="button" class="btn-buy-now-mobile">Choose this plan</button></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- 2nd Card -->

                <!--3rd Card -->
                <div class="card">
                    <div class="card-header" id="headingThree" style="background: #f29833">
                        <div>
                            <h5 class="mb-0">
                                <button class="btn btn-link btn-plan-link" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                    Advance
                                </button>
                            </h5>


                            <div class="accordion-plan-details">
                                <h5 class="mb-0">$150.00</h5>
                                <h6>Per Month</h6>
                            </div>
                        </div>
                        <div class="accordion-clear"></div>
                    </div>


                    <div id="collapseThree" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body accordion-body-bg">
                            <ul class="accordion-list">
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Create an account</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Load products</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Digital catalogue / Look book</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Your products in 3D</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Load inspiration</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Tag and be tagged</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Communicate with customers</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Promote using featured marketing</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Receive and fulfill quotes</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Create pitch packs in Designa</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Create your own event 3D using template</li>
                            </ul>
                            <div class="text-center">
                                <a href="#"><button type="button" class="btn-buy-now-mobile">Choose this plan</button></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- 3rd Card -->

                <!--4th Card -->
                <div class="card">
                    <div class="card-header" id="headingFour" style="background: #cbcbcb;">
                        <div>
                            <h5 class="mb-0">
                                <button class="btn btn-link btn-plan-link" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                                    Pro
                                </button>
                            </h5>


                            <div class="accordion-plan-details">
                                <h5 class="mb-0">$300.00</h5>
                                <h6>Per Month</h6>
                            </div>
                        </div>
                        <div class="accordion-clear"></div>
                    </div>


                    <div id="collapseFour" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body accordion-body-bg">
                            <ul class="accordion-list">
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Create an account</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Load products</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Digital catalogue / Look book</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Your products in 3D</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> 3D virtual showroom</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Immersive 3D customised room styling using template</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Load inspiration</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Tag and be tagged</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Communicate with customers</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Promote using featured marketing</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Register for Designa VR Expo</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Receive and fulfill quotes</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Create pitch packs in Designa</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Create you own event 3D using template</li>
                                <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Transform a room with custom 3D build</li>
                            </ul>
                            <div class="text-center">
                                <a href="#"><button type="button" class="btn-buy-now-mobile">Choose this plan</button></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- 4th Card -->
            </div>
        </div>
        <!-- Mobile -->

        <!-- Free Trial -->
        <section class="bg-white" id="freetrial">
            <div class="container px-4">
                <div class="row gx-4 justify-content-center">
                    <div class="col-lg-6 text-center">
                        <img src="/landing-page01/img/landing-page-banner3.png" class="img-fluid" alt="">
                    </div>
                    <div class="col-lg-6 text-center">
                        <div class="start-free-trial">
                            <h2>START YOUR 1 MONTH FREE TRIAL</h2>
                            <a href="https://designa.studio/register/supplier" target="_blank"><button type="button" class="btn-signup2">SIGN UP</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <!-- Contact section-->
        <section id="contact" class="bg-light">
            <div class="container px-4">
                <div class="row gx-4 justify-content-center">
                    <div class="col-lg-8 text-center">
                        <h2>Need more information</h2>
                        <p class="lead">Request a demo now</p>
                        <div id="app">
                            <request-form-covid></request-form-covid>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Footer-->
        <footer class="py-5 bg-white">
            <div class="container px-4">
                <div class="row gx-4 justify-content-center">
                    <div class="col-lg-12 text-center">
                        <div class="social-circle">
                            <a href="https://www.facebook.com/Designa-Studio-260657538157535/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        </div>
                        <div class="social-circle">
                            <a href="https://www.linkedin.com/company/designastudio/" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                        </div>
                        <div class="social-circle">
                            <a href="https://www.instagram.com/designastudio.au/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        </div>
                        <p style="font-size: 14px; padding-top: 20px;">
                            Follow us to receive instant update<br>
                            <a href="mailto:hello@designa.studio" style="color: #4E4E4E;">hello@designa.studio</a>
                        </p>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="/landing-page01/js/scripts.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous" type="3fdf24a46cbb971ce9cc62d4-text/javascript"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous" type="3fdf24a46cbb971ce9cc62d4-text/javascript"></script>

        <script src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js" data-cf-settings="3fdf24a46cbb971ce9cc62d4-|49" defer=""></script>
        <script src="/js/app.js"></script>
    </body>
</html>
