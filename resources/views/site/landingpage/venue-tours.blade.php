<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="Improve your venue's online presence and drive traffic to your business with an interactive 3D virtual tour." />
        <meta name="author" content="designa.studio" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>The New Gold Standard For Virtual Tours / Designa Studio</title>
        <link rel="icon" type="image/x-icon" href="https://designa.studio/images/logo.ico" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link href="/landing-page01/css/styles.css" rel="stylesheet" />
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>

        <meta name="description" content="Impress clients and gain high-quality leads.">
        <meta property="og:type" content="website">
        <meta property="og:title" content="The New Gold Standard For Virtual Tours">
        <meta property="og:description" content="Improve your venue's online presence and drive traffic to your business with an interactive 3D virtual tour.">
        {{-- <meta property="og:image" content="{{ url($inspiration->thumbnail) }}"> --}}
        <meta property="og:image" content="{{ url('landing-page01/img/landing-page-banner3.png') }}">
        <meta property="og:url" content="{{ route('landing.venue.tours') }}">

        <link href="https://fonts.googleapis.com/css2?family=Spartan:wght@400;500;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <link rel="stylesheet" type="text/css" href="/landing-page01/css/slick.css"/>
        <link rel="stylesheet" type="text/css" href="/landing-page01/css/slick-theme.css"/>

        <!-- Hotjar Tracking Code for https://designa.studio/venue-tours -->
        <script>
            (function(h,o,t,j,a,r){
                h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                h._hjSettings={hjid:2559195,hjsv:6};
                a=o.getElementsByTagName('head')[0];
                r=o.createElement('script');r.async=1;
                r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                a.appendChild(r);
            })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
        </script>

          <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5CNLCXG');</script>
        <!-- End Google Tag Manager -->
        <script type="application/javascript">
            (function(b,o,n,g,s,r,c){if(b[s])return;b[s]={};b[s].scriptToken="XzQxNTUxODEzNw";b[s].callsQueue=[];b[s].api=function(){b[s].callsQueue.push(arguments);};r=o.createElement(n);c=o.getElementsByTagName(n)[0];r.async=1;r.src=g;r.id=s+n;c.parentNode.insertBefore(r,c);})(window,document,"script","https://cdn.oribi.io/XzQxNTUxODEzNw/oribi.js","ORIBI");
        </script>
    </head>
    <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5CNLCXG"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <div id="app">
         <!-- Navigation-->
         <nav class="navbar navbar-expand-lg navbar-white bg-white fixed-top" id="mainNav">
            <div class="container px-4">
                <a class="navbar-brand"><img src="/landing-page01/img/designa-logo.png" alt="Designa Studio" class="logo"></a>
                <button class="navbar-toggler navbar-light" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ms-auto">
                        <li class="nav-item"><a class="nav-link" href="#about3">about</a></li>
                        <li class="nav-item"><a class="nav-link" href="#benefits3">benefits</a></li>
                        <li class="nav-item"><a class="nav-link" href="#comparison">comparison</a></li>
                        <li class="nav-item"><a class="nav-link" href="#testimonials">testimonials</a></li>
                        <li class="nav-item"><a class="nav-link" href="#services">services</a></li>
                        <li class="nav-item"><a class="nav-link" href="#contact">contact</a></li>
                        <li class="nav-item"><a class="nav-link btn-try" href="https://designa.studio/request-3d?form=QuickForm" target="_blank">Register Now</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Header Desktop -->
        <header class="video-height text-white banner-desktop" id="about3">
            <div class="container px-4 text-center image-text-center">
                <h1 class="fw-bolder header-intro2">The New Gold Standard For Virtual Tours</h1>
                <p class="lead2">Improve your venue's online presence and drive traffic to your business with an interactive 3D virtual tour</p>
                <p class="benefits-space"><a href="https://designa.studio/request-3d?form=QuickForm" target="_blank"><button type="button" class="video-start-now-btn">START NOW</button></a></p>
                <video id="background-video" autoplay loop muted poster="/landing-page01/img/banner-video-poster01.png">
                    <source src="/landing-page01/img/banner-video01.mp4" type="video/mp4">
                </video>
            </div>
        </header>

        <!-- Header Mobile-->
        <header class="video-page-banner text-white banner-mobile" id="about3">
            <div class="container px-4 text-center image-text-center">
                <h1 class="fw-bolder header-intro2">The New Gold Standard For Virtual Tours</h1>
                <p class="lead2">Improve your venue's online presence and drive traffic to your business with an interactive 3D virtual tour</p>
                <p class="benefits-space"><a href="https://designa.studio/request-3d?form=QuickForm" target="_blank"><button type="button" class="video-start-now-btn">START NOW</button></a></p>
            </div>
        </header>

        <!-- Benefits section-->
        <section class="bg-light about" id="benefits3">
            <div class="container px-4">
                <div class="row gx-4 justify-content-center">
                    <div class="col-lg-12">
                        <h2 class="text-center">Benefits</h2>
                    </div>
                </div>

                <div class="row gx-4 text-center marketing-space">
                    <div class="col-lg-4">
                        <div class="plan-box3">
                            <img src="/landing-page01/img/stunning-3d.png" alt="" class="img-fluid">
                            <h6 class="text-black">3D video walkthroughs</h6>
                            <ul>
                                <li>Virtual tours allow you to show clients your space without them ever stepping foot into it, taking the experience to the next level.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="plan-box3 about-space">
                            <img src="/landing-page01/img/accurate-3d.png" alt="" class="img-fluid">
                            <h6 class="text-black">Accurate 3D floor plans</h6>
                            <ul>
                                <li>A client can begin mentally mapping their event onto your area by just presenting your floor plans.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="plan-box3 about-space">
                            <img src="/landing-page01/img/3d-video2.png" alt="" class="img-fluid">
                            <h6 class="text-black">Stunning 3D images</h6>
                            <ul>
                                <li>Showcase your space with stunning professional 3D highlighting your venue's unique features.</li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row gx-4 text-center marketing-space">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-4">
                        <div class="plan-box3 about-space2">
                            <img src="/landing-page01/img/available-anytime.png" alt="" class="img-fluid">
                            <h6 class="text-black">Available Anytime, Anywhere</h6>
                            <ul>
                                <li>Present your venue on every device for easy access for your clients.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="plan-box3 about-space">
                            <img src="/landing-page01/img/get-more-leads2.png" alt="" class="img-fluid">
                            <h6 class="text-black">Get More Leads</h6>
                            <ul>
                                <li>Get that momentum with a steady stream of quality leads through an innovative online hub for the event industry.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-2"></div>
                </div>
            </div>
        </section>

        <!-- Additional -->
        <section id="additional" class="bg-white">
            <div class="container px-4">
                <div class="row gx-4 justify-content-center">
                    <div class="col-lg-6 text-center">
                        <img src="/landing-page01/img/landing-page-banner3.png" class="img-fluid" alt="">
                    </div>
                    <div class="col-lg-6 text-left">
                        <div class="submit-photos">
                            <h2 style="padding-bottom: 10px;">Getting Setup is easy.</h2>
                            <p>All you need to provide is floorplan of your venue and some videos or photos or your event space and we do the rest.</p>
                            <p>Our professional 3D artists will take over your space and take 5 working days to see your venue in stunning 3D.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Comparison section-->
        <section class="bg-light" id="comparison">
            <div class="container px-4">
                <h2 class="text-center">What sets us apart?</h2>
                <div class="row gx-4">
                    <div class="col-lg-4">
                        <div class="comparison-box">
                            <h6 class="comparison-box-space">Why Choose Designa?</h6>
                            <ul>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>See your event in 3D instantly</div></li>
                                <li><i class="fa fa-check-square-o check-benefits" aria-hidden="true"></i> <div>Shortlist your favorite venues, suppliers, and planners</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Boost your brand with Designa</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>User-friendly interface</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Easy to find inspirations</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Provides ultra-realistic floorplan</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Accepts add-on services</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Create a digital catalogue</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Compatible with Web, Android and iOS mobile application</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Get expert advice from professionals</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>An inbuilt quoting system</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Live chat with prospective clients</div></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="comparison-box">
                            <h6>What do you get with online directory services?</h6>
                            <ul>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Irrelevant leads</div></li>
                                <li><i class="fa fa-check-square-o check-benefits" aria-hidden="true"></i> <div>Complex structure for users</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Expensive subscription plan</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>No control to filter trolls</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Hidden costs</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Low ROI</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Increased clutter</div></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="comparison-box">
                            <h6>What to do you get with venue finding service?</h6>
                            <ul>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Grouped with irrelevant services</div></li>
                                <li><i class="fa fa-check-square-o check-benefits" aria-hidden="true"></i> <div>Vague information</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>One size fits all offering</div></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Testimonial section -->
        <section id="testimonials" class="bg-white">
            <div class="container px-4">
                <h2 class="text-center">What our clients say</h2>
                <div class="row gx-4">
                    <div class="col-lg-12">
                        <div class="multiple-items">
                            <div class="col-lg-6">
                               <div class="box-quote">
                                    <blockquote>
                                        Love using this app to create floorplans and showcase our products at different venues.  It is very impressive.  By far the best system we have used.
                                    </blockquote>
                                    <div class="box-quote-details">
                                        <ul class="box-quote-list">
                                          <li><img src="/landing-page01/img/wlh-logo-small.png" class="img-fluid"></li>
                                          <li class="box-quote-list-space">White Label Hire</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                               <div class="box-quote">
                                    <blockquote>
                                        As a stylist the ability to show clients the vision is essential and no other other app does this quite like Designa.  A fantastic tool.
                                    </blockquote>
                                    <div class="box-quote-details">
                                        <ul class="box-quote-list">
                                          <li><img src="/landing-page01/img/jalten-logo-small.png" class="img-fluid"></li>
                                          <li class="box-quote-list-space">John Alten</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row gx-4">
                    <div class="col-lg-12 text-center video-space">
                        <h2>Watch Designa transform your event in 30 seconds</h2>
                        <div class="video-section video-section-bg">
                            <div class="video-wrapper">
                                <video controls poster="/landing-page01/img/video-poster.png" id="je-video">
                                    <source src="/landing-page01/img/video-designa.mp4" type="video/mp4">
                                  Your browser does not support HTML5 video.
                                </video>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Logo section -->
        <section id="logo-list" class="bg-light">
            <div class="container px-4">
                <div class="row gx-4">
                    <div class="col-lg-3">
                        <img src="/landing-page01/img/logo-hyatt.png" alt="HYATT" class="img-fluid">
                    </div>
                    <div class="col-lg-3">
                        <img src="/landing-page01/img/logo-four-seasons.png" alt="FOUR SEASONS" class="img-fluid">
                    </div>
                    <div class="col-lg-3">
                        <img src="/landing-page01/img/logo-sofitel.png" alt="SOFITEL" class="img-fluid">
                    </div>
                    <div class="col-lg-3">
                        <img src="/landing-page01/img/logo-miramare-gardens.png" alt="MIRAMARE GARDENS" class="img-fluid">
                    </div>
                </div>
        </section>

        <!-- Services section -->
        <section id="services" class="bg-white">
            <div class="container px-4">
                <div class="row gx-4">
                    <div class="col-lg-6">
                        <div class="submit-photos text-center mb-3" style="padding-top: 5px;">
                            <p class="title">
                                Request a Demo
                            </p>
                            <p>
                                If you need a bit more help, we would love to arrange a one on one demo. Our team is ready to show you the platform and take you through all the functions at your fingertips.
                            </p>
                            <request-form-venue-tours type="demo"></request-form-venue-tours>
                        </div>
                    </div>
                    <div class="col-lg-6 text-left">
                        <img src="/landing-page01/img/banner-request-a-demo.png" alt="" class="img-fluid">
                    </div>
                </div>

                <div class="row gx-4">
                    <div class="col-lg-6">
                        <img src="/landing-page01/img/banner-request-white-gloves.png" alt="" class="img-fluid">
                    </div>
                    <div class="col-lg-6">
                        <div class="submit-photos2 text-center">
                            <p class="title">
                                Request White Glove Service
                            </p>
                            <p>
                                Designa Studio has a team ready to go to set up your account and start you off with loading your products and inspiration. To register your interest, simply provide your email address below, and we will be in contact.
                            </p>
                            <request-white-glove-service-venue-tours></request-white-glove-service-venue-tours>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Contact section-->
        <section id="contact" class="bg-light">
            <div class="container px-4">
                <div class="row gx-4 justify-content-center">
                    <div class="col-lg-8 text-center" id="app">
                        <h2>Need more information</h2>
                        <request-form-covid></request-form-covid>
                    </div>
                </div>
            </div>
        </section>

        <!-- Footer-->
        <footer class="py-5 bg-white">
            <div class="container px-4">
                <div class="row gx-4 justify-content-center">
                    <div class="col-lg-12 text-center">
                        <div class="social-circle">
                            <a href="https://www.facebook.com/Designa-Studio-260657538157535/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        </div>
                        <div class="social-circle">
                            <a href="https://www.linkedin.com/company/designastudio/" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                        </div>
                        <div class="social-circle">
                            <a href="https://www.instagram.com/designastudio.au/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        </div>
                        <p style="font-size: 14px; padding-top: 20px;">
                            Follow us to receive instant update<br>
                            <a href="mailto:hello@designa.studio" style="color: #4E4E4E;">hello@designa.studio</a>
                        </p>
                    </div>
                </div>
            </div>
        </footer>
        </div>

        <!-- Modal section -->
        <div id="myModal" class="modal fade">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body effect7">
                    <button type="button" class="close" data-bs-dismiss="modal">&times;</button>
                    <div class="modal-content-details">
                        <p class="text-center title">
                            With thousands of event spaces in Australia, you must stand out online.
                        </p>
                        <p class="text-center">
                            Give your venue a 360-degree panorama view of improved marketing and impress clients.
                        </p>
                        <p class="text-center">
                            <a href="#services" class="btn btn-warning text-white" style="background-color:#f79827" >Register Now</a>
                        <p>
                    </div>
                </div>
            </div>
        </div>

        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script> --}}
        <script src="/landing-page01/js/scripts.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous" type="3fdf24a46cbb971ce9cc62d4-text/javascript"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous" type="3fdf24a46cbb971ce9cc62d4-text/javascript"></script>

        <script src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js" data-cf-settings="3fdf24a46cbb971ce9cc62d4-|49" defer=""></script>

        <script src='/js/app.js'></script>
        <script type="text/javascript" src="/landing-page01/js/slick.js"></script>
        {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js" integrity="sha512-3j3VU6WC5rPQB4Ld1jnLV7Kd5xr+cq9avvhwqzbH/taCRNURoeEpoPBK9pDyeukwSxwRPJ8fDgvYXd6SkaZ2TA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script> --}}

        <script type="text/javascript">
            var  modalLoaded = false
            $(document).ready(function(){
                $(window).scroll(function() {
                    if($(window).scrollTop() + $(window).height() >= ($(document).height() - 600)) {
                        if(!modalLoaded){
                            setTimeout(function(){
                                $('#myModal').modal('show');
                                modalLoaded = true;
                            }, 3000)
                        }
                    }
                });


                $('.multiple-items').slick({
                    dots: true,
                    infinite: false,
                    speed: 300,
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    responsive: [
                        {
                            breakpoint: 1024,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 3,
                                infinite: true,
                                dots: true
                            }
                        },
                        {
                            breakpoint: 600,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 2
                            }
                        },
                        {
                            breakpoint: 480,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                            }
                        }]
                    });
            });
        </script>

    </body>
</html>
