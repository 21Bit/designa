@extends('site.includes.layouts.full')

@section('content')
  <div class="page-header-img">
      <!-- <h1 class="text-orange text-center">Notification</h1> -->
      <img src="/site/images/notification-bg.jpg" class="bg-image"  alt="">
      <img class="bottom-pattern" src="/site/images/bottom-pattern.png" alt="">
  </div>
  <div class="container">
        <div class="row">
                <div class="col-sm-3">
                    @include('site.includes.menus.user-side-menu')
                </div>
                <div class="col-sm-9 site-notification-page">
                    <div class="btn-group pull-right" role="group" aria-label="Basic example">
                        <a href="{{ route('user.saved.inspirations') }}" class="btn btn-warning text-white">Inspirations</a>
                        <a href="{{ route('user.saved.venues') }}" class="btn btn-outline-secondary"> Venues</a>
                        <a href="{{ route('user.saved.decors') }}" class="btn btn-outline-secondary"> Decors</a>
                    </div>
                    <h4 class="title mb-4">
                        Saved Inspirations
                    </h4>
                    <div class="clearfix"></div>
                        @forelse($inspirations as $inspiration)
                            <div class="media">
                                <img class="mr-3 mb-0 rounded" src="{{ $inspiration->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png') }}"  alt="Generic placeholder image">
                                <div class="media-body">
                                    <a href="{{ route('site.inspiration.show', $inspiration->slug) }}" >
                                        <h5 class="mt-0 mb-0 text-orange">{{ $inspiration->name }}</h5>
                                    </a>
                                        <small>
                                            {{ $inspiration->company ? $inspiration->company->name : " supplier is no more exist in the system" }}
                                        </small>
                                </div>
                            </div>
                        @empty
                            <h4 class="mt-5 text-center text-muted" > No Item Found yet..</h4>
                        @endforelse
                    {{ $inspirations->links() }}
                </div>
            </div>
  </div>



@endsection
