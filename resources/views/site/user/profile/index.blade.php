@extends('site.includes.layouts.full')

@section('content')
  <div class="page-header-img">
      <!-- <h1 class="text-orange text-center">Notification</h1> -->
      <img src="/site/images/notification-bg.jpg" class="bg-image"  alt="">
      <img class="bottom-pattern" src="/site/images/bottom-pattern.png" alt="">
  </div> 
  <div class="container">
        <div class="row">
                <div class="col-sm-3">
                    @include('site.includes.menus.user-side-menu')
                </div>
                <div class="col-sm-9 site-profile-page">
                    <div class="text-right">
                        <small>
                            <a href="{{ route('user.profile.edit') }}" class="mr-3"><i class="fa fa-pencil"></i> Edit Profile</a>
                            <a href="{{ route('user.profile.changepassword') }}"><i class="fa fa-cog"></i> Change Password</a>
                        </small>
                    </div>
                    <h4 class="mb-3">Welcome {{ Auth::user()->name  }}!</h4>
                    <div class="row mb-4">
                        <div class="col-sm-4">
                            <div class="box">
                                <h2 cpass="number mb-0">{{ $saveinspirations }}</h2>
                                <div class="title mt-0">Saved Inspiration</div>
                                <a href="{{ route('user.saved.inspirations') }}">View More..</a>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="box">
                                <h2 class="number mb-0">{{ $savevenues }}</h2>
                                <div class="title mt-0">Saved Venues</div>
                                <a href="{{ route('user.saved.venues') }}">View More..</a>
                                
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="box">
                                <h2 class="number mb-0">{{ $savedecors }}</h2>
                                <div class="title mt-0">Saved Decors</div>
                                <a href="{{ route('user.saved.decors') }}">View More..</a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="quotes mb-4 border-box">
                        <a href="{{ route('user.qoute.create') }}" class="pull-right"><i class="fa fa-plus"></i> Request New Qoute</a>
                        <label for="">Recent Quotes</label>
                        @if($quotes)
                            <div class="bg-white quote-table" >
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Ref. #</th>
                                            <th>Event Name</th>
                                            <th>Event Type</th>
                                            <th>Number of Decor</th>
                                            <th>Created On</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($quotes as $quote)
                                            <tr>
                                                <td>
                                                    <a href="{{ route('user.qoute.show', $quote->reference_number) }}">
                                                        {{ $quote->event_name }}
                                                    </a>
                                                    </td>
                                                <td>{{ $quote->event_name }}</td>
                                                <td>{{ $quote->event_type }}</td>
                                                <td>{{ $quote->decors()->count() }}</td>
                                                <td>{{ $quote->created_at }}</td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="4" align="center">No Quote Yet..</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        @else
                            <h4 class="text-center text-muted mt-3"> No Quotes yet!..</h4>
                        @endif
                    </div>

                    <div class="quotes mb-4">
                        <a  class="pull-right text-orange" href="{{ route('user.notification.index') }}">See All Notifications</a>
                        <label for="">Recent Notification</label>
                        <div class="clearfix"></div>
                        @foreach($notifications as $notification)
                            <div class="media">
                                <img class="mr-3 mb-0 rounded-circle" src="{{ $notification->data['avatar'] }}"  alt="Generic placeholder image">
                                <div class="media-body">
                                    <h5 class="mt-0 mb-0">{{ $notification->data['title'] }}</h5>
                                    {{ $notification->data['message'] }} <br>
                                    <small><i class="fa fa-clock-o"></i> {{ $notification->created_at->diffforhumans() }}</small>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
  </div>


 
@endsection
