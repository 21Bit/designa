@extends('site.includes.layouts.full')

@section('content')
  <div class="page-header-img">
      <!-- <h1 class="text-orange text-center">Notification</h1> -->
      <img src="/site/images/notification-bg.jpg" class="bg-image"  alt="">
      <img class="bottom-pattern" src="/site/images/bottom-pattern.png" alt="">
  </div> 
  <div class="container">
        <div class="row">
                <div class="col-sm-3">
                    @include('site.includes.menus.user-side-menu')
                </div>
                <div class="col-sm-9 site-profile-page">
                    <h4 class="mb-3">Change Password</h4>
                    @include('site.includes.alerts.message')
                    @include('site.includes.alerts.error')
                    <div class="bg-white">
                        <div class="p-3">
                            <form action="{{ route('user.profile.changePasswordPost') }}" method="POST">
                                @csrf
                                <p>
                                    <label for=""> Current Password</label>
                                    <input type="password" class="form-control form-control-sm" required  placeholder=" old password" name="current_password">
                                </p>
                                <hr>
                                <p>
                                    <label for=""> New Password</label>
                                    <input type="password" class="form-control form-control-sm" required  placeholder="new password" name="password">
                                </p>
                                <p>
                                    <label for=""> Repeat Password</label>
                                    <input type="password" class="form-control form-control-sm" required  placeholder=" repeat new password" name="password_confirmation">
                                </p>
                                <br>
                                <p>
                                    <button class="btn btn-warning"><i class="fa fa-save"> </i> Submit</button>
                                    <a href="{{ route('user.profile.index') }}" class="btn"><i class="fa fa-ban"> </i> Cancel</a>
                                </p>
                            </form>
                        </div>
                      
                    </div>
                </div>
            </div>
  </div>


 
@endsection
