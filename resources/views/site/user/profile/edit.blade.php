@extends('site.includes.layouts.full')

@section('content')
  <div class="page-header-img">
      <!-- <h1 class="text-orange text-center">Notification</h1> -->
      <img src="/site/images/notification-bg.jpg" class="bg-image"  alt="">
      <img class="bottom-pattern" src="/site/images/bottom-pattern.png" alt="">
  </div> 
  <div class="container">
        <div class="row">
                <div class="col-sm-3">
                    @include('site.includes.menus.user-side-menu')
                </div>
                <div class="col-sm-9 site-profile-page">
                    @include('dashboard.includes.alerts.success')
                    <form action="{{ route('user.profile.update') }}" method="POST">
                        @csrf
                        @method("PUT")
                        <div class="bg-white p-5">
                            <div class="p-5">
                                <div class="text-center">
                                    <img src="{{ Auth::user()->getProfilePicture() }}" id="img-preview" class="mw-100 rounded-circle mx-auto d-block mb-2" style="width:165px; height: 165px;" alt="User Image">
                                    <input type="file"  id="img-picker"  style="display:none"  name="company_logo" accept="image/*">
                                    <input type="hidden" name="picture">
                                    <label for="img-picker" class="btn btn-secondary btn-xs mb-2"> Change Profile</label>
                                </div>
                                <p>
                                    <label for=""> Name</label>
                                    <input type="text" value="{{ $user->name }}" class="form-control" name="name">
                                </p>
                                <p>
                                    <button class="btn btn-warning" type="submit"> Save</button>
                                </p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
  </div>
  <div class="modal" id="cropper-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <label>Crop Image</label>
                    <div id="crop-picture-container" class="mw-100"></div>
                    <button id="crop-done-btn" class="btn btn-primary btn-lg">Done</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.4/croppie.css" integrity="sha256-M8o9uqnAVROBWo3/2ZHSIJG+ZHbaQdpljJLLvdpeKcI=" crossorigin="anonymous" />
@endpush
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.4/croppie.min.js" integrity="sha256-bQTfUf1lSu0N421HV2ITHiSjpZ6/5aS6mUNlojIGGWg=" crossorigin="anonymous"></script>
    <script>

        // for logo
            var resize = $('#crop-picture-container').croppie({
                enableExif: true,
                enableOrientation: true,
                viewport: { // Default { width: 100, height: 100, type: 'square' } 
                    width: 250,
                    height: 250,
                    type: 'square'
                },
                boundary: {
                    width: 400,
                    height: 400
                }
            });
            
            
            $('#img-picker').on('change', function () {
                var reader = new FileReader();
            
                $("#cropper-modal").modal("show")
            
                reader.onload = function (e) {
                    resize.croppie('bind', {
                        url: e.target.result
                    }).then(function () {
                        //console.log('jQuery bind complete');
                    });
                }
                
                reader.readAsDataURL(this.files[0]);
            
            });
            
            $("#crop-done-btn").on('click', function (ev) {
                resize.croppie('result', {
                    type: 'canvas',
                    size: 'viewport'
                }).then(function (img) {
                    console.log(img)
                    $("#img-preview").attr("src", img)
                    $("input[name=picture").val(img)
                    $("#cropper-modal").modal("hide")
                });     
            });
    </script>
@endpush