@extends('site.includes.layouts.full')

@section('content')
  <div class="page-header-img">
      <!-- <h1 class="text-orange text-center">Notification</h1> -->
      <img src="/site/images/notification-bg.jpg" class="bg-image"  alt="">
      <img class="bottom-pattern" src="/site/images/bottom-pattern.png" alt="">
  </div> 
  <div class="container mb-4">
        <div class="row">
                <div class="col-sm-3">
                    @include('site.includes.menus.user-side-menu')
                </div>
                <div class="col-sm-9">
                    <div class="text-right">
                        <a href="{{ route('user.qoute.index') }}" class="mr-3"><i class="fa fa-ban"></i> Back to List</a>
                        <a href="{{ route('user.qoute.create') }}" ><i class="fa fa-plus"></i> Request New Qoute</a>

                    </div>
                    <h4 class="title mb-4">
                        Quotes
                    </h4>
                    <div class="bg-orange p-3 rounded text-white mb-3">
                        <h2 class="mb-0">{{ $qoute->event_name }}</h2>
                        <table>
                            <tr>
                                <td>Reference Number</td>
                                <td>: {{ $qoute->reference_number }}</td>
                            </tr>
                            <tr>
                                <td>Type</td>
                                <td>: {{ is_numeric($qoute->event_type) ? $qoute->category->name : $qoute->event_type }}</td>
                            </tr>
                            <tr>
                                <td>Location</td>
                                <td>: {{ $qoute->location }}</td>
                            </tr>
                            <tr>
                                <td>Number of Guests</td>
                                <td>: {{ $qoute->number_of_guests }}</td>
                            </tr>
                            <tr>
                                <td>Date</td>
                                <td>: {{ $qoute->event_date }}</td>
                            </tr>
                            <tr>
                                <td>Time Start</td>
                                <td>: {{ $qoute->time_start }}</td>
                            </tr>
                            <tr>
                                <td>Time End</td>
                                <td>: {{ $qoute->time_end }}</td>
                            </tr>
                        </table>

                    </div>
       
               
                            @foreach($decorlist as $data)
                                 <table class="table table-striped table-bordered" style='font-size:12px;'>
                                    <tbody>
                                        <tr>
                                            <td colspan="5">
                                            <img src="{{ $data['supplier']->image('logo')->path('logos', '/images/placeholders/placeholder250x200.png') }}" style="width:100px; height:100px" >
                                            <h4 style="display:inline-block" class="ml-2">
                                                {{  $data['supplier']->name }}
                                            </h4>
                                        </td>
                                        </tr>
                                        <tr>
                                            <th></th>
                                            <th>Quantity</th>
                                            @foreach($pricingcomponents as $pricingComponent)
                                            <th>{{ $pricingComponent->name  }}  @if($pricingComponent->multipliable) (each) @endif</th>
                                            @endforeach
                                        </tr>
                                        @foreach($data['decor'] as $decor)
                                            <tr>
                                                <td>
                                                    <img class="mr-3 mb-0"  src="{{ $decor->product->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png')  }}" style="width:34px; height:34px" alt="Generic placeholder image">
                                                {{ $decor->product->name }}
                                                </td>
                                                <td>{{ $decor->quantity ?? 1 }}</th>
                                                @foreach($pricingcomponents as $pricing)
                                                <td>{{ $decor->getComponentPrice($pricing->id) ? '$' . number_format($decor->getComponentPrice($pricing->id)) : "-" }} </td>
                                                @endforeach
                                            </<tr>
                                        @endforeach
                                        <tr>
                                            <td colspan='5' class="text-right">
                                                Total: <h4 class="text-orange" style="display:inline-block">${{ number_format($qoute->supplierTotalPrice($data['supplier']->id)) }}</h4>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            @endforeach
                            
                  
                
                 
                </div>
            </div>
  </div>


 
@endsection

