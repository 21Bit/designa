@extends('site.includes.layouts.full')

@section('content')
  <div class="page-header-img">
      <!-- <h1 class="text-orange text-center">Notification</h1> -->
      <img src="/site/images/notification-bg.jpg" class="bg-image"  alt="">
      <img class="bottom-pattern" src="/site/images/bottom-pattern.png" alt="">
  </div> 
  <div class="container">
        <div class="row">
                <div class="col-sm-3">
                    @include('site.includes.menus.user-side-menu')
                </div>
                <div class="col-sm-9">
                    <a href="{{ route('user.qoute.create') }}" class="pull-right"><i class="fa fa-plus"></i> Request New Qoute</a>
                    <h4 class="title mb-4">
                        Quotes
                    </h4>
                    <div class="bg-white quote-table" >
                        <table>
                            <thead>
                                <tr>
                                    <th>Event Name</th>
                                    <th>Event Type</th>
                                    <th>Number of Decor</th>
                                    <th>Created On</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($quotes as $quote)
                                    <tr>
                                        <td>
                                            <a href="{{ route('user.qoute.show', $quote->reference_number) }}">
                                                {{ $quote->event_name }}
                                            </a>
                                        </td>
                                        <td>{{ $quote->event_type }}</td>
                                        <td>{{ $quote->decors()->count() }}</td>
                                        <td>{{ $quote->created_at }}</td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="4" align="center">No Quote Yet..</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                       
                    </div>
                    <br>
                     {{ $quotes->links() }}
                 
                </div>
            </div>
  </div>


 
@endsection
