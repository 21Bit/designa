@extends('site.includes.layouts.full')

@section('content')
  <div class="page-header-img">
      <!-- <h1 class="text-orange text-center">Notification</h1> -->
      <img src="/site/images/notification-bg.jpg" class="bg-image"  alt="">
      <img class="bottom-pattern" src="/site/images/bottom-pattern.png" alt="">
  </div> 
  <div class="container">
        <div class="row">
                <div class="col-sm-3">
                    @include('site.includes.menus.user-side-menu')
                </div>
                <div class="col-sm-9 mb-5">
                    <a href="{{ route('user.qoute.index') }}" class="pull-right"><i class="fa fa-ban"></i> Cancel</a>
                    <h4 class="title mb-4">
                        Quotes
                    </h4>
                    <create-qoute-component route="{{ route('user.qoute.list.filter') }}" storeroute="{{ route('user.qoute.store') }}"></create-qoute-component>
                 
                </div>
            </div>
  </div>


 
@endsection
