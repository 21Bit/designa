@extends('site.includes.layouts.full')

@section('content')
  <div class="page-header-img">
      <img src="/site/images/notification-bg.jpg" class="bg-image"  alt="">
      <img class="bottom-pattern" src="/site/images/bottom-pattern.png" alt="">
  </div> 
  <div class="container">
        <div class="row">
            <div class="col-sm-3">
                @include('site.includes.menus.user-side-menu')
            </div>
            <div class="col-sm-9 site-notification-page">
                <div class="btn-group pull-right" role="group" aria-label="Basic example">
                    <button type="button" class="btn btn-outline-secondary">Mark All Read</button>
                    <button type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#delete-all-notification-modal"><i class="fa fa-trash"></i> Delete All</button>
                </div>
                <h4 class="title mb-4">
                    Notifications
                </h4>
                <div class="clearfix"></div>
                    @forelse($notifications as $notification)
                        <div class="media" id="div-{{ $notification->id }}">
                            <img class="mr-3 mb-0 rounded-circle" src="{{ $notification->data['avatar'] }}"  alt="Generic placeholder image">
                            <div class="media-body">
                                <a href="{{ $notification->data['link'] }}?notif={{$notification->id}}">
                                <h5 class="mt-0 mb-0">{{ $notification->data['title'] }}</h5>
                                </a>
                                {!! $notification->data['message'] !!} 
                                <br>
                                <small><i class="fa fa-clock-o"></i> {{ $notification->created_at->diffforhumans() }}</small>

                                <div class="item-buttons">
                                    <a href="#" data-type="div" data-buttons="delete" data-id="{{ $notification->id }}" data-url="{{ route('user.notification.destroy', $notification->id) }}"><i class="fa fa-trash"></i></a>
                                </div>
                            </div>
                            </div>
                    @empty
                        <h4 class="text-center mt-5 text-muted">No notification yet.</h4>
                    @endforelse
                {{ $notifications->links() }}
            </div>
        </div>
  </div>
    <div class="modal delete-modal" id='delete-modal' tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <div class="icon"><i class="fa fa-trash"></i></div>
                    <h4 class="mb-3">Are you sure?</h4>
                    
                    <p class="text-muted text-bold">
                        This will delete in your notifications.
                    </p>
                    <hr>
                    <div class="text-right">
                        <a href="#" data-dismiss="modal" class="text-muted mr-3">Cancel</a> <button class="btn btn-danger" id='deleteButtonModal' > Delete</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal delete-modal" id='delete-all-notification-modal' tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <div class="icon"><i class="fa fa-trash"></i></div>
                    <h4 class="mb-3">Are you sure?</h4>
                    
                    <p class="text-muted text-bold">
                        This will delete all of your notifications.
                    </p>
                    <hr>
                    <div class="text-right">
                        <a href="#" data-dismiss="modal" class="text-muted mr-3">Cancel</a> <button class="btn btn-danger" data-url="{{ route('user.notification.deletelall') }}" id='deleteAllNotificationButtonModal' > Delete</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
