@extends('site.includes.layouts.full')

@section('content')
  <div class="page-header-img">
      <img src="/site/images/notification-bg.jpg" class="bg-image"  alt="">
      <img class="bottom-pattern" src="/site/images/bottom-pattern.png" alt="">
  </div>
  <div class="container mb-5 event-board">
        <div class="row">
            <div class="col-sm-3">
                @include('site.includes.menus.user-side-menu')
            </div>
            <div class="col-sm-9 site-notification-page">
                <div class="text-center">
                    <h5>
                        {{ $project->title }}
                    </h5>
                    <div class="wizard mt-5">
                        <div class="wizard-inner">
                            <div class="connecting-line"></div>
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="{{ route('user.event-board.edit.categories', $project->id)  }}" aria-controls="step1" role="tab" aria-expanded="true"><span class="round-tab">1 </span> <i>Categories</i></a>
                                </li>
                                <li role="presentation" class="disabled">
                                    <a href="#" aria-controls="step2" role="tab" aria-expanded="false"><span class="round-tab">2</span> <i>Inspirations</i></a>
                                </li>
                                <li role="presentation" class="disabled">
                                    <a href="#" aria-controls="step3" role="tab"><span class="round-tab">3</span><i> &nbsp;&nbsp;&nbsp;Decor</i></a>
                                </li>
                                <li role="presentation" class="disabled">
                                    <a href="#" aria-controls="step4" role="tab"><span class="round-tab">4</span> <i>Overview</i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class='p-3'>
                        <button href="#" form="categories_form" class="pull-right btn btn-warning text-white text-bold circle-side-btn">
                            <i class="fa fa-arrow-right"></i> Save & Proceed
                        </button>
                        <h5 class="mb-3 text-left">
                            Let's get start creating your vission
                        </h5>
                    </div>
                </div>
                <form id="categories_form" action="{{ route('user.event-board.update.categories', $project->id) }}" method="post">
                    @csrf
                    <input type="hidden" name="redirect" value="{{ route('user.event-board.edit.inspirations', ['project' => $project->id, 'saved' => 1]) }}">
                    <div class="mb-3">
                        <div class="mb-2">
                            Themes
                        </div>
                        @foreach($themes as $theme)
                            <div class="checkbox-chips">
                                <input name="themes[]" @if(in_array($theme->id, $selectedCategories)) checked @endif type="checkbox" value="{{ $theme->id }}" id="{{ $theme->id }}">
                                <label for="{{ $theme->id}}"> {{ $theme->name }}</label>
                            </div>
                        @endforeach
                    </div>
                    <div class="mb-3">
                        <div class="mb-2">
                            Product Types
                        </div>
                        @foreach($product_types as $product_type)
                            <div class="checkbox-chips">
                                <input name="product_types[]"  @if(in_array($product_type->id, $selectedCategories)) checked @endif type="checkbox" value="{{ $product_type->id }}" id="{{ $product_type->id }}">
                                <label for="{{ $product_type->id }}"> {{ $product_type->name }}</label>
                            </div>
                        @endforeach
                    </div>
                    <div class="mb-3">
                        <div class="mb-2">
                            Colours
                        </div>
                        @foreach($colors as $color)
                            <div class="checkbox-chips">
                                <input name="colours[]" @if(in_array($color->id, $selectedCategories)) checked @endif type="checkbox" value="{{ $color->id }}" id="{{ $color->id}}">
                                <label for="{{ $color->id }}"> {{ $color->name }}</label>
                            </div>
                        @endforeach
                    </div>
                </form>
            </div>
        </div>
  </div>
@endsection
