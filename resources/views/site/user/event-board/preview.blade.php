@extends('site.includes.layouts.full')

@section('content')
  <div class="page-header-img">
      <img src="/site/images/notification-bg.jpg" class="bg-image"  alt="">
      <img class="bottom-pattern" src="/site/images/bottom-pattern.png" alt="">
  </div>
  <div class="container mb-5 event-board">
        <div class="row">
            <div class="col-sm-3">
                @include('site.includes.menus.user-side-menu')
            </div>
            <div class="col-sm-9 site-notification-page">
                <div class="text-center">
                    <h5>
                        {{ $project->title }}
                    </h5>
                    <div class="wizard mt-5">
                        <div class="wizard-inner">
                            <div class="connecting-line"></div>
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="{{ route('user.event-board.edit.categories', $project->id) }}" aria-controls="step1" role="tab" aria-expanded="true">
                                        <span class="round-tab">1 </span> <i>Categories</i></a>
                                </li>
                                <li role="presentation" class="active">
                                    <a href="{{ route('user.event-board.edit.inspirations', ['saved' => 1, 'project' => $project->id]) }}" aria-controls="step2" role="tab" aria-expanded="false">
                                        <span class="round-tab">2</span> <i>Inspirations</i></a>
                                </li>
                                <li role="presentation" class="active">
                                    <a href="{{ route('user.event-board.edit.decors', ['saved' => 1, 'project' => $project->id]) }}" aria-controls="step3" role="tab">
                                        <span class="round-tab">3</span><i> &nbsp;&nbsp;&nbsp;Decor</i></a>
                                </li>
                                <li role="presentation" class="active">
                                    <a href="#step4" aria-controls="step4" role="tab">
                                        <span class="round-tab">4</span> <i>Overview</i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class='p-3'>
{{--                        @if(request()->get('saved'))--}}
{{--                            <a href="{{ route('user.event-board.edit.decors', $project->id) }}" class="pull-right btn btn-warning text-white mb-2">--}}
{{--                                <i class="fa fa-arrow-right"></i> Save & Proceed--}}
{{--                            </a>--}}
{{--                        @else--}}
{{--                            <a href="#" class="pull-right btn btn-warning text-white mb-2">--}}
{{--                                <i class="fa fa-arrow-right"></i> Save & Proceed--}}
{{--                            </a>--}}
{{--                        @endif--}}
                        <h5 class="mb-3 text-left">
                           Your mood board
                            is now created.
                        </h5>
                        <div class="clearfix"></div>
                        <user-event-board-preview url="{{ route('user.event-board.preview.api', $project->id) }}" />
                    </div>
                </div>

            </div>
        </div>
  </div>
@endsection
