@extends('site.includes.layouts.full')

@section('content')
    <div class="page-header-img">
        <img src="/site/images/notification-bg.jpg" class="bg-image"  alt="">
        <img class="bottom-pattern" src="/site/images/bottom-pattern.png" alt="">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                @include('site.includes.menus.user-side-menu')
            </div>
            <div class="col-sm-9 site-notification-page">
                <div class="text-right mb-2">
                    <button type="button" class="btn btn-warning text-white" data-toggle="modal" data-target="#eventBoardModal">
                        <i class="fa fa-plus"></i> Compose
                      </button>
                </div>
                <table class="table border-0">
                    @forelse($projects as $project)
                        <tr>
                            <td>
                              <a href="{{ route('user.event-board.preview', $project->id) }}" style="display:block;">
                                <div class="col thumbnail-image" style="background-image: url({{ $project->thumbnail }}); padding-top:55%; border-radius:10px; background-size:cover; background-position:center;">
                                </div>
                              </a>
                            </td>
                            <td>
                                <strong>{{ $project->title }}</strong>
                                <p>
                                    {{ Str::limit($project->description, 140) }}
                                </p>
                            </td>
                            <td></td>
                            <td class="text-right">
                                <a href="{{ route('user.event-board.edit.categories', $project->id) }}" class="mr-3">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <a href="#">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td class="text-center">
                                <img src="/images/placeholders/no-data.png" alt="">
                                <h4 class="text-muted mt-3">No Event Board</h4>
                            </td>
                        </tr>
                    @endforelse
                </table>
            </div>
        </div>
    </div>
  <!-- Modal -->
    <div class="modal fade" id="eventBoardModal" tabindex="-1" role="dialog" aria-labelledby="eventBoardModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
                <form action="" method="post" >
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="eventBoardModalLabel">New Event Board</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>
                            <label for="title">Title</label>
                            <input type="text" required name="title" id="title" class="form-control">
                        </p>
                        <p>
                            <label for="description">Description</label>
                            <textarea id="description" rows="5" required name="description" class="form-control"></textarea>
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-warning text-white">Proceed</button>
                    </div>
                </form>
        </div>
        </div>
    </div>
@endsection
