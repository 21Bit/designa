@extends('site.includes.layouts.full')

@section('page-title', $supplier->name)

@section('page-meta')
    <meta name="description" content="{{ Str::limit($supplier->description, 50) }}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="{{ $supplier->name }}">
    <meta property="og:description" content="{{ $supplier->description }}">
    <meta property="og:image" content="{{ url($supplier->image('logo')->path('logos', '/images/placeholders/placeholder.png')) }}">
    <meta property="og:url" content="{{ route('site.supplier.show', $supplier->slug) }}">
@endsection

@section('content')
    <div class="inspiration-header" style="background-image:url('{{ $supplier->image('cover')->path('covers', 'https://via.placeholder.com/1920x450') }}')">
    </div>
    <div id="supplier-page">
        <div class="text-center mb-3">
            <img class="logo img img-thumbnail mb-2" src="{{ $supplier->logo }}" alt="">
            <h2 class="company-name" style='text-transform: none;'>{{ $supplier->name }}</h2>
            {{ $supplier->roleList() }}
            @if(in_array('product', $supplier->permissions()))
             <div class="tag-line">Based in {{ $supplier->country }} @if(in_array('product', $supplier->permissions())) @if($supplier->will_travel == 1) - Will Travel Internationally @endif @endif </div>
            @endif
            {{-- @if($supplier->email)
            <div class="text-center"><i class="fa fa-envelope"></i> {{ $supplier->email }}</div>
            @endif
            @if($supplier->telephone)
                <div class="text-center"><i class="fa fa-phone"></i> {{ $supplier->telephone }}</div>
            @endif --}}
            <div class="social-media">
                @if($supplier->twitter_link)
                    <a href="{{ $supplier->twitter_link }}">
                        <i class="fa fa-twitter"></i>
                    </a>
                @endif
                @if($supplier->facebook_link)
                    <a href="{{ $supplier->facebook_link }}">
                        <i class="fa fa-facebook"></i>
                    </a>
                @endif
                @if($supplier->instagram_link)
                    <a href="{{ $supplier->instagram_link }}">
                        <i class="fa fa-instagram"></i>
                    </a>
                 @endif
                @if($supplier->youtube_link)
                    <a href="{{ $supplier->youtube_link }}">
                        <i class="fa fa-youtube"></i>
                    </a>
                @endif
                @if($supplier->website)
                    <a href="{{ $supplier->website }}">
                        <i class="fa fa-globe"></i>
                    </a>
                @endif
              
            </div>
            @auth
            <heart-component :default="{{ $supplier->hearts()->whereUserId(Auth::user()->id)->first() ? 1 : 0  }}" :id="{{ $supplier->id }}" type="/supplier" styleclass="btn btn-sm btn-outline-secondary btn-lg mt-4"> Save Supplier</heart-component>
            @endif
        </div>
        <div class="container-fluid">

            <div class="line-tab">
                <div class="container">
                    <nav class="mb-3">
                        <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="inspiration-tab" data-toggle="tab" href="#nav-inspiration" role="tab" aria-controls="nav-inspiration" aria-selected="true">INSPIRATIONS</a>
                            {{-- <a class="nav-item nav-link" id="inspiration-tagged-tab" data-toggle="tab" href="#nav-inspiration-tagged" role="tab" aria-controls="nav-inspiration-tagged" aria-selected="true">TAGGED INSPIRATIONS</a> --}}
                            @if(in_array('venue', $supplier->permissions()))
                                @if($supplier->single_venue)
                                    <a class="nav-item nav-link" id="review-tab" data-toggle="tab" href="#nav-venue" role="tab" aria-controls="nav-venue" aria-selected="false">EVENT SPACES</a>
                                @else
                                    <a class="nav-item nav-link" id="review-tab" data-toggle="tab" href="#nav-venue" role="tab" aria-controls="nav-venue" aria-selected="false">VENUES</a>
                                @endif
                            @endif
                            @if(in_array('product', $supplier->permissions()))
                                <a class="nav-item nav-link" id="product-tab" data-toggle="tab" href="#nav-product" role="tab" aria-controls="nav-product" aria-selected="false">PRODUCT</a>
                            @endif
                            <a class="nav-item nav-link" id="review-tab" data-toggle="tab" href="#nav-review" role="tab" aria-controls="nav-review" aria-selected="false">REVIEWS</a>
                            <a class="nav-item nav-link" id="about-tab" data-toggle="tab" href="#nav-about" role="tab" aria-controls="nav-about" aria-selected="false">ABOUT</a>
                        </div>
                    </nav>
                </div>
				<div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
					<div class="tab-pane fade show active" id="nav-inspiration" role="tabpanel" aria-labelledby="inspiration-tab">
                        <div class="row">
                            <div class="col-10">
                                <supplier-inspiration-component
                                    default-category="" 
                                    default-category-parent=""
                                    istagged="true"
                                    supplier="{{ $supplier->id }}"
                                    isvenue="{{ in_array('product', $supplier->permissions()) }}"
                                >
                                </supplier-inspiration-component>
                            </div>
                        </div>
                        
					</div>
					<div class="tab-pane fade show" id="nav-inspiration-tagged" role="tabpanel" aria-labelledby="inspiration-tab">
                        <inspiration-tagged-component
                            istagged="true"
                            supplier="{{ $supplier->id }}">
                        </inspiration-tagged-component>                        
					</div>
                    @if(in_array('venue', $supplier->permissions()))
                        <div class="tab-pane fade" id="nav-venue" role="tabpanel" aria-labelledby="venue-tab">
                            @if($supplier->single_venue)
                                <div class="container">
                                    <div class="row">
                                        @forelse($supplier->singleVenueEventSpaces() as $functionspace)
                                            <div class="col-12 shadow-sm mb-3 bg-white ">
                                                <div class="row shadow-sm p-2">
                                                    <div class="col-md-3 shadow-sm rounded" style="background-color:#e1e1e1 ;padding-top: 20%;  background-position:center; background-repeat:no-repeat;   background-size:cover; background-image: url('{{ $functionspace->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder250x200.png') }}')">
                                                    
                                                    </div>
                                                    <div class="col-md-9">
                                                            <h4 class="mb-0 d-block mt-3 mb-2 text-bold">
                                                                {{ $functionspace->name }}
                                                            </h4>    
                                                            <span class='mr-3'>
                                                                Max Seated: {{ $functionspace->max_capacity_seated }} 
                                                            </span>
                                                            <span>
                                                                Max Standing: {{ $functionspace->max_capacity_standing }} 
                                                            </span>
                                                            <div class="pt-3">
                                                                <small class="text-muted">
                                                                    {!! nl2br($functionspace->description) !!}
                                                                </small>
                                                            </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @empty
                                            <div class="col-12 text-center p-5">
                                                <img src="/images/placeholders/no-data.png" alt="">
                                                {{-- <strong class="text-muted mt-5">No function space</strong> --}}
                                            </div>
                                        @endforelse
                                    </div>
                                </div>
                            @else
                                <div class="row">
                                    <div class="col-sm-11">
                                        <supplier-venue-filter-component  supplier="{{ $supplier->id }}"></supplier-venue-filter-component>
                                    </div>
                                </div>
                            @endif
                        </div>  
                    @endif
                    @if(in_array('product', $supplier->permissions()))
                        <div class="tab-pane fade" id="nav-product" role="tabpanel" aria-labelledby="product-tab">
                            <div class="row">
                                <div class="col-sm-11">
                                    <supplier-product-filter-component  supplier="{{ $supplier->id }}"></supplier-product-filter-component>
                                </div>
                            </div>
                        </div>  
                    @endif
					<div class="tab-pane fade" id="nav-review" role="tabpanel" aria-labelledby="review-tab">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-8 offset-sm-2">
                                    <review-component  type="supplier" :id="{{ $supplier->id }}"></review-component>
                                </div>
                            </div>
                        </div>
					</div>
					<div class="tab-pane fade" id="nav-about" role="tabpanel" aria-labelledby="about-tab">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-8 offset-sm-2">
                                    @if($supplier->description)
                                    <div class="bg-white p-3 rounded mb-3">
                                        <small class="d-block border-bottom pt-0 pb-2" for="">Description</small> 
                                        {{ $supplier->description  }}
                                    </div>
                                    @endif
                                    <div class="bg-white p-3 rounded mb-3">
                                        <small class="d-block border-bottom pt-0 pb-2" for="">Based in</small> 
                                        {{ $supplier->town }} {{ $supplier->country  }}
                                    </div>
                                    @if($supplier->services)
                                        <div class="bg-white p-3 rounded mb-3">
                                            {{-- <small class="d-block border-bottom pt-0 pb-2" for="">Will Travel Interationally</small> 
                                            {{ $supplier->will_travel }}  --}}
                                            Will Travel Internationally
                                        </div>
                                    @endif
                                    @if($supplier->services)
                                        <div class="bg-white p-3 rounded mb-3">
                                            <small class="d-block border-bottom pt-0 pb-2" for="">Services</small> 
                                            {{ $supplier->services }} 
                                        </div>
                                    @endif
                                    @if($supplier->recognition)
                                        <div class="bg-white p-3 rounded mb-3">
                                            <small class="d-block border-bottom pt-0 pb-2" for="">Recognition</small> 
                                            {{ $supplier->recognition }} 
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
					</div>
				</div>
            </div>
        </div>
    </div>
@endsection