@extends('site.includes.layouts.full')

@section('page-title', 'Suppliers')

@section('content')
<div class="page-header-img mb-4">
    <h1 class="text-orange">Supplier</h1>
    <img class="bg-image" src="/site/images/supplier.png" alt="">
    <img class="bottom-pattern" src="/site/images/bottom-pattern.png" alt="">
</div>

<suplier-filter-component default-category="{{ $defaultCategory }}" ></suplier-filter-component>


@endsection
