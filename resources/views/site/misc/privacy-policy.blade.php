@extends('site.includes.layouts.full')

@section('page-title', 'Privacy Policy')

@section('content')
    <!-- <div class="page-header-img mb-4">
        <h1 class="text-black text-center">Decors</h1>
        <img class="bg-image" src="/site/images/decor-header.jpg" alt="">
        <img class="bottom-pattern" src="/site/images/bottom-pattern.png" alt="">
    </div> -->
    
    <div class="container">
        <h1 class="text-center m-5">Privacy Policy</h1>

        <h4 class='text-black'>Respecting your Privacy</h4>
        <p>
            Designa Studio is committed to supporting the National Privacy Principles for all fair handling of Personal Information which set clear standards for the collection, use, disclosure, storage of and access to Personal Information collected during the course of business operations and compliance with the Privacy Act 1988 (Commonwealth) and its amendments (the Act), and the Australian Privacy Principles (APPs) set out in that Act. You can see the full text of the APPs online at the Office of the Australian Information Commissioner’s website at:
        </p>

        <p>
            <a href="http://www.oaic.gov.au/privacy/privacy-act/the-privacy-act">http://www.oaic.gov.au/privacy/privacy-act/the-privacy-act </a> 
        </p>

        <p>
            Personal Information means your name, mailing or residential address, telephone number, email address and other transaction and registration details including the company you work for and your role and financial information, including credit card information and information collected from credit reporting agencies.
        </p>
        
        <p>
            Our respect for your rights to the privacy of your personal information is paramount. We have policies and procedures in place to ensure that all personal information, no matter how or where it is obtained, is handled sensitively, securely and in accordance with the APPs.
            This Privacy Policy sets out:
            <ul>
                <li>What sort of personal information is collected and stored;</li>
                <li>How information is collected and used;</li>
                <li>What happens if you choose not to provide the required information; and</li>
                <li>How you can request access to the personal information we hold about you.</li>
            </ul>
        </p>
        <p>
            From time to time it may be necessary for Designa Studio to review and revise this Privacy Policy. We reserve the right to change our policy at any time. Amendments or replacements of the Privacy Policy will be posted on the website.
        </p>



        <h4 class="mt-5 text-black"> 
            Collection of Your Personal Information
        </h4>
        <p>
            We only collect personal information relevant to our business relationship with you so that we can provide our services and products. We may collect your personal information:
            <ul>
                <li>(a)directly from you including via telephone or email;</li>
                <li>(b)when your register on our website;</li>
                <li>(c)when you place an order or service request with us;</li>
                <li>(d)where you access and interact with our website; and</li>
                <li>(e)from other sources including a third party such as credit reporting.</li>
            </ul>
        </p>


        <h4 class="mt-5 text-black">
            Is your personal information secure?
        </h4>
        <p>
            Where you submit personal information, we manage it by using up-to-date techniques and processes that meet current industry standards to ensure that your personal information is kept secure and confidential. We also take measures to destroy or permanently de-identify personal information if it is no longer required.
        </p>


        <h4 class="mt-5 text-black">
                Use and Disclosure of Personal Information
        </h4>
        <p>
            You acknowledge and consent that by providing your personal information to us that we may use and disclose your personal information for the purpose for which it was collected or for a related or ancillary purpose such as:
            <ul>
                <li>(a)supply of our services to you in accordance with our Terms and Conditions of Hire and use, to facilitate, process, carry out and respond to your request;</li>
                <li>(b)to our third party service providers to assist us in providing and improving our services to you and developing, improving and marketing our services to you;</li>
                <li>(c)for regulatory reporting and compliance with our legal obligations, to various regulatory bodies and law enforcement officials and agencies to protect against fraud and for related security purposes;</li>
                <li>(d)to the manufacturer, importer, distributor, seller or supplier of any goods hired to you for the purpose of ascertaining and obtaining any information required to perform our services for you;</li>
                <li>(e)to identify you and conduct appropriate checks, including credit checks;</li>
                <li>(f)set up, administer and manage our services and products and systems, including the management and administration of an account; and</li>
                <li>(g)to our successors and/or assigns.</li>
            </ul>
        </p>


        <h4 class="mt-5 text-black">
            What happens if you do not want to disclose personal information?
        </h4>
        <p>
            You have no obligation to provide any personal information requested by us, but if you choose to withhold personal information, we may not be able to provide you with the products and services that depend on the collection of that information.
        </p>
        

        <h4 class="mt-5 text-black">
            Providing Personal Information About Another Person
        </h4>
        <p>
            You represent to us that where you provide personal information to us about another person, you are authorised to provide that information to us, and that you will inform that person who we are, how we use and disclose their information, and that they can gain access to that information.
        </p>


        <h4 class="mt-5 text-black">Linked Sites</h4>
        <p>
            Designa Studio website may contain links to websites which are owned or operated by other parties. You should make enquiries as to the privacy policies of these parties. We are not responsible for information, or the privacy practices of such websites.
        </p>

        <h4 class="mt-5 text-black">How can you access your personal information?</h4>
        <p>You may request access to personal information that we hold about you. We will provide you with access to personal information in accordance with the Act and APPs and we may not grant you access to the personal information that we hold where the APPs allow us to do so. If you are refused access to information, we will provide you with reasons for the refusal.</p>


        <h4 class="mt-5 text-black">How to contact us</h4>
        <p>
                If you have any questions in relation to privacy, please contact 0423 635 155. For more information about privacy law please visit <a href="http://www.oaic.gove.au/privacy" target="_blank">http://www.oaic.gove.au/privacy</a>. 
        </p>

    </div>   
    <div class="mt-4 text-black"></div>
@endsection