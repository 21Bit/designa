@extends('site.includes.layouts.full')

@section('page-title', 'Terms and Conditions')

@section('content')

    <div class="container">
        <h1 class="text-center m-5">Designa Studio Terms and Conditions</h1>
        
        <h5>Effective 15 June 2021</h5>

        <h4 class='text-black'>Information about these Terms</h4>
        <ol>
            <li>Designa Studio Pty Ltd, (Designa, we or our), is the operator of various subscription services made available in Australia from time to time (the/a Designa Service).</li>
            <li>Users of a Designa Service (users or you) agree to be bound by these terms and conditions (Terms).  These Terms apply to each Designa Service you subscribe to and your use of, and access to, a Designa Service indicates your acceptance of these Terms (and any changes to them).</li>
        </ol>

        <h4>Changes</h4>
        <ol start="3">
            <li>These Terms and any Subscription Fee are subject to change by Designa on 21 days’ notice to you. If any change to these Terms or a Subscription Fee has a detrimental impact to you, you may cancel your subscription to the Designa Service in accordance with clause 33 below.</li>
            <li>The Designa Services, including content that is made available to via the Designa Services, is updated regularly and may change from time to time. Where we make a material change to the Designa Service which has a detrimental impact, we will provide you with 21 days’ notice prior to making that change where reasonably practicable. A change for which we will endeavour to provide you notice under this clause 4 includes removing a module without providing a similar replacement module or reducing the price you pay. If it has a detrimental impact to you, you may cancel your subscription to the Designa Service in accordance with clause 33 below.</li>
        </ol>

        <h4>Accessing a Designa Service</h4>
        <ol start="5">
            <li>To join a Designa Service you must be at least 18 years old and you must register or hold a Designa account.  You can access the content within the Designa Service from Australia only. You acknowledge that you cannot access or view the content within the Designa Service from anywhere outside of Australia.</li>
            <li>Users of a Designa Service are responsible for all costs arising from the use of the Designa Service including, without limitation, all subscription, browsing/data charges and internet costs. Subscription Fees do not include data costs that are dependent on how much and how you download content and your agreement with your provider. Please contact your ISP or carrier network provider about these costs.</li>
            <li>You must ensure, before you subscribe to a Designa Service, that your internet connection speed is appropriate for viewing content and that your personal computer, tablet, mobile devices, operating system and any other associated connected equipment or property (including your television) are compatible. Designa software may vary by device and medium, and functionalities and features may also differ between devices.</li>
            <li>The number of modules you can use across your devices to view a Designa Service will depend on your Subscription Plan.</li>
            <li>A Designa Service (or a particular Subscription Plan) may implement a limit on the number of devices that may be registered to your account at any one time.  Designa may monitor the number of devices registered to a Designa account.</li>
            <li>When you use a Designa Service, some information will be automatically collected through the use of cookies to assist us in making your user experience of the Designa Service more enjoyable. Designa Services cannot operate if you set your browser to reject all cookies.</li>
            <li>You will provide us with proof of your identity if we reasonably ask you to do so from time to time.</li>
        </ol>

        <h4>Subscriptions and fees</h4>
        <ol start="12">
            <li>You can subscribe to a Designa Service by purchasing a subscription plan for a Designa Service (Subscription Plan) for a specified Subscription Period displayed at the time of purchase (Subscription Period) via a Designa Service website, application or via Designa approved third party retailers.</li>
            <li>The price for each Subscription Plan will be displayed at the time of purchase including, where applicable, any introductory pricing for a specified period (Subscription Fee). You must pay the Subscription Fee in advance of each Subscription Period. If you choose to pay via a third party biller, eg Apple, your Subscription Fee will be charged to your third party billing account.</li>
            <li>Per clause 3 above, we may change your Subscription Fee at any time by giving at least 21 days' notice to you.  We may also change your Subscription Fee without notice if required by law, or if any regulatory authority requests or requires a change to any part of our pricing which directly affects your Subscription Fee or our pricing structure (if this happens we will use best endeavours to give you reasonable notice).</li>
        </ol>

        <h4>Auto-renewal and cancellation</h4>
        <ol start="15">
            <li>Your Subscription Plan will automatically renew at the end of each Subscription Period for a further Subscription Period, unless you cancel your Subscription Plan. You may cancel your Subscription Plan through your Designa account at any time prior to your renewal date, or as otherwise specified by us or one of our third party billing partners. For Subscription Plans purchased through Apple or Android marketplaces you must cancel through your Apple or Android account at least 24 hours prior to your renewal date. If you do not cancel by the relevant time we will automatically renew your membership and your nominated account/credit card will be charged for the next Subscription Period.</li>
            <li>Cancellation of your auto-renewal will only be effective at the end of the current Subscription Period and you will continue to have access to the Designa Service until the end of the relevant Subscription Period.  No pro-rata refunds will be provided to customers that cancel during a Subscription Period.</li>
        </ol>

        <h4>Manage your Subscription Plan</h4>
        <ol start="17">
            <li>If you subscribe to a Designa Service and you wish to change your Subscription Plan, you can manage your Subscription Plan through your Designa account or, where applicable, via our third party sales and billing partners. For Subscription Plans purchased through Apple or Android marketplaces you can manage your Subscription Plan in the ‘Account Settings’ section of your Apple or Android Account.</li>
            <li>The following clause 18 only applies if you purchased your Subscription Plan through a Designa Service website or application.  If you wish to upgrade your subscription, you will be able to access your new Subscription Plan immediately.  If you upgrade during your current Subscription Period, the new Subscription Fee will be applied pro-rata for the remainder of the Subscription Period (unless you are in a free trial period) and you will be charged this amount immediately upon upgrade.  The full Subscription Fee for your new Subscription Plan will be charged in advance at the start of your next Subscription Period.  If you wish to downgrade your subscription, the downgrade will take effect at the end of your current Subscription Period (or free trial if applicable) and no pro-rata refunds will be provided. You will be charged your new Subscription Fee at the start of your next Subscription Period.</li>
        </ol>

        <h4>Free trials and promotions</h4>
        <ol start="19">
            <li>Designa may offer free trial periods to a Designa Service from time to time.  During a free trial period, you will not be required to pay any Subscription Fee.  Please note that your credit card may be charged a small nominal amount to verify your credit card but this verification charge will be refunded without you needing to take any further action. You may cancel your free trial subscription, in accordance with clause 15. If you do not cancel your free trial subscription, you will be charged the Subscription Fee in accordance with the Subscription Plan that you selected during your initial registration for the free trial.</li>
            <li>Designa may also offer other promotional discounts or benefits (Promotions).  We reserve the right to offer, withdraw, change, cancel or determine your eligibility for any Promotion or free trial in our discretion for the purposes of preventing abuse of the Promotion or free trial. Promotions and free trials may be subject to terms and conditions which shall apply in addition to, and prevail to the extent of any inconsistency with, these Terms.</li>
        </ol>

        <h4>Credits / refunds / vouchers</h4>
        <ol start="21">
            <li>If you are eligible for a credit or refund, or redeem a Designa Service gift card, the credit will apply to your Designa account and, if you subscribe to multiple Designa Services, it will be applied to the next Designa Service for which you are billed.</li>
            <li>If you are eligible for a Designa Service voucher, the voucher will be applied to that Designa Service.</li>
        </ol>

        <h4>Third Party Billing</h4>
        <ol start="23">
            <li>If you elect to be billed for your Designa Service via a Designa approved third party biller, your Subscription Fee will be charged by that third party and may be subject to additional third billing party terms which will be notified to you by the third party biller.</li>
        </ol>

        <h4>Permitted Use of a Designa Service</h4>
        <ol start="24">
            <li>You must follow our reasonable instructions about how you use the Designa Services. You must not use a Designa Service for any improper or unlawful purpose and you must not allow anyone else to do the same.</li>
            <li>You must not circumvent, disable or otherwise interfere with security-related features of the Service or any features which prevent or restrict the use, distribution or copying of a Designa Service or any content within a Designa Service.</li>
            <li>You must not alter or modify the content or Designa Service in any way.</li>
            <li>You are not permitted to copy, record, reproduce, republish, post, broadcast, upload, communicate or make any of the content of a Designa Service available to any other person or authorise or assist anyone else to do so, unless we permit you to do so.</li>
            <li>You must not permit another person to use your Designa Service.</li>
            <li>You are solely responsible and liable for activity that occurs on your Designa account and you are responsible for maintaining the confidentiality of your password and Designa account details.</li>
            <li>You are responsible for keeping your Designa account passwords secure and confidential and you must not permit another person to use your Designa account password to access and use a Designa Service. If you know or suspect that your password has been compromised, or you suspect or become aware of any other breach of security, you must tell us immediately. In these circumstances, you must also ensure that your password is changed as soon as possible.</li>
            <li>If we reasonably believe that there has been, or is likely to be, a breach of security of your password, we may block or reset your password and require you to reset your password.</li>
        </ol>

        <h4>Suspension and cancellation</h4>
        <ol start="32">
            <li>Designa may cancel a Designa Service at any time and will provide you with reasonable notice prior to cancellation.  Where cancellation by us is due to your breach of our Terms, we are under no obligation to provide any refund or provide notice.</li>
            <li>You may notify us that you wish to cancel as a result of us changing these Terms, a Subscription Fee or a Designa Service in a way that has a detrimental effect on you.  If you do so, your cancellation will be effective as of the date of the change or your notice to us, and you will receive a pro-rata refund for any amount already paid to us in respect of any period after that date.</li>
            <li>Designa may suspend or restrict your use of or access to all or any of a Designa Service in the following circumstances:</li>
                <ol type="a">
                    <li>where necessary for technical or operational reasons;</li>
                    <li>where we have been directed to by a content provider;</li>
                    <li>where we reasonably believe a Designa Service is unsecure or is being used in breach of these Terms or is inconsistent with the requirements of our content providers, including any violation of the limited licence granted to you at clause 35 below;</li>
                    <li>if any payments owed to us have not been paid by the due date;</li>
                    <li>if you breach this Agreement and if the breach is able to be remedied, you do not remedy the breach within a reasonable period of receiving notice from us requiring you to do so;</li>
                    <li>if we consider on reasonable grounds that you, or someone else using your Designa account, have committed or may be committing any fraudulent activity against us or against any other person or organisation through your or their use of a Designa Service;</li>
                    <li>we believe on reasonable grounds that you have caused genuine distress to, or have exhibited inappropriate behaviour towards any of Designa’s employees, agents or contractors, community including but not limited to behaviour that is harassing, indecent, abusive and/or offensive;</li>
                    <li>we believe on reasonable grounds that you have made multiple complaints without a reasonable basis for doing so and you continue to make such complaints after we have made reasonable efforts to notify you to stop; or</li>
                    <li>where we are required to do so by law or to comply with (or manage our compliance with) a notice, order, direction or request of a regulator, one of our content suppliers or an emergency services organisation.</li>
                </ol>
        </ol>

        <h4>Intellectual Property Rights</h4>
        <ol start="35">
            <li>Copyright, trade marks and intellectual property rights in all material, content or software supplied as part of a Designa Service, are owned by Designa or where applicable its licensors, content partners or advertisers, and users obtain no interest in those rights.  We grant you a non-exclusive, non-transferable, non-sublicensable, revocable limited licence to use such rights for your personal use of a Designa Service only, in accordance with these Terms.</li>
            <li>Except for the foregoing limited license, no right, title or interest in any material, content or software supplied as part of a Designa Service shall be deemed transferred to you, and there shall be only a limited licence and not a sale with respect to any material, content or software supplied as part of a Designa Service.</li>
            <li>All content on a Designa Service is protected by Australian and international copyright and other intellectual property laws.  You may not do anything which interferes with or breaches those laws or intellectual property rights in the content of the Designa Service.</li>
            <li>You will not, and you will not assist, facilitate or authorise any third party to copy, transfer, publish, rent, reproduce, record, upload, communicate, frame, reverse engineer, decrypt, decompile, disassemble, alter or commercially exploit a Designa Service or any material or content you obtain from a Designa Service;</li>
            <li>Content owners (including Designa) use content access technologies to protect their intellectual property, including copyrighted content.  If the access technology fails to protect the content, content owners may require Designa to restrict or prevent the delivery of protected content to specified devices or software applications.  In certain cases, you may be required to upgrade the content access technology to continue to access the Designa Service.  If you decline such an upgrade, you will not be able to access content that requires the upgrade.</li>
            <li>The Designa Services contains technology subject to certain intellectual property rights of the copyright owner of the relevant technology (e.g. Microsoft, Apple or Android or Google).  Use or distribution of this technology outside of a Designa Service is prohibited without the appropriate license from the applicable owner of the intellectual property rights.</li>
        </ol>

        <h4>Third party content</h4>
        <ol start="41">
            <li>The Designa Services may include third party content, which is subject to that third party’s terms and conditions of use.  We encourage you to read those third party terms and conditions of use prior to accessing such third party content.  As Designa has no control over such content, you acknowledge and agree that Designa is not responsible for the availability of the third party content and that Designa neither endorses nor is responsible for any such content, including advertising, products or other materials.  Designa shall not be responsible or liable for any loss or damage of any kind incurred by you as the result of any such dealings or as the result of the presence of such third parties on a Designa Service.</li>
        </ol>

        <h4>User Generated Material</h4>
        <ol start="42">
            <li>A Designa Service may contain facilities (including facilities provided via social media sites such as Twitter and Facebook) that enable users to interact with other users and post and/or upload or communicate content (including text, audio, video and photographs) (User Generated Material).</li>
            <li>You are responsible for any User Generated Material that you post, upload, communicate or otherwise make available via a Designa Service. You must not post, upload, communicate or otherwise make available User Generated Material:</li>
                <ol type="a">
                    <li>which is abusive, defamatory, obscene, profane or vulgar;</li>
                    <li>which is likely to offend, insult, humiliate or intimidate others based on nationality, gender, age, race, religion, sexual orientation, or any disability;</li>
                    <li>which is not your original work, or which violates or infringes the rights of any other person, including material which is protected by copyright, trade mark or other intellectual property rights;</li>
                    <li>which is false, misleading or deceptive;</li>
                    <li>in a way that impersonates any person or entity or otherwise misrepresents your sponsorship, approval or affiliation with a person or entity; or</li>
                    <li>which violates any law or regulation.</li>
                </ol>
            <li>Designa assumes no responsibility for monitoring the Designa Services for inappropriate User Generated Material. If at any time Designa elects, in its sole discretion, to monitor User Generated Material on a Designa Service, Designa nonetheless assumes no responsibility for any User Generated Material and no obligation to modify or remove any inappropriate User Generated Material. Designa may reject, refuse to post or delete any User Generated Material for any or no reason.</li>
            <li>We make no warranties or representation and accept no liability in relation to any User Generated Material including its content, adequacy, completeness or accuracy.</li>
            <li>You grant to Designa, its assignees and authorised nominees a non-exclusive, perpetual, worldwide, transferable and royalty free licence to use, publish, display, modify, perform, adapt, communicate, reproduce, exploit, exhibit and in all media throughout the world any User Generated Material posted, uploaded, communicated or otherwise made available by you via a Designa Service.</li>
            <li>To the extent permitted by applicable law, you unconditionally and irrevocably:</li>
                <ol type="a">
                    <li>consent, and will obtain all other necessary unconditional and irrevocable written consents from any persons involved in producing any User Generated Material, to any act or omission that would otherwise infringe any moral rights in any User Generated Material which is posted, uploaded, communicated or otherwise made available by you via a Designa Service; and</li>
                    <li>waive, and will obtain all other necessary unconditional and irrevocable written waivers of, all moral rights.</li>
                </ol>
        </ol>

        <h4>Competitions</h4>
        <ol start="48">
            <li>Terms and conditions of entry for any promotion or competition (Competition Terms) conducted via a Designa Service will be accessible on the Designa Service. You should read those Competition Terms in conjunction with these Terms.</li>
        </ol>
        
        <h4>Liability</h4>
        <ol start="49">
            <li>You will be responsible for all actions and omissions of any person who uses your Designa Service account or a Designa Service you subscribe to.</li>
            <li>
                Nothing in these Terms excludes, restricts or modifies any rights that you have under existing laws or regulations and codes, including the Competition and Consumer Act 2010 (Cth) and fair trading laws. The Designa Service provided to you under these Terms comes with guarantees that cannot be excluded under the Australian Consumer Law set out in Schedule 2 of the Competition and Consumer Act 2010 (Cth). For major failures with the Designa Service, you are entitled:
                    <ul>
                        <li>to cancel your Designa Service contract with us; and</li>
                        <li>to a refund for the unused portion, or to compensation for its reduced value.</li>
                    </ul>
                You are also entitled to be compensated for any other reasonably foreseeable loss or damage. If the failure does not amount to a major failure you are entitled to have problems with the Designa Service rectified in a reasonable time and, if this is not done, to cancel your contract and obtain a refund for the unused portion of the contract.
            </li>
            <li>Apart from any rights you have that cannot be lawfully excluded, we do not make any promises or assurances to you about a Designa Service and/or these Terms. To the maximum extent permitted by law, all other terms, conditions and warranties, whether express or implied by legislation or the common law or otherwise relating to the provision by us of a Designa Service, or otherwise in connection with these Terms, are expressly excluded.</li>
            <li>Where permitted under local law, including the Australian Consumer Law, the liability, excluding any liability arising from gross negligence, wilful misconduct and/or fraud, of Designa, its related bodies corporate, suppliers, agents and contractors for any breach of a term or condition implied by law is limited at Designa’s discretion, to the supply of any service again or the payment of the cost of having any service supplied again.  However, Designa cannot limit its liability in this manner if you establish that it would not be fair or reasonable for Designa to do so.</li>
            <li>Except where we have breached any of your rights under the Australian Consumer Law or any other applicable legislation which cannot be lawfully excluded or limited, we are not responsible in any way for any indirect or consequential loss that arises out of or in connection with this Agreement or the Designa Service.</li>
            <li>The information within a Designa Service is not comprehensive and is intended only to provide a summary of the subject matter covered. Designa does not undertake to keep the Designa Services up-to-date and is not liable to you or any other person for any loss or disappointment suffered if a Designa Service or content within it is not available or the content within the Designa Service is incorrect, incomplete or not up-to-date. You must make your own assessment of it and rely on it wholly at your own risk.</li>
            <li>In addition to any rights against you that we may have under these Terms, you will be liable to Designa and must keep us, our related bodies corporate, suppliers, agents and contractors indemnified against any loss, cost, expenses, damage or other liability (including legal costs on a solicitor/client basis) arising out of any claim or demand against us by you or any person other than you, which arises from or is connected with our supply or removal of a Designa Service to you or your use of a Designa Service, unless and to the extent that any loss, cost, expense, damage or other liability is caused by or contributed to our, our related bodies’, suppliers’, agents’ or contractors’ wilful default, negligence or breach of these Terms or any law.</li>
            <li>
                You agree to fully indemnify and hold harmless Designa and its related bodies corporate, suppliers, agents and contractors against any and all liabilities, claims (actual, threatened or potential), loss (including consequential loss) and damage incurred in connection with and arising from:
                    <ul>
                        <li>your use of or access to a Designa Service in breach of these Terms; or</li>
                        <li>User Generated Material which you post, upload, communicate or otherwise make available.</li>
                    </ul>
                This indemnity will survive these Terms and your use of the Designa Services.
            </li>
        </ol>

        <h4>Privacy and Data</h4>
        <ol start="57">
            <li>By entering into these Terms, you consent to us collecting, using and disclosing your Personal Information on the terms of these Terms and Designa’s Privay Policy</li>
        </ol>

        <h4>Classification</h4>
        <ol start="58">
            <li>You accept full responsibility for reviewing all classification information supplied for each piece of content for the purpose of informing, and where appropriate safeguarding, other viewers of the content.  Where you allow children or young people to view content via a Designa Service, you are responsible for making sure that the content is suitable for them.</li>
            <li>When you set up your Designa Service and Designa account, you are applying to access Restricted Content and you must provide a declaration that you are over 18 years of age in order to access such Restricted Content.</li>
        </ol>

        <h4>Promotion and Advertising</h4>
        <ol start="60">
            <li>Your dealings with, or participation in promotions by, any third party advertisers on or through a Designa Service are solely between you and such third party.  You agree that we shall not be responsible or liable for any loss or damage of any kind incurred by you as the result of any such dealings or as the result of the presence of such third parties on a Designa Service.</li>
        </ol>

        <h4>Notices and Revisions</h4>
        <ol start="61">
            <li>If we give you any notice that is required under these Terms, we shall give it to you by electronic communication or via a Designa Service website or a Designa Service.</li>
            <li>You must provide us with accurate, true and correct contact details and you must keep this information up to date.  Please ensure you notify us of any changes to your information by updating your contact information on your Designa Account.</li>
        </ol>

        <h4>Notices and Revisions</h4>
        <ol start="63">
            <li>We can transfer our rights and obligations under these Terms to any third party, provided such transfer does not detrimentally affect your rights under these Terms.</li>
            <li>All or any part of any term of these Terms that is found to be unfair or unenforceable will be treated as deleted and the remainder of the terms will continue to govern each of our respective obligations going forward.</li>
            <li>Your use of the Designa Services and these Terms is governed by the laws in force in New South Wales, Australia. You submit to the non-exclusive jurisdiction of the courts exercising jurisdiction in New South Wales, Australia.</li>
        </ol>

    </div>   
    <div class="mt-4 text-black"></div>
@endsection