@extends('site.includes.layouts.full')
@push('styles')
  <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyBMnJ6SDTRTpOca3b0RD3VYGdgl_WWtuak"></script>
@endpush
@section('content')

  <div class="page-header-img mb-4">
       <h1 class="text-orange">Inspiration</h1>
      <img src="/site/images/inspiration-header.png" class="bg-image"  alt="">
      <img class="bottom-pattern" src="/site/images/bottom-pattern.png" alt="">
  </div>
        
  <inspiration-filter-index-component default-category="{{ $defaultCategory }}" default-category-parent="{{ $defaultCategoryParent }}"></inspiration-filter-index-component>
  
  <div class="mb-3"></div>
@endsection
