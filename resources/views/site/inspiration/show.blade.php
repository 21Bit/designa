@extends('site.includes.layouts.full')

@section('page-title', $inspiration->name)

@section('page-meta')
    <meta name="description" content="{{ Str::limit($inspiration->description, 150) }}">
    <meta property="og:type" content="website">
    <meta property="og:title" content="{{ $inspiration->name }}">
    <meta property="og:description" content="{{ Str::limit($inspiration->description, 50) }}">
    {{-- <meta property="og:image" content="{{ url($inspiration->thumbnail) }}"> --}}
    <meta property="og:image" content="https://api.screenshotmachine.com/?key=715884&url={{ route('site.inspiration.show', $inspiration->slug) }}&dimension=1024x768">
    <meta property="og:url" content="{{ route('site.inspiration.show', $inspiration->slug) }}">
@endsection

@section('content')
    
    <div class="inspiration-header" style="background-image:url('{{ $inspiration->cover }}'); background-size:cover !important;">
        <div class="header-box-inspiration">
            <h1>{{ $inspiration->name }}</h1>
            <b for="">Setting</b>
                : {{ $setting }}
            <br>
            <b for="">Color</b> :
                @foreach($colors as $color)
                    <div class="color-div">
                        <span style="background-color:{{ $color->description }}">&nbsp;</span>
                    </div>
                @endforeach
            <br>
            <b for=""><i class="fa fa-map-marker"></i></b> 
                {{ $inspiration->locationName }}
            <br>
            
            <b for=""><i class="fa fa-camera"></i></b>
                {{ count($gallery) + 1 }} photos @if($inspiration->images_by) by {{ $inspiration->images_by }}  @endif
        
        </div>
        <img class="bottom-pattern" src="/site/images/bottom-pattern.png">
    </div>
    
    <div class="container-fluid inspiration-page  mt-5">
  
            <div class="row mt-2" style="position: relative">
                <div class="col-sm-2 ">
                    <div class="box bg-white p-2 mb-1">
                        <small class="d-block text-center text-muted"><i>Posted by</i></small>
                        <div class="text-center">
                            <div class="p-4">
                                <img src="{{ $inspiration->company->image('logo')->path('logos', '/images/placeholders/placeholder.png') }}" class="w-100" alt="">
                                {{-- <img src="{{ $inspiration->companyLogo  }}" class="w-100" alt=""> --}}
                            </div>
                            <h5>
                                <a href="{{ route('site.supplier.show', optional($inspiration->company)->slug) }}" class="text-orange company-name-inspiration-show" >
                                    {{ optional($inspiration->company)->name }}
                                </a>
                            </h5>
                            <small class="line-height-1">{{ $inspiration->company->roleList() }}</small>
                            <div class="d-flex">
                                <div class="w-50 p-1">
                                    <button class="btn btn-default btn-sm btn-block">Message</button>
                                </div>
                                <div class="w-50 p-1">
                                    <heart-component :default="{{ Auth::user() ? $inspiration->company->hearts()->whereUserId(Auth::user()->id)->first() ? 1 : 0 : 0  }}" :id="{{ $inspiration->company->id }}" type="supplier" styleclass="btn btn-default btn-sm btn-block"></heart-component>
                                </div>
                            </div>
                        </div>
                    
                    </div>
                
                    @auth
                    <div class="box bg-white mb-1 pb-4 text-center pl-3 pr-3">
                        @auth
                            <heart-component :default="{{ $inspiration->hearts()->whereUserId(Auth::user()->id)->first() ? 1 : 0  }}" :id="{{ $inspiration->id }}" type="inspiration" styleclass="btn btn-sm btn-outline-secondary btn-lg mt-4"> Save Inspiration</heart-component>
                        @endif
                    </div>
                    @endauth
                        <div class="box bg-white  p-2 mb-3">
                            @auth
                                @if(Auth::user()->type == "supplier" || Auth::user()->type == "administrator" && Auth::user()->company_id != $inspiration->id)
                                    <small>
                                        <a href="#" data-toggle="modal" data-target="#requestTagModal" title=" Request Tag"  class="pull-right text-secondary"><i class="fa fa-tags"></i></a>
                                    </small>
                                @endif
                            @endif
                            <div class="clearfix"></div>
                            <div class="text-muted d-block mb-3 text-center supplier-divider mb-2">Suppliers</div>
                         
                            <div>
                                @foreach($suppliers as $supplier)
                                    <div class="inspiration-suppliers">
                                        @if($supplier['isCompany'])
                                            <div class="buttons">
                                                <div class="d-flex">
                                                    <div class="w-50 p-1">
                                                        <a class="btn btn-default btn-sm"><i class="fa fa-envelope"></i></a>
                                                    </div>
                                                    <div class="w-50 p-1">
                                                        <heart-component :default="{{ $supplier['isHearted']  }}" :id="{{ $supplier['company_id'] }}" type="supplier" styleclass="btn btn-default  btn-sm"></heart-component>
                                                    </div>
                                                </div>     
                                            </div>
                                            
                                            @if($supplier['role'] == "decor_hire")
                                                <a href="{{ route('site.supplier.show', $supplier['slug']) }}"  data-target="">
                                            @else
                                                <a href="{{ route('site.supplier.show', $supplier['slug']) }}" target="_blank">
                                            @endif
                                            
                                                <h4>
                                                    {{ $supplier['name'] }}
                                                </h4>
                                                <small>{{ $supplier['role'] }}</small>
                                            @if($supplier['isCompany'])
                                                </a>
                                                
                                            @endif
                                        @else
                                            <div style="
                                                color:#796a6a !important;
                                                background-color: transparent !important;
                                                border: none;
                                                outline: none;
                                                box-shadow: none;
                                            ">

                                                <h4 style="
                                                    font-size:16px;
                                                    color:#333;
                                                    padding-right: 60px;
                                                ">
                                                    {{ $supplier['name'] }}
                                                </h4>
                                                <small>{{ $supplier['role'] }}</small>
                                            </div>
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                        </div>
                </div>
                <div class="col-sm-8 pb-3">
        
                    @foreach($videos as $video)
                        <div class="mb-3 text-center">
                            <video controls title="{{ $video->title }}" class="video-js vjs-theme-sea" data-setup='{}'>
                                <source src="/videos/{{ $video->path }}" type="video/mp4">
                                Your browser does not support HTML5 video.
                            </video>
                        </div>
                    @endforeach
                    <div class="inspiration-grid">
                        <div class="inspiration-item">
                            <div class="image-gallery">
                                <a href="{{ $inspiration->thumbnail }}" data-lightbox="PORTFOLIO">
                                    <img src="{{ $inspiration->thumbnail }}" alt="gallery-image" class="margin">
                                </a>
                            </div>
                        </div>
                        
                        @foreach($gallery as $image)
                            <div class="inspiration-item">
                                <div class="image-gallery">
                                    <a href="/images/gallery/{{ $image->path }}" data-lightbox="PORTFOLIO">
                                        <img src="/images/gallery/{{ $image->path }}"  alt="gallery-image" class="margin">
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-sm-2">
                    @if(count($inspiration->decorHiresWithProducts()))
                        <h5 class="text-center text-muted mb-3"> Décor Supplied by</h5>
                        @foreach($inspiration->decorHiresWithProducts() as $supplier)
                            <div class="bg-white p-3 m-2">

                                <div class="company-box">
                                    @if($supplier->company)
                                        <div class="mb-3">
                                            <div>
                                                <b  style='font-family: "Proxima Nova Bold";color: #2f2f2f;'><small>{{ $supplier->role->display_name }}</small> </b><br>
                                                <a href="{{ route('site.supplier.show', $supplier->company->slug) }}" class="name text-orange company-name-inspiration-show">{{ optional($supplier->company)->name }}</a> <br>
                                            </div>
                                        </div>
                                    @else
                                        <div class="mb-3">
                                            <b  style='font-family: "Proxima Nova Bold";color: #2f2f2f;'><small>{{ $supplier->role->display_name }}</small></b><br>
                                            <strong for="">{{ $supplier->name }}</strong> <br>
                                        </div>
                                    @endif
                                </div>
                                
                                @forelse($inspiration->products()->whereCompanyId($supplier->company_id)->get() as $product)
                                    <inspiration-tagged-product productid="{{ $product->id }}"></inspiration-tagged-product>
                                @empty
                                    <h6 class="text-center text-muted mt-5"> No décor attached</h6>
                                @endforelse
                            
                                
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>  
    </div>
    @auth
        @if(Auth::user()->type == "supplier" || Auth::user()->type == "administrator" && Auth::user()->company_id != $inspiration->id)
            <!-- Modal -->
            <div class="modal fade" id="requestTagModal" tabindex="-1" role="dialog" aria-labelledby="requestTagModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                        <h5 class="modal-title" id="requestTagModalLabel">Request for to be Tagged</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                        <div class="modal-body">
                            <p>
                                By Requesting to be tagged, you are sending a notification to the entity who posted the inspiration that were a supplier at this event. The entity which loaded the inspiration will need to confirm this before the tag will be published.
                            </p>
                            <div class="p-3">
                                @if(Auth::user()->company)
                                    @foreach(Auth::user()->companyRoles as $role)
                                        <form action="{{ route('site.inspiration.requesttag') }}" method="POST">
                                            @csrf
                                            <input type="hidden" name="inspiration_id" value="{{ $inspiration->id }}">
                                            <input type="hidden" name="role_id" value="{{ $role->id }}">
                                            <button type='submit' class="btn btn-lg btn-outline-warning btn-block mb-2">Request as {{ $role->display_name }} </button>
                                        </form>
                                    @endforeach
                                @endif 
                            </div>
                        
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endif
@endsection

@push('styles')
    <link href="https://vjs.zencdn.net/7.14.3/video-js.css" rel="stylesheet" />
    <link href="https://unpkg.com/@videojs/themes@1/dist/sea/index.css" rel="stylesheet">
    <style>
        .vjs-play-progress{
            background-color:#f79827 !important;
        }
    </style>
@endpush

@push('scripts')
    <script src="https://vjs.zencdn.net/7.14.3/video.min.js"></script>
@endpush