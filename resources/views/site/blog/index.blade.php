
@extends(isMobile() ? 'site.includes.layouts.for-supplier' : 'site.includes.layouts.full')
@section('page-title', 'Designa for Suppliers')

@section('page-meta')
    <meta name="description" content="Some suppliers that have already joined the Designa Studio Community">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Designa for Suppliers">
    <meta property="og:description" content="Some suppliers that have already joined the Designa Studio Community">
    <meta property="og:image" content="{{ url('/site/images/support.png') }}">
    <meta property="og:url" content="{{ url('/for-supplier/resources/blogs') }}">
@endsection

@section('content')
    <div class="page-header-img">
        <h1 class="text-orange text-center">Designa for Supplier</h1>
        <img class="bg-image" src="/site/images/support.png" alt="">
        {{-- <img class="bottom-pattern" src="/site/images/bottom-pattern.png" alt=""> --}}
    </div>
    
    @if(!isMobile())
        <div style="background-color:#e6e6e6">
            <div class="container">
                <ul class="nav nav-pills justify-content-center mb-3 custom-nav-pills" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link " id="pills-supplier-tab" href="{{ route('site.for-supplier', 'supplier-hub') }}" role="tab" aria-controls="pills-supplier" aria-selected="true">Supplier Hub</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-mobile-tab"  href="{{ route('site.for-supplier', 'mobile-app') }}" role="tab" aria-controls="pills-mobile" aria-selected="true">Mobile App</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-resources-tab" href="{{ route('site.for-supplier', 'resources') }}" role="tab" aria-controls="pills-resources" aria-selected="false">Resources</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-advertising-tab"  href="{{ route('site.for-supplier', 'advertising-on-designa') }}" role="tab" aria-controls="pills-advertising" aria-selected="false">Advertising on Designa</a>
                    </li>                  
                </ul>
            </div>
        </div>
    @endif
    <div class="container">
        @if(!isMobile())
            <div class="tab-pane active " id="webinar" role="tabpanel" aria-labelledby="pills-resources-tab" style="padding-top: 20px;">
                <!-- second tab -->
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link " id="nav-webinar-tab" data-toggle="tab" href="#nav-webinar" role="tab" aria-controls="nav-webinar" aria-selected="true">Webinar Registration</a>
                        <a class="nav-item nav-link" id="nav-videos-tab" data-toggle="tab" href="#nav-videos" role="tab" aria-controls="nav-videos" aria-selected="false">User Videos</a>
                        <a class="nav-item nav-link" id="nav-guidelines-tab" data-toggle="tab" href="#nav-guidelines" role="tab" aria-controls="nav-guidelines" aria-selected="false">Image Guidelines</a>
                        <a class="nav-item nav-link" id="nav-tagging-tab" data-toggle="tab" href="#nav-tagging" role="tab" aria-controls="nav-tagging" aria-selected="false">Tagging Suppliers</a>
                        <a href="{{ route('site.blog.index') }}" class="nav-item nav-link active" style="background-color: #f7f7f7;border-bottom:none;"  role="tab" aria-controls="nav-blogs" aria-selected="false">Blogs and Articles</a>
                        <a class="nav-item nav-link" id="nav-why-tab" data-toggle="tab" href="#nav-why" role="tab" aria-controls="nav-why" aria-selected="false">Why Designa</a> 
                    </div>
                </nav>
            </div>
        @endif

        <!-- Blogs and Articles -->  
        <div class="blog-articles-box" id="nav-blogs" aria-labelledby="nav-blogs-tab">
            <section class="recent-articles">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- carousel -->
                        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                @foreach($blogSliders as $blogSlider)
                                    <div class="carousel-item @if($loop->first) active @endif ">
                                        <div class="row">
                                            <div class="col-lg-8" style="background-position:center; background-size:cover; padding-top:35%; background-image:url({{ $blogSlider->image('thumbnail')->path('blogs','/images/placeholders/placeholder250x200.png') }});">
                                                {{-- <img class="d-block img-fluid" src="{{ $blogSlider->image('thumbnail')->path('blogs','/images/placeholders/placeholder250x200.png') }}" alt=""> --}}
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="carousel-caption d-md-block">
                                                    <p style="color: #f39c12;">{{ $blogSlider->category }}</p>
                                                    <h1 style="text-align: left;">{{ $blogSlider->title }}</h1>
                                                    <p>{!! Str::limit(strip_tags($blogSlider->description), 200) !!}</p>
                                                    <p><a href="{{ route('site.blog.show', $blogSlider->id) }}">Read more..</a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @if(!isMobile())
                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 recent-articles-title">
                        <h5>Recent Articles</h5>
                        <hr class="hr-orange">            
                    </div>
                </div>

                <div class="row">        
                    @foreach($blogs as $blog)
                        <div class="col-lg-4">            
                            <div class="mr-2">      
                                <a href="{{ route('site.blog.show', $blog->id) }}">
                                    <div style="background-position:center; background-size:cover; padding-top:75%; background-image:url({{ $blog->image('thumbnail')->path('blogs','/images/placeholders/placeholder250x200.png') }});">
                                    </div>             
                                </a>
                                <h6>{{ $blog->category }}</h6>
                                <p>
                                    {{ $blog->title }}
                                </p>
                            </div>
                        </div>
                    @endforeach
                </div>

            </section>
            <section class="latest-post">        
                <div class=" latest-post-title">
                    <h5>Latest Post</h5>
                    <hr class="hr-orange"> 
                    <div class="latest-post-box">
                        <img src="/site/images/designa-sketch-logo.png" class="img-fluid designa-sketch-logo">
                        <h5>designastudio.au</h5>
                        <p>
                            We're an online marketplace for the events industry.
                        </p>
                    </div>

                    <!-- SnapWidget -->
                
                    <iframe src="https://snapwidget.com/embed/876885" class="snapwidget-widget" allowtransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden;  width:100%; "></iframe> 

                    <div class="text-center">
                        @if(!isMobile())
                        <div class="pt-5 pb-5">
                        @endif

                            <a href="https://www.instagram.com/designastudio.au/" class="d-block mb-3 mt-3" target="_blank">VIEW MORE POST</a>
                            
                            <div class="get-newsletter">
                                <h1>GET OUR NEWSLETTER</h1>
                                <newsletter-email></newsletter-email>
                            </div>

                        @if(!isMobile())
                        </div>
                        @endif
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://snapwidget.com/js/snapwidget.js"></script>
    <script>
       $(function() {
            $('.scroll-down').click (function() {
            $('html, body').animate({scrollTop: $('.ok').offset().top }, 'slow');
                return false;
            });
        });
    </script>
    
    <script>
        $(function () {
            var hash = window.location.hash;
            hash && $('ul.nav a[href="' + hash + '"]').tab('show');
        });
    </script>


@endpush

@push('styles')
    <style>
    
        .need-box h4 {
            padding-top: 20px;
            padding-bottom: 10px;
        }
 
        .fa-briefcase, .fa-laptop {
            font-size: 47px;
        }
        
        .scroll-down {
            height: 50px;
            width: 50px;
            background-color: #f39c12;
            border-radius: 50%;
            display: inline-block;
            color: #fff;
            float: right;
            font-size: 29px;
        }
        
        .custom-nav-pills{
            display:flex;
        }

        .custom-nav-pills .nav-item{
            flex:1;
        }
         .custom-nav-pills .nav-link.active, .custom-nav-pills .show >.nav-link {
            border-top: 4px solid!important;
            font-weight: 500;
            background-color: #f7f7f7;
            border-color: transparent transparent #f39c12;
            color: #f39c12;
            border-radius: inherit;
        }

        .custom-nav-pills .nav-link {
            display: block;
            text-align: center;
            border-top: 4px solid #e6e6e6;
            padding:1em 0px 1em 0px;
            font-size:18px;
        }

        h1 {
            color: #343433;            
            text-align: center;
            font-size: 31px;
        }

        .send-form-box {
            background: rgb(230, 230, 230); 
            text-align: left; 
            padding: 20px 110px;
            margin-top: 24px;
        }

        .send-row {
            display: grid;
            grid-template-columns: auto 1fr auto;
            grid-gap: 15px;
            align-items: center;
        }

        .store-icon {
            display: grid;
            grid-template-columns: 1fr 1fr;
            grid-gap: 15px;
            align-items: center;
        }

        .stores {
            padding: 0 267px;    
        }

        .embed-container { 
            position: relative; 
            padding-bottom: 56.25%; 
            height: 0; 
            overflow: hidden; 
            max-width: 100%; 
        } 
        
        .embed-container iframe, 
        .embed-container object, 
        .embed-container embed,
        .embed-container-resources iframe,
        .embed-container-resources object,
        .embed-container-resources embed { 
            position: absolute; 
            top: 0; 
            left: 0; 
            width: 100%; 
            height: 100%; 
        }
        
        .embed-container-resources {
            position: relative;
            padding-bottom: 43.25%;
            height: 0;
            overflow: hidden;
            max-width: 70%;
            margin: 0 auto;
            text-align: center;
        }
        
        .agree-text {
            font-size: 12px;
            padding: 20px 125px;
        }
        
        .search-box {
            width: 357px;
        }
        
        .title-space {
            padding-top: 10px;
        }
        
        .benefits {
            padding-top: 77px;
            padding-bottom: 30px;
        }
        
        .reach-box {
            background: url(/site/images/icon/reach-new-customers.png);
            height: 246px;
            width: 356px;
        }
        
        .directly-box {
            background: url(/site/images/icon/directly-engage-and-inspire.png);
            height: 236px;
            width: 356px;
        }
        
        .instantly-box {
            background: url(/site/images/icon/instantly-notify-customers.png);
            height: 236px;
            width: 356px;
        }
        
        .tag-box {
            background: url(/site/images/icon/tag-share-and-promote.png);
            height: 236px;
            width: 356px;
        }
        
        .receive-box {
            background: url(/site/images/icon/receive-real%20time-quotes.png);
            height: 236px;
            width: 356px;
        }
        
        .message-box {
            background: url(/site/images/icon/message-and-engage-clients.png);
            height: 236px;
            width: 356px;
        }
        
        .clients-box {
            background: url(/site/images/icon/designa-studio-allows.png);
            height: 236px;
            width: 356px;
        }
        
        .reach-box p,
        .directly-box p,
        .instantly-box p, 
        .tag-box p,
        .receive-box p,
        .message-box p,
        .clients-box p {
            position: relative;
            top: 50%;
            text-align: center;
            left: 0;
            right: 0;
            padding-left: 50px;
            padding-right: 50px;
        }
        
        .benefits-space {
            padding-top: 40px;
        }
        
        .suppliers img {
            width: 200px;
        }
        
        .btn-supplier {
            width: 300px;
        }
        
        #inquiry-box {
            position: fixed;
            bottom: 40px;
            right: 40px;
            z-index: 9999;
            width: 450px;
        }
        
        .need-box {
            background: #fff;
            /*padding: 57px 67px;*/
            padding: 43px 73px;
            margin-top: 57px;
            text-align: center;
        }
        
        .page-header-img h1 {
            display: none;
        }
        
        .advertising-text {
            /*font-size: 18px;
            padding: 30px;
            border: 1px solid #e6e6e6;*/
            color: #f39c12;
            font-size: 36px;
            font-weight: bold;
            font-family: 'Poppins', sans-serif;
        }
        
        .webform-box {
            padding-bottom: 16px;
            padding-left: 20%;
            padding-right: 20%;
        }
        
        .countdown li {
          display: inline-block;
          font-size: 1.5em;
          list-style-type: none;
          padding: 1em;
          text-transform: uppercase;
        }
        
        .countdown li span {
          display: block;
          font-size: 4.5rem;
        }
        
        .launching {
          text-align: center;
          padding-bottom: 26px;
          color: rgb(243, 156, 18);
          font-size: 28px;
        }
        
        .countdown-box {
            padding: 30px 0 0 0;
            text-align: center;
        }
        
        .webinar-list {
             width: 19%; 
             margin: auto; 
             text-align: left;
             padding-bottom: 20px;
        }

        
        /* For Image Guidelines */
        .logo-header {
            width: 200px; 
            margin: 71px auto 0px auto; 
            display: block;
        }

        .logo-content {
            width: 200px; 
            margin: 109px auto 0px auto; 
            display: block;
        }

        ul.ulguidelines {
            list-style: none; /* Remove default bullets */
        }

        ul.ulguidelines li::before {
          content: "\2022";  /* Add content: \2022 is the CSS Code/unicode for a bullet */
          color: rgb(247, 148, 30); /* Change the color */
          font-weight: bold; /* If you want it to be bold */
          display: inline-block; /* Needed to add space between the bullet and the text */
          width: 20px; /* Also needed for space (tweak if needed) */
          margin-left: -23px; /* Also needed for space (tweak if needed) */
          font-size: 22px;
          line-height: 20px;
        }

        .guidelines-box {
            background: #f7941e;            
            padding-bottom: 40px;
            padding-top: 40px;
            color: #fff;
            text-align: center;
        }

        .guidelines-box h1 {
            text-align: left;                       
            font-size: 38px;
            color: #fff;
            text-align: center;
        }

        .guidelines-margin {
            padding: 50px 100px 25px 100px;
        }

        .brush-bg {
            background: url(/site/images/brush-bg.png); 
            height: 78px; 
            background-size: contain; 
            background-repeat: no-repeat; 
            background-position: left center;
            margin-top: 20px;
        }

        .brush-bg h4 {
            padding-top: 26px; 
            padding-left: 87px; 
            font-size: 24px; 
            color: #fff;
        }

        .guidelines-margin-intro {
           padding: 20px 100px 10px 100px;
        }

        .guidelines-margin2 {
            padding: 10px 100px;
        }

        .guidelines-margin2 h6 {
            font-weight: bold;
            padding-top: 10px;
        }

        .hr-orange {
            margin-top: 0; 
            border-top: 1px solid #ffd19b;
        }

        .guidelines-img {
            height: 233px;
            margin: 14px auto 0 auto;
            display: block;
        }

        .pcheckout {
            padding: 0px 100px 10px 100px;
        }

        
        /* For Blogs and Articles */
        .recent-articles h6 {
            font-weight: bold;
            color: #f39c12;
            padding-top: 20px;
        }

        .recent-articles h5,
        .latest-post h5  {
            font-weight: bold;
        }

        .latest-post iframe {
            height: 583px !important
        }


        .blog-articles-box {
            padding-top: 50px; 
            padding-bottom: 50px; 
            color: #58585a;
        }

        .blog-articles-logo {
        text-align: center; 
        width: 164px;
        }

        .blog-articles-search {
        width: 230px;
        }

        .recent-articles {
        padding-bottom: 50px;
        }

        .recent-articles-title {
            padding-top: 60px;
        }

        .latest-post-title {
            padding-top: 40px;
        }

        .latest-post-box {
        padding-top: 20px; 
        text-align: center;
        }

        .designa-sketch-logo {
        width: 146px; 
        margin: 0 auto; 
        display: block; 
        padding-bottom: 20px;
        }

        .get-newsletter {
        /* padding-top: 60px; */
        text-align: center;
        /* padding-left: 300px;
        padding-right: 300px; */
        }

        .get-newsletter h1 {
        color: #f39c12;
        font-weight: bold;
        }

        .get-newsletter-box {
        /* padding-left: 70px;
        padding-right: 70px;  */
        /* padding-top: 10px; */
        }

        .get-email {
        width: 247px; 
        float: left; 
        border: none;
        background: #f0f0f0;
        }

        .get-subscribe {
        width: 123px; border: none;
        background: #f7941e;
        color: #fff;
        border-radius: initial;
        }


        /* Carousel */
        .carousel-control-prev {
            left: 70% !important;
        }

        .carousel-control-next {
            right: 26% !important;
        }

        .carousel-control-next, .carousel-control-prev {
            top: 356px !important;
            width: 20px !important;
            z-index: 100 !important;
        }

        .carousel-control-prev-icon,
        .carousel-control-next-icon {
            filter:
        /* for demonstration purposes; originals not entirely black */
        contrast(1000%)
        /* black to white */
        invert(100%)
        /* white to off-white */
        sepia(100%)
        /* off-white to yellow */
        saturate(10000%)
        /* do whatever you want with yellow */
        hue-rotate(90deg);
        }

        #carouselExampleControls {
            -webkit-box-shadow: -1px 1px 28px 3px rgba(0,0,0,0.08);
            -moz-box-shadow: -1px 1px 28px 3px rgba(0,0,0,0.08);
            box-shadow: -1px 1px 28px 3px rgba(0,0,0,0.08);
        }

        .carousel-caption {
            top: 16px !important; 
            text-align: left !important; 
            left: 5% !important; 
            color: #000 !important;
        }

         /* Individual Blog */
        .individual-box {
            -webkit-box-shadow: -1px 1px 28px 3px rgba(0,0,0,0.08);
            -moz-box-shadow: -1px 1px 28px 3px rgba(0,0,0,0.08);
            box-shadow: -1px 1px 28px 3px rgba(0,0,0,0.08);
            padding: 20px;
        }

        /* Why Designa */
        .event-planning-list {
            padding-top: 30px;  
        }

        .event-planning-list img {
            width: 57px;            
        }

        .icon-right {
            text-align: right;
        }

        .event-planning-list p {
            padding-top: 14px;
        }

        .icon-space {
            padding-bottom: 30px;
        }

        .steps-box {
            background: #f79624;
            color: #fff;
            padding: 15px;
        }

        .steps-box ul {
            padding-left: 17px;
            list-style: none;
            font-size: 14px;
            line-height: 25px;
        }

        .steps-box ul li::before {
          content: "\2022";  /* Add content: \2022 is the CSS Code/unicode for a bullet */
          color: #fff; /* Change the color */
          font-weight: bold; /* If you want it to be bold */
          display: inline-block; /* Needed to add space between the bullet and the text */
          width: 12px; /* Also needed for space (tweak if needed) */
          margin-left: -17px; /* Also needed for space (tweak if needed) */
          font-size: 17px;
          line-height: 20px;
        }

        .steps-box p {
            margin-bottom: 0;
        }

        .box-sm {
            margin-top: 300px
        }

        .box-md {
            margin-top: 150px;   
        }

        .box-lg {
            margin-top: 74px;
            padding-bottom: 91px;
        }

        .why-gradient {
            background: rgb(247,150,37);
            background: linear-gradient(90deg, rgba(247,150,37,1) 0%, rgba(255,255,255,1) 100%); 
            height: 5px; 
            width: 106px; 
            margin-bottom: 20px;
        }

        ul.usedesigna {
            list-style: none
        }

        
        ul.usedesigna li:before {    
            content: "\2713";
            color: white;
            font-weight: bold;
            display: inline-block;    
            margin-left: -39px;
            font-size: 28px;
            line-height: 24px;
            background: orange;
            border-radius: 50%;
            width: 50px;
            height: 50px;
            text-align: center;
            padding-top: 15px;
            margin-right: 18px;
            float: left;
        }

        .usechecklist {
            display: grid; 
            grid-template-columns: 1fr 1fr; 
            grid-gap: 20px;
        }

        .usechecklist > li {
             display: grid; 
             grid-template-columns: auto 1fr; 
             grid-gap: 15px;
        }

        .usechecklist img {
            width: 54px;
        }


        /* Tagging Suppliers */
        .brush-bg-tagging {
            background: url(/site/images/brush-bg.png); 
            height: 86px; 
            background-size: contain; 
            background-repeat: no-repeat; 
            background-position: left center;
            margin-top: 20px;
        }

        .brush-bg-tagging h4 {
            padding-top: 14px;
            padding-left: 99px;
            font-size: 24px;
            color: #fff;
            width: 302px;
        }

        h4.width {
            padding-top: 14px;
            padding-left: 99px;
            font-size: 24px;
            color: #fff;
            width: auto;            
        }

        .tagging-creates {
            padding-top: 49px;
        }

        .tagging-img {
            margin: 0 auto;
            display: block;
            width: 590px;
            padding-top: 10px;
        }

        .tagging-app {
            text-align: center; 
            padding-bottom: 30px
        }

        .tagging-img01 {
            margin: 0 auto;
            display: block;
            width: 590px;
            padding-top: 10px; 
            padding-bottom: 30px;
        }

        .tagging-img02 {
            margin: 0 auto;
            display: block;
            width: 590px;
            padding-top: 10px;             
        }

        .tagging-logo {
            width: 200px; 
            margin-left: -14px;
        }


        
        @media (max-width: 939px) {
            .usechecklist {
                grid-template-columns: 1fr; 
                padding-left: 0;
            }
        }

        @media (min-width: 1561px) {
            .carousel .carousel-item {
                height: auto !important;
            }
        }


        @media (max-width: 480px) and (min-width: 320px) {
            .carousel .carousel-item {
                height: 500px;
            }
        }        


        
        /* Portrait */
        @media only screen 
          and (min-device-width: 375px) 
          and (max-device-width: 667px) 
          and (-webkit-min-device-pixel-ratio: 2)
          and (orientation: portrait) { 
              .custom-nav-pills {
                display: block !important;
                padding-bottom: 22px !important;
              }
              
              .form-space {
                 padding-bottom: 10px;
              }
              
              .send-row {
                 display: block !important;
              }
              
              .send-form-box {
                padding: 20px 42px !important;
                text-align: center !important;
              }
              
              .agree-text {
                padding: 13px 0px 0px 0px !important;
                text-align: center !important;
              }
              
              .stores {
                padding: 0 60px !important;
              }
              
              .search-box {
                width: 205px !important;
              }
              
              .title-space {
                padding-bottom: 10px;
             }
             
             .suppliers img {
                 float: none !important;
             }
             
             .btn-supplier {
                width: 257px !important;
                margin-bottom: 14px !important;
                float: none !important;
            }
            
            .header-search,
            .header-links,
            .sections,
            .links {
                display: none !important;
            }
            
            #inquiry-box {
                width: auto !important;
            }
            
            .need-box {
                padding: 57px 37px !important;
            }
            
            .embed-container-resources {
                padding-bottom: 53% !important;
                max-width: 100% !important;
            }
            
            .logo {
                height: auto !important;
            }
            
            .launching {
                padding-left: 20px;
                padding-right: 20px;
            }
            
            .countdown-box {
                text-align: left !important;
            }
            
            .webinar-list {
                width: 68% !important;
                padding-bottom: 16px !important;
            }
            
            .webform-box {
                padding-left: 0 !important;
                padding-right: 0 !important;
            }

            /* For guidelines */
            .logo-header {
                margin: 0px auto 44px auto !important;
            }

            .guidelines-margin {
                padding: 25px 40px 0px 40px !important;
            }

            .guidelines-margin-intro {
                padding: 20px 40px 10px 40px !important;
            }

            .guidelines-margin2 {
                padding: 10px ;
            } 

            .guidelines-img {
                margin: 14px auto 17px auto !important;
            }

            .logo-content {
                margin: 30px auto !important;
            }

            .pcheckout {
                padding: 0px 40px 10px 40px !important;
            }

            .nav-tabs {
                display: block !important;
            }

            /* For Get Newsletter */
            .get-newsletter,
            .get-newsletter-box {
                padding-left: 0 !important;
                padding-right: 0 !important;
            }

            .blog-articles-search {
                width: 170px !important;
            }

            .blog-articles-logo {
                margin: 0 auto !important;
                display: block !important;
                padding-bottom: 28px !important;
            }

            .latest-post iframe {
                height: 201px !important;
            }

            .get-email {
                width: 219px !important;
            }

            /* Carouse */
            .carousel-caption {
                top: 0 !important;
                left: 14% !important
            }

            .carousel-control-next, .carousel-control-prev {
                top: 496px !important;
            }

            .carousel-control-prev {
                left: 40px !important;
            }

            .carousel-control-next {
                    right: 75% !important;
            }

            .latest-post-title {
                padding-top: 0 !important;
            }

            /* why designa */
            .icon-right,
            .check-center {
                text-align: center !important;
            }

            .box-sm, .box-md, .box-lg, .box-xl {
                margin-top: 20px !important;
            }

            .box-lg {
                padding-bottom: 9px !important;
            }

            /* Tagging Suppliers */
            .brush-bg-tagging h4 {
                padding-left: 41px !important;
            }

            .brush-bg-tagging {
                background-size: cover !important;
                background-position: -136px center !important;
            }

    </style>
    
    
@endpush