<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>
</head>
<body>
        <input id="lat">
        <input id="lng">
        <input id="address">
        <script>
                var lat = document.getElementById("lat")
                var lng = document.getElementById("lng")
                var address = document.getElementById("address")
                navigator.geolocation.watchPosition(  function(position) {
                        lat.value = position.coords.latitude;
                        lng.value = position.coords.longitude;

                        fetch(`https://cors-anywhere.herokuapp.com/https://maps.googleapis.com/maps/api/geocode/json?latlng=${position.coords.latitude}, ${position.coords.longitude}&key=AIzaSyBMnJ6SDTRTpOca3b0RD3VYGdgl_WWtuak`)     
                                .then(response => response.json())
                                .then(data => {
                                        address.value = data.plus_code.compound_code
                                }); 

                        
                }); 
        </script>      
</body>
</html>