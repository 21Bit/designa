@extends('dashboard.includes.layouts.main')

@section('page_title', 'Edit Company')

@section("content-hearder")
        <h1>
            Companies
            {{-- <small>it all starts here</small> --}}
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('dashboard.company.index') }}">Companies</a></li>
            <li class="active">Edit</li>
        </ol>
@endsection


@section('content')
@include('dashboard.includes.alerts.success')
@if(!$company->is_active)
    <div class="alert alert-info">
        <form action="{{ route('dashboard.company.make-live') }}"  method="POST">
            @csrf
            <input type="hidden" name="id" value="{{ $company->id }}">
            <button  type="submit" class="btn btn-default">Send Make Live Mail</button>
        </form>
    </div>
@endif
<form action="{{ route('dashboard.company.update', $company->id) }}" method="post" enctype="multipart/form-data" >
    <div class="text-right mb-2">
        <button class="btn btn-warning"><i class="fa fa-save"></i> Save Changes</button>
        <a href="{{ route('dashboard.company.index') }}" class="btn btn-default"><i class="fa fa-ban"></i> Cancel</a>
    </div>
        @include("dashboard.includes.alerts.error")
    <div class="box">
        <div class="box-body">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-sm-4">
                    <h5 class="header">Company Information</h5>
                    <p>
                        <label for="">Name</label>
                        <input type="text" required name="name" value="{{ $company->name }}" class="form-control input-sm">
                    </p>
                    <p>
                        <label for="">Will travel</label>
                        <input type="text" name="will_travel" value="{{ $company->will_travel }}" class="form-control input-sm">
                    </p>
                    <p>
                        <label for="">Services</label>
                        <input type="text" name="services" value="{{ $company->services }}" class="form-control input-sm">
                    </p>
                    <p>
                        <label for="">Recognition</label>
                        <input type="text" name="recognition" value="{{ $company->recognition }}" class="form-control input-sm">
                    </p>
                    <p>
                        <label for="">Description</label>
                        <textarea style="resize: none" name="description" id="" cols="30" rows="10" class="form-control">{{ $company->description }}</textarea>
                    </p>
                    <p class="mb-5">
                        <label for="">Active</label> <br>
                        <input type="checkbox" data-type="icheck" @if($company->is_active) checked @endif  name="is_active"> Yes! Its active
                    </p>
                    <p class="mb-5">
                        <label for="">Lock Active</label> <br>
                        <input type="checkbox" data-type="icheck" @if($company->is_lock) checked @endif  name="is_lock"> Yes! Lock Active
                    </p>
                    <p>
                        <label for="" class="mb-0">Category</label>
                        @foreach($categories as $category)
                            <div class="pl-3">
                                <input type="checkbox" data-type="icheck" @if(in_array($category->id, $company->categories()->pluck('id')->toArray())) checked @endif id="checkbox-{{ $category->id }}" name='categories[]' value="{{ $category->id }}">
                                <label style="font-weight: normal" for="checkbox-{{ $category->id }}">{{ $category->name }}</label>
                            </div>
                        @endforeach
                    </p>
                     <p>
                            <label for="" class="mb-0">Offers</label>
                            @foreach($roles as $role)
                                <div class="pl-3">
                                    <input type="checkbox" data-type="icheck" @if(in_array($role->id, $company->roles()->pluck('id')->toArray())) checked @endif id="role-{{ $role->id }}" name='roles[]' value="{{ $role->id }}">
                                    <label style="font-weight: normal" for="role-{{ $role->id }}">{{ $role->display_name }}</label>
                                </div>
                            @endforeach
                        </p>
                </div>
                <div class="col-sm-4">
                    <h5 class="header">Address</h5>
                    <p>
                        <label for="">County</label>
                        <select name="country" required class="form-control input-sm">
                            <option value="Australia">Australia</option>
                            <option value="Philippines" @if($company->country == "Philippines") selected @endif>Philippines</option>
                        </select>
                    </p>
                    <p>
                        <label for="">Town</label>
                        <input type="text" required name="town" value="{{ $company->town }}" class="form-control input-sm">
                    </p>
                    <p>
                        <label for="">State</label>
                        <input type="text" required name="state" value="{{ $company->state }}" class="form-control input-sm">
                    </p>
                    <p>
                        <label for="">Post Code</label>
                        <input type="text" required name="post_code" value="{{ $company->post_code }}" class="form-control input-sm">
                    </p>
                    
                    <hr>
                    <h5 class="header">Contact Informations</h5>
                    <p>
                        <label for="">Email</label>
                        <input type="email" required name="email" value="{{ $company->email }}" class="form-control input-sm">
                    </p>
                    <p>
                        <label for="">Telephone</label>
                        <input type="text" required name="telephone_number" value="{{ $company->telephone_number }}" class="form-control input-sm">
                    </p>
                    <p>
                        <label for="">Facebook URL</label>
                        <input type="text" name="facebook_link" value="{{ $company->facebook_link }}" class="form-control input-sm">
                    </p>
                    <p>
                        <label for="">Twitter URL</label>
                        <input type="text" name="twitter_link" value="{{ $company->twitter_link }}" class="form-control input-sm">
                    </p>
                    <p>
                        <label for="">Instagram URL</label>
                        <input type="text" name="instagram_link" value="{{ $company->instagram_link }}" class="form-control input-sm">
                    </p>
                    <p>
                        <label for="">Youtube URL</label>
                        <input type="text" name="youtube_link" value="{{ $company->youtube_link }}" class="form-control input-sm">
                    </p>
                    
                </div>
                <div class="col-sm-4">
                    <h5 class="header">Images</h5>
                    <p>
                        <label for="">Company Logo</label> <br />
                        <input type="file" style="display:none" id="logo"  name="company_logo" accept="image/*">
                        <label for="logo" class="btn btn-default btn-xs mb-2"> Change Logo</label>
                        <img src="{{ $company->image('logo')->path('logos', '/images/placeholders/company-logo.png') }}" class="img img-responsive" alt="">
                    </p>
                    <p>
                        <label for="">Cover Picture</label> <br />
                        <input type="file" style="display:none" id="cover" name="company_cover_picture"  accept="image/*">
                        <label for="cover" class="btn btn-default btn-xs mb-3"> Change Cover</label>
                        <img src="{{ $company->image('cover')->path('covers', 'https://via.placeholder.com/728x90.png') }}" class="img img-responsive" alt="">
                    </p>
                </div>
            </div>  
        </div>
    </div>
</form>
@endsection