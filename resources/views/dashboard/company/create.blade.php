@extends('dashboard.includes.layouts.main')

@section('page_title', 'Create Company')

@section("content-hearder")
        <h1>
            Companies
            {{-- <small>it all starts here</small> --}}
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('dashboard.company.index') }}">Companies</a></li>
            <li class="active">Create</li>
        </ol>
@endsection

@section('content')
@include("dashboard.includes.alerts.error")
@include("dashboard.includes.alerts.success")
<form action="{{ route('dashboard.company.store') }}" method="post" enctype="multipart/form-data">
        <div class="text-right mb-2">
            <button class="btn btn-warning"><i class="fa fa-save"></i> Save</button>
            <a href="{{ route('dashboard.company.index') }}" class="btn btn-default"><i class="fa fa-ban"></i> Cancel</a>
        </div>
    <div class="box">
        <div class="box-body">
                @csrf
                <div class="row">
                    <div class="col-sm-4">
                        <h5 class="header text-muted"><i>Company Information</i></h5>
                        <p>
                            <label for="">Name</label>
                            <input type="text" value="{{ old('name') }}"  required name="name" class="form-control input-sm">
                        </p>
                        <p>
                            <label for="">Will travel</label>
                            <input type="text" required value="{{ old('will_travel') }}" name="will_travel" class="form-control input-sm">
                        </p>
                        <p>
                            <label for="">Services</label>
                            <input type="text" value="{{ old('services') }}"  required name="services" class="form-control input-sm">
                        </p>
                        <p>
                            <label for="">Recognition</label>
                            <input type="text" required name="recognition"  value="{{ old('recognition') }}" class="form-control input-sm">
                        </p>
                        <p>
                            <label for="">Description</label>
                            <textarea style="resize: none" name="description" id="texteditor" cols="30" rows="10" class="form-control">{{ old('description') }}</textarea>
                        </p>

                        <p>
                            <label class="mb-0">Active</label>
                            <div class="pl-3">
                                <input type="checkbox" @if(old('is_active')) checked @endif  data-type="icheck" name="is_active" id="is_active">
                                <label for="is_active" style="font-weight: normal">Yes! Its active</label>
                            </div>
                        </p>

                        <p>
                            <label for="" class="mb-0">Category</label>
                            @foreach($categories as $category)
                                <div class="pl-3">
                                    <input type="checkbox" data-type="icheck" id="category-{{ $category->id }}" name='categories[]' @if(in_array($category->id, (old('categories') ?? []))) checked @endif value="{{ $category->id }}">
                                    <label style="font-weight: normal" for="category-{{ $category->id }}">{{ $category->name }}</label>
                                </div>
                            @endforeach
                        </p>
                        <p>
                            <label for="" class="mb-0">Offers</label>
                            @foreach($roles as $role)
                                <div class="pl-3">
                                    <input type="checkbox" data-type="icheck" id="role-{{ $role->id }}" name='offers[]' @if(in_array($role->id, (old('offers') ?? []))) checked @endif  value="{{ $role->id }}">
                                    <label style="font-weight: normal" for="role-{{ $role->id }}">{{ $role->display_name }}</label>
                                </div>
                            @endforeach
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <h5 class="header text-muted"><i>Address Information </i></h5>
                            <p>
                                <label for="">County</label>
                                <select name="country" required class="form-control input-sm">
                                    <option value="Australia">Australia</option>
                                </select>
                            </p>
                            <p>
                                <label for="">Town</label>
                                <input type="text" value="{{ old('town') }}" required name="town" class="form-control input-sm">
                            </p>
                            <p>
                                <label for="">State</label>
                                <input type="text" value="{{ old('state') }}" required name="state" class="form-control input-sm">
                            </p>
                            <p>
                                <label for="">Post Code</label>
                                <input type="text" value="{{ old('post_code') }}" required name="post_code" class="form-control input-sm">
                            </p>

                        <hr>
                        <h5 class="header text-muted"><i>Account Informations</i></h5>
                            <p>
                                <label for="">Email</label>
                                <input type="email" required name="email"  value="{{ old('email') }}" class="form-control input-sm">
                            </p>
                            <p>
                                <label for="">Password <small>(for the account)</small></label>
                                <input type="password" name="password" required class="form-control input-sm">
                            </p>

                        <hr>
                        <h5 class="header text-muted"><i>Contact Informations</i></h5>
                            <p>
                                <label for="">Telephone</label>
                                <input type="text" required name="telephone_number"  value="{{ old('telephone_number') }}" class="form-control input-sm">
                            </p>
                            <p>
                                <label for="">Facebook URL</label>
                                <input type="text" name="facebook_link"  value="{{ old('facebook_link') }}" class="form-control input-sm">
                            </p>
                            <p>
                                <label for="">Twitter URL</label>
                                <input type="text" name="twitter_link" value="{{ old('twitter_link') }}" class="form-control input-sm">
                            </p>
                            <p>
                                <label for="">Instagram URL</label>
                                <input type="text" name="instagram_link"  value="{{ old('instagram_link') }}" class="form-control input-sm">
                            </p>
                            <p>
                                <label for="">Youtube URL</label>
                                <input type="text" name="youtube_link"  value="{{ old('youtube_link') }}" class="form-control input-sm">
                            </p>
                    </div>
                    <div class="col-sm-4">
                        <h5 class="header text-muted"><i>Images</i></h5>
                        <p>
                            <label for="">Company Logo</label>
                            <input type="file" id="logo"  name="company_logo" style='display:none'  data-type="image-picker" data-preview="#preview-thumbnail" data-preview-type='label'  accept="image/*">
                            <label for="logo" id="preview-thumbnail" style="cursor:pointer;background-image:url('/images/placeholders/placeholder.png');background-size:cover; background-position:center; width: 100%; height: 200px;"></label>

                        </p>
                        <p>
                            <label for="">Cover Picture</label>
                            <input type="file" id='cover'  name="company_cover_picture"  style='display:none' data-type="image-picker" data-preview="#preview-cover" data-preview-type='label'  accept="image/*">
                             <label for="cover" id="preview-cover" style="cursor:pointer;background-image:url('/images/placeholders/placeholder.png');background-size:cover; background-position:center; width: 100%; height: 200px;"></label>
                        </p>

                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@push('scripts')
    <script src="{{ asset('/dashboard/ckeditor/ckeditor.js')}} "></script>
    <script>
        CKEDITOR.replace( 'texteditor', {
            toolbar: [
                {
                name: 'document',
                items: [ '-', 'NewPage', 'Preview', '-', 'Templates' ]
                },
                [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo', ],
                {
                name: 'basicstyles',
                items: [ 'Bold', 'Italic']
                }
            ],
            enterMode : CKEDITOR.ENTER_BR,
            shiftEnterMode: CKEDITOR.ENTER_P,
            menubar: false,
            branding:false,
            forced_root_block : false,
        });
    </script>
@endpush
