@extends('dashboard.includes.layouts.main')

@section('page_title', 'Companies')

@section("content-hearder")
        <h1>
            Companies
            {{-- <small>it all starts here</small> --}}
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Companies</li>
        </ol>
@endsection

@section('content')
        @include("dashboard.includes.alerts.success")
        @include("dashboard.includes.alerts.error")
        <div class="box">
            <div class="box-body">
                <form>
                    <div class="row">
                        <div class="col-sm-6">
                                <div class="input-group">
                                    <input type="text" name="q" value="{{ Request::get('q') }}" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="submit">Search</button>
                                    </span>
                                </div><!-- /input-group -->
                        </div>
                        <div class="col-sm-3">
                            <select name="role" class="form-control">
                                <option value="">-- all roles --</option>
                                @foreach($roles as $role)
                                    <option @if(request()->get('role') == $role->id) selected @endif value="{{ $role->id }}">{{ $role->display_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <div>
                                <a href="{{ route('dashboard.company.create') }}" class="btn btn-default"><i class="fa fa-plus"></i> New</a>
                                <button type="button" href="#" id="delete-all-btn" class="btn btn-default"><i class="fa fa-trash"></i> Delete</button>
                            </div>
                        </div>
                    </div>
                </form>
                <br>
                <form action="{{ route('dashboard.company.destroy', 0) }}" method="post" id="delete-all-form">
                    @csrf
                    @method('DELETE')
                    <table class="table">
                        <thead>
                            <tr>
                                <th><input type="checkbox" id="checkAll" ></th>
                                <th>Active</th>
                                <th>Name</th>
                                <th>Email/Telephone</th>
                                <th>Location/Address</th>
                                <th>Roles</th>
                                <th>Event Types</th>
                                <th>Registered</th>
                                <th>Trial</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($companies as $company)
                                <tr>
                                    <td style="width:10px"><input type="checkbox" name="item_checked[]" value="{{ $company->id }}" ></td>
                                    <td>@if($company->is_active) <i class="fa fa-check text-success"></i> @endif</td>
                                    <td style="width:350px"><img style='width:32px' src="{{ $company->logo }}"> {{ $company->name }}</td>
                                    <td>{{ $company->email }} <br> {{ $company->telephone_number }}</td>
                                    <td>{{ $company->town }} {{ $company->state }}</td>
                                    <td>{{ $company->roleList() }}</td>
                                    <td>{{ $company->caterList() }}</td>
                                    <td style="width:150px">{{ $company->created_at->format('Y-m-d h:iA') }}</td>
                                    <td style="width:150px">
                                        {{ $company->trialEnds() }}
                                    </td>
                                    <td class="text-right" width="100">
                                        <div class="dropdown">
                                          <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                                            <i class="fa fa-ellipsis-v"></i>
                                          </button>
                                          <ul class="dropdown-menu">
                                              @if($company->hasTrial())
                                                <li><a href="#" class="cancel-trial-btn" data-id="{{ $company->id  }}" data-url="{{ route('dashboard.company.cancel-trial') }}" data-name="{{ $company->name }}">Cancel Trial</a></li>
                                              @else
                                                <li><a href="#" class="trial-btn" data-id="{{ $company->id  }}" data-name="{{ $company->name }}">Add Free Trial</a></li>
                                              @endif
                                                <li><a href="{{ route('dashboard.company.liftusers', $company->id) }}">Lift Email</a></li>
                                                <li><a href="#" data-link="{{ $company->loginlink }}" class="link-modal-btn">Login Link</a></li>
                                                <li><a href="{{ route('dashboard.company.edit', $company->id) }}">Edit Company</a></li>
                                          </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $companies->appends(['q' => request()->get('q'), 'role' => request()->get('role')])->links() }}
                </form>
            </div>
        </div>
    <div class="modal in fade" id="free-trial-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('dashboard.company.add-trial') }}" method="POST">
                  @csrf
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Trial for <span id="trial-company-name"></span></h4>
                  </div>
                  <div class="modal-body">
                      <input type="hidden" id="company-id-trial" name="company_id">
                        <p>
                            <label for="">Days of Trial</label>
                            <input type="number" required name="days" class="form-control" value="30">
                            <small>Note: this will add from current date</small>
                        </p>
                        <p>
                            <label>
                                <input type="checkbox" name="sendEmail">
                                Send Email
                            </label>
                        </p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
            </form>
        </div>
      </div>
    </div>

    <div class="modal in fade" id="link-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-body">
              <h3 class="mt-0">Login link</h3>
                  <input type="text" id="link-input" class="form-control" placeholder="link">
{{--                <div class="input-group">--}}
{{--                  <span class="input-group-btn">--}}
{{--                    <button class="btn btn-outline-light" onclick="copyToClipboard('link-input')" type="button"> Copy link</button>--}}
{{--                  </span>--}}
{{--                </div>--}}
                <div class="text-success text-right text-copy-success" style="display:none">Link Copied!</div>
              <div class="text-right mt-4">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Done</button>
              </div>
          </div>
        </div>
      </div>
    </div>
@endsection
@push('scripts')
    <script>
        $('select[name=role]').change(function(){
            $(this).closest('form').submit();
        })
    </script>
@endpush

