
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Designa Studio</title>   

  <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&display=swap" type="text/css">
</head>

<body style="margin: 0; padding: 0; min-width: 100%!important; font-family: 'Open Sans';">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
  <td>
    <!--[if (gte mso 9)|(IE)]>
      <table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td>
    <![endif]-->     
    <table bgcolor="#ffffff" style="width: 100%; max-width: 600px; border: 1px solid #f8f8f8;" align="center" cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td style="background: #fff; height: 10px;"></td>
      </tr>
      <tr>
        <td bgcolor="#fff8f3" style="padding: 10px 0;">
          <table width="auto" align="center" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td height="50">
                <a href="https://designa.studio" target="_blank"><img src="https://designa.studio/images/designa-logo-sm.png" width="auto" height="50" border="0" alt="" />
              </td>
            </tr>
          </table>
          <!--[if (gte mso 9)|(IE)]>
            <table width="425" align="left" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td>
          <![endif]-->
          
          <!--[if (gte mso 9)|(IE)]>
                </td>
              </tr>
          </table>
          <![endif]-->
        </td>
      </tr>
      <tr>
        <td style="background: #fff; height: 15px;"></td>
      </tr>      
      <tr>
        <td style="padding: 30px 40px; border-bottom: 1px solid #f2eeed;">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td style="font-size: 16px; line-height: 28px; font-weight: normal;">
                Hi
              </td>
            </tr>
            <tr>
              <td style="padding: 0 0 15px 0; font-size: 30px; line-height: 28px; font-weight: bold;">
                Michael <!-- you can change the name here -->
              </td>
            </tr>
            <tr>
              <td style="font-size: 14px; line-height: 22px;">
                <p>White Label Hire has invited you to join the growing number of businesses in the Event Industry using Designa Studio.</p>
                <p>What is Designa? Well we would say it is the evolution of how people are inspired, search suppliers or décor and interact with vendors when planning their next event.  It is everything a person would need to know about events and how to Design a memorable one.</p>
                <p>Want to know a bit more, check out this quick video.</p>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td style="padding: 30px 40px; border-bottom: 1px solid #f2eeed;" bgcolor="#f7f7f7">        
          <!--[if (gte mso 9)|(IE)]>
            <table width="380" align="left" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td>
          <![endif]-->
          <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">  
            <tr>
              <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">                  
                  <tr>
                    <td>
                      <table border="0" cellspacing="0" cellpadding="0">
                        <tr align="center">
                          <td>
                            <a href="https://youtu.be/v6dsLli56Eg" target="_blank"><img src="https://designa.studio/images/video.png" width="100%" height="auto" /></a>
                          </td>                          
                        </tr>                        
                      </table>
                    </td>
                  </tr>                                                     
                </table>
              </td>
            </tr>
          </table>
          <!--[if (gte mso 9)|(IE)]>
                </td>
              </tr>
          </table>
          <![endif]-->
        </td>
      </tr> 
      <tr>
        <td style="padding: 30px 40px; border-bottom: 1px solid #f2eeed;">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr align="center">
              <td style="font-size: 16px; line-height: 28px; font-weight: normal;">
                Getting
              </td>
            </tr>
            <tr align="center">
              <td style="padding: 0 0 15px 0; font-size: 30px; line-height: 28px; font-weight: bold;">
                Started
              </td>
            </tr>
            <tr align="center">
              <td style="font-size: 14px; line-height: 22px;">
                <p style="padding-bottom: 15px;">Want to jump in and get started? Simply follow this link to sign up. <span style="font-weight: bold;">You will need to do this on a desktop.</span></p>
              </td>
            </tr>
            <tr>
              <td height="45" align="center">
                <a href="https://designa.studio/register/supplier" target="_blank" style="text-align: center; font-family: 'Open Sans', sans-serif; color: #f7941e; text-decoration: none; border: 2px solid #f7941e; border-radius: 30px; padding: 8px 19px; font-weight: normal; font-size: 14px;">Sign Up</a>
              </td>
            </tr>                        
          </table>
        </td>
      </tr>
      <tr>
        <td background="https://designa.studio/images/video-banner.jpg" style="padding: 30px 40px; border-bottom: 1px solid #f2eeed; height: 103px; width: auto; background-size: cover;">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr align="center">
              <td style="font-size: 20px; font-weight: 600; color: #fff;">
                There are also heaps of video tutorials here that can help if you get stuck.
                <p style="text-align: center; padding-top: 25px;"><a href="https://designa.studio/for-supplier/resources#videos" target="_blank" style="text-align: center; font-family: 'Open Sans', sans-serif; color: #fff; text-decoration: none; border: 2px solid #fff; border-radius: 30px; padding: 8px 19px; font-weight: normal; font-size: 14px;">View Videos</a></p>
              </td>
              
            </tr>                                   
          </table>
        </td>
      </tr>
      <tr>
        <td style="padding: 30px 40px; border-bottom: 1px solid #f2eeed;">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td style="font-size: 16px; line-height: 28px; font-weight: normal;">
                Book in a
              </td>
            </tr>
            <tr>
              <td style="padding: 0 0 15px 0; font-size: 30px; line-height: 28px; font-weight: bold;">
                Demo
              </td>
            </tr>
            <tr>
              <td style="font-size: 14px; line-height: 22px;">
                <p>If you need a bit more help, we would love to arrange a one on one demo.  Our Team are ready to show you the Designa Studio system and take you through all the functions that are at your disposal.</p>
              </td>
            </tr>
            <tr>
              <td height="45" align="center" style="padding-top: 20px;">
                <a href="https://designa.studio/for-supplier#pills-resources" target="_blank" style="text-align: center; font-family: 'Open Sans', sans-serif; color: #f7941e; text-decoration: none; border: 2px solid #f7941e; border-radius: 30px; padding: 8px 19px; font-weight: normal; font-size: 14px;">Book in a Demo</a>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td bgcolor="#f7f7f7" style="padding: 30px 40px; border-bottom: 1px solid #f2eeed;"> 
          <p style="padding: 0; margin-bottom: 0;">Kind Regards,</p>
          <p style="padding: 0; margin-top: 0; font-weight: bold; font-size: 30px;">Designa Team</p>
        </td>        
      </tr> 
      <tr>
      <tr>
        <td height="20px" bgcolor="#fff"></td>          
      </tr>  
      <tr>              
        <td style="padding: 30px 40px; border-bottom: 1px solid #f2eeed;" bgcolor="#fff8f3">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">            
            <tr>
              <td style="font-size: 16px; line-height: normal; font-weight: normal;">
                Live
              </td>
            </tr>
            <tr>
              <td style="padding: 0 0 15px 0; font-size: 30px; line-height: normal; font-weight: bold;">
                "How To"
              </td>
            </tr>
            <tr>
              <td style="font-size: 14px; line-height: 22px;">
                <p>Webinars on everything from Creating a profile to loading inspiration and décor are now available. </p>
              </td>
            </tr>
            <tr>
              <td height="45" align="center" style="padding-top: 15px;">
                <a href="https://designa.studio/for-supplier#pills-resources" target="_blank" style="text-align: center; font-family: 'Open Sans', sans-serif; color: #f7941e; text-decoration: none; border: 2px solid #f7941e; border-radius: 30px; padding: 8px 19px; font-weight: normal; font-size: 14px;">Check times and Register</a>
              </td>
            </tr>
          </table>
        </td>
      </tr>             
      <tr>
        <td align="center" style="font-size: 14px; line-height: 22px; padding: 30px 40px;">
          Contact
          <p style="padding: 0; line-height: 0; font-weight: bold; font-size: 30px;">Follow Us</p>
          <a href="https://www.instagram.com/designastudio.au/" style="text-decoration: none;" target="_blank">
              <img src="https://designa.studio/images/instagram-icon.jpg" width="auto" height="auto" alt="Instagram" border="0" style="text-decoration: none;"  />
            </a>
            <a href="https://www.facebook.com/Designa-Studio-260657538157535/" style="text-decoration: none;" target="_blank">
              <img src="https://designa.studio/images/fb-icon.jpg"width="auto" height="auto" alt="Facebook" border="0" style="text-decoration: none;" />
            </a>
            <a href="https://www.youtube.com/channel/UC2nO0C_he52F2cqLbxGyoBA" style="text-decoration: none;" target="_blank">
              <img src="https://designa.studio/images/youtube-icon.jpg" width="auto" height="auto" alt="Twitter" border="0" style="text-decoration: none;"  />
            </a>
        </td>
      </tr>      
      <tr>
        <td bgcolor="#f7941e" style="    padding: 20px 30px 15px 30px;">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 14px;
    color: #ffffff;">               
                <a href="https://designa.studio/" target="_blank" style="text-decoration: none;"><font color="#ffffff">www.designa.studio</font></a>                 
              </td>
            </tr>            
          </table>
        </td>
      </tr>
    </table>
    <!--[if (gte mso 9)|(IE)]>
          </td>
        </tr>
    </table>
    <![endif]-->
    </td>
  </tr>
</table>
</body>
</html>