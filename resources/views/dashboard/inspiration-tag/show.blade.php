@extends('dashboard.includes.layouts.main')

@section('page_title', $inspiration->name)

@section("content-hearder")
    <h1>
        Inspirations
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.home') }}">Home</a></li>
        <li><a href="{{ route('dashboard.inspiration.tags.index') }}">Inspiration Tags</a></li>
        <li class="active">{{ $inspiration->name }}</li>
    </ol>
@endsection

@section('content')
    <div class="inspiration-header" style="background-image:url('{{ $inspiration->image('cover')->path('covers', 'https://via.placeholder.com/1920x450') }}')">
    </div>
    <div style="background-color: #fff">
        <div class="row" >
            <div class="col-sm-3" style="margin-top:-150px">
                <div class="pl-3">
                    <img
                    src="{{ $inspiration->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png') }}"
                    class="img img-responsive thumbnail mb-0" alt="">
                </div>
                <div class="box mb-3 ml-2">
                    <div class="box-header with-border">
                        <h3 class="box-title">Information</h3>
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <strong>
                            <a class="mb-0" href="{{ route('dashboard.company.show', optional($inspiration->company)->slug) }}">
                                {{ optional($inspiration->company)->name }}
                            </a>
                        </strong>
                        <p class="text-muted">
                            Company
                        </p>
                        <hr>
                       
                        <strong>
                            @foreach($inspiration->caters as $cater)
                            {{ $cater->name }}@if(!$loop->last), @endif
                            @endforeach
                        </strong>
                        <p class="text-muted">
                            Category
                        </p>
                
                    </div>
                </div>
                
                <div class="box mb-3 ml-2">
                    <div class="box-header with-border">
                        Description
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        {!! $inspiration->description !!}
                    </div>
                </div>
                
                <div class="box mb-3 ml-2">
                    <div class="box-header with-border">
                        Colours
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        @foreach($inspiration->colors as $color)
                        <span class='color-span' style="background-color:{{ $color->description }}">&nbsp;&nbsp;</span>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                <div class="p-3">
                        <h1 class="title">{{ $inspiration->name }}</h1>
                </div>
                <div class="p-3">
                    <div class="info-box ">
                        <span class="info-box-icon"><i class="fa fa-tags"></i></span>
            
                        <div class="info-box-content">
                            <span class="info-box-number">You are tagged as {{ optional($inspirationtag->role)->display_name }}</span>
                            <br>
                            @if($inspirationtag->status  == 'pending')
                                <button onclick="$('#confirm-form').submit()" class="btn btn-success"><i class="fa fa-check"></i> Confirm</button>
                                <button onclick="$('#reject-form').submit()"  class="btn btn-danger"><i class="fa fa-ban"></i> Reject</button>
                                
                                <!-- for accept -->
                                <form method="POST" action="{{ route('dashboard.inspiration.tags.changestatus', $inspirationtag->id) }}" id="confirm-form">
                                    @csrf
                                    <input type="hidden" name="status" value="confirmed">
                                </form>  

                                <!-- for reject -->
                                <form method="POST" action="{{ route('dashboard.inspiration.tags.changestatus', $inspirationtag->id) }}" id="reject-form">
                                    @csrf
                                    <input type="hidden" name="status" value="rejected">
                                </form>  
                            @elseif($inspirationtag->status == "rejected")
                                <button  onclick="$('#pending-form').submit()" class="btn btn-default"><i class="fa fa-ban"></i> Cancel Reject</button>

                                <!-- for accept -->
                                <form method="POST" action="{{ route('dashboard.inspiration.tags.changestatus', $inspirationtag->id) }}" id="pending-form">
                                    @csrf
                                    <input type="hidden" name="status" value="pending">
                                </form> 
                            @else
                                <button  onclick="$('#pending-form').submit()" class="btn btn-default"><i class="fa fa-ban"></i> Untag</button>

                                <!-- for accept -->
                                <form method="POST" action="{{ route('dashboard.inspiration.tags.changestatus', $inspirationtag->id) }}" id="pending-form">
                                    @csrf
                                    <input type="hidden" name="status" value="pending">
                                </form>  
                            @endif
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    {{-- @if(Auth::user()->company->allPermissions()  && $inspirationtag->status  == 'confirmed') --}}
                    @if( $inspirationtag->status  == 'confirmed')
                        <h3>
                            Décor
                        </h3>
                        <div class="row mb-5">
                            <div class="col-sm-4">
                                <form action="{{ route('dashboard.inspiration.tags.addproduct', $inspiration->id) }}" method="POST">
                                    @csrf
                                    <p>
                                        <label for="">Add Decors</label>
                                        <select type="text" name="product" data-type="select2" placeholder="search decor" class="form-control">
                                            @foreach($products as $productList)
                                                <option @if(in_array($productList->id, $inspiration->products()->pluck('products.id')->toArray())) disabled @endif value="{{ $productList->id }}">{{ $productList->name }}</option>
                                            @endforeach
                                        </select>
                                    </p>
                                    <p>
                                        <button type="submit" class="btn btn-warning"> Submit</button>
                                    </p>
                                </form>
                            </div>
                            <div class="col-sm-8">
                                <div class="row">
                                    @foreach($inspiration->products()->whereCompanyId(Auth::user()->company_id)->get() as $product)
                                    <div class="col-xs-4">
                                            <div class="box card-box">
                                                    <div class="box-header p-0">
                                                        <a href="{{ route('dashboard.product.show', $product->slug) }}"></a>
                                                            <img src="{{ $product->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png') }}" class="img img-responsive" style="width:100%" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="box-body pt-0">
                                                        <small class="text-muted">{{ optional($product->categoryType('product_type')->first())->name }}</small>
                                                        <div class="colors">
                                                            @foreach($product->colors as $color)
                                                                <span style="background-color:{{$color->description}};">&nbsp;</span>
                                                            @endforeach
                                                        </div>
                                                        <h4>
                                                            <a href="">{{ $product->name }}</a>
                                                        </h4>
                                                        <p class="text-muted">
                                                            <small>
                                                                {{ Str::limit($product->description, 60) }}
                                                            </small>
                                                        </p>
                                                        <div class="bottom-buttons">
                                                            <div class="row text-muted">
                                                                <div class="col-xs-3">
                                                                    <i class="fa fa-comments-o"></i> {{ $product->reviews()->count() }}
                                                                </div>
                                                                <div class="col-xs-3">
                                                                    <i class="fa fa-heart-o"></i>  {{ $product->hearts()->count() }}
                                                                </div>
                                                                <div class="col-xs-3">
                                                                    <i class="fa fa-file-image-o mr-1"></i> {{ $product->images()->where('type', '!=', 'thumbnail')->count() }}
                                                                </div>
                                                                <div class="col-xs-3">
                                                                    <a href="{{ route('dashboard.inspiration.tags.removeproduct', [$inspiration->id, $product->id]) }}" class="btn btn-xs btn-block btn-danger"><i class="fa fa-trash"></i></a>
                                                                    {{-- <div class="dropdown">
                                                                        <a class="btn btn-xs btn-block dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                                            <i class="fa fa-ellipsis-v"></i>
                                                                        </a>
                                                                        <ul class="dropdown-menu dropdown-menu-right">
                                                                            <li><a href="{{ route('dashboard.inspiration.tags.removeproduct', [$inspiration->id, $product->id]) }}"><i class="fa fa-pencil"></i> Remove</a></li>
                                                                        </ul>
                                                                    </div> --}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                            </div>
                                        </div>
                                        @endforeach
                                </div>
                          

                            </div>
                        </div>
                    @endif
                </div>
                @if(Request::get('gallery') == true)
                    <div class="box no-border">
                        <div class="box-body">
                            <br>
                            <h4>Gallery</h4>
                            <a href="{{ route('dashboard.inspiration.tags.show', $inspirationtag->id) }}"><i class="fa fa-ban"></i> Hide Gallery</a>
                            <div class="inspiration-grid mt-3">
                                @foreach($gallery as $image)
                                    <div class="inspiration-item" id="div-{{ $image->id }}">
                                        <div class="image-gallery">
                                            <a href="/images/gallery/{{ $image->path }}" data-lightbox="PORTFOLIO">
                                                <img src="/images/gallery/{{ $image->path }}" class="img img-responsive mb-4" alt="gallery-image" class="margin">
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @else
                    
                    <a href="?gallery=true" ><i class="fa fa-image"></i> Show Gallery</a>
                    
                @endif

            </div>
        </div>
    </div>
@endsection