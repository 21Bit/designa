@extends('dashboard.includes.layouts.main')

@section('page_title', 'Inspiration Tags')

@section("content-hearder")
        <h1>
            Inspirations Tags
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard.home') }}"> Home</a></li>
            <li class="active">Inpirations Tags</li>
        </ol>
@endsection

@section('content')
    <form>
        <div class="row">
            <div class="col-sm-3">
                <select class="form-control input-sm" name="role">
                    <option value="">- all role - </option>
                    @foreach($roles as $role)
                        <option @if(Request::get('role') == $role->id) selected @endif value="{{ $role->id }}">{{ $role->display_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-sm-4">
                <div class="input-group">
                    <input value="{{ Request::get('q') }}"type="text" name="q" class="form-control input-sm" placeholder=" Search Inspiration.." aria-label="...">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-search"></i> Search</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="box mt-3">
        <table class="table">
            <thead>
                <tr>
                    <th>Inspiration</th>
                    <th>Role</th>
                    <th>Supplier</th>
                    <th>Status</th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @forelse($inspirationtags as $tag)
                    <tr>
                        <td>{{ $tag->inspiration->name }}</td>
                        <td>{{ $tag->role->display_name }}</td>
                        <td>{{ optional($tag->company)->name }}</td>
                        <td style="width:150px;">
                            @if($tag->status  == 'confirmed')
                                <label for="" class="btn btn-success btn-xs btn-block" style='border-radius: 0px;'><i class="fa fa-check"></i> CONFIRMED </label>
                                <small><i>at {{ date('M. d, Y h:ia' , strtotime($tag->updated_at)) }}</i></small>
                            @elseif($tag->status == "rejected")
                                <label for="" class="btn btn-danger btn-xs btn-block" style='border-radius: 0px;'><i class="fa fa-check"></i> REJECTED </label>
                                <small><i>at {{ date('M. d, Y h:ia' , strtotime($tag->updated_at)) }}</i></small>
                            @else
                                <label for="" class="btn btn-default btn-xs btn-block" style='border-radius: 0px;'><i class="fa fa-check"></i> PENDING </label>
                            @endif
                        </td>
                        <td class="text-right">Tagged on {{ date('M. d, Y h:ia ' , strtotime($tag->created_at)) }}</td>
                        <td class="text-right">
                            <a href="{{ route('dashboard.inspiration.tags.show', $tag->id) }}" class="btn btn-xs btn-default"><i class="fa fa-eye"></i> More Detail</a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="6" class="text-center text-muted"> No inspiration tag found</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
        <div class="p-2">
            {{ $inspirationtags->links() }}
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $('select').change(function(){
            $(this).closest('form').submit()
        })
    </script>
@endpush
