@extends('dashboard.includes.layouts.main')

@section('page_title', 'Chat')

{{-- @section("content-hearder")
    <h1>
        Chat
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">System / Color</li>
    </ol>
@endsection --}}

@section('content')
    <chat-component userid="{{ Auth::user()->id }}"></chat-component>
@endsection