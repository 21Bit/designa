@extends('dashboard.includes.layouts.main')

@section('page_title', 'Activity Logs')

@section("content-hearder")
        <h1>
            Activity Log
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Activity Log</li>
        </ol>
@endsection

@section('content')
        <div class="box">
            <div class="box-body">
                <br>
                <div class="table-responsive">
                        {!! $html->table() !!}
                </div>
            </div>
        </div>
        <div class="box mt-5">
            <div class="box-body">
                <div class="chart-container">
                    
                    <div class="row mb-3">                        
                        <div class="col-xs-3"> 
                            <label for="activity_type">-Activity-</label>                          
                            <select name="activity_type" id="activity_type" class="form-control">
                                <option value="LOGIN">Login</option>
                                <option value="INSPIRATION" >Inspiration</option>
                                <option value="INVITE" >Invite</option>
                            </select>                            
                        </div>
                        <div class="col-xs-3">
                            <label for="month">-Month-</label>
                            <select name="month" id="month" class="form-control">
                                <option value="01">January</option>
                                <option value="02" >Febuary</option>
                                <option value="03" >March</option>
                                <option value="04" >April</option>
                                <option value="05" >May</option>
                                <option value="06" >June</option>
                                <option value="07" >July</option>
                                <option value="08" >August</option>
                                <option value="09" >September</option>
                                <option value="10" >October</option>
                                <option value="11" >November</option>
                                <option value="12" >December</option>
                            </select>
                        </div>
                        <div class="col-xs-3">
                            <label for="year">-Year-</label>
                            <select name="year" id="year" class="form-control">
                            </select>
                        </div>
                    </div>
                    {{-- <div class="col-xs-12">
                        <span v-show="loadingData">
                        <i class="fa fa-spin fa-spinner"></i> Loading data...
                        </span>
                    </div> --}}
                    <canvas id="myAreaChart" width="100%" height="30"></canvas>
                </div>
            </div>
        </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap.min.css">
    <style>
        .chart-container {
            position: relative;
            margin: auto;
        }
    </style>
@endpush

@push('scripts')
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap.min.js"></script>
    {!! $html->scripts() !!}
    <script src="{{url( 'vendor/jquery.min.js' )}}"></script>
    <script src="{{url( 'vendor/Chart.min.js' )}}"></script>
    <script>
            $.ajax({
                url : "{{ route('activity.statistics') }}",
                type : 'GET',
                processData : false,
                contentType : false,
                success     : function (res) {                        
                    fetchData(res);
                }
            });
            
            
            function fetchData(res)
            {
                var json = [];
                        $.map(res.stats, function(value, index) {
                            json.push(value);                        
                        });				 

                        var date = json.map(function (e) {
                            return e.date;
                        });
                        
                        var mobile = json.map(function (e) {
                            return e.mobile;
                        });
                                               
                        var web = json.map(function (e) {
                            return e.web;
                        });
                        
                        createCompletedJobsChart( date, mobile, web );

                        $('#year').empty();
                        $.map(res.years, function(value, index) {
                            for (var i = 0; i < value.length; i++) {
                                $('#year').append('<option value="' + value[i] + '">' + value[i]+ '</option>');
                            }                       
                        });	

                        $.map(res.month, function(value, index) {
                            $('#month option[value="'+value+'"]').prop('selected', true);
                        });	
            }

            function getData() {
                const month = $('#month').val();
                const year = $('#year').val()
                const activity_type = $('#activity_type').val();
                
                $.ajax({
                    url:"{{ route('activity.statistics') }}",
                    method:"GET",
                    data:{ _token : '{{ csrf_token() }}',  month : month,  year : year, activity_type : activity_type},
                    dataType:"json",
                    success:function(res)
                    {
                        fetchData(res)
                    }
                });
            }

            $('#activity_type').change(function(){                
                // fetchData();
                getData();
            });

            $('#month').change(function(){
                // const school_year = $('#school_year').val();
                getData();
                                
            });

            $('#year').change(function(){                
                getData();
            });


            function createCompletedJobsChart( labels, mobile, web ) {
                var ctx = document.getElementById("myAreaChart");
                var myLineChart = new Chart(ctx, {
                    type: 'line',
                    data: {
                        labels: labels,
                        datasets: [
                            {
                                label: "Mobile",
                                lineTension: 0.1,
                                fill: false,
                                borderColor: "#f39c12",
                                pointRadius: 5,
                                pointBackgroundColor: "#f39c12",
                                pointBorderColor: "#f39c12",
                                pointHoverRadius: 5,
                                pointHoverBackgroundColor: "#f39c12",
                                pointHitRadius: 20,
                                pointBorderWidth: 2,
                                data: mobile, 
                            },					
                            {
                                label: "Web",
                                lineTension: 0.1,
                                fill: false,
                                borderColor: "rgba(2,117,216,1)",
                                pointRadius: 5,
                                pointBackgroundColor: "rgba(2,117,216,1)",
                                pointBorderColor: "rgba(255,255,255,0.8)",
                                pointHoverRadius: 5,
                                pointHoverBackgroundColor: "rgba(2,117,216,1)",
                                pointHitRadius: 20,
                                pointBorderWidth: 2,
                                data: web, 
                            }
                        ],
                        
                    },
                    options: {
                        scales: {
                            xAxes: [{
                                time: {
                                    unit: 'date'
                                },
                                gridLines: {
                                    display: false
                                },
                                ticks: {
                                    display: true,
                                    beginAtZero: true
                                }   
                            }],
                            yAxes: [{
                                ticks: {
                                    display: true,
                                    beginAtZero: true
                                },
                                gridLines: {
                                    display: false
                                }
                            }],
                        },
                        legend: {
                            display: true
                        }
                    }
                });
            }

    </script>
@endpush