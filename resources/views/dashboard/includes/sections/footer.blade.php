    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> {{ version() }}
        </div>
        <strong>Copyright &copy; {{ date('Y') }}.</strong> All rights
        reserved.
    </footer>
</div>
@routes
<script src="{{ asset('js/dashboard.js') }}"></script>
@stack('scripts')
    @show

</body>
</html>