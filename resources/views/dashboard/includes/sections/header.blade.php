<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="/images/logo.ico">

    <title>@yield("page_title", config('app.name')) - {{ config("app.name") }}</title>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <title>Document</title>
    <link rel="stylesheet" href="{{ mix('css/dashboard.css') }}">
    @stack('styles')
        @show
</head>
<!DOCTYPE html>
<html>
<body class="hold-transition skin-yellow-light sidebar-mini">
    <div class='overlay-loading'>
        <h4>
            <i class="fa fa-spinner fa-spin"></i>
            Uploading please wait...
        </h4>
    </div>
    @yield('unity-container')
    <div class="wrapper" id="app">
        