<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ Auth::user()->getProfilePicture() }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li><a href="{{ route('dashboard.home') }}"><i class="fa fa-dashboard"></i> <span>Home</span></a></li>

            @permission('inspiration')
                @access('load-inspiration')
                <li class="treeview acive">
                    <a href="#">
                        <i class="fa fa-pie-chart"></i>
                        <span>Inspiration</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu" style="display:block">
                        <li><a href="{{ route('dashboard.inspiration.create') }}">Add New</a></li>
                        <li><a href="{{ route('dashboard.inspiration.index') }}">All</a></li>
                        @access(['tag-and-be-tagged'])
                        <li><a href="{{ route('dashboard.inspiration.tags.index') }}">Tags</a></li>
                        @endaccess
                    </ul>
                </li>
                @endaccess
            @endpermission


            {{-- @permission('product') --}}
            @if(Auth::user()->hasAccess('load-product') && Auth::user()->isAbleTo('product'))
                <li class="treeview acive">
                    <a href="#">
                        <i class="fa fa-folder"></i>
                        <span>Product</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>

                    <ul class="treeview-menu" style="display:block">
                        <li><a href="{{ route('dashboard.product.create') }}">Add New</a></li>
                        <li><a href="{{ route('dashboard.product.index') }}">All</a></li>
                        @if(Auth::user()->type == "administrator" || Auth::user()->is_superadmin)
                            <li><a href="{{ route('dashboard.category.index', ['type' => 'product_type', 'module' => 'product', 'credits' => true]) }}">Types</a></li>
                        @endif
                    </ul>
                </li>
                <li>
                    <a href="{{ route('dashboard.quote.index') }}"><i class="fa fa-folder"></i> <span>Quote</span>
                    @if(optional(auth()->user()->company)->uncompleteQoutes())
                        <div class="label label-primary pull-right">
                            {{ optional(auth()->user()->company)->uncompleteQoutes() }}
                        </div>
                    @endif
                    </a>
                </li>
            @endif
            {{-- @endpermission --}}
            @access('create-your-own-event-3d-using-template')
                 <li class="treeview  {!! back_end_active_menu('3d', 2) !!}">
                    <a href="#">
                        <i class="fa fa-cube"></i>
                        <span>3D Models</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu " {!! back_end_active_menu_true('3d', 2) !!} >
                        @access('transform-a-room-with-custom-3d-builds')
                            <li><a href="{{ route('dashboard.baselayer.index') }}">Base Layer</a></li>
                        @endaccess
                        <li><a href="{{ route('dashboard.template.index') }}">Templates</a></li>
                        <li><a href="{{ route('dashboard.floorplan.index') }}">Floor Plans</a></li>
                    </ul>
                </li>
            @endaccess

            @access('create-pitch-packs-in-designa')
                <li>
                    <a href="{{ route('dashboard.event-board.index') }}"><i class="fa fa-folder"></i> <span>Event Board</span></a>
                </li>
            @endaccess

            @permission('venue')
                @if(Auth::user()->company->single_venue)
                    <li>
                        <a href="{{ route('dashboard.venue.index') }}"><i class="fa fa-map-marker"></i> Venue Set-up</a>
                    </li>
                @else
                    <li class="treeview {!! back_end_active_menu('venue', 2) !!}">
                        <a href="#">
                            <i class="fa fa-map-marker"></i>
                            <span>Venue</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu" {!! back_end_active_menu_true('venue', 2) !!}>
                            <li><a href="{{ route('dashboard.venue.create') }}">Add New</a></li>
                            <li><a href="{{ route('dashboard.venue.index') }}">All</a></li>
                            @if(Auth::user()->type == "administrator" || Auth::user()->is_superadmin)
                                <li><a href="{{ route('dashboard.category.index', ['type' => 'venue_type', 'module' => 'venue', 'credits' => true]) }}">Types</a></li>
                                <li><a href="{{ route('dashboard.category.index', ['type' => 'amenity', 'module' => 'venue']) }}">Amenities</a></li>
                                <li><a href="{{ route('dashboard.category.index', ['type' => 'catering_option', 'module' => 'venue']) }}">Catering Options</a></li>
                                <li><a href="{{ route('dashboard.category.index', ['type' => 'beverage_option', 'module' => 'venue']) }}">Beverage Options</a></li>
                                <li><a href="{{ route('dashboard.category.index', ['type' => 'average_cost_per_head', 'module' => 'venue']) }}">Average Cost per Head</a></li>
                            @endif
                        </ul>
                    </li>
                @endif
            @elseif(Auth::user()->is_superadmin)
                <li class="treeview {!! back_end_active_menu('venue', 2) !!}">
                    <a href="#">
                        <i class="fa fa-map-marker"></i>
                        <span>Venue</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu" {!! back_end_active_menu_true('venue', 2) !!}>
                        <li><a href="{{ route('dashboard.venue.create') }}">Add New</a></li>
                        <li><a href="{{ route('dashboard.venue.index') }}">All</a></li>
                        @if(Auth::user()->type == "administrator" || Auth::user()->is_superadmin)
                            <li><a href="{{ route('dashboard.category.index', ['type' => 'venue_type', 'module' => 'venue', 'credits' => true]) }}">Types</a></li>
                            <li><a href="{{ route('dashboard.category.index', ['type' => 'amenity', 'module' => 'venue']) }}">Amenities</a></li>
                            <li><a href="{{ route('dashboard.category.index', ['type' => 'catering_option', 'module' => 'venue']) }}">Catering Options</a></li>
                            <li><a href="{{ route('dashboard.category.index', ['type' => 'beverage_option', 'module' => 'venue']) }}">Beverage Options</a></li>
                            <li><a href="{{ route('dashboard.category.index', ['type' => 'average_cost_per_head', 'module' => 'venue']) }}">Average Cost per Head</a></li>
                        @endpermission
                    </ul>
                </li>
            @endif



            {{-- <li><a href="https://adminlte.io/docs"><i class="fa fa-envelope"></i> <span>Message</span></a>
            </li> --}}
            @if(Auth::user()->is_superadmin)
                <li class="treeview {{ back_end_active_menu('company', 2) }}">
                    <a href="#">
                        <i class="fa fa-folder"></i>
                        <span>Company</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu"  {!! back_end_active_menu_true('company', 2) !!}>
                        <li><a href="{{ route('dashboard.company.create') }}">Add New</a></li>
                        <li><a href="{{ route('dashboard.company.index') }}">All</a></li>
                    </ul>
                </li>
            @endpermission
            @if(Auth::user()->is_superadmin)
                <li class="treeview {{ back_end_active_menu('blog', 2) }}">
                    <a href="#">
                        <i class="fa fa-folder"></i>
                        <span>Blog</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu"  {!! back_end_active_menu_true('blog', 2) !!}>
                        <li><a href="{{ route('dashboard.blog.create') }}">Add New</a></li>
                        <li><a href="{{ route('dashboard.blog.index') }}">All</a></li>
                    </ul>
                </li>
            @endpermission
            @if(Auth::user()->is_superadmin)
                <li class="treeview {{ back_end_active_menu(['color', 'category', 'role', 'general', 'pricing-component'], 2) }}">
                    <a href="#">
                        <i class="fa fa-gear"></i>
                        <span>System</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu"  {!! back_end_active_menu_true(['color', 'category', 'role', 'general', 'pricing-component', 'user', 'logs', 'subscription-plan'], 2) !!}>
                        <li><a href="{{ route('dashboard.subscription-plan.index')}}">Subscription Plan</a></li>
                        <li><a href="{{ route('dashboard.activity-log.index')}}">Activity Logs</a></li>
                        <li class="{!! back_end_active_menu(['user'], 2, "active") !!}"><a href="{{ route('dashboard.user.index') }}">Users</a></li>
                        <li><a href="{{ route('dashboard.category.index', ['type' => 'cater',  'module' => 'category']) }}">Caters</a></li>
                        <li><a href="{{ route('dashboard.category.index', ['type' => 'color', 'module' => 'category']) }}">Colors</a></li>
                        <li><a href="{{ route('dashboard.role.index') }}">Roles</a></li>
                        <li class="{!! back_end_active_menu(['pricing-component'], 2, "active") !!}"><a href="{{ route('dashboard.pricing-component.index') }}">Pricing Components</a></li>
                    </ul>
                </li>
            @endpermission
            {{-- <li class="header">OTHER LINKS</li>
            <li><a href="#"><i class="fa  fa-smile-o text-yellow"></i> <span>About Us</span></a></li>
            <li><a href="#"><i class="fa  fa-question-circle-o text-red"></i> <span>Help</span></a></li> --}}
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
