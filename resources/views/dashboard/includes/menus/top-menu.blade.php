<header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo" style="background-color:#f9fafc">
        <span class="logo-mini" style="color:#f39c12"><b>D</b>S</span>
        <img class="img img-responsive" src="/images/designa-logo-sm.png"  onerror="this.onerror=null;this.src='http://example.com/existent-image.jpg';">
    </a>


    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <notification-component listroute="{{ route('dashboard.notification.index') }}"></notification-component>
                {{-- <li>
                    <a href="{{ route('dashboard.chat.index') }}"><i class="fa fa-envelope-o"></i></a>
                </li> --}}
                <li>
                    <a href="{{ route('dashboard.invite.index') }}">Invite Others</a>
                </li>
                <li>
                    <a href="/">Visit Website</a>
                </li>
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ Auth::user()->getProfilePicture() }}" class="user-image" alt="User Image">
                        <span class="hidden-xs">{{ Auth::user()->name }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="{{ Auth::user()->getProfilePicture() }}" class="img-circle" alt="User Image">

                            <p>
                                {{ Auth::user()->name }} - {{ ucFirst(Auth::user()->type) }} <br />
                                <small>{{ optional(Auth::user()->company)->name }}</small>
                                <small>Member since {{ Auth::user()->created_at->format('M. Y') }}</small>
                            </p>
                        </li>

                        <!-- Menu Body -->
                        {{-- <li class="user-body">
                            <div class="row">
                                <div class="col-xs-4 text-center">
                                    <a href="#">Followers</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Sales</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#">Friends</a>
                                </div>
                            </div>
                            <!-- /.row -->
                        </li> --}}
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{ route('dashboard.profile.index', ['tab' => 'business']) }}" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="#" class="btn btn-default btn-flat"  href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Sign out</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
