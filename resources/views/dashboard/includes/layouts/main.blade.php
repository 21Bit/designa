@include('dashboard.includes.sections.header')

@include('dashboard.includes.menus.top-menu')

@include('dashboard.includes.menus.side-menu')
    <div class="content-wrapper">
        <section class="content-header">
            @yield('content-header')
            @if(Auth::user()->onTrial())
                <div class="alert p-2 mb-0">
                    Trial ends at {{  Auth::user()->trialEndsAt()->format('M d, Y h:iA') }}. <a href="{{ route('dashboard.profile.index', ['tab' => 'subscription'])  }}" class="text-orange"> Learn more about premium plans.</a>
                </div>
            @endif
        </section>
        <section class="content">
           @yield('content')
        </section>
    </div>
@include('dashboard.includes.sections.footer')

