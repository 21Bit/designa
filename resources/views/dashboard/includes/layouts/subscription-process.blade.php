<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="/images/logo.ico">

    <title>@yield("page_title", config('app.name')) - {{ config("app.name") }}</title>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    @stack('styles')
        @show
</head>
<!DOCTYPE html>
<html>
<body>
    <div id="app">
        @yield('content')
        <div class="text-right pr-3 pb-3 mt-5">
            <footer class="main-footer ">
                <strong>Copyright &copy; {{ date('Y') }}.</strong> All rights
                reserved.
                <div class="pull-right hidden-xs">
                    <b>Version</b> {{ version() }}
                </div>
            </footer>
        </div>
    </div>
@routes
<script src="{{ asset('js/dashboard.js') }}"></script>
@stack('scripts')
    @show

</body>
</html>
        