<script src="https://cdn.tiny.cloud/1/3etjjswjc4u1mtvnr70q7p3ahavix9rhnp8puim5vad1kjt7/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
        tinymce.init({
        selector: 'textarea',
        menubar: false,
        height:500,
        branding: false,
        force_br_newlines : false,
        force_p_newlines : false,
        forced_root_block : '',
        plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table paste code'
        ],
        toolbar: 'undo redo | formatselect | ' +
                'bold italic backcolor | alignleft aligncenter ' +
                'alignright alignjustify | bullist numlist outdent indent | paste' +
                'removeformat | help',
        paste_data_images: true,
        content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'});
</script>