@extends('dashboard.includes.layouts.main')
@section('page_title', 'Create Inspiration')
@section("content-hearder")
    <h1>
        Inspiration
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('dashboard.inspiration.index') }}">Inspiration</a></li>
        <li class="active">Create Inspiration</li>
    </ol>
@endsection

@section('content')
<form action="{{ route('dashboard.inspiration.update', $inspiration->id) }}"  id='form-with-loading' method="post" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="text-right mb-2">
        <button class="btn btn-warning" type="submit" @if(!Auth::user()->is_superadmin) @if($inspiration->is_lock) disabled @endif  @endif><i class="fa fa-save"></i> Save</button>
        <a href="{{ route('dashboard.inspiration.index') }}" class="btn btn-default"><i class="fa fa-ban"></i> Cancel</a>
    </div>
    @include("dashboard.includes.alerts.error")
    @include("dashboard.includes.alerts.success")
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-sm-3">
                    <p>
                        <label for="">Status *</label>
                        <select name="is_published" @if(!Auth::user()->is_superadmin) @if($inspiration->is_lock) disabled @endif  @endif class="form-control">
                            <option value="1" >Published</option>
                            <option value="0" @if($inspiration->is_published == 0) selected @endif  >Draft</option>
                        </select>
                    </p>
                    <p>
                        <input type="checkbox"  data-type="icheck" name="proceed" id="proceed"> <label for="proceed" style="font-weight: normal">Proceed to Manage</label>
                    </p>
                    @if(Auth::user()->is_superadmin)
                        <p>
                            <input type="checkbox" {{ $inspiration->featured == 1 ? 'checked' : '' }} 
                                data-type="icheck" value="1" name="featured" id="featured">

                            <label for="featured" style="font-weight: normal">Feature Status</label>                            
                        </p>
                    @endif
                    <p>
                        <label class="text-muted mb-0" for="">Thumbnail Image</label>
                        <div class="form-group mt-0">
                            <label for="thumbnail" class="btn btn-default btn-xs"> Change Image</label>
                        </div>
                        {{-- <label for="">Thumbnail Image <small><i>({{ config('filesystems.accepted_image_types') }})</i></small></label> --}}
                        <input type="file" style="display: none"  id="thumbnail"  data-type="image-picker" data-preview="#preview-thumbnail" data-preview-type='img' name="thumbnail">
                        <div class="image-overlay">
                            <img src="{{ $inspiration->image('thumbnail')->path('thumbnails','/images/placeholders/placeholder.png') }}" id="preview-thumbnail" alt="" class="img img-responsive">
                        </div>
                    </p>
                    <hr>
                    <p>
                        {{-- <label for="">Cover Image <small>(1920x450 recomended)<i>({{ config('filesystems.accepted_image_types') }})</i></small></label> --}}
                        
                        <label class="text-muted mb-0" for="">Cover Image</label>
                        <div class="form-group mt-0">
                            <p class="help-block  mt-0">
                                <small>
                                    Recomended size: 1920x450px
                                </small>
                            </p>
                            <label for="cover" class="btn btn-default btn-xs"> Change Image</label>
                        </div>

                        <input type="file" style="display: none"  id="cover" data-type="image-picker" data-preview="#preview-cover" data-preview-type='img' name="cover">
                        {{-- <label for="cover" class="btn btn-default btn-xs mb-2"  > Change Image</label> --}}
                        <img src="{{ $inspiration->image('cover')->path('covers','/images/placeholders/placeholder.png') }}"  id="preview-cover"  alt="" class="img img-responsive">
                       
                        {{-- <label class="text-muted" for="">Cover Picture</label> <small><i>1920x450</i></small> <br />
                        <input type="hidden" name="cropped_image_cover">
                        <input type="file" style="display:none" id="img-picker-cover" name="company_cover_picture" data-type="image-picker" data-preview="#preview-cover" data-preview-type='img'  accept="image/*">
                        <label for="img-picker-cover" class="btn btn-default btn-xs mb-3"> Change Cover</label>
                        <img src="{{ $inspiration->image('cover')->path('covers','/images/placeholders/placeholder.png') }}" id="img-preview-cover" class="img img-responsive" alt=""> --}}
                    </p>
                    <hr>
                    <div class="text-center">
                        <p class="margin text-muted">Note: Accepted types are <code>{{ config('filesystems.accepted_image_types') }}</code></p>
                    </div>

                    @if(Auth::user()->is_superadmin)
                        <hr>
                        <p>
                            <input type="checkbox" {{ $inspiration->is_lock ==   1 ? 'checked' : '' }} 
                                value="1" data-type="icheck"  name="is_lock" id="is_lock">
                            <label for="is_lock" style="font-weight: normal">Lock Status</label>    
                            <div class="row">
                                <div class="col-sm-8">
                                    <i>Reason</i>
                                    <select id="reason"  class="form-control" name="reason">
                                        @foreach($lockingReasons as $lockingReason)
                                            <option value="{{ $lockingReason  }}">{{ $lockingReason }}</option>
                                        @endforeach
                                    </select>                        
                                </div>
                                <div class="col-sm-4">
                                    <i>Send Email</i>
                                    <select name="sendLockEmail" class="form-control">
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                            </div>
                        </p>
                    @endif
                </div>
                <div class="col-sm-9">
                    <!-- name and year -->
                    <div class="row">
                        <div class="col-sm-8">
                            <p>
                                <label for="">Name *</label>
                                <input type="text" required autofocus name="name" value="{{ $inspiration->name }}" placeholder="Project name" required class="form-control">
                            </p>
                        </div>
                        <div class="col-sm-4">
                            <p>
                                <label for="">Year</label>
                                <select name="project_year" id="" class="form-control">
                                    @foreach($years as $year)
                                    <option @if($inspiration->project_year == $year) selected @endif value="{{ $year }}">{{ $year }}</option>
                                    @endforeach
                                </select>
                            </p>
                        </div>
                          <div class="col-sm-8">
                            {{-- <p>
                                <label for="">Location</label>
                                <input type="text" value="{{ $inspiration->location }}" name="location" class="form-control">
                            </p> --}}
                                <p>
                                    <label for="">Location</label> 
                                    <inspiration-venue-input standalone='yes' defaultvenueid="{{ $inspiration->venue_id }}" defaultlocation="{{ $inspiration->venue ? $inspiration->venue->name : $inspiration->location }}" defaultlatitude="{{ $inspiration->latitude }}" defaultlongitude="{{ $inspiration->longitude }}"  styleclass='form-control'></inspiration-venue-input>
                                </p>
                        </div>

                        <div class="col-sm-4">
                            <p>
                                <label for="">Images By <small><i>(optional)</i></small></label>
                                <input type="text" name="images_by" value="{{ $inspiration->images_by }}" class="form-control">
                            </p>
                        </div>
                    </div>

                    <!-- desciption -->
                    <div class="row">
                        <div class="col-sm-12">
                            <p>
                                <label for="">Desciption</label>
                                <textarea name="description" cols="30" rows="6" id='texteditor' class="form-control">{!! $inspiration->description  !!}</textarea>
                            </p>    
                        </div>
                    </div>

                    <!-- category and colors -->
                    <category-setup route-categories="dashboard.inspiration.api.caters"
                        default-category="{{ optional($inspiration->categories()->whereType('cater')->first())->id }}"
                        default-style="{{ optional($inspiration->categories()->whereType('style')->first())->id }}"
                        default-setting="{{ optional($inspiration->categories()->whereType('setting')->first())->id }}"
                    ></category-setup>
              
                    <!-- other suppliers -->
                    <h4 class="mb-3">Colours</h4>
                    @foreach($colors as $color)
                        <div class="pl-3">
                            <input type="checkbox" data-type="icheck" @if(in_array($color->id, $inspiration->categoryType('color')->pluck('id')->toArray())) checked @endif  id="color-{{ $color->id }}" name='colors[]' value="{{ $color->id }}">
                            <label style="font-weight: normal;" for="color-{{ $color->id }}"><span class='color-span mr-2' style="background-color:{{ $color->description }}">&nbsp;&nbsp;</span> {{ $color->name }}</label>
                        </div>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

 {{-- <div class="modal" id="cropper-modal-cover" tabindex="-1" role="dialog" style="overflow: auto">
    <div class="modal-dialog modal-lg" style="width:100%" role="document">
        <div class="modal-content">
            <div class="modal-body text-center" style="overflow:auto; width:100%">
                <label>Crop Cover</label>
                <div id="crop-picture-container-cover"></div>
                <button id="crop-done-btn-cover" class="btn btn-primary btn-lg">Done</button>
            </div>
        </div>
    </div>
</div> --}}
@endsection

{{-- @push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.4/croppie.css" integrity="sha256-M8o9uqnAVROBWo3/2ZHSIJG+ZHbaQdpljJLLvdpeKcI=" crossorigin="anonymous" />
@endpush --}}

@push('scripts')
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.4/croppie.min.js" integrity="sha256-bQTfUf1lSu0N421HV2ITHiSjpZ6/5aS6mUNlojIGGWg=" crossorigin="anonymous"></script> --}}
    <script src="{{ asset('/dashboard/ckeditor/ckeditor.js')}} "></script>
    <script>
        //  $('#cropper-modal-cover').on('shown.bs.modal', function () {
        //     $('body').addClass('zoomout')
        // })          
        // $('#cropper-modal-cover').on('hidden.bs.modal', function () {
        //     $('body').removeClass('zoomout')
        // })          

        // var resizecover = $('#crop-picture-container-cover').croppie({
        //     enableExif: true,
        //     enableOrientation: true,
        //     viewport: { // Default { width: 100, height: 100, type: 'square' } 
        //         width: 1920,
        //         height: 450,
        //         type: 'square'
        //     },
        //     enforceBoundary: false,
        //     boundary: {
        //         width: 1980,
        //         height: 600
        //     }
        // });
        
            
        // $('#img-picker-cover').on('change', function () {
        //     var reader = new FileReader();
        
        //     $("#cropper-modal-cover").modal("show")
        
        //     reader.onload = function (e) {
        //         resizecover.croppie('bind', {
        //             url: e.target.result
        //         }).then(function () {
        //         });
        //     }
            
        //     reader.readAsDataURL(this.files[0]);
        
        // });
        
        // $("#crop-done-btn-cover").on('click', function (ev) {
        //     resizecover.croppie('result', {
        //         type: 'canvas',
        //         size: 'viewport'
        //     }).then(function (img) {
        //         $("#img-preview-cover").attr("src", img)
        //         $("input[name=cropped_image_cover").val(img)
        //         $("#cropper-modal-cover").modal("hide")
        //     });
        // });

  

        CKEDITOR.replace( 'texteditor', {
            toolbar: [
                { 
                name: 'document', 
                items: [ '-', 'NewPage', 'Preview', '-', 'Templates' ] 
                }, 
                [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo', ],
                { 
                name: 'basicstyles', 
                items: [ 'Bold', 'Italic']
                }
            ],
            enterMode : CKEDITOR.ENTER_BR,
            shiftEnterMode: CKEDITOR.ENTER_P,
            menubar: false,
            branding:false,
            forced_root_block : false,
        });

    

    </script>
@endpush
