@extends('dashboard.includes.layouts.main')
@section('page_title', 'Inspirations')
@section("content-header")
        <div class="row">
            <div class="col-sm-8">
                <h1 class='m-0 mb-2' style="display: inline-block">Inspiration</h1>
                <a href="{{ route('dashboard.inspiration.create') }}" class="btn btn-warning ml-3" style="margin-top: -20px"><i class="fa fa-plus"></i> Add New</a>
            </div>
            <div class="col-sm-4">
                <form>
                    <div class="input-group input-group-sm hidden-xs pull-right mt-3" style="width: 350px;">
                        <input type="text" name="search" value="{{ Request::get('search') }}" class="form-control pull-right" placeholder="Search inspiration">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
@endsection
@section('content')
    @if(Auth::user()->is_superadmin)
        <div class="box p-3">
            <div class="row">
                <form >
                    
                        <div class="col-sm-3">
                            Supplier
                            <select  data-type="select2" class="form-control input-sm" name="supplier">
                                <option value=""> All </option>
                                @foreach($suppliers as $supplier)
                                    <option @if(Request::get('supplier') == $supplier->id) selected @endif value="{{ $supplier->id }}" @if(Request::get('supplier') == $supplier->id) selected @endif >{{ $supplier->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-2 col-mb-2">
                            Featured
                            <select data-type="select2" class="form-control input-sm d-inline-block" name="feature">
                                <option value="">- All -</option>
                                <option value="1" {{ Request::get('feature') == 1 ? 'selected' : '' }}>Non Featured</option>
                                <option value="2" {{ Request::get('feature') == 2 ? 'selected' : '' }}>Featured</option>
                            </select>
                        </div>
                        <div class="col-sm-2 col-mb-2">
                            Publish Status
                            <select data-type="select2" class="form-control input-sm" name="status">
                                <option value="">All</option>
                                <option value="1" {{ Request::get('status') == 1 ? 'selected' : '' }}>Draft</option>
                                <option value="2" {{ Request::get('status') == 2 ? 'selected' : '' }}>Publish</option>
                            </select>
                        </div>
                        <div class="col-sm-2 col-mb-2">
                            Lock Status
                            <select data-type="select2" class="form-control input-sm" name="lock">
                                <option value=""> All</option>
                                <option value="1" {{ Request::get('lock') == 1 ? 'selected' : '' }}>Unlocked Only</option>
                                <option value="2" {{ Request::get('lock') == 2 ? 'selected' : '' }}>Lock Only</option>
                            </select>
                        </div>
                        <div class="col-sm-2 col-mb-2">
                            Cater
                            <select data-type="select2" class="form-control input-sm" name="category">
                                <option value=""> All </option>
                                @foreach($categories as $category)
                                    <option @if(Request::get('search') == $category->id) selected @endif value="{{ $category->id }}" @if(Request::get('category') == $category->id) selected @endif >{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                
                </form>
            </div>
        </div>
    @endif
    
    <div class="row">
        @forelse($inspirations as $inspiration)
            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6" id="div-{{ $inspiration->id }}">
                <div class="box card-box product-box">
                        <a href="{{ route('dashboard.inspiration.show', $inspiration->slug) }}">
                    <div class="box-header" style="padding-top:75%; background-image:url('{{ $inspiration->thumbnail }}'); background-size:cover; background-position:center;">
                            {{-- <img src="{{ $inspiration->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png') }}" class="img img-responsive" alt=""> --}}
                    </div>
                        </a>
                    <div class="box-body pt-0">
                        <small class="text-muted">{{ optional($inspiration->categories->where('type', 'cater')->first())->name }}</small>
                        <div class="colors">
                            @foreach($inspiration->colors as $color)
                                <span style="background-color:{{$color->description}};">&nbsp;</span>
                            @endforeach
                        </div>
                        <h4>
                            <a href="">{{ $inspiration->name }}</a>
                        </h4>
                        <p class="text-muted">
                            <small>
                                {!! Str::limit(strip_tags($inspiration->description), 60) !!}
                            </small>
                        </p>

                    </div>
                    <div class="box-footer">
                        <div class="bottom-buttons">
                            <div class="row text-muted">
                                <div class="col-xs-3">
                                    <i class="fa fa-comments-o"></i> {{ $inspiration->review_counts }}
                                </div>
                                <div class="col-xs-3">
                                    <i class="fa fa-heart-o"></i> {{ $inspiration->heart_counts }}
                                </div>
                                <div class="col-xs-3">
                                    <i class="fa fa-file-image-o mr-1"></i> {{ $inspiration->image_counts }}
                                </div>
                                <div class="col-xs-3">
                                    <div class="dropdown">
                                        <a class="btn btn-xs btn-block dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            <i class="fa fa-ellipsis-v"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-right dropdown-menu-up">
                                            <li>
                                                <a href="{{ route('dashboard.inspiration.edit', $inspiration->id) }}">
                                                    <i class="fa fa-pencil"></i> Edit
                                                </a>
                                            </li>
                                            <li><a href="#" data-type="div" data-buttons="delete" data-id="{{ $inspiration->id }}" data-url="{{ route('dashboard.inspiration.destroy', $inspiration->id) }}"><i class="fa fa-trash"></i> Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @empty
            <h4 class="text-center text-muted mt-5">No inspiration</h4>
        @endforelse
    </div>
    {{
        $inspirations->appends([
            'feature' => Request::get('feature'),
            'status' => Request::get('status'),
            'lock' => Request::get('lock'),
            'supplier' => Request::get('supplier'),
            'category' => Request::get('category'),
            'search' => Request::get('search')
        ])->links()
    }}
@endsection
@push('scripts')
    <script>
        $('select').change(function(){
            $(this).closest('form').submit()
        })
    </script>
@endpush


