@extends('dashboard.includes.layouts.main')

@section('page_title', $inspiration->name)

@section("content-hearder")
    <h1>
        Inspirations
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.home') }}">Home</a></li>
        <li><a href="{{ route('dashboard.inspiration.index') }}">Inspirations</a></li>
        <li class="active">{{ $inspiration->name }}</li>
    </ol>
@endsection

@section('content')
    <div class="inspiration-header" style="background-image:url('{{ $inspiration->image('cover')->path('covers', 'https://via.placeholder.com/1920x450') }}')">
    </div>
    <div style="background-color: #fff">
        <div class="row" >
            <div class="col-sm-3" style="margin-top:-150px">
                <div class="pl-3">
                    <img
                    src="{{ $inspiration->image('thumbnail')->path('thumbnails', 'https://via.placeholder.com/250x200') }}"
                    class="img img-responsive thumbnail mb-0" alt="">
                </div>
                <div class="mb-3 mt-2 pl-3">
                        <div class="row">
                            <div class="col-xs-4">
                                <a target="_blank" href="{{ route('site.inspiration.show', $inspiration->slug) }}" targe="_blank" class="btn btn-xs btn-block"><i
                                        class="fa fa-pencil"></i>
                                    Preview
                                </a>
                            </div>
                            <div class="col-xs-4">
                                <a href="{{ route('dashboard.inspiration.edit', $inspiration->id) }}" class="btn btn-xs btn-block"><i
                                        class="fa fa-pencil"></i>
                                    Edit
                                </a>
                            </div>
                             <div class="col-xs-4">
                                <a  data-type="page" data-redirect="{{ route('dashboard.inspiration.index') }}" data-buttons="delete" data-id="{{ $inspiration->id }}" data-url="{{ route('dashboard.inspiration.destroy', $inspiration->id) }}" class="btn btn-xs btn-block"><i class="fa fa-trash"></i> Delete</a>
                            </div>
                        </div>
                </div>
                <div class="box mb-3 ml-2">
                    <div class="box-header with-border">
                        <h3 class="box-title">Information</h3>
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <strong>
                            <a class="mb-0" href="{{ route('dashboard.company.show', optional($inspiration->company)->slug) }}">
                                {{ optional($inspiration->company)->name }}
                            </a>
                        </strong>
                        <p class="text-muted">
                            Company
                        </p>
                        <hr>
                      
                        <strong>
                            {{ $inspiration->categories()->whereType('cater')->first()->name }}
                        </strong>
                        <p class="text-muted">
                            Category
                        </p>
                        <hr>
                
                        <strong>
                           {{ $inspiration->categories()->whereType('setting')->first()->name }}
                        </strong>
                        <p class="text-muted">
                            Setting
                        </p>
                         <hr>

                        <strong>
                            {{ $inspiration->categories()->whereType('style')->first()->name }}
                        </strong>
                        <p class="text-muted">
                            Style
                        </p>
                
                    </div>
                </div>
                
                <div class="box mb-3 ml-2">
                    <div class="box-header with-border">
                        Description
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        {!! $inspiration->description !!}
                    </div>
                </div>

                @if($inspiration->images_by)
                    <div class="box mb-3 ml-2">
                        <div class="box-header with-border">
                            Images By
                            <div class="box-tools">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body">
                            {{ $inspiration->images_by }}
                        </div>
                    </div>
                @endif
                
                <div class="box mb-3 ml-2">
                    <div class="box-header with-border">
                        Colours
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        @foreach($inspiration->colors as $color)
                        <span class='color-span' style="background-color:{{ $color->description }}">&nbsp;&nbsp;</span>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                <div class="p-3">
                        <h1 class="title">{{ $inspiration->name }}</h1>
                </div>
                <div class="box no-border">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="{{ route('dashboard.inspiration.show', $inspiration->slug) }}?tab=supplier" aria-controls="home" role="tab"> SUPPLIER</a></li>
                            <li role="presentation"><a href="{{ route('dashboard.inspiration.show', $inspiration->slug) }}?tab=gallery" aria-controls="home" role="tab"> GALLERY</a></li>
                            <li role="presentation"><a href="{{ route('dashboard.inspiration.show', $inspiration->slug) }}?tab=videos" aria-controls="home" role="tab"> VIDEOS</a></li>
                            <li role="presentation"><a href="{{ route('dashboard.inspiration.show', $inspiration->slug) }}?tab=hearts" aria-controls="messages" role="tab"> SAVED</a></li>
                        </ul>
                    </div>
                    <div class="box-body">
                        <button class='btn btn-default rounded-0' id="addSupplierButton">
                            <i class="fa fa-plus"></i> Add Supplier
                        </button>
                        {{-- <div id="supplier-form" style="display:none"> --}}
                        <div id="supplier-form" >
                            <a href="#" class="pull-right text-danger" id="closeSupplierForm" ><i class="fa fa-ban"></i> Cancel</a>
                            <div class="clearfix"></div>
                            <form action="{{ route('dashboard.inspiration.addsupplier', $inspiration->id) }}" method="post">
                                @csrf
                                <inspiration-tag-suppliers venue="6" product="2"></inspiration-tag-suppliers>
                            </form>
                        </div>
                        <ul class="products-list product-list-in-box mt-2" id="supplier-list">
                            @foreach($suppliers as $supplier)
                                <li class="item" id="div-{{ $supplier['id'] }}">
                                    <div class="product-img">
                                        @if($supplier['thumbnail'])
                                            <img src="{{ $supplier['thumbnail'] }}" class="img img-responsive" alt="Product Image">
                                        @else
                                            {{-- <img src="{{ asset('/images/placeholders/company-logo-32x32.png') }}" class="img img-responsive"> --}}
                                        @endif
                                    </div>
                                    <div class="product-info">
                                        <small  class="pull-right">
                                            <a href="#" class="text-muted" data-buttons="delete" data-id="{{ $supplier['id'] }}" data-url="{{ route('dashboard.inspiration.deletesupplier', $supplier['id']) }}" data-type="div">
                                                <i class="fa fa-remove"></i> Remove
                                            </a>
                                        </small>
                                        <a href="javascript:void(0)" class="product-title">{{ $supplier['company'] }}
                                        </a>
                                        <span class="product-description">
                                            {{ $supplier['role'] }}
                                        </span>
                                        @if($supplier['request_by'] == Auth::user()->company_id)
                                            <small>
                                                - {{ strtoupper($supplier['status'])}}
                                            </small>    
                                        @else
                                            @if($supplier['status']  == 'pending')
                                                <button onclick="$('#confirm-form').submit()" class="btn btn-xs btn-warning"><i class="fa fa-check"></i> Confirm</button>
                                                <button onclick="$('#reject-form').submit()"  class="btn btn-xs btn-default"><i class="fa fa-ban"></i> Reject</button>
                                                
                                                <!-- for accept -->
                                                <form method="POST" action="{{ route('dashboard.inspiration.tags.changestatus', $supplier['id']) }}" id="confirm-form">
                                                    @csrf
                                                    <input type="hidden" name="status" value="confirmed">
                                                </form>  
                
                                                <!-- for reject -->
                                                <form method="POST" action="{{ route('dashboard.inspiration.tags.changestatus', $supplier['id']) }}" id="reject-form">
                                                    @csrf
                                                    <input type="hidden" name="status" value="rejected">
                                                </form>  
                                            @elseif($supplier['status'] == "rejected")
                                                <button  onclick="$('#pending-form').submit()" class="btn  btn-xs btn-default"><i class="fa fa-ban"></i> Cancel Reject</button>
                
                                                <!-- for accept -->
                                                <form method="POST" action="{{ route('dashboard.inspiration.tags.changestatus', $supplier['id']) }}" id="pending-form">
                                                    @csrf
                                                    <input type="hidden" name="status" value="pending">
                                                </form> 
                                            @else
                                                <button  onclick="$('#pending-form').submit()" class="btn  btn-xs btn-default"><i class="fa fa-ban"></i> Untag</button>
                
                                                <!-- for accept -->
                                                <form method="POST" action="{{ route('dashboard.inspiration.tags.changestatus', $supplier['id']) }}" id="pending-form">
                                                    @csrf
                                                    <input type="hidden" name="status" value="pending">
                                                </form>  
                                            @endif
                                        @endif
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function(){
            $('#addSupplierButton').click(function(){
                $('#supplier-form').show();
                $('#supplier-list').hide();
                $(this).hide();
            })

            $('#closeSupplierForm').click(function(e){
                e.preventDefault();
                $('#supplier-form').hide();
                $('#supplier-list').show();
                $('#addSupplierButton').show();
            })
        })
    </script>
@endpush
