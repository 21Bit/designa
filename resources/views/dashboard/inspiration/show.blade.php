@extends('dashboard.includes.layouts.main')

@section('page_title', $inspiration->name)

@section("content-hearder")
    <h1>
        Inspirations
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.home') }}">Home</a></li>
        <li><a href="{{ route('dashboard.inspiration.index') }}">Inspirations</a></li>
        <li class="active">{{ $inspiration->name }}</li>
    </ol>
@endsection

@section('content')
    <div class="inspiration-header" style="background-image:url('{{ $inspiration->image('cover')->path('covers', 'https://via.placeholder.com/1920x450') }}')">
    </div>
    <div style="background-color: #fff">
        <div class="row" >
            <div class="col-sm-3" style="margin-top:-150px">
                <div class="pl-3">
                    <img
                    src="{{ $inspiration->image('thumbnail')->path('thumbnails', 'https://via.placeholder.com/250x200') }}"
                    class="img img-responsive thumbnail mb-0" alt="">
                </div>

                <div class="mb-3 mt-2 pl-3">
                        <div class="row">
                            <div class="col-xs-4">
                                <a target="_blank" href="{{ route('site.inspiration.show', $inspiration->slug) }}"  targe="_blank" class="btn btn-xs btn-block"><i
                                        class="fa fa-pencil"></i>
                                    Preview
                                </a>
                            </div>
                            <div class="col-xs-4">
                                <a href="{{ route('dashboard.inspiration.edit', $inspiration->id) }}" class="btn btn-xs btn-block"><i
                                        class="fa fa-pencil"></i>
                                    Edit
                                </a>
                            </div>
                             <div class="col-xs-4">
                                <a  data-type="page" data-redirect="{{ route('dashboard.inspiration.index') }}" data-buttons="delete" data-id="{{ $inspiration->id }}" data-url="{{ route('dashboard.inspiration.destroy', $inspiration->id) }}" class="btn btn-xs btn-block"><i class="fa fa-trash"></i> Delete</a>
                            </div>
                        </div>
                </div>
                <div class="box mb-3 ml-2">
                    <div class="box-header with-border">
                        <h3 class="box-title">Information</h3>
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        <strong>
                            <a class="mb-0" href="{{ route('dashboard.company.show', optional($inspiration->company)->slug) }}">
                                {{ optional($inspiration->company)->name }}
                            </a>
                        </strong>
                        <p class="text-muted">
                            Company
                        </p>
                        <hr>

                        <strong>
                            {{ optional($inspiration->categories()->whereType('cater')->first())->name }}
                        </strong>
                        <p class="text-muted">
                            Category
                        </p>
                         <hr>

                        <strong>
                           {{ optional($inspiration->categories()->whereType('setting')->first())->name }}
                        </strong>
                        <p class="text-muted">
                            Setting
                        </p>
                         <hr>

                        <strong>
                            {{ optional($inspiration->categories()->whereType('style')->first())->name }}
                        </strong>
                        <p class="text-muted">
                            Style
                        </p>

                    </div>
                </div>

                <div class="box mb-3 ml-2">
                    <div class="box-header with-border">
                        Description
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        {!! $inspiration->description !!}
                    </div>
                </div>
                @if($inspiration->images_by)
                    <div class="box mb-3 ml-2">
                        <div class="box-header with-border">
                            Images By
                            <div class="box-tools">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="box-body">
                            {{ $inspiration->images_by }}
                        </div>
                    </div>
                @endif
                <div class="box mb-3 ml-2">
                    <div class="box-header with-border">
                        Colours
                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body">
                        @foreach($inspiration->colors as $color)
                        <span class='color-span' style="background-color:{{ $color->description }}">&nbsp;&nbsp;</span>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                <div class="p-3">
                        <h1 class="title">{{ $inspiration->name }}</h1>
                </div>
                <div class="box no-border">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation"><a href="{{ route('dashboard.inspiration.show', $inspiration->slug) }}?tab=supplier" aria-controls="home" role="tab"> SUPPLIER</a></li>
                            <li role="presentation" class="active"><a href="{{ route('dashboard.inspiration.show', $inspiration->slug) }}?tab=gallery" aria-controls="home" role="tab"> GALLERY</a></li>
                            <li role="presentation"><a href="{{ route('dashboard.inspiration.show', $inspiration->slug) }}?tab=videos" aria-controls="home" role="tab"> VIDEOS</a></li>
                            <li role="presentation"><a href="{{ route('dashboard.inspiration.show', $inspiration->slug) }}?tab=hearts" aria-controls="messages" role="tab"> SAVED</a></li>
                        </ul>
                    </div>
                    <div class="box-body">
                        @if(Session::has('gallery-upload-success'))
                            <div class="alert alert-success alert-dismissible mt-1 mb-3">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ Session::get('gallery-upload-success') }}
                            </div>
                        @endif
                        @include('dashboard.includes.alerts.error')

                        <form action="{{ route('dashboard.inspiration.uploadgallery', $inspiration->id) }}" method="POST" id="form-with-loading" enctype="multipart/form-data">
                            @csrf
                            <label for="gallery-input" class="btn btn-warning"><i class="fa fa-image"></i> Upload Image</label>
                            <div style="display:none">
                                <input name="file" accept="image/*" id="gallery-input" type="file"  />
                            </div>
                        </form>
                        <br>
                        <div class="inspiration-grid">
                            @foreach($gallery as $image)
                                <div class="inspiration-item" id="div-{{ $image->id }}">
                                    <div class="image-gallery" style="background-image:url('/images/gallery/{{ $image->path }}'); background-size:cover; background-position:center; padding-top:75%; margin-bottom:10px;">
                                        <a href="/images/gallery/{{ $image->path }}" data-lightbox="PORTFOLIO"></a>
                                        <a href='#'  data-type="div" data-buttons="delete" data-id="{{ $image->id }}" data-url="{{ route('dashboard.gallery.destroy', $image->id) }}"   class="btn btn-md trash-btn"><i class="fa fa-trash"></i></a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function(){
            $('#gallery-input').change(function(){
                $(this).closest('form').submit();
            })
        })
    </script>
@endpush
