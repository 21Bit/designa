@extends('dashboard.includes.layouts.main')

@section("content-header")
     <h1>
        Good Morning {{ auth()->user()->name }}!
      </h1>
      <div style="border-bottom:1px solid #ccc; padding-bottom:10px;">
      Dashboard
      </div>
@endsection

@section("content")
   {{-- <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Monthly Recap Report</h3>
      </div>
      <div class="box-body"> --}}
        <dashboard-statistics></dashboard-statistics>
      {{-- </div>
  </div> --}}
@endsection