@extends('dashboard.includes.layouts.main')

@section('page_title', 'Edit Color')

@section("content-hearder")
        <h1>
            Color
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">System / Color</li>
        </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-4">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit color</h3>
                </div>
                <div class="box-body">
                    <form action="{{ route('dashboard.color.update', $color->id) }}" method="post">
                        @csrf
                        @method("PUT")
                        <p>
                            <label for="">Name *</label>
                            <input type="text" name="name" value="{{ $color->name }}" required class="form-control input-sm">
                        </p>
                        <p>
                            <label for="">Color *</label> <br />
                            <input type="color" value="{{ $color->rgb_code }}" name="rgb_code">
                        </p>
                        <button type="submit" class="btn btn-warning btn-sm mt-3"><i class="fa fa-save"></i> Save Changes</button>
                        <a href="{{ route('dashboard.color.index') }}" class="btn btn-default btn-sm  mt-3"><i class="fa fa-ban"></i> Cancel</a>
                    </form>
                </div>
            </div>
        </div>
       
    </div>
   
@endsection