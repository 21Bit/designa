@extends('dashboard.includes.layouts.main')

@section('page_title', 'Color')

@section("content-hearder")
    <h1>
        Colors
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">System / Color</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-4">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Add color</h3>
                </div>
                <div class="box-body">
                    <form action="{{ route('dashboard.color.store') }}" method="post">
                        @csrf
                        <p>
                            <label for="">Name *</label>
                            <input type="text" name="name" required class="form-control input-sm">
                        </p>
                        <p>
                            <label for="">Color *</label> <br />
                            <input type="color" name="rgb_code">
                        </p>
                        <button type="submit" class="btn btn-warning btn-sm mt-3"><i class="fa fa-save"></i> Save</button>
                    </form>
                </div>
            </div>
        </div>
           
        <div class="col-sm-8">
            <div class="box">
                <div class="box-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($colors as $color)
                                <tr>
                                    <td>
                                        <span class='color-span mr-2' style="background-color:{{ $color->rgb_code }}">&nbsp;&nbsp;</span>
                                        {{ $color->name }}</td>
                                    <td class="text-right">
                                        <a href="{{ route('dashboard.color.edit', $color->id) }}" class="mr-3"><i class="fa fa-pencil"></i></a>
                                        <a href="#" data-buttons="delete" data-type="table" data-url="{{ route('dashboard.color.destroy', $color->id) }}"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            @empty

                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
   
@endsection