@extends('dashboard.includes.layouts.main')

@section('page_title', 'Edit Color')

@section("content-hearder")
    <h1>
        Pricing Components
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Pricing Components</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-4">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit color</h3>
                </div>
                <div class="box-body">
                    <form action="{{ route('dashboard.pricing-component.update', $pricing->id) }}" method="post">
                        @csrf
                        @method("PUT")
                        <p>
                            <label for="">Name *</label>
                            <input type="text" name="name" value="{{ $pricing->name }}" required class="form-control input-sm">
                        </p>
                        <div class="mb-4">
                            <input type="checkbox" name="active" @if($pricing->active) checked @endif id="isactive">
                            <label for="isactive mb-0"> Active</label>
                            <div class="ml-4 mt-0"><small>
                                    This will indicate for this pricing component to show in the actual result.
                                </small>
                            </div>
                        </div>
                        <div>
                            <input type="checkbox" name="multipliable" @if($pricing->multipliable) checked @endif id="mutiplier">
                            <label for="mutiplier"> Multipliable</label>
                            <div class="ml-4 mt-0">
                                <small>
                                    This option is use if this pricing is multiplied to the quoted decor's quantity.    
                                </small>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-warning btn-sm mt-3"><i class="fa fa-save"></i> Save Changes</button>
                        <a href="{{ route('dashboard.pricing-component.index') }}" class="btn btn-default btn-sm  mt-3"><i class="fa fa-ban"></i> Cancel</a>
                    </form>
                </div>
            </div>
        </div>
       
    </div>
   
@endsection