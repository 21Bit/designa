@extends('dashboard.includes.layouts.main')

@section('page_title', 'Color')

@section("content-hearder")
    <h1>
        Pricing Components
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Pricing Components</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-4">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Add color</h3>
                </div>
                <div class="box-body">
                    <form action="{{ route('dashboard.pricing-component.store') }}" method="post">
                        @csrf
                        <p>
                            <label for="">Name *</label>
                            <input type="text" name="name" required class="form-control input-sm">
                        </p>
                        <div class="mb-4">
                            <input type="checkbox" name="active" id="isactive">
                            <label for="isactive mb-0"> Active</label>
                            <div class="ml-4 mt-0"><small>
                                    This will indicate for this pricing component to show in the actual result.
                                </small>
                            </div>
                        </div>
                        <div>
                            <input type="checkbox" name="multipliable" id="mutiplier">
                            <label for="mutiplier"> Multipliable</label>
                            <div class="ml-4 mt-0">
                                <small>
                                    This option is use if this pricing is multiplied to the quoted decor's quantity.    
                                </small>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-warning btn-sm mt-3"><i class="fa fa-save"></i> Save</button>
                    </form>
                </div>
            </div>
        </div>
           
        <div class="col-sm-8">
            <div class="box">
                <div class="box-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Active</th>
                                <th>Multiplier</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($pricings as $pricing)
                                <tr>
                                    <td>
                                        {{ $pricing->name }}
                                    </td>
                                    <td>
                                        {!! $pricing->active ?  "<i class='fa fa-check-circle text-success' ></i>" : '' !!}
                                    </td>
                                    <td>
                                        {!! $pricing->multipliable ?  "<i class='fa fa-check-circle text-success' ></i>" : '' !!}
                                    </td>
                                    <td class="text-right">
                                        <a href="{{ route('dashboard.pricing-component.edit', $pricing->id) }}" class="mr-3"><i class="fa fa-pencil"></i></a>
                                        <a href="#" data-buttons="delete" data-type="table" data-url="{{ route('dashboard.pricing-component.destroy', $pricing->id) }}"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            @empty

                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
   
@endsection