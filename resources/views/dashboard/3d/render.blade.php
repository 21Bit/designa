<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $pagetitle ?? " Template" }}</title>

</head>
<body style="margin:0; padding:0;">
  <div id="unityContainer" style="width: 100%; height: 100%;position: fixed; margin: auto"></div>
  <script src="/Build/UnityLoader.js"></script>
  <script>
    function UnityProgress(unityInstance, progress) {
        if (!unityInstance.Module)
          return;
          if (!unityInstance.logo) {
            unityInstance.logo = document.createElement("div");
            unityInstance.logo.className = "logo " + unityInstance.Module.splashScreenStyle;
            unityInstance.container.appendChild(unityInstance.logo);
          }
          if (!unityInstance.progress) {
            unityInstance.progress = document.createElement("div");
            unityInstance.progress.className = "progress " + unityInstance.Module.splashScreenStyle;
            unityInstance.progress.empty = document.createElement("div");
            unityInstance.progress.empty.className = "empty";
            unityInstance.progress.appendChild(unityInstance.progress.empty);
            unityInstance.progress.full = document.createElement("div");
            unityInstance.progress.full.className = "full";
            unityInstance.progress.appendChild(unityInstance.progress.full);
            unityInstance.container.appendChild(unityInstance.progress);
          }
          unityInstance.progress.full.style.width = (100 * progress) + "%";
          unityInstance.progress.empty.style.width = (100 * (1 - progress)) + "%";
          if (progress == 1)
            unityInstance.logo.style.display = unityInstance.progress.style.display = "none";
      }

     var settings =
      {
          onProgress: UnityProgress,
          Module:
          {
              preRun: [ function() { console.log("About to run....") ; } ],
              postRun: [ function() {
                unityInstance.SendMessage('Data', 'dataFromDesigna', '{{ $params }}')
              }],
          }
      };
    var unityInstance = UnityLoader.instantiate("unityContainer", "/Build/Designa_WebGL.json", settings);

  </script>
</body>
</html>
