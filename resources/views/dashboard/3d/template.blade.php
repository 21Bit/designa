@extends('dashboard.includes.layouts.main')

@section('page_title', 'Decoration Templates')

@section("content-hearder")
        <h1>
            Decoration Templates
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">3D - Decoration Templates</li>
        </ol>
@endsection

@section('content')
        @section('content-header')
            @if(Request::get('create'))
                <a href="{{ route('dashboard.floorplan.index') }}"  class="btn btn-warning pull-right"><i class="fa fa-ban"></i> Cancel</a>
            @endif
            <h1>3D Models - Templates
            </h1>

        @endsection
        <div class="box">
            <div class="row">
                    <div class="col-sm-9 m-0">
                        <a href="{{ route('dashboard.template.index', [ 'create' => Request::get('create') ])  }}" class="ml-2 btn border-bottom btn-lg rounded-0 text-muted"
                            @if(!Request::get('tab'))
                                 style="border-bottom:3px solid #f39c12; color:#f39c12;"
                            @endif
                        >Public Templates</a>
                        @if(Auth::user()->is_superadmin)
                             <a href="{{ route('dashboard.template.index', ['tab' => 'all-templates', 'create' => Request::get('create') ])  }}" class="ml-2 btn border-bottom btn-lg rounded-0 text-muted"
                                @if(Request::get('tab') == "all-templates")
                                     style="border-bottom:3px solid #f39c12; color:#f39c12;"
                                @endif
                            >All Templates</a>
                        @endif
                        <a href="{{ route('dashboard.template.index', ['tab' => 'my-templates', 'create' => Request::get('create')])  }}" class="btn border-bottom-1 btn-lg text-muted"
                            @if(Request::get('tab') == "my-templates")
                                 style="border-bottom:3px solid #f39c12; color:#f39c12;"
                            @endif
                        >My Templates</a>
                    </div>
                    <div class="col-sm-3">
                        <form action="">
                                <div class="input-group m-3">
                                    @if(Request::get('create'))
                                        <input type="hidden" name="create" value="{{ Request::get('create') }}">
                                    @endif
                                    <input type="search" name="q" class="form-control" value="{{ Request::get('q') }}" placeholder=" search for template..">
                                    <div class="input-group-btn">
                                        <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                        </form>
                    </div>
            </div>
        </div>
        @if(Request::get('create') == "floorplan")
            <div class="callout callout-success">
                <h4 class="mb-0">Please select template.</h4>
                <p>This will serve as floor plan's template.</p>
            </div>
        @endif
        <div class="row">
            @forelse($models as $model)
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6" id="div-{{ $model->id }}">
                    <div class="box card-box product-box">
                        <div class="box-header" style="padding-top:75%;
                                    background-image:url('{{ $model->modellable->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png') }}'); background-size:cover; background-position:center;">
                        </div>
                        <div class="box-body pt-0 pb-3">
                            <div class="flex">
                                <div class="flex1">
                                    <h4 class="text-orange mb-1">
                                    {{ $model->model_name }}
                                    </h4>
                                    <div>
                                        Guest: {{ $model->number_of_guest }}
                                    </div>
                                    <div>
                                        Owner: {{ $model->companyOwner()  }}
                                    </div>
                                    <div>
                                        Last Update: {{ $model->updated_at->format('M d, Y  h:iA')  }}
                                    </div>
                                </div>
                                @if($model->isOwned() || Auth::user()->is_superadmin)
                                    <div class="p-3">
                                        <a class="h4 text-muted" href="{{ route('dashboard.template.edit', $model->id) }}"><i class="fa fa-pencil"></i></a>
                                        <a data-id="{{ $model->id }}" data-buttons="delete" data-url="{{ route('dashboard.template.destroy', $model->id) }}" data-type="div" class="p-2 mt-3 h4 text-muted"><i class="fa fa-trash"></i></a>
                                    </div>
                                @endif
                            </div>
                            <div class="mt-3 mb-0">
                                <product-quotation-3d-component savingurl="{{ route('dashboard.template.generate-quote') }}" id="{{ $model->id }}" name="{{ $model->model_name  }}"></product-quotation-3d-component>
                                <a href="{{ route('dashboard.template.generate-floorplan', $model->id) }}" class="btn btn-default btn-block mt-2  circle-round-btn"><i class="fa fa-cube"></i> Generate Floor Plan</a>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
                <div class="text-center">
                    <h2 class="text-muted ">No Template Found!</h2>
                    @if(!Request::get('create'))
                            <a href="{{ route('dashboard.baselayer.index', ['banner' => 'creating-template']) }}"  type="button" class="btn btn-default btn-lg mt-2 addFormModalButton" >Create template</a>
                    @endif
                </div>
            @endforelse
        </div>
        {{ $models->appends(['tab' => Request::get('tab')])->links() }}


@endsection

@push('scripts')
    <script>
        $(document).ready(function(){

            $('select[name=access]').change(function(){
                $(this).closest('form').submit();
            })

            $('#event-space-select').submit(function(e){
                e.preventDefault();
                var eventSpace = $('select[name=event_space_id]').val()
                window.open('/dashboard/3d/baselayer/' +  eventSpace, '_self')
            })
        })
    </script>
@endpush
