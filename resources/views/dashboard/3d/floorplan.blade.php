@extends('dashboard.includes.layouts.main')

@section('page_title', 'Floor Plans')

@section("content-hearder")
        <h1>
            3D Models
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">3D</li>
        </ol>
@endsection

@section('content')
        @section('content-header')
            <a href="{{ route('dashboard.template.index', ['create' => 'floorplan']) }}"  class="btn btn-warning pull-right" ><i class="fa fa-plus"></i> New Floor Plan</a>
            <h1>3D Models - Floor Plan</h1>
        @endsection

        <div class="box">
            <div class="row">
                <div class="col-sm-9 m-0">
                    <a href="{{ route('dashboard.floorplan.index')  }}" class="ml-2 btn border-bottom btn-lg rounded-0 text-muted"
                        @if(!Request::get('tab'))
                             style="border-bottom:3px solid #f39c12; color:#f39c12;"
                        @endif
                    >Public Floor Plans</a>
                    @if(Auth::user()->is_superadmin)
                         <a href="{{ route('dashboard.floorplan.index', ['tab' => 'all-floorplans'])  }}" class="ml-2 btn border-bottom btn-lg rounded-0 text-muted"
                            @if(Request::get('tab') == "all-floorplans")
                                 style="border-bottom:3px solid #f39c12; color:#f39c12;"
                            @endif
                        >All Floor Plans</a>
                    @endif
                    <a href="{{ route('dashboard.floorplan.index', ['tab' => 'my-floorplans'])  }}" class="btn border-bottom-1 btn-lg text-muted"
                        @if(Request::get('tab') == "my-floorplans")
                             style="border-bottom:3px solid #f39c12; color:#f39c12;"
                        @endif
                    >My Floor Plans</a>
                </div>
                <div class="col-sm-3">
                        <form action="">
                                <div class="input-group m-3">
                                    <input type="search" name="q" class="form-control" value="{{ Request::get('q') }}" placeholder=" search for floor plans..">
                                    <div class="input-group-btn">
                                        <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                        </form>
                    </div>
            </div>
        </div>

        @if(Request::get('banner') == "creating-template")
            <div class="callout callout-success">
                <h4 class="mb-0">Please select your Floor Plan.</h4>
                <p>This will serve as your template's Floor Plan.</p>
          </div>
        @endif
        <div class="row">
            @forelse($models as $model)
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6" id="div-{{ $model->id }}">
                    <div class="box card-box product-box">
                        <div class="box-header" style="padding-top:75%;
                                    background-image:url('{{ $model->modellable->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png') }}'); background-size:cover; background-position:center;">
                        </div>
                        <div class="box-body pt-0 pb-0">
                            {{-- <small class="text-muted">{{ $model->modellable->venue->categoryList('cater') }}</small> --}}
                            <h4 class="text-orange">
                               {{ $model->model_name }}
                            </h4>
                            <div>
                                Guest: {{ $model->number_of_guest }}
                            </div>
                            <div>
                                Owner: {{ $model->companyOwner()  }}
                            </div>
                            <div>
                                Last Update: {{ $model->updated_at->format('M d, Y  h:iA')  }}
                            </div>
                            <div class=" pb-3">
                                <div style="display:flex">
                                    @if(count($model->product_list))
                                        <div style="flex:1" class="p-2">
                                            <product-quotation-3d-component savingurl="{{ route('dashboard.template.generate-quote') }}" id="{{ $model->id }}" name="{{ $model->model_name  }}"></product-quotation-3d-component>
                                                {{--<button data-model_id="{{ $model->id }}" data-event_name="{{$model->model_name}}" data-number_of_guests="{{ $model->number_of_guest }}" type="button" class="btn btn-block btn-warning request-quotation-btn">Request Quotation</button>--}}
                                        </div>
                                    @endif
                                    <div style="flex:1" class="p-2">
                                        <a href="{{ route('dashboard.floorplan.edit', $model->id) }}" class="btn btn-block  btn-default circle-round-btn"><i class="fa fa-pencil"></i> Edit</a>
                                    </div>
                                    <div style="flex:1" class="p-2">
                                        <button data-id="{{ $model->id }}" data-buttons="delete" data-url="{{ route('dashboard.floorplan.destroy', $model->id) }}" data-type="div" class="btn btn-block btn-default circle-round-btn"><i class="fa fa-trash"></i> Delete</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
                <div class="text-center">
                    <h2 class="text-muted ">No Floor Plan Found!</h2>
                    <a href="{{ route('dashboard.template.index', ['create' => 'floorplan']) }}"  class="btn btn-default btn-lg mt-2 " >Create your first Floor Plan</a>
                </div>
            @endforelse
        </div>
@endsection
