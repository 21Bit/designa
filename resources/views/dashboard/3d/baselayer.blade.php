@extends('dashboard.includes.layouts.main')

@section('page_title', 'Base Layers')

@section("content-hearder")
        <h1>
            Base Layers
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">3D - Base Layers</li>
        </ol>
@endsection

@section('content')
        @section('content-header')
            @if(!Request::get('create'))
                <button  type="button" class="btn btn-warning pull-right addFormModalButton" ><i class="fa fa-plus"></i> New Base Layer</button>
            @else
                <a href="{{ route('dashboard.template.index') }}"  class="btn btn-warning pull-right"><i class="fa fa-ban"></i> Cancel</a>
            @endif
            <h1>3D Models - Base Layers</h1>
        @endsection

   <div class="box">
            <div class="row">
                    <div class="col-sm-9 m-0">
                        <a href="{{ route('dashboard.baselayer.index', [ 'create' => Request::get('create') ])  }}" class="ml-2 btn border-bottom btn-lg rounded-0 text-muted"
                            @if(!Request::get('tab'))
                                 style="border-bottom:3px solid #f39c12; color:#f39c12;"
                            @endif
                        >Public Base Layers</a>
                        <a href="{{ route('dashboard.baselayer.index', ['tab' => 'my-baselayers', 'create' => Request::get('create')])  }}" class="btn border-bottom-1 btn-lg text-muted"
                            @if(Request::get('tab') == "my-baselayers")
                                 style="border-bottom:3px solid #f39c12; color:#f39c12;"
                            @endif
                        >My Base Layers</a>
                    </div>
                    <div class="col-sm-3">
                        <form action="">
                                <div class="input-group m-3">
                                    @if(Request::get('create'))
                                        <input type="hidden" name="create" value="{{ Request::get('create') }}">
                                    @endif
                                    <input type="search" name="q" class="form-control" value="{{ Request::get('q') }}" placeholder=" search for template..">
                                    <div class="input-group-btn">
                                        <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                        </form>
                    </div>
            </div>
        </div>

        @if(Request::get('banner') == "creating-template")
            <div class="callout callout-success">
                <h4 class="mb-0">Please select your Base Layer.</h4>
                <p>This will serve as your template's base layer.</p>
          </div>
        @endif
        <div class="row">
            @forelse($models as $model)
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6" id="div-{{ $model->id }}">
                    <div class="box card-box product-box">
                        <div class="box-header" style="padding-top:75%;
                                    background-image:url('{{ $model->modellable->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png') }}'); background-size:cover; background-position:center;">
                        </div>
                        <div class="box-body pt-0 pb-0" style="min-height: 200px">
                            <small class="text-muted">{{ optional($model->modellable->venue)->categoryList('cater') }}</small>
                            @if($model->is_public)
                                <span class="float-right text-muted"><i class="fa fa-eye"></i></span>
                            @else
                                <span class="float-right text-muted"><i class="fa fa-eye-slash"></i></span>
                            @endif
                            <div class="flex">
                                <div class="flex1">
                                    <h4 class="text-orange">
                                        {{ $model->model_name }}
                                    </h4>
                                    @if($model->modellable->venue)
                                    <div class="text-muted">
                                        By: <a href="{{ route('site.venue.show', $model->modellable->venue->company->slug) }}">{{ $model->modellable->venue->company->name }}</a>
                                    </div>
                                    @endif
                                    <div class="text-muted">
                                        Guest: {{ $model->number_of_guest }}
                                    </div>
                                    {{-- <div class="text-muted">
                                        Created: {{ $model->created_at->format('M d, Y') }}
                                    </div> --}}
                                </div>
                                @if($model->isOwned() || Auth::user()->is_superadmin)
                                    <div class="p-3">
                                        <a class="h4 text-muted" href="{{ route('dashboard.baselayer.edit', $model->modellable->id) }}"><i class="fa fa-pencil"></i></a>
                                        <a data-id="{{ $model->id }}" data-buttons="delete" data-url="{{ route('dashboard.baselayer.destroy', $model->id) }}" data-type="div" class="p-2 mt-3 h4 text-muted"><i class="fa fa-trash"></i></a>
                                    </div>
                                @endif
                            </div>
                            <div class="flex pb-3">
                                <div class="pt-4 flex1">
                                    <label class="mr-3">
                                        Max Standing: {{ number_format($model->modellable->max_capacity_standing) }}
                                    </label>
                                    <label>
                                        Max Seated: {{ number_format($model->modellable->max_capacity_seated) }}
                                    </label>
                                </div>
                                <div class="p-2 text-right">
                                    <a href="{{ route('dashboard.baselayer.generate-template', $model->modellable->id) }}" class="btn btn-warning mt-3 pt-2 pb-2 pl-3 pr-3 generate-model circle-round-btn">Decorate</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            @empty
                <div class="text-center">
                    <h2 class="text-muted ">No Base Layer Found!</h2>
                    <button  type="button" class="btn btn-default btn-lg mt-2 addFormModalButton" >Create your first Base layer</button>
                </div>
            @endforelse
        </div>

        {{ $models->appends(['tab' => Request::get('tab')])->links() }}
        <!-- Modal -->
    <div class="modal fade in" id="addFormModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="event-space-select">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Select Event Space</h4>
                    </div>
                    <div class="modal-body">
                        <p>
                            <select name="event_space_id" data-type="select2" required style="width: 100%"  class="form-control d-block">
                                @foreach($venues as $venue)
                                    <optgroup label="{{ $venue->name }}">
                                        @foreach($venue->functionspaces as $fs)
                                            <option @if(!$fs->asset_bundle_web) disabled @endif value="{{ $fs->id }}">{{ $fs->name }}</option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-warning">Create Model</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="/Build/UnityLoader.js"></script>
    <script>
        var unityInstance = UnityLoader.instantiate("unityContainer", "/Build/WEB_3dModule.json");
        $(document).ready(function(){
            $('.addFormModalButton').click(function(){
                $('#addFormModal').modal('show')
            })

            $('.generate-model').click(function(){
                var params = $(this).data('params')
                // $('#unityContainer').show()
                unityInstance.SendMessage('Data', 'dataFromDesigna', params)
            })

            $('select[name=owned]').change(function(){
                $(this).closest('form').submit();
            })

            $('#event-space-select').submit(function(e){
                e.preventDefault();
                var eventSpace = $('select[name=event_space_id]').val()
                window.open('/dashboard/3d/baselayer/' +  eventSpace, '_self')
            })
        })
    </script>
@endpush
