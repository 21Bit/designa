@extends('dashboard.includes.layouts.main')

@section('page_title', 'Roles')

@section("content-hearder")
        <h1>
            Roles
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">System / Roles</li>
        </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-4">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit role</h3>
                </div>
                <div class="box-body">
                    <form action="{{ route('dashboard.role.update', $role->id) }}" method="post">
                        @csrf
                        @method('PUT')
                        <p>
                            <label for="">Name *</label>
                            <input type="text" name="name" value="{{ $role->display_name }}" required class="form-control input-sm">
                        </p>
                        <p>
                            <label for="">Description </label>
                            <textarea name="description" id="" cols="30" rows="10" class="form-control">{{ $role->description }}</textarea>
                        </p>
                        <p>
                            <label for="">Credit By <small>(optional)</small></label>
                            <input type='text' name="credit_by" value="{{ $role->credit_by }}" class="form-control">
                        </p>
                        <p>
                            <label for="">Credit Location <small>(optional)</small></label>
                            <input type='text' name="credit_location" value="{{ $role->credit_location }}" class="form-control">
                        </p>
                        <button type="submit" class="btn btn-warning btn-sm"><i class="fa fa-save"></i> Save Changes</button>
                        <a href="{{ route('dashboard.role.index') }}" class="btn btn-default btn-sm"><i class="fa fa-ban"></i> Cancel</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
   
@endsection