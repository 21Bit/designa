@extends('dashboard.includes.layouts.main')

@section('page_title', 'Roles')

@section("content-hearder")
        <h1>
            Roles
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">System / Roles</li>
        </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-4">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Add role</h3>
                </div>
                <div class="box-body">
                    <form action="{{ route('dashboard.role.store') }}" method="post">
                        @csrf
                        <p>
                            <label for="">Name *</label>
                            <input type="text" name="name" required class="form-control input-sm">
                        </p>
                        <p>
                            <label for="">Description </label>
                            <textarea name="description" id="" cols="30" rows="10" class="form-control"></textarea>
                        </p>
                        <p>
                            <label for="">Credit By <small>(optional)</small></label>
                            <input type="text" name="credit_by"  class="form-control">
                        </p>
                        <p>
                            <label for="">Credit Location <small>(optional)</small></label>
                            <input type="text" name="credit_location"  class="form-control">
                        </p>
                        <button type="submit" class="btn btn-warning btn-sm mt-3"><i class="fa fa-save"></i> Save</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="box">
                <div class="box-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Permissions/Modules</th>
                                <th>Description</th>
                                <th>Credit By</th>
                                <th>Credit Location</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($roles as $role)
                                <tr>
                                    <td>{{ $role->display_name }}</td>
                                    <td class="pl-5">{{ $role->permissions()->count() }}</td>
                                    <td>{{ $role->description }}</td>
                                    <td>{{ $role->credit_by }}</td>
                                    <td>{{ $role->credit_location }}</td>
                                    <td class="text-right">
                                        <div class="dropdown">
                                            <a class="dropdown-toggle tex-default" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                <i class="fa fa-gear"></i>
                                            </a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li>
                                                    <a href="{{ route('dashboard.role.show', $role->id) }}"><i class="fa fa-list"></i> Manage</a>
                                                </li>
                                                <li>
                                                    <a href="{{ route('dashboard.role.edit', $role->id) }}"><i class="fa fa-pencil"></i> Edit</a>
                                                </li>
                                                <li>
                                                    <a href="#" data-buttons="delete" data-type="table" data-url="{{ route('dashboard.role.destroy', $role->id) }}"><i class="fa fa-trash"></i> Delete</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @empty

                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
   
@endsection