@extends('dashboard.includes.layouts.main')

@section('page_title', 'Roles')

@section("content-hearder")
        <h1>
            Roles
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">System / Roles</li>
        </ol>
@endsection

@section('content')
<form action="{{route('dashboard.role.assignpermission', $role->id)}}" method="POST">
    @csrf
    <div class="text-right mb-2">
        <button class="btn btn-warning" type="submit"><i class="fa fa-save"></i> Save</button>
        <a href="{{ route('dashboard.role.index') }}" class="btn btn-default"><i class="fa fa-ban"></i> Cancel</a>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="orange mt-0 mb-0">{{ $role->display_name }}</h3>
                </div>
                <div class="box-body">
                    <p class="mb-4">
                        {{ $role->description }}
                    </p>
                    <label for="">Company Has these Role</label>
                    <ul>
                        @foreach($role->companies as $company)
                            <li><a target="_blank" href="{{ route('dashboard.company.show', $company->slug) }}">{{ $company->name }}</a></li>
                        @endforeach
                    </ul>
      
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="box">
                <div class="box-body">
                    Permissions
                    <table class="table">
                        @forelse($permissions as $permission)
                            <tr>
                                <td width="20">
                                    <input data-type="icheck" name='permissions[]' id="checkbox-{{ $permission->id }}" value="{{$permission->id}}" @if(in_array($permission->id, $role->permissions()->pluck('id')->toArray())) checked  @endif type="checkbox" class="icheck">
                                </td>
                                <td>
                                    <label for="checkbox-{{ $permission->id }}" class="d-block">
                                        {{ $permission->display_name }}
                                    </label>
                                    <p>
                                        {{ $permission->description }}
                                    </p>
                                </td>
                            </tr>
                        @empty
                        
                        @endforelse
                    </table>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection