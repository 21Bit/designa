@extends('dashboard.includes.layouts.main')

@section('page_title', 'Subscription and Billing')

@section('content')
<section class="content">

    @include('dashboard.includes.alerts.success')
    @include('dashboard.includes.alerts.error')

    <div class="box no-border">

        {{-- include tab --}}
        @include('dashboard.profile.includes.tab')
        <div class="box-body">
            @if(count($paymentMethods))
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        @if ($user->hasIncompletePayment())
                            <a href="{{ route('cashier.payment', $user->subscription()->latestPayment()->id) }}">
                                Please confirm your payment.
                            </a>
                        @endif

                        <div class="panel panel-default">
                            <div class="panel-body pt-2 pb-3">
                                <span style="font-size:4em">{{ $user->balance() }}</span> / Balance
                            </div>
                        </div>

                        <h3>
                            Charges
                        </h3>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Charges</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Sub-total</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($user->subscriptions as $index => $subscription)
                                    <tr>
                                        <td>{{ getSubscriptionPlanName($subscription->stripe_price) }}</td>
                                        <td>${{ number_format(getSubscriptionPlanPrice($subscription->stripe_price) ,2) }} /{{ getSubscriptionPlanPriceBillingPeriod($subscription->stripe_price) }}</td>
                                        <td>{{ $subscription->quantity }}</td>
                                        <td>
                                            @if($subscription->onTrial('Designa Plan'))
                                                <del style="">
                                            @endif
                                            ${{ number_format(getSubscriptionPlanPrice($subscription->stripe_price) * $subscription->quantity) }}
                                            @if($subscription->onTrial('Designa Plan'))
                                                </del>
                                            @endif

                                            @if($subscription->onTrial('Designa Plan'))
                                                <span class="text-danger">Trial untill {{ $subscription->trial_ends_at->format('M d, Y')  }}</span>
                                            @endif
                                        </td>
                                        <td class="text-right">
                                            @if(!in_array(getSubscriptionPlanName($subscription->stripe_price),["Event Spaces", 'Venue Charges']) && getSubscriptionPlanName($subscription->stripe_price) != 'Pro')
                                                @if(config('subscription.september_promo'))
                                                    <a href="{{ route('dashboard.subscription.plans', ['only' => encrypt('Pro')]) }}" class="btn btn-danger"> Get your Pro trial</a>
                                                @else
                                                    <a href="{{ route('dashboard.subscription.plans') }}"> Change Plan</a>
                                                @endif
                                            @elseif(!in_array(getSubscriptionPlanName($subscription->stripe_price),["Event Spaces", 'Venue Charges']) && Auth::user()->is_superadmin)
                                                <a href="{{ route('dashboard.subscription.plans') }}"> Change Plan</a> &nbsp; | &nbsp;
                                            @endif

                                            @if(!in_array(getSubscriptionPlanName($subscription->stripe_price),["Beginner", "Event Spaces", 'Venue Charges']) )
                                                @if(!Auth::user()->is_superadmin)
                                                    <a type="button" href="#" data-toggle="modal" data-target="#amendmentsModal">
                                                        Request Amendments &nbsp;
                                                    </a>
                                                @endif
                                            @endif

                                            @if(!$subscription->onGracePeriod())
                                                @if(Auth::user()->is_superadmin)
                                                    <a onclick="if(!confirm('Are you sure to cancel?')){ return false; }" href="{{ route('dashboard.subscription.cancel', $subscription->name) }}" class="text-danger">
                                                        Cancel <small>(super admin)</small>
                                                    </a>
                                                @endif
                                            @endif

                                            @if($subscription->onGracePeriod())
                                                &nbsp; | &nbsp;
                                                Expires at : {{ date('M d ,Y',strtotime($subscription->ends_at)) }}
                                            @endif

                                            @if($subscription->onGracePeriod())
                                                &nbsp; | &nbsp;
                                                <a href="{{ route('dashboard.subscription.resume', $subscription->name) }}">resume</a>
                                            @endif

                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="5" class="text-center p-4">
                                            Beginner Subscription
                                            <br>
                                            <br>
                                            @if(config('subscription.september_promo'))
                                                <a href="{{ route('dashboard.subscription.plans', ['only' => encrypt('Pro')]) }}" class='btn btn-danger'> Start Trial to Pro</a>
                                            @else
                                                <a href="{{ route('dashboard.subscription.plans') }}" class='btn btn-warning'> Ugrade</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>

                        <hr>
                        <h3>
                            Payment Methods @if(count($paymentMethods)) <i class="fa fa-check-circle-o text-success"></i>@endif
                            <button role="button" data-toggle="collapse" href="#paymentMethodDiv" aria-expanded="false" aria-controls="paymentMethodDiv" class="pull-right btn btn-sm btn-warning"><i class="fa fa-plus"></i> Add Payment Method</button>
                        </h3>
                        <div class="collapse" id="paymentMethodDiv">
                            <div class="panel panel-default" id="add-payment-form-container">
                                <div class="panel-heading">
                                    Add Payment Method
                                </div>
                                <div class="panel-body">
                                    <form action="{{ route('dashboard.payment-method.store') }}" method="POST" id="add-payment-method-form">
                                        @csrf
                                        <p>
                                            <label for="">Card Holder Name</label>
                                            <input class='form-control' placeholder="Card Holder Name" value="{{ Auth::user()->name }}" required id="card-holder-name" type="text">
                                        </p>

                                        <p>
                                            <label for="">Card Details</label>
                                            <!-- Stripe Elements Placeholder -->
                                            <div id="card-element" class='form-control'></div>
                                        </p>
                                        <p>
                                            <div id="card-errors"></div>
                                        </p>

                                        <button id="card-button" type="submit" class="btn btn-warning" data-secret="{{ $intent->client_secret }}">
                                            Create Payment Method
                                        </button>
                                        <button role="button" type="button" data-toggle="collapse" href="#paymentMethodDiv" aria-expanded="false" aria-controls="paymentMethodDiv" class="btn btn-default"> Close</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped table-sm mb-0">
                            <tbody>
                                @foreach($paymentMethods as $paymentMethod)
                                    <tr>
                                        <td width='250px'>
                                            <i class="fa fa-cc-{{ $paymentMethod->card->brand }}" ></i> **** {{ $paymentMethod->card->last4 }}
                                            @if(Auth::user()->default_payment_method->id  == $paymentMethod->id)
                                            <small class='bg-gray pl-3 pr-3 pt-1 pb-1 ml-2' style="border-radius: 8px;"> Default</small>
                                            @endif
                                        </td>
                                        <td>Expires {{ date('m', ($paymentMethod->card->exp_month)) }}/{{ $paymentMethod->card->exp_year }}</td>
                                        @if(Auth::user()->default_payment_method->id  != $paymentMethod->id)
                                            <td class="text-right dropdown">
                                                <a href="#" class="btn btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-ellipsis-v"></i>
                                                </a>
                                                <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                    <li><a href="#" onclick="if(confirm('Are you sure to make it default?')){ $('#form-{{ $paymentMethod->id }}').submit() }"> Make as Default</a></li>
                                                    <li><a href="#" onclick="if(confirm('Are you sure to delete?')){ $('#deleteform-{{ $paymentMethod->id }}').submit() }"> Delete</a></li>
                                                </ul>
                                                <form method="POST" id="form-{{ $paymentMethod->id }}" action="{{ route('dashboard.payment-method.setdefault', $paymentMethod->id) }}"> @csrf </form>
                                                <form method="POST" id="deleteform-{{ $paymentMethod->id }}" action="{{ route('dashboard.payment-method.destroy', $paymentMethod->id) }}"> @csrf @method("DELETE") </form>
                                            </td>
                                        @else
                                            <td></td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                        <hr>
{{--                        <h3>Invoices</h3>--}}
{{--                        <table class='table table-striped'>--}}
{{--                            @foreach ($invoices as $invoice)--}}
{{--                                <tr>--}}
{{--                                    <td>{{ $invoice->date()->toFormattedDateString() }}</td>--}}
{{--                                    <td>{{ $invoice->total() }}</td>--}}
{{--                                    <td class='text-right'><a href="{{ route('dashboard.subscription.invoice.download', $invoice->id) }}">Download</a></td>--}}
{{--                                </tr>--}}
{{--                            @endforeach--}}
{{--                        </table>--}}
                    </div>


                </div>
            @else
                <div class="row">
                    <div class="col-sm-8">
                        <p>
                            Please add first payment method to get started.
                        </p>
                        <div class="panel panel-default" id="add-payment-form-container">
                            <div class="panel-heading">
                                Add Payment Method
                            </div>
                            <div class="panel-body">
                                <form action="{{ route('dashboard.payment-method.store') }}" method="POST" id="add-payment-method-form">
                                    @csrf
                                    <p>
                                        <label for="">Card Holder Name</label>
                                        <input class='form-control' placeholder="Card Holder Name" value="{{ Auth::user()->name }}" required id="card-holder-name" type="text">
                                    </p>

                                    <p>
                                        <label for="">Card Details</label>
                                        <!-- Stripe Elements Placeholder -->
                                        <div id="card-element" class='form-control'></div>
                                    </p>
                                    <p>
                                        <div id="card-errors"></div>
                                    </p>

                                    <button id="card-button" type="submit" class="btn btn-warning" data-secret="{{ $intent->client_secret }}">
                                        Create Payment Method
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade in" id="amendmentsModal" tabindex="-1" role="dialog" aria-labelledby="amendmentsModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <subscription-amendments-form-component url="{{ route('dashboard.subscription.send.amendment') }}"></subscription-amendments-form-component>
        </div>
        </div>
    </div>
</section>



@endsection

@push('scripts')
    {{-- @if(!count($paymentMethods)) --}}
        <script src="https://js.stripe.com/v3/"></script>
        <script>
            // Custom styling can be passed to options when creating an Element.
            // (Note that this demo uses a wider set of styles than the guide below.)
            var style = {
                base: {
                    lineHeight: '1.429'
                },
                invalid: {
                    color: '#fa755a',
                    iconColor: '#fa755a'
                }
            };

            const stripe = Stripe('{{ env("STRIPE_KEY") }}');
            const elements = stripe.elements();
            const cardElement = elements.create('card');
            cardElement.mount('#card-element');

            const errorElement = document.getElementById('card-errors');
            const form = document.getElementById('add-payment-method-form');
            const cardHolderName = document.getElementById('card-holder-name');
            const cardButton = document.getElementById('card-button');
            const clientSecret = cardButton.dataset.secret;

            cardButton.addEventListener('click', async (e) => {
                e.preventDefault();
                cardButton.innerHTML = "<i class='fa fa-spin fa-spinner'></i> processing.."
                cardButton.setAttribute('disabled', 'disabled');
                const { setupIntent, error } = await stripe.confirmCardSetup(
                    clientSecret, {
                        payment_method: {
                            card: cardElement,
                            billing_details: { name: cardHolderName.value }
                        }
                    }
                );

                if (error) {
                    errorElement.textContent = error.message;
                    cardButton.innerHTML = "Create Payment Method"
                    cardButton.removeAttribute('disabled');
                } else {
                    paymentMethodHandler(setupIntent.payment_method)
                }

            });

            function paymentMethodHandler(payment_method) {
                var hiddenInput = document.createElement('input');
                hiddenInput.setAttribute('type', 'hidden');
                hiddenInput.setAttribute('name', 'payment_method');
                hiddenInput.setAttribute('value', payment_method);
                form.appendChild(hiddenInput);
                form.submit();
            }

        </script>
    {{-- @endif --}}
@endpush

