@extends('dashboard.includes.layouts.main')

@section('page_title', 'Business Information')

@section('content')
<section class="content">
    
    @include('dashboard.includes.alerts.success')
    @include('dashboard.includes.alerts.error')
    
    <div class="box no-border">
        
        {{-- include tab --}}
        @include('dashboard.profile.includes.tab')
        
        <div class="box-body">
            <a href="{{ route('site.supplier.show', $company->slug) }}" target="_blank" class="mt-0 pull-right"><i class="fa fa-eye"></i> Preview Profile</a>
            <form action="{{ route('dashboard.profile.update') }}" method="post"  id='form-with-loading'  enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row">  
                    <div class="col-sm-5">
                    <input type="hidden" required class="form-control" name="account_information" value="account_information">
                    <div class="mb-5">
                        <h4 class="header-title">ACCOUNT</h4>
                        <p>
                        <div class="text-muted" for="">Business Name</div>
                        <input type="text" value="{{ old('name') ?? $company->name }}" readonly class="form-control input-md">
                        <a href="#"><i class="fa fa-question-circle"></i> Request for change name</a>
                        </p>  
                      
                        <p>
                            <div class="text-muted" >Services *</div>
                            <input type="text" name="services" value="{{ old('services') ?? $company->services }}" class="form-control input-sm">
                        </p>
                        <p>
                            <div class="text-muted" >Recognition</div>
                            <input type="text" name="recognition"  value="{{ old('recognition') ?? $company->recognition }}" class="form-control input-sm">
                        </p>
                        <p>
                            <div class="text-muted"><input type="checkbox" name="will_travel" @if($company->will_travel == 1) checked @endif> Will Travel International</div>
                        </p>
                        <p>
                            <div class="text-muted" >Description *</div>
                            <textarea name="description" cols="8" class="form-control">{{ old('description') ?? $company->description }}</textarea>
                        </p>
                    </div>

                    <div class="mb-5">
                        <h4 class="header-title">CONTACT INFORMATION</h4>
                        <p>
                            <div class="text-muted" for="">Email Address *</div>
                            <input type="text" required class="form-control" name="email" value="{{ old('email') ?? $company->email}}">
                        </p>
                        <p>
                            <div class="text-muted" for="">Telephone Number *</div>
                            <input type="text" required class="form-control" name="telephone_number" value="{{ old('telephone_number') ?? $company->telephone_number}}">
                        </p>
                        <p>
                        <div class="text-muted">Facebook URL</div>
                            <input type="url" name="facebook_link" value="{{ old('facebook_link') ?? $company->facebook_link }}" class="form-control input-sm">
                        </p>
                        <p>
                            <div class="text-muted">Twitter URL</div>
                            <input type="url" name="twitter_link" value="{{ old('twitter_link') ?? $company->twitter_link }}" class="form-control input-sm">
                        </p>
                        <p>
                            <div class="text-muted">Instagram URL</div>
                            <input type="url" name="instagram_link" value="{{ old('instagram_link') ?? $company->instagram_link }}" class="form-control input-sm">
                        </p>
                        <p>
                            <div class="text-muted">Youtube URL</div>
                            <input type="url" name="youtube_link" value="{{ old('youtube_link') ?? $company->youtube_link }}" class="form-control input-sm">
                        </p>
                    </div>
                    <div class="mb-5">
                        <h4 class="header-title">Categories</h4>
                            <p>
                                <ul>
                                    @foreach($company->categories()->whereType('cater')->get() as $cater)
                                        <li>{{ $cater->name }}</li>
                                    @endforeach
                                </ul>
                            </p>
                    </div>
                    <div class="mb-5">
                        <h4 class="header-title">Visibility</h4>
                            <p>
                                <input type="checkbox" data-type="icheck" name="is_active" @if($company->is_active == 1) checked @endif id="is_active" > 
                                <label for="is_active">Visible to anyone.</label>
                            </p>
                    </div>
                    @if(in_array('venue', $company->permissions()))
                        <div class="mb-5">
                            <h4 class="header-title">Single Venue Supplier</h4>
                                <p>
                                    <input type="checkbox" data-type="icheck" name="single_venue" @if($company->single_venue == 1) checked @endif id="single_venue" > 
                                    <label for="single_venue">Yes, We only cater single venue.</label>
                                </p>
                        </div>
                    @endif
                    <div class="mb-5">
                        <h4 class="header-title">Role</h4> 
                        <p>
                            <a href="#"><i class="fa fa-question-circle"></i> Request for Role change. </a>
                                <ul>
                                    @foreach($company->roles as $role)
                                        <li>{{ $role->display_name }}</li>
                                    @endforeach
                                </ul>
                            </p>
                    </div>

                    <button class="btn btn-warning" type="submit"><i class="fa fa-save"></i> Save Changes</button>
            </form>
                    </div>
                    <div class="col-sm-7">
                        
                        <div class="row">
                            <div class="col-sm-5">
                                <label class="text-muted" for="">Company Logo</label> 
                                 <div class="form-group mt-0">
                                    <p class="help-block  mt-0">
                                        <small>
                                            Recomended size: 250x250
                                        </small>
                                    </p>
                                    <label for="img-picker-logo" class="btn btn-default btn-xs"> Change Image</label>
                                </div>
                                <input type="file"  id="img-picker-logo"  style="display:none"  name="company_logo" accept="image/*">
                                <input type="hidden" name="cropped_image_logo">
                                <img src="{{ $company->image('logo')->path('logos', '/images/placeholders/placeholder250x250.png') }}" id="img-preview-logo" class="img img-responsive" alt="">
                            </div>
                            <div class="col-sm-7">
                                <label class="text-muted" for="">Cover Image</label>
                                <div class="form-group mt-0">
                                    <p class="help-block  mt-0">
                                        <small>
                                            Recomended size: 1920x450px
                                        </small>
                                    </p>
                                    <label for="cover" class="btn btn-default btn-xs"> Change Image</label>
                                </div>
                                <input type="file" style="display: none"  id="cover" data-type="image-picker" data-preview="#preview-cover" data-preview-type='img' name="cover">
                                
                                <img src="{{ $company->image('cover')->path('covers','/images/placeholders/placeholder-cover.png') }}"  id="preview-cover"  alt="" class="img img-responsive">
                                
                                {{-- <label class="text-muted" for="">Cover Picture</label> <small><i>1920x450</i></small> <br />
                                <input type="hidden" name="cropped_image_cover">
                                <input type="file" style="display:none" id="img-picker-cover" name="company_cover_picture" data-type="image-picker" data-preview="#preview-cover" data-preview-type='img'  accept="image/*">
                                <label for="img-picker-cover" class="btn btn-default btn-xs mb-3"> Change Cover</label>
                                <img src="{{ $company->image('cover')->path('covers', 'https://via.placeholder.com/1920x450.png') }}" id="img-preview-cover" class="img img-responsive" alt=""> --}}
                            </div>
                        </div>
                        <div class="text-center">
                            <p class="margin">Note: Accepted types are <code>{{ config('filesystems.accepted_image_types') }}</code></p>

                        </div>
           
                        <hr>
                        <div class="mb-5">
                            <h4 class="header-title">ADDRESS</h4>
                            {{-- <address-component town="{{ $company->town }}" state="{{ $company->state }}" post_code="{{ $company->post_code }}" latitude="{{ $company->latitude }}" longitude="{{ $company->longitude }}" country="{{ $company->country }}" ></address-component> --}}
                            {{-- <form action="{{ route('dashboard.profile.update') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')  --}}
                                <div class="col-sm-6">
                                    <p>
                                        <div class="text-muted" for="">Town *</div>
                                        <input type="text" required class="form-control" name="town" value="{{ $company->town }}">
                                    </p>
                                </div>
                                <div class="col-sm-6">
                                    <p>
                                        <div class="text-muted" for="">State *</div>
                                        <input type="text" required class="form-control" name="state" value="{{ $company->state }}">
                                    </p>
                                </div>
                                <div class="col-sm-6">
                                    <p>
                                        <div class="text-muted" for="">Country *</div>
                                        <input type="text" required class="form-control" name="country" value="{{ $company->country }}">
                                    </p>
                                </div>
                                <div class="col-sm-6">
                                    <p>
                                        <div class="text-muted" for="">Post Code *</div>
                                        <input type="text" required class="form-control" name="post_code" value="{{ $company->post_code }}">
                                    </p>
                                </div>
                                {{-- <input type="hidden" required class="form-control" name="address" value="address">
                                <div class="col-md-12">
                                        <button class="btn btn-warning" type="submit"><i class="fa fa-save"></i> Save Changes</button>
                                </div>
                            </form> --}}
                            
                        
                        </div>
                        <div class="mb-5">
                            <h4 class="header-title">Area of Services</h4>
                            <area-location-component getlocationroute="{{ route('dashboard.profile.getlocations', $company->id) }}" addroute="{{ route('dashboard.profile.addlocation', $company->id) }}"></area-location-component>

                        </div>
                    </div>


                </div>
           
        </div>
        
    </div>

    <div class="modal" id="cropper-modal-logo" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <label>Crop Image</label>
                    <div id="crop-picture-container-logo" class="mw-100"></div>
                    <button id="crop-done-btn-logo" class="btn btn-primary btn-lg">Done</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="cropper-modal-cover" tabindex="-1" role="dialog" style="overflow: auto">
        <div class="modal-dialog modal-lg" style="width:100%" role="document">
            <div class="modal-content">
                <div class="modal-body text-center" style="overflow:auto; width:100%">
                    <label>Crop Cover</label>
                    <div id="crop-picture-container-cover"></div>
                    <button id="crop-done-btn-cover" class="btn btn-primary btn-lg">Done</button>
                </div>
            </div>
        </div>
    </div>
 
</section
@endsection

@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.4/croppie.css" integrity="sha256-M8o9uqnAVROBWo3/2ZHSIJG+ZHbaQdpljJLLvdpeKcI=" crossorigin="anonymous" />
@endpush
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.4/croppie.min.js" integrity="sha256-bQTfUf1lSu0N421HV2ITHiSjpZ6/5aS6mUNlojIGGWg=" crossorigin="anonymous"></script>
    <script>
        //zoming page for large image cropping
        $('#cropper-modal-cover').on('shown.bs.modal', function () {
            $('body').addClass('zoomout')
        })          
        $('#cropper-modal-cover').on('hidden.bs.modal', function () {
            $('body').removeClass('zoomout')
        })          

        // for logo
            var resize = $('#crop-picture-container-logo').croppie({
                enableExif: true,
                enableOrientation: true,
                viewport: { // Default { width: 100, height: 100, type: 'square' } 
                    width: 250,
                    height: 250,
                    type: 'square'
                },
                enforceBoundary: false,
                boundary: {
                    width: 400,
                    height: 400
                }
            });
            
            
            $('#img-picker-logo').on('change', function () {
                var reader = new FileReader();
            
                $("#cropper-modal-logo").modal("show")
            
                reader.onload = function (e) {
                    resize.croppie('bind', {
                        url: e.target.result
                    }).then(function () {
                        //console.log('jQuery bind complete');
                    });
                }
                
                reader.readAsDataURL(this.files[0]);
            
            });
            
            $("#crop-done-btn-logo").on('click', function (ev) {
                resize.croppie('result', {
                    type: 'canvas',
                    size: 'viewport'
                }).then(function (img) {
                    $("#img-preview-logo").attr("src", img)
                    $("input[name=cropped_image_logo").val(img)
                    $("#cropper-modal-logo").modal("hide")
                });
            });


            var resizecover = $('#crop-picture-container-cover').croppie({
                enableExif: true,
                enableOrientation: true,
                viewport: { // Default { width: 100, height: 100, type: 'square' } 
                    width: 1920,
                    height: 450,
                    type: 'square'
                },
                enforceBoundary: false,
                boundary: {
                    width: 1980,
                    height: 600
                }
            });
            
            
            $('#img-picker-cover').on('change', function () {
                var reader = new FileReader();
            
                $("#cropper-modal-cover").modal("show")
            
                reader.onload = function (e) {
                    resizecover.croppie('bind', {
                        url: e.target.result
                    }).then(function () {
                    });
                }
                
                reader.readAsDataURL(this.files[0]);
            
            });
            
            $("#crop-done-btn-cover").on('click', function (ev) {
                resizecover.croppie('result', {
                    type: 'canvas',
                    size: 'viewport'
                }).then(function (img) {
                    $("#img-preview-cover").attr("src", img)
                    $("input[name=cropped_image_cover").val(img)
                    $("#cropper-modal-cover").modal("hide")
                });
            });
    </script>
@endpush