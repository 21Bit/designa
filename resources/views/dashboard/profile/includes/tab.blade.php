<div class="nav-tabs-custom">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" @if(Request::get('tab') == "business") class="active" @endif ><a href="{{ route('dashboard.profile.index', ['tab' => 'business']) }}" aria-controls="home" role="tab">BUSINESS INFORMATION</a></li>
        <li role="presentation" @if(Request::get('tab') == "account") class="active" @endif ><a href="{{ route('dashboard.profile.index', ['tab' => 'account']) }}" aria-controls="home" role="tab">ACCOUNT INFORMATION</a></li>
        <li role="presentation" @if(Request::get('tab') == "password") class="active" @endif ><a href="{{ route('dashboard.profile.index', ['tab' => 'password']) }}" aria-controls="home" role="tab">CHANGE PASSWORD</a></li>
        <li role="presentation" @if(Request::get('tab') == "subscription") class="active" @endif ><a href="{{ route('dashboard.profile.index', ['tab' => 'subscription']) }}" aria-controls="home" role="tab">SUBSCRIPTION & BILLING</a></li>
    </ul>
</div>