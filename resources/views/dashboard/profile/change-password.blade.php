@extends('dashboard.includes.layouts.main')

@section('page_title', 'Account Information')

@section('content')
<section class="content">
    
    @include('dashboard.includes.alerts.success')
    @include('dashboard.includes.alerts.warning')
    @include('dashboard.includes.alerts.error')
    
    <div class="box no-border">

        {{-- include tab --}}
        @include('dashboard.profile.includes.tab')
        
        <div class="box-body">
            <div class="row">
                <div class="col-sm-4">
                    <form action="{{ route('dashboard.profile.changepassword') }}" method="POST" >
                        @csrf
                        <p>
                            <div class="text-muted">Current Password *</div>
                            <input type="password"  name='current_password' required class="form-control">
                        </p>
                        <p>
                            <div class="text-muted">New Password *</div>
                            <input type="password"  name='password' required class="form-control">
                        </p>
                        <p>
                            <div class="text-muted">Repeat New Password *</div>
                            <input type="password"  name='password_confirmation' required class="form-control">
                        </p>
                        <br>
                        <p>
                            <button class="btn btn-warning"><i class="fa fa-save"></i> Save Changes</button>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection