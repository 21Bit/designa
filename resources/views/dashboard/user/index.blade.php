@extends('dashboard.includes.layouts.main')

@section('page_title', 'Users')

@section("content-hearder")
        <h1>
            Users
            {{-- <small>it all starts here</small> --}}
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Users</li>
        </ol>
@endsection

@section('content')
        @include("dashboard.includes.alerts.success")
        @include("dashboard.includes.alerts.error")
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-6">
                        <form>
                            <div class="input-group">
                                <input type="text" name="q" value="{{ Request::get('q') }}" class="form-control" placeholder="Search for...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit">Search</button>
                                </span>
                            </div><!-- /input-group -->
                        </form>
                    </div>
                    <div class="col-sm-6">
                        <div>
                            {{-- <a href="{{ route('dashboard.user.create') }}" class="btn btn-default"><i class="fa fa-plus"></i> New</a> --}}
                            <button href="#" id="delete-all-btn" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                        </div>
                    </div>
                </div>
                <form action="{{ route('dashboard.user.destroy', 0) }}" method="post" id="delete-all-form">
                    @csrf
                    @method('DELETE')
                    <table class="table">
                        <thead>
                            <tr>
                                <th><input type="checkbox"></th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Type</th>
                                <th>Company</th>
                                <th></th>
                            </tr>
                        </thead>
                        @foreach($users as $user)
                            <tr>
                                <td><input type="checkbox" value="{{ $user->id }}" name='item_checked[]'></td>
                                <td>
                                    <img src="{{ $user->getProfilePicture(true) }}" class="mr-2" style="width: 35px;">
                                    {{ $user->name }}
                                </td>
                                <td>{{ $user->email }}</td>
                                <td>{{ ucfirst($user->type) }}</td>
                                <td>{{ optional($user->company)->name }}</td>
                                <td class="text-right">
                                    <a href="{{ route('dashboard.user.edit', $user->id) }}" class="btn"><i class="fa fa-pencil"></i> Edit</a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </form>
                {{ $users->appends(['q' => Request::get('q')])->links() }}
            </div>
        </div>
@endsection
