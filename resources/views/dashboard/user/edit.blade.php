@extends('dashboard.includes.layouts.main')

@section('page_title', 'Account Information')

@section('content')
<section class="content">
    
    @include('dashboard.includes.alerts.success')
    @include('dashboard.includes.alerts.error')
    
    <div class="box no-border">
        
        <div class="box-body">
            <div class="row">
                <div class="col-sm-4">
                    <form action="{{ route('dashboard.user.update', $user->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <p>
                            <img src="{{ Auth::user()->getProfilePicture() }}" id="img-preview" class="img image-responsive" alt="User Image">
                            <input type="file"  id="img-picker"  style="display:none"  name="company_logo" accept="image/*">
                            <input type="hidden" name="picture">
                            <br>
                            <label for="img-picker" style="margin-top:10px;" class="btn btn-default btn-xs mb-2"> Change Profile</label>
                        </p>
                        @if($user->company)
                                <p>
                                <div class="text-muted">Company</div>
                                <input type="text" value="{{ $user->company->name }}" disabled class="form-control">
                                </p>
                        @endif
                        <p>
                            <div class="text-muted">Name *</div>
                            <input type="text" value="{{ $user->name }}" required name='name' class="form-control">
                        </p>
                        {{-- @if(Auth::user()->type == "administrator")
                        <p>
                            <div class="text-muted">Company *</div>
                            <select name='company' class='form-control'>
                                @foreach($companies as $company)
                                    <option @if(Auth::user()->company_id == $company->id) selected @endif value="{{ $company->id }}">{{ $company->name }}</option> 
                                @endforeach
                            </select>
                        </p>
                        @endif --}}
                        <p>
                            <div class="text-muted">Email Address *</div>
                            <input type="text" value="{{ $user->email }}" required name='email' class="form-control">
                        </p>
                        <p>
                            <div class="text-muted">Password <small><i>(if provided consider change password)</i></small></div>
                            <input type="password"  name='password'  class="form-control">
                        </p>
                        <br>
                        <p>
                            <button class="btn btn-warning"><i class="fa fa-save"></i> Save Changes</button>
                            <a class="btn btn-warning" href="{{ route('dashboard.user.index') }}"><i class="fa fa-ban"></i> Back</a>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal" id="cropper-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body text-center">
                <label>Crop Image</label>
                <div id="crop-picture-container" class="mw-100"></div>
                <button id="crop-done-btn" class="btn btn-primary btn-lg">Done</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.4/croppie.css" integrity="sha256-M8o9uqnAVROBWo3/2ZHSIJG+ZHbaQdpljJLLvdpeKcI=" crossorigin="anonymous" />
@endpush
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.4/croppie.min.js" integrity="sha256-bQTfUf1lSu0N421HV2ITHiSjpZ6/5aS6mUNlojIGGWg=" crossorigin="anonymous"></script>
    <script>

        // for logo
            var resize = $('#crop-picture-container').croppie({
                enableExif: true,
                enableOrientation: true,
                viewport: { // Default { width: 100, height: 100, type: 'square' } 
                    width: 250,
                    height: 250,
                    type: 'square'
                },
                boundary: {
                    width: 400,
                    height: 400
                }
            });
            
            
            $('#img-picker').on('change', function () {
                var reader = new FileReader();
            
                $("#cropper-modal").modal("show")
            
                reader.onload = function (e) {
                    resize.croppie('bind', {
                        url: e.target.result
                    }).then(function () {
                        //console.log('jQuery bind complete');
                    });
                }
                
                reader.readAsDataURL(this.files[0]);
            
            });
            
            $("#crop-done-btn").on('click', function (ev) {
                resize.croppie('result', {
                    type: 'canvas',
                    size: 'viewport'
                }).then(function (img) {
                    $("#img-preview").attr("src", img)
                    $("input[name=picture]").val(img)
                    $("#cropper-modal").modal("hide")
                });
            });
    </script>
@endpush