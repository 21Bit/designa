@extends('dashboard.includes.layouts.main')

@section('page_title', 'Create Product')

@section("content-hearder")
        <h1>
            Products
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard.home') }}">Home</a></li>
            <li><a href="{{ route('dashboard.product.index') }}">Product</a></li>
            <li class="active">Create Product</li>
        </ol>
@endsection

@section('content')
    @include("dashboard.includes.alerts.error")
    @include("dashboard.includes.alerts.warning")
    <form action="{{ route('dashboard.product.save') }}"  id='form-with-loading' method="POST" enctype="multipart/form-data">
        @csrf
        <div class="text-right mb-2">
            <button class="btn btn-warning" type="submit"><i class="fa fa-save"></i> Save</button>
            <a href="{{ route('dashboard.product.index') }}" class="btn btn-default"><i class="fa fa-ban"></i> Cancel</a>
        </div>
        <div class="box">
            <div class="box-body">

                <div class="row">
                    <div class="col-sm-3">
                        <p>
                            <label for="">Status *</label>
                            <select name="is_published" id="" class="form-control">
                                <option value="1">Published</option>
                                <option value="0" @if(old('is_published') == 0) selected @endif>Draft</option>
                            </select>
                        </p>
                        <p>
                            <input type="checkbox" data-type="icheck" checked name="proceed" id="proceed"> <label for="proceed" style="font-weight: normal">Proceed to Manage</label>
                        </p>
                        <p>
                            <label for="">Thumbnail Image <small><i>({{ config('filesystems.accepted_image_types') }})</i></small></label>
                            <input type="file" style="display: none" id="thumbnail" accept="image/*"  data-type="image-picker" data-preview="#preview-thumbnail" data-preview-type='img' name="thumbnail">
                            <br>
                            <label for="thumbnail" class="btn btn-default btn-xs d-block m-2" ><i class="fa fa-image"></i>  Choose Image</label>
                            <img src="/images/placeholders/placeholder.png" class="img img-responsive" id="preview-thumbnail" alt="">
                            <!-- <img src="https://via.placeholder.com/250" class="img img-responsive" alt=""> -->
                        </p>
                    </div>
                    <div class="col-sm-6">
                        <p>
                            <label for="">Name *</label>
                            <input type="text" autofocus name="name" value="{{ old('name') }}" required class="form-control input-sm">
                        </p>
                        <p>
                            <label for="">Product Identifier <small>(optional)</small></label>
                            <input type="text" name="product_identifier" value="{{ old('product_identifier') }}" class="form-control input-sm">
                        </p>

                        <p>
                            <label for="">Desciption</label>
                            <textarea name="description" id="" cols="30" rows="10" class="form-control">{{ old('description') }}</textarea>
                        </p>

                        <p>
                            <label for="">Product Size <small>(optional)</small></label>
                            <input type="text" value="{{ old('product_size') }}"  name="product_size" class="form-control input-sm">
                        </p>
                        {{-- <p>
                            Type *
                            <select name="product_type" id="" class="form-control">
                                @foreach($types as $type)
                                    <option value="{{ $type->id }}">{{ $type->name }}</option>
                                    @foreach($type->styles as $style)
                                        <option value="{{ $style->id }}"> -- {{ $style->name }}</option>
                                    @endforeach
                                @endforeach
                            </select>
                        </p> --}}
                        <p>
                            <product-type-select
                                caterories-route="/producttypes" ></product-type-select>
                        </p>

                    </div>
                    <div class="col-sm-3">
                        <p>
                            <label for="" class="mb-0">Category</label>
                            @foreach($categories as $category)
                                <div class="pl-3">
                                    <input type="checkbox" data-type="icheck" id="category-{{ $category->id }}" name='categories[]' value="{{ $category->id }}">
                                    <label style="font-weight: normal" for="category-{{ $category->id }}">{{ $category->name }}</label>
                                </div>
                            @endforeach

                        </p>
                        <p>
                            <label for="" class="mb-0">Color Themes</label>
                            @foreach($colors as $color)
                                <div class="pl-3">
                                    <input type="checkbox" data-type="icheck" id="color-{{ $color->id }}" name='colors[]' value="{{ $color->id }}">
                                    <label style="font-weight: normal;" for="color-{{ $color->id }}"><span class='color-span mr-2' style="background-color:{{ $color->description }}">&nbsp;&nbsp;</span> {{ $color->name }}</label>
                                </div>
                            @endforeach
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
