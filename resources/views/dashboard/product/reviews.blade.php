@extends('dashboard.includes.layouts.main')

@section('page_title', $product->name)

@section("content-hearder")
    <h1>
        Products
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.home') }}">Home</a></li>
        <li><a href="{{ route('dashboard.product.index') }}">Products</a></li>
        <li class="active">{{ $product->name }}</li>
    </ol>
@endsection

@section('content')
<div class="row">
    <div class="col-sm-3">
        @include('dashboard.product.includes.side-details')
    </div>
    <div class="col-sm-9">
        <div class="box">
            <div class="box-body">
                <h1 class="orange mt-0">{{ $product->name }}</h1>
            </div>
        </div>
        <div class="box no-border">
            {{-- show product tab --}}
            @include('dashboard.product.includes.tab')
            
            <div class="box-body">
                <review-component  type="product" :id="{{ $product->id }}"></review-component>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function(){
            $('#gallery-input').change(function(){
                $(this).closest('form').submit();
            })
        })
    </script>
@endpush