<div class="nav-tabs-custom">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" @if(Request::get('tab') == '') class="active" @endif ><a href="{{ route('dashboard.product.show', $product->slug) }}" aria-controls="home" role="tab"><i class="fa fa-file-image-o"></i> Gallery</a></li>
        @if(in_array('Pro', Auth::user()->subscribedPlans()) || Auth::user()->is_superadmin)
            <li role="presentation" @if(Request::get('tab') == '3d') class="active" @endif ><a href="{{ route('dashboard.product.show', $product->slug) }}?tab=3d" aria-controls="home" role="tab"><i class="fa fa-cube"></i> 3D Models</a></li>
        @endif
        <li role="presentation" @if(Request::get('tab') == 'reviews') class="active" @endif ><a href="{{ route('dashboard.product.show', $product->slug) }}?tab=reviews" aria-controls="profile" role="tab"><i class="fa fa-comment-o"></i> Reviews</a></li>
        <li role="presentation" @if(Request::get('tab') == 'hearts') class="active" @endif ><a href="{{ route('dashboard.product.show', $product->slug) }}?tab=hearts" aria-controls="messages" role="tab"><i class="fa fa-heart-o"></i> Saved</a></li>
    </ul>
</div>
