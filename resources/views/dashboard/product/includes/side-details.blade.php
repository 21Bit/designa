<img src="{{ $product->thumbnail }}" class="img img-responsive img-thumbnail mb-2" alt="">
<div class="box mb-3">
    <div class="box-body">
        <div class="row">
            <div class="col-xs-6">
                <a href="{{ route('dashboard.product.edit', $product->id) }}" class="btn btn-xs btn-block"><i class="fa fa-pencil"></i>
                    Edit
                </a>
            </div>
            <div class="col-xs-6">
                <a href="" class="btn btn-xs btn-block"><i class="fa fa-trash"></i> Delete</a>
            </div>
        </div>
    </div>
</div>
<div class="box mb-3">
    <div class="box-header with-border">
        <h3 class="box-title">Information</h3>
        <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        <strong>
            <a class="mb-0" href="{{ route('dashboard.company.show', $product->company->slug) }}">
                {{ $product->company->name }}
            </a>
        </strong>
        <p class="text-muted">
            Company
        </p>
        <hr>

        <strong>
            @foreach($product->caters as $cater)
            {{ $cater->name }}@if(!$loop->last), @endif
            @endforeach
        </strong>
        <p class="text-muted">
            Category
        </p>
        <hr>

        <strong>
            {{ optional($product->product_type)->name }}
        </strong>
        <p class="text-muted">
            Product Category
        </p>
        <hr>

        <strong>
            {{ optional($product->style)->name }}
        </strong>
        <p class="text-muted">
            Product Type
        </p>
        
    </div>
</div>

<div class="box mb-3">
    <div class="box-header with-border">
        Description
        <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        {!! $product->description !!}
    </div>
</div>

<div class="box mb-3">
    <div class="box-header with-border">
        Colours
        <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        @foreach($product->colors as $color)
        <span class='color-span'
            style="background-color:{{ $color->description }}">&nbsp;&nbsp;</span>
        @endforeach
    </div>
</div>