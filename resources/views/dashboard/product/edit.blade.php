@extends('dashboard.includes.layouts.main')

@section('page_title', 'Edit Product')

@section("content-hearder")
        <h1>
            Products
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard.home') }}">Home</a></li>
            <li><a href="{{ route('dashboard.product.index') }}">Product</a></li>
            <li class="active">Edit Product</li>
        </ol>
@endsection

@section('content')
<form action="{{ route('dashboard.product.update', $product->id) }}"  id='form-with-loading' method="post" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="text-right mb-2">
        <button class="btn btn-warning" @if(!Auth::user()->is_superadmin) @if($product->is_lock) disabled @endif  @endif><i class="fa fa-save"></i> Save Changes</button>
        <a href="{{ route('dashboard.product.index') }}" class="btn btn-default"><i class="fa fa-ban"></i> Cancel</a>
    </div>
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-sm-3">
                    <p>
                        <label for="">Status *</label>
                        <select name="is_published" @if(!Auth::user()->is_superadmin) @if($product->is_lock) disabled @endif  @endif class="form-control">
                            <option value="1" {{ $product->is_published == 1 ? 'selected' : '' }}>Published</option>
                            <option value="0" {{ $product->is_published == 0 ? 'selected' : '' }}>Draft</option>
                        </select>
                    </p>
                    
                    <p>
                        <input type="checkbox" name="proceed" data-type="icheck" checked id="proceed"> 
                        <label for="proceed" style="font-weight: normal">Proceed to Manage</label>
                    </p>

                    @if(Auth::user()->is_superadmin)
                        <p>
                            <input type="checkbox" {{ $product->is_lock == 1 ? 'checked' : '' }} 
                                data-type="icheck" value="1" name="is_lock" id="is_lock">

                            <label for="is_lock" style="font-weight: normal">Lock Status</label>                            
                        </p>
                        <p>
                            <input type="checkbox" {{ $product->featured == 1 ? 'checked' : '' }} 
                                data-type="icheck" value="1" name="featured" id="featured">

                            <label for="featured" style="font-weight: normal">Feature Status</label>                            
                        </p>
                    @endif

                    <p>
                        <label for="">Thumbnail Image <small><i>({{ config('filesystems.accepted_image_types') }})</i></small></label> <br>
                        <input type="file"  accept="image/*" style="display: none" id="thumbnail" name="thumbnail"  data-type="image-picker" data-preview="#preview-thumbnail" data-preview-type='img' >
                        <label for="thumbnail" class="btn btn-xs btn-default mb-2"><i class="fa fa-file-image-o"></i> Change Picture</label>
                        <img src="{{ $product->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder250x250.png') }}" id="preview-thumbnail" class="img img-responsive" alt="">
                    </p>
                </div>
                <div class="col-sm-6">
                    <p>
                        <label for="">Name *</label>
                        <input type="text" autofocus name="name" value="{{ $product->name }}" required class="form-control input-sm">
                    </p>
                    <p>
                        <label for="">Product Identifier <small>(optional)</small></label>
                        <input type="text" name="product_identifier" value="{{ old('product_identifier') ?? $product->product_identifier }}" class="form-control input-sm">
                    </p>
                    <p>
                        <label for="">Desciption</label>
                        <textarea name="description" id="" cols="30" rows="10" class="form-control">{{  old('description') ?? $product->description }}</textarea>
                    </p>
                      <p>
                        <label for="">Product Size <small>(optional)</small></label>
                        <input type="text" name="product_size" value="{{ old('product_size') ?? $product->product_size }}" class="form-control input-sm">
                    </p>

                    <p>
                        <product-type-select 
                            defaulttype="{{ optional($product->product_type)->id }}" 
                            defaultstyle="{{ optional($product->style)->id }}" 
                            caterories-route="/producttypes" ></product-type-select>
                    </p>
                    @if(Auth::user()->is_superadmin)
                    <p>
                        <label for="">Supplier</label>
                        <select name="company_id" id="" class="form-control">
                            @foreach($companies as $company)
                                <option value="{{ $company->id }}" @if($product->company_id == $company->id) selected @endif >{{ $company->name }}</option>
                            @endforeach
                        </select>
                    </p>
                    @endif
                    
                </div>
                <div class="col-sm-3">
                    <p>
                        <label for="" class="mb-0">Category</label>
                        @foreach($categories as $category)
                        <div class="pl-3">
                            <input type="checkbox" data-type="icheck" @if(in_array($category->id, $product->categoryType('cater')->pluck('id')->toArray())) checked @endif   id="category-{{ $category->id }}" name='categories[]' value="{{ $category->id }}">
                            <label style="font-weight: normal" for="category-{{ $category->id }}">{{ $category->name }}</label>
                        </div>
                        @endforeach
                    </p>
                    <p>
                        <label for="" class="mb-0">Color Themes</label> <i class="fa fa-pencil"></i>
                        @foreach($colors as $color)
                            <div class="pl-3">
                                <input type="checkbox" data-type="icheck" @if(in_array($color->id, $product->categoryType('color')->pluck('id')->toArray())) checked @endif  id="color-{{ $color->id }}" name='colors[]' value="{{ $color->id }}">
                                <label style="font-weight: normal;" for="color-{{ $color->id }}"><span class='color-span mr-2' style="background-color:{{ $color->description }}">&nbsp;&nbsp;</span> {{ $color->name }}</label>
                            </div>
                        @endforeach
                    </p>
                </div>
                
            </div>
        </div>
    </div>
</form>
@endsection