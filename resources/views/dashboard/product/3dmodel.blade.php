@extends('dashboard.includes.layouts.main')

@section('page_title', $product->name)

@section("content-hearder")
    <h1>
        Products
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.home') }}">Home</a></li>
        <li><a href="{{ route('dashboard.product.index') }}">Products</a></li>
        <li class="active">{{ $product->name }}</li>
    </ol>
@endsection

@section('content')
<div class="row">
    <div class="col-sm-3">
        @include('dashboard.product.includes.side-details')
    </div>
    <div class="col-sm-9">
        <div class="box">
            <div class="box-body">
                <h1 class="orange mt-0">{{ $product->name }}</h1>
            </div>
        </div>
        <div class="box no-border">
            {{-- show product tab --}}
            @include('dashboard.product.includes.tab')

            <div class="box-body">
                <div class="row">
                    <div class="col-sm-6 pl-5">
                        <h3>Welcome to 3D world!</h3>
                        <p>This awesome feature allows you to make your product available in 3D and to be use in supplier's 3D templates.</p>
                        <p>For more 3D creation for your product, We here to help you out.</p>
                     </div>
                    <div class="col-sm-6">
                        <div class="panel panel-default mb-4">
                            <div class="panel-heading">
                                Web Asset Bundle
                                <label for="web_asset_bundle_input" class="btn btn-sm btn-primary pull-right"
                                       @if(!Auth::user()->hasSubscriptionAccess(['your-products-in-3d-10-is-pro']))
                                         disabled
                                       @endif
                                       style="margin-top:-5px;"><i class="fa fa-upload"></i> Upload File</label>
                            </div>
                            <div class="panel-body pb-3">
                                <form action="{{ route('dashboard.product.upload-asset-bundle', $product->id) }}" style="display:none;" method="POST" enctype="multipart/form-data" >
                                    @csrf
                                    <input type="hidden" name='type' value="web">
                                    <input type="file" id="web_asset_bundle_input"
                                            @if(!Auth::user()->hasSubscriptionAccess(['your-products-in-3d-10-is-pro']))
                                             disabled
                                           @endif
                                           name="asset_bundle_file" class="input-file">
                                </form>
                                @if($product->asset_bundle_web)
                                    <div id="div-{{ $product->asset_bundle_web->id }}">
                                        <div class='alert bg-gray border p-3 d-inline-block mb-0' >
                                            {{ $product->asset_bundle_web->model_name }}
                                            <button data-buttons="delete" data-url="{{ route('dashboard.product.delete-asset-bundle', $product->asset_bundle_web->id)}}" data-type="div" data-id="{{ $product->asset_bundle_web->id }}" class="pull-right btn btn-xs"><i class="fa fa-remove"></i> Remove</button>
                                        </div>
                                        <small class="pl-2"><i>uploaded {{ $product->asset_bundle_web->created_at->diffForHumans() }}</i></small>
                                    </div>
                                @else
                                    <div class="pb-2">
                                        <i class='text-muted'> No asset bundle uploaded</i>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="panel panel-default mb-4">
                            <div class="panel-heading">
                                Android Asset Bundle
                                <label for="android_asset_bundle_input" class="btn btn-sm btn-primary pull-right" style="margin-top:-5px;"><i class="fa fa-upload"></i> Upload File</label>
                            </div>
                            <div class="panel-body pb-3">
                                <form action="{{ route('dashboard.product.upload-asset-bundle', $product->id) }}" style="display:none;" method="POST" enctype="multipart/form-data" >
                                    @csrf
                                    <input type="hidden" name='type' value="android">
                                    <input type="file" id="android_asset_bundle_input" name="asset_bundle_file" class="input-file">
                                </form>
                                @if($product->asset_bundle_android)
                                    <div id="div-{{ $product->asset_bundle_android->id }}">
                                        <div class='alert bg-gray border p-3 d-inline-block mb-0' >
                                            {{ $product->asset_bundle_android->model_name }}
                                            <button  data-buttons="delete" data-url="{{ route('dashboard.product.delete-asset-bundle', $product->asset_bundle_android->id)}}" data-type="div" data-id="{{ $product->asset_bundle_android->id }}" class="pull-right btn btn-xs"><i class="fa fa-remove"></i> Remove</button>
                                        </div>
                                        <small class="pl-2"><i>uploaded {{ $product->asset_bundle_android->created_at->diffForHumans() }}</i></small>
                                    </div>
                                @else
                                    <div class="pb-2">
                                        <i class='text-muted'> No asset bundle uploaded</i>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="panel panel-default mb-4">
                            <div class="panel-heading">
                                iOS Asset Bundle
                                <label for="ios_asset_bundle_input" class="btn btn-sm btn-primary pull-right" style="margin-top:-5px;"><i class="fa fa-upload"></i> Upload File</label>
                            </div>
                            <div class="panel-body pb-3">
                                <form action="{{ route('dashboard.product.upload-asset-bundle', $product->id) }}" style="display:none;" method="POST" enctype="multipart/form-data" >
                                    @csrf
                                    <input type="hidden" name='type' value="ios">
                                    <input type="file" id="ios_asset_bundle_input" name="asset_bundle_file" class="input-file">
                                </form>
                                @if($product->asset_bundle_ios)
                                    <div id="div-{{ $product->asset_bundle_ios->id }}">
                                        <div class='alert bg-gray border p-3 d-inline-block mb-0' >
                                            {{ $product->asset_bundle_ios->model_name }}
                                            <button  data-buttons="delete" data-url="{{ route('dashboard.product.delete-asset-bundle', $product->asset_bundle_ios->id)}}" data-type="div" data-id="{{ $product->asset_bundle_ios->id }}" class="pull-right btn btn-xs"><i class="fa fa-remove"></i> Remove</button>
                                        </div>
                                        <small class="pl-2"><i>uploaded {{ $product->asset_bundle_ios->created_at->diffForHumans() }}</i></small>
                                    </div>
                                @else
                                    <div class="pb-2">
                                        <i class='text-muted'> No asset bundle uploaded</i>
                                    </div>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function(){
            $('.input-file').change(function(){
                var form  = $(this).closest('form')
                form.trigger('submit')
            })
        })
    </script>
@endpush
