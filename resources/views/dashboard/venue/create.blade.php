@extends('dashboard.includes.layouts.main')
@section('page_title', 'Create Venue')
@section("content-hearder")
    <h1>
        Venue
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('dashboard.venue.index') }}">Venue</a></li>
        <li class="active">Create Venue</li>
    </ol>
@endsection

@section('content')
@include("dashboard.includes.alerts.error")
<form action="{{ route('dashboard.venue.store') }}" id="form-with-loading" method="post" enctype="multipart/form-data">
    <div class="text-right mb-2">
        <button class="btn btn-warning" type="submit"><i class="fa fa-save"></i> Save</button>
        <a href="{{ route('dashboard.venue.index') }}" class="btn btn-default"><i class="fa fa-ban"></i> Cancel</a>
    </div>
    @include('dashboard.includes.alerts.error')
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-sm-3">
                    <p>
                        <label for="">Status *</label>
                        <select name="is_published" class="form-control">
                            <option value="1">Published</option>
                            <option value="0">Draft</option>
                        </select>
                    </p>
                    <p>
                        <input type="checkbox" name="proceed" checked id="proceed"> <label for="proceed" style="font-weight: normal">Proceed to Manage</label>
                    </p>
                    <p>
                        {{-- <label for="">Thumbnail Image</label> <small>(250x250)</small>
                        <input type="file" style="display: none" id="thumbnail" name='thumbnail' data-type="thumbnail-picker" />
                        <label for="thumbnail" class='btn btn-xs btn-secondary'><i class="fa fa-file-image"></i> Choose Image</label>
                        <img id="thumbnail-image" class='img img-responsive' src="https://via.placeholder.com/250x200">                  --}}

                                <label class="text-muted" for="">Thumbnail</label> <small><i>800x650</i></small> <br />
                                <input type="file"  id="img-picker-logo"  style="display:none"  name="thumbnail" accept="image/*">
                                <input type="hidden" name="cropped_image">
                                <label for="img-picker-logo" class="btn btn-default btn-xs mb-2"> Choose Image</label>
                                <img src="/images/placeholders/placeholder.png" id="img-preview-thumbnail" class="img img-responsive" alt="">
                                
                    </p>
                    <p>
                        <label class="text-muted" for="">Cover Picture</label> <small><i>1920x450</i></small> <br />
                        <input type="hidden" name="cropped_image_cover">
                        <input type="file" style="display:none" id="img-picker-cover" name="company_cover_picture" data-type="image-picker" data-preview="#preview-cover" data-preview-type='img'  accept="image/*">
                        <label for="img-picker-cover" class="btn btn-default btn-xs mb-3"> Change Cover</label>
                        <img src="/images/placeholders/placeholder.png" id="img-preview-cover" class="img img-responsive" alt="">
                    </p>
                </div>
                <div class="col-sm-5">
                    @csrf
                    <p>
                        <label for="">Name *</label>
                        <input type="text" autofocus name="name" required value="{{ old('name') }}" class="form-control input-sm">
                    </p>
                    <p>
                        <label for="">Location *</label>
                        <geolocation-autocomplete standalone="yes" styleclass='form-control'></geolocation-autocomplete>
                    </p>
                   
                    {{-- <p>
                        <label for="">Price *</label>
                        <input type="number" min="1" name="price" required class="form-control input-sm">
                    </p> --}}
                    <p>
                        <label for="">Capacity *</label>
                        <input type="number" value="{{ old('capacity') }}"  min="1" name="capacity" required class="form-control input-sm">
                    </p>
                    <p>
                        <label for="">Max Seated *</label>
                        <input type="number"  min="1" value="{{ old('max_seated') }}" name="max_seated" required class="form-control input-sm">
                    </p>
                    <p>
                        <label for="">Max Standing *</label>
                        <input type="number"  min="1" name="max_standing" value="{{ old('max_standing') }}" required class="form-control input-sm">
                    </p>
                    <p>
                        <label for="">Venue Email Address *</label>
                        <input type="text" value="{{ old('email') ?? optional(request()->user()->company)->email }}" name="email" required class="form-control input-sm">
                    </p>
                    <p>
                        <label for="">Venue Contact Number *</label>
                        <input type="text" name="contact_number" value="{{ old('contact_number') ?? optional(request()->user()->company)->telephone_number }}" required class="form-control input-sm">
                    </p>
                    <p>
                        <label for="">Desciption</label>
                        <textarea name="description" id="texteditor" cols="30" rows="10" class="form-control">{!! old('description')  !!}</textarea>
                    </p>
                    <p>
                        <label for="">Operation Schedules *</label> <br />
                        <small><i>Note: leaving blank meaning no operation on that day</i></small>
                        <div class="row mb-2 mt-0">
                            <div class="col-xs-4"></div>
                            <div class="col-xs-4"><small>Opening Time</small></div>
                            <div class="col-xs-4"><small>Closing Time</small></div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-xs-4 pt-1"><b>Monday</b></div>
                            <div class="col-xs-4">
                                <input type="time" name='timein[]'  class='form-control input-sm'>
                            </div>
                            <div class="col-xs-4">
                                <input type="time" name="timeout[]" class='form-control input-sm'>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-xs-4 pt-1"><b>Tuesday</b></div>
                            <div class="col-xs-4">
                                <input type="time" name='timein[]' class='form-control input-sm'>
                            </div>
                            <div class="col-xs-4">
                                <input type="time" name="timeout[]" class='form-control input-sm'>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-xs-4 pt-1"><b>Wednesday</b></div>
                            <div class="col-xs-4">
                                <input type="time" name='timein[]' class='form-control input-sm'>
                            </div>
                            <div class="col-xs-4">
                                <input type="time" name="timeout[]" class='form-control input-sm'>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-xs-4 pt-1"><b>Thursday</b></div>
                            <div class="col-xs-4">
                                <input type="time" name='timein[]' class='form-control input-sm'>
                            </div>
                            <div class="col-xs-4">
                                <input type="time" name="timeout[]" class='form-control input-sm'>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-xs-4 pt-1"><b>Friday</b></div>
                            <div class="col-xs-4">
                                <input type="time" name='timein[]' class='form-control input-sm'>
                            </div>
                            <div class="col-xs-4">
                                <input type="time" name="timeout[]" class='form-control input-sm'>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-xs-4 pt-1"><b>Saturday</b></div>
                            <div class="col-xs-4">
                                <input type="time" name='timein[]' class='form-control input-sm'>
                            </div>
                            <div class="col-xs-4">
                                <input type="time" name="timeout[]" class='form-control input-sm'>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-xs-4 pt-1"><b>Sunday</b></div>
                            <div class="col-xs-4">
                                <input type="time" name='timein[]' class='form-control input-sm'>
                            </div>
                            <div class="col-xs-4">
                                <input type="time" name="timeout[]" class='form-control input-sm'>
                            </div>
                        </div>
                        <!-- <input type="text" name="operating_hours" required class="form-control input-sm"> -->
                    </p>
                </div>
                <div class="col-sm-4">
                    <p>
                        <label for="" class="mb-0">Category</label>
                        <select name="categories[]" class="form-control" data-type="select2"  multiple>
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}" @if(in_array($category->id, old('categories') ?? [])) selected @endif   @if( !in_array($category->id, Auth::user()->caterPluck('id'))) disabled @endif >{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </p>
                    <p>
                        <label for="" class="mb-0">Type</label>
                        <select name="types[]" class="form-control" data-type="select2" multiple>
                            @foreach($types as $type)
                                <option value="{{ $type->id }}" @if(in_array($type->id, old('types') ?? [])) selected @endif  >{{ $type->name }}</option>
                            @endforeach
                        </select>
                    </p>
                    <p>
                        <label for="" class="mb-0">Catering Options</label>
                        <select name="catering_options[]" class="form-control" data-type="select2"  multiple>
                            @foreach($catering_options as $catering_option)
                                <option value="{{ $catering_option->id }}" @if(in_array($catering_option->id, old('catering_options') ?? [])) selected @endif   >{{ $catering_option->name }}</option>
                            @endforeach
                        </select>
                    </p>
                    <p>
                        <label for="" class="mb-0">Beverage Options</label>
                        <select name="beverage_options[]" class="form-control" data-type="select2"  multiple>
                            @foreach($beverage_options as $beverage_option)
                                <option value="{{ $beverage_option->id }}" @if(in_array($beverage_option->id, old('beverage_options') ?? [])) selected @endif  >{{ $beverage_option->name }}</option>
                            @endforeach
                        </select>
                    </p>
                    <p>
                        <label for="" class="mb-0">Amenities</label>
                        <select name="amenities[]" class="form-control" data-type="select2"  multiple>
                            @foreach($amenities as $amenity)
                                <option value="{{ $amenity->id }}" @if(in_array($amenity->id, old('amenities') ?? [])) selected @endif  >{{ $amenity->name }}</option>
                            @endforeach
                        </select>
                    </p>
                    <p>
                        <label for="" class="mb-0">Average Cost per Heads</label>
                        <select name="average_cost_per_heads[]" class="form-control" data-type="select2"  multiple>
                            @foreach($average_cost_per_heads as $average_cost_per_head)
                                <option value="{{ $average_cost_per_head->id }}" @if(in_array($average_cost_per_head->id, old('average_cost_per_heads') ?? [])) selected @endif  >{{ $average_cost_per_head->name }}</option>
                            @endforeach
                        </select>
                    </p>
                </div>
                
            </div>
        </div>
    </div>
</form>

 <div class="modal" id="cropper-modal" tabindex="-1" role="dialog" >
    <div class="modal-dialog modal-lg mw-100" style="width:100%;height: 100%; margin:0; padding:0;" role="document">

            <div class="modal-body text-center mw-100" style="background-color:#fff">
                <label>Crop Thumbnail</label>
                <div id="crop-picture-container-logo" class="mw-100"  ></div>
                
                <div class="bg-white">
                    <button id="crop-done-btn-logo" class="btn btn-primary btn-lg">Done</button> 
                </div>
            </div>
        <!-- </div> -->
    </div>
</div>

 <div class="modal" id="cropper-modal-cover" tabindex="-1" role="dialog" style="overflow: auto">
    <div class="modal-dialog modal-lg" style="width:100%" role="document">
        <div class="modal-content">
            <div class="modal-body text-center" style="overflow:auto; width:100%">
                <label>Crop Cover</label>
                <div id="crop-picture-container-cover"></div>
                <button id="crop-done-btn-cover" class="btn btn-primary btn-lg">Done</button>
            </div>
        </div>
    </div>
</div>
 
@endsection

@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.4/croppie.css" integrity="sha256-M8o9uqnAVROBWo3/2ZHSIJG+ZHbaQdpljJLLvdpeKcI=" crossorigin="anonymous" />

@endpush

@push('scripts')
    <script src="{{ asset('/dashboard/ckeditor/ckeditor.js')}} "></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.4/croppie.min.js" integrity="sha256-bQTfUf1lSu0N421HV2ITHiSjpZ6/5aS6mUNlojIGGWg=" crossorigin="anonymous"></script>
    <script>

      

        $('#loadingModal').modal('show')

        CKEDITOR.replace( 'texteditor', {
            toolbar: [
                { 
                name: 'document', 
                items: [ '-', 'NewPage', 'Preview', '-', 'Templates' ] 
                }, 
                [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo', ],
                { 
                name: 'basicstyles', 
                items: [ 'Bold', 'Italic']
                }
            ],
            enterMode : CKEDITOR.ENTER_BR,
            shiftEnterMode: CKEDITOR.ENTER_P,
            menubar: false,
            branding:false,
            forced_root_block : false,
        });

        var resize = $('#crop-picture-container-logo').croppie({
                enableExif: true,
                enableOrientation: true,
                viewport: { // Default { width: 100, height: 100, type: 'square' } 
                    width: 800,
                    height: 650,
                    type: 'square'
                },
                boundary: {
                    width: 900,
                    height: 700
                }
            });

            
            
            $('#cropper-modal-cover').on('shown.bs.modal', function () {
                $('body').addClass('zoomout')
            })          
            $('#cropper-modal-cover').on('hidden.bs.modal', function () {
                $('body').removeClass('zoomout')
            })          

            var resizecover = $('#crop-picture-container-cover').croppie({
                enableExif: true,
                enableOrientation: true,
                viewport: { // Default { width: 100, height: 100, type: 'square' } 
                    width: 1920,
                    height: 450,
                    type: 'square'
                },
                enforceBoundary: false,
                boundary: {
                    width: 1980,
                    height: 600
                }
            });
        
            
            
            $('#img-picker-logo').on('change', function () {
                var reader = new FileReader();
            
                $("#cropper-modal").modal("show")
            
                reader.onload = function (e) {
                    resize.croppie('bind', {
                        url: e.target.result
                    }).then(function () {
                        //console.log('jQuery bind complete');
                    });
                }
                
                reader.readAsDataURL(this.files[0]);
            
            });
            
            $("#crop-done-btn-logo").on('click', function (ev) {
                resize.croppie('result', {
                    type: 'canvas',
                    size: 'viewport'
                }).then(function (img) {
                    $("#img-preview-thumbnail").attr("src", img)
                    $("input[name=cropped_image").val(img)
                    $("#cropper-modal").modal("hide")
                });
            });


              $('#img-picker-cover').on('change', function () {
                    var reader = new FileReader();
                
                    $("#cropper-modal-cover").modal("show")
                
                    reader.onload = function (e) {
                        resizecover.croppie('bind', {
                            url: e.target.result
                        }).then(function () {
                        });
                    }
                    
                    reader.readAsDataURL(this.files[0]);
                
                });
                
                $("#crop-done-btn-cover").on('click', function (ev) {
                    resizecover.croppie('result', {
                        type: 'canvas',
                        size: 'viewport'
                    }).then(function (img) {
                        $("#img-preview-cover").attr("src", img)
                        $("input[name=cropped_image_cover").val(img)
                        $("#cropper-modal-cover").modal("hide")
                    });
                });

    </script>
@endpush

