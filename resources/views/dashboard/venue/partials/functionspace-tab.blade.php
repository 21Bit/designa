<div class="nav-tabs-custom">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" @if(Request::get('tab')  == "") class="active" @endif ><a href="{{ route('dashboard.venue.show',$venue->slug) }}" aria-controls="home" role="tab"><i class="fa fa-building"></i> Function Space</a></li>
        <li role="presentation" @if(Request::get('tab')  == "gallery") class="active" @endif ><a  href="{{ route('dashboard.venue.show',$venue->slug) }}?tab=gallery" aria-controls="home" role="tab"><i class="fa fa-file-image-o"></i> Gallery</a></li>
        <li role="presentation" @if(Request::get('tab')  == "comment") class="active" @endif ><a href="{{ route('dashboard.venue.show',$venue->slug) }}?tab=comment" aria-controls="profile" role="tab"><i class="fa fa-comment-o"></i> Comments</a></li>
        <li role="presentation" @if(Request::get('tab')  == "heart") class="active" @endif ><a href="{{ route('dashboard.venue.show',$venue->slug) }}?tab=heart" aria-controls="messages" role="tab"><i class="fa fa-heart-o"></i> Saved</a></li>
    </ul>
</div>