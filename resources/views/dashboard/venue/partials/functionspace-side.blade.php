<img src="{{ $venue->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder250x200.png') }}" class="img img-responsive img-thumbnail mb-2" alt="">
{{-- <img src="{{ $venue->image('cover')->path('covers', '/images/placeholders/placeholder.png') }}" class="img img-responsive img-thumbnail mb-2" alt=""> --}}
<div class="box mb-3">
    <div class="box-body">
        <div class="row">
            <div class="col-xs-4">
                <a target="_blank" href="{{ route('site.venue.show', $venue->slug)  }}" class="btn btn-xs btn-block"><i class="fa fa-eye"></i>
                    Preview
                </a>
            </div>
            <div class="col-xs-4">
                <a href="{{ route('dashboard.venue.edit', $venue->id)  }}?redirect={{route('dashboard.venue.show', $venue->slug)}}" class="btn btn-xs btn-block"><i class="fa fa-pencil"></i>
                    Edit
                </a>
            </div>
            <div class="col-xs-4">
                    <a  data-type="page" data-redirect="{{ route('dashboard.venue.index') }}" data-buttons="delete" data-id="{{ $venue->id }}" data-url="{{ route('dashboard.venue.destroy', $venue->id) }}" class="btn btn-xs btn-block"><i class="fa fa-trash"></i> Delete</a>
            </div>
        </div>
    </div>
</div>
<div class="box mb-3">
    <div class="box-header with-border">
        <h3 class="box-title">Information</h3>
        <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        <strong>
            <a class="mb-0" href="{{ route('dashboard.company.show', $venue->company->slug) }}">
                {{ $venue->company->name }}
            </a>
        </strong>
        <p class="text-muted">
            Company
        </p>
        <hr>

        <strong>
            @foreach($venue->caters as $category)
                {{ $category->name }}@if(!$loop->last), @endif
            @endforeach
        </strong>
        <p class="text-muted">
            Category
        </p>
        <hr>

        <strong>
            @foreach($venue->categoryType('venue_type')->get() as $type)
                {{ $type->name }}@if(!$loop->last), @endif
            @endforeach
        </strong>
        <p class="text-muted">
            Type
        </p>
        <hr>

        <strong>
            @foreach($venue->categoryType('catering_option')->get() as $catering_option)
                {{ $catering_option->name }}@if(!$loop->last), @endif
            @endforeach
        </strong>
        <p class="text-muted">
            Catering Options
        </p>
        <hr>

        <strong>
            @foreach($venue->categoryType('beverage_option')->get() as $beverage_option)
                {{ $beverage_option->name }}@if(!$loop->last), @endif
            @endforeach
        </strong>
        <p class="text-muted">
            Beverage Options
        </p>
        <hr>

        <strong>
            @foreach($venue->categoryType('amenity')->get() as $amenity)
                {{ $amenity->name }}@if(!$loop->last), @endif
            @endforeach
        </strong>
        <p class="text-muted">
            Amenities
        </p>
        <hr>

        

        <strong>
            {{ $venue->capacity }} Persons
        </strong>
        <p class="text-muted">
            Capacity
        </p>
        
    </div>
</div>

<div class="box mb-3">
    <div class="box-header with-border">
        Contact Information
        <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        <strong>
            {{ $venue->location }}
        </strong>
        <p class="text-muted">
            Location
        </p>
        <hr>

        <strong>
            {{ $venue->contact_number }}
        </strong>
        <p class="text-muted">
            Contact Number
        </p>
        <hr>

        <strong>
            {{ $venue->email }}
        </strong>
        <p class="text-muted">
            Email Number
        </p>
    </div>
</div>
<div class="box mb-3">
    <div class="box-header with-border">
        Description
        <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        {!! $venue->description !!}
    </div>
</div>

<div class="box mb-3">

    <div class="box-body">
        @foreach($venue->colors as $color)
        <span class='color-span'
            style="background-color:{{ $color->desription }}">&nbsp;&nbsp;</span>
        @endforeach
    </div>
</div>
        