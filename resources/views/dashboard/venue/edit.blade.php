@extends('dashboard.includes.layouts.main')

@section('page_title', 'Edit Venue')

@section("content-hearder")
<h1>
    Venue
</h1>
<ol class="breadcrumb">
    <li><a href="{{ route('dashboard.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="{{ route('dashboard.venue.index') }}">Venue</a></li>
    <li class="active">Edit Venue</li>
</ol>
@endsection

@section('content')
@include("dashboard.includes.alerts.error")
<form action="{{ route('dashboard.venue.update', $venue->id) }}" id='form-with-loading' method="post" enctype="multipart/form-data">
    @csrf
    @method('PUT')

    <div class="text-right mb-2">
        <button class="btn btn-warning" type="submit" @if(!Auth::user()->is_superadmin) @if($venue->is_lock) disabled @endif  @endif><i class="fa fa-save"></i> Save</button>
        <a href="{{ route('dashboard.venue.show', $venue->slug) }}" class="btn btn-primary"><i class="fa fa-save"></i> Manager</a>
        <a href="{{ request('redirect') ?? route('dashboard.venue.index') }}" class="btn btn-default"><i class="fa fa-ban"></i> Cancel</a>
    </div>
    <div class="box">
        <div class="box-body">
            <div class="row">
                  <div class="col-sm-3">
                    <p>
                        <label for="">Status *</label>
                        <select name="is_published" id="" class="form-control" @if(!Auth::user()->is_superadmin) @if($venue->is_lock) disabled @endif  @endif>
                            <option value="1">Published</option>
                            <option value="0" @if($venue->is_published == 0) selected @endif>Draft</option>
                        </select>
                    </p>
                    <p>
                        <input type="checkbox" data-type="icheck" name="proceed" id="proceed"> 
                        <label for="proceed" style="font-weight: normal">Proceed to Manage</label>
                    </p>
                    @if(Auth::user()->is_superadmin)
                        <p>
                            <input type="checkbox" {{ $venue->is_lock == 1 ? 'checked' : '' }} 
                                data-type="icheck" value="1" name="is_lock" id="is_lock">

                            <label for="is_lock" style="font-weight: normal">Lock Status</label>                            
                        </p>
                        <p>
                            <input type="checkbox" {{ $venue->featured == 1 ? 'checked' : '' }} 
                                data-type="icheck" value="1" name="featured" id="featured">

                            <label for="featured" style="font-weight: normal">Feature Status</label>                            
                        </p>
                    @endif
                    <p>
                        {{-- <label for="">Thumbnail Image</label> <small>(250x250)</small>
                        <input type="file" style="display: none" id="thumbnail" name="thumbnail"> <br />
                        <img src="{{ $venue->image('thumbnail')->path('thumbnails', 'https://via.placeholder.com/250') }}" class="img img-responsive" alt="">  --}}

                        {{-- <label for="">Thumbnail Image</label> <small>(250x250)</small>
                        <input type="file" style="display: none" id="thumbnail" name='thumbnail' data-type="thumbnail-picker" />
                        <label for="thumbnail" class="btn btn-default btn-xs d-block mb-2"><i class="fa fa-file-image-o"></i> Change Thumbnail</label>
                        <img id="thumbnail-image" class='w-100' src="{{ $venue->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png') }}">                  --}}
                          <label class="text-muted" for="">Thumbnail Logo</label> <small><i>800x650</i></small> <br />
                        <input type="file"  id="img-picker-logo"  style="display:none"  name="thumbnail" accept="image/*">
                        <input type="hidden" name="cropped_image">
                        <label for="img-picker-logo" class="btn btn-default btn-xs mb-2"> Choose Image</label>
                        <img src="{{ $venue->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png') }}" id="img-preview-thumbnail" class="img img-responsive" alt="">
                    </p>
                    
                     <p>
                        <label class="text-muted" for="">Cover Picture</label> <small><i>1920x450</i></small> <br />
                        <input type="hidden" name="cropped_image_cover">
                        <input type="file" style="display:none" id="img-picker-cover" name="company_cover_picture" data-type="image-picker" data-preview="#preview-cover" data-preview-type='img'  accept="image/*">
                        <label for="img-picker-cover" class="btn btn-default btn-xs mb-3"> Change Cover</label>
                        <img src="{{ $venue->image('cover')->path('covers', '/images/placeholders/placeholder.png') }}" id="img-preview-cover" class="img img-responsive" alt="">
                    </p>
                  
                </div>
                <div class="col-sm-5">
                    <p>
                        <label for="">Name *</label>
                        <input type="text" autofocus name="name" value="{{ $venue->name }}" required class="form-control input-sm">
                    </p>
                    <p>
                        <label for="">Location *</label>
                        {{-- <input type="text" name="location" value="{{ $venue->location }}" required class="form-control input-sm"> --}}
                        <geolocation-autocomplete standalone="yes" defaultlocation="{{ $venue->location }}" defaultlatitude="{{ $venue->latitude }}" defaultlongitude="{{ $venue->longitude }}" styleclass='form-control'></geolocation-autocomplete>
                    </p>
                    {{-- <p>
                        <label for="">Price *</label>
                        <input type="number" min="1" name="price" value="{{ $venue->price }}" required class="form-control input-sm">
                    </p> --}}
                    <p>
                        <label for="">Capacity *</label>
                        <input type="number" min="1" name="capacity" value="{{ $venue->capacity }}" required class="form-control input-sm">
                    </p>
                    <p>
                        <label for="">Max Seated *</label>
                        <input type="number"  min="1" name="max_seated" required value="{{ $venue->max_seated }}" class="form-control input-sm">
                    </p>
                    <p>
                        <label for="">Max Standing *</label>
                        <input type="number"  min="1" name="max_standing" required value="{{ $venue->max_standing }}" class="form-control input-sm">
                    </p>
                     <p>
                        <label for="">Venue Email Address *</label>
                        <input type="text" name="email"  value="{{ $venue->email }}" required class="form-control input-sm">
                    </p>
                    <p>
                        <label for="">Venue Contact Number *</label>
                        <input type="text" name="contact_number"  value="{{ $venue->contact_number }}" required class="form-control input-sm">
                    </p>
                    <p>
                        <label for="">Desciption</label>
                        <textarea name="description" id="texteditor" cols="30" rows="10" class="form-control">{!! $venue->description !!}</textarea>
                    </p>
                     <p>
                        <label for="">Operation Schedules *</label> <br />
                        <small><i>Note: leaving blank meaning no operation on that day</i></small>
                        <div class="row mb-2 mt-0">
                            <div class="col-xs-4"></div>
                            <div class="col-xs-4"><small>Opening Time</small></div>
                            <div class="col-xs-4"><small>Closing Time</small></div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-xs-4 pt-1"><b>Monday</b></div>
                            <div class="col-xs-4">
                                <input type="time" name='timein[]' value="{{ $operationHours[0][0] ?? '' }}" class='form-control input-sm'>
                            </div>
                            <div class="col-xs-4">
                                <input type="time" name="timeout[]" value="{{ $operationHours[0][1] ?? '' }}" class='form-control input-sm'>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-xs-4 pt-1"><b>Tuesday</b></div>
                            <div class="col-xs-4">
                                <input type="time" name='timein[]' value="{{ $operationHours[1][0] ?? '' }}" class='form-control input-sm'>
                            </div>
                            <div class="col-xs-4">
                                <input type="time" name="timeout[]" value="{{ $operationHours[1][1] ?? '' }}" class='form-control input-sm'>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-xs-4 pt-1"><b>Wednesday</b></div>
                            <div class="col-xs-4">
                                <input type="time" name='timein[]' value="{{ $operationHours[2][0] ?? '' }}" class='form-control input-sm'>
                            </div>
                            <div class="col-xs-4">
                                <input type="time" name="timeout[]" value="{{ $operationHours[2][1] ?? '' }}" class='form-control input-sm'>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-xs-4 pt-1"><b>Thursday</b></div>
                            <div class="col-xs-4">
                                <input type="time" name='timein[]' value="{{ $operationHours[3][0] ?? '' }}" class='form-control input-sm'>
                            </div>
                            <div class="col-xs-4">
                                <input type="time" name="timeout[]" value="{{ $operationHours[3][1] ?? '' }}" class='form-control input-sm'>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-xs-4 pt-1"><b>Friday</b></div>
                            <div class="col-xs-4">
                                <input type="time" name='timein[]' value="{{ $operationHours[4][0] ?? '' }}" class='form-control input-sm'>
                            </div>
                            <div class="col-xs-4">
                                <input type="time" name="timeout[]" value="{{ $operationHours[4][1] ?? '' }}" class='form-control input-sm'>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-xs-4 pt-1"><b>Saturday</b></div>
                            <div class="col-xs-4">
                                <input type="time" name='timein[]' value="{{ $operationHours[5][0] ?? '' }}" class='form-control input-sm'>
                            </div>
                            <div class="col-xs-4">
                                <input type="time" name="timeout[]" value="{{ $operationHours[5][1] ?? '' }}" class='form-control input-sm'>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-xs-4 pt-1"><b>Sunday</b></div>
                            <div class="col-xs-4">
                                <input type="time" name='timein[]' value="{{ $operationHours[6][0] ?? '' }}" class='form-control input-sm'>
                            </div>
                            <div class="col-xs-4">
                                <input type="time" name="timeout[]" value="{{ $operationHours[6][1] ?? '' }}" class='form-control input-sm'>
                            </div>
                        </div>
                        <!-- <input type="text" name="operating_hours" required class="form-control input-sm"> -->
                    </p>
                    
                </div>
                <div class="col-sm-4">
                    <p>
                        <label for="" class="mb-0">Category</label>
                        <select name="categories[]" class="form-control" data-type="select2"  multiple>
                            @foreach($categories as $category)
                                <option  @if( !in_array($category->id, Auth::user()->caterPluck('id'))) disabled @endif   @if( !in_array($category->id, Auth::user()->caterPluck('id'))) disabled @endif   @if(in_array($category->id, $venue->categoryType('cater')->pluck('id')->toArray())) selected @endif value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </p>
                     <p>
                        <label for="" class="mb-0">Type</label>
                        <select name="types[]" class="form-control" data-type="select2" multiple>
                            @foreach($types as $type)
                                <option  @if(in_array($type->id, $venue->categoryType('venue_type')->pluck('id')->toArray())) selected @endif  value="{{ $type->id }}">{{ $type->name }}</option>
                            @endforeach
                        </select>
                    </p>
                    <p>
                        <label for="" class="mb-0">Catering Options</label>
                        <select name="catering_options[]" class="form-control" data-type="select2"  multiple>
                            @foreach($catering_options as $catering_option)
                                <option  @if(in_array($catering_option->id, $venue->categoryType('catering_option')->pluck('id')->toArray())) selected @endif  value="{{ $catering_option->id }}">{{ $catering_option->name }}</option>
                            @endforeach
                        </select>
                    </p>
                    <p>
                        <label for="" class="mb-0">Beverage Options</label>
                        <select name="beverage_options[]" class="form-control" data-type="select2"  multiple>
                            @foreach($beverage_options as $beverage_option)
                                <option  @if(in_array($beverage_option->id, $venue->categoryType('beverage_option')->pluck('id')->toArray())) selected @endif  value="{{ $beverage_option->id }}">{{ $beverage_option->name }}</option>
                            @endforeach
                        </select>
                    </p>
                    <p>
                        <label for="" class="mb-0">Amenities</label>
                        <select name="amenities[]" class="form-control" data-type="select2"  multiple>
                            @foreach($amenities as $amenity)
                                <option @if(in_array($amenity->id, $venue->categoryType('amenity')->pluck('id')->toArray())) selected @endif  value="{{ $amenity->id }}">{{ $amenity->name }}</option>
                            @endforeach
                        </select>
                    </p>
                    <p>
                        <label for="" class="mb-0">Average Cost per Heads</label>
                        <select name="average_cost_per_heads[]" class="form-control" data-type="select2"  multiple>
                            @foreach($average_cost_per_heads as $average_cost_per_head)
                                <option @if(in_array($average_cost_per_head->id, $venue->categoryType('average_cost_per_head')->pluck('id')->toArray())) selected @endif  value="{{ $average_cost_per_head->id }}">{{ $average_cost_per_head->name }}</option>
                            @endforeach
                        </select>
                    </p>
                    @if(Auth::user()->id == 197)
                    <p>
                        <label for="" class="mb-0">Company</label>
                        <select name="company_id" class="form-control" data-type="select2">
                            @foreach($suppliers as $supplier)
                            <option @if($supplier->id == $venue->company_id ) selected @endif  value="{{ $supplier->id }}">{{ $supplier->name }}</option>
                            @endforeach
                        </select>
                    </p>
                    @endif

                    <p>
                        <label for="">Base Layer</label>
                        <input type="file" name="base_layer">
                    </p>
                    @if($venue->base_layer)
                        <div class="well p-2" style="position: relative">
                            <label for="">
                                {{  $venue->base_layer->model_name }}
                            </label> <br>
                            <small>Uploaded at {{$venue->base_layer->created_at }}</small>
                            <div style="position: absolute; top:5px; right:5px;">
                                <span class="pull-right">
                                    <button class="btn btn-danger"><i class="fa fa-remove"></i></button>
                                </span>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                            
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</form>

 <div class="modal" id="cropper-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg mw-100" style="width:100%;height: 100%; margin:0; padding:0;" role="document">

            <div class="modal-body text-center mw-100">
                <label>Crop Image</label>
                <div id="crop-picture-container-logo" class="mw-100"></div>
                <button id="crop-done-btn-logo" class="btn btn-primary btn-lg">Done</button>
            </div>

    </div>
</div>

<div class="modal" id="cropper-modal-cover" tabindex="-1" role="dialog" style="overflow: auto">
    <div class="modal-dialog modal-lg" style="width:100%" role="document">
        <div class="modal-content">
            <div class="modal-body text-center" style="overflow:auto; width:100%">
                <label>Crop Cover</label>
                <div id="crop-picture-container-cover"></div>
                <button id="crop-done-btn-cover" class="btn btn-primary btn-lg">Done</button>
            </div>
        </div>
    </div>
</div>


@endsection

@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.4/croppie.css" integrity="sha256-M8o9uqnAVROBWo3/2ZHSIJG+ZHbaQdpljJLLvdpeKcI=" crossorigin="anonymous" />
@endpush

@push('scripts')
    <script src="{{ asset('/dashboard/ckeditor/ckeditor.js')}} "></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.4/croppie.min.js" integrity="sha256-bQTfUf1lSu0N421HV2ITHiSjpZ6/5aS6mUNlojIGGWg=" crossorigin="anonymous"></script>
    <script>

        $('#cropper-modal-cover').on('shown.bs.modal', function () {
            $('body').addClass('zoomout')
        })          
        $('#cropper-modal-cover').on('hidden.bs.modal', function () {
            $('body').removeClass('zoomout')
        })          

         $('#img-picker-cover').on('change', function () {
            var reader = new FileReader();
        
            $("#cropper-modal-cover").modal("show")
        
            reader.onload = function (e) {
                resizecover.croppie('bind', {
                    url: e.target.result
                }).then(function () {
                });
            }
            
            reader.readAsDataURL(this.files[0]);
        
        });
        
        $("#crop-done-btn-cover").on('click', function (ev) {
            resizecover.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function (img) {
                $("#img-preview-cover").attr("src", img)
                $("input[name=cropped_image_cover").val(img)
                $("#cropper-modal-cover").modal("hide")
            });
        });

        var resizecover = $('#crop-picture-container-cover').croppie({
            enableExif: true,
            enableOrientation: true,
            viewport: { // Default { width: 100, height: 100, type: 'square' } 
                width: 1920,
                height: 450,
                type: 'square'
            },
            enforceBoundary: false,
            boundary: {
                width: 1980,
                height: 600
            }
        });



        CKEDITOR.replace( 'texteditor', {
            toolbar: [
                { 
                name: 'document', 
                items: [ '-', 'NewPage', 'Preview', '-', 'Templates' ] 
                }, 
                [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo', ],
                { 
                name: 'basicstyles', 
                items: [ 'Bold', 'Italic']
                }
            ],
            enterMode : CKEDITOR.ENTER_BR,
            shiftEnterMode: CKEDITOR.ENTER_P,
            menubar: false,
            branding:false,
            forced_root_block : false,
        });

        var resize = $('#crop-picture-container-logo').croppie({
                enableExif: true,
                enableOrientation: true,
                viewport: { // Default { width: 100, height: 100, type: 'square' } 
                    width: 1200,
                    height: 850,
                    type: 'square'
                },
                boundary: {
                    width: 1300,
                    height: 900
                }
            });
            
            
            $('#img-picker-logo').on('change', function () {
                var reader = new FileReader();
            
                $("#cropper-modal").modal("show")
            
                reader.onload = function (e) {
                    resize.croppie('bind', {
                        url: e.target.result
                    }).then(function () {
                        //console.log('jQuery bind complete');
                    });
                }
                
                reader.readAsDataURL(this.files[0]);
            
            });
            
            $("#crop-done-btn-logo").on('click', function (ev) {
                resize.croppie('result', {
                    type: 'canvas',
                    size: 'viewport'
                }).then(function (img) {
                    $("#img-preview-thumbnail").attr("src", img)
                    $("input[name=cropped_image").val(img)
                    $("#cropper-modal").modal("hide")
                });
            });
    </script>
@endpush