@extends('dashboard.includes.layouts.main')

@section('page_title', $venue->name)

@section("content-hearder")
    <h1>
        Venues
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.home') }}">Home</a></li>
        <li><a href="{{ route('dashboard.venue.index') }}">Venues</a></li>
        <li class="active">{{ $venue->name }}</li>
    </ol>
@endsection

@section('content')
<div class="row">
    <div class="col-sm-3">
        <img src="{{ $venue->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder250x200.png') }}"  class="w-100 img-thumbnail mb-2" alt="">
        <div class="box mb-3">
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-4">
                        <a target="_blank" href="{{ route('site.venue.show', $venue->slug)  }}" class="btn btn-xs btn-block"><i class="fa fa-eye"></i>
                            Preview
                        </a>
                    </div>
                    <div class="col-xs-4">
                        <a href="{{ route('dashboard.venue.edit', $venue->id)  }}?redirect={{route('dashboard.venue.show', $venue->slug)}}" class="btn btn-xs btn-block"><i class="fa fa-pencil"></i>
                            Edit
                        </a>
                    </div>
                    <div class="col-xs-4">
                        <a  data-type="page" data-redirect="{{ route('dashboard.venue.index') }}" data-buttons="delete" data-id="{{ $venue->id }}" data-url="{{ route('dashboard.venue.destroy', $venue->id) }}" class="btn btn-xs btn-block"><i class="fa fa-trash"></i> Delete</a>
                    </div>
                </div>
            </div>
        </div>
        
        @include('dashboard.venue.partials.side-information', ['venue' => $venue, 'explodedTimes'=> $explodedTimes])
        
        
    </div>
    <div class="col-sm-9">
        <div class="box">
            <div class="box-body">
                <h1 class="orange mt-0">{{ $venue->name }}</h1>
            </div>
        </div>
        <div class="box no-border">
            <div class="nav-tabs-custom">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation"><a href="{{ route('dashboard.venue.show',$venue->slug) }}" aria-controls="home" role="tab"><i class="fa fa-building"></i> Function Space</a></li>
                <li role="presentation"><a href="{{ route('dashboard.venue.show',$venue->slug) }}?tab=gallery" aria-controls="home" role="tab"><i class="fa fa-file-image-o"></i> Gallery</a></li>
                <li role="presentation"  class="active"><a href="{{ route('dashboard.venue.show',$venue->slug) }}?tab=reviews" aria-controls="profile" role="tab"><i class="fa fa-comment-o"></i> Reviews</a></li>
                <li role="presentation"><a href="{{ route('dashboard.venue.show',$venue->slug) }}?tab=heart" aria-controls="messages" role="tab"><i class="fa fa-heart-o"></i> Saved</a></li>
            </ul>
            </div>
            <div class="box-body">
                 <review-component  type="venue" :id="{{ $venue->id }}"></review-component>
            </div>
        </div>
    </div>
</div>
@endsection