@extends('dashboard.includes.layouts.main')

@section('page_title', $venue->name)

@section("content-hearder")
    <h1>
        Venues
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.home') }}">Home</a></li>
        <li><a href="{{ route('dashboard.venue.index') }}">Venues</a></li>
        <li class="active">{{ $venue->name }}</li>
    </ol>
@endsection

@section('content')
<div class="row">
    <div class="col-sm-3">
        
        @include('dashboard.venue.partials.side-information', ['venue' => $venue, 'explodedTimes'=> $explodedTimes])
        
        
    </div>
    <div class="col-sm-9">
        <div class="box">
            <div class="box-body">
                <h1 class="orange mt-0">{{ $venue->name }}</h1>
            </div>
        </div>
        <div class="box no-border">

            @include('dashboard.venue.partials.functionspace-tab')
            
            <div class="box-body">
                <div id="dropzone">
                    <form action="{{ route('dashboard.venue.uploadgallery', $venue->id) }}" class="dropzone" type="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="fallback">
                            <input name="file" type="file" multiple />
                        </div>
                        
                    </form>
                         
                </div>
                <div class="inspiration-grid mt-3">
                    @foreach($gallery as $image)
                        <div class="inspiration-item" id="div-{{ $image->id }}">
                            <div class="image-gallery">
                                <a href="/images/gallery/{{ $image->path }}" data-lightbox="PORTFOLIO">
                                    <img src="/images/gallery/{{ $image->path }}" class="img img-responsive mb-4" alt="gallery-image" class="margin" data-dz-remove>
                                </a>
                                <a href='#'  data-type="div" data-buttons="delete" data-id="{{ $image->id }}" data-url="{{ route('dashboard.gallery.destroy', $image->id) }}"   class="btn btn-md trash-btn"><i class="fa fa-trash"></i></a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.css" integrity="sha256-0Z6mOrdLEtgqvj7tidYQnCYWG3G2GAIpatAWKhDx+VM=" crossorigin="anonymous" />
@endpush
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js" integrity="sha256-cs4thShDfjkqFGk5s2Lxj35sgSRr4MRcyccmi0WKqCM=" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function(){
            $("div#dropzone").dropzone({ 
                url: "{{ route('dashboard.venue.uploadgallery', $venue->id) }}",
                type: 'POST',
                dataType: 'JSON',
                addRemoveLinks : true,
                dictRemoveFile: 'Remove',
                removedfile: function(file) {
                    var name = file.name;        
                    $.ajax({
                        type: 'DELETE',
                        url: '',
                        data: "",
                        dataType: 'json'
                    });
                    var _ref;
                    return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;        
                }
                acceptedFiles:".jpg,.png,.JPG,,PNG",
                maxFilesize: 5,
                dictDefaultMessage: '<span class="text-center"><span class="font-lg visible-xs-block visible-sm-block visible-lg-block"><span class="font-lg"><i class="fa fa-caret-right text-danger"></i> Drop files <span class="font-xs">to upload</span></span><span>&nbsp&nbsp<h4 class="display-inline"> (Or Click)</h4></span>',
                dictResponseError: 'Error uploading file!',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            });
         
        })
    </script>
@endpush