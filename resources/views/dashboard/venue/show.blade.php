@extends('dashboard.includes.layouts.main')

@section('page_title', $venue->name)

@section("content-hearder")
    <h1>
        Venues
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.home') }}">Home</a></li>
        <li><a href="{{ route('dashboard.venue.index') }}">Venues</a></li>
        <li class="active">{{ $venue->name }}</li>
    </ol>
@endsection

@section('content')
<div class="row">
    <div class="col-sm-3">
        <img src="{{ $venue->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png') }}" class="img img-responsive img-thumbnail mb-2" alt="">
        <img src="{{ $venue->image('cover')->path('covers', '/images/placeholders/placeholder.png') }}" class="img img-responsive img-thumbnail mb-2" alt="">
        <div class="box mb-3">
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-4">
                        <a target="_blank" href="{{ route('site.venue.show', $venue->slug)  }}" class="btn btn-xs btn-block"><i class="fa fa-eye"></i>
                            Preview
                        </a>
                    </div>
                    <div class="col-xs-4">
                        <a href="{{ route('dashboard.venue.edit', $venue->id)  }}?redirect={{route('dashboard.venue.show', $venue->slug)}}" class="btn btn-xs btn-block"><i class="fa fa-pencil"></i>
                            Edit
                        </a>
                    </div>
                    <div class="col-xs-4">
                      <a  data-type="page" data-redirect="{{ route('dashboard.venue.index') }}" data-buttons="delete" data-id="{{ $venue->id }}" data-url="{{ route('dashboard.venue.destroy', $venue->id) }}" class="btn btn-xs btn-block"><i class="fa fa-trash"></i> Delete</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="box mb-3">
            <div class="box-header with-border">
                Information
                <div class="box-tools">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <strong>
                    <a class="mb-0" href="{{ route('dashboard.company.show', $venue->company->slug) }}">
                        {{ $venue->company->name }}
                    </a>
                </strong>
                <p class="text-muted">
                    Company
                </p>
                <hr>

                <strong>
                    @foreach($venue->caters as $category)
                        {{ $category->name }}@if(!$loop->last), @endif
                    @endforeach
                </strong>
                <p class="text-muted">
                    Category
                </p>
                <hr>

                <strong>
                    @foreach($venue->categoryType('venue_type')->get() as $type)
                        {{ $type->name }}@if(!$loop->last), @endif
                    @endforeach
                </strong>
                <p class="text-muted">
                    Type
                </p>
                <hr>

                <strong>
                    @foreach($venue->categoryType('catering_option')->get() as $catering_option)
                        {{ $catering_option->name }}@if(!$loop->last), @endif
                    @endforeach
                </strong>
                <p class="text-muted">
                    Catering Options
                </p>
                <hr>

                <strong>
                    @foreach($venue->categoryType('beverage_option')->get() as $beverage_option)
                        {{ $beverage_option->name }}@if(!$loop->last), @endif
                    @endforeach
                </strong>
                <p class="text-muted">
                    Beverage Options
                </p>
                <hr>

                <strong>
                    @foreach($venue->categoryType('amenity')->get() as $amenity)
                        {{ $amenity->name }}@if(!$loop->last), @endif
                    @endforeach
                </strong>
                <p class="text-muted">
                    Amenities
                </p>
                <hr>

               

                <strong>
                    {{ $venue->capacity }} Persons
                </strong>
                <p class="text-muted">
                    Capacity
                </p>
                
            </div>
        </div>

        <div class="box mb-3">
            <div class="box-header with-border">
                Operating Hours
                <div class="box-tools">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <strong>
                    {{ $venue->location }}
                </strong>
                <p class="text-muted">
                    Location
                </p>
            </div>
        </div>
        <div class="box mb-3">
            <div class="box-header with-border">
                Contact Information
                <div class="box-tools">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <strong>
                    {{ $venue->location }}
                </strong>
                <p class="text-muted">
                    Location
                </p>
                <hr>

                <strong>
                    {{ $venue->contact_number }}
                </strong>
                <p class="text-muted">
                    Contact Number
                </p>
                <hr>

                <strong>
                    {{ $venue->email }}
                </strong>
                <p class="text-muted">
                    Email Number
                </p>
            </div>
        </div>
        <div class="box mb-3">
            <div class="box-header with-border">
                Description
                <div class="box-tools">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                {!! $venue->description !!}
            </div>
        </div>

        <div class="box mb-3">
            <div class="box-body">
                @foreach($venue->colors as $color)
                <span class='color-span'
                    style="background-color:{{ $color->description }}">&nbsp;&nbsp;</span>
                @endforeach
            </div>
        </div>
        
    </div>
    <div class="col-sm-9">
        <div class="box">
            <div class="box-body">
                <h1 class="orange mt-0">{{ $venue->name }}</h1>
            </div>
        </div>
        <div class="box no-border">
            <div class="nav-tabs-custom">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" ><a href="#home" aria-controls="home" role="tab"><i class="fa fa-building"></i> Function Space</a></li>
                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab"><i class="fa fa-file-image-o"></i> Gallery</a></li>
                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab"><i class="fa fa-comment-o"></i> Comments</a></li>
                <li role="presentation"><a href="#messages" aria-controls="messages" role="tab"><i class="fa fa-heart-o"></i> Saved</a></li>
            </ul>
            </div>
            <div class="box-body">
                <div>
                    <form action="{{ route('dashboard.venue.uploadgallery', $venue->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="file" style="display: none" accept="image/*" name="gallery" id="gallery-input">
                        <label for="gallery-input" class="btn btn-default btn-sm"><i class="fa fa-upload"></i> Upload</label>
                    </form>
                </div>
                <div class="row mt-3">
                    @foreach($gallery as $image)
                        <div class="col-sm-3">
                            <img src="/images/thumbnails/{{ $image->path }}" class="img img-responsive mb-4" alt="gallery-image" class="margin">
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection