@extends('dashboard.includes.layouts.main')

@section('page_title', 'Venues')

@section("content-hearder")
        <h1>
            Venues
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Venues</li>
        </ol>
@endsection

@section('content')
    <div class="row">
        <form>
            @if(Auth::user()->is_superadmin)
                <div class="col-sm-3 mb-2">
                    <select class="form-control input-sm" name="feature">
                        <option value="0">- Featured -</option>
                        <option value="1" {{ Request::get('feature') == 1 ? 'selected' : '' }}>Featured</option>                                  
                    </select>
                </div>

                <div class="col-sm-3 mb-2">
                    <select class="form-control input-sm" name="status">
                        <option value="" selected>- Filter Status -</option>
                        <option value="">All</option>    
                        <option value="1" {{ Request::get('status') == 1 ? 'selected' : '' }}>Publish</option>
                        <option value="2" {{ Request::get('status') == 2 ? 'selected' : '' }}>Draft</option>                                    
                    </select>
                </div>
                
                <div class="col-sm-3 mb-2">
                    <select class="form-control input-sm" name="lock">
                        <option value="0">- Lock -</option>
                        <option value="1" {{ Request::get('lock') == 1 ? 'selected' : '' }}>Lock</option>                           
                    </select>
                </div>
            @endif
            <div class="col-sm-3">               
                <select class="form-control input-sm" name="category">
                    <option value="">- all category - </option>
                    @foreach($categories as $category)
                        <option @if(Request::get('search') == $category->id) selected @endif value="{{ $category->id }}" @if(Request::get('category') == $category->id) selected @endif >{{ $category->name }}</option>
                    @endforeach
                </select>                        
            </div>
            @if(Auth::user()->is_superadmin)
                <div class="col-sm-3">
                    <select class="form-control input-sm" name="supplier">
                        <option value="">- all suppliers - </option>
                        @foreach($suppliers as $supplier)
                            <option @if(Request::get('supplier') == $supplier->id) selected @endif value="{{ $supplier->id }}" @if(Request::get('supplier') == $supplier->id) selected @endif >{{ $supplier->name }}</option>
                        @endforeach
                    </select>
                </div>
            @endif
            <div class="col-sm-6">
                <!-- <a href="{{ route('dashboard.venue.create') }}" class="btn btn-default btn-sm"><i class="fa fa-plus"></i> New</a>
                <button href="#" id="delete-all-btn" class="btn btn-default btn-sm"><i class="fa fa-trash"></i> Delete</button> -->
            
                
                    <div class="input-group">
                        <input type="text" class="form-control input-sm" name="search" value="{{ Request::get('search') }}" placeholder=" Search Venue.." aria-label="...">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-search"></i> Search</button>
                            <a href="{{ route('dashboard.venue.create') }}" class="btn btn-default btn-sm"><i class="fa fa-plus"></i> New</a>
                            @if(
                                Request::get('feature') || Request::get('status') || Request::get('lock')||
                                Request::get('category') || Request::get('supplier') || Request::get('search')
                                )
                                <a href="{{ route('dashboard.venue.index') }}" class="btn btn-default btn-sm"><i class="fa fa-ban"></i> Clear Filter</a>
                            @endif
                        </div>
                    </div>
                
        
            </div>
        </form>
      
    </div>
    <br />
    @forelse($venues as $venue)
        <div class="info-box venue-box" id="div-{{ $venue->id }}">
            <a href="{{ route('dashboard.venue.show', $venue->slug) }}">
                <span class="info-box-icon" style="background-image: url('{{ $venue->image('thumbnail')->path('thumbnails','/images/placeholders/placeholder250x200.png') }}'); background-position:center"></span>
            </a>
            <div class="info-box-content">
                <span class="info-box-text text-muted">{{ optional($venue->company)->name }}</span>
                <span class="info-box-title">{{ $venue->name }}</span>
                <div class="description mr-3">
                    <small>
                        <i class="fa fa-map-marker"></i> {{ $venue->location }}
                    </small>
                </div> 
                <div class="description">
                    <small> 
                        <i class="fa fa-user"></i> {{ $venue->capacity }} persons
                    </small>
                </div>
                <div class="description">
                    <small> 
                        <i class="fa fa-umbrella"></i>
                        @foreach($venue->caters as $category)
                            {{ $category->name }}@if(!$loop->last), @endif
                        @endforeach
                    </small>
                </div>
                <div class="colors">
                    @foreach($venue->colors as $color)
                        <span class='color-span' style="border-radius:0px; width: 10px; height: 10px; background-color:{{ $color->description }}">&nbsp;&nbsp;</span>
                    @endforeach
                </div>
                <div class="buttons">
                    <!-- <a href="" class="mr-3 ml-4"><i class="fa fa-list"></i> Manage</a> -->
                    <a href="{{ route('dashboard.venue.edit', $venue->id) }}" class="mr-3 ml-4"><i class="fa fa-pencil"></i> Edit</a>
                    <a href="#" data-type="div" data-buttons="delete" data-id="{{ $venue->id }}" data-url="{{ route('dashboard.venue.destroy', $venue->id) }}"><i class="fa fa-trash"></i> Delete</a>
                </div>
            </div>
        </div>
    @empty
           <h4 class="text-center text-muted mt-5">No Venue</h4>
    @endforelse
    {{-- {{ $venues->links() }} --}}
    {{ 
        $venues->appends([
            'feature' => Request::get('feature'),
            'status' => Request::get('status'),
            'lock' => Request::get('lock'),
            'supplier' => Request::get('supplier'), 
            'category' => Request::get('category'), 
            'search' => Request::get('search')
        ])->links()
    }}
@endsection