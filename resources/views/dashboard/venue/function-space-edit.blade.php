@extends('dashboard.includes.layouts.main')

@section('page_title', $venue->name)

@section("content-hearder")
    <h1>
        Venues
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.home') }}">Home</a></li>
        <li><a href="{{ route('dashboard.venue.index') }}">Venues</a></li>
        <li class="active">{{ $venue->name }}</li>
    </ol>
@endsection

@section('content')
<div class="row">
    <div class="col-sm-3">
        @include('dashboard.venue.partials.functionspace-side')
    </div>
    <div class="col-sm-9">
        <div class="box">
            <div class="box-body">
                <h1 class="orange mt-0">{{ $venue->name }}</h1>
            </div>
        </div>
        <div class="box no-border">
            @include('dashboard.venue.partials.functionspace-tab')
            <div class="box-body">
                <form action="{{ route('dashboard.functionspace.update', $functionspace->id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <input type="hidden" value="{{ $venue->id }}" name="venue_id">
                    <div class="text-right mb-3">
                    <button class="btn btn-warning btn-sm">
                        <i class="fa fa-save"></i> Save Changes
                    </button>    
                    <a href="{{ route('dashboard.venue.show',$venue->slug) }}" class="btn btn-default btn-sm" id="hide-function-space-form">
                        <i class="fa fa-ban"></i> Cancel
                    </a>    
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <p>
                                <label for="">Function Space Name *</label>
                                <input type="text" name="name" value="{{ $functionspace->name }}" class="form-control input-sm">
                            </p>
                            <div class="row">
                                <div class="col-sm-6">
                                    <p>
                                        <label for="">Max Capacity Seated *</label>
                                        <input type="number" min="1" value="{{ $functionspace->max_capacity_seated }}" name="max_capacity_seated" class="form-control input-sm">
                                    </p>
                                </div>
                                <div class="col-sm-6">
                                    <p>
                                        <label for="">Max Capacity Standing *</label>
                                        <input type="number" min="1" value="{{ $functionspace->max_capacity_standing }}" name="max_capacity_standing" class="form-control input-sm">
                                    </p>
                                </div>
                            </div>
                            <p>
                                <label for="">Description</label>
                                <textarea name="description" class="form-control" rows="5">{!! $functionspace->description !!}</textarea>
                            </p>

                            {{-- <div class="panel panel-default mb-4">
                                <div class="panel-heading">
                                    Web Asset Bundle
                                    <label for="web_asset_bundle_input" class="btn btn-sm btn-primary pull-right" style="margin-top:-5px;"><i class="fa fa-upload"></i> Upload File</label>
                                </div>
                                <div class="panel-body pb-3">
                                    <form action="{{ route('dashboard.functionspace.upload-asset-bundle', $functionspace->id) }}" style="display:none;" method="POST" enctype="multipart/form-data" >
                                        @csrf 
                                        <input type="hidden" name='type' value="web">
                                        <input type="file" id="web_asset_bundle_input" name="asset_bundle_file" class="input-file">
                                    </form>
                                    @if($functionspace->asset_bundle_web)
                                        <div id="div-{{ $functionspace->asset_bundle_web->id }}">
                                            <div class='alert bg-gray border p-3 d-inline-block mb-0' >
                                                {{ $functionspace->asset_bundle_web->model_name }}
                                                <button data-buttons="delete" data-url="{{ route('dashboard.functionspace.delete-asset-bundle', $functionspace->asset_bundle_web->id)}}" data-type="div" data-id="{{ $functionspace->asset_bundle_web->id }}" class="pull-right btn btn-xs"><i class="fa fa-remove"></i> Remove</button>
                                            </div>
                                            <small class="pl-2"><i>uploaded {{ $functionspace->asset_bundle_web->created_at->diffForHumans() }}</i></small>
                                        </div>
                                    @else
                                        <div class="pb-2"> 
                                            <i class='text-muted'> No asset bundle uploaded</i>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="panel panel-default mb-4">
                                <div class="panel-heading">
                                    Android Asset Bundle
                                    <label for="android_asset_bundle_input" class="btn btn-sm btn-primary pull-right" style="margin-top:-5px;"><i class="fa fa-upload"></i> Upload File</label>
                                </div>
                                <div class="panel-body pb-3">
                                    <form action="{{ route('dashboard.functionspace.upload-asset-bundle', $functionspace->id) }}" style="display:none;" method="POST" enctype="multipart/form-data" >
                                        @csrf 
                                        <input type="hidden" name='type' value="android">
                                        <input type="file" id="android_asset_bundle_input" name="asset_bundle_file" class="input-file">
                                    </form>
                                    @if($functionspace->asset_bundle_android)
                                        <div id="div-{{ $functionspace->asset_bundle_android->id }}">
                                            <div class='alert bg-gray border p-3 d-inline-block mb-0' >
                                                {{ $functionspace->asset_bundle_android->model_name }}
                                                <button  data-buttons="delete" data-url="{{ route('dashboard.functionspace.delete-asset-bundle', $functionspace->asset_bundle_android->id)}}" data-type="div" data-id="{{ $functionspace->asset_bundle_android->id }}" class="pull-right btn btn-xs"><i class="fa fa-remove"></i> Remove</button>
                                            </div>
                                            <small class="pl-2"><i>uploaded {{ $functionspace->asset_bundle_android->created_at->diffForHumans() }}</i></small>
                                        </div>
                                    @else
                                        <div class="pb-2"> 
                                            <i class='text-muted'> No asset bundle uploaded</i>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="panel panel-default mb-4">
                                <div class="panel-heading">
                                    iOS Asset Bundle
                                    <label for="ios_asset_bundle_input" class="btn btn-sm btn-primary pull-right" style="margin-top:-5px;"><i class="fa fa-upload"></i> Upload File</label>
                                </div>
                                <div class="panel-body pb-3">
                                    <form action="{{ route('dashboard.functionspace.upload-asset-bundle', $functionspace->id) }}" style="display:none;" method="POST" enctype="multipart/form-data" >
                                        @csrf 
                                        <input type="hidden" name='type' value="ios">
                                        <input type="file" id="ios_asset_bundle_input" name="asset_bundle_file" class="input-file">
                                    </form>
                                    @if($functionspace->asset_bundle_ios)
                                        <div id="div-{{ $functionspace->asset_bundle_ios->id }}">
                                            <div class='alert bg-gray border p-3 d-inline-block mb-0' >
                                                {{ $functionspace->asset_bundle_ios->model_name }}
                                                <button  data-buttons="delete" data-url="{{ route('dashboard.functionspace.delete-asset-bundle', $functionspace->asset_bundle_ios->id)}}" data-type="div" data-id="{{ $functionspace->asset_bundle_ios->id }}" class="pull-right btn btn-xs"><i class="fa fa-remove"></i> Remove</button>
                                            </div>
                                            <small class="pl-2"><i>uploaded {{ $functionspace->asset_bundle_ios->created_at->diffForHumans() }}</i></small>
                                        </div>   
                                    @else
                                        <div class="pb-2"> 
                                            <i class='text-muted'> No asset bundle uploaded</i>
                                        </div>
                                    @endif
                                </div>
                            </div> --}}
                        
                            
                        </div>
                        <div class="col-sm-4">
                            <p>
                                <label for="">Thumbnail Image *</label> <small>(250x250)</small>
                                <input type="file" style="display: none" id="thumbnail" name="thumbnail">
                                 <label for="thumbnail" class="btn btn-default btn-xs d-block mb-2"><i class="fa fa-file-image-o"></i> Change Thumbnail</label>
                                <img src="{{ $functionspace->image('thumbnail')->path('thumbnails', 'https://via.placeholder.com/250') }}" class="img img-responsive" alt=""> 
                            </p>
                            <p>
                                <label for="">Add Floor Plan</label>
                                <input type="file" style="display: none" id="floor_plan" name="floor_plan">
                                <label for="floor_plan" class="btn btn-default btn-xs d-block mb-2"><i class="fa fa-file-image-o"></i> Change Floor Plan</label>
                               <img src="{{ $functionspace->image('floorplan')->path('floorplans', 'https://via.placeholder.com/250xs200') }}" class="img img-responsive" alt=""> 
                            </p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
    <script>
        $(document).ready(function(){
            $('.input-file').change(function(){
                var form  = $(this).closest('form')
                form.trigger('submit')
            })
        })
    </script>
@endpush