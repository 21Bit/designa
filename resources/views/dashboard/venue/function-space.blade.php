@extends('dashboard.includes.layouts.main')

@section('page_title', $venue->name)

@section("content-hearder")
    <h1>
        Venues
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.home') }}">Home</a></li>
        <li><a href="{{ route('dashboard.venue.index') }}">Venues</a></li>
        <li class="active">{{ $venue->name }}</li>
    </ol>
@endsection

@section('content')
@if(Auth::user()->eventSpaceLimitReached())
    <div class="alert alert-danger p-2 mb-2 mt-0">
        <strong><i class="fa fa-exclamation-triangle"></i> Event Spaces number limit reached</strong>. Please upgrade your plan to continue adding your event spaces. <a href="{{ route('dashboard.profile.index', ['tab' => 'subscription'])  }}" class="text-underline"> Manage my plan.</a>
    </div>
@endif
<div class="row">
    <div class="col-sm-3">
        @include('dashboard.venue.partials.side-information', ['venue' => $venue, 'explodedTimes'=> $explodedTimes])
    </div>
    <div class="col-sm-9">
        <div class="box">
            <div class="box-body">
                <h1 class="orange mt-0">{{ $venue->name }}</h1>
            </div>
        </div>
        <div class="box no-border">

            @include('dashboard.venue.partials.functionspace-tab')

            <div class="box-body">
                @if(!Auth::user()->eventSpaceLimitReached())
                    <div class="text-right">
                        <button class="btn btn-warning btn-sm mb-3" id='show-function-space-form'><i class="fa fa-plus"></i> New Function Space</button>
                    </div>
                @endif
                <div id="function-space-form" >
                    <form action="{{ route('dashboard.functionspace.store') }}" id='form-with-loading' method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" value="{{ $venue->id }}" name="venue_id">
                        <div class="text-right mb-3">
                        <button class="btn btn-warning btn-sm">
                            <i class="fa fa-save"></i> Save
                        </button>
                        <button type="button" class="btn btn-default btn-sm" id="hide-function-space-form">
                            <i class="fa fa-ban"></i> Hide
                        </button>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <p>
                                    <label for="">Function Space Name *</label>
                                    <input type="text" name="name" required class="form-control input-sm">
                                </p>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <p>
                                            <label for="">Max Capacity Seated *</label>
                                            <input type="number" required min="1" name="max_capacity_seated" class="form-control input-sm">
                                        </p>
                                    </div>
                                    <div class="col-sm-6">
                                        <p>
                                            <label for="">Max Capacity Standing *</label>
                                            <input type="number" required  min="1" name="max_capacity_standing" class="form-control input-sm">
                                        </p>
                                    </div>
                                </div>
                                <p>
                                    <label for="">Description</label>
                                    <textarea name="description" class="form-control" rows="5"></textarea>
                                </p>

                            </div>
                            <div class="col-sm-4">
                                <p>
                                    <label for="">Thumbnail Image *</label> <small>(250x250)</small>
                                    <input type="file" style="display: none" data-type="image-picker" data-preview="#thumbnail-preview" data-preview-type="label" id="thumbnail" name="thumbnail">
                                    <label for="thumbnail" class="label-placeholder" id="thumbnail-preview"
                                        style="background-image:url('/images/placeholders/placeholder250x250.png');"></label>
                                </p>
                                <p>
                                    <label for="">Add Floor Plan</label>
                                    <input type="file" style="display: none" id="floor_plan"  data-type="image-picker" data-preview="#floor-plan-preview" data-preview-type="label" name="floor_plan">
                                    <label for="floor_plan" class="label-placeholder" id="floor-plan-preview"
                                        style="background-image:url('/images/placeholders/placeholder250x250.png');"></label>
                                </p>
                            </div>
                        </div>
                    </form>
                </div>
                <div id="function-space-list">
                    @forelse($functionspaces as $functionspace)
                        <div class="row" id="column-{{ $functionspace->id }}">
                            <div class="col-sm-3" >
                                <div class="box-header img-rounded p-0" style="background-image: url({{ $functionspace->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png') }}); padding-top:75% !important; background-position:center;">
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div class="text-right">
                                    <a href="{{ route('dashboard.functionspace.edit', $functionspace->id)  }}?slug={{ $venue->slug }}"><i class="fa fa-pencil"></i> Edit</a>
                                </div>
                                <h4 class="orange">
                                 {{ $functionspace->name }}
                                </h4>
                                <p>
                                    <div>
                                        {{ $functionspace->max_capacity_seated }}
                                        <small class="d-block">Max Seated</small>
                                    </div>
                                    <div>
                                        {{ $functionspace->max_capacity_standing }}
                                        <small class="d-block">Max Standing</small>
                                    </div>
                                </p>
                                <p class="text-muted">
                                    <small>
                                        {{ Str::limit($functionspace->description, 120) }}
                                    </small>
                                </p>

                                <form action="{{ route('dashboard.functionspace.upload-asset-bundle', $functionspace->id) }}" style="display:none;" method="POST" enctype="multipart/form-data" >
                                    @csrf
                                    <input type="hidden" name='type' value="web">
                                    <input type="file" id="web_asset_bundle_input" name="asset_bundle_file" class="input-file">
                                </form>
                                @access('transform-a-room-with-custom-3d-builds')
                                    <i>Asset Bundles</i>
                                    <table class="table">
                                        <tr>
                                            <td width='150px'>For Web</td>
                                            @if($functionspace->asset_bundle_web)
                                                <td >
                                                    <div id="div-{{ $functionspace->asset_bundle_web->id }}">
                                                        <div class='alert bg-gray border p-2 d-inline-block mb-0' >
                                                            {{ $functionspace->asset_bundle_web->model_name }}
                                                            <button data-buttons="delete" data-url="{{ route('dashboard.functionspace.delete-asset-bundle', $functionspace->asset_bundle_web->id)}}" data-type="div" data-id="{{ $functionspace->asset_bundle_web->id }}" class="pull-right btn btn-xs"><i class="fa fa-remove"></i> Remove</button>
                                                        </div>
                                                        <small class="pl-2"><i>uploaded {{ $functionspace->asset_bundle_web->created_at->diffForHumans() }}</i></small>
                                                    </div>
                                                </td>
                                            @else
                                                <td>
                                                    <i class='text-muted'> No asset bundle uploaded</i>
                                                    <label for="web_asset_bundle_input-{{$functionspace->id}}" class="btn btn-sm btn-default ml-2  btn-xs" style="margin-top:-5px;"><i class="fa fa-upload"></i> Upload Web Asset Bundle</label>
                                                </td>
                                            @endif
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>For Android</td>
                                            @if($functionspace->asset_bundle_android)
                                                <td>
                                                    <div id="div-{{ $functionspace->asset_bundle_android->id }}">
                                                        <div class='alert bg-gray border p-2 d-inline-block mb-0' >
                                                            {{ $functionspace->asset_bundle_android->model_name }}
                                                            <button  data-buttons="delete" data-url="{{ route('dashboard.functionspace.delete-asset-bundle', $functionspace->asset_bundle_android->id)}}" data-type="div" data-id="{{ $functionspace->asset_bundle_android->id }}" class="pull-right btn btn-xs"><i class="fa fa-remove"></i> Remove</button>
                                                        </div>
                                                        <small class="pl-2"><i>uploaded {{ $functionspace->asset_bundle_android->created_at->diffForHumans() }}</i></small>
                                                    </div>
                                                </td>
                                            @else
                                                <td>
                                                    <i class='text-muted'> No asset bundle uploaded</i>
                                                    <label for="android_asset_bundle_input-{{$functionspace->id}}" class="btn btn-sm btn-default ml-2  btn-xs" style="margin-top:-5px;"><i class="fa fa-upload"></i> Upload Android Asset Bundle</label>
                                                </td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <td>For IOS</td>
                                            @if($functionspace->asset_bundle_ios)
                                                <td>
                                                    <div id="div-{{ $functionspace->asset_bundle_ios->id }}">
                                                        <div class='alert bg-gray border p-2 d-inline-block mb-0' >
                                                            {{ $functionspace->asset_bundle_ios->model_name }}
                                                            <button  data-buttons="delete" data-url="{{ route('dashboard.functionspace.delete-asset-bundle', $functionspace->asset_bundle_ios->id)}}" data-type="div" data-id="{{ $functionspace->asset_bundle_ios->id }}" class="pull-right btn btn-xs"><i class="fa fa-remove"></i> Remove</button>
                                                        </div>
                                                        <small class="pl-2"><i>uploaded {{ $functionspace->asset_bundle_ios->created_at->diffForHumans() }}</i></small>
                                                    </div>
                                                </td>
                                            @else
                                                <td>
                                                    <i class='text-muted'> No asset bundle uploaded</i>
                                                    <label for="ios_asset_bundle_input-{{$functionspace->id}}" class="btn btn-sm btn-default ml-2  btn-xs" style="margin-top:-5px;"><i class="fa fa-upload"></i> Upload Ios Asset Bundle</label>
                                                </td>
                                            @endif
                                        </tr>

                                    </table>

                                    <form id='form-web-{{$functionspace->id}}' action="{{ route('dashboard.functionspace.upload-asset-bundle', $functionspace->id) }}" style="display:none;" method="POST" enctype="multipart/form-data" >
                                        @csrf
                                        <input type="hidden" name='type' value="web">
                                        <input type="file" id="web_asset_bundle_input-{{$functionspace->id}}" name="asset_bundle_file" class="input-file">
                                    </form>
                                    <form id='form-android-{{$functionspace->id}}' action="{{ route('dashboard.functionspace.upload-asset-bundle', $functionspace->id) }}" style="display:none;" method="POST" enctype="multipart/form-data" >
                                        @csrf
                                        <input type="hidden" name='type' value="android">
                                        <input type="file" id="android_asset_bundle_input-{{$functionspace->id}}" name="asset_bundle_file" class="input-file">
                                    </form>
                                    <form id='form-ios-{{$functionspace->id}}' action="{{ route('dashboard.functionspace.upload-asset-bundle', $functionspace->id) }}" style="display:none;" method="POST" enctype="multipart/form-data" >
                                        @csrf
                                        <input type="hidden" name='type' value="ios">
                                        <input type="file" id="ios_asset_bundle_input-{{$functionspace->id}}" name="asset_bundle_file" class="input-file">
                                    </form>
                                @endaccess
                            </div>
                        </div>
                        @if(!$loop->last)
                        <hr>
                        @endif
                    @empty
                        <h4 class="text-center text-muted mt-5">No function space</h4>
                    @endforelse
                    {{ $functionspaces->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('scripts')
    <script>
        $(document).ready(function(){
            $('.input-file').change(function(){
                var form  = $(this).closest('form')
                form.trigger('submit')
            })
        })
    </script>
@endpush
