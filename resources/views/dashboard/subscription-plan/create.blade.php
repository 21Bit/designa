@extends('dashboard.includes.layouts.main')
@section('page_title', "Subscription Plans")
@section("content-hearder")
        <h1>{{ categoryTitle(Request::get('type')) }}</h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard.home') }}"> Home</a></li>
            <li class="active">Subscription Plans</li>
        </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-4">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Add Subscription Plan</h3>
                </div>
                <div class="box-body">
                    <form action="{{ route('dashboard.subscription-plan.store') }}" method="post">
                        @csrf
                        <p>
                            <label for="#">Type</label>
                            <select name="type" id="" class="form-control">
                                <option value="basic">Basic</option>
                                <option value="event-spaces" @if(Request::get('type') == "event-spaces") selected @endif >Event Spaces</option>
                                <option value="venues" @if(Request::get('type') == "venues") selected @endif >Venues</option>
                            </select>
                        </p>
                        <p>
                            <label for="">Stripe ID <small><i>(if item is exist on stripe dashboard)</i></small></label>
                            <input type="text" name="stripe_id" class="form-control">
                        </p>
                        <p>
                            <label for="">Name *</label>
                            <input type="text" name="name" required class="form-control input-sm">
                        </p>
                        <p>
                            <label for="">Description</label>
                            <textarea name="description" id="" cols="30" rows="10" class="form-control"></textarea>
                        </p>
                        <p>
                            <label for="" class="mb-0">Access</label>
                            @foreach($accesses as $access)
                                <div class="checkbox">
                                    <label>
                                        <input value="{{ $access->id }}" name="accesses[]" type="checkbox">
                                        {{ $access->display_name }}
                                    </label>
                                </div>
                            @endforeach
                        </p>
                        <hr>
                        <button type="submit" class="btn btn-warning btn-sm"><i class="fa fa-save"></i> Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection