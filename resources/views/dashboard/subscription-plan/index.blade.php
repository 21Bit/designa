@extends('dashboard.includes.layouts.main')
@section('page_title', "Subscription Plans")
@section("content-hearder")
        <h1>{{ categoryTitle(Request::get('type')) }}</h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard.home') }}"> Home</a></li>
            <li class="active">Subscription Plans</li>
        </ol>
@endsection

@section('content')
    <div class="box mb-0">
        <div class="box-body">
            <div class="text-right mb-3">
                <a href="{{ route('dashboard.subscription-plan.create') }}" class="btn btn-warning"><i class="fa fa-plus"></i> Add New</a>
            </div>
            <h2>Subscription Plans</h2>
            <table class="table table-hover table-bordered table-condensed mb-4">
                <thead>
                    <tr>
                        <td>
                            
                        </td>
                        @foreach($plans as $plan)
                            <th>
                                <span class="float-right">
                                    <a href="{{ route('dashboard.subscription-plan.edit', $plan->id) }}" class="mr-3"><i class="fa fa-pencil"></i></a>
                                    <a href="#" data-buttons="delete" data-type="table" data-url="{{ route('dashboard.subscription-plan.destroy', $plan->id) }}"><i class="fa fa-trash"></i></a>
                                </span>
                                <h3 class="mb-0">
                                    {{ $plan->name }}
                                </h3>
                            </th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th colspan="{{ count($plans) }}">
                            <h4 class="m-0">
                                Prices
                            </h4>
                        </th>
                    </tr>
                    <tr>
                        <td class='pl-5'>Monthly</td>
                        @foreach($plans as $plan)
                            <td>
                                @forelse($plan->prices()->where('billing_period', 'monthly')->get() as $monthlyPrice)
                                <div>
                                    ${{ number_format($monthlyPrice->price, 2) }} <small class="text-muted"><i>{{ $monthlyPrice->stripe_id }}</i></small>
                                </div>
                                @empty
                                    <small class="text-muted">No pricing</small>
                                @endforelse
                            </td>
                        @endforeach
                    </tr>
                    <tr>
                        <td class='pl-5'>Yearly</td>
                        @foreach($plans as $plan)
                            <td>
                                @forelse($plan->prices()->where('billing_period', 'yearly')->get() as $yearlyPrice)
                                    <div>
                                        ${{ number_format($yearlyPrice->price, 2) }} <small class="text-muted"><i>{{ $yearlyPrice->stripe_id }}</i></small>
                                    </div>
                                @empty
                                    <small class="text-muted">No pricing</small>
                                @endforelse
                            </td>
                        @endforeach
                        
                    </tr>
                    @if(count($plans))
                        <tr>
                            <th colspan="{{ count($accesses) }}">
                                <h4 class="m-0">
                                Features
                                </h4>
                            </th>
                        </tr>
                        @foreach($accesses as $access)
                            <tr>
                                <td style="width:25%;" class='pl-5'>{{ $access->display_name }}</td>
                                @foreach($plans as $plan)
                                    <td class="text-center @if(in_array($access->id, $plan->accesses()->pluck('accesses.id')->toArray())) bg-success @endif">
                                        @if(in_array($access->id, $plan->accesses()->pluck('accesses.id')->toArray())) <i class="fa fa-check text-white"></i> @endif
                                    </td>
                                @endforeach
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td class="text-center">No subscription plan found. Please create one first.</td>
                        </tr>
                    @endif
                </tbody>
            </table>
            <hr>
            <h2>Venue Charges</h2>
                @forelse($venues as $venue)
                    @forelse($venue->prices()->where('billing_period', 'monthly')->get() as $yearlyPrice)
                        <div>
                            ${{ number_format($yearlyPrice->price, 2) }} <small class="text-muted"><i>{{ $yearlyPrice->stripe_id }}</i></small>
                            <a href="{{ route('dashboard.subscription-plan.edit', $venue->id) }}"> Edit</a>
                        </div>
                    @empty
                        <small class="text-muted">No pricing</small> <a href="{{ route('dashboard.subscription-plan.edit', $venue->id) }}"> Add</a>
                    @endforelse
                @empty
                    <small class="text-muted">No Charges</small>
                    <a href="{{ route('dashboard.subscription-plan.create', ['type' => 'venues']) }}"> Add</a>
                @endforelse
            <hr>

            <h2>Event Space Charges</h2>
                @forelse($eventSpaces as $eventSpace)
                    @forelse($eventSpace->prices()->where('billing_period', 'monthly')->get() as $yearlyPrice)
                        <div>
                            ${{ number_format($yearlyPrice->price, 2) }} <small class="text-muted"><i>{{ $yearlyPrice->stripe_id }}</i></small>
                            <a href="{{ route('dashboard.subscription-plan.edit', $eventSpace->id) }}"> Edit</a>
                        </div>
                    @empty
                        <small class="text-muted">No pricing</small>
                        <a href="{{ route('dashboard.subscription-plan.edit', $eventSpace->id) }}"> Add</a>
                    @endforelse
                @empty
                        <small class="text-muted">No Charges</small>
                        <a href="{{ route('dashboard.subscription-plan.create', ['type' => 'event-spaces']) }}"> Add</a>
                @endforelse
        </div>
    </div>

@endsection
@push('scripts')
    <script>
        $(document).ready(function(){
            $('select[name=billing_period]').change(function(){
                $(this).closest('form').submit()
            })
        })
    </script>
@endpush