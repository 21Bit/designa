@extends('dashboard.includes.layouts.main')

@section('page_title', "Edit " . $plan->name)

@section("content-hearder")
        <h1>
        {{ categoryTitle(Request::get('type')) }}
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard.home') }}"> Home</a></li>
            {{-- <li><a href="{{ route('dashboard.' . Request::get('module') . '.index') }}">{{ ucFirst(Request::get('module')) }}</a></li> --}}
            <li class="active">Subscription Plans</li>
        </ol>
@endsection

@section('content')
    @include('dashboard.includes.alerts.success')
    @include('dashboard.includes.alerts.error')
    <div class="row">
        <div class="col-sm-4">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Subscription Plan</h3>
                </div>
                <div class="box-body">
                    <form action="{{ route('dashboard.subscription-plan.update', $plan->id) }}" method="POST">
                        @csrf @method('PUT')
                        <p>
                            Stripe ID
                            <input type="text" class="form-control" value="{{ $plan->stripe_id }}" name="stripe_id">
                        </p>
                        <p>
                            <label for="">Name *</label>
                            <input type="text" name="name" value="{{ $plan->name }}" required class="form-control input-sm">
                        </p>
                        <p>
                            <label for="">Desciption</label>
                            <textarea name="description" id="" cols="30" rows="10" class="form-control">{{ $plan->description }}</textarea>
                        </p>
                        <p>
                            <label for="" class="mb-0">Access</label>
                            @foreach($accesses as $access)
                                <div class="checkbox">
                                    <label>
                                        <input value="{{ $access->id }}" @if(in_array($access->id, $plan->accesses()->pluck('accesses.id')->toArray())) checked @endif name="accesses[]" type="checkbox">
                                        {{ $access->display_name }}
                                    </label>
                                </div>
                            @endforeach
                        </p>
                        <hr>
                        <button type="submit" class="btn btn-warning btn-sm"><i class="fa fa-save"></i> Save Changes</button>
                        <a href="{{ route('dashboard.subscription-plan.index')}}" class="btn btn-default btn-sm"><i class="fa fa-ban"></i> Cancel</a>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            @if(count($plan->prices) < 2)
            <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Add Pricing</h3>
                    </div>
                    <div class="box-body">
                        <form action="{{ route('dashboard.subscription-plan.price.add') }}" method="post">
                            @csrf
                            <input type="hidden" value="{{ $plan->id }}" name="plan_id">
                            <input type="hidden" value="{{ $plan->stripe_id }}" name="product_stripe_id">
                            <p>
                                Stripe ID <small><i>(if already exist in stripe)</i></small>
                                <input type="text" class="form-control" name="stripe_id">
                            </p>
                            <p>
                                Billing Period
                                <select class="form-control" name="billing_period">
                                    <option value="monthly">Monthly</option>
                                    <option value="yearly">Yearly</option>
                                </select>
                            </p>
                            <p>
                                Price
                                <input class="form-control" type="text" name="price" required>
                            </p>
                            <p>
                                <button type="submit" class="btn btn-warning">Submit</button>
                            </p>
                        </form>
                    </div>
            </div>
            @endif
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Prices</h3>
                </div>
                <div class="box-body">
                    @forelse($plan->prices as $price)
                        {{-- <div class="pb-3 pt-2">
                            ${{ number_format($price->price, 2) }} / {{ $price->billing_period }}
                        </div> --}}

                        <div class="box mb-2">
                            <div class="box-header with-border">
                                <h3 class="box-title">{{ ucFirst($price->billing_period) }}</h3>
                            </div>
                            <div class="box-body">
                                <form action="{{ route('dashboard.subscription-plan.price.update', $price->id) }}" method="post">
                                    @csrf
                                    <input type="hidden" value="{{ $plan->id }}" name="plan_id">
                                    <input type="hidden" value="{{ $plan->stripe_id }}" name="product_stripe_id">
                                    <p>
                                        Stripe ID <small><i>(if already exist in stripe)</i></small>
                                        <input type="text" value="{{ $price->stripe_id }}" class="form-control" name="stripe_id">
                                    </p>
                                    <p>
                                        Billing Period
                                        <select class="form-control" name="billing_period">
                                            <option value="monthly">Monthly</option>
                                            <option value="yearly" @if($price->billing_period == "yearly") selected @endif >Yearly</option>
                                        </select>
                                    </p>
                                    <p>
                                        Price
                                        <input class="form-control" type="text" name="price" value="{{ $price->price }}" required>
                                    </p>
                                    <p>
                                        <button type="submit" class="btn btn-warning">Save Changes</button>
                                    </p>
                                </form>
                            </div>
                        </div>
                    @empty

                    @endforelse
                </div>
            </div>
        </div>
    </div>

@endsection
