@extends('dashboard.includes.layouts.subscription-process')
@section('page_title', 'Choose a Plan')
@section('content')
    <div class="p-2 border-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-6">
                    <img src="/site/images/logo.svg" style="height: 45px;" lt="">
                </div>
                <div class="col-6 text-right">
                    <a href="{{ route('dashboard.profile.index', ['tab' => 'subscription']) }}" class="btn btn-outline-light text-dark pull-right mt-1">Cancel</a>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-gray">
        <div class="container pt-3 mb-4">
            <h1 class="text-center mb-3">Choose Subscription Plan</h1>
            <form id="billing-form">
                <input type="hidden" name="only" value="{{ request()->only }}">
                <select name="period" onchange="document.getElementById('billing-form').submit()" class="form-control w-auto mb-3 pr-2 pl-2">
                    <option value="monthly">Monthly</option>
                    <option value="yearly" @if(Request::get('period') == "yearly") selected @endif >Yearly</option>
                </select>
            </form>
            <div class="row">
                @forelse($plans as $plan)
                    <div class="col-12 col-sm-6 col-md-6 col-lg-3 mb-4">
                        <div class="card h-100 p-2 plan-card">
                                <div class="text-center p-2 border-bottom mb-3">
                                    <div class="mt-0 font-orange">
                                        <small>
                                        {{ \Str::upper($plan->name) }}
                                        </small>
                                    </div>
                                    <div class="mb-1">
                                        <span class="dollar font-orange">$</span>
                                        <span class="amount">{{ number_format(optional($plan->getPrice(Request::get('period') ?? 'monthly'))->price) }}</span>
                                        @if($plan->name != "Beginner")
                                        <span class="billing-period font-orange">/ {{Request::get('period') ?? 'monthly' }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="p-2">
                                    <small>
                                        <ul>
                                            @foreach($plan->accesses as $access)
                                                <li>{{ $access->display_name }}</li>
                                            @endforeach
                                        </ul>
                                    </small>
                                </div>
                                <div class="footer text-center">
                                    @if(Auth::user()->inSubscriptions(optional($plan->getPrice(Request::get('period') ?? 'monthly'))->stripe_id))
                                        <a href="#"  class="btn btn-danger btn-md disabled" disabled>Current Plan</a>
                                    @else
                                        <a

                                            href="{{ route('dashboard.subscription.methods', ['plan' => encrypt(optional($plan->getPrice(Request::get('period') ?? 'monthly'))->stripe_id ?? '0')]) }}" class="btn
                                              @if($only)
                                                @if($only != $plan->name)
                                                    disabled btn-outline-dark
                                                @endif
                                            @endif
                                             btn-outline-info btn-md">Choose Plan</a>
                                    @endif
                                </div>
                            <!-- </label> -->
                        </div>
                    </div>
                @empty
                    <div class="col-12 text-center">
                        <img src="/images/404.svg" class="mw-100" alt="404 image">
                        <!-- <h4 class="text-muted">No plan found</h4> -->
                    </div>
                @endforelse
            </div>
        </div>
    </div>
@endsection
@push('styles')
    <style>
        .font-orange{
            color:#f39c12;
        }
        .plan-card{
            display:relative;
            padding-bottom:55px !important;
        }

        .plan-card .footer{
            position:absolute;
            bottom:15px;
            width:95% !important;
        }

        .plan-card .footer a{
            border-radius: 25px;
            padding:5px 15px 5px 15px;
        }

        .plan-card .btn-warning{
            background-color:#f39c12 !important;
        }

        .plan-card .btn-warning:hover{
            background-color:lighten;
        }

        .amount{
            font-size:3em;
            color:#f39c12;
            font-weight: bold;
        }

        ul {
            padding-left: 20px;
            text-indent: 0px;
            list-style: none;
            list-style-position: outside;
        }

        ul li{
            margin-bottom: 5px;
        }

        ul li:before {
            content: '✓ ';
            margin-left: -1.3em;
            margin-right: .100em;
        }
    </style>
@endpush
