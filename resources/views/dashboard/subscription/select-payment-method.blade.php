@extends('dashboard.includes.layouts.subscription-process')
@section('page_title', 'Choose a payment method')
@section('content')
    <div class="p-2 border-bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-6">
                    <img src="/site/images/logo.svg" style="height: 45px;" lt="">
                </div>
                <div class="col-6 text-right">
                    <a href="{{ route('dashboard.profile.index', ['tab' => 'subscription']) }}" class="btn btn-outline-light text-dark pull-right mt-1">Cancel</a>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-gray">
        <div class="container pt-3 mb-5">
            <div class="text-center mb-4  pt-0">
                <h3 class="">Lets take the last Step!</h3>
                <p>Select payment method and enjoy all the feature within the plan.</p>
            </div>

            <div class="text-center p-2 mb-3">
                <div class="mt-0 font-orange">
                    {{ \Str::upper($price->subscriptionPlan->name) }} 
                </div>
                <div class="mb-1">
                    <span class="dollar font-orange">$</span>
                    <span class="amount">{{ number_format($price->price) }}</span>
                    <span class="billing-period font-orange">/{{ $price->billing_period }}</span>
                </div>
                <a href="{{ route('dashboard.subscription.plans', ['period' => $price->billing_period]) }}" class="text-muted"><small>Change Plan</small></a>
            </div>
            @if(Session::has('warning'))
                <div class="alert alert-danger alert-dismissible mt-1 mb-3">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('warning') }}
                </div>
            @endif
  
  
            
            <h5 class="text-muted mt-4">Select Card.</h5>
            @if(count($paymentMethods))
                <ul class="list-group">
                    @foreach($paymentMethods as $paymentMethod)
                        <li class="list-group-item">
                            <button data-form="form-{{ $paymentMethod->id }}" class="btn paymentmethod-btn btn-outline-info float-right mt-2 pl-4 pr-4" style="border-radius: 25px;"> Select</button>
                            <form action="{{ route('dashboard.subscription.store') }}" id="form-{{ $paymentMethod->id }}" method="POST">
                                @csrf
                                <input type="hidden" name="paymentMethod" value="{{ $paymentMethod->id }}">
                                <input type="hidden" name="price" value="{{ $price->stripe_id }}">
                            </form>
                            <img src="/site/images/credit_cards/{{ $paymentMethod->card->brand }}.png" alt="">
                            <i class="fa fa-cc-{{ $paymentMethod->card->brand }}" ></i> **** **** **** {{ $paymentMethod->card->last4 }}
                        </li>
                    @endforeach
                </ul>
            @else
                <div class="card" id="add-payment-form-container">
                    <div class="card-body">
                        <form action="{{ route('dashboard.payment-method.store') }}" method="POST" id="add-payment-method-form">
                            @csrf
                            <input class='form-control' placeholder="Card Holder Name" value="{{ Auth::user()->name }}" required id="card-holder-name" type="hidden">
                            <p>
                                <label for="">Card Details</label>
                                <div id="card-element" class='form-control'></div>
                            </p>
                            <p>
                                <div id="card-errors"></div>
                            </p>
                            <button id="card-button" type="submit" class="btn btn-warning" data-secret="{{ $intent->client_secret }}">
                                Create Payment Method
                            </button>
                        </form>
                    </div>
                </div>
            @endif
            
        </div>
    </div>
@endsection
@push('styles')
    <style>
        .font-orange{
            color:#f39c12;
        }

        .amount{
            font-size:3em;
            color:#f39c12;
            font-weight: bold;
        }
    </style>
@endpush

@push('scripts')
    <script>
        $(document).ready(function(){
            $('.paymentmethod-btn').click(function(){
                var form = $(this).data('form')
                $(this).attr('disabled', true)
                $(this).removeClass('btn-outline-info').addClass('btn-outline-warning')
                $(this).html("<i class='fa-spinner fa-spin'></i> Proccessing..");
                $('#' + form).submit();
            })
        }) 
    </script>
@endpush