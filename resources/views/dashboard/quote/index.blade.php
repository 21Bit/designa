@extends('dashboard.includes.layouts.main')

@section('page_title', 'Quotes')

@section("content-header")
    <h1>
        Quotes
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.home') }}"> Home</a></li>
        <li class="active"> Quotes</li>
    </ol>
@endsection

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Quotes</h3>
        </div>
        <div class="box-body">
            <table class="table">
                <thead>
                    <tr>
                        <th>Status</th>
                        <th>Customer</th>
                        <th>Event</th>
                        <th>Reference</th>
                        <th>Decor</th>
                        <th>Date/Time</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($quotes as $quote)
                        <tr>
                            <td>
                                @if($quote->priced_component == $pricingComponentsCount)
                                    Completed
                                @else
                                    Pending
                                @endif
                            </td>
                            <td>
                                {{ $quote->customer  }}
                            </td>
                            <td>{{ $quote->event_name }}</td>
                            <td>{{ $quote->reference_number }}</td>
                            <td>
                                {{ $quote->product_counts }}
                            </td>
                            <td>
                                {{ date('M d, Y', strtotime($quote->created_at)) }}
                            </td>
                            <td class="text-right">
                                <a href="{{ route('dashboard.quote.show', $quote->reference_number)  }}" class="btn btn-primary"> Manage</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $quotes->links()  }}
        </div>
    </div>
@endsection

