@extends('dashboard.includes.layouts.main')

@section('page_title', $quote->event_name)

@section("content-hearder")
        <h1>
        Quote
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard.home') }}"> Home</a></li>
            <li class="active"> Quote</li>
        </ol>
@endsection

@section('content')
   
    <form action="{{ route('dashboard.quote.savepricing', $quote->id) }}" method="POST">
        @csrf
        <div class="text-right mb-2">
            <div class="text-right">
                <a href="{{ route('dashboard.quote.index') }}" class="btn btn-default"><i class="fa fa-ban"></i> Cancel</a>
            </div>
        </div>
         @include('dashboard.includes.alerts.success')
        @include('dashboard.includes.alerts.error')
        <div class="box">
            <div class="box-header">
                <div class="row">
                    <div class="col-sm-6">
                        <h2 class="mt-0 text-orange">{{ $quote->event_name }}</h2>
                        <table>
                            {{-- <tr>
                                <td>Event Name</td>
                                <td>: {{ optional($quote->category)->name }}</td>
                            </tr> --}}
                            <tr>
                                <td>Reference Number</td>
                                <td>: {{ $quote->reference_number }}</td>
                            </tr>
                            <tr>
                                <td>Location</td>
                                <td>: {{ $quote->location }}</td>
                            </tr>
                            <tr>
                                <td>Number of Guests</td>
                                <td>: {{ $quote->number_of_guests }}</td>
                            </tr>
                            <tr>
                                <td>Date</td>
                                <td>: {{ $quote->event_date }}</td>
                            </tr>
                            <tr>
                                <td>Time Start</td>
                                <td>: {{ $quote->time_start }}</td>
                            </tr>
                            <tr>
                                <td>Time End</td>
                                <td>: {{ $quote->time_end }}</td>
                            </tr>
                            <tr>
                                <td>Requested On</td>
                                <td>: {{ $quote->created_at->format('M d, Y h:ia') }}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-sm-6 text-right">
                        <div class="pull-right p-3 text-center" style="width:300px">
                            <img src="{{ $quote->user->getProfilePicture() }}" alt="" style="width:115px; height: 115px;"> <br>
                            <h4 class="mb-0">{{ $quote->user->name }}</h4>
                            {{ $quote->user->email }}
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="box-body">
                        
                <table class="table">
                    <thead>
                        <tr>
                            <td></td>
                            <td>Quantity</td>
                            @foreach($pricingcomponents as $pricingComponent)
                            <td>{{ $pricingComponent->name  }} @if($pricingComponent->multipliable) (each) @endif</td>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($decors as $decor)
                            <tr>
                                <td>
                                    <div class="d-flex">
                                            <img class="mr-3 mb-0"src="{{ $decor->product->thumbnail  }}" align="left" style="width:50px; height:50px" alt="Generic placeholder image">
                                            {{ $decor->product->name }}
                                            {{ $decor->id }}
                                            <quote-product-comment-component decorid="{{ $decor->id }}"></quote-product-comment-component>
                                    </div>
                                </td>
                                <td>{{ $decor->quantity }}</th>
                                @foreach($pricingcomponents as $pricing)
                                    <td width="150px">
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" name="price-{{ $decor->id }}-{{ $pricing->id }}" value="{{ $decor->getComponentPrice($pricing->id) }}" class="form-control" aria-label="Amount (to the nearest dollar)">
                                        </div>          
                                    </td>
                                @endforeach
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="text-right">
                    <div class="mb-2">
                        <input type="checkbox" name="notify" data-type='icheck' checked> Notify customer<br>
                    </div>
                    <button type="submit" class="btn btn-warning btn-lg"><i class="fa fa-save"></i> Save Changes</button>
                </div>
            </div>
        </div>
    </form>    
@endsection
