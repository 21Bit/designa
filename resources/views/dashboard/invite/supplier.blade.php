@extends('dashboard.includes.layouts.main')

@section('page_title', 'Invite')

@section("content-hearder")
        <h1>
        {{ categoryTitle(Request::get('type')) }}
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard.home') }}"> Home</a></li>
            {{-- <li><a href="{{ route('dashboard.' . Request::get('module') . '.index') }}">{{ ucFirst(Request::get('module')) }}</a></li> --}}
            <li class="active">Invite</li>
        </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-6">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Invite Form</h3>
                </div>
                <div class="box-body">
                    @include('dashboard.includes.alerts.success')
                    @include('dashboard.includes.alerts.error')
                    <form action="{{ route('dashboard.invite.send') }}" method="POST">
                        @csrf
                        <p>
                            Please fill all the fields to be use in the email for invitation.
                        </p>

                        <p>
                            <label for="">First Name *</label>
                            <input type="text" name="first_name" required class="form-control">
                        </p>
                        <p>
                            <label for="">Last Name</label>
                            <input type="text" name="last_name" class="form-control">
                        </p>
                        <p>
                            <label for="">Email Address *</label>
                            <input type="email" name="email" class="form-control">
                        </p>
                        <p>
                            <label for="">Company Name</label>
                            <input type="text" name="company" class="form-control">
                        </p>
                        <p>
                            <button class="btn btn-primary mt-3" type="submit"><i class="fa fa-send"></i> Send Invitation Email</button>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection