@extends('dashboard.includes.layouts.main')

@section('page_title', 'Notifications')

@section("content-hearder")
    <h1>
        Notification
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Notification</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-6">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">All Notification</h3>
                </div>
                <div class="box-body">
                    @foreach($notifications as $notification)
                        <div class="box-body chat" id="chat-box">
                            <!-- chat item -->
                            <div class="item">
                                <img src="{{ $notification->data['avatar'] }}" alt="user image" class="online">
                
                                <p class="message">
                                <a href="{{ notification_url($notification->data['link'], $notification->id) }}" class="name">
                                    <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> {{ $notification->created_at->diffforhumans() }}</small>
                                    {!! $notification->data['title'] !!}
                                </a>
                                {!! $notification->data['message'] !!}
                                </p>
                            </div>
                        </div>
                    @endforeach
                    <!-- /.chat -->
                </div>
            </div>
            {{ $notifications->links() }}
        </div>
    </div>
   
@endsection