@extends('dashboard.includes.layouts.main')

@section('page_title', $blog->id)

@section("content-hearder")
    <h1>
        Blog
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.home') }}">Home</a></li>
        <li><a href="{{ route('dashboard.blog.index') }}">Blog</a></li>
        <li class="active">{{ $blog->id }}</li>
    </ol>
@endsection

@section('content')
    <div class="blog-header" style="background-image:url('{{ $blog->image('cover')->path('covers', 'https://via.placeholder.com/1920x450') }}')">
    </div>
    <div>
        <div class="row" >
            <div class="col-sm-8">
                <div class="text-right mb-2">
                    <a href="{{ route('dashboard.blog.edit', $blog->id) }}" class="mr-3 ml-4 btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>
                    <a href="#" class="btn btn-default" data-type="div" data-buttons="delete" data-id="{{ $blog->id }}" data-url="{{ route('dashboard.blog.destroy', $blog->id) }}"><i class="fa fa-trash"></i> Delete</a>
                </div>
                <div class="mr-2 individual-box">
                    <p style="color: rgb(243, 156, 18);">{{  $blog->category }}</p>
                    <h4 class="text-left">{{ $blog->title }}</h4>
                    <img src="/images/blogs/{{ $image->path }}" class="img img-responsive mb-4" alt="blog-image" class="margin">
                    <p style="padding-top: 20px;">
                        {!! $blog->description !!}
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.css" integrity="sha256-0Z6mOrdLEtgqvj7tidYQnCYWG3G2GAIpatAWKhDx+VM=" crossorigin="anonymous" />
    <style>
        .individual-box {
            -webkit-box-shadow: -1px 1px 28px 3px rgba(0,0,0,0.08);
            -moz-box-shadow: -1px 1px 28px 3px rgba(0,0,0,0.08);
            box-shadow: -1px 1px 28px 3px rgba(0,0,0,0.08);
            padding: 20px;
            background-color: #fff
        }
        .img-fluid {
            max-width: 100%;
            height: auto;
        }
        img {
            vertical-align: middle;
            border-style: none;
        }

    </style>
@endpush
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js" integrity="sha256-cs4thShDfjkqFGk5s2Lxj35sgSRr4MRcyccmi0WKqCM=" crossorigin="anonymous"></script>
    {{-- <script>
        $(document).ready(function(){
            $("div#dropzone").dropzone({
                url: "{{ route('dashboard.blog.uploadgallery', $blog->id) }}",
                type: 'POST',
                dataType: 'JSON',
                addRemoveLinks : true,
                acceptedFiles:".jpg,.png,.JPG,.PNG",
                maxFilesize: 5,
                dictDefaultMessage: '<span class="text-center"><span class="font-lg visible-xs-block visible-sm-block visible-lg-block"><span class="font-lg"><i class="fa fa-caret-right text-danger"></i> Drop files <span class="font-xs">to upload</span></span><span>&nbsp&nbsp<h4 class="display-inline"> (Or Click)</h4></span>',
                dictResponseError: 'Error uploading file!',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            });

        })
    </script> --}}
@endpush
