@extends('dashboard.includes.layouts.main')

@section('page_title', 'Create Blog')

@section("content-hearder")
        <h1>
            Companies
            {{-- <small>it all starts here</small> --}}
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('dashboard.blog.index') }}">Blogs</a></li>
            <li class="active">Create</li>
        </ol>
@endsection


@section('content')
<form action="{{ route('dashboard.blog.store') }}" id='form-with-loading' method="post" enctype="multipart/form-data">
        @csrf
        <div class="text-right mb-2">
            <button class="btn btn-warning"><i class="fa fa-save"></i> Save</button>
            <a href="{{ route('dashboard.blog.index') }}" class="btn btn-default"><i class="fa fa-ban"></i> Cancel</a>
        </div>
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-sm-8">
                    <p>
                        <label for="">Title *</label>
                        <input type="text" value="{{ old("title") }}" required autofocus name="title" placeholder="Title" required class="form-control">
                    </p>
                    <p>
                        <label for="">Desciption</label>
                        <textarea name="description" cols="30" rows="6" id="texteditor" class="form-control">{!! old('description') !!}</textarea>
                    </p>
                </div>
                <div class="col-sm-4">
                    <p>
                        <label for="">Status *</label>
                        <select name="is_published" class="form-control">
                            <option value="1">Published</option>
                            <option value="0">Draft</option>
                        </select>
                    </p>
                    <p>
                        <label for="">Category *</label>
                        <input type="text" value="{{ old("category") }}" required autofocus name="category" placeholder="Category" required class="form-control">
                    </p>
                    <p>
                        <label for="">Image <small><i>({{ config('filesystems.accepted_image_types') }})</i></small></label>
                        <input type="file" style="display: none" id="thumbnail" accept="image/*"
                        data-type="image-picker" data-preview="#preview-thumbnail" data-preview-type='img' name="thumbnail">
                        <br>
                        <label for="thumbnail" class="btn btn-default btn-xs d-block m-2" ><i class="fa fa-image"></i>  Choose Image</label>
                        <img src="/images/placeholders/placeholder.png" class="img img-responsive" id="preview-thumbnail" alt="">
                    </p>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@push('scripts')
    @include('dashboard.includes.editors.tinymce')
@endpush
