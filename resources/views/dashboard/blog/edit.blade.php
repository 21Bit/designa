@extends('dashboard.includes.layouts.main')

@section('page_title', 'Edit Blog')

@section("content-hearder")
        <h1>
            Companies
            {{-- <small>it all starts here</small> --}}
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('dashboard.blog.index') }}">Companies</a></li>
            <li class="active">Edit</li>
        </ol>
@endsection


@section('content')
<form action="{{ route('dashboard.blog.update', $blog->id) }}" id='form-with-loading' method="post" enctype="multipart/form-data" >
@csrf
@method('PUT')
    <div class="text-right mb-2">
        <button class="btn btn-warning"><i class="fa fa-save"></i> Save Changes</button>
        <a href="{{ route('dashboard.blog.index') }}" class="btn btn-default"><i class="fa fa-ban"></i> Cancel</a>
    </div>
    @include("dashboard.includes.alerts.error")
    @include('dashboard.includes.alerts.success') 
    <div class="box">
        <div class="box-body">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-sm-8">
                    <p>
                        <label for="">Title *</label>
                        <input type="text" value="{{ $blog->title }}" required autofocus name="title" placeholder="Title" required class="form-control">
                    </p>
                    <p>
                        <label for="">Desciption</label>
                        <textarea name="description" cols="30" rows="6" id="texteditor" class="form-control">{!! $blog->description  !!}</textarea>
                    </p>  
                </div>
                <div class="col-sm-4">
                    <p>
                        <label for="">Status *</label>
                        <select name="is_published" @if(!Auth::user()->is_superadmin) @if($inspiration->is_lock) disabled @endif  @endif class="form-control">
                            <option value="1" {{ $blog->is_published == 1 ? 'selected' : '' }} >Published</option>
                            <option value="0" {{ $blog->is_published == 0 ? 'selected' : '' }}>Draft</option>
                        </select>
                    </p>
                    <p>
                        <label for="">Category *</label>
                        <input type="text" value="{{ $blog->category }}" required autofocus name="category" placeholder="Category" required class="form-control">
                    </p>
                    <p>
                        <label class="text-muted mb-0" for="">Image<small><i>({{ config('filesystems.accepted_image_types') }})</i></small></label></label>
                        <div class="form-group mt-0">
                            <label for="thumbnail" class="btn btn-default btn-xs"> Change Image</label>
                        </div>
                     
    
                         <input type="file"  accept="image/*" style="display: none" 
                            id="thumbnail" name="thumbnail"  data-type="image-picker" 
                            data-preview="#preview-thumbnail" data-preview-type='img' >
                         
                        <div class="image-overlay">
                            <img src="{{ $blog->image('thumbnail')->path('blogs','/images/placeholders/placeholder.png') }}" id="preview-thumbnail" alt="" class="img img-responsive">
                        </div>
                    </p>
                </div>
            </div>  
        </div>
    </div>
</form>
@endsection

@push('scripts')
    @include('dashboard.includes.editors.tinymce')
@endpush