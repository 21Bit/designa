@extends('dashboard.includes.layouts.main')

@section('page_title', 'Companies')

@section("content-hearder")
        <h1>
            Blog
            {{-- <small>it all starts here</small> --}}
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Blogs</li>
        </ol>
@endsection

@section('content')
        @include("dashboard.includes.alerts.success")
        @include("dashboard.includes.alerts.error")
        <div class="box">
            <div class="box-body">
                <div class="row">
                    
                        <form>
                            <div class="col-sm-3 mb-2">
                                Publish Status
                                <select class="form-control input-sm" name="status">
                                    <option value="">All</option>    
                                    <option value="1" {{ Request::get('status') == 1 ? 'selected' : '' }}>Draft</option>                                    
                                    <option value="2" {{ Request::get('status') == 2 ? 'selected' : '' }}>Publish</option>
                                </select>
                            </div>
                            <div class="col-sm-6 mb-2">
                                Search
                                <div class="input-group">
                                    <input type="text" class="form-control input-sm" value="{{ Request::get('search') }}"  name="search" placeholder=" Search product.." aria-label="...">
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default btn-sm"><i class="fa fa-sort"></i> Apply Filter</button>
                                        <a href="{{ route('dashboard.blog.create') }}" class="btn btn-default btn-sm"><i class="fa fa-plus"></i> New</a>
                                        @if(Request::get('status') || Request::get('search'))
                                            <a href="{{ route('dashboard.blog.index') }}" class="btn btn-default btn-sm">
                                                <i class="fa fa-ban"></i> Clear Filter
                                            </a>
                                        @endif
                                        
                                    </div>
                                </div>
                            </div>
                        </form>
                   
                </div>
                <br>
            </div>
        </div>
        @forelse($blogs as $blog)
            <div class="info-box blog-box" id="div-{{ $blog->id }}">
                <a href="{{ route('dashboard.blog.show', $blog->id) }}">
                    <span class="info-box-icon" style="background-image: url('{{ $blog->image('thumbnail')->path('blogs','/images/placeholders/placeholder250x200.png') }}'); background-position:center"></span>
                </a>
                <div class="info-box-content">
                    <span class="info-box-text text-muted">{{ $blog->category }}</span>
                    <span class="info-box-title">{{ $blog->title }}</span>
                    <div class="description">
                        {{-- {{ substr($blog->description, 0,  100) }} --}}
                        {!! substr($blog->description, 0,  550) !!}....
                    </div>
                    
                    <div class="buttons">
                        <!-- <a href="" class="mr-3 ml-4"><i class="fa fa-list"></i> Manage</a> -->
                        <a href="{{ route('dashboard.blog.edit', $blog->id) }}" class="mr-3 ml-4"><i class="fa fa-pencil"></i> Edit</a>
                        <a href="#" data-type="div" data-buttons="delete" data-id="{{ $blog->id }}" data-url="{{ route('dashboard.blog.destroy', $blog->id) }}"><i class="fa fa-trash"></i> Delete</a>
                    </div>
                </div>
            </div>            
        @empty
            <h4 class="text-center text-muted mt-5">No Blog</h4>
        @endforelse

        {{ 
            $blogs->appends([
                'status' => Request::get('status'),
                'search' => Request::get('search')
            ])->links()
        }}
@endsection
