@extends('dashboard.includes.layouts.main')

@section('page_title', 'Edit Cateory')

@section("content-hearder")
        <h1>
            {{ categoryTitle($category->type, true) }}
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Category / {{ categoryTitle($category->type, true) }}</li>
        </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-4">
            <div class="box">
                <div class="box-header with-border">
                      <h3 class="box-title">Edit {{ categoryTitle($category->type, true) }}</h3>
                </div>
                <div class="box-body">
                    <form action="{{ route('dashboard.category.update', $category->id) }}" method="post">
                        @csrf
                        @method('PUT')
                        <p>
                            <label for="">Name *</label>
                            <input type="text" name="name" value="{{ $category->name }}" required class="form-control input-sm">
                        </p>

                        @if(Request::get('credits'))
                            <p>
                                <label for="">Credit By</label>
                                <input type="text" name='credit_by' value="{{ $category->credit_by }}" required class="form-control input-sm">
                            </p>
                            <p>
                                <label for="">Credit Location</label>
                                <input type="text" name="credit_location" value="{{ $category->credit_location  }}"  required class="form-control input-sm">
                            </p>
                        @endif

                        @if($category->type == "color")
                            <p>
                                <label for="">Color *</label> <br />
                                <input type="color" value="{{ $category->description }}" name="rgb_code">
                            </p>
                        @else
                            <p>
                                <label for="">Desciption</label>
                                <textarea name="description" id="" cols="30" rows="10" class="form-control">{{ $category->description }}</textarea>
                            </p>
                        @endif
                        <button type="submit" class="btn btn-warning btn-sm"><i class="fa fa-save"></i> Save Changes</button>
                        <a href="{{ route('dashboard.category.index', ['type' => $category->type, 'module' => $category->module ?? "category", "credits" => Request::get('credits') ? 1 : 0]) }}" class="btn btn-default btn-sm"><i class="fa fa-ban"></i> Cancel</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
   
@endsection