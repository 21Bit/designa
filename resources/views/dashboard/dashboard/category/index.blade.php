@extends('dashboard.includes.layouts.main')

@section('page_title', categoryTitle(Request::get('type')))

@section("content-hearder")
        <h1>
        {{ categoryTitle(Request::get('type')) }}
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard.home') }}"> Home</a></li>
            {{-- <li><a href="{{ route('dashboard.' . Request::get('module') . '.index') }}">{{ ucFirst(Request::get('module')) }}</a></li> --}}
            <li class="active">{{ categoryTitle(Request::get('type')) }}</li>
        </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-4">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Add {{ categoryTitle(Request::get('type'), true) }}</h3>
                </div>
                <div class="box-body">
                    <form action="{{ route('dashboard.category.store') }}" method="post">
                        <input type="hidden" name="type" value="{{ Request::get('type') }}">
                        <input type="hidden" name="module" value="{{ Request::get('module') }}">
                        @csrf
                        <p>
                            <label for="">Name *</label>
                            <input type="text" name="name" required class="form-control input-sm">
                        </p>

                        @if(Request::get('credits'))
                            <p>
                                <label for="">Credit By</label>
                                <input type="text" name='credit_by' required class="form-control input-sm">
                            </p>
                            <p>
                                <label for="">Credit Location</label>
                                <input type="text" name="credit_location" required class="form-control input-sm">
                            </p>
                        @endif

                        @if(Request::get('type') == "color")
                            <p>
                                <label for="">Color *</label> <br />
                                <input type="color" name="description">
                            </p>
                        @else
                        <p>
                            <label for="">Desciption</label>
                            <textarea name="description" id="" cols="30" rows="10" class="form-control"></textarea>
                        </p>
                        @endif
                        <button type="submit" class="btn btn-warning btn-sm"><i class="fa fa-save"></i> Save</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="box mb-0">
                <div class="box-body">
                    @if(Request::get('type') == "cater" || Request::get('type') == "product_type" )
                        <sub-category-manager 
                            caterories-route="{{ url()->full() }}"
                            module="{{ Request::get('module') }}"
                            subcategory-add-route={{ route("dashboard.category.store.subcategory") }}
                             ></sub-category-manager>
                    @else
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Desciption</th>
                                    @if(Request::get('credits'))
                                        <th>Credit By</th>
                                        <th>Credit Location</th>
                                    @endif
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($categories as $category)
                                    <tr>
                                        <td>
                                        @if(Request::get('type') == "color")
                                             <span class='color-span mr-2' style="background-color:{{ $category->description }}">&nbsp;&nbsp;</span>
                                        @endif
                                        {{ $category->name }}</td>
                                        <td>{{ $category->description }}</td>
                                        @if(Request::get('credits'))
                                            <td>{{ $category->credit_by }}</td>
                                            <td>{{ $category->credit_location }}</td>
                                        @endif
                                        <td class="text-right">
                                            <a href="{{ route('dashboard.category.edit', $category->id) }}@if(Request::get('credits') == 1)?credits=true @endif" class="mr-3"><i class="fa fa-pencil"></i></a>
                                            <a href="#" data-buttons="delete" data-type="table" data-url="{{ route('dashboard.category.destroy', $category->id) }}"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @empty

                                @endforelse
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>
            {{ $categories->appends(['type' => Request::get('type'), 'module' => Request::get('module')])->links() }}
        </div>
    </div>

@endsection