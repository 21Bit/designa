@extends('dashboard.includes.layouts.main')
@section("content")
     <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{ $saves }}</h3>
              <p>Total Saves</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

         <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{ $shares }}</h3>
              <p>Total Shares</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{ $posts }}</h3>
              <p>Total Posts</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{ $views }}</h3>
              <p>Totol Views</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>
            <h3 class="mb-0">Total Saved Summary</h3 class="mb-0">
            <h1 class="mt-0">{{ $monthname }}</h1>
            <div class="chart">
                <!-- Sales Chart Canvas -->
                <canvas id="areaChart"></canvas>
            </div>

        <!-- ./col -->  
    </section>
@endsection
@push('scripts')
    <script>
     $(document).ready(function(){
        // Get context with jQuery - using jQuery's .get() method.
    var areaChartCanvas = $('#areaChart').get(0).getContext('2d')


    var areaChartData = {
      labels  : {!! json_encode($labels)  !!},
      datasets: [
      {
          label               : 'Mobile',
          backgroundColor     : 'rgba(254,150,26,0.6)',
          strokeColor         : 'rgba(60,141,188,0.8)',
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : {!! json_encode($datamobile)  !!}
        },
        {
          label               : 'Web',
          backgroundColor     : 'rgba(214, 214, 214, 1)',
          strokeColor         : 'rgba(210, 214, 222, 1)',
          pointColor          : 'rgba(210, 214, 222, 1)',
          pointStrokeColor    : '#c1c7d1',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(220,220,220,1)',
          data                : {!! json_encode($dataweb)  !!}
        },
        
      ]
    }

    //Create the line chart
    var areaChart = new Chart(areaChartCanvas, {
      type:'line',
      data: areaChartData,
      options: {
        datasetStroke: true,
        bezierCurve: true,
        scales: {
            xAxes: [{
                gridLines: {
                    color: "rgba(0, 0, 0, 0)",
                }
            }],
            yAxes: [{
                gridLines: {
                    {{-- color: "rgba(0, 0, 0, 0)", --}}
                },
                ticks: {
                    display: false
                }   
            }]
        }
      }
    })

      })
    </script>
@endpush