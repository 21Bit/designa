@extends('dashboard.includes.layouts.main')

@section('page_title', 'Event Board')

@section("content-hearder")
    <h1>
        Event Board
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Event Board</li>
    </ol>
@endsection

@section('content')
    <div class="text-right mb-2">
        <button class="btn btn-default btn-md" type="button" data-toggle="modal" data-target="#addEvent"><i class="fa fa-plus"></i> New Board </button>
    </div>

    @forelse($projects as $project)
        <div class="project-box" >
            <div class="row">
                <a href="{{ route('dashboard.event-board.show', $project->slug) }}" style="display:block;">
                    <div class="col thumbnail-image" style="background-image: url({{ $project->thumbnail }}); padding-top:55%; border-radius:10px;">
                </div>
            </a>
                <div class="col details">
                    <h2>{{ $project->title }}</h2>
                    <p>
                        {{ Str::limit($project->description, 150) }}
                    </p>
                </div>
                <div class="col colors">
                    @foreach($project->colors as $color)
                        <span class="color-cicle" style="background-color:{{ $color->description }};"></span>
                    @endforeach
                </div>
                <div class="col misc">
                    <div class="dropdown">
                        <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" style="font-size:24px; cursor: pointer;" aria-expanded="false">
                            &nbsp; <i class="fa fa-ellipsis-v"></i> &nbsp;
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <li><a href="{{ route('dashboard.event-board.edit.categories', $project->id) }}"><i class="fa fa-edit"></i> Edit</a></li>
                            <li><a href="#"
                                    onclick="if(confirm(`Are you sure to delete?`)){ $('#form-{{ $project->id }}').submit(); }"><i class="fa fa-trash-o"></i>  Delete</a></li>
                        </ul>
                        <form id="form-{{ $project->id }}" action="{{ route('dashboard.event-board.destroy', $project->id) }}" method="POST">
                            @csrf @method('DELETE')
                        </form>
                    </div>
                    <div class="image-counts">
                        {{ $project->inspiration_images_count }} images <br>
                        {{ $project->product_images_count }} Decors
                    </div>
                </div>
            </div>
        </div>
    @empty
        <h5 class="text-center text-muted">No Event Board</h5>
    @endforelse

    {{  $projects->links() }}


    <!-- Modal -->
    <div class="modal fade in" id="addEvent" tabindex="-1" role="dialog" aria-labelledby="addEventLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{ route('dashboard.event-board.store')}}" method="post">
                    @csrf
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="addEventLabel">New Event Board</h4>
                    </div>
                    <div class="modal-body">
                        <p>
                            <label for=""> Title</label>
                            <input type="text" placeholder=" title" name="title" class="form-control">
                        </p>
                        <p>
                            <label for=""> Description</label>
                            <textarea type="text" rows="8" placeholder=" description" name="description" class="form-control"></textarea>
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Proceed</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

