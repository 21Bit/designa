@extends('dashboard.includes.layouts.main')

@section('page_title', 'Event Board')

@section("content-hearder")
        <h1>
            Event Board
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Event Board</li>
        </ol>
@endsection

@section('content')
<div class="row" style='position: fixed; bottom:0; width:100%; z-index:999; background-color: #ffff; border-top:1px solid rgb(228, 228, 228);'>
    <div class="col-sm-3 text-right pt-5">
        <br>
        <a href="{{ route('dashboard.event-board.edit.products', $project->id) }}"  class="h3" >Back</a>
    </div>
    <div class="col-sm-6">
        <div>
            <div class="wizard">
                <div class="wizard-inner">
                    <div class="connecting-line"></div>
                    <ul class="nav nav-tabs"  role="tablist">
    
                        <li role="presentation" class="active">
                            <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                                <span class="round-tab">
                                    <span class="fa fa-check"></span>
                                </span>
                            </a>
                        </li>
    
                        <li role="presentation" class="active">
                            <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">
                                <span class="round-tab">
                                    <span class="fa fa-check"></span>
                                </span>
                            </a>
                        </li>
                        <li role="presentation" class="active">
                            <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Step 3">
                                <span class="round-tab">
                                    <span class="fa fa-check"></span>
                                </span>
                            </a>
                        </li>
    
                        <li role="presentation" class="active">
                            <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
                                <span class="round-tab">
                                    <span class="fa fa-check"></span>
                                </span>
                            </a>
                        </li>
    
                        <li role="presentation" class="active">
                            <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
                                <span class="round-tab">
                                    <span class="fa fa-check"></span>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-3 text-left  pt-5">
        <br>
        <a href="{{ route('dashboard.event-board.edit.suppliers', $project->id) }}"  class="h3" >Done</a>
    </div>
</div>

    <div class="box  no-border event-board-create">
        <div class="box-header text-center with-border">
            <a href="{{ route('dashboard.event-board.edit.categories', $project->id) }}" class="pull-left h1" ><i class="fa fa-arrow-left"></i></a>
            <h2>
                Event Board Complete
            </h2>
        </div>
        <div class="box-body no-border">
            <h4 class="mb-5">Your mood board <br> is now created.</h4>
            <event-board-preview url="{{ route('dashboard.event-board.edit.preview.api', $project->id) }}"></event-board-preview>
            
           
        </div>
    </div>
    <br>
    <br>
    <br>
@endsection
