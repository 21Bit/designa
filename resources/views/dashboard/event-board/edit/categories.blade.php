@extends('dashboard.includes.layouts.main')

@section('page_title', 'Event Board')

@section("content-hearder")
        <h1>
            Event Board
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Event Board</li>
        </ol>
@endsection

@section('content')

    <div class="row" style='position: fixed; bottom:0; width:100%; z-index:999; background-color: #ffff; border-top:1px solid rgb(228, 228, 228);'>
        <div class="col-sm-3 text-right pt-5">
            <br>
            <a href="{{ route('dashboard.event-board.index') }}"  class="h3" >Back</a>
        </div>
        <div class="col-sm-6">
            <div>
                <div class="wizard">
                    <div class="wizard-inner">
                        <div class="connecting-line"></div>
                        <ul class="nav nav-tabs"  role="tablist">

                            <li role="presentation" class="active">
                                <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                                    <span class="round-tab">
                                        1
                                    </span>
                                </a>
                            </li>

                            <li role="presentation" class="disabled">
                                <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">
                                    <span class="round-tab">
                                        2
                                    </span>
                                </a>
                            </li>
                            <li role="presentation" class="disabled">
                                <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Step 3">
                                    <span class="round-tab">
                                        3
                                    </span>
                                </a>
                            </li>

                            <li role="presentation" class="disabled">
                                <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
                                    <span class="round-tab">
                                        4
                                    </span>
                                </a>
                            </li>

                            <li role="presentation" class="disabled">
                                <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
                                    <span class="round-tab">
                                        5
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-3 text-left  pt-5">
            <br>
            <a  id="formSubmit" type="submit" style="cursor: pointer" class="h3" >Next</a>
        </div>
    </div>
    <div class="box  no-border">
        <div class="box-header text-center with-border">
            {{-- <a href="#" class="pull-left"><i class="fa fa-arrow-left"></i></a> --}}
            <h1>
                {{ $project->title }}
            </h1>
        </div>
        <div class="box-body no-border text-center">
            <h4 class="mb-5">Let's get start creating your vission</h4>
            <form action="{{ route('dashboard.event-board.update.categories', $project->id) }}" method="POST" id="categorieForm">
                @csrf
                <input type="hidden" name="redirect" value="{{ route('dashboard.event-board.edit.inspirations', $project->id) }}">
                <div class="categories-container">
                    <h3 for="">Theme</h3>
                    @foreach($event_types as $event_type)
                        <div class="chips main">
                            <input type="radio" name="event_type" @if(in_array($event_type->id, $category_ids)) checked @endif  id="{{ $event_type->id }}" value="{{ $event_type->id }}" >
                            <label for="{{ $event_type->id }}"> {{ $event_type->name }}</label>
                        </div>
                    @endforeach
                </div>
                <div class="categories-container">
                    <h3 for="">Settings</h3>
                    @foreach($settings as $setting)
                        <div class="chips main">
                            <input type="radio"  name="setting" @if(in_array($setting->id, $category_ids)) checked @endif  id="{{ $setting->id }}" value="{{ $setting->id }}" >
                            <label for="{{ $setting->id }}"> {{ $setting->name }}</label>
                        </div>
                    @endforeach
                </div>
                <div class="categories-container">
                    <h3 for="">Style</h3>
                    @foreach($styles as $style)
                        <div class="chips main">
                            <input type="radio"  name="style" @if(in_array($style->id, $category_ids)) checked @endif  id="{{ $style->id }}" value="{{ $style->id }}" >
                            <label for="{{ $style->id }}"> {{ $style->name }}</label>
                        </div>
                    @endforeach
                </div>
                <div class="categories-container">
                    <h3 for="">Color</h3>
                    @foreach($colors as $color)
                        <div class="chips main">
                            <input type="checkbox"  name="colors[]" @if(in_array($color->id, $category_ids)) checked @endif  id="{{ $color->id }}" value="{{ $color->id }}" >
                            <label for="{{ $color->id }}"> {{ $color->name }}</label>
                        </div>
                    @endforeach
                </div>
            </form>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function(){
            $('#formSubmit').click(function(e){
                e.preventDefault()
                $('#categorieForm').submit();
            })
        })
    </script>
@endpush
@push('styles')
    <style>
        .categories-container{
            padding: 20px 0px;
            max-width: 500px;
            margin: 0 auto;
        }

        .categories-container label{
            margin-bottom: 10px;
        }
    </style>
@endpush
