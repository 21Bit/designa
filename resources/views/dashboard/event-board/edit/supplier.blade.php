@extends('dashboard.includes.layouts.main')

@section('page_title', 'Event Board')

@section("content-hearder")
        <h1>
            Event Board
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Event Board</li>
        </ol>
@endsection

@section('content')
    <div class="box  no-border event-board-create">
        <div class="box-header text-center with-border">
            <h2>
                Event Board Complete
            </h2>
        </div>
        <div class="box-body no-border">
            <div class="row">
                <div class="col-sm-6"><h4 class="mb-5">Here is a list of professionals who've <br> brought these moods to life.</h4></div>
                <div class="col-sm-6 text-right p-3 pr-5">
                    <a href="{{ route('dashboard.event-board.index') }}" class="btn rounded btn-lg btn-warning"><i class="fa fa-check"></i> Done</a>
                </div>
            </div>
            
            <div class="row">
                @foreach($suppliers as $supplier)
                    <div class="col-sm-4 col-md-3">
                        <div class="supplier-box shadow">
                            <div class="detail">
                                <h4 class="m-0">{{ $supplier->name }}</h4>
                                <small>{{ $supplier->roleList() }}</small>
                                <a href="">
                                    <i class="fa fa-envelope-o"></i>
                                </a>
                            </div>
                            <div class="image" style="background-image:url({{ $supplier->image('cover')->path('covers', '/images/placeholders/placeholder250x250.png') }})"></div>
                            <div class="location">
                                <div class="row">
                                    <div class="col-sm-8 col-xs-12 ">
                                        <i class="fa fa-map-marker text-orange"></i> {{ $supplier->state }}, {{ $supplier->country }}
                                    </div>
                                    <div class="col-sm-4 col-xs-12 text-orange text-right">
                                        <star-rating rating="{{ $supplier->overall_rating }}"></star-rating>
                                        
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                @endforeach
            </div> 
           
        </div>
    </div>
    <br>
    <br>
    <br>
@endsection

