@extends('dashboard.includes.layouts.main')

@section('page_title', 'Event Board')

@section("content-hearder")
        <h1>
            Event Board
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('dashboard.home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Event Board</li>
        </ol>
@endsection

@section('content')
        @include('dashboard.includes.alerts.success')
    <div class="box  no-border event-board-create">
        <div class='p-4'>
            <a href="{{ route('dashboard.event-board.index') }}" class="h3" ><i class="fa fa-arrow-left"></i></a>
            @if($pitchpack_url)
                <a href="{{ $pitchpack_url }}" target="_blank" class="pull-right btn btn-warning"><i class="fa fa-file-pdf-o"></i> Show Pitchpack</a>
            @else
                <a href="{{ route('dashboard.eventboard.generate.pitchpack', $project->id) }}" class="pull-right btn btn-warning"><i class="fa fa-file-pdf-o"></i> Generate Pitchpack</a>
            @endif
        </div>
        
        <div class="p-5">
            <div class="row">
                <div class="col-sm-6">
                    <h1 class="text-orange mt-0">
                        {{ $project->title }}
                    </h1>
                    <p>
                        {{ $project->description }}
                    </p>
                    <table>
                        <tr>
                            <td class="text-orange"><strong>Occation: </strong></label></td>
                            <td class="pl-4"> <strong >{{ $project->cater_name }}</strong></td>
                        </tr>
                        <tr>
                            <td class="text-orange"><label for="">Setting: </label></td>
                            <td class="pl-4"> <strong >{{ $project->setting_name }}</strong></td>
                        </tr>
                        <tr>
                            <td class="text-orange"><label for="">Theme:</label></td>
                            <td class="pl-4"> <strong >{{ $project->style_name }}</strong></td>
                        </tr>
                    </table>
                </div>
                <div class="col-sm-6 text-right pt-4" >
                    <div class="colors mb-4">
                        @foreach($project->colors as $color)
                            <span class="color-cicle" style="background-color:{{ $color->description }};"></span>
                        @endforeach
                    </div>
                    <div class="p-3">
                        Created By
                        <div class="text-orange">
                            {{ $project->created_by  }}
                        </div>
                    </div>
                </div>
            </div>
            <event-board-show url="{{ route('dashboard.event-board.edit.preview.api', $project->id) }}"></event-board-show>
        </div>
    </div>
@endsection

@push('styles')
    <style>
          .colors{
            /* width: 350px; */
            align-content: center;
            justify-items: center;
            align-self: center;
            justify-self: center;
        }

        .color-cicle{
            width:200px;
            border-radius: 100%;
            padding:14px 24px;
            margin:5px;
        }
    </style>
@endpush
