<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="/images/logo.ico">

    <title>{{ $project->title }}</title>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Document</title>
    <style rel="stylesheet" type="text/css">

        .text-right{
            text-align:right!important;
        }

        img{
            background-color:#fff;
            border:1px solid #dee2e6;
            border-radius:.25rem;
            max-width:30%;
            height:auto;
        }

        .text-bold{
            font-weight: bold!important;
        }

        .container-fluid{
            width:100%;
            margin-right:auto;
            margin-left:auto;
        }


        .page-break {
            page-break-after: always;
        }
    </style>
   
</head>
<body>
    <div class="container-fluid">
        <table style="width:100%;">
            <tr>
                <td>
                    <br>
                        @foreach($project->colors as $color)
                            <span style="background-color:{{ $color->description }}; height:50px; border-radius:25px;width:50px;display:inline-block;"></span>
                        @endforeach
                    <br>
                    <table>
                        <tr>
                            <td class="text-bold"><label><strong>Occation: </strong></label></td>
                            <td class="pl-4"> {{ $project->cater_name }}</td>
                        </tr>
                        <tr>
                            <td class="text-bold"><label for="">Setting: </label></td>
                            <td class="pl-4"> {{ $project->setting_name }}</td>
                        </tr>
                        <tr>
                            <td class="text-bold"><label for="">Theme:</label></td>
                            <td class="pl-4"> {{ $project->style_name }}</td>
                        </tr>
                    </table>
                </td>
                <td>
                    <div>
                        <h2 class="mb-0 text-right"> {{ strtoupper($project->title) }}</h2>
                        <div class="text-right mb-1">Created By: <span class="text-orange">{{ $project->created_by  }}</span></div>
                        <p style="font-weight: 10px; text-align:justify;" class="text-justify">
                            {{ $project->description }}
                        </p>
                    </div>
                </td>
            </tr>
        </table>
     
        <h3>Inspirations</h3>
        <br>
        <br>
        <div class="flex-container" style="margin-top:20px;">
            @foreach($project->inspirationImages as $inspirationImage)
                @if($inspirationImage->type == "gallery")
                    @if(file_exists(public_path('images/gallery/' . $inspirationImage->path)))
                        <img src="{{ public_path('images/gallery/' . $inspirationImage->path) }}" class="img-thumbnail" style='margin-top:45px;'>
                    @endif
                @else
                    @if(file_exists(public_path('images/thumbnails/' . $inspirationImage->path)))
                        <img src="{{ public_path('images/thumbnails/' . $inspirationImage->path) }}" class="img-thumbnail" style='margin-top:45px;'>
                    @endif
                @endif
            @endforeach
        </div>
     

    </div>
    <div class="page-break"></div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-6">
                <br>
                    @foreach($project->colors as $color)
                        <span style="background-color:{{ $color->description }}; height:50px; border-radius:25px;width:50px;display:inline-block;"></span>
                    @endforeach
                <br>
                <table>
                    <tr>
                        <td class="text-bold"><label><strong>Occation: </strong></label></td>
                        <td class="pl-4"> {{ $project->cater_name }}</td>
                    </tr>
                    <tr>
                        <td class="text-bold"><label for="">Setting: </label></td>
                        <td class="pl-4"> {{ $project->setting_name }}</td>
                    </tr>
                    <tr>
                        <td class="text-bold"><label for="">Theme:</label></td>
                        <td class="pl-4"> {{ $project->style_name }}</td>
                    </tr>
                </table>

            </div>
            <div class="col-xs-5" >
                <div>
                    <h2 class="mb-0 text-right"> {{ strtoupper($project->title) }}</h2>
                    <div class="text-right mb-1">Created By: <span class="text-orange">{{ $project->created_by  }}</span></div>
                    <p style="font-weight: 10px; text-align:justify;" class="text-justify">
                        {{ $project->description }}
                    </p>
                </div>
            </div>
        </div>
        <h3>Decors</h3>
        <br>
        <br>
        <div class="flex-container">
            @foreach($project->productImages()->get() as $decorImage)
                @if($decorImage->type == "thumbnail")
                    @if(file_exists(public_path('images/thumbnails/' . $decorImage->path)))
                        <img src="{{ public_path('images/thumbnails/' . $decorImage->path) }}" class="img-thumbnail" style='margin-top:45px;'>
                    @endif
                @else
                    @if(file_exists(public_path('images/gallery/' . $decorImage->path)))
                        <img src="{{ public_path('images/gallery/' . $decorImage->path) }}" class="img-thumbnail" style='margin-top:45px;'>
                    @endif
                @endif
            @endforeach
        </div>
    </div>
    </body>
</html>
