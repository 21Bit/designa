<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield("page_title", config('app.name')) - {{ config("app.name") }}</title>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="/css/dashboard.css">
    @stack('styles')
        @show
</head>
<!DOCTYPE html>
<html>
<body class="hold-transition login-page">
    <div class="login-box" id="app">
        <div class="login-box-body">
            <img class="img img-responsive mb-5 center-block" src="/images/designa-logo-sm.png" >
            <form method="POST" action="{{ route('login') }}">
                <div class="form-group has-feedback  @error('email') has-error @enderror">
                    <input type="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="Email">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    @error('email')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group has-feedback">
                    <input type="password" name="password" class="form-control" placeholder="Password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        <div class="checkbox icheck">
                            <label>
                            <input type="checkbox" name="remember" data-type="icheck"> Remember Me
                            </label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                    </div>
                    <!-- /.col -->
                </div>
                @csrf
            </form>

            <div class="social-auth-links text-center">
                <p>- OR -</p>
                <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
                    Facebook</a>
                <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
                    Google+</a>
            </div>      
                <!-- /.social-auth-links -->

      
                @if (Route::has('password.request'))
                    {{-- <a class="btn btn-link" href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a> --}}
                    <a  href="{{ route('password.request') }}">I forgot my password</a><br>
                @endif
            <a href="register.html" class="text-center">Register a new membership</a>
        </div>
    </div>
@routes
<script src="/js/dashboard.js"></script>
</body>
</html>

