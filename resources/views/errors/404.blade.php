@extends('site.includes.layouts.full')

@section('page-title', 'Page not found')

@section('content')
  {{-- <div class="page-header-img">
      <!-- <h1 class="text-orange text-center">Notification</h1> -->
      <img src="/site/images/notification-bg.jpg" class="bg-image"  alt="">
      <img class="bottom-pattern" src="/site/images/bottom-pattern.png" alt="">
  </div>  --}}
  <div class="container">
        <img src='/images/404.svg' class="mw-100">
  </div>
@endsection
