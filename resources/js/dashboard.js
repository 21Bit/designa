window._ = require('lodash');

try {
    window.$ = window.jQuery = require('jquery');
    require('croppie');
    require('bootstrap');
    require('jquery-slimscroll');
    require('fastclick');
    require('select2');
    var iCheck = require('icheck');
    require('./adminLTE');
    require('lightbox2');
    var Chart = require('chart.js');
    require("moment")
    // require("dropzone")
} catch (e) { }


window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}


import Echo from 'laravel-echo'
import swal from 'sweetalert';

window.Pusher = require('pusher-js');


window.Echo = new Echo({
    broadcaster: 'pusher',
    key: process.env.MIX_PUSHER_APP_KEY,
    cluster: process.env.MIX_PUSHER_APP_CLUSTER,
    encrypted: true
});

import Vue from 'vue';
import Vuex from 'vuex'
import store from './store';
import Toasted from 'vue-toasted';
import VueTimeago from 'vue-timeago'
import VueLazyload from 'vue-lazyload'
import VueMasonry from 'vue-masonry-css'
import VueChatScroll from 'vue-chat-scroll'
import * as VueGoogleMaps from 'vue2-google-maps'


Object.defineProperty(Vue.prototype, 'route', { value: route });

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('sub-category-manager', require('./components/Dashboard/Category/SubCategory/MainComponent').default);
Vue.component('product-type-select', require('./components/Dashboard/Product/Type/MainComponent').default);

//Ui
Vue.component('select2', require('./components/Ui/Select2').default);
Vue.component('star-rating', require('./components/Ui/StarRating').default);

// Inspiration
Vue.component('dashboard-statistics', require('./components/Dashboard/Statistics/MainComponent').default);

Vue.component('venue-setup', require('./components/Dashboard/Inspiration/Create/VenueSetUp').default);
Vue.component('venue-supplier-select', require('./components/Dashboard/Inspiration/Create/VenueSupplier').default);
Vue.component('venue-list-select', require('./components/Dashboard/Inspiration/Create/VenueList').default);
Vue.component('category-setup', require('./components/Dashboard/Inspiration/Create/CategorySetUp').default);
Vue.component('inspiration-tag-suppliers', require('./components/Dashboard/Inspiration/Tagging/MainComponent').default);
Vue.component('notification-component', require('./components/Dashboard/Notification/MainComponent').default);
Vue.component('review-component', require('./components/Dashboard/Review/MainComponent.vue').default);
Vue.component('quote-product-comment-component', require('./components/Dashboard/QuoteProductComment/MainComponent.vue').default);
Vue.component('comment-component', require('./components/Dashboard/Comment/MainComponent.vue').default);
Vue.component('address-component', require('./components/Dashboard/Address/MainComponent.vue').default);
// Vue.component('address-component', require('./components/Dashboard/Address/Edit.vue').default);
Vue.component('area-location-component', require('./components/Dashboard/AreaOfServices/MainComponent.vue').default);
Vue.component('geolocation-autocomplete', require('./components/UI/GeoLocationInput/MainComponent.vue').default);
Vue.component('inspiration-location-input', require('./components/Dashboard/Inspiration/LocationInput/MainComponent.vue').default);
Vue.component('inspiration-venue-input', require('./components/Dashboard/Inspiration/VenueSearch/MainComponent.vue').default);

Vue.component('chat-component', require('./components/Dashboard/Chat/MainComponent.vue').default);
Vue.component('product-quotation-3d-component', require('./components/Dashboard/3DProductQuotation/MainComponent.vue').default);

Vue.component('subscription-amendments-form-component', require('./components/Dashboard/SubscriptionAmendmentsForm/MainComponent.vue').default);


// Event Board Manager
Vue.component('event-board-inspiration-selection', require('./components/Dashboard/EventBoard/Creating/InspirationSelection/MainComponent.vue').default);
Vue.component('event-board-product-selection', require('./components/Dashboard/EventBoard/Creating/ProductSelection/MainComponent.vue').default);
Vue.component('event-board-preview', require('./components/Dashboard/EventBoard/Creating/Preview/MainComponent.vue').default);
Vue.component('event-board-show', require('./components/Dashboard/EventBoard/Creating/Preview/ForShow.vue').default);


Vue.use(Vuex)
Vue.use(Toasted)
Vue.use(VueLazyload)
Vue.use(VueMasonry);
Vue.use(VueChatScroll)
Vue.use(VueTimeago, {
    name: 'Timeago', // Component name, `Timeago` by default
    locale: 'en', // Default locale
  })

Vue.use(VueGoogleMaps, {
    load: {
      key: 'AIzaSyBMnJ6SDTRTpOca3b0RD3VYGdgl_WWtuak',
      libraries: 'places',
      v: '3.36',
    },
});



const app = new Vue({
    el: '#app',
    store
});

$(document).ready(function(){
    if (window.Notification && Notification.permission !== "granted") {
        Notification.requestPermission(function (status) {
          if (Notification.permission !== status) {
            Notification.permission = status;
          }
        });
    }

})

$('.trial-btn').click(function(e){
    e.preventDefault()
    var id = $(this).data('id')
    var name = $(this).data('name')
    $('#company-id-trial').val(id)
    $('#trial-company-name').html(name)
    $('#free-trial-modal').modal('show')
})

$('.cancel-trial-btn').click(function(e){
    e.preventDefault()
    var company = $(this).data('name')
    var id = $(this).data('id')
    var url = $(this).data('url')
    swal({
          title: `Are you sure to end trial for ${company}?`,
          icon: "warning",
          buttons: true,
          dangerMode: true,
    })
    .then((cancelTrial) => {
          if (cancelTrial) {
              axios.post(url, {
                  company_id: id
              })
                .then(response => {
                    location.reload()
                })
                .catch( error => {
                    console.error(error)
                })
          }
    });
})

$('.link-modal-btn').click(function(e){
    var link = $(this).data('link')
    e.preventDefault()
    $('#link-input').val(link)
    $('#link-modal').modal('show')
})

function copyToClipboard(id){
      var copyText = document.getElementById(id);
      copyText.select();
      copyText.setSelectionRange(0, 99999)
      document.execCommand("copy");
      $('.text-copy-success').show()

    setTimeout(function(){
      $('.text-copy-success').hide()
    }, 10000)
}



$(document).on('click','#checkAll', function(){
    $('input:checkbox').not(this).prop('checked', this.checked);
});

$('#delete-all-btn').click(function(){
    if(confirm("Are you sure to delete?")){
        $('#delete-all-form').submit();
    }
})

$(document).on('click', '.location-search-results', function(){
    $('.location-search-results').removeClass('active');
    $(this).addClass('active')
})

$('.open-comment-box').click(function(){

})

$('#form-with-loading').submit(function(e){
    $('.overlay-loading').css('display', 'block');
});


/*
 *  Data Button Delete
 *
*/


$(document).on('click','[data-buttons="delete"]', function(e){
    e.preventDefault()
    var url = $(this).data('url')
    var id = $(this).data('id')
    var type = $(this).data('type')
    var redirect = $(this).data('redirect')

    swal({
          title: "Are you sure to delete?",
          // text: "Once deleted, you will not be able to recover this imaginary file!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
    })
    .then((willDelete) => {
          if (willDelete) {
              axios.delete(url)
                .then(response => {
                    if(type == 'table'){
                        $(this).closest('tr').fadeOut();
                    } else if(type == "column"){
                        $('#column-' + id).fadeOut();
                    }else if(type == "div"){
                        $('#div-' + id).fadeOut();
                    }else if(type == "page"){
                        window.open(redirect, '_self')
                    }
                })
                .catch( error => {
                    console.error(error)
                })
          }
    });

    return false
})



/*
 *  Data Button Delete
 *
*/
$('[data-type="select2"]').select2()
$('[data-toggle="tooltip"]').tooltip()

/*
 *  Data Button Delete
 *
*/
$('#gallery-input').change(function () {
    $(this).closest('form').submit();
})
$('#video-input').change(function () {
    $(this).closest('form').submit();
})


$('[data-type="image-picker"]').change(function(e){

    var previewTarget = $(this).data('preview')
    var previewType = $(this).data('preview-type')
    var input = this

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            if(previewType == 'img'){
                $(previewTarget).attr('src', e.target.result);
            }else if( previewType = "label"){
                $(previewTarget).css('background-image', 'url(' + e.target.result  + ')').css('background-color', 'transparent').css('background-size', 'contain')
            }
        }

        reader.readAsDataURL(input.files[0]);

    }
})


$('input[data-type=icheck]').iCheck({
    checkboxClass: 'icheckbox_square-orange',
    radioClass: 'iradio_square-orange',
    increaseArea: '20%' /* optional */
});

$('#show-function-space-form').click(function(){
    $('#function-space-form').show()
    $(this).hide()
    $('#function-space-list').hide()
})

$('#hide-function-space-form').click(function(){
    $('#function-space-form').hide()
    $('#show-function-space-form').show()
    $('#function-space-list').show()
})

// Thumbnail Image Picker Croppie
var $resizeThumbnail = $('#croppie-container').croppie({
    enableExif: true,
    enableOrientation: true,
    viewport: {
        width: 250,
        height: 200,
        type: 'square'
    },
    boundary: {
        width: 300,
        height: 300
    }
});

$('[data-type="thumbnail-picker"]').change(function (e) {
    var input = this
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#thumbnail-image').attr('src', e.target.result)
            // $("#thumbnail-cropper-modal").modal("show")
            // $resizeThumbnail.croppie('bind', {
            //     url: e.target.result
            // }).then(function (e) {

            // });
        }
        reader.readAsDataURL(input.files[0]);
    }
})

$("#thumbnail-cropper-done").on('click', function (ev) {
    $resizeThumbnail.croppie('result', {
        type: 'canvas',
        size: 'original'
    }).then(function (img) {
        console.log(img)
        $('#thumbnail-image').attr('src', img)
        $('input[name=thumbnail]').val(img)
        $("#thumbnail-cropper-modal").modal("hide")
    });
});

function validate(formData, jqForm, options) {
    var form = jqForm[0];
    if (!form.file.value) {
        alert('File not found');
        return false;
    }
}


// var progressDiv = $('.progress')
// var progressBar = $('.progress-bar')

// $('.upload-file-form').ajaxForm({
//     beforeSubmit: validate,
//     beforeSend: function() {
//         progressDiv.show()
//         status.empty();
//         var percentVal = '0%';
//         progressBar.css('width',percentVal)
//     },
//     uploadProgress: function(event, position, total, percentComplete) {
//         var percentVal = percentComplete + '%';
//         progressBar.css('width', percentVal)
//     },
//     success: function() {
//         var percentVal = 'Wait, Saving';
//         progressBar.html('Processing..')
//     },
//     complete: function(xhr) {
//         // status.html(xhr.responseText);
//         alert('Uploaded Successfully');
//         window.location.href = "/file-upload";
//     }

// });
