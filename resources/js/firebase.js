
  // firebase
var firebase = require('firebase/app').default;
require('firebase/auth');
require('firebase/database');

var config = {
  apiKey: "AIzaSyBCIWEAg4qcw_TLZvXdtciciNJGKoWI0aA",
  authDomain: "designa-4096b.firebaseapp.com",
  databaseURL: "https://designa-4096b.firebaseio.com",
  projectId: "designa-4096b",
  storageBucket: "designa-4096b.appspot.com",
  messagingSenderId: "331645019413",
  appId: "1:331645019413:web:7f833fc961b1aa7061b51c"
};

let firebaseApp = firebase.initializeApp(config)
var firebaseDb = firebase.database()

export {
  firebaseDb,
  firebaseApp
}
