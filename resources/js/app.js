require('./bootstrap');


require('lightbox2');
window.Masonry = require('masonry-layout');

//variables
let url = "";
let id = "";
let type = "";

window.Vue = require('vue').default;
// import AutocompleteVue from 'autocomplete-vue';
import VueMasonry from 'vue-masonry-css'


Vue.component(
    'main-search-component',
    require('./components/Site/MainSearch/MainComponent.vue').default
);
Vue.component(
    'modal-login-component',
    require('./components/Site/LoginModal/MainComponent.vue').default
);
Vue.component(
    'venue-filter-component',
    require('./components/Site/Venue/MainComponent.vue').default
);
Vue.component(
    'venue-related-component',
    require('./components/Site/Venue/Supplier/RelatedVenueComponent.vue').default
);
Vue.component(
    'supplier-venue-filter-component',
    require('./components/Site/Venue/Supplier/MainComponent.vue').default
);
Vue.component(
    'inspiration-filter-component',
    require('./components/Site/Inspiration/MainComponent.vue').default
);
Vue.component(
    'supplier-product-filter-component',
    require('./components/Site/Product/SupplierProduct/MainComponent.vue').default
);
Vue.component(
    'inspiration-filter-index-component',
    require('./components/Site/Inspiration/ForIndexLayout.vue').default
);
Vue.component(
    'supplier-inspiration-component',
    require('./components/Site/Inspiration/SupplierInspiration.vue').default
);
Vue.component(
    'inspiration-tagged-component',
    require('./components/Site/Inspiration/TaggedInspirationComponent.vue').default
);
Vue.component(
    'inspiration-tagged-product',
    require('./components/Site/Inspiration/Product/Item.vue').default
);
Vue.component(
    'suplier-filter-component',
    require('./components/Site/Supplier/MainComponent.vue').default
);
Vue.component(
    'product-filter-component',
    require('./components/Site/Product/MainComponent.vue').default
);
Vue.component(
    'supplier-product-filter-component',
    require('./components/Site/Product/SupplierProduct/MainComponent.vue').default
);
Vue.component(
    'review-component',
    require('./components/Site/Review/MainComponent.vue').default
);
Vue.component(
    'profile-dropdown',
    require('./components/Site/DropdownMenu/MainComponent.vue').default
);
Vue.component(
    'heart-component',
    require('./components/Site/Heart/MainComponent.vue').default
);
Vue.component(
    'create-qoute-component',
    require('./components/Site/Qoute/CreateNewQoute/MainComponent.vue').default
);
Vue.component(
    'geolocation-autocomplete',
    require('./components/UI/GeoLocationInput/MainComponent.vue').default
);
Vue.component(
    'inquiery-box',
    require('./components/Site/InquiryBox/MainComponent.vue').default
);
Vue.component(
    'request-form',
    require('./components/Site/RequestForm/MainComponent.vue').default
);
Vue.component(
    'request-form-venue-tours',
    require('./components/Site/RequestForm/ForVenueTours.vue').default
);
Vue.component(
    'request-form-covid',
    require('./components/Site/RequestFormCovid/MainComponent.vue').default
);Vue.component(
    'request-form-covid-venue-directory',
    require('./components/Site/RequestFormCovidVenueDirectory/MainComponent.vue').default
);
Vue.component(
    'email-register',
    require('./components/Site/EmailRegister/MainComponent.vue').default
);
Vue.component(
    'newsletter-email',
    require('./components/Site/NewsLetterSender/MainComponent.vue').default
);
Vue.component(
    'request-white-glove-service',
    require('./components/Site/RequestWhiteGloveService/MainComponent.vue').default
);
Vue.component(
    'request-white-glove-service-venue-tours',
    require('./components/Site/RequestWhiteGloveService/ForVenueTours.vue').default
);
Vue.component(
    'webinar-registration-form',
    require('./components/Site/WebinarRegistrationForm/MainComponent.vue').default
);
Vue.component('venue-seach',
    require('./components/Ui/VenueSearch/MainComponent1.vue').default
);

Vue.component('geolocation-input-with-distance',
    require('./components/Ui/GeoLocatioInputDistance/MainComponent.vue').default
);

Vue.component('request-3d-form',
    require('./components/Site/Request3dForms/MainComponent.vue').default
);
Vue.component('contest-3d-registration-form',
    require('./components/Site/Contest3dRegistration/MainComponent.vue').default
);
Vue.component('test-cors',
    require('./components/Tools/TestCors/MainComponent.vue').default
);

Vue.component('venue-marketing-usa-form',
    require('./components/Site/LandingUsaMarketingForm/MainComponent.vue').default
);


Vue.component('user-event-board-inspiration-selection',
    require('./components/User/EventBoard/InspirationSelection/MainComponent.vue').default
);

Vue.component('user-event-board-decor-selection',
    require('./components/User/EventBoard/DecorSelection/MainComponent.vue').default
);
Vue.component('user-event-board-preview',
    require('./components/User/EventBoard/PreviewComponent.vue').default
);

// Vue.component(
//     'autocomplete-vue',
//     AutocompleteVue);

import VueLazyload from 'vue-lazyload'

import axios from 'axios';
Vue.use(VueLazyload)
Vue.use(VueMasonry);

const app = new Vue({
    el: '#app',
});


import Echo from 'laravel-echo'

window.Pusher = require('pusher-js');

window.Echo = new Echo({
    broadcaster: 'pusher',
    key: process.env.MIX_PUSHER_APP_KEY,
    cluster: process.env.MIX_PUSHER_APP_CLUSTER,
    encrypted: true
});


$(document).ready(function  (){
    $('body').on('show.bs.modal', function () {
        $('.sticky-top').css('margin-left', '-=0px');
        $('.sticky-top').css('padding-right', '0px');
        $('body').removeAttr('style');
    });




    $('body').on('hidden.bs.modal', function () {
        $('.sticky-top').css('margin-left', 'auto');
    });

    $('.openModalLogin').click(function(e){
        e.preventDefault()
        $('#loginModal').modal('show')
        $('#loginModal li > a[href="#login"]').tab("show");
    })

    $('.openModalRegister').click(function(){
        $('#loginModal').modal('show')
        $('#loginModal li > a[href="#register"]').tab("show");
    })

    $(document).on('click','.quoting-button-add', function(){
        $(this).html('<i class="fa fa-check"></i>')
        console.log("Yeah")
    });

    $('.load-more-content').show();
    $('.show-less').show()

    $('.show-more').click(function(){
        var target = $(this).data('target')
        $(target).show()
        $('.show-less').show()
        $(this).hide();
    })

    $('.show-less').click(function(){
        var target = $(this).data('target')
        $(target).hide()
        $(this).hide()
        $('.show-more').show()
    })

    $('[data-buttons="delete"]').click(function(){
        $('#delete-modal').modal("show")
     //   if(confirm("Are you sure to delete?")){
            url = $(this).data('url');
            id = $(this).data('id');
            type = $(this).data('type');
      //  }
        return false
    })

    $('#deleteButtonModal').click(function(){
        axios.delete(url)
            .then(response => {
                if(type == 'table'){
                    $(this).closest('tr').fadeOut();
                } else if(type == "column"){
                    $('#column-' + id).fadeOut();
                }else if(type == "div"){
                    $("#div-" + id).fadeOut();
                }

                $('#delete-modal').modal("hide")
                url = '';
                id = '';
                type = '';
            })
            .catch( error => {
                console.error(error)
            })
    })

    $('#deleteAllNotificationButtonModal').click(function(){
        var deleteAllUrl = $(this).data('url')
        axios.delete(deleteAllUrl)
            .then(response => {
                $('.media').remove();
                $('#delete-all-notification-modal').modal("hide")
            })
            .catch( error => {
                console.error(error)
            })
    });
})


data: [
    {
        venue_name: "sample",
        event_spaces: [
            {
                id: 1,
                name: "samplename",
                hasAssetBundle: false
            },
            {
                id: 1,
                name: "samplename",
                hasAssetBundle: false
            },
        ]
    },
      {
        venue_name: "sample 1",
        event_spaces: [
            {
                id: 1,
                name: "samplename",
                hasAssetBundle: false
            },
            {
                id: 1,
                name: "samplename",
                hasAssetBundle: false
            },
        ]
    },

]
