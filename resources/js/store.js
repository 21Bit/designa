import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

const state = {
    
    //categories
    categories: [],
    selectedCategory:{},
    categorySettings: [],
    categoryStyles:[],

    //company
    companies:[],

    //venue
    venueSupplier:{},
    venues:[]
}


const getters = {

    //for categories
    categories : state => state.categories,
    selectedCategory: stage => state.selectedCategory,
    categorySettings: state => state.categorySettings,
    categoryStyles: state => state.categoryStyles,

    //for company
    companies: state => state.companies,
    venueSupplier: state => state.venueSupplier,
    venues: state => state.venues
}

const mutations = {

    //for categories
    categories: (state, categories) => (state.categories =  categories),
    setSelectedCategory:(state, id) => (state.selectedCategory = _.find(state.categories, {'id': id})),
    setCategorySettings: (state) => (state.categorySettings = state.selectedCategory.settings),
    setategoryStyles: (state) => (state.categoryStyles = state.selectedCategory.styles),
    
    //for company
    setCompanies: (state, payload) => (state.companies = payload),

    //for venue
    setVenueSupplier: ( state, supplier) => (state.venueSupplier = supplier),
    setVenues: (state, venues) => (state.venues = venues)
}

const actions = {
    //for categories
    async loadCategories({ commit }, url) {
        axios.get( url )
            .then (response => {
                commit( 'categories', response.data )
            })
            .catch( error => {
                console.log(error)
            })
    },

    async setSelectedCategory({commit}, id){
        commit('setSelectedCategory', id);
    },

    async setVenueSupplier( { commit } , supplier){
        commit('setVenueSupplier', supplier);
    },

    async getCompanies({ commit }, url){

        axios.post(url)
            .then (response => {
                var payload = response.data
                commit('setCompanies', payload);
            })
    },

    async getVenues({commit}, url){
        axios.post(url)
            .then(response => {
                commit('setVenues', response.data)
            })
            .catch( error => {
                console.log(error)
            })
    }

    
}

export default new Vuex.Store({
    state, mutations, actions, getters
});

