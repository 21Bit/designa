

window.axios = require('axios');
window.Vue = require('vue');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

Vue.component(
    'event-registration-form',
    require('./components/Site/EventRegistration/MainComponent.vue').default
);


const app = new Vue({
    el: '#app',
});


