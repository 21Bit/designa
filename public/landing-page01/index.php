<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Designa Studio</title>
        <link rel="icon" type="image/x-icon" href="https://designa.studio/images/logo.ico" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link href="css/styles.css" rel="stylesheet" />
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>

        <link href="https://fonts.googleapis.com/css2?family=Spartan:wght@400;500;700&display=swap" rel="stylesheet">        
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-white bg-white fixed-top" id="mainNav">
            <div class="container px-4">
                <a class="navbar-brand"><img src="img/designa-logo.png" alt="Designa Studio" class="logo"></a>
                <button class="navbar-toggler navbar-light" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ms-auto">
                        <li class="nav-item"><a class="nav-link" href="#page-top">home</a></li>
                        <li class="nav-item"><a class="nav-link" href="#about">about</a></li>
                        <li class="nav-item"><a class="nav-link" href="#benefits">benefits</a></li>
                        <li class="nav-item"><a class="nav-link" href="#comparison">comparison</a></li>
                        <li class="nav-item"><a class="nav-link" href="#pricing">pricing</a></li>
                        <li class="nav-item"><a class="nav-link" href="#contact">contact</a></li>
                        <li class="nav-item"><a class="nav-link btn-try" href="https://designa.studio/for-supplier/supplier-hub/#demo" target="_blank">Try It Free </a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Header-->
        <header class="landing-page-banner1 text-white">
            <div class="container px-4 text-center">
                <h1 class="fw-bolder">Designed to be helpful</h1>
                <p class="lead">Our platform lets you make changes to your event with ease, following health standards to minimise the transmission of COVID-19.</p>                
            </div>
        </header>
        <!-- About section-->
        <section id="about" class="bg-light">
            <div class="container px-4">
                <div class="row gx-4 justify-content-center">
                    <div class="col-lg-12">
                        <h2 class="text-center">Plan With Confidence</h2>
                        <p class="lead">We're to take all uncertainty out of your planning and give you the utmost confidence to make your event safe and secure.</p>
                        <div class="text-center">
                            <img src="img/plan-banner.png" alt="" class="img-fluid">
                        </div>    
                    </div>
                </div>

                <div class="row gx-4 text-center">
                    <div class="col-lg-4">
                        <div class="plan-box">
                            <h6>Design With Safety</h6>
                            <ul>
                                <li>Remodel floor plans and seating arrangements that observe social distancing and other minimum health protocols.</li>
                            </ul>
                        </div>    
                    </div>
                    <div class="col-lg-4">
                        <div class="plan-box">
                            <h6>Meet With Serenity</h6>
                            <ul>
                                <li>Stay connected with your clients and other suppliers with our built-in messaging and video features.</li>
                            </ul>
                        </div>   
                    </div>
                    <div class="col-lg-4">
                        <div class="plan-box">
                            <h6>Mindblowing Performance</h6>
                            <ul>
                                <li>Your venue is stunning 3D</li>
                                <li>Transform floorplans instantly</li>
                                <li>Boost your brand with Designa</li>
                            </ul>
                        </div>    
                    </div>
                </div>

                <div class="row gx-4 text-center">
                    <div class="col-lg-12">
                        <a href="https://designa.studio/register/supplier" target="_blank"><button type="button" class="btn-signup">SIGN UP</button></a>
                    </div>
                </div>
            </div>
        </section>
        <!-- Benefits section-->
        <section class="bg-white" id="benefits">
            <div class="container px-4">
                <div class="row gx-4 justify-content-center">
                    <div class="col-lg-6 text-center">
                        <h2>Marvelous Benefits</h2>
                        <ul class="benefits-list">
                            <li><i class="fa fa-check-circle check-benefits" aria-hidden="true"></i> It increases venue bookings</li>
                            <li><i class="fa fa-check-circle check-benefits" aria-hidden="true"></i> It Widens Exposure To Events Organizers</li>
                            <li><i class="fa fa-check-circle check-benefits" aria-hidden="true"></i> It Upsells Your Products and Services</li>
                            <li><i class="fa fa-check-circle check-benefits" aria-hidden="true"></i> It Launches Anytime, Anywhere</li>
                            <li><i class="fa fa-check-circle check-benefits" aria-hidden="true"></i> It is Effortless</li>
                        </ul>
                        <div class="join">Join Today, it's Free!</div>
                        <a href="https://designa.studio/register/supplier" target="_blank"><button type="button" class="btn-signup2">SIGN UP</button></a>                       
                    </div>
                    <div class="col-lg-6">
                       <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                          <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>     
                          </ol>
                          <div class="carousel-inner">
                            <div class="carousel-item active">
                              <img class="d-block w-100 img-fluid" src="img/slider-image01.png" alt="">
                            </div>
                            <div class="carousel-item">
                              <img class="d-block w-100 img-fluid" src="img/slider-image02.png" alt="">
                            </div>                               
                          </div>                          
                        </div>                      
                    </div>
                </div>
            </div>
        </section>
        <!-- Comparison section-->
        <section class="bg-light" id="comparison">
            <div class="container px-4">
                <div class="row gx-4">
                    <div class="col-lg-4">                        
                        <div class="comparison-box">                            
                            <h6 style="padding-top: 27px;">Why Choose Designa?</h6>
                            <ul>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>See your event in 3D instantly</div></li>
                                <li><i class="fa fa-check-square-o check-benefits" aria-hidden="true"></i> <div>Shortlist your favorite venues, suppliers, and planners</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Boost your brand with Designa</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>User-friendly interface</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Easy to find inspirations</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Provides ultra-realistic floorplan</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Accepts add-ons services</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Create digital catalog</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Compatible in Web, Android, and iOS mobile application</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Get expert advice from professionals</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Full quoting inbuilt system</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Live chat with prospective clients</div></li>
                            </ul>
                        </div>    
                    </div>
                    <div class="col-lg-4">
                        <div class="comparison-box">                            
                            <h6>What do you get with online directory services?</h6>
                            <ul>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Irrelevant leads</div></li>
                                <li><i class="fa fa-check-square-o check-benefits" aria-hidden="true"></i> <div>Complex structure for users</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Expensive subscription plan</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>No control to filter trolls</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Hiddent costs</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Low ROI</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Increased clutter</div></li>                                
                            </ul>
                        </div>   
                    </div>
                    <div class="col-lg-4">
                        <div class="comparison-box">                            
                            <h6>What to do you get with online services?</h6>
                            <ul>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>Grouped with irrelevant services</div></li>
                                <li><i class="fa fa-check-square-o check-benefits" aria-hidden="true"></i> <div>Vague information</div></li>
                                <li><i class="fa fa-check-square-o check-benefits2" aria-hidden="true"></i> <div>One size fits all offering</div></li>                                
                            </ul>
                        </div>    
                    </div>
                </div>

                <div class="row gx-4">
                    <div class="col-lg-12 text-center" style="padding-top: 50px;"> 
                        <h2>START YOUR 1 MONTH FREE TRIAL</h2>
                        <div>
                            <a href="https://youtu.be/CJmUEeZcwZE" target="_blank"><img src="img/video.png" alt="" class="img-fluid video-image"></a>
                        </div>
                    </div>    
                </div>
            </div>
        </section>
        <!-- Subscription section-->
        <section class="bg-white" id="pricing">
            <!-- Table -->
<div class="subs-content subs-desktop bg-white">
    <div class="container">
        <div class="row">
                    <div class="col-lg-12 text-center"> 
                        <h2>Subscription</h2>
                        <p style="padding-bottom: 40px; padding-top: 0;" class="lead">Upgrade to Pro</p>
                    </div>    
                </div>
        <div class="row">                        
            <table class="table  table-borderless table-responsive-sm">
                <thead>
                <tr>
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col" >
                        <h5 class="subs-header-popular rounded-top">Most Popular</h5>
                    </th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <thead>
                <tr>
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col" class="subs-header-lg">
                        <h5 >Beginner</h5>
                        <h4>$0.00</h4>
                        <h5>Per Month</h5>
                    </th>
                    <th scope="col"></th>
                    <th scope="col" class="subs-header-sm">
                        <h5>Intermediate</h5>
                        <h4>$0.00</h4>
                        <h5>Per Month</h5>
                    </th>
                    <th scope="col"></th>
                    <th scope="col" class="subs-header-sm">
                        <h5>Advance</h5>
                        <h4>$0.00</h4>
                        <h5>Per Month</h5>
                    </th>
                    <th scope="col"></th>
                    <th scope="col" class="subs-header-sm">
                        <h5>Pro</h5>
                        <h4>$0.00</h4>
                        <h5>Per Month</h5>
                    </th>
                </tr>
              </thead>
              <tbody class="bg-light">                
                <tr>
                    <td class="shadow p-3 mb-5 bg-white rounded">
                        <h6>Online Presence</h6>
                    </td>
                    <td ></td>
                    <td class="shadow p-3 mb-5 bg-white rounded"></td>
                    <td></td>
                    <td class="shadow p-3 mb-5 bg-white rounded"></td>
                    <td></td>
                    <td class="shadow p-3 mb-5 bg-white rounded"></td>
                    <td></td>
                    <td class="shadow p-3 mb-5 bg-white rounded"></td>
                </tr>
                <tr>
                    <td class="shadow p-3 mb-5 bg-white rounded">
                        <span>Create an Account</span>
                    </td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                </tr>
                <tr>
                    <td class="shadow p-3 mb-5 bg-white rounded">
                        <span>Load Product</span>
                    </td>
                    <td></td>
                    <td class="shadow p-3 mb-5 bg-white rounded"></td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>                    
                </tr>
                <tr>
                    <td class="shadow p-3 mb-5 bg-white rounded">
                        <span>Digital Catalogue / Look Book</span>
                    </td>
                    <td></td>
                    <td class="shadow p-3 mb-5 bg-white rounded"></td>
                    <td></td>
                    <td class="shadow p-3 mb-5 bg-white rounded"></td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                </tr>
                <tr>
                    <td class="shadow p-3 mb-5 bg-white rounded">
                       <span> Your Products in 3D / 10+ is Pro</span>
                    </td>
                    <td></td>
                    <td class="shadow p-3 mb-5 bg-white rounded"></td>
                    <td></td>
                    <td class="shadow p-3 mb-5 bg-white rounded"></td>
                    <td></td>
                    <td class="shadow p-3 mb-5 bg-white rounded"></td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                </tr>
                <tr>
                    <td class="shadow p-3 mb-5 bg-white rounded2">
                        <span>Immersive 3D customised room styling using template</span>
                    </td>
                    <td></td>
                    <td class="shadow p-3 mb-5 bg-white rounded2"></td>
                    <td></td>
                    <td class="shadow p-3 mb-5 bg-white rounded2"></td>
                    <td></td>
                    <td class="shadow p-3 mb-5 bg-white rounded2"></td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded2"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>                    
                </tr>
               
                <tr>
                    <td class="shadow p-3 mb-5 bg-white rounded">
                        <h6>Showcasing yourself</h6>
                    </td>
                    <td></td>
                    <td class="shadow p-3 mb-5 bg-white rounded"></td>
                    <td></td>
                    <td class="shadow p-3 mb-5 bg-white rounded"></td>
                    <td></td>
                    <td class="shadow p-3 mb-5 bg-white rounded"></td>
                    <td></td>
                    <td class="shadow p-3 mb-5 bg-white rounded"></td>                    
                </tr>
                <tr>
                    <td class="shadow p-3 mb-5 bg-white rounded">
                        <span>Load Inspiration</span>
                    </td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                </tr>
                <tr>
                    <td class="shadow p-3 mb-5 bg-white rounded">
                        <span>Tag and be tagged</span>
                    </td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                    <td ></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                </tr>
                <tr>
                    <td class="shadow p-3 mb-5 bg-white rounded">
                        <span>Communicate with customers</span>
                    </td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                </tr>
                <tr>
                    <td class="shadow p-3 mb-5 bg-white rounded">
                        <span>Promote using featured marketing</span>
                    </td>
                    <td></td>
                    <td class="shadow p-3 mb-5 bg-white rounded"></td>
                    <td></td>
                    <td class="shadow p-3 mb-5 bg-white rounded"></td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                </tr>
                <tr>
                    <td class="shadow p-3 mb-5 bg-white rounded2">
                        <span>Register for the Designa VR Expo</span>
                    </td>
                    <td></td>
                    <td class="shadow p-3 mb-5 bg-white rounded2"></td>
                    <td></td>
                    <td class="shadow p-3 mb-5 bg-white rounded2"></td>
                    <td></td>
                    <td class="shadow p-3 mb-5 bg-white rounded2"></td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded2"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                </tr>
                <tr>
                    <td class="shadow p-3 mb-5 bg-white rounded">
                        <h6>Build your brand</h6>
                    </td>
                    <td></td>
                    <td class="shadow p-3 mb-5 bg-white rounded"></td>
                    <td></td>
                    <td class="shadow p-3 mb-5 bg-white rounded"></td>
                    <td></td>
                    <td class="shadow p-3 mb-5 bg-white rounded"></td>
                    <td></td>
                    <td class="shadow p-3 mb-5 bg-white rounded"></td>
                </tr>
                <tr>
                    <td class="shadow p-3 mb-5 bg-white rounded">
                        <span>Receive and Fulfill Quotes</span>
                    </td>
                    <td></td>
                    <td class="shadow p-3 mb-5 bg-white rounded"></td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                </tr>
                <tr>
                    <td class="shadow p-3 mb-5 bg-white rounded">
                        <span>Create Pitch Packs in Designa</span>
                    </td>
                    <td></td>
                    <td class="shadow p-3 mb-5 bg-white rounded"></td>
                    <td></td>
                    <td class="shadow p-3 mb-5 bg-white rounded"></td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                </tr>
                <tr>
                    <td class="shadow p-3 mb-5 bg-white rounded">
                        <span>Create your own event 3D using template</span>
                    </td>
                    <td></td>
                    <td class="shadow p-3 mb-5 bg-white rounded"></td>
                    <td></td>
                    <td class="shadow p-3 mb-5 bg-white rounded"></td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                </tr>
                <tr>
                    <td class="shadow p-3 mb-5 bg-white rounded">
                        <span>Transform a room with custom 3D Builds</span>
                    </td>
                    <td></td>
                    <td class="shadow p-3 mb-5 bg-white rounded"></td>
                    <td></td>
                    <td class="shadow p-3 mb-5 bg-white rounded"></td>
                    <td></td>
                    <td class="shadow p-3 mb-5 bg-white rounded"></td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded"><i class="fa fa-check-circle subs-fa" aria-hidden="true"></i></td>
                </tr>
                <tr>
                    <td class="shadow p-3 mb-5 bg-white rounded"></td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded">
                        <a href="#" class="btn-buy-now">Choose this plan</a>
                    </td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded">
                        <a href="#" class="btn-buy-now ">Choose this plan</a>
                    </td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded">
                        <a href="#" class="btn-buy-now">Choose this plan</a>
                    </td>
                    <td></td>
                    <td class="text-center shadow p-3 mb-5 bg-white rounded">
                        <a href="#" class="btn-buy-now">Choose this plan</a>
                    </td>
                </tr>
              </tbody>
                
            </table>
        </div>
    </div>
</div>
<!-- end of table -->
        </section>

<!-- Mobile -->
<div class="container subs-mobile">    
    <div id="accordion">

        <!-- 1st Card -->
        <div class="card">
            <div class="card-header" id="headingOne" style="background: #a1a1a1;">
                <div>
                    <h5 class="mb-0">
                        <button class="btn btn-link btn-plan-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Beginner    
                        </button>
                    </h5>
        

                    <div class="accordion-plan-details">
                        <h5 class="mb-0">$00.00</h5>
                        <h6>Per Month</h6>
                    </div>        
                </div>
                <div class="accordion-clear"></div>
            </div>
        

            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body accordion-body-bg">
                    <ul class="accordion-list">
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Create an Account</li>
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Load Inspiration</li>
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Tag and be tagged</li>
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Communicate with customers</li>
                    </ul>
                    <div class="text-center">            
                        <a href="#"><button type="button" class="btn-buy-now-mobile">Choose this plan</button></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- 1st Card -->

        <!-- 2nd Card -->
        <div class="card">
            <div class="card-header" id="headingTwo" style="background: #a1a1a1;">
                <div>
                    <h5 class="mb-0">
                        <button class="btn btn-link btn-plan-link" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                            Intermediate   
                        </button>
                    </h5>
        

                    <div class="accordion-plan-details">
                        <h5 class="mb-0">$00.00</h5>
                        <h6>Per Month</h6>
                    </div>        
                </div>
                <div class="accordion-clear"></div>
            </div>
        

            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                <div class="card-body accordion-body-bg">
                    <ul class="accordion-list">
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Create an Account</li>
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Load Product</li>
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Load Inspiration</li>
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Tag and be tagged</li>
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Communicate with customers</li>
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Receive and Fulfill Quotes</li>
                    </ul>
                    <div class="text-center">            
                        <a href="#"><button type="button" class="btn-buy-now-mobile">Choose this plan</button></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- 2nd Card -->

        <!--3rd Card -->
        <div class="card">
            <div class="card-header" id="headingThree" style="background: #f29833">
                <div>
                    <h5 class="mb-0">
                        <button class="btn btn-link btn-plan-link" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                            Advance   
                        </button>
                    </h5>
        

                    <div class="accordion-plan-details">
                        <h5 class="mb-0">$00.00</h5>
                        <h6>Per Month</h6>
                    </div>        
                </div>
                <div class="accordion-clear"></div>
            </div>
        

            <div id="collapseThree" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                <div class="card-body accordion-body-bg">
                    <ul class="accordion-list">
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Create an Account</li>
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Load Product</li>
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Digital Catalogue / Look Book</li>
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Your Products in 3D / 10+ is Pro</li>
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Loadin Inspiration</li>
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Tag an be tagged</li>
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Communicate with customers</li>
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Promote using featured</li>
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Receive and Fulfill Quotes</li>
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Create Pitch Packs in Designa</li>
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Create you own event 3D using template</li>
                    </ul>
                    <div class="text-center">            
                        <a href="#"><button type="button" class="btn-buy-now-mobile">Choose this plan</button></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- 3rd Card -->

        <!--4th Card -->
        <div class="card">
            <div class="card-header" id="headingFour" style="background: #a1a1a1;">
                <div>
                    <h5 class="mb-0">
                        <button class="btn btn-link btn-plan-link" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                            Pro   
                        </button>
                    </h5>
        

                    <div class="accordion-plan-details">
                        <h5 class="mb-0">$00.00</h5>
                        <h6>Per Month</h6>
                    </div>        
                </div>
                <div class="accordion-clear"></div>
            </div>
        

            <div id="collapseFour" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                <div class="card-body accordion-body-bg">
                    <ul class="accordion-list">
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Create an Account</li>
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Load Product</li>
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Digital Catalogue / Look Book</li>
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Your Products in 3D / 10+ is Pro</li>
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> 3D Virtual Showroom</li>
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Immersive 3D customised room styling using template</li>
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Loadin Inspiration</li>
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Tag an be tagged</li>
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Communicate with customers</li>
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Promote using featured</li>
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Register for the Designa VR Expo</li>
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Receive and Fulfill Quotes</li>
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Create Pitch Packs in Designa</li>
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Create you own event 3D using template</li>
                        <li><i aria-hidden="true" class="fa fa-check-circle subs-fa-mobile"></i> Transform a room with custom 3D Builds</li>
                    </ul>
                    <div class="text-center">            
                        <a href="#"><button type="button" class="btn-buy-now-mobile">Choose this plan</button></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- 4th Card -->
    </div>
</div>
<!-- Mobile -->

        <!-- Contact section-->
        <section id="contact" class="bg-light">
            <div class="container px-4">
                <div class="row gx-4 justify-content-center">
                    <div class="col-lg-8 text-center" id="app">
                        <h2>Need more information</h2>
                        <p class="lead">Request a demo now</p>
                        <!-- <form>
                          <div class="form-group">                            
                            <input type="text" class="form-control" id="address" name="address" aria-describedby="emailHelp" placeholder="Address">                            
                          </div>
                          <div class="form-group">
                            <input type="text" class="form-control" id="contact" name="contact" aria-describedby="emailHelp" placeholder="Contact Number">                            
                          </div>
                          <div class="form-group">
                            <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Email Address"> 
                          </div>
                          <button type="submit" class="btn-signup2">Submit</button>
                        </form> -->
                        <request-form></request-form>
                    </div>
                </div>
            </div>
        </section>
        <!-- Footer-->
        <footer class="py-5 bg-white">
            <div class="container px-4">
                <div class="row gx-4 justify-content-center">
                    <div class="col-lg-12 text-center">
                        <div class="social-circle">
                            <a href="https://www.facebook.com/Designa-Studio-260657538157535/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        </div>
                        <div class="social-circle">
                            <a href="https://www.linkedin.com/company/designastudio/" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                        </div>
                        <div class="social-circle">
                            <a href="https://www.instagram.com/designastudio.au/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        </div>
                        <p style="font-size: 14px; padding-top: 20px;">
                            Follow us to receive instant update<br>
                            <a href="mailto:hello@designa.studio" style="color: #4E4E4E;">hello@designa.studio</a>
                        </p>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
        <script src='https://designa.studio/js/app.js' type="3fdf24a46cbb971ce9cc62d4-text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous" type="3fdf24a46cbb971ce9cc62d4-text/javascript"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous" type="3fdf24a46cbb971ce9cc62d4-text/javascript"></script>

<script src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js" data-cf-settings="3fdf24a46cbb971ce9cc62d4-|49" defer=""></script>

<script src="/js/app.js" type="3fdf24a46cbb971ce9cc62d4-text/javascript"></script>


    </body>
</html>
