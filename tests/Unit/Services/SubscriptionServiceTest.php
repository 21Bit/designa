<?php

namespace Services;

use App\Models\User;
use App\Services\SubscriptionService;
use Tests\TestCase;

class SubscriptionServiceTest extends TestCase
{
    /** @test */
    public function user_can_have_free_trial()
    {
        $user = User::factory()->create(
            ['trial_ends_at' => now()->addDays(10)]
        );

        $this->assertTrue($user->onTrial());
        $this->assertFalse($user->onTrial('Designa  Plan'));

    }

}
