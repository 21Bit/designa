<?php

namespace Console\Commands;

use App\Console\Commands\CreateUserForCompany;
use App\Models\Company;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreateUserForCompanyTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function command_is_working()
    {
        $company = Company::factory()->create();
        $this->artisan("command:create-company-user")
            ->expectsQuestion('1 companies found. Are you sure to procced?[y,n]');
    }
}
