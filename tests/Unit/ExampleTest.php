<?php
test('google place is working', function(){
    $result = (new App\Services\GoogleService)->placeAutoComplete("Sydney");
    $this->assertTrue(count($result->predictions) > 0);
});
