<?php

namespace Database\Seeders;


use App\Models\SubscriptionPlan;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SubscriptionPlanAndPriceSetUpTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_runs_correctly()
    {
        $this->artisan("db:seed", ['--class' => 'SubscriptionPlanAndPriceSetUp']);
        $this->assertDatabaseCount(SubscriptionPlan::class, 6);
    }
}
