<?php

use Illuminate\Foundation\Testing\LazilyRefreshDatabase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

uses(TestCase::class, LazilyRefreshDatabase::class)->in('Feature');

expect()->extend('toBeOne', function () {
    return $this->toBe(1);
});

function actingAsSuperAdmin($isApi = false): TestCase
{
    $user = \App\Models\User::factory()->create(['type' => 'supplier', 'is_superadmin' => 1, 'company_id' => \App\Models\Company::factory()->create()]);
    return test()->actingAs($user, $isApi ? 'api' : '');
}

function actingAsSupplier($isApi = false): TestCase
{
    $user = \App\Models\User::factory()->create(['type' => 'supplier', 'company_id' => \App\Models\Company::factory()->create()]);
    return test()->actingAs($user, $isApi ? 'api' : '');
}


function something()
{
    // ..
}
