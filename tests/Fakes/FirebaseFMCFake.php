<?php

namespace Tests\Fakes;

use App\Facades\FirebaseFMC;
use Illuminate\Support\Facades\Http;

class FirebaseFMCFake extends FirebaseFMC
{
    public function sendSingle($firebase_ids, $data) {
        return true;
    }

    public static function setUp(): void
    {
        app()->instance(FirebaseFMC::class, new self());
    }
}
