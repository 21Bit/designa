<?php

namespace Tests\Feature\Http\Controllers\Site;

use App\Http\Controllers\Site\VenueController;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class VenueControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function index_can_render()
    {
        $this
            ->get('/venue')
            ->assertStatus(200)
            ->assertSee("Inspiration");
    }
}
