<?php
use function Pest\Faker\faker;

test('customer register register form can be render')->get('register/customer')->assertStatus(200)->group('Web');

test('supplier register form can be render')->get('register/supplier')->assertStatus(200)->group('Web');

test('customer can register in web', function () {
    $response = $this->post('/register/customer', [
        'name' => faker()->name,
        'email' => faker()->safeEmail,
        'password' => "password",
        'password_confirmation' => 'password'
    ]);

    $response->assertRedirect();
    $response->assertSessionDoesntHaveErrors();
})->group('Web');

test( 'user email no duplication in web', function(){
    \App\Models\User::factory()->create([
        'email' => 'duplicate@gmail.com'
    ]);

    $url = "/register/customer";
    $this->post($url, [
        'name' => faker()->name,
        'email' => "duplicate@gmail.com",
        'password' => "password",
        'password_confirmation' => 'password'
    ])->assertStatus(302);
})->group('Web');

test('supplier can register in web', function(){
    // for event types
    \App\Models\Category::factory()->count(5)->create();
    $event_types = [\App\Models\Category::whereType('cater')->first()->id];

    // for roles
    \App\Models\Role::factory()->count(5)->create();
    $roles = \App\Models\Role::limit(3)->get()->pluck('id');

    $response = $this->post('/register/supplier', [
        'company_name' => faker()->company,
        'description' => faker()->text,
        'town' => "sample",
        'state' => "sample",
        'post_code' => "sample code",
        'telephone_number' => 'telephone_number',
        'event_types' => $event_types,
        'categories' => $roles,
        'name' => faker()->name,
        'email' => faker()->safeEmail,
        'password' => "password",
        'password_confirmation' => 'password'
    ]);

    $response->assertRedirect();
})->group('Web');
