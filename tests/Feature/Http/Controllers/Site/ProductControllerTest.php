<?php

namespace Tests\Feature\Http\Controllers\Site;

use App\Http\Controllers\Site\ProductController;
use App\Models\Category;
use App\Models\Company;
use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProductControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function index_can_render()
    {
        $this
            ->get('/decor')
            ->assertStatus(200)
            ->assertSee("Inspiration");
    }

    /** @test */
    public function decor_loaded_from_api()
    {
        $count = 10;
        Product::factory()
            ->published()
            ->hasCaters(Category::factory()->cater()->create())
            ->hasCategories(Category::factory()->productType()->create())
            ->hasCompany(Company::factory()->active()->create())
            ->count($count)->create();

        $this
            ->postJson(action([ProductController::class, 'list']))
            ->assertJsonCount($count, 'data');
    }
}
