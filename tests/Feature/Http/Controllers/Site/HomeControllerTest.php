<?php
use App\Models\Venue;
use App\Models\Product;
use App\Models\Inspiration;
use Tests\Fakes\FirebaseFMCFake;
use Database\Seeders\CaterSeeder;
use Illuminate\Support\Facades\Notification;

it('can load the homepage', function(){
    FirebaseFMCFake::setUp();
    Notification::fake();

    $this->seed(CaterSeeder::class);

    Inspiration::factory()->count(3)->featured()->create();
    Venue::factory()->count(3)->featured()->create();
    Product::factory()->count(3)->featured()->create();
    $this->get('/')
        ->assertStatus(200)
        ->assertSee("Beautiful Events Designed by You");
});

test("Request 3D form can render", function(){
    $this->seed(CaterSeeder::class);
    $this->get(route('site.request-3d-form'))
        ->assertStatus(200);
})->group("Web");
