<?php

namespace Tests\Feature\Http\Controllers\API\Manager;

use App\Http\Controllers\API\Manager\MoodBoardController;
use App\Http\Controllers\Dashboard\ProjectController;
use App\Models\Company;
use App\Models\Image;
use App\Models\Project;
use App\Models\User;
use Illuminate\Foundation\Testing\LazilyRefreshDatabase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class MoodBoardControllerTest extends TestCase
{
    public User $user;
    use LazilyRefreshDatabase;

    function setUp(): void
    {
        parent::setUp(); // TODO: Change the autogenerated stub
        $this->user = User::factory()
                        ->superadmin()
                        ->hasCompany(Company::factory()->active()->create(['name' => 'White Label Hire']))
                        ->verified()->create();

        $this->actingAs($this->user, 'api');
    }


    /** @test */
    public function listing_can_be_render()
    {
        $project = $this->makeProject();

        $this->get(action([ProjectController::class,'index']))
            ->assertSee($project->title)
            ->assertOk();
    }


    /** @test */
    public function can_upload_gallery_image()
    {
        $project = $this->makeProject();
        $this->postJson(action([MoodBoardController::class, 'uploadGallery']),[
            'project_id' => $project->id,
            'images' => [
                UploadedFile::fake()->image('fake.jpg'),
                UploadedFile::fake()->image('fake2.jpg'),
            ]
        ])->dump();

        $this->assertDatabaseCount(Image::class, 1);
//        $this->assertDatabaseHas('project_images', ['project_id' => $project->id]);
    }

    /**
     * @return Project|\Illuminate\Database\Eloquent\Model
     */
    public function makeProject()
    {
        return Project::create([
            'title' => 'efwf',
            'description' => 'fwfewf',
            'user_id' => $this->user->id
        ]);
    }
}
