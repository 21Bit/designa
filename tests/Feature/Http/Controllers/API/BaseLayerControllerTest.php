<?php

use App\Http\Controllers\API\BaseLayerController;
use App\Models\User;
use App\Models\Company;

use function Pest\Laravel\post;

beforeEach(function(){
    $this->user = User::factory()
            ->hasCompany(Company::factory()->active()->create(['name' => 'White Label Hire']))
            ->verified()->ontrial()->create();

    $this->channel('api')->asSupplier($this->user);
});

it('can load baselayer json', function(){
    $this
        ->get(action([BaseLayerController::class,'index']))
        ->assertOk();
});