<?php

namespace Tests\Feature\Http\Controllers\Dashboard;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class FloorPlanModelController extends TestCase
{
    use WithFaker, RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->channel('api')->asSupplier();
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_floorplan_can_be_created()
    {
        $this->withoutExceptionHandling();
        \Storage::fake('3d-models');
        $response = $this->json('POST', "api/3d/floorplan", [
                            'name' => $this->faker->name,
                            'model_file' => UploadedFile::fake()->create('sample.dat')
                        ], ['Accept' => 'application/json']);

        $response->assertStatus(200);
    }
}
