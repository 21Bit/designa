<?php

namespace Tests\Feature\Http\Controllers\Dashboard;

use App\Http\Controllers\Dashboard\CompanyController;
use App\Models\Company;
use App\Models\User;
use App\Services\SubscriptionService;
use Database\Seeders\SubscriptionPlanAndPriceSetUp;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;


class CompanyControllerTest extends TestCase
{
    use RefreshDatabase;

    private $user;
    private $company;

    protected function setUp(): void
    {
        parent::setUp(); // TODO: Change the autogenerated stub
        $this->user = User::factory()
                        ->superadmin()
                        ->for($this->company = Company::factory()->active()->create())
                        ->create();
        $this->asSuperAdmin($this->user);
        $this->artisan('db:seed', ['--class' => SubscriptionPlanAndPriceSetUp::class]);
    }


    /** @test */
    public function can_cancel_company_trial()
    {
        $user = User::factory()
                    ->ontrial()
                    ->superadmin()
                    ->for($company = Company::factory()->active()->create())
                    ->create();

        $response = $this->post(action([CompanyController::class, 'cancelTrial']), ['company_id' => $company->id]);
        $response->assertRedirect();
        $response->assertSessionHas(['message' => 'Trial is cancelled for ' . $company->name]);
        $user->refresh();

        $this->assertDatabaseHas(User::class, ['id' => $user->id, 'trial_ends_at' => null, 'company_id' => $company->id]);
        $this->assertFalse($user->onGenericTrial());
    }


    /** @test */
    public function can_add_trail_to_company()
    {
            $this->withoutExceptionHandling();

            $this
                ->get(action([CompanyController::class, 'index']))
                ->assertSee('Add Free Trial');

            $response = $this->post(action([CompanyController::class, 'addTrial']), ['company_id' => $this->company->id, 'days' => 30]);
            $response->assertSessionHas(['success' => '30 of trials is added to ' . $this->company->name]);
            $response->assertRedirect();
            $this->user->refresh();
            $this->assertTrue($this->user->onTrial());

            $hasAccess = (new SubscriptionService())->userHasAccess($this->user, "your-products-in-3d-10-is-pro");
            $hasMarketing = (new SubscriptionService)->userHasAccess($this->user, "promote-using-featured-marketing");
            $has3d = (new SubscriptionService)->userHasAccess($this->user, "transform-a-room-with-custom-3d-builds");
            $this->assertTrue($hasAccess);
            $this->assertTrue($hasMarketing);
            $this->assertTrue($has3d);
    }
}
