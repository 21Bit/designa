<?php

use App\Http\Controllers\Dashboard\FunctionSpaceController;
use App\Models\FunctionSpace;
use App\Models\Venue;
use function Pest\Laravel\post;
use function Pest\Laravel\put;


beforeEach(function(){
    actingAsSupplier();
});


test('it can load functionspace of venue', function () {
    $functionspace = FunctionSpace::factory()->create();
    expect(true)->toBeTrue();
});

it('can add functionspace', function () {
    $venue = Venue::factory()->create();
    $picture = \Illuminate\Http\UploadedFile::fake()->image('sample.png');
    post(action([FunctionSpaceController::class, 'store']),[
        'name' => 'Sample',
        'max_capacity_seated' => random_int(1,5),
        'max_capacity_standing' => random_int(1,5),
        'description' => "sample description",
        'thumbnail' => $picture,
        'floor_plan' => $picture,
    ])
        ->assertValid();
});


it('can update functionspace', function () {
    $functionspace = FunctionSpace::factory()->create();
    $picture = \Illuminate\Http\UploadedFile::fake()->image('sample.png');
    put(action([FunctionSpaceController::class, 'update'], $functionspace->id),[
        'name' => 'Sample',
        'venue_id' => $functionspace->venue_id,
        'max_capacity_seated' => random_int(1,5),
        'max_capacity_standing' => random_int(1,5),
        'description' => "sample description",
        'thumbnail' => $picture,
        'floor_plan' => $picture,
    ])
        ->assertSessionHasNoErrors()
        ->assertRedirect();

    $this->assertDatabaseHas(FunctionSpace::class, ['name' => 'Sample']);
});

it('can upload asset bundle', function () {
    $functionspace = FunctionSpace::factory()->create();
    post(action([FunctionSpaceController::class, 'uploadAssetBundle'], $functionspace->id),[
        'type' => 'base_layer',
        'asset_bundle_file' => \Illuminate\Http\UploadedFile::fake()->image('file.file')
    ])
        ->assertRedirect();

    $this->assertDatabaseHas(\App\Models\ModelFile::class, ['model_name' => 'file.file']);
});
