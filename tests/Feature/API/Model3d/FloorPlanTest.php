<?php

namespace Tests\Feature\API\Model3d;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;use Tests\TestCase;

class FloorPlanTest extends TestCase
{
    use WithFaker, RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_floorplan_can_be_created()
    {
        $this->withoutExceptionHandling();
        \Storage::fake('3d-models');
        $user = User::factory()->create();
        $response = $this->actingAs($user)
                        ->json('POST', "api/3d/floorplan", [
                            'name' => $this->faker->name,
                            'model_file' => UploadedFile::fake()->create('sample.dat')
                        ], ['Accept' => 'application/json']);

        $response->assertStatus(200);
    }
}
