<?php

use App\Models\Product;

use function Pest\Laravel\{get, getJson, post};

beforeEach(fn () => actingAsSupplier(true));


it('products can be loaded in product list manager', function(){
    Product::factory()->count(20)->create();
    getJson(route('manager.index'))->assertStatus(200);
});

it('products can be added in product manager', function(){
    $data = [
        'name' => 'product 1',
        ''
    ];
    $response = post('/api/decor');
    $response->assertStatus(200);
});
