<?php
use function Pest\Faker\faker;

test('test customer can register in api', function(){
    $data =  [
        "name"             =>  faker()->name,
        "email"            =>  faker()->safeEmail,
        'password'         =>  bcrypt("password"),
        'device'           =>  faker()->text(6),
        'ios_device_token' =>  faker()->text(6),
        'firebase'         =>  faker()->text(6)
    ];

    $this->json('POST', 'api/register/customer', $data, ['Accept' => 'application/json'])
                ->assertStatus(200)->assertExactJson([
                    'status' => 200,
                    'message' => 'Registration Success'
                ]);
});

