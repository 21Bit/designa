const mix = require('laravel-mix');


mix.options({
    legacyNodePolyfills: true
});

mix.webpackConfig({
      resolve: {
        fallback: {
            // … omitted for brevity …
            "crypto": require.resolve("crypto-browserify"),
            "timers": require.resolve("timers-browserify"),
            "stream": require.resolve("stream-browserify")
        },
    },
})

 // App is for the Site or Front-end
mix.js('resources/js/app.js', 'public/js').vue({version: 2, extractStyle: true})
    .js('resources/js/eventregistration.js', 'public/js').vue({version: 2, extractStyle: true})
    .sass('resources/sass/app.scss', 'public/css')
    .version();

// as the name suggest it is for the dashboard
mix.js('resources/js/dashboard.js', 'public/js').vue({version: 2, extractStyle: true})
    .sass('resources/sass/dashboard.scss', 'public/css')
    .version();



// if (mix.inProduction()) {
//     mix.version();
// }
