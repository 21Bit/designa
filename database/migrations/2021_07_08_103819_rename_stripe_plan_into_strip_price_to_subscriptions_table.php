<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameStripePlanIntoStripPriceToSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('subscriptions', 'stripe_plan')){
            Schema::table('subscriptions', function (Blueprint $table) {
                $table->renameColumn('stripe_plan', 'stripe_price');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('subscriptions', 'stripe_price')){
            Schema::table('subscriptions', function (Blueprint $table) {
                $table->renameColumn('stripe_price', 'stripe_plan');
            });
        }
    }
}
