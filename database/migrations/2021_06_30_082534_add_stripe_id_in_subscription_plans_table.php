<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStripeIdInSubscriptionPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('subscription_plans', 'stripe_id')){
            Schema::table('subscription_plans', function (Blueprint $table) {
                $table->string('stripe_id')->nullable()->after('id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('subscription_plans', 'stripe_id')) {
            Schema::table('subscription_plans', function (Blueprint $table) {
                $table->dropColumn('stripe_id');
            });
        }
    }
}
