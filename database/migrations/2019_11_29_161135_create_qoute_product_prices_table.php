<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQouteProductPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qoute_product_prices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('qoute_product_id')->unsigned();
            $table->bigInteger('pricing_component_id')->unsigned();
            $table->double('price');
            $table->timestamps();

            $table->foreign('qoute_product_id')->references('id')->on('qoute_products')->onDelete('cascade');
            $table->foreign('pricing_component_id')->references('id')->on('pricing_components')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qoute_product_prices');
    }
}
