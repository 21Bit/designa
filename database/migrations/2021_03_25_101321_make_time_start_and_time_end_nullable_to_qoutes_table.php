<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MakeTimeStartAndTimeEndNullableToQoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('qoutes', function (Blueprint $table) {
            $table->string('time_start')->nullable()->change();
            $table->string('time_end')->nullable()->change();
            $table->string('event_date')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('qoutes', function (Blueprint $table) {
            $table->string('time_start')->change();
            $table->string('time_end')->change();
            $table->string('event_date')->change();
        });
    }
}
