<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTemplateIdInModelFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('model_files', 'decoration_template_id')) {
            Schema::table('model_files', function (Blueprint $table) {
                $table->unsignedBigInteger('decoration_template_id')
                        ->after('base_layer_id')
                        ->nullable();
                    });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('model_files', 'decoration_template_id')) {
            Schema::table('model_files', function (Blueprint $table) {
                $table->dropColumn('decoration_template_id');
            });
        }
    }
}
