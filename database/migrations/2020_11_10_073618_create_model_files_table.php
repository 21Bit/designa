<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('model_files', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('model_name')->nullable();
            $table->string('path');
            $table->boolean('is_public')->default(0);
            $table->integer('number_of_guest')->nullable();
            $table->string('type')->default("base_layer");
            $table->integer('base_layer_id')->nullable();
            $table->integer('decoration_template_id')->nullable();
            $table->morphs('modellable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('model_files');
    }
}
