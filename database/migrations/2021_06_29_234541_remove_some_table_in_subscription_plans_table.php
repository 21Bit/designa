<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveSomeTableInSubscriptionPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('subscription_plans', 'billing_period')){
             Schema::table('subscription_plans', function (Blueprint $table) {
                $table->dropColumn('billing_period');
            });
        }

        if(Schema::hasColumn('subscription_plans', 'stripe_plan')){
             Schema::table('subscription_plans', function (Blueprint $table) {
                $table->dropColumn('stripe_plan');
            });
        }

        if(Schema::hasColumn('subscription_plans', 'price')){
             Schema::table('subscription_plans', function (Blueprint $table) {
                $table->dropColumn('price');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscription_plans', function (Blueprint $table) {
            $table->string('stripe_plan');
            $table->float('price');
            $table->string('billing_period')->default('monthly')
                    ->after('name');
        });
    }
}
