<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ApplyChangesColumnsToSubscriptionItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('subscription_items', 'stripe_product') && Schema::hasColumn('subscription_items', 'stripe_plan')){
            Schema::table('subscription_items', function (Blueprint $table) {
                $table->string('stripe_product')->nullable();
                $table->renameColumn('stripe_plan', 'stripe_price')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('subscription_items', 'stripe_product') && Schema::hasColumn('subscription_items', 'stripe_price')){
            Schema::table('subscription_items', function (Blueprint $table) {
                $table->dropColumn('stripe_product');
                $table->renameColumn('stripe_price', 'stripe_plan');
            });
        }
    }
}
