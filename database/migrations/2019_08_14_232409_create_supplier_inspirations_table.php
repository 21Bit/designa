<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplierInspirationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_inspirations', function (Blueprint $table) {
        $table->bigIncrements('id');
            $table->unsignedBigInteger('inspiration_id');
            $table->unsignedBigInteger('role_id');
            $table->unsignedBigInteger('company_id')->nullable();
            $table->unsignedBigInteger('request_by')->nullable();
            $table->text('name')->nullable();
            $table->string('status')->default('pending');
            $table->string('email')->default('pending');
            $table->timestamps();

            $table->foreign('inspiration_id')->references('id')->on('inspirations')->onDelete('cascade');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_inspirations');
    }
}
