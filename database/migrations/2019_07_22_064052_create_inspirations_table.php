<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInspirationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inspirations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('company_id');
            $table->string('name');
            $table->text('description');
            $table->year('project_year');
            $table->integer('venue_id')->nullable();
            $table->text('location')->nullable();
            $table->string("latitude")->optional();
            $table->string("longitude")->optional();
            $table->text('slug');
            $table->text('images_by')->nullable();
            $table->boolean('is_published')->default(1);
            $table->boolean('is_lock')->default(0);
            $table->boolean('featured')->default(0);
            $table->timestamps();

            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inspirations');
    }
}
