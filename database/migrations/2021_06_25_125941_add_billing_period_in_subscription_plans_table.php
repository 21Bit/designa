<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddBillingPeriodInSubscriptionPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('subscription_plans', 'billing_period')){
            Schema::table('subscription_plans', function (Blueprint $table) {
                $table->string('billing_period')->default('monthly')
                            ->after('name');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('subscription_plans', 'billing_period')){
            Schema::table('subscription_plans', function (Blueprint $table) {
                $table->dropColumn('billing_period');
            });
        }
    }
}
