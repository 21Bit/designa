<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameColumnsForNewStripeInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('users', 'card_brand')){
            Schema::table('users', function (Blueprint $table) {
                $table->renameColumn('card_brand','pm_type');
            });
        }

        if(Schema::hasColumn('users', 'card_last_four')){
            Schema::table('users', function (Blueprint $table) {
                $table->renameColumn('card_last_four','pm_last_four');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('users', 'pm_type')){
            Schema::table('users', function (Blueprint $table) {
                $table->renameColumn('pm_type', 'card_brand');
            });
        }

        if(Schema::hasColumn('users', 'pm_last_four')){
            Schema::table('users', function (Blueprint $table) {
                $table->renameColumn('pm_last_four', 'card_brand');
            });
        }
    }
}
