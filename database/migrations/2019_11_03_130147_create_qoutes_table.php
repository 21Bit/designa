<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qoutes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reference_number');
            $table->bigInteger('user_id')->nullable()->unsigned();
            $table->string('event_name')->nullable();
            $table->string('event_type');
            $table->date('event_date');
            $table->string('location');
            $table->string('time_start');
            $table->string('time_end');
            $table->string('number_of_guests');
            $table->string('status')->default('ongoing');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qoutes');
    }
}
