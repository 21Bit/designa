<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVenuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('venues', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('company_id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->text('slug');
            
            $table->text('operating_hours')->nullable();
            $table->text('location');
            $table->text('email')->nullable();
            $table->text('contact_number')->nullable();
            $table->string('longitude')->nullable();
            $table->string('latitude')->nullable();

            // $table->integer('price')->nullable();

            $table->integer('max_seated')->default(0);
            $table->integer('max_standing')->default(0);
            $table->integer('capacity')->default(0);
            $table->integer('price')->nullable();
            
            $table->boolean('is_published')->default(1);
            $table->boolean('single_venue')->default(0);
            $table->boolean('is_lock')->default(0);
            $table->boolean('featured')->default(0);
            $table->timestamps();

            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('venues');
    }
}
