<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStripeStatusColumnToSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasColumn('subscriptions', 'stripe_status')){
            Schema::table('subscriptions', function (Blueprint $table) {
                $table->string('stripe_status')->after('stripe_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('subscriptions', 'stripe_status')){
            Schema::table('subscriptions', function (Blueprint $table) {
                $table->dropColumn('stripe_status');
            });
        }
    }
}
