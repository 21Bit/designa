<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->text('services')->nullable();
            $table->text('recognition')->nullable();
            $table->text('will_travel')->nullable();
            $table->text('slug');
            
            //address
            $table->string('town');
            $table->string('state');
            $table->string('country');
            $table->string('post_code');

            $table->float('latitude', 10,7)->nullable();
            $table->float('longitude', 10,7)->nullable();
            $table->boolean('single_venue')->default(0);

            
            //contract information
            $table->string('email')->nullable();
            $table->string('website')->nullable();
            $table->string('telephone_number')->nullabe();
            
                //links
                $table->text('facebook_link')->nullable();
                $table->text('twitter_link')->nullable();
                $table->text('instagram_link')->nullable();
                $table->text('youtube_link')->nullable();

            $table->boolean('is_active')->default(1);
            $table->boolean('is_lock')->default(0);
          
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
