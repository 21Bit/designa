<?php

namespace Database\Seeders;

use App\Models\SubscriptionPlan;
use App\Models\SubscriptionPlanPrice;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class SubscriptionPlanAndPriceSetUp extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $plans = [
            [
                'name' => 'Beginner',
                'type' => 'basic'
            ],
            [
                'name' => 'Intermediate',
                'type' => 'basic'
            ],
            [
                'name' => 'Advanced',
                'type' => 'basic'
            ],
            [
                'name' => 'Pro',
                'type' => 'basic'
            ],
            [
                'name' => 'Venue Charges',
                'type' => 'venue-charges'
            ],
            [
                'name' => 'Event Spaces',
                'type' => 'event-spaces'
            ],
        ];

        foreach($plans as $plan){
            SubscriptionPlanPrice::factory()
                            ->for(SubscriptionPlan::factory()->create($plan))
                            ->count(2)
                            ->sequence(new Sequence(
                                ['billing_period' => 'monthly'],
                                ['billing_period' => 'yearly'],
                            ));

            

        }
    }
}
