<?php
namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class ProductTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $module = "product";
        
        Category::whereModule('product')->delete();

        $data = [
            ['name' => 'Chandelier', 'type'=> 'product_type', 'module' => $module,'subcategories' => 
                [
                    'Traditional',
                    'Modern',
                    'Grand',
                    'Abstruct',
                ]
            ],
            ['name' => 'Candelware', 'type'=> 'product_type', 'module' => $module,'subcategories' => 
                [
                    'Candelabras',
                    'Tealight Holders',
                    'Pillar Candle Holders',
                    'Dinner candle holders',
                    'Floating candle holders'
                ]
            ],
            ['name' => 'Indoor Furniture', 'type'=> 'product_type', 'module' => $module, 'subcategories' => 
                [
                    'Bars',
                    'Lounges',
                    'Sofa Chair',
                    'Loveseats',
                    'Ottoman',
                    'Coffee Table',
                    'Round Guests Tables',
                    'Kings Tables',
                    'Bar Chair',
                    'Dining Chairs',
                    'Bar Tables',
                    'Knights Tables',
                    'Dessert Tables',
                ]
            ],
            ['name' => 'Outdoor Furniture', 'type'=> 'product_type', 'module' => $module,'subcategories' => 
                [
                    'Bars',
                    'Cocktail Tables',
                    'Ottoman',
                    'Bar Stools',
                    'Kings Tables',
                    'Knight Tables',
                    'Dessert Tables',
                    'Sofa Chairs',
                    'Bar Cart',
                    'Wine Barrel',
                    'Round Guest Tables',
                    'Luxury Tents and Structures',
                    'Arbours',
                    'Outdoor Lounge',
                ]
            ],
            ['name' => 'Tableware', 'type'=> 'product_type', 'module' => $module,'subcategories' => 
                [
                    'Lienen',
                    'Napkins',
                    'Plates',
                    'Glassware',
                    'Cutlery',
                    'Napkin Rings',
                    'Menu/name Holders',
                ]
            ],
            ['name' => 'Bridal Table', 'type'=> 'product_type', 'module' => $module,'subcategories' => 
                [
                    //
                ]
            ],
            ['name' => 'Dance Floors', 'type'=> 'product_type', 'module' => $module,'subcategories' => 
                [
                   ///
                ]
            ],
            ['name' => 'Drapping', 'type'=> 'product_type', 'module' => $module,'subcategories' => 
                [
                   //
                ]
            ],
            ['name' => 'Lighting & AV', 'type'=> 'product_type', 'module' => $module,'subcategories' => 
                [
                   //
                ]
            ],
            ['name' => 'Props', 'type'=> 'product_type', 'module' => $module,'subcategories' => 
                [
                  //
                ]
            ],
            ['name' => 'Stationary', 'type'=> 'product_type', 'module' => $module,'subcategories' => 
                [
                    'Traditional',
                    'Modern',
                    'Grand',
                    'Abstruct',
                ]
            ],
        ];

        //clear all first
        Category::whereType('product_type')->delete();

        foreach($data as $row){
            $cater = new Category;
            $cater->name = $row['name'];
            $cater->type = $row['type'];
            $cater->module = "category";
            $cater->save();
                
            foreach($row['subcategories'] as $sub){
                $subCat = new Category;
                $subCat->name = $sub;
                $subCat->parent_id = $cater->id;
                $subCat->type = "style";
                $subCat->module = $module;
                $subCat->save();
            }
        }
    }
}
