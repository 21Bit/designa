<?php
namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use App\Models\Company;
use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Traits\HasImageIntervention;

class CompanySeeder extends Seeder
{
    use HasImageIntervention;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->delete();
        DB::table('users')->delete();

        Company::factory()->count(10)->create()->each(function($company){
            $company->categories()->attach(Category::inRandomOrder()->whereType('cater')->take(3)->get()->pluck('id'));
            $company->users()->save(User::factory()->make());

            //role
            $roles = Role::inRandomOrder()->take(random_int(2,5))->pluck('id');
            $company->roles()->sync($roles);
            $company->asyncRoleToUsers();
        });
    }
}
