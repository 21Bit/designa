<?php
namespace Database\Seeders;

use DB;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $images = [
            '5d71da8f6be0a1567742607.jpg',
            '5d71da5437b861567742548.jpg',
            '5d71daae681c01567742638.jpg',
            '5d71dbe63b2431567742950.jpg',
            '5d718b87766fd1567722375.jpg',
            '5d718c1a589f51567722522.jpg',
            '5d732a180e0cc1567828504.jpg',
            '5d734db2888bf1567837618.jpg',
            '5d7302f51c4431567818485.jpg',
            '5d7328fd73cd71567828221.jpg',
            '5d7329cc28b961567828428.jpg',
            '5d7329e135d2f1567828449.jpg',
            '5d73289e80f191567828126.jpg',
            '5d732980c21231567828352.jpg',
            '5d73293146b931567828273.jpg',
            '5d73299853b3f1567828376.jpg'
        ];

        $galleries = [
            '5d718b87766fd1567722375.jpg',
            '5d73655237a101567843666.jpg',
            '5d71da8f6be0a1567742607.jpg',
            '5d71da5437b861567742548.jpg',
            '5d71daae681c01567742638.jpg',
            '5d71dbe63b2431567742950.jpg',
            '5d718b87766fd1567722375.jpg',
            '5d718c1a589f51567722522.jpg',
            '5d732a180e0cc1567828504.jpg',
            '5d734db2888bf1567837618.jpg',
            '5d7302f51c4431567818485.jpg',
            '5d7328fd73cd71567828221.jpg',
            '5d7329cc28b961567828428.jpg',
            '5d7329e135d2f1567828449.jpg',
            '5d73289e80f191567828126.jpg',
            '5d732980c21231567828352.jpg',
            '5d73293146b931567828273.jpg',
            '5d73299853b3f1567828376.jpg'
        ];

        DB::table('products')->delete();

        Product::factory()->count(10)->create()->each(function ($product)  use($galleries, $images) {
            $product->categories()->attach(Category::inRandomOrder()->whereType('color')->get()->pluck('id'));
            $product->categories()->attach(Category::inRandomOrder()->whereType('cater')->take(1)->get()->pluck('id'));
            $product->categories()->attach(Category::inRandomOrder()->whereType('product_type')->take(1)->get()->pluck('id'));
            $product->categories()->attach(Category::inRandomOrder()->whereType('style')->whereModule('product')->take(1)->get()->pluck('id'));


            $thumbnail = $product->addImage("thumbnail", $images[array_rand($images, 1)], public_path('images/thumbnails'), true);
            $product->images()->save($thumbnail);

            foreach($galleries as $image){
                $gallery = $product->addImage("gallery", $galleries[array_rand($galleries, 1)], public_path('images/gallery'), true);
                $product->images()->save($gallery);
            }
        });
    }
}
