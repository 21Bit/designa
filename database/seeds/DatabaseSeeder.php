<?php
namespace Database\Seeders;

use App\Models\Qoute;
use App\Models\Venue;
use App\Models\QouteProduct;
use Illuminate\Database\Seeder;
use Database\Seeders\AcccessSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {


        // Qoute::truncate();

        // factory(Qoute::class, 5)->create()->each(function($q){
        //     $q->decors()->save(factory(QouteProduct::class)->make());
        // });

        // $factory->define( App\Models\Qoute::class, function ($faker) {
        //     return [
        //         'reference_number' => time() . rand(10*45, 100*98),
        //         'user_id' => 136,
        //         'event_name' => $faker->name,
        //         'event_type' => 584,
        //         'location' => $faker->address,
        //         'event_date' => $faker->date,
        //         'time_start' => $faker->date,
        //         'time_end' => $faker->time,
        //         'number_of_guests' => 50,
        //     ];
       
        // });
        
        $this->call(AcccessSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(ProductTypeSeeder::class);
        $this->call(VenueCategoriesSeeder::class);
        $this->call(CaterSeeder::class);
        $this->call(ColorSeeder::class);
        $this->call(PermissionModuleSeeder::class);
        $this->call(CompanySeeder::class); // automatic seed 10 users based on company
        $this->call(ProductSeeder::class);
        $this->call(VenueSeeder::class);
        $this->call(InspirationSeeder::class);
        
        $this->call(UserSeeder::class);
    }
}
    