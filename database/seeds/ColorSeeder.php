<?php
namespace Database\Seeders;

use App\Models\Color;
use App\Models\Category;
use Illuminate\Database\Seeder;

class ColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
  
        $type = "color";
        $module = "setting";
        
        Category::whereModule('color')->delete();

        $data = [
            [
                'name' => 'Blue', 
                'description'=> '#CCFFFF',
                'type'=> $type, 
                'module' => $module
            ],
            [
                'name' => 'Green', 
                'description'=> '#99FFCC',
                'type'=> $type, 
                'module' => $module
            ],
            [
                'name' => 'Pink', 
                'description'=> '#FF3399',
                'type'=> $type, 
                'module' => $module
            ],
            [
                'name' => 'Red', 
                'description'=> '#CC0000',
                'type'=> $type, 
                'module' => $module
            ],
            [
                'name' => 'Black', 
                'description'=> '#202020',
                'type'=> $type, 
                'module' => $module
            ],
            [
                'name' => 'White', 
                'description'=> '#ffff',
                'type'=> $type, 
                'module' => $module
            ],
        ];

        Category::whereType($type)->delete();
        \DB::table('categories')->insert($data);
    }
}
