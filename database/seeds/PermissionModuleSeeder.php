<?php
namespace Database\Seeders;

use DB;
use Str;
use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name' => 'Venue', 'description' => 'A company that can access, create, edit and delete venues for their company'],
            ['name' => 'Product', 'description' => 'A company that can access, create, edit and delete products for their company'],
            ['name' => 'Inspiration', 'description' => 'A company that can access, create, edit and delete inspiration for their company'],
            ['name' => 'Setting', 'description' => 'A company that can access, settings for the whole system'],
            ['name' => 'Company', 'description' => 'A company that can access, create, edit and delete comapany/supplier for their company']
        ];
        DB::table('permissions')->delete();
        foreach($data as $row){
            $permission = new Permission;
            $permission->name = Str::snake($row['name']);
            $permission->display_name = $row['name'] . ' Module';
            $permission->description = $row['description'];
            $permission->save();
        }
    }
}
