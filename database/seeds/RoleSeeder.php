<?php
namespace Database\Seeders;

use DB;
use Str;
use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'Creative Director',
            'Decor Hire',
            'Florist',
            'Planner',
            'Stylist',
            'Venues',
            'Balloons',
            'Production',
            'Prop Hire',
            'Stationary',
            'Signage and Print'
        ];
        
        DB::table('roles')->delete();

        foreach($data as $row){
            $role = new Role;
            $role->name = Str::snake($row);
            $role->display_name = $row;
            $role->save();
        }
    }
}
