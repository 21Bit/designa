<?php
namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class VenueCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $module = "venue";
        Category::whereModule('venue')->delete();
        // Venue Type
        $data = [
            ['name' => 'Bar/Restaurant', 'type'=> 'venue_type', 'module' => $module],
            ['name' => 'Boats/Cruises', 'type'=> 'venue_type', 'module' => $module],
            ['name' => 'Farm/Barn', 'type'=> 'venue_type', 'module' => $module],
            ['name' => 'Event Space', 'type'=> 'venue_type', 'module' => $module],
            ['name' => 'Hotel', 'type'=> 'venue_type', 'module' => $module],
            ['name' => 'Outdoor', 'type'=> 'venue_type', 'module' => $module],
            ['name' => 'Reception', 'type'=> 'venue_type', 'module' => $module],
      
            ['name' => '$', 'type'=> "average_cost_per_head", 'module' => $module],
            ['name' => '$$', 'type'=> "average_cost_per_head", 'module' => $module],
            ['name' => '$$$', 'type'=> "average_cost_per_head", 'module' => $module],
            ['name' => '$$$$', 'type'=> "average_cost_per_head", 'module' => $module],
     
            ['name' => 'Outside caterers allowed', 'type'=> "catering_option", 'module' => $module],
            ['name' => 'Preferred caterers only', 'type'=> "catering_option", 'module' => $module],
            ['name' => 'In-house caterer only', 'type'=> "catering_option", 'module' => $module],
       
            ['name' => 'BYOB allowed', 'type'=> "beverage_option", 'module' => $module],
            ['name' => 'Preferred suppliers only', 'type'=> "beverage_option", 'module' => $module],
            ['name' => 'Beer & wine only', 'type'=> "beverage_option", 'module' => $module],
            ['name' => 'No alcohol allowed', 'type'=> "beverage_option", 'module' => $module],
            ['name' => 'In-house suppliers only', 'type'=> "beverage_option", 'module' => $module],
       
            ['name' => 'A/V Equipment', 'type'=> "amenity", 'module' => $module],
            ['name' => 'Beachfront', 'type'=> "amenity", 'module' => $module],
            ['name' => 'Breakout rooms', 'type'=> "amenity", 'module' => $module],
            ['name' => 'Coat Check', 'type'=> "amenity", 'module' => $module],
            ['name' => 'Handicap Accessible', 'type'=> "amenity", 'module' => $module],
            ['name' => 'Kosher Kitchen', 'type'=> "amenity", 'module' => $module],
            ['name' => 'Media Room', 'type'=> "amenity", 'module' => $module],
            ['name' => 'Outdoor', 'type'=> "amenity", 'module' => $module],
            ['name' => 'Pet Friendly', 'type'=> "amenity", 'module' => $module],
            ['name' => 'Outdoor', 'type'=> "amenity", 'module' => $module],
            ['name' => 'Pool', 'type'=> "amenity", 'module' => $module],
            ['name' => 'Street Parking', 'type'=> "amenity", 'module' => $module],
            ['name' => 'Valet Parking', 'type'=> "amenity", 'module' => $module],
            ['name' => 'Wifi', 'type'=> "amenity", 'module' => $module],
            ['name' => 'Windows', 'type'=> "amenity", 'module' => $module],
            ['name' => 'Dance Floor', 'type'=> "amenity", 'module' => $module],
        ];

        Category::whereModule($module)->delete(); 
              
        foreach($data as $row){
            Category::create($row);
        }
    }
}
