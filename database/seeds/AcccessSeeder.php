<?php
namespace Database\Seeders;

use Str;
use App\Models\Access;
use Illuminate\Database\Seeder;

class AcccessSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
                    'Load Product',
                    'Digital Catalogue / Look Book',
                    'Your Products in 3D / 10+ is Pro',
                    'Immersive 3D customised room styling using template',
                    'Load Inspiration',
                    'Tag and be tagged',
                    'Communicate with customers',
                    'Promote using featured marketing',
                    'Register for the Designa VR Expo',
                    'Receive and Fulfill Quotes',
                    'Create Pitch Packs in Designa',
                    'Create your own event 3D using template',
                    'Transform a room with custom 3D Builds'
                ];
        
        foreach($data as $data){
            $forName = Str::slug(strtolower($data));
            $toInsert = [
                'display_name' => $data,
                'name' => $forName
            ];
            
            Access::firstOrCreate($toInsert);
        }
    }
}
