<?php
namespace Database\Seeders;

use DB;
use App\Models\Venue;
use App\Models\Review;
use App\Models\Category;
use App\Models\FunctionSpace;
use Illuminate\Database\Seeder;

class VenueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        DB::table('venues')->delete();
        $galleries = [
            '5d718b87766fd1567722375.jpg',
            '5d73655237a101567843666.jpg',
            '5d71da8f6be0a1567742607.jpg',
            '5d71da5437b861567742548.jpg',
            '5d71daae681c01567742638.jpg',
            '5d71dbe63b2431567742950.jpg',
            '5d718b87766fd1567722375.jpg',
            '5d718c1a589f51567722522.jpg',
            '5d732a180e0cc1567828504.jpg',
            '5d734db2888bf1567837618.jpg',
            '5d7302f51c4431567818485.jpg',
            '5d7328fd73cd71567828221.jpg',
            '5d7329cc28b961567828428.jpg',
            '5d7329e135d2f1567828449.jpg',
            '5d73289e80f191567828126.jpg',
            '5d732980c21231567828352.jpg',
            '5d73293146b931567828273.jpg',
            '5d73299853b3f1567828376.jpg'
        ];

        Venue::factory()->count(5)->create()->each(function ($venue) use ($galleries) {
            $venue->categories()->attach(Category::inRandomOrder()->whereType('color')->take(3)->get()->pluck('id'));
            $venue->categories()->attach(Category::inRandomOrder()->whereType('cater')->take(3)->get()->pluck('id'));
            $venue->categories()->attach(Category::inRandomOrder()->whereType('venue_type')->take(3)->get()->pluck('id'));
            $venue->categories()->attach(Category::inRandomOrder()->whereType('amenity')->take(3)->get()->pluck('id'));
            $venue->categories()->attach(Category::inRandomOrder()->whereType('catering_option')->take(1)->get()->pluck('id'));
            $venue->categories()->attach(Category::inRandomOrder()->whereType('beverage_option')->take(3)->get()->pluck('id'));
            $venue->categories()->attach(Category::inRandomOrder()->whereType('average_cost_per_head')->take(1)->get()->pluck('id'));;
        
            $venue->functionSpaces()->createMany(FunctionSpace::factory()->count(random_int(1,8))->make()->toArray());

            foreach($galleries as $image){
                $gallery = $venue->addImage("gallery", $galleries[array_rand($galleries, 1)], public_path('images/gallery'), true);
                $venue->images()->save($gallery);
            }

            $venue->reviews()->createMany(Review::factory()->count(15)->make()->toArray());
        });
    }
}
