<?php
namespace Database\Seeders;

use App\Models\Role;
use App\Models\Image;
use App\Models\Company;
use App\Models\Category;
use App\Models\Inspiration;
use Illuminate\Database\Seeder;
use App\Models\SupplierInspiration;


class InspirationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Image::where('imagable_type','App\Models\Inspiration')->delete();
        Category::whereModule('inspiration')->delete();
        // Inspiration::where('id', '>', 0)->get()->map(function($q){
        //     $q->categories()->delete();
        // });

        Inspiration::where('id', '>', 0)->delete();
        $images = [
            '5d71da8f6be0a1567742607.jpg',
            '5d71da5437b861567742548.jpg',
            '5d71daae681c01567742638.jpg',
            '5d71dbe63b2431567742950.jpg',
            '5d718b87766fd1567722375.jpg',
            '5d718c1a589f51567722522.jpg',
            '5d732a180e0cc1567828504.jpg',
            '5d734db2888bf1567837618.jpg',
            '5d7302f51c4431567818485.jpg',
            '5d7328fd73cd71567828221.jpg',
            '5d7329cc28b961567828428.jpg',
            '5d7329e135d2f1567828449.jpg',
            '5d73289e80f191567828126.jpg',
            '5d732980c21231567828352.jpg',
            '5d73293146b931567828273.jpg',
            '5d73299853b3f1567828376.jpg'
        ];
        $covers = [
            '5d71da8f83ce31567742607.jpg',
            '5d71da56733a91567742550.jpg',
            '5d732a182aa7f1567828504.jpg',
            '5d7329e14d4d21567828449.jpg'
        ];
        $galleries = [
            '5d718b87766fd1567722375.jpg',
            '5d73655237a101567843666.jpg',
            '5d71da8f6be0a1567742607.jpg',
            '5d71da5437b861567742548.jpg',
            '5d71daae681c01567742638.jpg',
            '5d71dbe63b2431567742950.jpg',
            '5d718b87766fd1567722375.jpg',
            '5d718c1a589f51567722522.jpg',
            '5d732a180e0cc1567828504.jpg',
            '5d734db2888bf1567837618.jpg',
            '5d7302f51c4431567818485.jpg',
            '5d7328fd73cd71567828221.jpg',
            '5d7329cc28b961567828428.jpg',
            '5d7329e135d2f1567828449.jpg',
            '5d73289e80f191567828126.jpg',
            '5d732980c21231567828352.jpg',
            '5d73293146b931567828273.jpg',
            '5d73299853b3f1567828376.jpg'
        ];

        Inspiration::factory()->count(10)->create()->each(function ($inspiration) use ($images, $covers, $galleries){
            $caterId = Category::inRandomOrder()->whereType('cater')->take(1)->get()->pluck('id');
            $inspiration->categories()->attach($caterId);
            $inspiration->categories()->attach(Category::inRandomOrder()->whereType('color')->take(5)->get()->pluck('id'));
            $inspiration->categories()->attach(Category::inRandomOrder()->whereType('setting')->whereParentId($caterId)->take(1)->get()->pluck('id'));
            $inspiration->categories()->attach(Category::inRandomOrder()->whereType('style')->whereParentId($caterId)->take(1)->get()->pluck('id'));
        
            $thumbnail = $inspiration->addImage("thumbnail", $images[array_rand($images, 1)], public_path('images/thumbnails'), true);
            $inspiration->images()->save($thumbnail);

            $cover = $inspiration->addImage("cover", $covers[array_rand($covers, 1)], public_path('images/covers'), true);
            $inspiration->images()->save($cover);

            foreach($galleries as $image){
                $gallery = $inspiration->addImage("gallery", $galleries[array_rand($galleries, 1)], public_path('images/gallery'), true);
                $inspiration->images()->save($gallery);
            }

            //for venue supplier    
            $venueRole = Role::whereName('venues')->first();
            $venueCompany = Company::whereHas('roles', function($q){
                $q->where('id', 'venues');
            })->first();

            if($venueCompany){
                $supplierCompany = new SupplierInspiration;
                $supplierCompany->role_id = 6;
                $supplierCompany->company_id = $venueCompany->id; // this might optional
                $supplierCompany->status = 'confirmed';
                $supplierCompany->inspiration_id = $inspiration->id;
                $supplierCompany->save();
            }

            //Other Supplier 
            // $rand = random_int(3,5);
            // for($i=0; $i < $rand; $i++){
                $role = Role::inRandomOrder()->where('name', 'decor_hire')->first();
                
                $company = Company::whereHas('roles', function($q) use ($role){
                    $q->where('id', $role->id);
                })->first();
                
                if($company){
                    $otherSupplierCompany = new SupplierInspiration;
                    $otherSupplierCompany->role_id = $role->id;
                    $otherSupplierCompany->company_id = $company->id; // this might optional
                    $otherSupplierCompany->status = 'confirmed';
                    $otherSupplierCompany->inspiration_id = $inspiration->id;
                    $otherSupplierCompany->save();
                }
            // }

            // $inspiration->reviews()->createMany(factory(Review::class, 15)->make()->toArray());
        });
    }
}
