<?php
namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CaterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name' => 'Wedding','type' => 'cater', 'subcategories' =>
                [
                   'Automn',
                   'Boho',
                   'Casual',
                   'Country',
                   'Formal',
                   'Garden',
                   'Glamorous',
                   'Industrial',
                   'Modern',
                   'Romantic',
                   'Rustic',
                   'Spring',
                   'Summer',
                   'Tented',
                   'Traditional',
                   'Vintage',
                   'Winter',
                ]
            ],
            ['name' => 'Corporate','type' => 'cater', 'subcategories' =>
                [
                    'Awards',
                    'Christmas',
                    'PartyCocktail',
                    'Networking',
                    'Fundraisers',
                    'Gala',
                    'Product',
                    'Launch',
                    'LaunchSeminars / ConferenceTrade',
                    'ShowsTrade Show',
                    'ShowsTrade Show / Exhibition',
                ]],
            ['name' => 'Special Events','type' => 'cater', 'subcategories' =>[
                'Baby Shower',
                'Birthday',
                'Christening / Baptism',
                'Engagement',
                'Formal',
                'Gender RevealReunion',
            ]],
            ['name' => 'Cultural','type' => 'cater', 'subcategories' => [
                'Chinese',
                'European',
                'Indian',
                'Islamic',
                'Jewish',
                'Lebanese',
                'South East Asian',
            ]],
        ];

        $settings = [
            'Bar / Restaurant',
            'Boats / Cruises',
            'Destination',
            'Event Space',
            'Hotel',
            'Outdoor',
            'Reception',
        ];

        Category::whereType('cater')->delete();
        Category::whereType('style')->delete();
        Category::whereType('setting')->delete();

        foreach($data as $row){
            $cater = new Category;
                $cater->name = $row['name'];
                $cater->type = $row['type'];
                $cater->module = "category";
                $cater->save();

                foreach($row['subcategories'] as $sub){
                    $subCat = new Category;
                    $subCat->name = $sub;
                    $subCat->parent_id = $cater->id;
                    $subCat->type = "style";
                    $subCat->module = "category";
                    $subCat->save();
                }
                foreach($settings as $sub){
                    $subCat = new Category;
                    $subCat->name = $sub;
                    $subCat->parent_id = $cater->id;
                    $subCat->type = "setting";
                    $subCat->module = "category";
                    $subCat->save();
                }
        }
    }
}
