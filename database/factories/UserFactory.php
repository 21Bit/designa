<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\User;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'email' => $this->faker->safeEmail(),
            'type'  => 'supplier',
            'email_verified_at' => now(),
            'password' => bcrypt("password"),
            'type' => 'customer',
            'company_id' => '',
            'remember_token' => \Str::random(10),
        ];
    }

    public function verified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => now()
            ];
        });
    }
     public function superadmin()
    {
        return $this->state(function (array $attributes) {
            return [
                'is_superadmin' => 1,
                'type' => 'supplier'
            ];
        });
    }

    public function ontrial()
    {
        return $this->state(function (array $attributes) {
            return [
                'trial_ends_at' => now()->addDays(30),
            ];
        });
    }


    public function supplier()
    {
        return $this->state(function (array $attributes) {
            return [
                'type' => 'supplier',
            ];
        });
    }

    public function customer()
    {
        return $this->state(function (array $attributes) {
            return [
                'type' => 'customer',
            ];
        });
    }
}
