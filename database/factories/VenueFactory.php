<?php
namespace Database\Factories;

use App\Models\Venue;
use App\Models\Company;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factories\Factory;

class VenueFactory extends Factory{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Venue::class;


      /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(){
        return [
            'company_id'        => Company::factory()->create()->id, //Company::inRandomOrder()->first()->id,
            'name'              => $this->faker->catchPhrase,
            'description'       => $this->faker->sentence(30),
            'location'          => $this->faker->address,
            'email'             => $this->faker->safeEmail,
            'contact_number'    => $this->faker->phoneNumber,
            'capacity'          => random_int(4,500),
            'latitude'          => $this->faker->latitude,
            'longitude'         => $this->faker->longitude,
            'price'             => random_int(100,5000),
            'capacity'          => random_int(100,5000),
            'max_standing'      => random_int(100,5000),
            'max_seated'        => random_int(100,5000),
        ];
    }


    public function published()
    {
        return $this->state(function (array $attributes) {
            return [
                'is_published' => 1,
            ];
        });
    }


    public function locked()
    {
        return $this->state(function (array $attributes) {
            return [
                'is_lock' => 1,
            ];
        });
    }


    public function featured()
    {
        return $this->state(function (array $attributes) {
            return [
                'featured' => 1,
            ];
        });
    }
}

