<?php
namespace Database\Factories;

use App\Models\Company;
use App\Models\Inspiration;
use App\Models\Venue;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factories\Factory;

class InspirationFactory extends Factory{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Inspiration::class;


     /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'company_id'    => Company::factory()->create()->id,
            'name'          => $this->faker->catchPhrase,
            'description'   => $this->faker->sentence(30),
            'project_year'  => random_int(2016,2024),
            'venue_id'      => Venue::factory()->create()->id,
            'location'      => $this->faker->secondaryAddress,
            'latitude'      => $this->faker->latitude,
            'longitude'     => $this->faker->longitude,
            'is_published'  => 1,
            'is_lock'       => 0,
            'featured'      => 0,
        ];
    }

    public function published()
    {
        return $this->state(function (array $attributes) {
            return [
                'is_published' => 1,
            ];
        });
    }

    public function locked()
    {
        return $this->state(function (array $attributes) {
            return [
                'is_lock' => 1,
            ];
        });
    }


    public function featured()
    {
        return $this->state(function (array $attributes) {
            return [
                'featured' => 1,
            ];
        });
    }
}
