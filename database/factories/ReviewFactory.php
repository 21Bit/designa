<?php
namespace Database\Factories;

use App\Models\User;
use App\Models\Review;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factories\Factory;


class ReviewFactory extends Factory{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Review::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'user_id' => User::factory()->create()->id,
            'comment' => $this->faker->sentence(10),
            'rating'  => random_int(0,5)
        ];
    }
}
