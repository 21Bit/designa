<?php
namespace Database\Factories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Color;
use Faker\Generator as Faker;

class ColorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Category::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        $color = $this->faker->safeColorName;
        return [
            'name' => $this->faker->name,
            'description' => $this->faker->hexcolor($color),
            'type' => 'color',
            'module' => 'category',
            'credit_by' => $this->faker->name,
            'credit_location' => $this->faker->address
        ];
    }
}
