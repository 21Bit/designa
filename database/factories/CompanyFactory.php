<?php
namespace Database\Factories;

use App\Models\Company;
use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Company::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->catchPhrase,
            'description' => $this->faker->sentence(30),
            'will_travel' => $this->faker->sentence(30),
            'services' => $this->faker->sentence(30),
            'recognition' => $this->faker->sentence(30),

            //addess
            'town'  => $this->faker->city,
            'state' => $this->faker->state,
            'country' => $this->faker->country,
            'post_code' => $this->faker->postcode,

            //contacts
            'email' => $this->faker->email,
            'website'   => $this->faker->url,
            'telephone_number' => $this->faker->tollFreePhoneNumber,
        ];
    }

    public function active()
    {
        return $this->state(function (array $attributes) {
            return [
                'is_active' => 1
            ];
        });
    }

    public function singleVenue()
    {
        return $this->state(function (array $attributes) {
            return [
                'single_venue' => 1
            ];
        });
    }


    public function locked()
    {
        return $this->state(function (array $attributes) {
            return [
                'is_lock' => 1
            ];
        });
    }
}
