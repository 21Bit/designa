<?php
namespace Database\Factories;

use App\Models\Product;
use App\Models\Qoute;
use App\Models\QouteProduct;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factories\Factory;

class QouteProductFactory extends Factory{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = QouteProduct::class;


     /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
     {
        return [
            'qoute_id' => Qoute::factory()->create()->id,
            'product_id' => Product::factory()->create()->id,
            'quantity' => random_int(1,12)
        ];
    }
}
