<?php

namespace Database\Factories;

use App\Models\ModelFile;
use Illuminate\Database\Eloquent\Factories\Factory;

class ModelFileFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ModelFile::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'model_name' => $this->faker->words,
            'path' =>  $this->faker->file('/model','/model'),
            'is_public' => 1,
            'number_of_guest' => random_int(10,100),
            'type' => 'asset_bundle_web',
            'base_layer_id' => '',
            'decoration_template_id' => '',
            'modellable_id' => '',
        ];
    }


    public function public()
    {
        return $this->state(function (array $attributes) {
            return [
                'is_pubic' => 1,
            ];
        });
    }


    public function baseLayer()
    {
        return $this->state(function (array $attributes) {
            return [
                'type' => 'base_layer',
            ];
        });
    }

    public function decoration()
    {
        return $this->state(function (array $attributes) {
            return [
                'type' => 'decoration',
            ];
        });
    }

    public function assetBundleWeb()
    {
        return $this->state(function (array $attributes) {
            return [
                'type' => 'asset_bundle_web',
            ];
        });
    }


    public function assetBundleAndroid()
    {
        return $this->state(function (array $attributes) {
            return [
                'type' => 'asset_bundle_android',
            ];
        });
    }

    public function assetBundleIos()
    {
        return $this->state(function (array $attributes) {
            return [
                'type' => 'asset_bundle_ios',
            ];
        });
    }




}
