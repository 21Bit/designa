<?php

namespace Database\Factories;

use App\Models\Company;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'company_id' => Company::factory()->active()->create()->id,
            'name' => $this->faker->catchPhrase,
            'description' => $this->faker->sentence(30),
            'quantity' => random_int(4,20),
            'product_identifier' => random_int(4,20),
            'product_size' => random_int(4,20),
        ];
    }

    public function published()
    {
        return $this->state(function (array $attributes) {
            return [
                'is_published' => 1,
            ];
        });
    }


    public function locked()
    {
        return $this->state(function (array $attributes) {
            return [
                'is_lock' => 1,
            ];
        });
    }


    public function featured()
    {
        return $this->state(function (array $attributes) {
            return [
                'featured' => 1,
            ];
        });
    }

}
