<?php
namespace Database\Factories;

use App\Models\Venue;
use App\Models\FunctionSpace;
use Illuminate\Database\Eloquent\Factories\Factory;

class FunctionSpaceFactory extends Factory{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = FunctionSpace::class;


     /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(){
        return [
            'name'                  => $this->faker->catchPhrase,
            'venue_id'              => Venue::factory()->create()->id,
            'description'           => $this->faker->sentence(30),
            'max_capacity_seated'   => random_int(1, 25),
            'max_capacity_standing' => random_int(25,1000),
            'description'           => $this->faker->sentence(30),
        ];
    }
}
