<?php

namespace Database\Factories;

use App\Models\SubscriptionPlan;
use App\Models\SubscriptionPlanPrice;
use Illuminate\Database\Eloquent\Factories\Factory;

class SubscriptionPlanPriceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SubscriptionPlanPrice::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'subscription_plan_id' => SubscriptionPlan::factory()->create(),
            'billing_period' => 'monthly',
            'stripe_id'         => $this->faker->text,
            'price'         => $this->faker->randomFloat()
        ];
    }
}
