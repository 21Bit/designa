<?php
namespace Database\Factories;

use App\Models\Qoute;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factories\Factory;

class QouteFactory extends Factory{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Qoute::class;


     /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
     {
        return [
            'reference_number' => time() . rand(10*45, 100*98),
            'user_id' => User::factory()->customer()->create(),
            'event_name' => $this->faker->name,
            'event_type' => Category::factory()->cater()->create()->id,
            'location' => $this->faker->address,
            'event_date' => $this->faker->date,
            'time_start' => $this->faker->date,
            'time_end' => $this->faker->time,
            'number_of_guests' => 50,
        ];
     }
}
