<?php
namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Category;

class CategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Category::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->name,
            'description' => $this->faker->text(10),
            'type' => 'cater',
            'module' => 'category',
            'credit_by' => '',
            'credit_location' => ''
        ];
    }

    public function cater()
    {
        return $this->state(function (array $attributes) {
            return [
                'type' => 'cater',
                'module' => 'category'
            ];
        });
    }

    public function styleCategory()
    {
        return $this->state(function (array $attributes) {
            return [

                'type' => 'style',
                'module' => 'category'
            ];
        });
    }

    public function styleProduct()
    {
        return $this->state(function (array $attributes) {
            return [

                'type' => 'style',
                'module' => 'product'
            ];
        });
    }


    public function setting()
    {
        return $this->state(function (array $attributes) {
            return [

                'type' => 'setting',
                'module' => 'category'
            ];
        });
    }

    public function productType()
    {
        return $this->state(function (array $attributes) {
            return [

                'type' => 'product_type',
                'module' => 'category'
            ];
        });
    }

    public function color()
    {
        return $this->state(function (array $attributes) {
            return [

                'type' => 'color',
                'module' => 'category'
            ];
        });
    }

    public function venueType()
    {
        return $this->state(function (array $attributes) {
            return [
                'type' => 'venue_type',
                'module' => 'venue'
            ];
        });
    }

    public function averageCostPerHead()
    {
        return $this->state(function (array $attributes) {
            return [
                'type' => 'average_cost_per_head',
                'module' => 'venue'
            ];
        });
    }

    public function cateringOption()
    {
        return $this->state(function (array $attributes) {
            return [
                'type' => 'catering_option',
                'module' => 'venue'
            ];
        });
    }


    public function beverageOption()
    {
        return $this->state(function (array $attributes) {
            return [
                'type' => 'beverage_option',
                'module' => 'venue'
            ];
        });
    }
    public function amenity()
    {
        return $this->state(function (array $attributes) {
            return [
                'type' => 'amenity',
                'module' => 'venue'
            ];
        });
    }
}
