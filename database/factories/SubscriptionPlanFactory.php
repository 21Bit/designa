<?php

namespace Database\Factories;

use App\Models\SubscriptionPlan;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class SubscriptionPlanFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SubscriptionPlan::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->text;
        return [
            'stripe_id' => $this->faker->text,
            'name' => $name,
            'slug' => Str::slug($name),
            'type' => 'basic'
        ];
    }


    public function published()
    {
        return $this->state(function (array $attributes) {
            return [
                'is_published' => 1,
            ];
        });
    }
}
