<?php

Route::group(['as' => 'dashboard.', 'prefix' => 'dashboard', 'middleware' => ['auth', 'supplier'], ], function(){
    Route::get('/home', 'Dashboard\HomeController@index')->name('home');
    Route::post('/loggeduser', 'Dashboard\HomeController@loggedUser');

    Route::post('/notification/apilist', 'Dashboard\NotificationController@apiList');
    Route::resource('/notification', 'Dashboard\NotificationController');
    Route::get('/profile', 'Dashboard\ProfileController@index')->name('profile.index');
    Route::put('/profile', 'Dashboard\ProfileController@update')->name('profile.update');
    Route::post('/profile/addlocation/{id}', 'Dashboard\ProfileController@addAreaLocation')->name('profile.addlocation');
    Route::post('/profile/getlocations/{id}', 'Dashboard\ProfileController@getLocations')->name('profile.getlocations');
    Route::delete('/profile/deletelocation/{id}', 'Dashboard\ProfileController@deleteLocation')->name('profile.deletelocation');
    Route::put('/profile/account', 'Dashboard\ProfileController@updateAccount')->name('profile.update.account');
    Route::post('/profile/changepassword', 'Dashboard\ProfileController@changePassword')->name('profile.changepassword');
    Route::post('/profile/modules', 'Dashboard\ProfileController@modules')->name('profile.modules');

    Route::resource('user', 'Dashboard\UserController');
    Route::get('company/lift/{id}', 'Dashboard\CompanyController@liftUsers')->name('company.liftusers');
    Route::post('company/make-live', 'Dashboard\CompanyController@sendActiveLink')->name('company.make-live');
    Route::post('company/add-trial', 'Dashboard\CompanyController@addTrial')->name('company.add-trial');
    Route::post('company/cancel-trial', 'Dashboard\CompanyController@cancelTrial')->name('company.cancel-trial');
    Route::resource('company', 'Dashboard\CompanyController');

    // Chat
    Route::get('chat', 'Dashboard\ChatController@index')->name('chat.index');
    Route::post('chat/newchat', 'API\ChatController@newChat');
    Route::post('chat/newmessage', 'API\ChatController@newMessage');

    // blog
    Route::resource('blog', 'Dashboard\BlogController');

    Route::get('/logs', 'Dashboard\ActivityController@index')->name('activity-log.index');
    // Route::post('/statistics', 'Dashboard\ActivityController@statistics')->name('activity.statistics');
    // Route::get('/statistics', 'Dashboard\ActivityController@statistics')->name('activity.statistics');
    //categories
    Route::post('category/{id}/sub-categories', 'Dashboard\CategoryController@subCategories')->name('category.sub-categories'); // used in ajax required (subcategory )
    Route::post('category/sub-categories' ,'Dashboard\CategoryController@storeSubCategory')->name('category.store.subcategory'); // used in ajax required (subcategory )
    Route::post('category/sub-categories/{id}' ,'Dashboard\CategoryController@updateSubCategory')->name('category.update.subcategory'); // used in ajax required (subcategory )
    Route::resource('category', 'Dashboard\CategoryController');

    Route::resource('color', 'Dashboard\ColorController');
    Route::post('role/{id}/assignpermission', 'Dashboard\RoleController@assignPermission')->name('role.assignpermission');
    Route::resource('role', 'Dashboard\RoleController');

    // Inspiration
        // Ajax
        Route::get('inspiration/api/caters', 'Dashboard\InspirationController@caters')->name('inspiration.api.caters');
        Route::any('inspiration/api/companies', 'Dashboard\InspirationController@companies')->name('inspiration.api.companies');
        Route::any('inspiration/api/companies/role', 'Dashboard\InspirationController@roles')->name('inspiration.api.company.role');
        Route::any('inspiration/api/venues', 'Dashboard\InspirationController@venues')->name('inspiration.api.venues');


        Route::post('inspiration/{id}/upload', 'Dashboard\InspirationController@uploadgallery')->name('inspiration.uploadgallery');
        Route::post('inspiration/{id}/uploadvideo', 'Dashboard\InspirationController@uploadVideo')->name('inspiration.uploadvideo');
        Route::delete('inspiration/{id}/deletegallery', 'Dashboard\InspirationController@deletegallery')->name('inspiration.deletegallery');
        Route::delete('inspiration/{id}/deletevideo', 'Dashboard\InspirationController@deletevideo')->name('inspiration.deletevideo');
        Route::post('inspiration/{id}/addsupplier', 'Dashboard\InspirationController@addSupplier')->name('inspiration.addsupplier');
        Route::delete('inspiration/{id}/deletesupplier', 'Dashboard\InspirationController@deleteSupplier')->name('inspiration.deletesupplier');

        // Tagged Inspirations
        Route::get('/inspirations/tags', 'Dashboard\InspirationTagController@index')->name('inspiration.tags.index');
        Route::get('/inspirations/tags/{id}', 'Dashboard\InspirationTagController@show')->name('inspiration.tags.show');
        Route::get('/inspirations/tags/{id}/accept', 'Dashboard\InspirationTagController@show')->name('inspiration.tags.accept');

        Route::post('/inspirations/tags/{id}/status', 'Dashboard\InspirationTagController@changeStatus')->name('inspiration.tags.changestatus');
        Route::post('/inspirations/tags/{id}/addproduct', 'Dashboard\InspirationTagController@addProduct')->name('inspiration.tags.addproduct');
        Route::get('/inspirations/tags/{id}/{product}/removeproduct', 'Dashboard\InspirationTagController@removeProduct')->name('inspiration.tags.removeproduct');

        // Get Supplier venues
        Route::get('inspiration/getvenues/{id}','Dashboard\InspirationController@getVenues')->name('inspiration.getvenues');
        Route::get('inspiration/getproducts/{id}','Dashboard\InspirationController@getProducts')->name('inspiration.getproducts');
        Route::resource('inspiration', 'Dashboard\InspirationController');

    // Venue Routes
    Route::post('venue/{id}/upload', 'Dashboard\VenueController@uploadgallery')->name('venue.uploadgallery');
    Route::resource('venue', 'Dashboard\VenueController');
    Route::post('functionspace/{id}/upload-asset-bundle', 'Dashboard\FunctionSpaceController@uploadAssetBundle')->name('functionspace.upload-asset-bundle');
    Route::delete('functionspace/{id}/delete-asset-bundle', 'Dashboard\FunctionSpaceController@deleteModel')->name('functionspace.delete-asset-bundle');
    Route::resource('functionspace', 'Dashboard\FunctionSpaceController');

    // Product Route
    Route::post('decor/{id}/upload-asset-bundle', 'Dashboard\ProductController@uploadAssetBundle')->name('product.upload-asset-bundle');
    Route::delete('decor/{id}/delete-asset-bundle', 'Dashboard\ProductController@deleteModel')->name('product.delete-asset-bundle');
    Route::post('decor/{id}/upload', 'Dashboard\ProductController@uploadgallery')->name('product.uploadgallery');
    Route::delete('decor/{id}/delete', 'Dashboard\ProductController@deletegallery')->name('product.deletegallery');
    // Route::resource('/product', 'Dashboard\ProductController');

    // 3D models Routes
    Route::resource('3d/baselayer', 'Dashboard\BaseLayerController');

    Route::get('3d/baselayer/{id}/generate-template', 'Dashboard\BaseLayerController@generateTemplate')->name('baselayer.generate-template');
    Route::get('3d/template{id}/generate-floorplan', 'Dashboard\TemplateModelController@generateFloorplan')->name('template.generate-floorplan');
    Route::resource('3d/template', 'Dashboard\TemplateModelController');
    Route::post('3d/template/product-list/{id}', 'Dashboard\TemplateModelController@getProducts')->name('template.product-list');
    Route::post('3d/template/generate-quote', 'Dashboard\TemplateModelController@generateQuote')->name('template.generate-quote');
    Route::get('3d/floorplan/{id}/generate-template', 'Dashboard\FloorPlanModelController@generateTemplate')->name('floorplan.generate-template');
    Route::resource('3d/floorplan', 'Dashboard\FloorPlanModelController');

    // Project Route
    Route::get('event-board/update/{id}/categories', 'Dashboard\ProjectController@editCategories')->name('event-board.edit.categories');
    Route::post('event-board/update/{id}/categories', 'Dashboard\ProjectController@editCategoriesPost')->name('event-board.update.categories');

    Route::get('event-board/update/{id}/inspirations', 'Dashboard\ProjectController@editInspirations')->name('event-board.edit.inspirations');
    Route::get('event-board/update/{id}/preview', 'Dashboard\ProjectController@preview')->name('event-board.edit.preview');
    Route::get('event-board/update/{id}/preview/api', 'Dashboard\ProjectController@previewApi')->name('event-board.edit.preview.api');
    Route::get('event-board/update/{id}/suppliers', 'Dashboard\ProjectController@suppliers')->name('event-board.edit.suppliers');

    Route::get('event-board/update/{id}/products', 'Dashboard\ProjectController@editProducts')->name('event-board.edit.products');
    Route::get('event-board/update/{id}/products-listing', 'Dashboard\ProjectController@productsListing')->name('event-board.edit.products.listing');
    Route::post('event-board/update/{id}/inspirations', 'Dashboard\ProjectController@editInspirationsPost')->name('event-board.update.inspirations');
    Route::post('event-board/update/{id}/products', 'Dashboard\ProjectController@editProductsPost')->name('event-board.update.products');
    Route::get('event-board/project/{id}/generate-pitchpack', 'Dashboard\ProjectController@generatePDf')->name('eventboard.generate.pitchpack');
    Route::post('event-board/project/update-image', 'Dashboard\ProjectController@attachImage');

    Route::get('event-board/{id}/pdf', 'Dashboard\ProjectController@pdf');
    Route::resource('event-board', 'Dashboard\ProjectController');
    Route::resource('event-board', 'Dashboard\ProjectController');
    // Route::resource('/product', 'Dashboard\ProductController');

    Route::post('subscription/send-amendment', 'Dashboard\SubscriptionController@sendAmendment')->name('subscription.send.amendment');
    Route::post('payment-method/setdefault/{id}', 'Dashboard\PaymentMethodController@setDefault')->name('payment-method.setdefault');
    Route::resource('payment-method', 'Dashboard\PaymentMethodController');
    Route::get('subscription/resume/{id}', 'Dashboard\SubscriptionController@resumeSubscription')->name('subscription.resume');
    Route::get('subscription/cancel/{id}', 'Dashboard\SubscriptionController@cancelSubscription')->name('subscription.cancel');

    //subscription routes
        // Plans
        Route::post('/subscription-plan/add-price', 'Dashboard\SubscriptionPlanController@addPrice')->name('subscription-plan.price.add');
        Route::post('/subscription-plan/{id}/update-price', 'Dashboard\SubscriptionPlanController@updatePrice')->name('subscription-plan.price.update');
        Route::resource('subscription-plan', 'Dashboard\SubscriptionPlanController');

        // user subscriptions
        Route::get('/subscription/plans', 'Dashboard\SubscriptionController@plans')->name('subscription.plans');
        Route::get('/subscription/methods', 'Dashboard\SubscriptionController@methods')->name('subscription.methods');
        Route::get('/subscription/invoice/{invoice}','Dashboard\SubscriptionController@downloadInvoice')->name('subscription.invoice.download');
        Route::resource('subscription', 'Dashboard\SubscriptionController');


    // for quote system
    Route::get('/quote', 'Dashboard\QuoteController@index')->name('quote.index');
    Route::get('/quote/show/{ref}', 'Dashboard\QuoteController@show')->name('quote.show');
    Route::post('/quote/savepricing/{id}', 'Dashboard\QuoteController@savePricing')->name('quote.savepricing');
    Route::post('/quote/review', 'Dashboard\QuoteController@sendReview')->name('quote.review.save');
    Route::get('/quote/attachments/{id}', 'Dashboard\QuoteController@getAttachment')->name('quote.attachment.list');
    Route::post('/quote/uploadattachment', 'Dashboard\QuoteController@uploadAttachment')->name('quote.attachment.upload');

    Route::resource('pricing-component', 'Dashboard\PricingComponentController');


    Route::get('invite', 'Dashboard\InviteController@invite')->name('invite.index');
    Route::post('invite', 'Dashboard\InviteController@inviteSend')->name('invite.send');
/*
    * NOTE!!!
    * Made custom routes because of server error (save/store is not working) 24/10/2019
*/
    Route::get('decor', 'Dashboard\ProductController@index')->name('product.index');
    Route::get('decor/create', 'Dashboard\ProductController@create')->name('product.create');
    Route::get('decor/{id}', 'Dashboard\ProductController@show')->name('product.show');
    Route::get('decor/{id}/edit', 'Dashboard\ProductController@edit')->name('product.edit');;
    Route::put('decor/{id}', 'Dashboard\ProductController@update')->name('product.update');;
    Route::post('decor/save', 'Dashboard\ProductController@store')->name('product.save');
    Route::delete('decor/{id}', 'Dashboard\ProductController@destroy')->name('product.destroy');
    // Route::post('/product', 'Dashboard\Product   Controller@store')->name('product.store');
    // Route::resource('product-type', 'Dashboard\ProductTypeController')->except(['create', 'show']);

    //For Gallery
    Route::delete('gallery/{id}', 'Dashboard\GalleryController@destroy')->name('gallery.destroy');
});
