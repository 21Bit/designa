<?php

Route::group(['as' => 'user.', 'prefix' => 'user', 'middleware' => ['auth', 'customer', 'verified'], ], function(){

    Route::get('/profile', 'Site\User\ProfileController@index')->name('profile.index');
    Route::get('/profile/edit', 'Site\User\ProfileController@edit')->name('profile.edit');
    Route::put('/profile', 'Site\User\ProfileController@update')->name('profile.update');
    Route::put('/profile', 'Site\User\ProfileController@update')->name('profile.update');
    Route::get('/profile/changepassword', 'Site\User\ProfileController@changePassword')->name('profile.changepassword');
    Route::post('/profile/changepassword', 'Site\User\ProfileController@changePasswordPost')->name('profile.changePasswordPost');

    //saved items
    Route::get('/saved/inspiration', 'Site\User\SaveItemController@inspirations')->name('saved.inspirations');
    Route::get('/saved/venue', 'Site\User\SaveItemController@venues')->name('saved.venues');
    Route::get('/saved/decor', 'Site\User\SaveItemController@decors')->name('saved.decors');

    //Qoutes
    Route::post('qoute/list', 'Site\User\QouteController@list')->name('qoute.list.filter');
    Route::resource('qoute', 'Site\User\QouteController');

    //Project
    // Route::post('project/inspirations', 'Site\User\ProjectController@inspirations')->name('project.inspirations');

    Route::get('eventboard/{project}/categories', 'Site\User\EventBoardController@editCategories')->name('event-board.edit.categories');
    Route::post('eventboard/{project}/categories', 'Site\User\EventBoardController@updateCategories')->name('event-board.update.categories');

    Route::get('eventboard/{project}/inspirations', 'Site\User\EventBoardController@editInspirations')->name('event-board.edit.inspirations');
    Route::get('eventboard/inspiration-list/{project}', 'Site\User\EventBoardController@inspirationList')->name('event-board.edit.inspiration.list');

    Route::get('eventboard/{project}/decors', 'Site\User\EventBoardController@editDecors')->name('event-board.edit.decors');
    Route::get('eventboard/decor-list/{project}', 'Site\User\EventBoardController@decorList')->name('event-board.edit.decor.list');

    Route::post('eventboard/update-images', 'Site\User\EventBoardController@attachImage')->name('event-board.update.images');
    Route::get('eventboard/{project}/preview', 'Site\User\EventBoardController@preview')->name('event-board.preview');
    Route::post('eventboard/{project}/preview-api', 'Site\User\EventBoardController@previewApi')->name('event-board.preview.api');
    Route::resource('event-board', 'Site\User\EventBoardController');

    // Notifications
    Route::get('/notification', 'Site\User\NotificationController@index')->name('notification.index');
    Route::delete('/notification/{id}', 'Site\User\NotificationController@destroy')->name('notification.destroy');
    Route::delete('/notification/deletelall', 'Site\User\NotificationController@deletelAll')->name('notification.deletelall');

});
