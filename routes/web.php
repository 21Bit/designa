<?php

use Carbon\Carbon;
use App\Models\User;
use App\Models\Company;
use App\Models\Category;
use App\Models\Firebase;
use App\Models\Location;
use App\Models\Inspiration;
use Jenssegers\Agent\Agent;
use App\Facades\FirebaseFMC;
use App\Http\Controllers\WebhookController;
use App\Models\FunctionSpace;
use App\Mail\Site\InquiryMail;
use App\Mail\Dashboard\InviteMail;
use App\Models\SupplierInspiration;
use App\Mail\Dashboard\TagInvitation;
use Illuminate\Support\LazyCollection;
use App\Mail\Dashboard\TagSupplierProduct;
use App\Mail\Site\Request3d\AutoReplyMail;
use Symfony\Component\HttpFoundation\Request;
use App\Http\Resources\ModelFile\BaseLayerFile;
use App\Notifications\Customer\QuoteResponseNotification;
use App\Notifications\All\NewInspirationCreatedNotification;
use App\Notifications\Supplier\Hearts\HeartedInspirationNotification;
use App\Services\GoogleService;
use App\Services\SubscriptionService;

Route::get('/producttypes', function(){
    return Category::whereType('product_type')->with('styles')->get();
});


Route::get('test-location', function(Request $request){
    $distance = $request->distance ?? 5;
    return Company::select(
            DB::raw('*, ( 6367 * acos( cos( radians('.$request->latitude.') )
                 * cos( radians( latitude ) )
                 * cos( radians( longitude ) - radians('.$request->longitude.') )
                 + sin( radians('.$request->latitude.') )
                 * sin( radians( latitude ) ) ) ) AS address_distance')
        )
        ->orWhereHas('locations', function($q) use ($request, $distance){
            $q->select(
                DB::raw('*, ( 6367 * acos( cos( radians('.$request->latitude.') )
                 * cos( radians( latitude ) )
                 * cos( radians( longitude ) - radians('.$request->longitude.') )
                 + sin( radians('.$request->latitude.') )
                 * sin( radians( latitude ) ) ) ) AS distance')
            )
            ->whereNotNull('latitude')
            ->whereNotNull('longitude')
            ->having('distance', '<=', $distance);
        })
        ->whereNotNull('latitude')
        ->whereNotNull('longitude')
        ->orderBy('address_distance')->get();
});

// Route::stripeWebhooks('/stripe/webhook');

Route::get('preview/email', function(){
    Mail::to('jbdev21@gmail.com')->send(new AutoReplyMail());
});


Route::get('/inspirations/tags/{id}/accept', 'Site\HomeController@accept')->name('site.inspiration.tags.accept');
Route::get('/inspiration/tag/accepted', 'Site\HomeController@accepted')->name('site.inspiration.tags.accepted');


Route::get('lift/{email}', function($email){
    $user = User::where('email', $email)->first();
    if($user){
        $user->email_verified_at = date('Y-m-d h:i:s');
        $user->save();
        return "Account lifted successfully.";
    }

    return "No account found with that email.";
});


Route::get('product-list-with-categories', function(){
    $products =  App\Models\Product::with('categories')->get();
    echo "<table border='1'>
        <thead>
            <tr>
                <th>Product Name</th>
                <th>Category</th>
                <th>Subcategory</th>
            </tr>
        </thead>
        <tbody>";
        foreach($products as $product){
            echo "<tr>
                <td> $product->name</td>
                <td>" . optional($product->product_type)->name . "</td>
                <td>" . optional($product->style)->name . " </td>
            </tr>";
        }
    echo "</tbody>
    </table>";
});


route::get('autologin/{id}', function(Request $request){
    $word = $request->id;
    $removePrefix = str_replace(config('autologin.prefix'),'', $word);
    $id = str_replace(config('autologin.suffix'), '', $removePrefix);
    Auth::loginUsingId($id);

    // $activity = new App\Models\ActivityLog;
    // $activity->details = "Login in the system by admin";
    // $activity->type ="LOGIN";
    // $activity->origin = "web";

    // Auth::user()->activities()->save($activity);

    if($request->activation){
        $company = Company::find(Auth::user()->company_id);
        $company->is_active = 1;
        $company->save();
        $agent = new Agent;

        Inspiration::where('company_id', $company->id)
                ->update(['is_published' => 1]);

        App\Models\Venue::where('company_id', $company->id)
                ->update(['is_published' => 1]);

        App\Models\Product::where('company_id', $company->id)
                ->update(['is_published' => 1]);

        if($agent->isDesktop()){
            return redirect()->route('dashboard.profile.index',['tab' => 'business'])->with('success', 'Congratulations your profile is now live.');
        }else{
            return redirect()->route('mobile.profile.live');
        }
    }
    if($request->api == 1){
        return $tokenResult = Auth::user()->createToken('Designa Password Grant Client');
    }

    return redirect('/');
})->name('autologin');


Route::get('getlogs', function(Request $request){
    $companies = Company::all();
    if($request->type == "registrations"){
        $list = User::whereHas('company');

        if($request->date){
            $list->whereDate('created_at', $request->date);
        }

    }else{
        if($request->supplier){
            $list = User::whereType('supplier')->whereHas('company', function($q) use($request){
                $q->whereId($request->supplier);
            });
        }else{
            $list = User::whereType('supplier')->whereHas('company');
        }

        if($request->date){
            $list->whereHas('activities', function($q) use($request){
                $q->whereType('LOGIN')->whereDate('created_at',$request->date);
            });
        }
    }


    $suppliers = $list->get();

    if($request->type == "registrations"){
        return view('tool.registrations', compact('suppliers'));
    }else{
        return view('tool.logins', compact('suppliers', 'companies'));
    }

    // $html = '<table border="1" cellpadding="0" cellspacing="0">
    //             <thead>
    //                 <tr>
    //                     <th>Client Number</th>
    //                     <th>Client Name</th>
    //                     <th>Date Registered</th>
    //                     <th>Last Loging Date</th>
    //                     <th>No. of Logins</th>
    //                 </tr>
    //             </thead>
    //             <tbody>
    //         ';
    // foreach($suppliers as $supplier){
    //     $html .= "  <tr>
    //                     <td>$supplier->id</td>
    //                     <td>" . optional($supplier->company)->name . "</td>
    //                     <td>" . $supplier->created_at->format('d/m/Y') . "</td>
    //                     <td>" . optional($supplier->activities()->orderBy('created_at', 'DESC')->first())->created_at . "</td>
    //                     <td>". $supplier->activities()->whereType('LOGIN')->count()  ."</td>
    //                 </tr>
    //             ";
    // }

    // $html .= "</tbody>
    //             </table>
    //         ";

    // return $html;
});


Route::post('/login/web', 'Auth\LoginController@loginWebApi')->name('login.web');

Auth::routes(['verify' => true]);

Route::get('/product-preview', function(){
    return view('site.product-preview');
});

Route::get('inquiry-mail', function(){
    return new App\Mail\Site\InquiryMail("hehe");
});




Route::get('generage32', 'Dashboard\InspirationController@generate32');

// for dashboard routes
    include "web/dashboard.php";

    Route::get('/statistics', 'Dashboard\ActivityController@statistics')->name('activity.statistics');
    Route::post('/statistics', 'Dashboard\ActivityController@statistics')->name('activity.statistics');
    Route::get('/stats', 'Dashboard\ActivityController@getData')->name('activity.stats');
    Route::post('/stats', 'Dashboard\ActivityController@getData')->name('activity.stats');
// for user routes
    include "web/user.php";



Route::get('searchlocation', function(Request $request){
    $lat = $request->lat;
    $lng = $request->lng;
    return $companyIds = Location::select('locationable_id',
        DB::raw("6367 * acos(cos(radians(" . $lat . "))
            * cos(radians(locations.latitude))
            * cos(radians(locations.longitude) - radians(" . $lng . "))
            + sin(radians(" .$lat. "))
            * sin(radians(locations.latitude))) AS distance"))
        ->having('distance', '<', 25)
        ->orderBy('distance', 'asc')
        ->groupBy('locationable_id')
        ->get();

    $results = Company::find($companyIds);

    return $results;
});

Route::post('isloggedin', function(Request $request){
    if($request->user()){
        return 1;
    }

    return 0;
});


Route::get('randomfeature', function(){
    $inspirations = App\Models\Inspiration::all();
    foreach($inspirations as $inspiration){
        $inspiration->featured = random_int(0, 1);
        $inspiration->save();
    }
});

Route::get('migrations', function(){
    $files =   collect(\File::files(database_path('migrations')))
        ->sortBy(function ($q) {
            return str_replace('.php', '', pathinfo($q)['filename']);
        })
        ->map(function($q){
            $migration_file = str_replace('.php', '', pathinfo($q)['filename']);
            \DB::insert('insert INTO migrations (migration, batch) VALUES (?, ?)', [$migration_file, 1]);
            // return pathinfo($q)['filename'];
        });

    return $files;
});


// landing pages
    Route::get('/landing', 'Site\HomeController@mobileLanding')->name('mobile')->middleware('check.for.us.redirect');
    Route::get('/covid-free-events', 'Site\HomeController@landing1')->name('landingpage-2')->middleware('check.for.us.redirect');
    Route::get('/3d-technology', 'Site\HomeController@landing2')->name('landingpage-3d-techonology')->middleware('check.for.us.redirect');
    Route::get('/cost-effective', 'Site\HomeController@landing3')->name('landingpage-3')->middleware('check.for.us.redirect');
    Route::get('/venue-directory', 'Site\HomeController@venueDirectory')->name('landing.venue.directory')->middleware('check.for.us.redirect');
    Route::get('/venue-tours', 'Site\HomeController@venueTours')->name('landing.venue.tours')->middleware('check.for.us.redirect');
    Route::get('/contest-registration', 'Site\HomeController@contestRegistration')->name('landing.contest-registration')->middleware('check.for.us.redirect');
    Route::post('/contest-3d-registration', 'Site\HomeController@contestRegistrationSend')->name('landing.contest-post');

    Route::post('/venue-marketing-usa-send-email','Site\HomeController@venueMarketingUsaSendEmail')->name('landing.venue-marketing-usa-send-email');
    Route::get('/venue-marketing-usa-preregister','Site\HomeController@venueMarketingUsaPreregister')->name('landing.venue-marketing-usa-preregister');
    Route::get('/venue-marketing-usa','Site\HomeController@venueMarketingUsa')->name('landing.venue-marketing-usa');


Route::get('/email-verified', 'Site\HomeController@mobileEmailVerified')->name('email.verified');
Route::get('/profile-live', 'Site\HomeController@mobileProfileLiveLanding')->name('mobile.profile.live');

Route::group(['as' => 'site.', 'middleware' => ['checkmobile', 'check.for.us.redirect']], function(){
    Route::get('/', 'Site\HomeController@index')->name('site.index');

    // 3d Request
    Route::get('request-3d', 'Site\Request3dController@index')->name('request-3d-form');
    Route::post('request-3d', 'Site\Request3dController@submit')->name('request-3d-form.submit');

    // Pricing
    Route::get('pricing', 'Site\PricingController@index')->name('pricing.index');

    // Subscription
    Route::get('subscription', 'Site\SubscriptionController@index')->name('subscription');
    Route::post('subscription', 'Site\SubscriptionController@store')->name('subscription.store');

    // Inquiry
    Route::post('request-demo-covid', 'Site\HomeController@requestDemoCovid')->name('requestdemo.covid');
    Route::post('inquiry', 'Site\HomeController@sendInquiry')->name('sendinquiry');
    Route::post('register-webinar', 'Site\HomeController@registerWebinar')->name('registerWebinar');
    Route::post('request-white-glove-service', 'Site\HomeController@requestWhiteGloveService')->name('registerEmail');
    Route::post('register-email', 'Site\HomeController@registerEmail')->name('registerEmail');
    Route::post('register-newsletter-email', 'Site\HomeController@registerNewsLetterEmail')->name('registerEmail');
    Route::post('request-demo', 'Site\HomeController@requestDemo')->name('requestdemo');
    Route::get('event-registration', 'Site\HomeController@eventRegistration')->name('eventRegister');
    Route::post('event-registration', 'Site\HomeController@eventRegistrationPost')->name('eventRegisterPost');

    Route::get('for-supplier/{tab}', function($tab ){
        $blogs = App\Models\Blog::where('is_published', 1)->orderBY('id', 'DESC')->get();
        $recent_blogs = App\Models\Blog::where('is_published', 1)->orderBY('id', 'DESC')->limit(3)->get();
        // echo $tab;
        $tab_blog = request()->get('tab');

        $blog = explode('/',$tab_blog);
        // echo $blog[0].'<br/>';
        try {
            $tag_blog = $blog[0];
            $get_blog = $blogs[0];
        } catch (\Throwable $th) {
            $get_blog = '';
            $tag_blog = '';
        }
        return view('site.for-supplier-support-pages.' . $tab, compact('blogs','recent_blogs','get_blog','tag_blog'));

    })->name('for-supplier');

    Route::get('/for-supplier/resources/blogs','Site\BlogController@index')->name('blog.index');
    Route::get('/for-supplier/resources/blogs/{id}','Site\BlogController@show')->name('blog.show');
    Route::get('business-profile-assistance-form', 'Site\HomeController@assistanceForm')->name('supplier.assistance.form');
    Route::post('business-profile-assistance-form', 'Site\HomeController@assistanceFormStore')->name('supplier.assistance.form.post');

    // Search
    Route::post('search', 'Site\SearchController@search')->name('search');


    Route::get('/home', 'Site\HomeController@home')->name('home');
    Route::get('/terms-condition', 'Site\HomeController@termCondition')->name('term-condition');
    Route::get('/privacy-policy', 'Site\HomeController@privacyPolicy')->name('privacy-policy');

    //login Ajax
    Route::post('/login/ajax', 'Site\AuthenticationController@login')->name('login.ajax');

    //registration
    Route::get('/register/type', 'Site\RegisterController@type')->name('register.type');

    // register Supplier
    Route::get('/register/supplier', 'Site\RegisterController@registerSupplier')->name('register.supplier');
    Route::post('/register/supplier', 'Site\RegisterController@registerSupplierStore')->name('register.supplier.store');

    // register customer
    Route::get('/register/customer', 'Site\RegisterController@registerCustomer')->name('register.customer');
    Route::post('/register/customer', 'Site\RegisterController@registerCustomerStore')->name('register.customer.store');

    // register Supplier
    Route::get('/register/customer', 'Site\RegisterController@registercustomer')->name('register.customer');
    Route::post('/register/customer', 'Site\RegisterController@registercustomerStore')->name('register.customer.store');


    Route::get('/register/success', 'Site\RegisterController@success')->name('register.supplier.success');


    /*
     * Venue Routes
    */
    Route::resource('/venue', 'Site\VenueController')->except(['create', 'update', 'delete']);
        // For AJAX
        Route::post('/venue/list/related', 'Site\VenueController@relatedList')->name('venue.list.related');
        Route::post('/venue/list', 'Site\VenueController@list')->name('venue.list');
        Route::post('/venue/hearted', 'Site\VenueController@hearted');


    /*
     * product Routes
     */
        // For AJAX
        Route::any('/decor/list', 'Site\ProductController@list')->name('decor.list');
        Route::post('decor/hearted', 'Site\ProductController@hearted');

    Route::get('/decor', 'Site\ProductController@index')->name('decor.index');
    Route::get('/decor/{id}', 'Site\ProductController@show')->name('decor.show');
    Route::get('/decor/related/{id}', 'Site\ProductController@relatedItem')->name('decor.related.item');



    /*
     * Supplier Routes
    */
    Route::resource('/supplier', 'Site\SupplierController')->except(['create', 'update', 'delete']);
        // For AJAX
        Route::post('/supplier/list', 'Site\SupplierController@list')->name('supplier.list');
        Route::post('/supplier/hearted', 'Site\SupplierController@hearted')->name('supplier.hearted');

    /*
     * Review Routes
    */
    Route::post('/review/list', 'Site\ReviewController@list');
    Route::resource('/review', 'Site\ReviewController');



    /*
     * Inspiration Routes
     */
        // For AJAX
        Route::post('/inspiration/requesttag', 'Site\InspirationController@requestTag')->name('inspiration.requesttag');
        Route::post('/inspiration/hearted', 'Site\InspirationController@hearted')->name('inspiration.hearted');
        Route::post('/inspiration/tagged', 'Site\SupplierController@taggedInspiration')->name('inspiration.tagged');
        Route::post('/inspiration/list', 'Site\InspirationController@list')->name('inspiration.list');
    Route::get('/inspiration/filters-categories', 'Site\InspirationController@filterCategories')->name('inspiration.categories-list');
    Route::resource('/inspiration', 'Site\InspirationController')->except(['create', 'update', 'delete']);



});


Route::get('attachments/{filename}', function ($filename)
{
    $path = storage_path('app/attachments/' . $filename);

    if (!File::exists($path)) {
        abort(404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

// Route::get('images/{filename}', function ($filename)
// {
//     $path = storage_path('app/images/' . $filename);
//     return 1;
//     // return Storage::disk('images')->url($filename);

//     if (!File::exists($path)) {
//         abort(404);
//     }

//     $file = File::get($path);
//     $type = File::mimeType($path);

//     $response = Response::make($file, 200);
//     $response->header("Content-Type", $type);

//     return $response;
// });


Route::get('models/{filename}', function ($filename)
{
    $path = public_path('3d-models/' . $filename);

    if (!File::exists($path)) {
        abort(404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});


Route::get('pitch-pack/{filename}', 'PitchPackController@showPdf')->name('mood-board.show.pitchpack');

