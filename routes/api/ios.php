<?php

use Carbon\Carbon;
use App\Models\User;
use App\Models\Company;
use App\Models\Firebase;
use App\Facades\FirebaseFMC;
use Illuminate\Http\Request;

Route::group(['prefix' => 'ios'], function(){
    Route::post('login', 'API\AuthenticationController@login');

        // Route::post('notif-test', function(Request $request){

        //             // Put your device token here (without spaces):
        //     $deviceToken = 'B7844223A9F01245E9D3D1FFDF910AA09EECB653377A5EBC53CEF6ACF69F00DE';

        //     // Put your private key's passphrase here:
        //     $passphrase = 'designa';
        //     $pemfilename = storage_path('development.pem');

        //     // SIMPLE PUSH
        //     $body['aps'] = array(
        //         'alert' => array(
        //             'title' => "You have a notification",
        //             'body' => "Body of the message",
        //         ),
        //         'badge' => 1,
        //         'sound' => 'default',
        //         ); // Create the payload body


        //     ////////////////////////////////////////////////////////////////////////////////

        //     $ctx = stream_context_create();
        //     stream_context_set_option($ctx, 'ssl', 'local_cert', $pemfilename);
        //     stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

        //     $fp = stream_socket_client(
        //         'ssl://gateway.sandbox.push.apple.com:2195', $err,
        //         $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx); // Open a connection to the APNS server
        //     if (!$fp)
        //         exit("Failed to connect: $err $errstr" . PHP_EOL);
        //     echo 'Connected to APNS' . PHP_EOL;
        //     $payload = json_encode($body); // Encode the payload as JSON
        //     $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload; // Build the binary notification
        //     $result = fwrite($fp, $msg, strlen($msg)); // Send it to the server
        //     if (!$result)
        //         echo 'Message not delivered' . PHP_EOL;
        //     else
        //         echo 'Message successfully delivered' . PHP_EOL;
        //     fclose($fp); // Close the connection to the server

        //     // $deviceToken = 'B7844223A9F01245E9D3D1FFDF910AA09EECB653377A5EBC53CEF6ACF69F00DE'; //  iPad 5s Gold prod
        //     // $passphrase = 'designa';
        //     // $message = 'Hello Push Notification';
        //     // $ctx = stream_context_create();
        //     // stream_context_set_option($ctx, 'ssl', 'local_cert', storage_path('apns-dev-cert.pem')); // Pem file to generated // openssl pkcs12 -in pushcert.p12 -out pushcert.pem -nodes -clcerts // .p12 private key generated from Apple Developer Account
        //     // stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
        //     // $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx); // production
        //     // // $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx); // developement
        //     // stream_set_blocking($fp, 0);
        //     // echo "<p>Connection Open</p>";
        //     //     if(!$fp){
        //     //         echo "<p>Failed to connect!<br />Error Number: " . $err . " <br />Code: " . $errstrn . "</p>";
        //     //         return;
        //     //     } else {
        //     //         echo "<p>Sending notification!</p>";
        //     //     }


        //     // $body['aps'] = array('alert' => $message,'sound' => 'default','extra1'=>'10','extra2'=>'value');
        //     // $payload = json_encode($body);
        //     // $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
        //     // //var_dump($msg)
        //     // $result = fwrite($fp, $msg, strlen($msg));
        //     // if (!$result)
        //     //             echo '<p>Message not delivered ' . PHP_EOL . '!</p>';
        //     //         else
        //     //             echo '<p>Message successfully delivered ' . PHP_EOL . '!</p>';
        //     // fclose($fp);


        //         //  return  User::find(236);
        //     //    return  User::find(145)->mobileNotify(array(
        //     //         'title'         => $request->title ?? 'Title',  // title,
        //     //         'avatar'        => $request->avatar ?? 'https://pbs.twimg.com/profile_images/821849411991044096/lQFa_Vly_400x400.jpg',
        //     //         'message'       => $request->message ?? "lorem ipsom message" ,  // message,
        //     //         'notif_type'    => $request->notif_type ?? "inspiration_hearted",
        //     //         'item_id'       => $request->item_id ?? 1,
        //     //         'link'          => $request->link ?? "https://fb.com",
        //     //         'image'         => $request->image ?? 'https://www.unilad.co.uk/wp-content/uploads/2019/09/comedy_wildlife_awards_.jpg',  // image
        //     //         'timestamp'     => Carbon::now()->format('Y-m-d h:i:s') // timestamp
        //     //     ));
        //     // $firebase = new FirebaseFMC;
        //     // return $firebase->sendSingle(
        //     //     [$request->firebase],

        //     // );
        // });

        // register Supplier
        Route::post('/register/supplier', 'API\RegisterController@registerSupplier')->name('register.supplier.store');

        // register Supplier
        Route::post('/register/customer', 'API\RegisterController@registercustomer')->name('register.customer.store');

        // Get Venues
        Route::post('/getvenuesearch', 'API\VenueController@search');
        Route::get('/venuesearch', 'API\VenueController@searchMobile');

        Route::get('roles', function(){
                return App\Models\Role::all();
        })->name('api.roles');


        // For Reset Password
        Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmailForAPI');
        Route::post('password/reset', 'Auth\ResetPasswordController@resetAPI');


        // For Chat APIs
        Route::post('newchat', 'API\ChatController@newChat');
        Route::post('newmessage', 'API\ChatController@newMessage');
        Route::post('searchsupplier', 'API\ChatController@searchSupplier');

        // for Cetegories
        Route::get('venue-categories', 'API\CategoryController@venueCategories');
        Route::get('inspiration-categories', 'API\CategoryController@inspirationCategories');
        Route::get('product-categories', 'API\CategoryController@productCategories');
        Route::get('supplier-categories', 'API\CategoryController@supplierCategories');
        Route::get('category', 'API\CategoryController@category');
        Route::get('roles', 'API\CategoryController@roles');
        Route::post('inspiration/location', 'API\InspirationController@inspirationLocations');
        Route::post('search/supplier', 'API\CompanyController@search');


        Route::get('stocksrange', 'API\CategoryController@stocksRange');

        Route::group(['middleware' => ['json.response','auth:api']], function(){
                //Logging out
                Route::post('logout', 'API\AuthenticationController@logout');

                Route::get('/modules', function(Request $request){
                return [
                        'modules' => $request->user()->allPermissions()->map(function($q){
                        return $q->name;
                        })
                ];
                });

                Route::post('updatetoken', 'API\ProfileController@updatoken');

                //Profile
                Route::get('profile', 'API\ProfileController@profile');
                Route::get('profile/notification', 'API\ProfileController@notifications');
                Route::post('profile/notification/read', 'API\ProfileController@readNotification');
                Route::post('profile/notification/delete', 'API\ProfileController@deleteNotifications');
                Route::post('profile/update', 'API\ProfileController@update');

                //Quote
                Route::match(array('GET', 'POST'), 'quote/list', 'API\QuoteController@quotelist');
                Route::get('quote/show/{id}', 'API\QuoteController@show');
                Route::post('quote/saveddecor', 'API\QuoteController@savedDecor');
                Route::match(array('GET', 'POST'),'quote/alldecor', 'API\QuoteController@allDecor');
                Route::match(array('GET', 'POST'), 'quote/store', 'API\QuoteController@saveQuote');
                Route::post('quote/notify-reminder', 'API\QuoteController@sendSupplierReminder');
                Route::post('quote/sendcomment', 'API\QuoteController@sendComment');

                //
                Route::post('invite', 'API\InviteController@inviteSend');

                //Statistic
                Route::post('statistics/inspiration/list', 'API\InspirationController@inspirationStatisticList');
                Route::post('statistics/inspiration/show', 'API\InspirationController@inspirationStatisticsShow');
                Route::post('statistics/inspiration', 'API\InspirationController@inspirationStatistics');

                Route::post('statistics/profile', 'API\CompanyController@supplierStatistics');

                Route::post('statistics/venue', 'API\VenueController@venueStatistics');
                Route::post('statistics/venue/show', 'API\VenueController@venueStatisticsShow');
                Route::post('statistics/venue/list', 'API\VenueController@venueStatisticList');

                Route::post('statistics/decor', 'API\ProductController@decorStatistics');
                Route::post('statistics/decor/show', 'API\ProductController@decorStatisticsShow');
                Route::post('statistics/decor/list', 'API\ProductController@decorStatisticList');

                //for venues
                Route::post('venue/sendreview', 'API\VenueController@sendReview');
                Route::post('venue/hearted', 'API\VenueController@hearted');
                Route::post('venue/shared', 'API\VenueController@shared');
                Route::match(array('GET', 'POST'), 'venues', 'API\VenueController@list');
                // Route::get('venues/{id}', 'API\VenueController@list');
                Route::get('venues/show/{id}', 'API\VenueController@show');



                //for supplier
                Route::post('supplier/sendreview', 'API\CompanyController@sendReview');
                Route::post('supplier/{id}/inspiration/featured', 'API\CompanyController@featuredInspirations');
                Route::post('supplier/hearted', 'API\CompanyController@hearted');
                Route::post('supplier/shared', 'API\CompanyController@shared');
                // Route::get('supplier/{id}', 'API\CompanyController@list');
                Route::match(array('GET', 'POST'), 'supplier', 'API\CompanyController@list');
                Route::get('supplier/show/{id}', 'API\CompanyController@show');
                Route::post('supplier/statistic/inspiration', 'API\CompanyController@inspirationStatisctic');

                // for supplier qoutes
                Route::post('supplier/quote/save', 'API\QuoteController@supplierQouteSave');
                Route::get('supplier/quote/list/all', 'API\QuoteController@supplierQouteList');
                Route::get('supplier/quote/list/pending', 'API\QuoteController@supplierQouteListPending');
                Route::get('supplier/quote/list/responded', 'API\QuoteController@supplierQouteListResponded');
                Route::get('supplier/quote/show/{id}', 'API\QuoteController@supplierQouteShow');



                // for inspirations
                Route::post('inspiration/tagrequest', 'API\InspirationController@requestTag');
                Route::post('inspiration/hearted', 'API\InspirationController@hearted');
                Route::post('inspiration/shared', 'API\InspirationController@shared');
                Route::post('inspiration/viewed', 'API\InspirationController@viewed');
                Route::match(array('GET', 'POST'),'inspiration', 'API\InspirationController@list');

                // for tagged inspiration
                Route::post('taggedinspirations/products/options', 'API\TaggedInspirationController@productOptions');
                Route::post('taggedinspirations/products/selected', 'API\TaggedInspirationController@productSelected');
                Route::post('taggedinspirations/products/update', 'API\TaggedInspirationController@updateProduct');
                Route::get('taggedinspirations/pending', 'API\TaggedInspirationController@pending');
                Route::get('taggedinspirations/confirmed', 'API\TaggedInspirationController@confirmed');
                Route::get('taggedinspirations/rejected', 'API\TaggedInspirationController@rejected');
                Route::post('taggedinspirations/respond', 'API\TaggedInspirationController@respond');


                // for Products/decors
                Route::post('decor/sendreview', 'API\ProductController@sendReview');
                Route::post('decor/hearted', 'API\ProductController@hearted');
                Route::match(array('GET', 'POST'), 'decor', 'API\ProductController@list');
                // Route::get('decor/{id}', 'API\ProductController@list');
                Route::get('decor/show/{id}', 'API\ProductController@show');

                // Manager
                Route::post('/product/manager/gallery/list', 'API\Manager\ProductController@gallery');
                Route::post('/product/manager/upload/gallery', 'API\Manager\ProductController@uploadGallery');
                Route::delete('/product/manager/gallery/{id}', 'API\Manager\ProductController@deleteGallery');
                Route::post('/product/manager/{id}/update', 'API\Manager\ProductController@update');
                Route::resource('product/manager', 'API\Manager\ProductController')->except(['update']);

                //for hearted items
                Route::get('hearted/inspirations', 'API\HeartedController@inspirations');
                Route::get('hearted/venues', 'API\HeartedController@venues');
                Route::get('hearted/decors', 'API\HeartedController@decors');
                Route::get('hearted/suppliers', 'API\HeartedController@suppliers');


                // Inspiration Manager APIS
                Route::post('/inspiration/manager/change-status', 'API\Manager\InspirationManagerController@changeStatus');
                Route::post('/inspiration/manager/create/type-options', 'API\Manager\InspirationManagerController@options');
                Route::get('/inspiration/manager/gallery', 'API\Manager\InspirationManagerController@galleries');
                Route::post('/inspiration/manager/upload/gallery', 'API\Manager\InspirationManagerController@uploadGallery');
                Route::post('/inspiration/manager/upload/video', 'API\Manager\InspirationManagerController@uploadVideo');
                Route::delete('/inspiration/manager/gallery/{id}', 'API\Manager\InspirationManagerController@deleteGallery');
                Route::delete('/inspiration/manager/video', 'API\Manager\InspirationManagerController@deleteVideo');

                // Taging
                Route::post('/inspiration/manager/tag/roles', 'API\InspirationTagController@roles');
                Route::resource('/inspiration/manager/tag', 'API\InspirationTagController');

                Route::post('/inspiration/manager/{id}/update', 'API\Manager\InspirationManagerController@update');
                Route::resource('/inspiration/manager', 'API\Manager\InspirationManagerController')->except(['update']);


                Route::delete('/inspiration/manager/delete/gallery/{id}', 'API\Manager\InspirationManagerController@deleteGallery');

                /*
                listing GET api/inspiration/manager
                Storing POST api/inspiration/manager
                Showing GET api/inspiration/manager/{id}
                Update PUT/PATCH api/inspiration/manager/{id}
                Update DELETE api/inspiration/manager/{id}
                */
        });
});





