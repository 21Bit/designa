<?php
Route::group(['prefix' => 'google', 'namespace' => 'API'], function(){
    Route::get('geocode/json', 'GoogleMapController@geocodeJson');
    Route::get('place/autocomplete/json', 'GoogleMapController@placeAutoComplete');
    Route::get('place/details/json', 'GoogleMapController@detailsJson');
});