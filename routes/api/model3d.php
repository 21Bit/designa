<?php

Route::group(['prefix' => '3d', 'namespace' => 'API'], function(){
    // Route::get('eventspace/asset_bundle/{id}', 'FunctionSpaceController@baseLayer');
    Route::get('product/model/{id}', 'ProductController@baseLayer');
    
    Route::group(['middleware' => 'auth:api'], function(){
        Route::get('eventspace/list', 'Manager\Model3d\FunctionSpaceAssetBundleController@index');
        Route::resource('eventspace/asset_bundle', 'Manager\Model3d\FunctionSpaceAssetBundleController');
        Route::resource('baselayer', 'Manager\Model3d\BaseLayerController');
        Route::resource('floorplan', 'Manager\Model3d\FloorPlanController');
        Route::resource('model', 'ModelFileController');
        // Route::post('eventspace/asset_bundle', 'Manager\FunctionSpaceModelController@store');
        Route::resource('decoration-template', 'Manager\Model3d\DecorationController');
        Route::get('product-list', 'Manager\Model3d\ProductModelController@productList');
        Route::resource('product-template', 'Manager\Model3d\ProductModelController');
        Route::get('product-favorite-template', 'Manager\Model3d\ProductModelController@favorite');
    });
});