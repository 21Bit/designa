<?php
/* 
* API SEPARATION

* The purpose of separation is to manage each platform
* and for future additionals it would be easy to add up

* IOS ENDPOINTS
*/
include "api/ios.php";


/* 
* ANDROID ENDPOINTS
*/
include "api/android.php";


/* 
* GOOGLE ENDPOINTS
*/
include "api/google.php";



// 3d  model endpoints
include "api/model3d.php";