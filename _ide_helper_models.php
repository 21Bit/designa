<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Access
 *
 * @property int $id
 * @property string $name
 * @property string|null $display_name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Access newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Access newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Access query()
 * @method static \Illuminate\Database\Eloquent\Builder|Access whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Access whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Access whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Access whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Access whereUpdatedAt($value)
 */
	class Access extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\AccessSubscriptionPlan
 *
 * @method static \Illuminate\Database\Eloquent\Builder|AccessSubscriptionPlan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AccessSubscriptionPlan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AccessSubscriptionPlan query()
 */
	class AccessSubscriptionPlan extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ActivityLog
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $details
 * @property string $type
 * @property string $origin
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|ActivityLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ActivityLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ActivityLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|ActivityLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ActivityLog whereDetails($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ActivityLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ActivityLog whereOrigin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ActivityLog whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ActivityLog whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ActivityLog whereUserId($value)
 */
	class ActivityLog extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Attachment
 *
 * @property int $id
 * @property string|null $filename
 * @property string $path
 * @property string $type
 * @property string $attachable_type
 * @property int $attachable_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Attachment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Attachment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Attachment query()
 * @method static \Illuminate\Database\Eloquent\Builder|Attachment whereAttachableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Attachment whereAttachableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Attachment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Attachment whereFilename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Attachment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Attachment wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Attachment whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Attachment whereUpdatedAt($value)
 */
	class Attachment extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Blog
 *
 * @property int $id
 * @property string $category
 * @property string $title
 * @property string $description
 * @property int $is_published
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $images
 * @property-read int|null $images_count
 * @method static \Illuminate\Database\Eloquent\Builder|Blog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Blog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Blog query()
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereIsPublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Blog whereUpdatedAt($value)
 */
	class Blog extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Category
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property string|null $credit_by
 * @property string|null $credit_location
 * @property string|null $type
 * @property string|null $module
 * @property int|null $parent_id
 * @property string $slug
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|Category[] $categories
 * @property-read int|null $categories_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Company[] $companies
 * @property-read int|null $companies_count
 * @property-read Category|null $parent
 * @property-read \Illuminate\Database\Eloquent\Collection|Category[] $subCategories
 * @property-read int|null $sub_categories_count
 * @method static \Illuminate\Database\Eloquent\Builder|Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Category newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Category query()
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereCreditBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereCreditLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereModule($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereUpdatedAt($value)
 */
	class Category extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Color
 *
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $colorable
 * @method static \Illuminate\Database\Eloquent\Builder|Color newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Color newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Color query()
 */
	class Color extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Company
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property string|null $services
 * @property string|null $recognition
 * @property string $slug
 * @property string $town
 * @property string $state
 * @property string $country
 * @property int $will_travel
 * @property float|null $latitude
 * @property float|null $longitude
 * @property string $post_code
 * @property string|null $email
 * @property string|null $website
 * @property string $telephone_number
 * @property string|null $facebook_link
 * @property string|null $twitter_link
 * @property string|null $instagram_link
 * @property string|null $youtube_link
 * @property int $is_active
 * @property int $is_lock
 * @property int $single_venue
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Category[] $categories
 * @property-read int|null $categories_count
 * @property-read mixed $loginlink
 * @property-read mixed $logo
 * @property-read mixed $overall_rating
 * @property-read mixed $thumbnail
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Heart[] $hearts
 * @property-read int|null $hearts_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $images
 * @property-read int|null $images_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Inspiration[] $inspirations
 * @property-read int|null $inspirations_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Location[] $locations
 * @property-read int|null $locations_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Product[] $products
 * @property-read int|null $products_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Review[] $reviews
 * @property-read int|null $reviews_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Share[] $shares
 * @property-read int|null $shares_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read int|null $users_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Venue[] $venues
 * @property-read int|null $venues_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\View[] $views
 * @property-read int|null $views_count
 * @method static \Illuminate\Database\Eloquent\Builder|Company newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Company newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Company query()
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereFacebookLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereInstagramLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereIsLock($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereLatitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereLongitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company wherePostCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereRecognition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereServices($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereSingleVenue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereTelephoneNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereTown($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereTwitterLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereWebsite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereWillTravel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Company whereYoutubeLink($value)
 */
	class Company extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Firebase
 *
 * @property int $id
 * @property int $user_id
 * @property string $device
 * @property string|null $firebase_id
 * @property string|null $ios_device_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Firebase newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Firebase newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Firebase query()
 * @method static \Illuminate\Database\Eloquent\Builder|Firebase whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Firebase whereDevice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Firebase whereFirebaseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Firebase whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Firebase whereIosDeviceToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Firebase whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Firebase whereUserId($value)
 */
	class Firebase extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\FunctionSpace
 *
 * @property int $id
 * @property int $venue_id
 * @property string $name
 * @property string|null $description
 * @property int $max_capacity_seated
 * @property int $max_capacity_standing
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $asset_bundle_android
 * @property-read mixed $asset_bundle
 * @property-read mixed $asset_bundle_ios
 * @property-read mixed $asset_bundle_web
 * @property-read mixed $base_layer
 * @property-read mixed $base_layer_file
 * @property-read mixed $floor_plan
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $images
 * @property-read int|null $images_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ModelFile[] $modelFiles
 * @property-read int|null $model_files_count
 * @property-read \App\Models\Venue $venue
 * @method static \Illuminate\Database\Eloquent\Builder|FunctionSpace newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FunctionSpace newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FunctionSpace ownedBaseLayer()
 * @method static \Illuminate\Database\Eloquent\Builder|FunctionSpace query()
 * @method static \Illuminate\Database\Eloquent\Builder|FunctionSpace whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FunctionSpace whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FunctionSpace whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FunctionSpace whereMaxCapacitySeated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FunctionSpace whereMaxCapacityStanding($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FunctionSpace whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FunctionSpace whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FunctionSpace whereVenueId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FunctionSpace withAssetBundle()
 */
	class FunctionSpace extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Heart
 *
 * @property int $id
 * @property int $user_id
 * @property string $origin
 * @property string $heartable_type
 * @property int $heartable_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $heartable
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Heart newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Heart newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Heart query()
 * @method static \Illuminate\Database\Eloquent\Builder|Heart whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Heart whereHeartableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Heart whereHeartableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Heart whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Heart whereOrigin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Heart whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Heart whereUserId($value)
 */
	class Heart extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Image
 *
 * @property int $id
 * @property string $path
 * @property string $type
 * @property string $imagable_type
 * @property int $imagable_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Heart[] $hearts
 * @property-read int|null $hearts_count
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $imagable
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Share[] $shares
 * @property-read int|null $shares_count
 * @method static \Illuminate\Database\Eloquent\Builder|Image hasHeartCounts()
 * @method static \Illuminate\Database\Eloquent\Builder|Image newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Image newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Image query()
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereImagableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereImagableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereUpdatedAt($value)
 */
	class Image extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Inspiration
 *
 * @property int $id
 * @property int $company_id
 * @property string $name
 * @property string $description
 * @property string $project_year
 * @property int|null $venue_id
 * @property string|null $location
 * @property float|null $latitude
 * @property float|null $longitude
 * @property string $slug
 * @property string|null $images_by
 * @property int $is_lock
 * @property int $is_published
 * @property int $featured
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Category[] $categories
 * @property-read int|null $categories_count
 * @property-read \App\Models\Company $company
 * @property-read mixed $company_logo
 * @property-read mixed $company_name
 * @property-read mixed $cover
 * @property-read mixed $description_for_mobile
 * @property-read mixed $hearts_counts
 * @property-read mixed $location_name
 * @property-read mixed $overall_rating
 * @property-read mixed $shares_counts
 * @property-read mixed $suppliers
 * @property-read mixed $thumbnail
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Heart[] $hearts
 * @property-read int|null $hearts_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $images
 * @property-read int|null $images_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Product[] $products
 * @property-read int|null $products_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Review[] $reviews
 * @property-read int|null $reviews_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Share[] $shares
 * @property-read int|null $shares_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SupplierInspiration[] $supplierRoles
 * @property-read int|null $supplier_roles_count
 * @property-read \App\Models\Venue|null $venue
 * @property-read \App\Models\Company $venueSupplier
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Venue[] $venues
 * @property-read int|null $venues_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Video[] $videos
 * @property-read int|null $videos_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\View[] $views
 * @property-read int|null $views_count
 * @method static \Illuminate\Database\Eloquent\Builder|Inspiration closeTo($latitude, $longitude, $distance)
 * @method static \Illuminate\Database\Eloquent\Builder|Inspiration hasHeartCounts()
 * @method static \Illuminate\Database\Eloquent\Builder|Inspiration newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Inspiration newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Inspiration query()
 * @method static \Illuminate\Database\Eloquent\Builder|Inspiration whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inspiration whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inspiration whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inspiration whereFeatured($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inspiration whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inspiration whereImagesBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inspiration whereIsLock($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inspiration whereIsPublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inspiration whereLatitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inspiration whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inspiration whereLongitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inspiration whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inspiration whereProjectYear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inspiration whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inspiration whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inspiration whereVenueId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Inspiration withActiveCompany()
 */
	class Inspiration extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Link
 *
 * @property int $id
 * @property string $url
 * @property string $type
 * @property string|null $source
 * @property string $linkable_type
 * @property int $linkable_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Link newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Link newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Link query()
 * @method static \Illuminate\Database\Eloquent\Builder|Link whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Link whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Link whereLinkableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Link whereLinkableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Link whereSource($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Link whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Link whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Link whereUrl($value)
 */
	class Link extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Location
 *
 * @property int $id
 * @property float $longitude
 * @property float $latitude
 * @property string $address
 * @property int|null $radius
 * @property string $locationable_type
 * @property int $locationable_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $locationable
 * @method static \Illuminate\Database\Eloquent\Builder|Location newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Location newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Location query()
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereLatitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereLocationableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereLocationableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereLongitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereRadius($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereUpdatedAt($value)
 */
	class Location extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Message
 *
 * @property int $id
 * @property int $company_id
 * @property int $user_id
 * @property string $message
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Message newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Message newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Message query()
 * @method static \Illuminate\Database\Eloquent\Builder|Message whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Message whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Message whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Message whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Message whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Message whereUserId($value)
 */
	class Message extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ModelFile
 *
 * @property int $id
 * @property string|null $model_name
 * @property string $path
 * @property int $is_public
 * @property int|null $number_of_guest
 * @property string $type
 * @property int|null $base_layer_id
 * @property int|null $decoration_template_id
 * @property string $modellable_type
 * @property int $modellable_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read ModelFile|null $baseLayer
 * @property-read mixed $base_asset_bundle_android
 * @property-read mixed $base_asset_bundle_ios
 * @property-read mixed $base_asset_bundle_web
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $modellable
 * @method static \Illuminate\Database\Eloquent\Builder|ModelFile newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ModelFile newQuery()
 * @method static \Illuminate\Database\Query\Builder|ModelFile onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|ModelFile query()
 * @method static \Illuminate\Database\Eloquent\Builder|ModelFile whereBaseLayerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModelFile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModelFile whereDecorationTemplateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModelFile whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModelFile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModelFile whereIsPublic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModelFile whereModelName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModelFile whereModellableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModelFile whereModellableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModelFile whereNumberOfGuest($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModelFile wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModelFile whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ModelFile whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|ModelFile withTrashed()
 * @method static \Illuminate\Database\Query\Builder|ModelFile withoutTrashed()
 */
	class ModelFile extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Permission
 *
 * @property int $id
 * @property string $name
 * @property string|null $display_name
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @method static \Illuminate\Database\Eloquent\Builder|Permission newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Permission newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Permission query()
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Permission whereUpdatedAt($value)
 */
	class Permission extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PricingComponent
 *
 * @property int $id
 * @property string $name
 * @property int $multipliable
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|PricingComponent newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PricingComponent newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PricingComponent query()
 * @method static \Illuminate\Database\Eloquent\Builder|PricingComponent whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PricingComponent whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PricingComponent whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PricingComponent whereMultipliable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PricingComponent whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PricingComponent whereUpdatedAt($value)
 */
	class PricingComponent extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Product
 *
 * @property int $id
 * @property int $company_id
 * @property string $name
 * @property string|null $description
 * @property int|null $quantity
 * @property string|null $product_identifier
 * @property string|null $product_size
 * @property string $slug
 * @property int $is_published
 * @property int $is_lock
 * @property int $featured
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Category[] $categories
 * @property-read int|null $categories_count
 * @property-read \App\Models\Company $company
 * @property-read mixed $asset_bundle_android
 * @property-read mixed $asset_bundle
 * @property-read mixed $asset_bundle_ios
 * @property-read mixed $asset_bundle_web
 * @property-read mixed $base_layer
 * @property-read mixed $base_layer_file
 * @property-read mixed $floor_plan
 * @property-read mixed $overall_rating
 * @property-read mixed $product_type
 * @property-read mixed $style
 * @property-read mixed $thumbnail
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Heart[] $hearts
 * @property-read int|null $hearts_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $images
 * @property-read int|null $images_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ModelFile[] $modelFiles
 * @property-read int|null $model_files_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Review[] $reviews
 * @property-read int|null $reviews_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Share[] $shares
 * @property-read int|null $shares_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\View[] $views
 * @property-read int|null $views_count
 * @method static \Illuminate\Database\Eloquent\Builder|Product hasHeartCounts()
 * @method static \Illuminate\Database\Eloquent\Builder|Product newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Product newQuery()
 * @method static \Illuminate\Database\Query\Builder|Product onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Product ownedBaseLayer()
 * @method static \Illuminate\Database\Eloquent\Builder|Product query()
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereFeatured($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereIsLock($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereIsPublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereProductIdentifier($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereProductSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product withAssetBundle()
 * @method static \Illuminate\Database\Eloquent\Builder|Product withColors()
 * @method static \Illuminate\Database\Query\Builder|Product withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Product withoutTrashed()
 */
	class Product extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Project
 *
 * @property int $id
 * @property int $user_id
 * @property string $title
 * @property string $description
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $slug
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Category[] $categories
 * @property-read int|null $categories_count
 * @property-read mixed $cater_name
 * @property-read mixed $colors
 * @property-read mixed $created_by
 * @property-read mixed $pdf
 * @property-read mixed $setting_name
 * @property-read mixed $style_name
 * @property-read mixed $thumbnail
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $images
 * @property-read int|null $images_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $inspirationImages
 * @property-read int|null $inspiration_images_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Product[] $products
 * @property-read int|null $products_count
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Project newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Project newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Project query()
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Project whereUserId($value)
 */
	class Project extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Qoute
 *
 * @property int $id
 * @property string $reference_number
 * @property int|null $user_id
 * @property string|null $event_name
 * @property string $event_type
 * @property string $event_date
 * @property string $location
 * @property string $time_start
 * @property string $time_end
 * @property string $number_of_guests
 * @property string|null $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Category $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\QouteProduct[] $decors
 * @property-read int|null $decors_count
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Qoute hasNoPrice($supplier = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|Qoute hasPrice($supplier = 0)
 * @method static \Illuminate\Database\Eloquent\Builder|Qoute newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Qoute newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Qoute query()
 * @method static \Illuminate\Database\Eloquent\Builder|Qoute whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Qoute whereEventDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Qoute whereEventName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Qoute whereEventType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Qoute whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Qoute whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Qoute whereNumberOfGuests($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Qoute whereReferenceNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Qoute whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Qoute whereTimeEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Qoute whereTimeStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Qoute whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Qoute whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Qoute withPricingStatus($supplierId)
 */
	class Qoute extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\QouteProduct
 *
 * @property int $id
 * @property int|null $qoute_id
 * @property int|null $product_id
 * @property string|null $quantity
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Attachment[] $attachments
 * @property-read int|null $attachments_count
 * @property-read mixed $has_no_price
 * @property-read mixed $has_price
 * @property-read mixed $overall_rating
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\QouteProductPrice[] $prices
 * @property-read int|null $prices_count
 * @property-read \App\Models\Product|null $product
 * @property-read \App\Models\Qoute|null $qoute
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Review[] $reviews
 * @property-read int|null $reviews_count
 * @method static \Illuminate\Database\Eloquent\Builder|QouteProduct hasNoPrice($supplier)
 * @method static \Illuminate\Database\Eloquent\Builder|QouteProduct hasPrice($supplier)
 * @method static \Illuminate\Database\Eloquent\Builder|QouteProduct newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|QouteProduct newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|QouteProduct query()
 * @method static \Illuminate\Database\Eloquent\Builder|QouteProduct whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|QouteProduct whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|QouteProduct whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|QouteProduct whereQouteId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|QouteProduct whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|QouteProduct whereUpdatedAt($value)
 */
	class QouteProduct extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\QouteProductPrice
 *
 * @property int $id
 * @property int $qoute_product_id
 * @property int $pricing_component_id
 * @property float $price
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\PricingComponent $pricingComponent
 * @method static \Illuminate\Database\Eloquent\Builder|QouteProductPrice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|QouteProductPrice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|QouteProductPrice query()
 * @method static \Illuminate\Database\Eloquent\Builder|QouteProductPrice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|QouteProductPrice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|QouteProductPrice wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|QouteProductPrice wherePricingComponentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|QouteProductPrice whereQouteProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|QouteProductPrice whereUpdatedAt($value)
 */
	class QouteProductPrice extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Reply
 *
 * @property int $id
 * @property int $message_id
 * @property int $user_id
 * @property string $message
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Reply newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Reply newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Reply query()
 * @method static \Illuminate\Database\Eloquent\Builder|Reply whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reply whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reply whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reply whereMessageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reply whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Reply whereUserId($value)
 */
	class Reply extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Review
 *
 * @property int $id
 * @property int $user_id
 * @property string $comment
 * @property int|null $rating
 * @property string $origin
 * @property string $reviewable_type
 * @property int $reviewable_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $reviewable
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Review newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Review newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Review query()
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereOrigin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereReviewableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereReviewableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereUserId($value)
 */
	class Review extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Role
 *
 * @property int $id
 * @property string $name
 * @property string|null $display_name
 * @property string|null $description
 * @property string|null $credit_by
 * @property string|null $credit_location
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Company[] $companies
 * @property-read int|null $companies_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @method static \Illuminate\Database\Eloquent\Builder|Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Role newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Role query()
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereCreditBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereCreditLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Role whereUpdatedAt($value)
 */
	class Role extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Share
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $data
 * @property string $origin
 * @property string $sharable_type
 * @property int $sharable_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Share newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Share newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Share query()
 * @method static \Illuminate\Database\Eloquent\Builder|Share whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Share whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Share whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Share whereOrigin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Share whereSharableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Share whereSharableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Share whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Share whereUserId($value)
 */
	class Share extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\SubscriptionPlan
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $stripe_plan
 * @property float $price
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Access[] $accesses
 * @property-read int|null $accesses_count
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan query()
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan whereStripePlan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SubscriptionPlan whereUpdatedAt($value)
 */
	class SubscriptionPlan extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\SupplierInspiration
 *
 * @property int $id
 * @property int $inspiration_id
 * @property int $role_id
 * @property int|null $company_id
 * @property int|null $request_by
 * @property string|null $name
 * @property string|null $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $email
 * @property-read \App\Models\Company|null $company
 * @property-read \App\Models\Inspiration $inspiration
 * @property-read \App\Models\Company|null $inviter
 * @property-read \App\Models\Role $role
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierInspiration newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierInspiration newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierInspiration query()
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierInspiration whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierInspiration whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierInspiration whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierInspiration whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierInspiration whereInspirationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierInspiration whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierInspiration whereRequestBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierInspiration whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierInspiration whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SupplierInspiration whereUpdatedAt($value)
 */
	class SupplierInspiration extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property int|null $company_id
 * @property string $name
 * @property string|null $profile_picture
 * @property string $email
 * @property string $type
 * @property int $is_superadmin
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $stripe_id
 * @property string|null $card_brand
 * @property string|null $card_last_four
 * @property string|null $trial_ends_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ActivityLog[] $activities
 * @property-read int|null $activities_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read int|null $clients_count
 * @property-read \App\Models\Company|null $company
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Firebase[] $firebases
 * @property-read int|null $firebases_count
 * @property-read mixed $asset_bundle_android
 * @property-read mixed $asset_bundle
 * @property-read mixed $asset_bundle_ios
 * @property-read mixed $asset_bundle_web
 * @property-read mixed $base_layer
 * @property-read mixed $base_layer_file
 * @property-read mixed $company_roles
 * @property-read mixed $current_plan
 * @property-read mixed $default_payment_method
 * @property-read mixed $distance
 * @property-read mixed $floor_plan
 * @property-read mixed $user_payment_methods
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Heart[] $hearted
 * @property-read int|null $hearted_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $images
 * @property-read int|null $images_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ModelFile[] $modelFiles
 * @property-read int|null $model_files_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Cashier\Subscription[] $subscriptions
 * @property-read int|null $subscriptions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @property-read int|null $tokens_count
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User orWherePermissionIs($permission = '')
 * @method static \Illuminate\Database\Eloquent\Builder|User orWhereRoleIs($role = '', $team = null)
 * @method static \Illuminate\Database\Eloquent\Builder|User ownedBaseLayer()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCardBrand($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCardLastFour($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDoesntHavePermission()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereDoesntHaveRole()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereIsSuperadmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePermissionIs($permission = '', $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|User whereProfilePicture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRoleIs($role = '', $team = null, $boolean = 'and')
 * @method static \Illuminate\Database\Eloquent\Builder|User whereStripeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTrialEndsAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User withAssetBundle()
 */
	class User extends \Eloquent implements \Illuminate\Contracts\Auth\MustVerifyEmail {}
}

namespace App\Models{
/**
 * App\Models\Venue
 *
 * @property int $id
 * @property int $company_id
 * @property string $name
 * @property string|null $description
 * @property string $slug
 * @property string|null $operating_hours
 * @property string $location
 * @property string|null $email
 * @property string|null $contact_number
 * @property string|null $longitude
 * @property string|null $latitude
 * @property int|null $price
 * @property int $capacity
 * @property int|null $max_standing
 * @property int|null $max_seated
 * @property int $is_lock
 * @property int $is_published
 * @property int $featured
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Attachment[] $attachments
 * @property-read int|null $attachments_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Category[] $categories
 * @property-read int|null $categories_count
 * @property-read \App\Models\Company $company
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\FunctionSpace[] $functionSpaces
 * @property-read int|null $function_spaces_count
 * @property-read mixed $asset_bundle_android
 * @property-read mixed $asset_bundle
 * @property-read mixed $asset_bundle_ios
 * @property-read mixed $asset_bundle_web
 * @property-read mixed $base_layer
 * @property-read mixed $base_layer_file
 * @property-read mixed $company_logo
 * @property-read mixed $floor_plan
 * @property-read mixed $overall_rating
 * @property-read mixed $thumbnail
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Heart[] $hearts
 * @property-read int|null $hearts_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $images
 * @property-read int|null $images_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ModelFile[] $modelFiles
 * @property-read int|null $model_files_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Review[] $reviews
 * @property-read int|null $reviews_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Share[] $shares
 * @property-read int|null $shares_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\View[] $views
 * @property-read int|null $views_count
 * @method static \Illuminate\Database\Eloquent\Builder|Venue hasHeartCounts()
 * @method static \Illuminate\Database\Eloquent\Builder|Venue newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Venue newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Venue ownedBaseLayer()
 * @method static \Illuminate\Database\Eloquent\Builder|Venue query()
 * @method static \Illuminate\Database\Eloquent\Builder|Venue whereCapacity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Venue whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Venue whereContactNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Venue whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Venue whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Venue whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Venue whereFeatured($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Venue whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Venue whereIsLock($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Venue whereIsPublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Venue whereLatitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Venue whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Venue whereLongitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Venue whereMaxSeated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Venue whereMaxStanding($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Venue whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Venue whereOperatingHours($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Venue wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Venue whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Venue whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Venue withAssetBundle()
 */
	class Venue extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Video
 *
 * @property int $id
 * @property string|null $title
 * @property string $path
 * @property string $type
 * @property string $videoable_type
 * @property int $videoable_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $videoable
 * @method static \Illuminate\Database\Eloquent\Builder|Video newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Video newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Video query()
 * @method static \Illuminate\Database\Eloquent\Builder|Video whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Video whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Video wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Video whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Video whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Video whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Video whereVideoableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Video whereVideoableType($value)
 */
	class Video extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\View
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $data
 * @property string|null $origin
 * @property string $viewable_type
 * @property int $viewable_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User|null $user
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $viewable
 * @method static \Illuminate\Database\Eloquent\Builder|View newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|View newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|View query()
 * @method static \Illuminate\Database\Eloquent\Builder|View whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|View whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|View whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|View whereOrigin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|View whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|View whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|View whereViewableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|View whereViewableType($value)
 */
	class View extends \Eloquent {}
}

