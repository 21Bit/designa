<?php

namespace App\Notifications\Supplier\NewItem;

use Carbon\Carbon;
use App\Models\Company;
use App\Models\Inspiration;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;

class NewInspirationNotification extends Notification implements ShouldQueue
{
    use Queueable;
    public $inspiration;
    public $company;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Inspiration $inspiration)
    {
        $this->inspiration = $inspiration;
        $this->company = Company::find($inspiration->company_id);
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
            'title'         => 'New Inspiration',  // title,
            'avatar'        => $this->company->image('logo')->path('logos', '/images/placeholders/placeholder.png'),
            'message'       => optional($this->company)->name . ' has created new inspiration entitled <b>' . $this->inspiration->name . "</b>",  // message,
            'notif_type'    => 'new_inspiration',
            'item_id'       => $this->inspiration->id,
            'link'          => route('site.inspiration.show', $this->inspiration->slug) .'?tab=hearts',
            'image'         => $this->inspiration->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),  // image
            'read_at'       => '',
            'timestamp'     => Carbon::now()->toDateTimeString() // timestamp
        ];
    }


    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'title'         => 'New Inspiration',  // title,
            'avatar'        => $this->company->image('logo')->path('logos', '/images/placeholders/placeholder.png'),
            'message'       => optional($this->company)->name . ' has created new inspiration entitled ' . $this->inspiration->name,  // message,
            'notif_type'    => 'new_inspiration',
            'item_id'       => $this->inspiration->id,
            'link'          => route('site.inspiration.show', $this->inspiration->slug) .'?tab=hearts',
            'image'         => $this->inspiration->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),  // image
            'read_at'       => '',
            'timestamp'     => Carbon::now()->toDateTimeString() // timestamp
        ]);
    }
}
