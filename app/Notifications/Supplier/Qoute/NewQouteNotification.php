<?php

namespace App\Notifications\Supplier\Qoute;

use Carbon\Carbon;
use App\Models\Qoute;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;

class NewQouteNotification extends Notification implements ShouldQueue
{
    use Queueable;
    public $qoute;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Qoute $qoute)
    {
        $this->qoute = $qoute;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'. 'email'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

     public function toDatabase($notifiable)
    {
        return [
            'title'         => 'New Qoute',  // title,
            'avatar'        => $this->qoute->user->getProfilePicture(),
            'message'       => $this->qoute->user->name . ' had new qoute entitled <b>' . $this->qoute->event_name . '</b>',  // message,
            'notif_type'    => 'new_qoute',
            'item_id'       => $this->qoute->id,
            'link'          => route('dashboard.quote.show', $this->qoute->reference_number),
            'image'         => '',  // image
            'read_at'       => '',
            'timestamp'     => Carbon::now()->format("Y-m-d H:i:s") // timestamp
        ];
    }


    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'title'         => 'New Qoute',  // title,
            'avatar'        => $this->qoute->user->getProfilePicture(),
            'message'       => $this->qoute->user->name . ' had new qoute entitled ' . $this->qoute->event_name,  // message,
            'notif_type'    => 'new_qoute',
            'item_id'       => $this->qoute->id,
            'link'          => route('dashboard.quote.show', $this->qoute->reference_number),
            'image'         => '',  // image
            'read_at'       => '',
            'timestamp'     => Carbon::now()->format("Y-m-d H:i:s") // timestamp
        ]);
    }
}
