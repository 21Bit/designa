<?php

namespace App\Notifications\Supplier\Review;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Venue;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;

class ReviewedVenueNotification extends Notification implements ShouldQueue
{
    use Queueable;
    public $venue;
    public $user;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Venue $venue, User $user)
    {
        $this->venue = $venue;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
            'title'         => 'Reviewed Venue',  // title,
            'avatar'        => $this->user->getProfilePicture(),
            'message'       => $this->user->name . ' has reviewed your venue entitled ' . $this->venue->name,  // message,
            'notif_type'    => 'reviewed_venue',
            'item_id'       => $this->venue->id,
            'link'          => route('dashboard.venue.show', $this->venue->slug) .'?tab=hearts',
            'image'         => $this->venue->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),  // image
            'read_at'       => '',
            'timestamp'     => Carbon::now()->toDateTimeString() // timestamp
        ];
    }


    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'title'         => 'Reviewed Venue',  // title,
            'avatar'        => $this->user->getProfilePicture(),
            'message'       => $this->user->name . ' has reviewed your venue entitled ' . $this->venue->name,  // message,
            'notif_type'    => 'reviewed_venue',
            'item_id'       => $this->venue->id,
            'link'          => route('dashboard.venue.show', $this->venue->slug) .'?tab=hearts',
            'image'         => $this->venue->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),  // image
            'read_at'       => '',
            'timestamp'     => Carbon::now()->toDateTimeString() // timestamp
        ]);
    }
}
