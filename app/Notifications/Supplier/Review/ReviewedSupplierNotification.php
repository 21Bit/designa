<?php

namespace App\Notifications\Supplier\Review;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Company;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;

class ReviewedSupplierNotification extends Notification implements ShouldQueue
{
    use Queueable;
    protected $company;
    protected $user;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Company $company, User $user)
    {
        $this->user = $user;
        $this->company = $company;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
            'title'         => 'Reviewed Supplier',  // title,
            'avatar'        => $this->user->getProfilePicture(),
            'message'       => $this->user->name . ' had reviewed you as supplier',  // message,
            'notif_type'    => 'reviewed_supplier',
            'item_id'       => $this->company->id,
            'link'          => route('dashboard.profile.index'),
            'image'         => $this->company->image('logo')->path('logos', '/images/placeholders/placeholder.png'),  // image
            'read_at'       => '',
            'timestamp'     => Carbon::now()->toDateTimeString() // timestamp
        ];
    }


    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'title'         => 'Reviewed Supplier',  // title,
            'avatar'        => $this->user->getProfilePicture(),
            'message'       => $this->user->name . ' had reviewed you as supplier',  // message,
            'notif_type'    => 'reviewed_supplier',
            'item_id'       => $this->company->id,
            'link'          => route('dashboard.profile.index'),
            'image'         => $this->company->image('logo')->path('logos', '/images/placeholders/placeholder.png'),  // image
            'read_at'       => '',
            'timestamp'     => Carbon::now()->toDateTimeString() // timestamp
        ]);
    }
}
