<?php

namespace App\Notifications\Supplier\EventBoard;

use App\Models\Project;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PitchPackCreatedNotification extends Notification implements  ShouldQueue
{
    use Queueable;
    public Project $eventBoard;
    public User $user;
    private $file;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Project $eventBoard, $file)
    {
        $this->eventBoard = $eventBoard;
        $this->user = $eventBoard->user;
        $this->file = $file;
    }


    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)->subject('Pitchpack is created!')
                ->from('no-reply@designa.studio')
                ->line('Congratulations! Your pitchpack is ready.')
                ->action('View Pitchpack', url('/pdf/' . $this->eventBoard->slug . '.pdf'))
                ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
            'title'         => 'Your pitchpack is now ready!',  // title,
            'avatar'        => '/img/pdf-placeholder.png',
            'message'       => 'Your <b>' . $this->eventBoard->title . '</b> is now ready for viewing',  // message,
            'notif_type'    => 'pitchpack_created',
            'item_id'       => $this->eventBoard->id,
            'link'          => route('dashboard.event-board.show', $this->eventBoard->slug) .'?id=hearts',
            'image'         => '',  // image
            'read_at'       => '',
            'timestamp'     => Carbon::now()->format("Y-m-d H:i:s") // timestamp
        ];
    }


    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'title'         => 'Your pitchpack is now ready!',  // title,
            'avatar'        => '/img/pdf-placeholder.png',
            'message'       => 'Your ' . $this->eventBoard->title . ' is now ready for viewing',  // message,
            'notif_type'    => 'pitchpack_created',
            'item_id'       => $this->eventBoard->id,
            'link'          => route('dashboard.event-board.show', $this->eventBoard->slug) .'?id=hearts',
            'image'         => '',  // image
            'read_at'       => '',
            'timestamp'     => Carbon::now()->format("Y-m-d H:i:s") // timestamp
        ]);
    }
}
