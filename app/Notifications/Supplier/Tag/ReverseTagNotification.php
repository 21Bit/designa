<?php

namespace App\Notifications\Supplier\Tag;

use Carbon\Carbon;
use App\Models\Role;
use App\Models\Company;
use App\Models\Inspiration;
use Illuminate\Bus\Queueable;
use App\Models\SupplierInspiration;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;

class ReverseTagNotification extends Notification implements ShouldQueue
{
    use Queueable;
    public $supplierInspiration;
    public $inspiration;
    public $company;
    public $role;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(SupplierInspiration $supplierInspiration, Inspiration $inspiration, Company $company, Role $role)
    {
        $this->supplierInspiration = $supplierInspiration;
        $this->inspiration = $inspiration;
        $this->company = $company;
        $this->role = $role;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
            'title'         => 'Request Tagged Inspiration',  // title,
            'avatar'        =>$this->company->image('logo')->path('logos', '/images/placeholders/placeholder.png'),
            'message'       => $this->company->name . ' requested for you to tagged them  in your inspiration entitled ' . $this->inspiration->name . " as " . $this->role->display_name,  // message,
            'notif_type'    => 'reverse_tagged_inspiration',
            'item_id'       => $this->inspiration->id,
            'link'          => route('dashboard.inspiration.show', $this->inspiration->slug) .'?tab=supplier',
            'image'         => $this->inspiration->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),  // image
            'read_at'       => '',
            'timestamp'     => Carbon::now()->toDateTimeString() // timestamp
        ];
    }


    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'title'         => 'Request Tagged Inspiration',  // title,
            'avatar'        => $this->company->image('logo')->path('logos', '/images/placeholders/placeholder.png'),
            'message'       => $this->company->name . ' requested for you to tagged them in your inspiration entitled ' . $this->inspiration->name . " as " . $this->role->display_name,  // message,
            'notif_type'    => 'tagged_inspiration',
            'item_id'       => $this->inspiration->id,
            'link'          => route('dashboard.inspiration.show', $this->inspiration->slug) .'?tab=supplier',
            'image'         => $this->inspiration->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),  // image
            'read_at'       => '',
            'timestamp'     => Carbon::now()->toDateTimeString() // timestamp
        ]);
    }
}
