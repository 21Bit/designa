<?php

namespace App\Notifications\Supplier\Hearts;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;

class HeartedProductNotification extends Notification implements ShouldQueue
{
    use Queueable;
    public $product;
    public $user;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Product $product, User $user)
    {
        $this->product = $product;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
            'title'         => 'Hearted Product',  // title,
            'avatar'        => $this->user->getProfilePicture(),
            'message'       => $this->user->name . ' has saved your product entitled <b>' . $this->product->name . '</b>',  // message,
            'notif_type'    => 'hearted_product',
            'item_id'       => $this->product->id,
            'link'          => route('dashboard.product.show', $this->product->slug) .'?tab=hearts',
            'image'         => $this->product->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),  // image
            'read_at'       => '',
            'timestamp'     => Carbon::now()->format("Y-m-d h:i:s") // timestamp
        ];
    }


    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'title'         => 'Hearted Product',  // title,
            'avatar'        => $this->user->getProfilePicture(),
            'message'       => $this->user->name . ' has saved your product entitled <b>' . $this->product->name . '</b>',  // message,
            'notif_type'    => 'hearted_product',
            'item_id'       => $this->product->id,
            'link'          => route('dashboard.product.show', $this->product->slug) .'?tab=hearts',
            'image'         => $this->product->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),  // image
            'read_at'       => '',
            'timestamp'     => Carbon::now()->format("Y-m-d h:i:s") // timestamp
        ]);
    }
}
