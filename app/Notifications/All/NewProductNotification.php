<?php

namespace App\Notifications\All;

use Carbon\Carbon;
use App\Models\Company;
use App\Models\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;

class NewProductNotification extends Notification  implements ShouldQueue
{
    use Queueable;
    protected $product;
    protected $company;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
        $this->company = Company::find($product->company_id);
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
            'title'         => 'New Product',  // title,
            'avatar'        => $this->company->image('logo')->path('logos', '/images/placeholders/placeholder.png'),
            'message'       => $this->company->name . ' has new product entitled <b>' . $this->product->name . '</b>',  // message,
            'notif_type'    => 'new_product',
            'item_id'       => $this->product->id,
            'link'          => route('site.decor.index', $this->product->slug),
            'image'         => $this->product->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),  // image
            'read_at'       => '',
            'timestamp'     => Carbon::now()->format('Y-m-d h:i:s') // timestamp
        ];
    }


    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'title'         => 'New Product',  // title,
            'avatar'        => $this->company->image('logo')->path('logos', '/images/placeholders/placeholder.png'),
            'message'       => $this->company->name . ' has new product entitled ' . $this->product->name,  // message,
            'notif_type'    => 'new_product',
            'item_id'       => $this->product->id,
            'link'          => route('site.decor.index', $this->product->slug),
            'image'         => $this->product->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),  // image
            'read_at'       => '',
            'timestamp'     => Carbon::now()->format('Y-m-d h:i:s') // timestamp
        ]);
    }
}
