<?php

namespace App\Notifications\Customer;

use Carbon\Carbon;
use App\Models\Qoute;
use App\Models\Company;
use App\Channels\FmcChannel;
use App\Facades\FirebaseFMC;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;

class QuoteResponseNotification extends Notification  implements ShouldQueue
{
    use Queueable;
    public $company;
    public $quote;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Qoute $quote, Company $company)
    {
        $this->company = $company;
        $this->quote = $quote;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }


    public function toDatabase($notifiable)
    {
        return [
            'title'         => 'Quote Pricing Update',  // title,
            'avatar'        => $this->company->logo,
            'message'       => $this->company->name . " has updated pricing for your quotation for <b>" . $this->quote->event_name . '</b>',  // message,
            'notif_type'    => 'quote_pricing_update',
            'item_id'       => $this->quote->reference_number,
            'link'          => route('user.qoute.show', $this->quote->reference_number),
            'image'         => $this->company->logo,  // image
            'read_at'       => '',
            'timestamp'     => Carbon::now()->format("Y-m-d h:i:s") // timestamp
        ];
    }


    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'title'         => 'Quote Pricing Update',  // title,
            'avatar'        => $this->company->logo,
            'message'       => $this->company->name . " has updated pricing for your quotation for " . $this->quote->event_name,  // message,
            'notif_type'    => 'quote_pricing_update',
            'item_id'       => $this->quote->reference_number,
            'link'          => route('user.qoute.show', $this->quote->reference_number),
            'image'         => $this->company->logo,  // image
            'read_at'       => '',
            'timestamp'     => Carbon::now()->format("Y-m-d h:i:s") // timestamp
        ]);
    }

    // public function toFmc($notifiable){
    //     \Log::info("hehe");
    //     $firebase = new FirebaseFMC;
    //     $firebase->sendSingle(
    //         ['eajSq0tudHI:APA91bHJLIez5AW5xVHcMVg_AZChzbGYzMXPxiTJCtL7WF_v8k0CdkATwd2Pid6FCXwFncwEvHivucN3qju4LYUbl5H9BBGqgLN4usSnZnwbQLqba1Zwjd99nmROrziZoPJY0MywCU0X'],
    //         array(
    //             'title'         => 'Hearted Inspiration',  // title,
    //             'avatar'        => $notifiable->getProfilePicture(),
    //             'message'       => $notifiable->name . ' has saved your inspiration entitled ',  // message,
    //             'notif_type'    => 'hearted_inspiration',
    //             'item_id'       => 1,
    //             'link'          => "",
    //             'image'         => '', //$inspiration->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),  // image
    //             'timestamp'     => Carbon::now()->toDateTimeString()->format('Y-m-d h:i:s') // timestamp
    //         )
    //     );
    // }
}
