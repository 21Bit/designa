<?php

namespace App\Http\Resources\Model3dMobile;

use App\Models\ModelFile;
use Illuminate\Http\Resources\Json\JsonResource;

class FloorplanTemplateJsonFormat extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                                => $this->id,
            'floorplan_name'                    => $this->model_name,
            'number_of_guest'                   => $this->number_of_guest,
            'base_layer_id'                     => ModelFile::find($this->base_layer_id)->modellable_id,
            'decoration_template_id'            => $this->decoration_template_id,
            'created_at'                        => $this->created_at,
            'is_public'                         => $this->is_public
        ];
    }
}
