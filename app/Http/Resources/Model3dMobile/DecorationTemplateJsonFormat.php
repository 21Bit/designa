<?php

namespace App\Http\Resources\Model3dMobile;

use App\Models\ModelFile;
use Illuminate\Http\Resources\Json\JsonResource;

class DecorationTemplateJsonFormat extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $event_space_id =  ModelFile::find($this->base_layer_id)->modellable->id;
        return [
            'id'                                => $this->id,
            'name'                              => $this->model_name,
            'number_of_guest'                   => $this->number_of_guest,
            'event_space_id'                    => $event_space_id,
            'base_layer_id'                     => $this->base_layer_id,
            'created_at'                        => $this->created_at
        ];
    }
}
