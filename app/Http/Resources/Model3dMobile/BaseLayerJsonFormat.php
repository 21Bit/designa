<?php

namespace App\Http\Resources\Model3dMobile;

use Illuminate\Http\Resources\Json\JsonResource;

class BaseLayerJsonFormat extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                        => $this->id,
            'baselayer_name'            => $this->model_name,
            'event_space_id'            => $this->modellable_id,
            'thumbnail'                 => $this->modellable->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),
            'max_seated'                => $this->modellable->max_capacity_seated ?? 0,
            'max_standing'              => $this->modellable->max_capacity_standing ?? 0,
            'is_public'                 => $this->is_public
        ];
    }
}
