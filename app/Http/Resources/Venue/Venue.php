<?php

namespace App\Http\Resources\Venue;

use App\Models\Inspiration;
// use App\Http\Resources\Inspiration\Inspiration as InspirationResource;
use Illuminate\Http\Resources\Json\JsonResource;

class Venue extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                    => $this->id,
            'thumbnail'             => $this->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),
            'name'                  => $this->name,
            'company'               => optional($this->company)->name,
            'company_id'            => optional($this->company)->id,
            'company_thumbnail'     => optional($this->company)->image('logo')->path('logos', '/images/placeholders/placeholder250x200.png'),
            'type'                  => $this->categoryList('venue_type'),
            'hearts'                =>  1,
            'hearted'               => $this->hearts()->whereUserId($request->user()->id)->first() ? 1 : 0,
            'shares'                => 10,
            'rating'                => round($this->reviews()->avg('rating'), 0),
            // 'reviews'       => ReviewResource::collection($this->reviews),
            // 'reviewed'      => $this->reviews()->whereUserId($request->user()->id)->first() ? 1 : 0,
            'price'                 => $this->price,
            'max_seated'            => $this->max_seated,
            'max_standing'          => $this->max_standing,
            'capacity'              => $this->capacity,
            'link'                  => route('site.venue.show', $this->slug),
            'location'              => $this->location,
            'recent_post'           => Inspiration::whereCompanyId($this->company_id)->orderBy('created_at','DESC')->count() ? Inspiration::whereCompanyId($this->company_id)->orderBy('created_at','DESC')->take(4)->get()->map(function($q){
                    return [
                        'id' => $q->id,
                        'thumbnail' => $q->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),
                        'name' => $q->name
                    ];
            }) :  [
                [
                    "id" => random_int(500,10000),
                    "thumbnail" =>"/images/thumbnails/5dc0ab70eb64e1572907888.jpg",
                    "name" =>"Luxury Garden Themed Wedding"
                ],
                [
                    "id" => random_int(500,10000),
                    "thumbnail" =>"/images/thumbnails/5dc0ab70eb64e1572907888.jpg",
                    "name" =>"Luxury Garden Themed Wedding"
                ]
            ]
        ];
    }
}
