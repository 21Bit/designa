<?php

namespace App\Http\Resources\Venue\Web;

use Auth;
use Illuminate\Http\Resources\Json\JsonResource;

class Venue extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                    => $this->id,
            'slug'                  => route('site.venue.show', $this->slug),
            'name'                  => $this->name,
            'thumbnail'             => $this->thumbnail,
            'location'              => $this->location,
            'description'           => $this->description,
            'venue_types'           => $this->categoryList('venue_type'),
            'capacity'              => $this->capacity,
            'average_cost_per_head' => $this->categoryList('average_cost_per_head'),
            'gallery_count'         => $this->images()->where('type', 'gallery')->count(),
            'function_spaces'       => $this->functionSpaces_count,
            'shares'                => $this->shares_count,
            'ratings'               => round($this->reviews()->avg('rating'), 0),
            'hearts'                => $this->hearts_count,
            'hearted'               => $this->hasHeartedOfLoggedUser(),
            'reviews'               => $this->reviews_count,
            'company'               => optional($this->company)->name,
            'company_id'            => optional($this->company)->id,
            'company_thumbnail'     =>  $this->companyLogo,
            'company_link'          => route('site.supplier.show', $this->company_slug)
        ];
    }

}
