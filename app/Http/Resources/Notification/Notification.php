<?php

namespace App\Http\Resources\Notification;

use Illuminate\Http\Resources\Json\JsonResource;

class Notification extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return  [
            "id"            => $this->id,
            "title"         => $this->data['title'],
            "avatar"        => $this->data['avatar'],
            "message"       => $this->data['message'],
            "notif_type"    => $this->data['notif_type'],
            "item_id"       => (int) $this->data['item_id'],
            "link"          => $this->data['link'] . '&notif=' . $this->id,
            "image"         => $this->data['image'],
            "timestamp"     => date('Y-m-d h:i:s', strtotime($this->created_at)), // Carbon::parse($this->created_at)->toIso8601String();
            'read_at'       => $this->read_at ? date('Y-m-d h:i:s', strtotime($this->read_at)) : ''
        ];
    }
}
