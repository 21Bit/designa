<?php

namespace App\Http\Resources\Notification\Dashboard;

use Illuminate\Http\Resources\Json\JsonResource;

class Notification extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return  [
            "id"            => $this->id,
            "title"         => $this->data['title'],
            "avatar"        => $this->data['avatar'],
            "message"       => $this->data['message'],
            "notif_type"    => $this->data['notif_type'],
            "item_id"       => $this->data['item_id'],
            "link"          => notification_url($this->data['link'], $this->id),
            "image"         => $this->data['image'],
            "timestamp"     => $this->created_at,
            'read_at'       => $this->read_at
        ];
    }
}
