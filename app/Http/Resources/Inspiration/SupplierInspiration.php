<?php

namespace App\Http\Resources\Inspiration;

use App\Models\Venue;
use App\Models\Video;
use App\Models\Product;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Video\Video as VideoResource;
use App\Http\Resources\Image\Image as ImageResource;
use App\Http\Resources\Venue\Venue as VenueResource;
use App\Http\Resources\Color\Category as ColorResource;
use App\Http\Resources\Company\Company as CompanyResource;
use App\Http\Resources\Product\Product as ProductResource;
use App\Http\Resources\Category\Category as CategoryResource;
use App\Http\Resources\Category\HashTag\Category as CategoryHashTagResource;

class SupplierInspiration extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'role' => $this->role->display_name,
            'company' => $this->company->name,
            'tagged_date' => $this->created_at->format('Y-m-d h:i:s'),
            'status'    => $this->status,
            'inspiration' => [
                            'id'                    =>  $this->inspiration->id,
                            'link'                  =>  route('site.inspiration.show', $this->inspiration->slug),
                            'name'                  =>  $this->inspiration->name,
                            'company'               =>  optional($this->inspiration->company)->name,
                            'company_id'            =>  optional($this->inspiration->company)->id,
                            'company_thumbnail'     =>  optional($this->inspiration->company)->image('logo')->path('logos', '/images/placeholders/placeholder250x200.png'),
                            'images'                =>  $this->inspiration->imageSlider() ? ImageResource::collection($this->inspiration->imageSlider()) : [],
                            'videos'                =>  VideoResource::collection($this->inspiration->videos()->where('type', 'inspiration_video')->get()) ?? (object) [] ,
                            'description'           =>  str_replace(['\r', '\n', '&nbsp;' ,'\r\n\r\n'], ' ',strip_tags($this->inspiration->description)),
                            'location'              =>  $this->inspiration->location_name,
                            'rating'                =>  round($this->inspiration->reviews()->avg('rating'), 0),
                            'shares'                =>  $this->inspiration->shares()->count(),
                            'hearts'                =>  $this->inspiration->hearts()->count(),
                            'hearted'               =>  $this->inspiration->hearts()->whereUserId($request->user()->id)->first() ? 1 : 0,
                            'featured'              =>  $this->inspiration->featured ? 1 : 0,// $this->inspiration->featured,
                            'hashtags'              =>  $this->inspiration->categories()->whereIn('type', ['cater','style', 'setting', 'color'])->get() ? CategoryHashTagResource::collection(optional($this->inspiration->categories()->whereIn('type', ['cater','style', 'setting', 'color'])->get())) : (object) [],
                            'event_type'            =>  $this->inspiration->categories()->whereType('cater')->first() ?  new CategoryResource(optional($this->inspiration->categories()->whereType('cater')->first())) : (object) [],
                            'style'                 =>  $this->inspiration->categories()->whereType('style')->first() ? new CategoryResource(optional($this->inspiration->categories()->whereType('style')->first())) : (object) [],
                            'setting'               =>  $this->inspiration->categories()->whereType('setting')->first() ?  new CategoryResource(optional($this->inspiration->categories()->whereType('setting')->first())) : (object) [],
                            'colors'                =>  $this->inspiration->categories()->whereType('color')->get() ? ColorResource::collection($this->inspiration->categories()->whereType('color')->get()) : [],
                            
                            'venues'                => [
                                                        "type"  => $this->inspiration->venue ? 'supplier' : 'physical',
                                                        'data' => $this->getVenueData($this->inspiration)
                                                    ] ,
                                                        
                            'suppliers'             =>  $this->inspiration->supplierRoles()->has('company')->has('role')->get() ?  $this->inspiration->supplierRoles()->has('company')->has('role')->get()->map(function($q){
                                                            return [
                                                                'id' => $q->id,
                                                                'role' => $q->role->display_name,
                                                                'company' => new CompanyResource($q->company)
                                                            ];
                                                        }) : [],

                            'decors'                =>   ProductResource::collection($this->inspiration->products) //Product::find([133, 135,136]) ? ProductResource::collection(Product::find([133, 135,136,138])) : []
                    ]
        ];
    }

    function getVenueData($data){
        if($data->venue){
            return new VenueResource($data->venue);
        }else{
            return [
                'location' => $data->location ?? '',
                'latitude' => $data->latitude ?? 0,
                'longitude' => $data->longitude ?? 0 
            ];
        }
    }
}
