<?php

namespace App\Http\Resources\Inspiration;

use App\Models\Venue;
use App\Models\Product;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Image\Image as ImageResource;
use App\Http\Resources\Venue\Venue as VenueResource;
use App\Http\Resources\Color\Category as ColorResource;
use App\Http\Resources\Company\Company as CompanyResource;
use App\Http\Resources\Product\Product as ProductResource;
use App\Http\Resources\Category\Category as CategoryResource;
use App\Http\Resources\Category\HashTag\Category as CategoryHashTagResource;

class InspirationManager extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $location = $this->venue ? $this->venue->name : $this->location;
        return [
            'id'                    =>  $this->id,
            'is_published'          =>  $this->is_published ? 1 : 0,
            'name'                  =>  $this->name,
            'thumbnail'             =>  $this->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),
            'cover'                 =>  $this->image('cover')->path('covers', 'https://via.placeholder.com/1920x450'),
            'description'           =>  str_replace(['\r', '\n', '&nbsp;' ,'\r\n\r\n'], ' ',strip_tags($this->description)),
            'location'              =>  $location ?? "" ,
            'rating'                =>  round($this->reviews()->avg('rating'), 0),
            'shares'                =>  $this->shares()->count(),
            'hearts'                =>  $this->hearts()->count(),
            'hearted'               =>  $this->hearts()->whereUserId($request->user()->id)->first() ? 1 : 0,
            'featured'              =>  $this->featured ? 1 : 0,// $this->featured,
            'hashtags'              =>  $this->categories()->whereIn('type', ['cater','style', 'setting', 'color'])->get() ? CategoryHashTagResource::collection(optional($this->categories()->whereIn('type', ['cater','style', 'setting', 'color'])->get())) : (object) [],
            'event_type'            =>  $this->categories()->whereType('cater')->first() ?  new CategoryResource(optional($this->categories()->whereType('cater')->first())) : (object) [],
            'style'                 =>  $this->categories()->whereType('style')->first() ? new CategoryResource(optional($this->categories()->whereType('style')->first())) : (object) [],
            'setting'               =>  $this->categories()->whereType('setting')->first() ?  new CategoryResource(optional($this->categories()->whereType('setting')->first())) : (object) [],
            'colors'                =>  $this->categories()->whereType('color')->get() ? ColorResource::collection($this->categories()->whereType('color')->get()) : [],

            'images'                => ImageResource::collection($this->images()->whereType('gallery')->get()),
            // 'video'                 => VideoResource::collection($this->videos),

        ];
    }



}
