<?php

namespace App\Http\Resources\Inspiration\Web;

use Auth;
use App\Http\Resources\Company\Company as CompanyResource;
use App\Http\Resources\Product\Product as ProductResource;
use App\Http\Resources\Image\Image as ImageResource;
use App\Http\Resources\Venue\Venue as VenueResource;
use Illuminate\Http\Resources\Json\JsonResource;

class Inspiration extends JsonResource
{


    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id'                    => $this->id,
            'slug'                  => $this->slug,
            'link'                  => route('site.inspiration.show', $this->slug),
            'name'                  => $this->name,
            'company'               => $this->companyName,
            'company_link'          => route('site.supplier.show', $this->company->slug),
            'company_thumbnail'     => $this->company->logo,
            'thumbnail'             => $this->thumbnail,
            'is_published'          => $this->is_published,
            'description'           => $this->description,
            'location'              => $this->locationName,
            'rating'                => $this->reviews_count,
            'shares'                => $this->shares_count,
            'hearts'                => $this->hearts_count,
            'hearted'               => $this->hasHeartedOfLoggedUser(),
        ];
    }


}
