<?php

namespace App\Http\Resources\Inspiration\Web;

use Storage;
use App\Models\Project;
use Illuminate\Http\Resources\Json\JsonResource;

class EventBoardInspirationSelection extends JsonResource
{

    public function toArray($request)
    {
        $project = Project::find(request()->segment(4));

        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'thumbnail'     => $this->thumbnail,
            'location'      => $this->location,
            'images'        => $this->images()->where('type', 'gallery')->get()->map(function ($q) use ($project) {
                                    return [
                                        'id' => $q->id,
                                        'path' => Storage::disk('sm-images')->url($q->path),
                                        'selected' => $project->inspirationImages()->where('image_id', $q->id)->count() ? 1 : 0
                                    ];
                                })
        ];
    }

}
