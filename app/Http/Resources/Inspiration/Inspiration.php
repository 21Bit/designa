<?php

namespace App\Http\Resources\Inspiration;

use Auth;
use App\Models\Venue;
use App\Models\Product;
use App\Http\Resources\Video\Video as VideoResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Image\Image as ImageResource;
use App\Http\Resources\Venue\Venue as VenueResource;
use App\Http\Resources\Color\Category as ColorResource;
use App\Http\Resources\Company\Company as CompanyResource;
use App\Http\Resources\Product\Product as ProductResource;
use App\Http\Resources\Category\Category as CategoryResource;
use App\Http\Resources\Category\HashTag\Category as CategoryHashTagResource;
use App\Models\Video;

class Inspiration extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if(Auth::user()){
            $hearted  = $this->hearts()->whereUserId(Auth::user()->id)->first() ? 1 : 0;
        }else{
            $hearted = 0;
        }

        return [
            'id'                    =>  $this->id,
            'link'                  =>  route('site.inspiration.show', $this->slug),
            'is_published'          =>  $this->is_published ? 1 : 0,
            'name'                  =>  $this->name,
            'company'               =>  $this->companyName,
            'company_id'            =>  optional($this->company)->id,
            'company_thumbnail'     =>  optional($this->company)->image('logo')->path('logos', '/images/placeholders/placeholder250x200.png'),
            'images'                =>  $this->imageSlider() ? ImageResource::collection($this->imageSlider()) : [],
            'videos'                =>  VideoResource::collection($this->videos()->where('type', 'inspiration_video')->get()) ?? (object) [] ,
            'description'           =>  $this->description_for_mobile,
            'location'              =>  $this->locationName ,
            'rating'                =>  round($this->reviews()->avg('rating'), 0),
            'shares'                =>  $this->shares()->count(),
            'hearts'                =>  $this->hearts()->count(),
            'hearted'               =>  $hearted,
            'featured'              =>  $this->featured ? 1 : 0,// $this->featured,
            'hashtags'              =>  $this->categories()->whereIn('type', ['cater','style', 'setting', 'color'])->get() ? CategoryHashTagResource::collection(optional($this->categories()->whereIn('type', ['cater','style', 'setting', 'color'])->get())) : (object) [],
            'event_type'            =>  $this->categories()->whereType('cater')->first() ?  new CategoryResource(optional($this->categories()->whereType('cater')->first())) : (object) [],
            'style'                 =>  $this->categories()->whereType('style')->first() ? new CategoryResource(optional($this->categories()->whereType('style')->first())) : (object) [],
            'setting'               =>  $this->categories()->whereType('setting')->first() ?  new CategoryResource(optional($this->categories()->whereType('setting')->first())) : (object) [],
            'colors'                =>  $this->categories()->whereType('color')->get() ? ColorResource::collection($this->categories()->whereType('color')->get()) : [],
            
            'venues'                => [
                                        "type"  => $this->venue ? 'supplier' : 'physical',
                                        'data' => $this->getVenueData($this)
                                    ] ,
                                        
            'suppliers'             =>  $this->supplierRoles()->where('status', 'confirmed')->has('company')->has('role')->get() ?  $this->supplierRoles()->where('status', 'confirmed')->has('company')->has('role')->get()->map(function($q){
                                            return [
                                                'id' => $q->id,
                                                'role' => $q->role->display_name,
                                                'company' => new CompanyResource($q->company)
                                            ];
                                        }) : [],

            'decors'                =>   ProductResource::collection($this->products) //Product::find([133, 135,136]) ? ProductResource::collection(Product::find([133, 135,136,138])) : []
        ];
    }



    function getVenueData($data){
        if($data->venue){
            return new VenueResource($data->venue);
        }else{
            return [
                'location' => $data->location ?? '',
                'latitude' => $data->latitude ?? 0,
                'longitude' => $data->longitude ?? 0 
            ];
        }
    }

    
}
