<?php

namespace App\Http\Resources\Inspiration\Location;

use Illuminate\Http\Resources\Json\JsonResource;

class Inspiration extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'location' => $this->location
        ];
    }
}
