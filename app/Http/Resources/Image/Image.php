<?php

namespace App\Http\Resources\Image;

use Illuminate\Http\Resources\Json\JsonResource;

class Image extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if($this->type == "gallery"){
            return [
                'id' => $this->id,
                'preloader' => imageViewer('/images/32x32/'.$this->path, '/images/placeholders/placeholder.png'),
                'image' => imageViewer('/images/gallery/'.$this->path, '/images/placeholders/placeholder.png'),
                'shares' => $this->shares()->count(),
                'hearts'    => $this->hearts()->count()
            ];
        }else if($this->type == "thumbnail"){
            return [
                'id' => $this->id,
                'preloader' => imageViewer('/images/32x32/'.$this->path, '/images/placeholders/placeholder.png'),
                'image' => imageViewer('/images/thumbnails/'.$this->path, '/images/placeholders/placeholder.png'),
                // 'shares' => random_int(1,50),
                // 'hearts'    => $this->hearts()->count()
            ];
        }
    }
}
