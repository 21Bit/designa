<?php

namespace App\Http\Resources\Review;

use Auth;
use Illuminate\Http\Resources\Json\JsonResource;

class Review extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       return [
            'id' => $this->id,
            'thumbnail' => $this->user->getProfilePicture(), 
            'name' => optional($this->user)->name,
            'company' => optional($this->user)->name,
            'comment' => $this->comment,
            'rating'    => $this->rating,
            'created_at' => $this->created_at->format('M d, Y h:i a '),
            'updated_at' => $this->created_at->format('M d, Y h:i a '),
            'owner'      => $this->user_id  == optional(Auth::user())->id ? 1 : 0
       ];
    }
}
