<?php

namespace App\Http\Resources\Review\Comment;

use Auth;
use Illuminate\Http\Resources\Json\JsonResource;

class Review extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'thumbnail'     => '/images/placeholders/placeholder.png',
            'name'          => optional($this->user)->name,
            'comment'       => $this->comment,
            'created_at'    => $this->created_at->format('M d, Y h:i a '),
            'owner'         => $this->user_id
       ];
    }
}
