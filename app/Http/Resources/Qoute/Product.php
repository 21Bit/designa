<?php

namespace App\Http\Resources\Qoute;

use Illuminate\Http\Resources\Json\JsonResource;

class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'thumbnail'     => $this->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder250x200.png'),
            'company'       => $this->company->name,
            'product_type'  => $this->categories()->whereType('product_type')->first()->name,
            'quantity'      => 1,
            'hearted'       => $request->user ? $this->hearts()->whereUserId($request->user()->id)->first() ? 1 : 0 : 0
        ];
    }
}
