<?php

namespace App\Http\Resources\Qoute;

use App\Http\Resources\Attachment\Attachment;
use App\Models\Company;
use App\Models\Category;
use App\Models\PricingComponent;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Review\Comment\Review as Comment;

class Qoute extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $qoute = $this;

        $supplierIds = Array();
        $data  = array();


        foreach($qoute->decors as $decor){
            if($decor->product){
                $company_id = $decor->product->company_id;
                if(!in_array($company_id, $supplierIds)){
                    array_push($supplierIds, $company_id);
                }
            }
        }

            // for($i = 0; $supplierIds > $i; $i++){
            foreach($supplierIds as $supplierId){
                $supplier  = Company::find($supplierId);
                $array = array(
                   'supplier' => [
                            'id' => $supplier->id,
                            'name' => $supplier->name,
                            'avatar' => $supplier->image('logo')->path('logos', '/images/placeholders/placeholder.png'),
                            'status' => $qoute->isSupplierPricingsComplete($supplierId) ? "completed" : "pending", //completed, pending 
                            'total_price' => $qoute->supplierTotalPrice($supplierId),
                            'reference_number' => $qoute->reference_number, 
                        ],
                    'decor' => $qoute->decors()->whereHas('product', function($q) use ($supplierId){
                            $q->whereCompanyId($supplierId);
                        })->get()->map(function($decor){
                            return [
                                'id' => $decor->id,
                                'product' => $decor->product->name,
                                'thumbnail' => $decor->product->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),
                                'quantity' => $decor->quantity ?? 0,
                                'responses' => PricingComponent::whereActive(1)->get()->map(function($q) use ($decor){
                                    return [
                                        'name' => $q->name,
                                        'price' => $decor->getComponentPrice($q->id),
                                        // 'subtotal' => $decor->getSubTotalPrice($q->id)
                                    ];  
                                }),
                                'product_total_cost' => $decor->getTotal(),
                                'comments' => Comment::collection($decor->reviews),
                                'files' => Attachment::collection($decor->attachments)
                            ];
                        })
                );
            array_push($data, $array);
        }


        return [
                'quote_information' =>  [
                        'id' => $qoute->id,
                        'event_name' => $qoute->event_name,
                        'event_type' => Category::find($qoute->event_type)->name ?? "",
                        'event_date' => $qoute->event_date,
                        'location' => $qoute->location,
                        'time_start' => $qoute->time_start,
                        'time_end' => $qoute->time_end,
                        'number_of_guests' => $qoute->number_of_guests,
                        'reference_number' => $qoute->reference_number,
                        'created_at' => $qoute->created_at->format('Y-m-d H:i:s'),
                        'overall_total' => $qoute->getOverAllPrice()
                ],
                'list' =>  $data,
            ];
    }

}
