<?php

namespace App\Http\Resources\Qoute;

use Auth;
use Illuminate\Http\Resources\Json\JsonResource;

class SupplierQoute extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'avatar'        => optional($this->user)->getProfilePicture(),
            'products'      => $this->decors()->whereHas('product',function($q) use ($request){
                                    $q->where('company_id', $request->user()->company_id);
                                })->count(),
            'event_name'    => $this->event_name,
            'name'          => optional($this->user)->name,
            'customer_id'   => optional($this->user)->id,
            'created_at'    => $this->created_at,
            'status'        => $this->isSupplierPricingsComplete($request->user()->company_id) ? "Completed" : "Pending",
            'reference_id'  => $this->reference_number,
            'item_id'       => $this->id
        ];
    }
}
