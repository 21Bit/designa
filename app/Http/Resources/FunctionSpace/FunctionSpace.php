<?php

namespace App\Http\Resources\FunctionSpace;

use Illuminate\Http\Resources\Json\JsonResource;

class FunctionSpace extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'thumbnail' => $this->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),
            'name' => $this->name,
            'description' => $this->description,
            'max_capacity_seated' => $this->max_capacity_seated,
            'max_capacity_standing' => $this->max_capacity_standing,
        ];
    }
}
