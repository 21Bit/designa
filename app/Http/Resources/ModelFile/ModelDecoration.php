<?php

namespace App\Http\Resources\ModelFile;

use Illuminate\Http\Resources\Json\JsonResource;

class ModelDecoration extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id'                                => $this->id,
            'name'                              => $this->model_name,
            'item_id'                           => $this->modellable_id,
            'is_public'                         => $this->is_public,
            'base_layer_id'                     => $this->base_layer_id,
            'number_of_guest'                   => $this->number_of_guest, 

            'Venue_AssetBundleWEB'              => $this->base_asset_bundle_web ? url($this->base_asset_bundle_web->path) : null,
            'Venue_AssetBundleIOS'              => $this->base_asset_bundle_ios ? url($this->base_asset_bundle_ios->path) : null,
            'Venue_AssetBundleAndroid'          => $this->base_asset_bundle_android ? url($this->base_asset_bundle_android->path) : null,

            'base_layer_binary_file_url'        => $this->base ? url($this->base->path) : '',
            'decoration_layer_binary_file_url'  => url($this->path),

            'deleted'                           => $this->deleted_at ? 1 : 0
        ];
    }
}
