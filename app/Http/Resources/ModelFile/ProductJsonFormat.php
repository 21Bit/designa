<?php

namespace App\Http\Resources\ModelFile;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductJsonFormat extends JsonResource{

    public function toArray($request)
    {
        return [
            'product_id'                => $this->id,
            'productName'               => $this->name,
            'category'                  => optional($this->product_type)->name,
            'subcategory'               => optional($this->style)->name,
            'description'               => $this->description,
            'supplierName'              => optional($this->company)->name,
            'supplierLogo'              => $this->company->image('logo')->path('32x32', '/images/placeholders/placeholder.png'),
            'date'                      => date('d/m/Y_H:i', strtotime($this->created_at)),
            'Product_AssetBundleWEB'    => $this->asset_bundle_web ? url($this->asset_bundle_web->path) : null,
            'Product_AssetBundleIOS'    => $this->asset_bundle_ios ? url($this->asset_bundle_ios->path) : null,
            'Product_AssetBundleAndroid'=> $this->asset_bundle_android ? url($this->asset_bundle_android->path) : null,
            'thumbnail_URLs'            => $this->images()->get()->map(function($q){
                                            $folder = $q->type == "thumbnail" ? 'images/thumbnails' : 'images/gallery';
                                            return [
                                                'thumbnail' => url($folder . '/' . $q->path)
                                            ];
                                        })
        ];
    }

//     “product_id”: 2
// “productName” :“Name”,
// “category” : “Tableware”,
// “subcategory” : “Linen”,
// “suplierName” : “SuplierName”,
// “description” : “example of product description”,
}
