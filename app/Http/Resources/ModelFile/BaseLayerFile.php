<?php

namespace App\Http\Resources\ModelFile;

use Illuminate\Http\Resources\Json\JsonResource;

class BaseLayerFile extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                        => $this->id,
            'baselayer_name'            => $this->model_name,
            'event_space_id'            => $this->modellable_id,
            'Venue_AssetBundleWEB'      => $this->modellable->asset_bundle_web ? url($this->modellable->asset_bundle_web->path) : null,
            'Venue_AssetBundleIOS'      => $this->modellable->asset_bundle_ios ? url($this->modellable->asset_bundle_ios->path) : null,
            'Venue_AssetBundleAndroid'  => $this->modellable->asset_bundle_android ? url($this->modellable->asset_bundle_android->path) : null,
            'binary_file'               => url($this->path), 
            'number_of_guest'           => $this->number_of_guest, 
            'deleted'                   => $this->deleted_at ? 1 : 0,
            'is_public'                 => $this->is_public,
        ];
    }
}
