<?php

namespace App\Http\Resources\ModelFile;

use Illuminate\Http\Resources\Json\JsonResource;

class ModelFileJSONFormat extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'model_id'          => $this->id,
            'model_name'        => $this->model_name,
            'item_id'           => $this->modellable_id,
            'path'              => url($this->path), 
            'is_public'         => $this->is_public,
            'base_layer_id'    =>  $this->base_layer_id,
        ];
    }
}
