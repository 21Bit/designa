<?php

namespace App\Http\Resources\ModelFile;

use Illuminate\Http\Resources\Json\JsonResource;

class FloorPlanFile extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                        => $this->id,
            'floorplan_name'            => $this->model_name,
            'binary_file'               => url($this->path),
            'deleted'                   => $this->deleted_at ? 1 : 0,
            'is_public'                 => $this->is_public,
        ];
    }
}
