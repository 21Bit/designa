<?php

namespace App\Http\Resources\ModelFile;

use Illuminate\Http\Resources\Json\JsonResource;

class AssetBundle extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    => $this->id,
            'Venue_AssetBundleWEB' => url($this->path), 
            'Venue_AssetBundleAndroid' => url($this->path), 
            'Venue_AssetBundleIOS' => url($this->path), 
            'is_public'   => $this->is_public,
            'event_space_id'    => $this->modellable_id,
        ];
    }
}
