<?php

namespace App\Http\Resources\Company;

use App\Models\Inspiration;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Review\Review as ReviewResource;

class Company extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'link'          => route('site.supplier.show', $this->slug),
            'name'          => $this->name,
            'logo'          => $this->image('logo')->path('logos', '/images/placeholders/placeholder.png'),
            'logo32_32'     => $this->image('logo')->path('32x32', '/images/placeholders/placeholder.png'),
            'cover'         => $this->image('cover')->path('covers', '/images/placeholders/placeholder.png'),
            'roles_list'    => $this->roleList(), // string,
            'stocks'        => random_int(1,10),
            'ratings'       => round($this->reviews()->avg('rating'), 0),
            'hearts'        => $this->hearts()->count(),
            'hearted'       => $this->hearts()->whereUserId($request->user()->id)->first() ? 1 : 0,
            'link'          =>  route('site.supplier.show', $this->slug),
            'shares'        => $this->shares()->count(),
            'location'      => $this->town . ', ' . $this->country,
            // 'reviews'        => ReviewResource::collection($this->reviews),
            // 'reviewed'      => $this->reviews()->whereUserId($request->user()->id)->first() ? 1 : 0,
            'recent_post'   => Inspiration::whereCompanyId($this->id)->orderBy('created_at','DESC')->count() ? Inspiration::whereCompanyId($this->id)->orderBy('created_at','DESC')->take(4)->get()->map(function($q){
                return [
                    'id' => $q->id,
                    'thumbnail' => $q->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),
                    'name' => $q->name
                ];
            })  : []
        ];
    }
}
