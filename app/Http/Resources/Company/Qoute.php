<?php

namespace App\Http\Resources\Company;

use Illuminate\Http\Resources\Json\JsonResource;

class Qoute extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'avatar' => optional($this->user)->getProfilePicture(),
            'productions' => $this->decors()->count(),
            'event_name'  => $this->event_name,
            'name' => optional($this->user)->name,
            'created_at' => $this->created_at,
            'status'  => $this->isSupplierPricingsComplete($request->user()->company_id) ? "Completed" : "Pending",
            'reference_id' => $this->reference_number,
            'item_id' => $this->id  
        ];
    }
}
