<?php

namespace App\Http\Resources\Company\Web;

use Illuminate\Http\Resources\Json\JsonResource;

class Company extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'name'      => $this->name,
            'distance'  => $this->when($this->address_distance, $this->address_distance ),
            'cover'      => $this->image('cover')->path('covers', '/images/placeholders/placeholder250x250.png'),
            'logo'      => $this->image('logo')->path('logos', '/images/placeholders/placeholder250x250.png'),
            'logo32_32' => $this->image('logo')->path('32x32', '/images/placeholders/placeholder.png'),
            'role_list' => $this->roleList(), // string,
            'slug'      => route('site.supplier.show', $this->slug),
            'stocks'    => $this->products_count,
            'ratings'   => round($this->reviews()->avg('rating'), 0),
            'hearts'    => $this->hearts_count,
            'shares'    => $this->shares_count,
            'location'  => $this->town . ', ' . $this->country
        ];
    }
}
