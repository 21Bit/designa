<?php

namespace App\Http\Resources\MoodBoard;


use App\Http\Resources\Category\Category as CategoryResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Color\Category as ColorResource;

class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this['id'],
            'name'          => $this['name'],
            'thumbnail'     => $this['thumbnail'],
            'colors'        => $this['colors'],
            'product_type'  => $this['product_type'],
            'by'            => $this['by'],
            'company_id'    => $this['company_id'],
        ];
        // list($width, $height) = getimagesize(public_path($this->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png')));
        // return [
        //     'id'            => $this->id,
        //     'name'          => $this->name,
        //     'thumbnail'     => [
        //                             'id' => optional($this->images('thumbnail')->first())->id,
        //                             'path' => $this->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'), 
        //                             'width' => $width,
        //                             'height' => $height,
        //                             'selected' => 0
        //                         ],
        //     'colors'        => ColorResource::collection($this->categories()->whereType('color')->get()),
        //     'product_type'  => optional($this->categories()->whereType('product_type')->first())->name,
        //     'by'            => optional($this->company)->name,
        //     'company_id'    => optional($this->company)->id,
        // ];
    }
}
