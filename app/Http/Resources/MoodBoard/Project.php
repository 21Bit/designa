<?php

namespace App\Http\Resources\MoodBoard;

use App\Http\Resources\MoodBoard\ProductImage;
use App\Models\Image;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\MoodBoard\InspirationImage;
use App\Http\Resources\Color\Category as ColorResource;
use App\Http\Resources\Category\Category as CategoryResource;

class Project extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                    => $this->id,
            'title'                 => $this->title,
            'description'           => $this->description,

            'event_type'            =>  $this->categories()->whereType('cater')->first() ?  new CategoryResource(optional($this->categories()->whereType('cater')->first())) : (object) [],
            'style'                 =>  $this->categories()->whereType('style')->first() ? new CategoryResource(optional($this->categories()->whereType('style')->first())) : (object) [],
            'setting'               =>  $this->categories()->whereType('setting')->first() ?  new CategoryResource(optional($this->categories()->whereType('setting')->first())) : (object) [],
            'colors'                =>  $this->categories()->whereType('color')->get() ? ColorResource::collection($this->categories()->whereType('color')->get()) : [],
            'pitchpack'             =>  route('mood-board.show.pitchpack', $this->slug),
            'inspiration_images'    =>  InspirationImage::collection($this->inspirationImages),
            'product_images'        =>  ProductImage::collection($this->productImages)
        ];
    }
}
