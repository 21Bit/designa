<?php

namespace App\Http\Resources\MoodBoard;

use App\Models\Venue;
use App\Models\Video;
use App\Models\Product;
use App\Models\Project;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\MoodBoard\InspirationImage;
use App\Http\Resources\Image\Image as ImageResource;
use App\Http\Resources\Venue\Venue as VenueResource;
use App\Http\Resources\Video\Video as VideoResource;
use App\Http\Resources\Color\Category as ColorResource;
use App\Http\Resources\Company\Company as CompanyResource;
use App\Http\Resources\Product\Product as ProductResource;
use App\Http\Resources\Category\Category as CategoryResource;
use App\Http\Resources\Category\HashTag\Category as CategoryHashTagResource;

class Inspiration extends JsonResource
{
    // public function __construct($resource, $project)
    // {
    //     parent::__construct($resource);
    //     $this->resource = $resource;
    //     // $this->project = $project;
    // }
    
    // protected $project;

    // public function project($project){
    //     $this->project = $project;
    //     return $this;
    // }

    public function toArray($request)
    {
        return [
            'id'                    =>  $this['id'],
            'name'                  =>  $this['name'],
            'company'               =>  $this['company'],
            'company_id'            =>  $this['company_id'],
            'thumbnail'             =>  $this['thumbnail'],
            'company_thumbnail'     =>  $this['company_thumbnail'],
            'images'                => $this['images']
        ];

    }

    
}
