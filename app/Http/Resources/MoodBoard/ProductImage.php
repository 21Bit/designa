<?php

namespace App\Http\Resources\MoodBoard;

use App\Models\Project;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductImage extends JsonResource
{
    public function __construct($resource, $project)
    {
        parent::__construct($resource);
        $this->resource = $resource;
        $this->project = $project;
    }

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'preloader'     => null,
            'image'         => imageViewer('/images/thumbnails/'. $this->path, '/images/placeholders/placeholder.png'),
            'selected'      => $this->project instanceof Project ? 1 : 0
            // 'selected' => $this->project->inspirationImages()->whereId($this->id)->count() ? 1 : 0
        ];

    }
}
