<?php

namespace App\Http\Resources\MoodBoard\Web;

use Illuminate\Http\Resources\Json\JsonResource;

class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this['id'],
            'name'          => $this['name'],
            'thumbnail'     => $this['thumbnail'],
            'colors'        => $this['colors'],
            'product_type'  => $this['product_type'],
            'by'            => $this['by'],
            'company_id'    => $this['company_id'],
            'company'       => $this['company'],
        ];
    }
}
