<?php

namespace App\Http\Resources\MoodBoard\Web;

use Illuminate\Http\Resources\Json\JsonResource;

class Inspiration extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this['id'],
            'name'          => $this['name'],
            'thumbnail'     => $this['thumbnail'],
            'location'      => $this['location'],
            'images'        => $this['images']
        ];
    }
}
