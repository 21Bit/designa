<?php

namespace App\Http\Resources\MoodBoard;

use Illuminate\Http\Resources\Json\ResourceCollection;

class InspirationCollection extends ResourceCollection
{
    protected $project;

    public function __construct($resource, $project)
    {
        $this->project = $project;

        parent::__construct($resource);
    }
    
    function toArray($request)
    {
        return parent::toArray($request, $this->project);
    }
}
