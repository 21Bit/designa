<?php

namespace App\Http\Resources\MoodBoard;

use App\Models\Project;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class InspirationImage extends JsonResource
{
    public function __construct($resource, $project)
    {
        parent::__construct($resource);
        $this->resource = $resource;
        $this->project = $project;
    }

    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'preloader'     => imageViewer('/images/32x32/'.$this->path, '/images/placeholders/placeholder.png'),
            'image'         => imageViewer('/images/gallery/'. $this->path, '/images/placeholders/placeholder.png'),
            'selected'      => $this->project instanceof Project ? 1 : 0,
            'ss'            => $this->project, //->inspirationImages()->whereId($this->id)->count() ? 1 : 0
        ];

    }
}
