<?php

namespace App\Http\Resources\Cater;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Category\CategoryCollection;

class Category extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'checked' => 0,
            'types' => new CategoryCollection($this->styles),
            'settings' => new CategoryCollection($this->settings)
        ];
    }
}
