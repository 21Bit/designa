<?php

namespace App\Http\Resources\Category\HashTag;

use Illuminate\Http\Resources\Json\JsonResource;

class Category extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => str_replace('/','',strtolower(\Str::camel($this->name))),
        ];
    }
}
