<?php

namespace App\Http\Resources\Product;


use App\Http\Resources\Category\Category as CategoryResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Color\Category as ColorResource;

class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        list($width, $height) = getimagesize(public_path($this->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png')));
        $style = $this->categories()->whereType('style')->first();

        return [
            'id'                    => $this->id,
            'name'                  => $this->name,
            // 'thumbnail'     => $this->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),

            'thumbnail'             => [
                                            'path' => $this->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),
                                            'width' => $width,
                                            'height' => $height
                                        ],
            'colors'                => ColorResource::collection($this->categories()->whereType('color')->get()),
            'product_type'          => optional($this->categories()->whereType('product_type')->first())->name . ($style ? " / " . $style->name : ''),
            'by'                    => optional($this->company)->name,
            'product_identifier'    => $this->product_identifier,
            'product_size'          => $this->product_size,
            'hearts'                => $this->hearts()->count(),
            'hearted'               => $this->hearts()->whereUserId($request->user()->id)->first() ? 1 : 0,
            'company_id'            => optional($this->company)->id,
            // 'link'          => route('site.product.show', $this->slug),
            'shares'                => random_int(1,50)
        ];
    }
}
