<?php

namespace App\Http\Resources\Product;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Image\Image as ImageResource;
use App\Http\Resources\Color\Category as ColorResource;
use App\Http\Resources\Category\Category as CategoryResource;

class ProductManager extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        list($width, $height) = getimagesize(public_path($this->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png')));

        return [
            'id'                    => $this->id,
            'name'                  => $this->name,
            'is_published'          => $this->is_published,

            'thumbnail'             => [
                                            'path' => $this->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),
                                            'width' => $width,
                                            'height' => $height
                                        ],
            'colors'                => ColorResource::collection($this->colors),
            'cater'                 => $this->caters()->count() ?  CategoryResource::collection(optional($this->caters)) : [],
            'description'           => $this->description,
            'product_type_id'       => optional($this->productType()->first())->id,
            'product_type'          => optional($this->productType()->first())->name,
            'product_identifier'     => $this->product_identifier,
            'product_size'          => $this->product_size,
            'product_style_id'      => optional($this->productStyle()->first())->id,
            'product_style'         => optional($this->productStyle()->first())->name,
            'by'                    => optional($this->company)->name,
            'gallery'               => ImageResource::collection($this->images()->whereType('gallery')->get()),
            'hearts'                => $this->hearts()->count(),
            'company_id'            => optional($this->company)->id,
            'shares'                => $this->shares()->count()
        ];
    }
}
