<?php

namespace App\Http\Resources\Product\Web;

use App\Http\Resources\Image\Image as ImageResource;
use App\Http\Resources\Color\Category as ColorCategory;
use App\Http\Resources\Category\Category as CategoryResource;
use App\Services\CategoryService;
use Illuminate\Http\Resources\Json\JsonResource;

class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $relatedItems = Product::orderBy('name', 'ASC');
        $categoryService = new CategoryService;
        if($request->categories){
            foreach($request->categories as $category){
                $relatedItems->whereHas('categories',function($q) use ($category){
                    $q->where('id', $category);
                });
            }
        }
        
        return [
            'id'                => $this->id,
            'thumbnail'         => $this->thumbnail,
            'company_logo'      => optional($this->company)->image('logo')->path('32x32', '/images/placeholders/placeholder250x200.png'),
            'name'              => $this->name,
            'description'       => $this->description,
            'colors'            => ColorCategory::collection($this->categories()->where('type', 'color')->get()),
            'event_type'        => $this->categories()->whereType('cater')->get()->implode('name', '/'), //$this->categoryList('cater', '/'),//CategoryResource::collection($this->categories()->whereType('cater')->get()),
            'product_type'      => $this->categories()->whereType('product_type')->get()->implode('name', '/'), //$this->categoryList('product_type', '/'), //CategoryResource::collection($this->categories()->whereType('product_type')->get()),
            'style'             => $this->categories()->whereType('style')->get()->implode('name', '/'), //CategoryResource::collection($this->categories()->whereType('style')->get()),
            'gallery'           => ImageResource::collection($this->images()->whereType('gallery')->get()),
            'shares'            => $this->shares_count,
            'hearts'            => $this->hearts_count,
            'reviews'           => $this->reviews_count,
            'rating'            => round($this->reviews->avg('rating')),
            'hearted'           => $this->hasHeartedOfLoggedUser(),
            'company'               => $this->company->name,
            'company_rating'        => $this->company->reviews()->avg('rating'),
            'company_thumbnail'     => $this->company->image('logo')->path('logos', '/images/placeholders/placeholder.png'),
            'companyUrl'            => $this->company ? route('site.supplier.show', $this->company->slug) : "",
        ];
    }

}
