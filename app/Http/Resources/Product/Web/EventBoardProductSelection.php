<?php

namespace App\Http\Resources\Product\Web;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Color\Category as ColorResource;
use App\Models\Project;

class EventBoardProductSelection extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $project = Project::find(request()->segment(4));
        $selected = $project->productImages()->pluck('image_id')->toArray();
        $thumbnail = $this->images('thumbnail')->first()->id;
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'thumbnail'     =>  [
                                    'id'        => $thumbnail,
                                    'path'      => $this->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),
                                    'selected'  => in_array($thumbnail, $selected)
                                ],
            'product_type'  => optional($this->categories()->whereType('product_type')->first())->name,
            'by'            => optional($this->company)->name,
            'company_id'    => optional($this->company)->id,
            'company'       => optional($this->company)->name,
        ];
    }
}
