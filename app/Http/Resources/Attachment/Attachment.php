<?php

namespace App\Http\Resources\Attachment;

use Illuminate\Http\Resources\Json\JsonResource;

class Attachment extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $infoPath = pathinfo(storage_path($this->path));
        return [
            'id' => $this->id,
            'filename' => $this->filename,
            'type'    => $infoPath['extension'],
            'path' => $this->path
        ];
    }
}
