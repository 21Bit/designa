<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\GoogleService;

class GoogleMapController extends Controller
{
    function geocodeJson(Request $request){
        return json_encode((new GoogleService)->geocodeJson($request->latlng));
    }


    function placeAutoComplete(Request $request){
        return json_encode((new GoogleService)->placeAutoComplete($request->input, $request->type));
    }


    function detailsJson(Request $request){
        return json_encode((new GoogleService)->detailsJson($request->place_id));
    }
}
