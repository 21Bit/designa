<?php

namespace App\Http\Controllers\API;

use Auth;

//for Inspiration
use App\Models\Inspiration;
use App\Http\Resources\Inspiration\Inspiration as InspirationResource;

//for Venues
use App\Models\Venue;
use App\Http\Resources\Venue\Venue as VenueResource;

//for Product
use App\Models\Product;
use App\Http\Resources\Product\Product as ProductResource;

//for Company
use App\Models\Company;
use App\Http\Resources\Company\Company as CompanyResource;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HeartedController extends Controller
{
    function inspirations(){
        $inspirationIds = Auth::user()->hearted()->where('heartable_type', 'App\Models\Inspiration')->pluck('heartable_id')->toArray();
        return InspirationResource::collection(Inspiration::whereIn('id', $inspirationIds)->paginate(10));
    }

    function venues(){
        $venueIds = Auth::user()->hearted()->where('heartable_type', 'App\Models\Venue')->pluck('heartable_id')->toArray();
        return VenueResource::collection(Venue::whereIn('id', $venueIds)->paginate(10));
    }

    function suppliers(){
        $companyIds = Auth::user()->hearted()->where('heartable_type', 'App\Models\Company')->pluck('heartable_id')->toArray();
        return CompanyResource::collection(Company::whereIn('id', $companyIds)->paginate(10));
    }

    function decors(){
        $productIds = Auth::user()->hearted()->where('heartable_type', 'App\Models\Product')->pluck('heartable_id')->toArray();
        return ProductResource::collection(Product::whereIn('id', $productIds)->paginate(10));
    }
}
