<?php

namespace App\Http\Controllers\API;

use Auth;
use App\Models\ModelFile;
use App\Models\Venue;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Model3dMobile\BaseLayerJsonFormat;

class BaseLayerController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $query  = ModelFile::whereType('base_layer')->has('modellable');

        if($request->tab == "my-baselayers"){
            $query->where('modellable_id', $request->user()->id);
        }else{
            $query->where('is_public', 1)->where('modellable_id', '!=', $request->user()->id);
        }

        if($request->q){
            $query->where('model_name', 'LIKE', "%$request->q%");
        }

        $models = $query->latest()->paginate(12);

        return BaseLayerJsonFormat::collection($models);
    }


    function eventSpaceList(Request $request): array
    {
        return [
            'data' => Venue::where('company_id', Auth::user()->company_id)->with('functionSpaces')->get()->map(function($q){
                    return [
                        'venue_name' => $q->name,
                        'event_spaces' => $q->functionSpaces->map(function($q){
                            return [
                                'id' => $q->id,
                                'name' => $q->name,
                                'has_asset_bundles' => $q->asset_bundle_web ? true : false
                            ];
                        })
                    ];
                })
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $beenUsed = ModelFile::where('base_layer_id', $id)
                ->withTrashed()->count();

        if($beenUsed){
            ModelFile::find($id)->delete();
        }else{
            ModelFile::find($id)->forceDelete();
        }

        return response()->json([
            'status' => 200,
            'message' => "Base Layer is deleted!"
        ], 200);
    }
}
