<?php

namespace App\Http\Controllers\API;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ModelFileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user  = Auth::user();
        return $user->model_file_list(true, 20);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user  = Auth::user();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user  = Auth::user();

        $file = $request->file('model_file');

        //Move Uploaded File
        $destinationPath = public_path('models/');
        
        $filename = time() . '.' . $file->getClientOriginalExtension();

        $path = $file->move($destinationPath, $filename);

        return $user->saveModelFile([
            'model_name'     => $request->name,
            'path'           => 'models/' . $filename,
            'is_public'      => $request->is_public,
            'base_layer_id'  => $request->base_layer_id,
        ]);

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user  = Auth::user();
        
        if($request->HasFile('model_file')){

            $file = $request->file('model_file');
            $destinationPath = public_path('models/');
            
            $filename = time() . '.' . $file->getClientOriginalExtension();
    
            $path = $file->move($destinationPath, $filename);

            return $user->updateModelFile([
                'id'             => $id,
                'model_name'     => $request->name,
                'path'           => 'models/' . $filename,
                'is_public'      => $request->is_public,
                'base_layer_id'  => $request->base_layer_id,
            ]);
        }
        
        
        return $user->updateModelFile([
            'id'             => $id,
            'model_name'     => $request->name,
            'is_public'      => $request->is_public ? 1 : 0,
            'base_layer_id'  => $request->base_layer_id,
        ]);

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();

        return $user->deleteModel([
            'id' => $id
        ]);
    }
}
