<?php

namespace App\Http\Controllers\API;

use App\Models\Heart;
use App\Models\Review;
use App\Models\Company;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Image\Image as ImageResource;
use App\Http\Resources\Review\Review as ReviewResource;
use App\Http\Resources\Product\Product as ProductResource;
use App\Http\Resources\Category\Category as CategoryResource;
use App\Http\Resources\Color\CategoryCollection as ColorCategories;

class ProductController extends Controller
{


    function baseLayer($id){
        $venue = Product::find($id);
        return $venue->base_layer_file;
    }


    function list(Request $request){
        $productList = Product::query()
                ->join('categorizables', function($join){
                    $join->on('products.id', '=', 'categorizables.categorizable_id');
                })
                ->join('categories', 'categories.id', '=', 'categorizables.category_id')
                ->join('companies', function($join){
                    $join->on('products.company_id', '=', 'companies.id')
                        ->where('companies.is_active', 1);
                });

        if($request->categories){
            $ids = $this->sanitizeCategories($request->categories);
            foreach($ids as $filter_id){
                $productList->whereHas('categories', function($q) use ($filter_id){
                    $q->where('categories.id', $filter_id);
                });
            }
        }


        if($request->supplier_id){
            $productList->where('companies.id', $request->supplier_id);
        }


        if($request->latitude && $request->longitude){
            $productList->addSelect(['address_distance' => function($query) use ($request){
                        $query->selectRaw("MIN(( 6367 * acos( cos( radians($request->latitude) )
                                            * cos( radians( `locations`.`latitude` ) )
                                            * cos( radians( `locations`.`longitude` ) - radians($request->longitude) )
                                            + sin( radians($request->latitude) )
                                            * sin( radians( `locations`.`latitude` ) ) )))
                                            from `locations`
                                            WHERE `products`.`company_id` = `locations`.`locationable_id`
                                            and `locations`.`locationable_type` = 'App\\\Models\\\Company'");
                    }
                ])
                ->orderBy('address_distance');

            if($request->distance != null){
                $productList->having('address_distance', '<=', $request->distance);
            }

        }

        $products = $productList
                        ->groupBy('products.id')
                        ->latest()
                        ->withCount(['hearts', 'reviews', 'shares'])
                        ->where('is_published', 1)->orderBy('name', 'ASC')->paginate(12);

        return ProductResource::collection($products);
    }

    function sanitizeCategories($categories){
        $cleanedbrackets = explode(',', str_replace(['[', ']'],'', $categories));
        $array = array();
        foreach($cleanedbrackets as $id){
            array_push($array, trim($id));
        }

        return $array;
    }

    function show(Request $request, $id){
        $product = Product::findOrFail($id);
        $product->viewed($request, '', 'mobile');
        $data = [];

        $data['product'] = new ProductResource($product);
        $data['thumbnail'] = $product->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png');
        $data['gallery'] = ImageResource::collection($product->images()->whereType('gallery')->get());
        $data['details'] = [
            'description' => $product->description,
            'event_type'  => optional($product->categories()->whereType('cater')->first())->name,
            'category'    => optional($product->categories()->whereType('product_type')->first())->name,
            'types'       => CategoryResource::collection($product->categories()->whereType('style')->get()),
        ];
        $data['reviews'] = ReviewResource::collection($product->reviews);
        return $data;
    }

    function hearted(Request $request){

        $product = Product::whereId($request->id)->first();
        if(!$product){
            return [
                'status' =>  404,
                'message' => 'No company Found'
            ];
        }
        $hearted = $product->hearts()->whereUserId($request->user()->id)->first();
        if($hearted){
            $heartedStatus = 0;
            $product->hearts()->whereUserId($request->user()->id)->first()->delete();
        }else{
            $heart = new Heart;
            $heart->user_id = $request->user()->id;
            $heart->origin = "mobile";
            $product->hearts()->save($heart);
            $heartedStatus = 1;
        }

        return [
            'status' => 200,
            'hearted' => $heartedStatus
        ];
    }

    function sendReview(Request $request){
        $product = Product::find($request->id);
        $comment = $request->comment;
        $rating = $request->rating;

        $review = new Review;
        $review->user_id = $request->user()->id;
        $review->comment = $comment;
        $review->origin = "mobile";
        $review->rating = $rating;

        $product->reviews()->save($review);

        return response()->json([
            'status'    => 200,
            'message'   => 'review created',
        ]);

    }


    function decorStatistics(Request $request){
        $company = Company::find($request->user()->company_id);
        $month = $request->month ? $request->month : date('m');
        $year = $request->year ? $request->year : date('Y');

        return [
            'month' => $month,
            'year'  => $year,
            'saved' =>  $company->decorHeartsCounts($month, $year),
            'views'  => $company->decorViewsCounts($month, $year),
            'reviews' => $company->decorReviewsCounts($month, $year),

            'stats' => [
                'saved'     => $this->dailySaves($month, $year, $company),
                'views'     => $this->dailyViews($month, $year, $company),
                'reviews'      => $this->dailyReviews($month, $year, $company)
            ]
        ];
    }

    function decorStatisticList(Request $request){
        $decors = Product::whereCompanyId($request->user()->company_id)->get();
        $month = $request->month ? $request->month : date('m');
        $year = $request->year ? $request->year : date('Y');
        $data = array();
        foreach($decors as $decor){
            $array = [
                'id' => $decor->id,
                'thumbnail' => $decor->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),
                'name'      => $decor->name,
                'saves'     => $decor->hearts()->whereMonth('created_at', $month)->whereYear('created_at', $year)->count(),//$decor->dailyHearts($month, $year),
                'views'     => $decor->views()->whereMonth('created_at', $month)->whereYear('created_at', $year)->count(),//$decor->dailyViews($month, $year),
                'reviews'    => $decor->reviews()->whereMonth('created_at', $month)->whereYear('created_at', $year)->count(),//$inspiration->dailyShares($month, $year)
            ];

            array_push($data, $array);
        }

        return [ 'list' => $data];
    }

    function decorStatisticsShow(Request $request){
        $decor = Product::find($request->id);
        $month = $request->month ? $request->month : date('m');
        $year = $request->year ? $request->year : date('Y');

        return [
            "data" => [
                'saves'     => $decor->dailyHearts($month, $year),
                'views'     => $decor->dailyViews($month, $year),
                'reviews'    => $decor->dailyReviews($month, $year),
            ]
        ];
    }


    function dailyShares($month, $year, $source){
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);

        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $date = date('Y-m-d', strtotime($year .'-' . $month . '-'. $i));
            $array = [
                'date'      => $i ."-" . $month,
                'web'       => $source->decorSharesCounts(null, null, $date, 'web'),
                'mobile'       => $source->decorSharesCounts(null, null, $date, 'mobile'),
                // 'mobile'    => $type == "post" ? $sourceData->where('origin', '')->whereMonth('created_at', $month)->whereYear('created_at', $year)->count() : 0
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }

    function dailySaves($month, $year, $source){
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);

        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $date = date('Y-m-d', strtotime($year .'-' . $month . '-'. $i));
            $array = [
                'date'      => $i ."-" . $month,
                'web'       => $source->decorHeartsCounts(null, null, $date, 'web'),
                'mobile'       => $source->decorHeartsCounts(null, null, $date, 'mobile'),
                // 'mobile'    => $type == "post" ? $sourceData->where('origin', '')->whereMonth('created_at', $month)->whereYear('created_at', $year)->count() : 0
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }

    function dailyViews($month, $year, $source){
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);

        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $date = date('Y-m-d', strtotime($year .'-' . $month . '-'. $i));
            $array = [
                'date'      => $i ."-" . $month,
                'web'       => $source->decorViewsCounts(null, null, $date, 'web'),
                'mobile'       => $source->decorViewsCounts(null, null, $date, 'mobile'),
                // 'mobile'    => $type == "post" ? $sourceData->where('origin', '')->whereMonth('created_at', $month)->whereYear('created_at', $year)->count() : 0
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }

    function dailyReviews($month, $year, $source){
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);

        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $date = date('Y-m-d', strtotime($year .'-' . $month . '-'. $i));
            $array = [
                'date'      => $i ."-" . $month,
                'web'       => $source->decorReviewsCounts(null, null, $date, 'web'),
                'mobile'       => $source->decorReviewsCounts(null, null, $date, 'mobile'),
                // 'mobile'    => $type == "post" ? $sourceData->where('origin', '')->whereMonth('created_at', $month)->whereYear('created_at', $year)->count() : 0
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }



}
