<?php

namespace App\Http\Controllers\API;

use Auth;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Firebase;
use App\Models\ActivityLog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthenticationController extends Controller
{

    function loginTest(Request $request){

    }
    
    function login(Request $request){
        
        $request->validate([
            'email'     => 'required|string|email',
            'password'  => 'required|string',
            // 'firebase'  => 'required',
            'device'    => 'required',
            // 'ios_token' => ''
        ]);

        $credentials = request(['email', 'password']);

        if(!Auth::attempt($credentials)){
            $user = User::whereEmail($request->email)->first();
            if($user){
                return response()->json([
                    'message' => 'Invalid email and password combination'
                ], 401);
            }else{
                return response()->json([
                    'message' => 'No account found'
                ], 401);
            }
        }else{
            $user = Auth::user();
            if($user->email_verified_at == "") {
                Auth::logout();
                return response()->json([
                    'message' => 'Your email is not verified yet.'
                ], 401);
            }
        }

        $user = $request->user();
        // $tokenResult = $user->createToken('Designa Password Grant Client');
        $tokenResult = $user->createToken('Designa Password Grant Client');
        $token = $tokenResult->token;
        $token->expires_at = Carbon::now()->addMinutes(5);
        

        // if ($request->remember_me){
        //     $token->expires_at = Carbon::now()->addYears(1);
        // }
            
        $token->save();

        // update or create firebase / ios id
        $request->user()->updateFirebaseId($request);

        // save activity log

        $activity = new ActivityLog;
        $activity->details = "Login in the system";
        $activity->type ="LOGIN";
        $activity->origin = "mobile";

        $request->user()->activities()->save($activity);

        return response()->json([
            'access_token'  => $tokenResult->accessToken,
            'expire'        => $tokenResult,
            'token_type'    => 'Bearer',
            'user_level'    => $user->type,
            'id'            => $user->id,
            'name'          => $user->name,
            'company_avatar'=> $user->company ? $user->company->image('logo')->path('logos', '/images/placeholders/placeholder.png') : "",
            'company_name'  => $user->company ? $user->company->name : "",
            'avatar'        =>  $user->getProfilePicture(),
            'modules'       =>  $request->user()->allPermissions()->map(function($q){
                                return $q->name;
                            }),
            'roles'         => $request->user()->company ? $request->user()->company->roles->map(function($q){
                                return [
                                        'id' => $q->id,
                                        'name' => $q->display_name
                                ];
                            }) : [],
            'expires_at'    => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);

    }


    function logout(Request $request){
        $firebase = Firebase::where('firebase_id', $request->firebase_id)->first();
        
        if($firebase){
            $firebase->delete();
        }

        if(!$request->user()){
            return response()->json([
                'message' => "Token Expired"
            ], 403); // Status code here
        }

        $token = $request->user()->token();
        $token->revoke();
    
        return response([
            'status' => 200,
            'message' => 'You have been succesfully logged out!'
        ], 200);
    }


}
