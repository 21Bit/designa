<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Facades\DB;
use Mail;
use Auth;
use Cache;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Heart;
use App\Models\Venue;
use App\Models\Review;
use App\Models\Company;
use App\Models\Product;
use App\Models\ActivityLog;
use App\Models\Inspiration;
use Illuminate\Http\Request;
use App\Mail\Dashboard\InviteMail;
use App\Http\Controllers\Controller;
use App\Http\Resources\Venue\Venue as VenueResource;
use App\Http\Resources\Review\Review as ReviewResource;
use App\Http\Resources\Company\Company as CompanyResource;
use App\Http\Resources\Product\Product as ProductResource;
use App\Http\Resources\Inspiration\Inspiration as InspirationResource;
use App\Http\Resources\Company\Search\Company as CompanySearchResource;

class CompanyController extends Controller
{
    function list(Request $request){
        return Cache::remember('api-supplier-' . json_encode($request->all()), 60, function() use($request){
            $companies = Company::where('is_active', 1);

            if($request->latitude){
                $companies->addSelect(
                        DB::raw('*, ( 6367 * acos( cos( radians(' . $request->latitude . ') )
                        * cos( radians( latitude ) )
                        * cos( radians( longitude ) - radians('. $request->longitude . ') )
                        + sin( radians('. $request->latitude .') )
                        * sin( radians( latitude ) ) ) ) AS address_distance')
                    )
                    ->whereNotNull('latitude')
                    ->whereNotNull('longitude')
                    ->orderBy('address_distance');

                if($request->distance != null){
                    $companies->having('address_distance', '<=', $request->distance);
                }
            }

            $categories = $this->sanitizeCategories($request->categories);
            if(count($categories)){
                if($categories[0] != ''){
                    $companies->where(function($q) use ($categories){
                         $q->whereHas('categories',function($q) use ($categories){
                                $q->whereIn('category_id', $categories);
                            })->orWhereHas('products', function($q) use ($categories){
                                    $q->whereHas('categories', function($q) use ($categories) {
                                        $q->whereIn('category_id', $categories);
                                    });
                            });
                    });
                }

            }

            if($request->roles){
                $roles = $this->sanitizeCategories($request->roles);
                if($roles[0] != ''){
                    $companies->whereHas('roles', function($q) use ($roles) {
                        $q->whereIn('id', $roles);
                    });
                }
            }

            if($request->min){
                $companies = $companies->get()->filter(function($q) use ($request) {
                    return $q->products()->count() >= $request->min && $q->products()->count() <= $request->max ;
                });
            }

//            $companies->whereDoesntHave('roles', function($q){
//                return $q->whereName('venues');
//            });

            return CompanyResource::collection($companies->paginate(10));
        });
    }


    function shared(Request $request){
        $supplier = Company::find($request->id);
        return $supplier->shared($request, '', 'mobile');
    }


    function featuredInspirations($id){

    }



    function show(Request $request, $id){
        $key = 'api.supplier.show.' . $id . serialize($request->all());
        return Cache::remember($key, Carbon::now()->addMinutes(15), function() use($request, $id){
            $company = Company::find($id);

            if($company){
                $company->viewed($request, '', 'mobile');
            }else{
                return abort(404);
            }

            $user = User::whereCompanyId($id)->first();
            $data = array();
            $data['company'] = [
                'id'            => $company->id,
                'name'          => $company->name,
                'logo'          => $company->image('logo')->path('logos', '/images/placeholders/placeholder.png'),
                'cover'         => $company->image('cover')->path('covers', '/images/placeholders/placeholder.png'),
                'roles_list'    => $company->roleList(), // string,
                'modules'       => $company->users->first()->allPermissions()->map(function($q){
                    return $q->name;
                }),
                'link'      => route('site.supplier.show', $company->slug),
            ];

            $data['contact_person'] = $user ?  [
                'id'        => $user->id,
                'name'      => $user->name,
                'avatar'      => $user->getProfilePicture(),
            ] : (object) [];

            $data['about'] = [
                'description'       => $company->description,
                'location'          => $company->town . ', ' . $company->country,
                'will_travel'       => $company->will_travel,
                'services'          => $company->services,
                'recognition'          => $company->recognition
            ];

            $taggedInspirations = Inspiration::whereHas('supplierRoles', function($q) use ($company){
                $q->whereCompanyId($company->id)->whereStatus('confirmed');
            })->where('is_published', 1)->get();

            $data['link']                   = route('site.supplier.show', $company->slug);
            $data['porfolios']              = Inspiration::whereCompanyId($id)->count() ? InspirationResource::collection(Inspiration::whereCompanyId($id)->orderBy('created_at', 'ASC')->get()):InspirationResource::collection(Inspiration::find([321,322])) ;
            $data['tagged_inspirations']    = count($taggedInspirations) ? InspirationResource::collection($taggedInspirations) : InspirationResource::collection(Inspiration::find([321,322])) ;
            $data['products']               = $company->products ? ProductResource::collection($company->products) : ProductResource::collection(Product::find([195,196])) ;
            $data['venues']                 = $company->venues ? VenueResource::collection($company->venues): VenueResource::collection(Venue::find([31,35]));
            $data['reviews']                = $company->reviews ? ReviewResource::collection($company->reviews) : ReviewResource::collection(Review::find([2902,2901]));
            $data['overall_rating']         = (int) $company->reviews()->average('rating');
            $data['reviewed']               = $company->reviews()->whereUserId(Auth::user()->id)->first() ? 1 : 0;
            $data['hearted']                = $company->hearts()->whereUserId(Auth::user()->id)->first() ? 1 : 0;
            $data['featured_inspirations']  = InspirationResource::collection(Inspiration::whereCompanyId($id)->where('featured', 1)->where('is_published', 1)->orderBy('created_at', 'ASC')->get());
            return $data;
        });
    }


    function hearted(Request $request){
        $company = Company::whereId($request->id)->first();
        if(!$company){
            return [
                'status' =>  404,
                'message' => 'No company Found'
            ];
        }
        $hearted = $company->hearts()->whereUserId($request->user()->id)->first();
        if($hearted){
            $heartedStatus = 0;
            $company->hearts()->whereUserId($request->user()->id)->first()->delete();
        }else{
            $heart = new Heart;
            $heart->user_id = $request->user()->id;
            $heart->origin = "mobile";
            $company->hearts()->save($heart);
            $heartedStatus = 1;
        }

        return [
            'status' => 200,
            'hearted' => $heartedStatus
        ];
    }

    function search(Request $request){

        if($request->supplier){
            $supplier =  Company::where('name', 'LIKE', '%' . $request->supplier. '%')->take(20)->groupBy('name')->get();
        }else{
            $supplier =  Company::groupBy('name')->take(20)->get();
        }

        return CompanySearchResource::collection($supplier);
    }

    function sendReview(Request $request){
        $company = Company::find($request->id);
        $comment = $request->comment;
        $rating = $request->rating;

        $review = new Review;
        $review->user_id = $request->user()->id;
        $review->comment = $comment;
        $review->origin = "mobile";
        $review->rating = $rating;

        $company->reviews()->save($review);

        return response()->json([
            'status'    => 200,
            'message'   => 'review created',
        ]);

    }


    // // For Staticstics
    // function inspirationStatisticList(Request $request){
    //     $inspirations = Inspiration::whereCompanyId($request->user()->company_id)->get();
    //     $data = array();
    //     foreach($inspirations as $inspiration){
    //         $array = [
    //             'thumbnail' => $inspiration->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),
    //             'name'      => $inspiration->name,
    //             'saves'     => random_int(1,50),
    //             'views'     => random_int(1,26),
    //             'shares'    => random_int(1,50)
    //         ];

    //         array_push($array, $data);
    //     }

    //     return $data;
    // }

    function supplierStatistics(Request $request){
        $company = Company::find($request->user()->company_id);
        $month = $request->month ? $request->month : date('m');
        $year = $request->year ? $request->year : date('Y');


        return [
            'month' => $month,
            'year'  => $year,
            'saved' =>  $company->hearts()->whereMonth('created_at',$month)->whereYear('created_at', $year)->count(),
            'reviews'   => $company->reviews()->whereMonth('created_at', $month)->whereYear('created_at', $year)->count(),
            'views'  =>$company->views()->whereMonth('created_at',$month)->whereYear('created_at', $year)->count(),

            'stats' => [
                'saved'     => $this->dailySaves($month, $year, $company),
                // 'shares'    => $this->dailyShares($month, $year, $company),
                'views'     => $this->dailyViews($month, $year, $company),
                'reviews'   => $this->dailyReviews($month, $year, $company)
            ]
        ];
    }




    function dailySaves($month, $year, $source){
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);

        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $date = date('Y-m-d', strtotime($year .'-' . $month . '-'. $i));
            $array = [
                'date'      => $i ."-" . $month,
                'web'       => $source->hearts()->whereDate('created_at', $date)->whereOrigin('web')->count(),
                'mobile'    => $source->hearts()->whereDate('created_at', $date)->whereOrigin('mobile')->count(),
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }

    function dailyViews($month, $year, $source){
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);

        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $date = date('Y-m-d', strtotime($year .'-' . $month . '-'. $i));
            $array = [
                'date'      => $i ."-" . $month,
                'web'       => $source->views()->whereDate('created_at', $date)->whereOrigin('web')->count(),
                'mobile'    => $source->views()->whereDate('created_at', $date)->whereOrigin('mobile')->count(),
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }


    function dailyReviews($month, $year, $source){
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);

        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $date = date('Y-m-d', strtotime($year .'-' . $month . '-'. $i));
            $array = [
                'date'      => $i ."-" . $month,
                'web'       => $source->reviews()->whereOrigin('web')->whereDate('created_at',$date)->count(),
                'mobile'    => $source->reviews()->whereOrigin('mobile')->whereDate('created_at',$date)->count(),
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }

}
