<?php

namespace App\Http\Controllers\API;

use Auth;
use Cache;
use App\Models\Heart;
use App\Models\Venue;
use App\Models\Review;
use App\Models\Company;
use App\Models\Inspiration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Resources\Image\Image as ImageResource;
use App\Http\Resources\Venue\Venue as VenueResource;
use App\Http\Resources\Review\Review as ReviewResource;
use App\Http\Resources\Company\Company as CompanyResource;
use App\Http\Resources\Inspiration\Inspiration as InspirationResource;
use App\Http\Resources\FunctionSpace\FunctionSpace as FunctionSpaceResource;

class VenueController extends Controller
{
    function shared(Request $request){
        $venue = Venue::find($request->id);
        return $venue->shared($request, '', 'mobile');
    }


    function baseLayer($id){
        $venue = Venue::find($id);
        return $venue->base_layer_file;
    }

    function list(Request $request){

        $venueList = Venue::query();

        if($request->categories){
            $ids = $this->sanitizeCategories($request->categories);
            foreach($ids as $id){
                $venueList->whereHas('categories', function($q) use ($id){
                    $q->where('categories.id', $id);
                });
            }
        }
        
        if($request->latitude){
            $ids =  Venue::select(
                DB::raw("*, round(
                    (
                        (
                            acos(
                                sin(( $request->latitude * pi() / 180))
                                *
                                sin(( `latitude` * pi() / 180)) + cos(( $request->latitude * pi() /180 ))
                                *
                                cos(( `latitude` * pi() / 180)) * cos((( $request->longitude - `longitude`) * pi()/180)))
                        ) * 180/pi()
                    ) * 60 * 1.1515 * 1.609344
                ) as distance"))
            ->orderBy('distance');

            if($request->distance){
                $ids->having('distance', '<=', $request->distance);
            }

            $idArray = $ids->get()->pluck('id')->toArray();
            $ids_ordered = implode(',', $idArray);
            
            $venueList->whereIn('id', $idArray)
                ->orderByRaw("FIELD(id, $ids_ordered)");
        }


        $venues =  $venueList->where('is_published', 1)->orderBy('name', 'ASC')->paginate(10);
        return VenueResource::collection($venues);

    }

    function sanitizeCategories($categories){
        $cleanedbrackets = explode(',', str_replace(['[', ']'],'', $categories));
        $array = array();
        foreach($cleanedbrackets as $id){
            array_push($array, trim($id));
        }

        return $array;
    }


    function show(Request $request, $id){
        $venue = Venue::findOrFail($id);
        $venue->viewed($request, '', 'mobile');
        $data = [];

        $data['id']                 = $venue->id;
        $data['link']               = route('site.venue.show', $venue->slug);
        $data['thumbnail']          = $venue->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png');
        $data['supplier']           = optional($venue->company)->name;
        $data['name']               = $venue->name;
        $data['type']               = $venue->categoryList('venue_type');
        $data['hearts']             = $venue->hearts()->count();
        $data['hearted']            = $venue->hearts()->whereUserId(Auth::user()->id)->first() ? 1 : 0;
        $data['reviewed']           = $venue->reviews()->whereUserId(Auth::user()->id)->first() ? 1 : 0;
        $data['shares']             = $venue->shares()->count();
        $data['rating']             = $venue->reviews()->whereUserId(Auth::user()->id)->first() ? $venue->reviews()->whereUserId(Auth::user()->id)->first()->rating : 0;
        $data['overall_rating']     = (int) $venue->reviews()->average('rating');
        $data['price']              = $venue->price;
        $data['location']           = $venue->location;
        $data['description']        = $venue->description;
        $data['type']               = $venue->categoryList('venue_type');
        $data['events']             = $venue->categoryList('cater');
        $data['catering_options']   = $venue->categoryList('catering_option');
        $data['beverage_options']   = $venue->categoryList('beverage_option');
        $data['amenities']          = $venue->categoryList('amenity');
        $data['avarage_cost']       = $venue->categoryList('average_cost_per_head');
        $data['operating_hours']    = $venue->operatingHoursList();
        $data['location']           = $venue->location;
        $data['contact_number']     = $venue->contact_number;
        $data['email']              = $venue->email;
        
        return [
            'about'                 => $data,
            'function_spaces'       => FunctionSpaceResource::collection($venue->functionspaces),
            'recent_inspiration'    => Inspiration::whereCompanyId($venue->company_id)->count() ? InspirationResource::collection(Inspiration::whereCompanyId($venue->company_id)->take(10)->get()) : InspirationResource::collection(Inspiration::find([321,322])),
            'gallery'               => ImageResource::collection($venue->images()->where('type', 'gallery')->get()),
            'reviews'               => ReviewResource::collection($venue->reviews),         
        ];
    }


    function hearted(Request $request){

        $venue = Venue::whereId($request->id)->first();
        if(!$venue){
            return [
                'status' =>  404,
                'message' => 'No Venue Found'
            ];
        }
        $hearted = $venue->hearts()->whereUserId($request->user()->id)->first();
        if($hearted){
            $heartedStatus = 0;
            $venue->hearts()->whereUserId($request->user()->id)->first()->delete();
        }else{
            $heart = new Heart;
            $heart->user_id = $request->user()->id;
            $heart->origin = "mobile";
            $venue->hearts()->save($heart);
            $heartedStatus = 1;
        }

        return [
            'status' => 200,
            'hearted' => $heartedStatus
        ];
    }

    function sendReview(Request $request){
        $venue = Venue::find($request->id);
        $comment = $request->comment;
        $rating = $request->rating;

        $review = new Review;
        $review->user_id = $request->user()->id;
        $review->comment = $comment;
        $review->origin = "mobile";
        $review->rating = $rating;

        $venue->reviews()->save($review);

        return response()->json([
            'status'    => 200,
            'message'   => 'review created',
        ]);
        
    }

    function search(Request $request){
        return Cache::remember(json_encode($request->all()), 60, function() use ($request){
            $array = [];
            $results = Venue::where('name', 'LIKE',  $request->search . '%')->where('is_published', 1)->take(8)->get();
            foreach($results as $result){
                array_push($array, [
                    'id' => $result->id,
                    'name' => $result->name . ' @ ' . $result->location
                ]);
            }

            return $array;
        });
    }

    function searchMobile(Request $request){
        $array = [];
        $results = Venue::where('name', 'LIKE', '%' . $request->search . '%')->where('is_published', 1)->take(8)->get();
        foreach($results as $result){
            array_push($array, [
                'id' => $result->id,
                'name' => $result->name
            ]);
        }

        return ['data' => $array];
    }



    function venueStatistics(Request $request){
        $company = Company::find($request->user()->company_id);
        $month = $request->month ? $request->month : date('m');
        $year = $request->year ? $request->year : date('Y');
       
        
        return [
            'month' => $month,
            'year'  => $year,
            'saved' =>  $company->venueHeartsCounts($month, $year),
            'shares' => $company->venueSharesCounts($month, $year),
            'post'   => $company->venues()->whereMonth('created_at', $month)->whereYear('created_at', $year)->count(),
            'views'  => $company->venueViewsCounts($month, $year),
            'reviews'  => $company->venueReviewsCounts($month, $year),

            'stats' => [
                'saved'     => $this->dailySaves($month, $year, $company),
                'shares'    => $this->dailyShares($month, $year, $company),
                'views'     => $this->dailyViews($month, $year, $company),
                'reviews'      => $this->dailyReviews($month, $year, $company)
            ]
        ];
    }

    function venueStatisticsShow(Request $request){
        $venue = Venue::find($request->id);
        $month = $request->month ? $request->month : date('m');
        $year = $request->year ? $request->year : date('Y');
       
        return [
            "data" => [
                    'saves'     => $venue->dailyHearts($month, $year),
                    'views'     => $venue->dailyViews($month, $year),
                    'shares'    => $venue->dailyShares($month, $year),
                    'reviews'    => $venue->dailyReviews($month, $year),
                ]
        ];
    }

    function dailyShares($month, $year, $source){  
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);  

        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $date = date('Y-m-d', strtotime($year .'-' . $month . '-'. $i));
            $array = [
                'date'      => $i ."-" . $month,
                'web'       => $source->venueSharesCounts(null, null, $date, 'web'),
                'mobile'       => $source->venueSharesCounts(null, null, $date, 'mobile'),
                // 'mobile'    => $type == "post" ? $sourceData->where('origin', '')->whereMonth('created_at', $month)->whereYear('created_at', $year)->count() : 0
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }

    function dailySaves($month, $year, $source){  
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);  

        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $date = date('Y-m-d', strtotime($year .'-' . $month . '-'. $i));
            $array = [
                'date'      => $i ."-" . $month,
                'web'       => $source->venueHeartsCounts(null, null, $date, 'web'),
                'mobile'       => $source->venueHeartsCounts(null, null, $date, 'mobile'),
                // 'mobile'    => $type == "post" ? $sourceData->where('origin', '')->whereMonth('created_at', $month)->whereYear('created_at', $year)->count() : 0
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }

    function dailyReviews($month, $year, $source){  
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);  

        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $date = date('Y-m-d', strtotime($year .'-' . $month . '-'. $i));
            $array = [
                'date'      => $i ."-" . $month,
                'web'       => $source->venueReviewsCounts(null, null, $date, 'web'),
                'mobile'       => $source->venueReviewsCounts(null, null, $date, 'mobile'),
                // 'mobile'    => $type == "post" ? $sourceData->where('origin', '')->whereMonth('created_at', $month)->whereYear('created_at', $year)->count() : 0
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }
    function dailyViews($month, $year, $source){  
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);  

        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $date = date('Y-m-d', strtotime($year .'-' . $month . '-'. $i));
            $array = [
                'date'      => $i ."-" . $month,
                'web'       => $source->venueViewsCounts(null, null, $date, 'web'),
                'mobile'       => $source->venueViewsCounts(null, null, $date, 'mobile'),
                // 'mobile'    => $type == "post" ? $sourceData->where('origin', '')->whereMonth('created_at', $month)->whereYear('created_at', $year)->count() : 0
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }


    
    function venueStatisticList(Request $request){
        $venues = Venue::whereCompanyId($request->user()->company_id)->get();
        $month = $request->month ? $request->month : date('m');
        $year = $request->year ? $request->year : date('Y');
        $data = array();
        foreach($venues as $venue){
            $array = [
                'id' => $venue->id,
                'thumbnail' => $venue->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),
                'name'      => $venue->name,
                'saves'     => $venue->hearts()->whereMonth('created_at', $month)->whereYear('created_at', $year)->count(),//$venue->dailyHearts($month, $year),
                'views'     => $venue->views()->whereMonth('created_at', $month)->whereYear('created_at', $year)->count(),//$venue->dailyViews($month, $year),
                'shares'    => $venue->shares()->whereMonth('created_at', $month)->whereYear('created_at', $year)->count(),//$inspiration->dailyShares($month, $year)
                'reviews'   => $venue->reviews()->whereMonth('created_at', $month)->whereYear('created_at', $year)->count()
            ];

            array_push($data, $array);
        }

        return [ 'list' => $data];
    }
}
