<?php

namespace App\Http\Controllers\API;

use App\Models\Inspiration;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StatisticController extends Controller
{
     // For Staticstics
     function inspirationStatisticList(Request $request){
        $inspirations = Inspiration::whereCompanyId($request->user()->company_id)->get();
        $month = $request->month ? $request->month : date('m');
        $year = $request->year ? $request->year : date('Y');
        $data = array();
        foreach($inspirations as $inspiration){
            $array = [
                'id' => $inspiration->id,
                'thumbnail' => $inspiration->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),
                'name'      => $inspiration->name,
                'saves'     => $inspiration->dailyHearts(),
                'views'     => $inspiration->dailyViews(),
                'shares'    => $inspiration->dailyShares()
            ];

            array_push($data, $array);
        }

        return [ 'list' => $data];
    }

    

    function inspirationStatistics(Request $request){
        $inspiration = Inspiration::find($request->inspiration_id);
        $month = $request->month ? $request->month : date('m');
        $year = $request->year ? $request->year : date('Y');
       
        return [
            "data" => [
                    'saves' => $this->testMonthly($month, $year),
                    'views' => $this->testMonthly($month, $year),
                    'shares' => $this->testMonthly($month, $year)
                ]
        ];
    }

    function inspirationStatistic(Request $request){
        $month = $request->month ? $request->month : date('m');
        $year = $request->year ? $request->year : date('Y');
        $inspiration = Inspiration::find($request->inspiration_id);

        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year); 
         
        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $array = [
                'date'      => $i ."-" . $month,
                'web'       => random_int(1,50),    
                'mobile'    => random_int(1,50)
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }

     function testMonthly($month, $year){

        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year); 
         
        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $array = [
                'date'      => $month ."-" . $i,
                'web'       => random_int(1,50),    
                'mobile'    => random_int(1,50)
            ];

            array_push($dateData, $array);
        }

        return $dateData;
     }

     
}
