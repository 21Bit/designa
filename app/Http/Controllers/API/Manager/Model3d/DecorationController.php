<?php

namespace App\Http\Controllers\API\Manager\Model3d;

use Auth;
use App\Models\ModelFile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ModelFile\ModelDecoration;

class DecorationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user  = Auth::user();
        return $user->model_file_list(true, 20);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return ModelDecoration
     */
    public function store(Request $request)
    {
        // for the base layer file extension should be in .dat
        $this->validate($request, [
            // 'eventspace_id' => 'required',
            'name' => 'required',
            'model_file' => 'required',
            'base_layer_id' => 'exists:model_files,id'
        ]);

        $user  = Auth::user();
        $file = $request->file('model_file');

        //Move Uploaded File
        $destinationPath = public_path('3d-models/');
        $filename = time() . '.' . $file->getClientOriginalExtension();

        $path = $file->move($destinationPath, $filename);
        $product_list = $request->product_list ?? '';
        if($request->decoration_template_id){
            $model =  $user->updateModelFile([
                'model_name'        => $request->name,
                'path'              => 'models/' . $filename,
                'type'              => 'decoration',
                'is_public'         => $request->is_public ? 1 : 0,
                'base_layer_id'     => $request->base_layer_id,
                'id'                => $request->decoration_template_id,
                'number_of_guest'   => $request->number_of_guest,
                'product_list'      => $product_list
            ]);
        }else{
            $model = $user->saveModelFile([
                'model_name'     => $request->name,
                'path'           => 'models/' . $filename,
                'type'           => 'decoration',
                'is_public'      => $request->is_public ? 1 : 0,
                'base_layer_id'  => $request->base_layer_id,
                'number_of_guest' => $request->number_of_guest,
                'product_list'      => $product_list
            ]);
        }

        return new ModelDecoration($model);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return ModelDecoration
     */
    public function show($id)
    {
        $model = ModelFile::withTrashed()->where('id', $id)->firstOrFail();
        return new ModelDecoration($model);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user  = Auth::user();

        if($request->HasFile('model_file')){
            $file = $request->file('model_file');
            $destinationPath = public_path('models/');

            $filename = time() . '.' . $file->getClientOriginalExtension();
            return $user->updateModelFile([
                'id'             => $id,
                'model_name'     => $request->name,
                'path'           => 'models/' . $filename,
                'is_public'      => $request->is_public,
                'base_layer_id'  => $request->base_layer_id,
            ]);
        }


        return $user->updateModelFile([
            'id'             => $id,
            'model_name'     => $request->name,
            'is_public'      => $request->is_public ? 1 : 0,
            'base_layer_id'  => $request->base_layer_id,
        ]);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $user = Auth::user();
        $user->deleteModel([
            'id' => $id
        ]);

        return response()->json([
            'message' => 'Item Deleted'
        ], 200);
    }
}
