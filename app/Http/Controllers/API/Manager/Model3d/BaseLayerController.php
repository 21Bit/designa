<?php

namespace App\Http\Controllers\API\Manager\Model3d;

use App\Models\ModelFile;
use Illuminate\Http\Request;
use App\Models\FunctionSpace;
use App\Http\Controllers\Controller;
use App\Http\Resources\ModelFile\BaseLayerFile;

class BaseLayerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $baseLayers = ModelFile::whereType('base_layer')->get();
        return BaseLayerFile::collection($baseLayers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return BaseLayerFile
     */
    public function store(Request $request)
    {
        // for the base layer file extension should be in .dat
        $this->validate($request, [
            'eventspace_id' => 'required',
            'base_layer' => 'required'
        ]);

        $functionSpace = FunctionSpace::findOrFail($request->eventspace_id);
        $file = $request->file('base_layer');

        //Move Uploaded File
        $filename = time() . '.' . $file->getClientOriginalExtension();
        $destinationPath = public_path('3d-models/');
        $file->move($destinationPath, $filename);

        $baseLayer = $functionSpace->base_layer;
        if($baseLayer){
            if($baseLayer->deleted_at){
                $baseLayer->restore();
            }
            
            $model = $functionSpace->updateModelFile([
                'id'                => $baseLayer->id,
//                'model_name'        => $functionSpace->name,
                'path'              => 'models/' . $filename,
                'number_of_guest'   => $request->number_of_guest,
                'is_public'         =>  $request->is_public ?? 0,
            ]);
        }else{
            $model = $functionSpace->saveModelFile([
                'model_name'        => $functionSpace->name,
                'path'              => 'models/' . $filename,
                'number_of_guest'   => $request->number_of_guest,
                'type'              => 'base_layer',
                'is_public'         =>  $request->is_public ?? 0,
            ]);
        }
        return new BaseLayerFile($model);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return void
     */
    public function show($id)
    {
        $functionSpace = FunctionSpace::findOrFail($id);

        if($functionSpace->modelFiles()->where('type', 'base_layer')->count()){
            return new BaseLayerFile($functionSpace->base_layer);
        }

        return abort(404);
    }

    /**pbas
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $baseLayer = ModelFile::find($id);
        $baseLayer->delete();

        return response()->json([
            'message' => 'Base Layer Deleted!',
            'status' => 1
        ], 200);
    }
}
