<?php

namespace App\Http\Controllers\API\Manager\Model3d;

use Str;
use Auth;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ModelFile\ProductJsonFormat;

class ProductModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $product = Product::whereIsPublished(1);

       if(strtolower($request->category)  != "draping"){
            $product->withAssetBundle();
        }

        if($request->category) {
            $product->whereHas('categories', function ($q) use ($request) {
                $q->whereType('product_type')->where('name', 'LIKE', '%' . $request->category . '%');
            });

        }

        
        if($request->subcategory) {
            $product->whereHas('categories', function ($q) use ($request) {
                $q->whereType('style')->where('name', 'LIKE', '%' . $request->subcategory . '%');
            });
        }


        return ProductJsonFormat::collection($product->paginate(25));
    }


    public function productList(Request $request)
    {
        $product = Product::whereIsPublished(1);
        if($request->category){
            $product->whereHas('categories', function($q) use ($request){
                $q->whereType('product_type')->where('name', 'LIKE', '%' . $request->category . '%');
            });
        }
         if($request->category   != "Draping"){
            $product->withAssetBundle();
        }
        if($request->subcategory){
            $product->whereHas('categories', function($q) use ($request){
                $q->whereType('style')->where('name','LIKE', '%'  . $request->subcategory . '%');
            });
        }


        return $product->get()->map(function($q){
            return [
                'product_id' => $q->id,
                'name'      => $q->name,
                'category'  => optional($q->product_type)->name,
                'subcategory' => optional($q->style)->name,
                'hasAssetbundles' => $q->asset_bundle_web ? true :  false,
            ];
        })->paginate(40);
        // return ProductJsonFormat::collection($products);
    }


    public function favorite(Request $request){
        $product = Product::whereIsPublished(1);
        if($request->category){
            $product->whereHas('categories', function($q) use ($request){
                // $categories = [str_replace(' ', '',$request->category), str_replace(' ', '',$request->category)];
                $q->whereType('product_type')->where('name', 'LIKE', '%' . $request->category . '%');
            })
            ->whereHas('categories', function($q) use ($request){
                $q->whereType('style')->where('name','LIKE', '%'  . $request->subcategory . '%');
            });
        }

        $products = $product
            ->whereHas('hearts', function($q){
                $q->where('user_id', Auth::user()->id);
            })
            ->withAssetBundle()->paginate(25);
        return ProductJsonFormat::collection($products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'product_id' => 'required',
            'asset_bundle_web' => 'required',
            'asset_bundle_android' => 'required',
            'asset_bundle_ios' => 'required',
        ]);

        $product = Product::findOrFail($request->product_id);

         // deleting old Android file
        $oldAndroid = $product->asset_bundle_android;
        if($oldAndroid){
            $oldAndroid->delete();
        }


        // deleting old web file
        $oldWeb = $product->asset_bundle_web;
        if($oldWeb){
            $oldWeb->delete();
        }


        // deleting old Ios file
        $oldIos = $product->asset_bundle_ios;
        if($oldIos){
            $oldIos->delete();
        }


        $destinationPath = public_path('3d-models/');

        $web = $request->file('asset_bundle_web');
        $android = $request->file('asset_bundle_android');
        $ios = $request->file('asset_bundle_ios');
        //Move Uploaded File

        $filenameWeb = time() . '_web.' . $web->getClientOriginalExtension();
        $filenameAndroid = time() . '_andriod.' . $android->getClientOriginalExtension();
        $filenameIos = time() . '_ios.' . $ios->getClientOriginalExtension();

        $webPath        = $web->move($destinationPath, $filenameWeb);
        $androidPath    = $android->move($destinationPath, $filenameAndroid);
        $iosPath        = $ios->move($destinationPath, $filenameIos);

        // saving web
        $product->saveModelFile([
            'model_name'        => $product->name,
            'path'              => 'models/' . $filenameWeb,
            'type'              => 'asset_bundle_web',
            'is_public'         =>  0,
        ]);

        // Saving android
        $product->saveModelFile([
            'model_name'        => $product->name,
            'path'              => 'models/' . $filenameAndroid,
            'type'              => 'asset_bundle_android',
            'is_public'         =>  0,
        ]);

        // Saving ios
        $product->saveModelFile([
            'model_name'        => $product->name,
            'path'              => 'models/' . $filenameIos,
            'type'              => 'asset_bundle_ios',
            'is_public'         =>  0,
        ]);

        return new ProductJsonFormat($product);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
