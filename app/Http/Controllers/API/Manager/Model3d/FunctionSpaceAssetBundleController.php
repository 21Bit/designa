<?php

namespace App\Http\Controllers\API\Manager\Model3d;

use Illuminate\Http\Request;
use App\Models\FunctionSpace;
use App\Http\Controllers\Controller;
use App\Http\Resources\ModelFile\AssetBundle;

class FunctionSpaceAssetBundleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return FunctionSpace::has('modelFiles')->get()->map(function($q){
            return [
                'id' => $q->id,
                'name' => $q->name
            ];
        });
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'eventspace_id' => 'required',
            'asset_bundle_web' => 'required',
            'asset_bundle_android' => 'required',
            'asset_bundle_ios' => 'required',
        ]);
        
        $functionspace = FunctionSpace::findOrFail($request->eventspace_id);
        
        $oldAndroid = $functionspace->asset_bundle_android;
        if($oldAndroid){
            $oldAndroid->delete();
        }

        $oldWeb = $functionspace->asset_bundle_web;
        if($oldWeb){
            $oldWeb->delete();
        }

        $oldIos = $functionspace->asset_bundle_ios;
        if($oldIos){
            $oldIos->delete();
        }

        
        $destinationPath = public_path('3d-models/');
        
        $web = $request->file('asset_bundle_web');
        $android = $request->file('asset_bundle_android');
        $ios = $request->file('asset_bundle_ios');
        //Move Uploaded File
        
        $filenameWeb = time() . '_web.' . $web->getClientOriginalExtension();
        $filenameAndroid = time() . '_andriod.' . $android->getClientOriginalExtension();
        $filenameIos = time() . '_ios.' . $ios->getClientOriginalExtension();

        $webPath        = $web->move($destinationPath, $filenameWeb);
        $androidPath    = $android->move($destinationPath, $filenameAndroid);
        $iosPath        = $ios->move($destinationPath, $filenameIos);

        // saving web
        $functionspace->saveModelFile([
            'model_name'        => $functionspace->name,
            'path'              => 'models/' . $filenameWeb,
            'type'              => 'asset_bundle_web',
            'is_public'         =>  0,
        ]);

        // Saving android
        $functionspace->saveModelFile([
            'model_name'        => $functionspace->name,
            'path'              => 'models/' . $filenameAndroid,
            'type'              => 'asset_bundle_android',
            'is_public'         =>  0,
        ]);

        // Saving ios
        $functionspace->saveModelFile([
            'model_name'        => $functionspace->name,
            'path'              => 'models/' . $filenameIos,
            'type'              => 'asset_bundle_ios',
            'is_public'         =>  0,
        ]);

        return response()->json([
            'data' => [
                'event_space_id'            => $request->eventspace_id,
                'Venue_AssetBundleWEB'      => url('models/' . $filenameWeb), 
                'Venue_AssetBundleAndroid'  => url('models/' . $filenameAndroid), 
                'Venue_AssetBundleIOS'      => url('models/' . $filenameIos), 
            ]
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $functionspace = FunctionSpace::findOrFail($id);
        return new AssetBundle($functionspace->asset_bundle_web);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
