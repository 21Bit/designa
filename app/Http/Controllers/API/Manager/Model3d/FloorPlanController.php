<?php

namespace App\Http\Controllers\API\Manager\Model3d;

use Auth;
use App\Http\Resources\ModelFile\ModelDecoration;
use App\Models\ModelFile;
use Illuminate\Http\Request;
use App\Models\FunctionSpace;
use App\Http\Controllers\Controller;
use App\Http\Resources\ModelFile\FloorPlanFile;

class FloorPlanController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
      */
    public function index()
    {
        $floorPlans = ModelFile::whereType('floor_plan')->get();
        return FloorPlanFile::collection($floorPlans);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return FloorPlanFile
     */
    public function store(Request $request)
    {
        // for the base layer file extension should be in .dat
        $this->validate($request, [
            'name' => 'required',
            'model_file' => 'required',
            'base_layer_id' => 'nullable|exists:model_files,id',
            'decoration_template_id' => 'nullable|exists:model_files,id'
        ]);

        $user  = Auth::user();
        $file = $request->file('model_file');

        //Move Uploaded File
        $filename = time() . '.' . $file->getClientOriginalExtension();
        $destinationPath = public_path('3d-models/');
        $file->move($destinationPath, $filename);
        $product_list = $request->product_list ?? '';

        if($request->id){
            $model =  $user->updateModelFile([
                'model_name'                => $request->name,
                'path'                      => 'models/' . $filename,
                'type'                      => 'floor_plan',
                'is_public'                 => $request->is_public ? 1 : 0,
                'base_layer_id'             => $request->base_layer_id,
                'id'                        => $request->id,
                'decoration_template_id'    => $request->decoration_template_id,
                'number_of_guest'           => $request->number_of_guest,
                'product_list'              => $product_list
            ]);
        }else{
            $model = $user->saveModelFile([
                'model_name'                => $request->name,
                'path'                      => 'models/' . $filename,
                'type'                      => 'floor_plan',
                'is_public'                 => $request->is_public ? 1 : 0,
                'base_layer_id'             => $request->base_layer_id,
                'decoration_template_id'    => $request->decoration_template_id,
                'number_of_guest'           => $request->number_of_guest,
                'product_list'              => $product_list
            ]);
        }

        return new FloorPlanFile($model);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = ModelFile::withTrashed()->where('id', $id)->firstOrFail();
        return new FloorPlanFile($model);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $baseLayer = ModelFile::find($id);
        $baseLayer->delete();

        return response()->json([
            'message' => 'Floor Plan Deleted!',
            'status' => 1
        ], 200);
    }
}
