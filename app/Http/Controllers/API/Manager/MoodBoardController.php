<?php

namespace App\Http\Controllers\API\Manager;

use App\Jobs\MakePitchPackPDFJob;
use Auth;
use Cache;
use App\Models\Image;
use App\Models\Company;
use App\Models\Product;
use App\Models\Project;
use App\Models\Inspiration;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\HasImageIntervention;
use App\Http\Resources\MoodBoard\InspirationImage;
use App\Http\Resources\MoodBoard\Project as ProjectResouce;
use App\Http\Resources\Color\Category as ColorResource;
use App\Http\Resources\MoodBoard\Product as ProductResource;
use App\Http\Resources\MoodBoard\Inspiration as InspirationResouce;

class MoodBoardController extends Controller
{

    use  HasImageIntervention;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $keyApi = 'api.event-board.list.' . Auth::user()->id;
        return  Cache::rememberForever($keyApi, function(){
            $projects = Project::whereUserId(Auth::user()->id)->orderBy('created_at', 'DESC')->paginate(15);
            return ProjectResouce::collection($projects);
        });
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    public function inspirations(Request $request){

        $inspirationsList = Inspiration::orderBy('name', 'ASC');
        $project = Project::find($request->project_id);

        if($request->saved){
            $inspirationsList->whereHas('hearts', function($q){
                $q->where('user_id', Auth::user()->id);
            });
        }else{
            $inspirationsList->doesnthave('hearts')->orWhereHas('hearts', function($q){
                $q->where('user_id', '!=',  Auth::user()->id);
            });
        }

        $event_types = $this->sanitizeCategories($request->event_types);
        $styles      = $this->sanitizeCategories($request->styles);
        $settings    = $this->sanitizeCategories($request->settings);
        $colors      = $this->sanitizeCategories($request->colors);

        if($event_types){
            $inspirationsList->whereHas('categories',function($q) use ($event_types){
                $q->whereType('cater')->whereIn('id', $event_types);
            });
        }

        // For Event Setting
        if($settings){
            $inspirationsList->whereHas('categories',function($q) use ($settings){
                $q->whereType('setting')->whereIn('categories.id', $settings);
            });
        }

        // For Event Style
        if($styles){
            $inspirationsList->whereHas('categories',function($q) use ($styles){
                $q->whereType('style')->whereIn('categories.id', $styles);
            });
        }

        // For Event Style
        if($colors){
            $inspirationsList->whereHas('categories',function($q) use ($colors){
                $q->whereType('color')->whereIn('categories.id', $colors);
            });
        }

        $inspirations =  $inspirationsList->get()->map(function($inspiration) use ($project){
            return [
                'id'                    =>  $inspiration->id,
                'name'                  =>  $inspiration->name,
                'company'               =>  $inspiration->companyName,
                'company_id'            =>  optional($inspiration->company)->id,
                'thumbnail'             =>  $inspiration->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),
                'company_thumbnail'     =>  optional($inspiration->company)->image('logo')->path('logos', '/images/placeholders/placeholder250x200.png'),
                // 'images'                =>  InspirationImage::collection($inspiration->images()->whereType('gallery')->get(), $inspiration->project),
                'images'                => $inspiration->images()->whereType('gallery')->get()->map(function($q) use ($project, $inspiration){
                                            return [
                                                'id' => $q->id,
                                                'preloader'     => imageViewer('/images/32x32/'.$q->path, '/images/placeholders/placeholder.png'),
                                                'image'         => imageViewer('/images/gallery/'. $q->path, '/images/placeholders/placeholder.png'),
                                                'selected'      =>  $project->inspirationImages()->where('image_id', $q->id)->count() ? 1 : 0
                                            ];
                                        })
                                    ];
        });

        return InspirationResouce::collection($inspirations->paginate(15));
    }


    function products(Request $request){
        $productList = Product::query();
        $project = Project::find($request->project_id);

        if($request->categories){
            $ids = $this->sanitizeCategories($request->categories);
            foreach($ids as $filter_id){
                $productList->whereHas('categories', function($q) use ($filter_id){
                    $q->where('categories.id', $filter_id);
                });
            }
        }

        if($request->saved){
            $productList->whereHas('hearts', function($q){
                $q->where('user_id', Auth::user()->id);
            });
        }else{
            $productList->doesnthave('hearts')->orWhereHas('hearts', function($q){
                $q->where('user_id', '!=',  Auth::user()->id);
            });
        }

        $products  = $productList->where('is_published', 1)->orderBy('name', 'ASC')->get()->map(function($product) use ($project){
            list($width, $height) = getimagesize(public_path($product->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png')));
                return [
                    'id'            => $product->id,
                    'name'          => $product->name,
                    'thumbnail'     =>  [
                                            'id'        => optional($product->images('thumbnail')->first())->id,
                                            'path'      => $product->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),
                                            'width'     => $width,
                                            'height'    => $height,
                                            'selected'  => $product->images()->whereType('thumbnail')->first() ? ($project->inspirationImages()->where('image_id', $product->images()->whereType('thumbnail')->first()->id)->count() ? 1 : 0) : 1
                                        ],
                    'colors'        => ColorResource::collection($product->categories()->whereType('color')->get()),
                    'product_type'  => optional($product->categories()->whereType('product_type')->first())->name,
                    'by'            => optional($product->company)->name,
                    'company_id'    => optional($product->company)->id,
                ];
        })->paginate(20);

        // $products = $productList->where('is_published', 1)->orderBy('name', 'ASC')->paginate(15);
        return ProductResource::collection($products);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'title'                     => 'required',
            'description'               => 'required',
            'colors'                    => 'required',
            'event_types'               => 'required',
            'style'                     => 'required',
            'setting'                   => 'required',
        ]);

        $project = new Project;
        $project->title = $request->title;
        $project->description = $request->description;
        $project->user_id = $request->user()->id;
        // $project->status = 'draft';
        $project->save();

        $project->categories()->whereType('color')->attach($request->colors);
        $project->categories()->whereType('cater')->attach($request->event_types);
        $project->categories()->whereType('style')->attach($request->style);
        $project->categories()->whereType('setting')->attach($request->setting);

        return response()->json([
            'data' => [
                    'status' => 1,
                    'message' => 'saving success',
                    'project'   => $project
                ]
        ], 200);
    }


    public function uploadGallery(Request $request){
        ini_set('max_execution_time', 0);
        $this->validate($request,[
            'project_id'    => ['required', 'exists:projects,id'],
            'images'        => ['array']
        ]);
        $project = Project::find($request->project_id);
        $ids = array();
        if($request->hasFile('images')){
            foreach($request->file('images') as $file){
                $gallery = $project->addImage("gallery", $file, public_path('images/gallery'));

                list($width, $height) = getimagesize(public_path('images/gallery/' . $gallery->path));
                if($width > 900){
                    $this->imageResize(public_path('images/gallery/' . $gallery->path), 900, null, true);
                }else if($height > 800){
                    $this->imageResize(public_path('images/gallery/' . $gallery->path), null, 800, true);
                }

                $this->imageResize(public_path('images/gallery/' . $gallery->path), 32, 32, false, public_path('images/32x32/' . $gallery->path));
                $project->inspirationImages()->save($gallery);
            }
        }

        return InspirationImage::collection($project->images);
    }


    function projectSuppliers(Request $request){
        $project = Project::find($request->project_id);

        $inspirations = $project->inspirationImages()->whereImagableType('App\Models\Inspiration')->groupBy('imagable_id')->pluck('imagable_id')->toArray();
        $products = $project->inspirationImages()->whereImagableType('App\Models\Product')->groupBy('imagable_id')->pluck('imagable_id')->toArray();

        $supplierEntry = [];

        $suppliers =  Company::whereHas('inspirations', function($q) use ($inspirations){
                $q->whereIn('id', $inspirations);
            })
            ->orWhereHas('products', function($q) use ($products){
                $q->whereIn('id', $products);
            })
            ->get();

        $role_counter = 0;
        $sup_counter = 0;
        foreach($suppliers as $supplier){

            $data  = [
                'id' => $supplier->id,
                'thumbnail' => $supplier->logo,
                'cover' => $supplier->image('cover')->path('covers', '/images/placeholders/placeholder250x250.png'),
                'name' => $supplier->name,
                'address' => $supplier->town . " " . $supplier->state . ' ' . $supplier->country,
                'role_list'=> $supplier->roleList(),
                'email'=> $supplier->email,
            ];

            foreach($supplier->roles as $role){

                    $rightKey = $this->searchForRole($role->display_name, $supplierEntry);

                        if($rightKey){
                            array_push($supplierEntry[$rightKey]['suppliers'], $data);
                            $role_counter++;
                        }else{
                            $sup_counter++;
                            array_push($supplierEntry, [
                                'role' => $role->display_name,
                                'suppliers' => [$data]
                            ]);
                        }
            }
        };

        dispatch(new MakePitchPackPDFJob($project));

        return [
            'data' => $supplierEntry,
        ];
    }


    function searchForRole($id, $array) {
        foreach ($array as $key => $val) {
            if ($val['role'] === $id) {
                return $key;
            }
        }
        return null;
     }




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = Project::find($id);
        return new ProjectResouce($project);
    }

    public function attachImage(Request $request){

        $project = Project::find($request->project_id);
        $currentIds = $project->inspirationImages()->pluck('images.id')->toArray();
        $ids = $request->id; // array
        $statusResult = '';

        Cache::forget('event-board.imagecount.inspiration.' . '.'. $project->id);
        Cache::forget('event-board.imagecount.product' . '.'. $project->id);
        Cache::forget('event-board.thumbnail.' . $project->id);

        if($request->status == "single"){
            if(is_array($ids)){
                $id = $ids[0];
            }else{
                $id = $ids;
            }
            if(in_array($id, $currentIds)){
                $project->inspirationImages()->detach($id);
                $statusResult = "detached";
            }else{
                $project->inspirationImages()->attach($id);
                $statusResult = "attached";
            }

        }else if($request->status == "attaching_all"){
            foreach($ids  as $id){
                if(!in_array($id, $currentIds)){
                    $project->inspirationImages()->attach($id);
                }
            }
            $statusResult = "attached_all";
        }else if($request->status == "detaching_all"){
            foreach($ids  as $id){
                if(in_array($id, $currentIds)){
                    $project->inspirationImages()->detach($id);
                }
            }

            $statusResult = "detached_all";
        }

        return response()->json([
            'data' => [
                    'status' => 1,
                    'message'  => 'update complete',
                    'type'     => $statusResult
                ]
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'                     => 'required',
            'description'               => 'required',
            'colors'                    => 'required',
            'event_types'               => 'required',
            'style'                     => 'required',
            'setting'                   => 'required',
        ]);

        $project = Project::find(id);
        $project->title = $request->title;
        $project->description = $request->description;
        // $project->status = 'draft';
        $project->save();

        $project->clearAttachCategory(['color', 'cater', 'style', 'setting']);

        $project->categories()->whereType('color')->attach($request->colors);
        $project->categories()->whereType('cater')->attach($request->event_types);
        $project->categories()->whereType('style')->attach($request->style);
        $project->categories()->whereType('setting')->attach($request->setting);

        return response()->json([
            'data' => [
                    'status' => 1,
                    'message' => 'saving success',
                    'project'   => $project
                ]
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = Project::find($id);
        $project->inspirationImages()->detach();
        $project->delete();

        return response()->json([
            'data' => [
                    'status' => 1,
                    'message' => 'project deleted',
                ]
        ], 200);
    }
}
