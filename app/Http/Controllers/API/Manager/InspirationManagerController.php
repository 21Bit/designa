<?php

namespace App\Http\Controllers\API\Manager;

use Auth;
use App\Models\Image;
use App\Models\Venue;
use App\Models\Video;
use App\Models\Category;
use App\Models\Inspiration;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\HasImageIntervention;
use App\Http\Resources\Image\Image as ImageResource;
use App\Http\Resources\Inspiration\InspirationManager as InspirationResource;

class InspirationManagerController extends Controller
{
    use HasImageIntervention;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $inspirations = Inspiration::whereCompanyId($request->user()->company_id)->orderBy('created_at', 'DESC')->paginate(15);
        return InspirationResource::collection($inspirations);
    }

    public function options()
    {
        return [
            'data' => Auth::user()->type == "supplier" ? Auth::user()->caters()->with('settings')->with('styles')->get() : Category::whereType('cater')->with('settings')->with('styles')->get(),
            'colors'    => Category::whereType('color')->orderBy("name", "ASC")->get()->map(function($q){
                return [
                    'id' => $q->id,
                    'name' => $q->name,
                    'rgb' => $q->description,
                ];
            }),
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'                      => 'required',
            'thumbnail'                 => 'required|mimes:jpeg,jpg,JPG,png,PNG|max:120000',
            'cover'                     => 'required',
            'colors'                    => 'required',
            'category'                  => 'required',
            'style'                     => 'required',
            'setting'                   => 'required',
            'description'               => 'required',
        ]);

        $inspiration = new Inspiration;
        $inspiration->company_id = Auth::user()->company_id;
        $inspiration->name = $request->name;
        $inspiration->project_year = $request->project_year;
        $inspiration->location = $request->location;
        $inspiration->description = $request->description;
        $inspiration->is_published = $request->is_published;
        $inspiration->images_by = $request->images_by;

        if($request->venue_id){
            $inspiration->venue_id = $request->venue_id;
            $venue = Venue::Find($request->venue_id);
            $inspiration->location = $venue->name ?? "";
        }

        if($request->latitude){
            $inspiration->location = $request->location;
            $inspiration->latitude = $request->latitude;
            $inspiration->longitude = $request->longitude;
        }

        $inspiration->save();

        $inspiration->categories()->whereType('color')->attach($request->colors);
        $inspiration->categories()->whereType('cater')->attach($request->category);
        $inspiration->categories()->whereType('style')->attach($request->style);
        $inspiration->categories()->whereType('setting')->attach($request->setting);

        if($request->thumbnail){
            $thumbnail = $inspiration->addImage("thumbnail", $request->thumbnail, public_path('images/thumbnails'), false, true);

            list($width, $height) = getimagesize(public_path('images/thumbnails/' . $thumbnail->path));
            if($width > 900){
                $this->imageResize(public_path('images/thumbnails/' . $thumbnail->path), 900, null, true);
            }

            $this->imageResize(public_path('images/thumbnails/' . $thumbnail->path), 32, 32, false, public_path('images/32x32/' . $thumbnail->path));
            $inspiration->images()->save($thumbnail);
        }

        if($request->cover){
            $image = $request->cover;
            $coverimage = $inspiration->addImage('cover', $request->cover, public_path('images/covers'), false, true);
            $inspiration->images()->save($coverimage);
        }

        return response()->json([
            'status' => 'success',
            'data' => [
                'inspiration' => $inspiration
            ]
        ], 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function uploadGallery(Request $request){
        ini_set('max_execution_time', 0);
        $this->validate($request, [
            'gallery' => "max:8000|mimes:jpeg,bmp,png"
        ]);

        $inspiration = Inspiration::find($request->inspiration_id);
        $ids = array();
        if($request->hasFile('images')){
            foreach($request->file('images') as $file){
                //add and upload
                $gallery = $inspiration->addImage("gallery", $file, public_path('images/gallery'));

                list($width, $height) = getimagesize(public_path('images/gallery/' . $gallery->path));
                if($width > 900){
                    $this->imageResize(public_path('images/gallery/' . $gallery->path), 900, null, true);
                }else if($height > 800){
                    $this->imageResize(public_path('images/gallery/' . $gallery->path), null, 800, true);
                }

                $this->imageResize(public_path('images/gallery/' . $gallery->path), 32, 32, false, public_path('images/32x32/' . $gallery->path));

                $inspiration->images()->save($gallery);
                array_push($ids, $gallery->id);
            }
        }

        return ImageResource::collection(Image::whereIn('id', $ids)->get());
    }


    function deleteGallery($id){
        $image = Image::find($id);

        if($image){
            // $image = $inspiration->images()->whereId($request->gallery_id)->first();
            removeImage($image->path, 'gallery');
            $image->delete();
            return response()->json([
                'message' => 'gallery deleted!'
            ], 200);
        }

        return response()->json([
            'message' => 'gallery not found'
        ], 404);
    }

    function galleries(Request $request){
        $inspiration = Inspiration::find($request->inspiration_id);
        return ImageResource::collection($inspiration->images()->whereType('gallery')->get());
    }

    public function uploadVideo(Request $request, $id){
        $this->validate($request, [
            'video' => "max:120000|mimes:mp4"
        ]);

        $inspiration = Inspiration::find($id);

        if($request->hasFile('video')){
            //add and upload
            $video = $inspiration->addVideo($request->title, "inspiration_video", $request->file('video'), public_path('videos'));
            $inspiration->videos()->save($video);
        }

        if(!$request->ajax()){
            return back();
        }
    }


    public function deleteVideo(Request $request, $id){
        $video = Video::find($id);
        removeVideo($video->path, 'videos');
        $video->delete();

        if(!$request->ajax()){
            return back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // \Log::info($request->all());
        $this->validate($request, [
            'name'                      => 'required',
            // 'thumbnail'                 => 'required|mimes:jpeg,jpg,JPG,png,PNG|max:120000',
            // 'cover'                     => 'required',
            'colors'                    => 'required',
            'category'                  => 'required',
            'style'                     => 'required',
            'setting'                   => 'required',
            'description'               => 'required',
        ]);


        $inspiration = Inspiration::find($id);
        $inspiration->name = $request->name;
        $inspiration->project_year = $request->project_year;
        $inspiration->location = $request->location;
        $inspiration->description = $request->description;
        $inspiration->is_published = $request->is_published;
        $inspiration->images_by = $request->images_by;

        if($request->venue_id){
            $inspiration->venue_id = $request->venue_id;
            $venue = Venue::Find($request->venue_id);
            $inspiration->location = $venue->name ?? "";
        }

        if($request->latitude){
            $inspiration->location = $request->location;
            $inspiration->latitude = $request->latitude;
            $inspiration->longitude = $request->longitude;
        }

        $inspiration->save();

          //clear first all the attached categories
        $inspiration->clearAttachCategory(['color', 'cater', 'style', 'setting']);

        $inspiration->categories()->whereType('color')->attach($request->colors);
        $inspiration->categories()->whereType('cater')->attach($request->category);
        $inspiration->categories()->whereType('style')->attach($request->style);
        $inspiration->categories()->whereType('setting')->attach($request->setting);

        if($request->thumbnail){
            $inspiration->clearImage('thumbnail');
            $thumbnail = $inspiration->addImage("thumbnail", $request->thumbnail, public_path('images/thumbnails'), false, true);

            list($width, $height) = getimagesize(public_path('images/thumbnails/' . $thumbnail->path));
            if($width > 900){
                $this->imageResize(public_path('images/thumbnails/' . $thumbnail->path), 900, null, true);
            }

            $this->imageResize(public_path('images/thumbnails/' . $thumbnail->path), 32, 32, false, public_path('images/32x32/' . $thumbnail->path));
            $inspiration->images()->save($thumbnail);
        }

        if($request->cover){
            $inspiration->clearImage('cover');
            $image = $request->cover;
            $coverimage = $inspiration->addImage('cover', $request->cover, public_path('images/covers'), false, true);
            $inspiration->images()->save($coverimage);
        }

        return response()->json([
            'status' => 'success',
            'data' => [
                'inspiration' => $inspiration
            ]
        ], 200);
    }

    public function changeStatus(Request $request){
        $inspiration = Inspiration::find($request->inspiration_id);
        $inspiration->is_published = $request->status;
        $inspiration->save();

         return response()->json([
            'status' => 'success',
            'data' => [
                'status' => $request->status
            ]
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $inspiration = Inspiration::find($id);

        //clear images
        $inspiration->clearImage('cover');
        $inspiration->clearImage('thumbnail');
        $inspiration->clearImage('gallery');
        $inspiration->clearVideo('inspiration_video');
        $inspiration->hearts()->delete();
        $inspiration->views()->delete();
        $inspiration->reviews()->delete();

        // inspiration
        $inspiration->clearAttachCategory(['color', 'cater', 'style', 'setting']);

        $inspiration->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'Inspiration delete success!'
        ], 200);
    }
}
