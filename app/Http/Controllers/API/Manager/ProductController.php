<?php

namespace App\Http\Controllers\API\Manager;

use Auth;
use App\Models\Image;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\HasImageIntervention;
use App\Http\Resources\Product\ProductManager;
use App\Http\Resources\Image\Image as ImageResource;

class ProductController extends Controller
{
    use HasImageIntervention;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = Product::whereCompanyId($request->user()->company_id)->orderBy('created_at', 'DESC')->paginate(15);
        return ProductManager::collection($products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          $this->validate($request, [
            'name' => 'required',
            'product_type' => 'required|integer',
            'thumbnail' => 'required',
        ]);
        
        $product = new Product;
        $product->name = $request->name;
        $product->description = $request->description;
        $product->quantity = $request->quantity;
        $product->product_identifier = $request->product_identifier;
        $product->product_size = $request->product_size;
        $product->is_published = $request->is_published ? 1 : 0;
        $product->company_id = Auth::user()->company_id;
        
        $product->save();
        
        $product->categories()->whereType('color')->attach($request->colors);
        $product->categories()->whereType('cater')->attach($request->caters);
        $product->categories()->whereType('product_type')->attach($request->product_type);
        // $product->categories()->whereType('style')->attach($request->styles);
            
        if($request->hasFile('thumbnail')){
            $thumbnail = $product->addImage("thumbnail", $request->file('thumbnail'), public_path('images/thumbnails'));
            
            // resize image if the image exceed 800px then reduce to 800 ^_^
            list($width, $height) = getimagesize(public_path('images/thumbnails/' . $thumbnail->path));

            if($width > 800){
                $this->imageResize(public_path('images/thumbnails/' . $thumbnail->path), 800, null, true);
            }

            $product->images()->save($thumbnail);
        }

        
        return new ProductManager($product);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function gallery(Request $request){
        $this->validate($request, [
            'product_id' => 'required'
        ]);
        
        $product = Product::find($request->product_id);
        return  ImageResource::collection($product->images()->whereType('gallery')->get());
    }

    public function uploadGallery(Request $request){
         $this->validate($request, [
            'images' => 'required'
        ]);
        
        $product = Product::find($request->product_id);
    
        if($request->hasFile('images')){
            $ids = array();
            foreach($request->file('images') as $file){  
                //add and upload
                $gallery = $product->addImage("gallery", $file, public_path('images/gallery'));
                
                // resize images for gallery
                list($width, $height) = getimagesize(public_path('images/gallery/' . $gallery->path));
                if($width > 800){
                    $this->imageResize(public_path('images/gallery/' . $gallery->path), 800, null, true);
                }
                
                $product->images()->save($gallery);
                array_push($ids, $gallery->id);
            }
        }

        return ImageResource::collection(Image::whereIn('id', $ids)->get());
    }

     function deleteGallery($id){
        $image = Image::find($id);
        
        if($image){
            // $image = $inspiration->images()->whereId($request->gallery_id)->first();
            removeImage($image->path, 'gallery');
            $image->delete();
            return response()->json([
                'message' => 'gallery deleted!'
            ], 200);
        }

        return response()->json([
            'message' => 'gallery not found'
        ], 404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $this->validate($request, [
            'name' => 'required',
            'product_type' => 'required|integer',
            'thumbnail' => 'nullable',
        ]);
        
        $product = Product::findOrFail($request->id);  
        $product->name = $request->name;
        $product->description = $request->description;
        $product->quantity = $request->quantity;
        $product->product_identifier = $request->product_identifier;
        $product->product_size = $request->product_size;
        $product->is_published = $request->is_published ? 1 : 0;
        $product->company_id = Auth::user()->company_id;
        
        $product->save();
        
        $product->clearAttachCategory(['cater', 'product_type' ,'color']); 
        $product->categories()->whereType('color')->attach($request->colors);
        $product->categories()->whereType('cater')->attach($request->categories);
        $product->categories()->whereType('product_type')->attach($request->product_type);
        $product->categories()->whereType('style')->attach($request->styles);
        
        
        if($request->hasFile('thumbnail')){
            $product->clearImage('thumbnail');

            $thumbnail = $product->addImage("thumbnail", $request->file('thumbnail'), public_path('images/thumbnails'));
            
            // resize image if the image exceed 800px then reduce to 800 ^_^
            list($width, $height) = getimagesize(public_path('images/thumbnails/' . $thumbnail->path));

            if($width > 800){
                $this->imageResize(public_path('images/thumbnails/' . $thumbnail->path), 800, null, true);
            }

            $product->images()->save($thumbnail);
          
        }

        return new ProductManager($product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $product = Product::find($id);

        //clearing all the images and picture
        $product->clearImage('thumbnail');
        $product->clearImage('gallery');

        $product->hearts()->delete();
        $product->views()->delete();
        $product->reviews()->delete();
        $product->shares()->delete();
        
        //clearning all the categories that attach to it
        $product->clearAttachCategory(['cater', 'product_type' ,'color']);

        //actual deletion
        $product->delete();

        return response()->json([
            'status' => 200,
            'message' => 'Product deleted!'
        ], 200);
    }
}
