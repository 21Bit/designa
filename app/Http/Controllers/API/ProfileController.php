<?php

namespace App\Http\Controllers\API;

use Auth;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Notification\Notification;

class ProfileController extends Controller
{
    function profile(){
        $data = [
            'name' => Auth::user()->name,
            'company_name' => optional(Auth::user()->company)->name,
            'town' => optional(Auth::user()->company)->town,
            'state' => optional(Auth::user()->company)->state,
            'post_code' => optional(Auth::user()->company)->post_code,
            'country' => optional(Auth::user()->company)->country,
            'telephone_number' => optional(Auth::user()->company)->telephone_number,
            'caters' => Auth::user()->company ? Auth::user()->company->categories()->whereType('cater')->pluck('id')->toArray() : null,
        ];

        return response()->json($data);

    }

    function notifications(Request $request){
        $notifications = $request->user()->notifications()->paginate(15);
        return Notification::collection($notifications);
    }

    function readNotification(Request $request){
        if($request->user()->notifications()->whereId($request->notif_id)->first()){
            $request->user()->notifications()->whereId($request->notif_id)->first()->markAsRead();
            return [
                'status'    => 200,
                'timestamp' => Carbon::now()->toDateTimeString()
            ];
        }
    }

    function updatetoken(Request $request){
        if($request->user()->updateFirebaseId($request)){
            return response()->json(['message' => 'success'], 200);
        }
    }
    

    function deleteNotifications(Request $request){
        $this->validate($request,[
            'notif_ids' => 'required'
        ]);

        $request->user()->notifications()->whereIn('id', $request->notif_ids)->delete();
        
        return [
            'status' => 200,
            'message' => 'Notifications Deleted'
        ];
    }


    function update(Request $request){
          // Verify
          $this->validate($request, [
            "name" =>  "required|string",
            // "company_name" =>  "required|string",
            // "town" =>  "required",
            // "state" =>  "required",
            // "post_code" =>  "required",
            // "telephone_number" =>  "required",
            // "password" =>  "confirmed",
        ]);
        
        if($request->company_name){
            $company = Company::find($request->user()->company_id);
            $company->name = $request->company_name;
            // $company->description = $request->description;

            $company->town = $request->town;
            $company->state = $request->state;
            $company->country = $request->country;
            $company->post_code = $request->post_code;

            $company->telephone_number = $request->telephone_number;

            $company->save();
            $company->categories()->sync($request->caters);
        }
        
        $user = User::find($request->user()->id);
        $user->name = $request->name;
        $user->save();

       
        if($request->picture){
            $image = $request->picture;
            $user->uploadPicture($request, true, $image);
        }
        
        $updatedUser = User::find($user->id);

        return response()->json([
            'status' => 200,
            'avatar' =>  $updatedUser->getProfilePicture(),
            'message' => 'Profile Update Success'
        ]);
    }


}
