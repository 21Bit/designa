<?php

namespace App\Http\Controllers\API;

use Cache;
use App\Models\Role;
use App\Models\Venue;
use App\Models\Company;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Role\RoleCollection;
use App\Http\Resources\Cater\CategoryCollection as CaterCategories;
use App\Http\Resources\Color\CategoryCollection as ColorCategories;
use App\Http\Resources\Category\CategoryCollection as CategoryResource;

class CategoryController extends Controller
{

    function venueCategories(){
        return Cache::remember('venue-categories', 60 * 60, function() {
            $data = [];
            $data['events'] = new CategoryResource(Category::whereType('cater')->get());
            $data['venue_types'] = new CategoryResource(Category::whereType('venue_type')->get());
            $data['catering_options'] = new CategoryResource(Category::whereType('catering_option')->get());
            $data['beverage_options'] = new CategoryResource(Category::whereType('beverage_option')->get());
            $data['amenities'] = new CategoryResource(Category::whereType('amenity')->get());
            $data['cost_per_head'] = new CategoryResource(Category::whereType('average_cost_per_head')->get());
            $data['colors'] = new ColorCategories(Category::whereType('color')->get());
            $data['capacity'] = Venue::orderBy('capacity', 'DESC')->first()->capacity;
            return $data;
        });
    }

    public function category(Request $request){
        return Cache::remember('categories-' . json_encode($request->all()), 60 * 24, function() use($request){
            $data = array();
            $categories = Category::whereType($request->type)->orderBy("name", "ASC")->get();
            foreach($categories as $category){
                if($request->with_image && $request->credits == null){
                    $arrayData = array(
                        'id' => $category->id,
                        'name' => $category->name,
                        'image' => imagePreview('/mobile/images/'.  \Str::plural(strtolower(str_replace(' ','_', $request->type))) .'/' . \Str::snake(str_replace([' ', '/'],'', $category->name)) . ".jpg",'/images/placeholders/placeholder.png')  
                        // 'image' => url('/mobile/images/' . \Str::plural($request->type) . '/' . \Str::snake(str_replace([' ','/'],'', $category->name)) . ".jpg")
                    );
                }else if($request->credits && $request->with_image){
                    $arrayData = array(
                        'id' => $category->id,
                        'name' => $category->name,
                        'image' => imagePreview('/mobile/images/'.  \Str::plural(strtolower(str_replace(' ','_', $request->type))) .'/' . \Str::snake(str_replace([' ', '/'],'', $category->name)) . ".jpg",'/images/placeholders/placeholder.png'),  
                        'credit_by' => $category->credit_by,
                        'credit_location' => $category->credit_location,
                    );
                }else if($request->with_settings){
                    $arrayData = array(
                        'id' => $category->id,
                        'name' => $category->name,  
                        'styles' => $category->styles->map(function($q){
                            return [
                                'id' => $q->id,
                                'name' => $q->name
                            ];
                        }),  
                    );
                }else{
                    if($request->type == 'color'){
                        $arrayData = array(
                            'id' => $category->id,
                            'name' => $category->name,  
                            'rgb' => $category->description,  
                        );
                    }else{
                        $arrayData = array(
                            'id' => $category->id,
                            'name' => $category->name,  
                        );
                    }
                
                }

                array_push($data, $arrayData);
            }

            return ['list' => 
                $data,
            ];  
        });
        
    }


    public function roles(Request $request){
        $data = array();
        if($request->except){
            $roles = Role::where('name', '!=', $request->except)->get();
        }else{
            $roles = Role::all();
        }
        foreach($roles as $role){
        
            if($request->with_image){
                $arrayData = array(
                    'id' => $role->id,
                    'name' => $role->display_name,  
                    'credit_by' => $role->credit_by,  
                    'credit_location' => $role->credit_location,  
                    'image' => imagePreview('/mobile/images/roles/' . \Str::snake(str_replace([' '],'', $role->display_name)) . ".jpg",'/images/placeholders/placeholder.png')
                );
            }else{
                $arrayData = array(
                    'id' => $role->id,
                    'name' => $role->display_name,  
                    'credit_by' => $role->credit_by,  
                    'credit_location' => $role->credit_location,   
                );
            }

            array_push($data, $arrayData);
        }

        return ['list' => 
            Cache::remember('categories-' . json_encode($request->all()), 60 * 60, function() use($data){
                return $data;
            })
        ];
    }



    function inspirationCategories(Request $request){
        $data = [];
        $data['events'] = new CaterCategories(Category::whereType('cater')->get());
        $data['colors'] = new ColorCategories(Category::whereType('color')->get());
        return $data;
    }

    function productCategories(Request $request){
        $data = [];
        $data['events'] = new CategoryResource(Category::whereType('cater')->get());
        $data['product_types'] = new CategoryResource(Category::whereType('product_type')->get());
        $data['companies'] = new CategoryResource(Company::all());
        $data['colors'] = new ColorCategories(Category::whereType('color')->get());
        return $data;
    }

    function supplierCategories(Request $request){
        $data = [];
        
        $productcompany = Company::all()->sortByDesc(function($e){
            return $e->products;
        })->first();

        $data['events'] = new CategoryResource(Category::whereType('cater')->get());
        $data['categories'] = new RoleCollection(Role::orderBy('display_name')->get());
        $data['product_types'] = new CategoryResource(Category::whereType('product_type')->get());
        $data['colors'] = new ColorCategories(Category::whereType('color')->get());
        $data['max_stocks'] = $productcompany ? $productcompany->count() : 0;
        return $data;
    }

    function stocksRange(){
        return ['min'=> 1, 'max' => 10];
    }

    

}
