<?php

namespace App\Http\Controllers\API;

use Auth;
use App\Models\Company;
use App\Models\Inspiration;
use Illuminate\Http\Request;
use App\Models\SupplierInspiration;
use App\Http\Controllers\Controller;

class InspirationTagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $inspiration = Inspiration::find($request->inspiration_id);
        $suppliers = $inspiration->supplierRoles()->has('role')->get()->map(function($q){
                if($q->company){
                    return [
                        'id' => $q->id,
                        'role' => $q->role->display_name,
                        'company' => optional($q->company)->name,
                        'thumbnail' => optional($q->company)->image('logo')->path('logos', '/images/placeholders/company-logo-32x32.png'),
                        'status' => $q->status,
                        'request_by' => $q->request_by
                    ];
                }else{
                    return [
                        'id' => $q->id,
                        'role' => optional($q->role)->display_name,
                        'company' => $q->name,
                        'thumbnail' => "",
                        'status' => $q->status,
                        'request_by' => ''
                    ];    
                }
            });
        return [
            'data' => $suppliers
        ];
    }


      public function roles(Request $request){
        $data =  Company::where('name', 'LIKE', '%' . $request->key . '%')->whereHas('roles', function($q) use ($request) {
                 $q->where('id', $request->role);
            })->get()->map(function($q){
                return [
                    'id' => $q->id,
                    'name' => $q->name,
                ];
            }) ?? [];

        return [
            'data' => $data
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $company = Company::find($request->supplier_id);
        $inspiration = Inspiration::find($request->inspiration_id);
        $owner = Company::find($request->supplier_id);

        $supplier = new SupplierInspiration;
        $supplier->role_id = $request->role_id;
        $supplier->company_id = $request->supplier_id; // this might optional
        $supplier->request_by = Auth::user()->company_id; // this might optional
        $supplier->name = $request->supplier_name; // this might optional

        if($request->supplier_id == Auth::user()->company_id){
            $supplier->status = 'confirmed';
        }else{
            $supplier->status =  "pending";
        }

        $supplier->inspiration_id = $request->inspiration_id;
        $supplier->save();

        $data =  [
                        'id'        => $supplier->id,
                        'role'      => $supplier->role->display_name,
                        'company'   => optional($supplier->company)->name,
                        'thumbnail' => optional($supplier->company)->image('logo')->path('logos', '/images/placeholders/company-logo-32x32.png'),
                        'status'    => $supplier->status,
                        'request_by' => $supplier->request_by
                    ];

        return [
            'data' => $data
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tag = SupplierInspiration::find($id);
        $tag->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'tag deleted!'
        ], 200);

    }
}
