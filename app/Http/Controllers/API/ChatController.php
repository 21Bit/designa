<?php

namespace App\Http\Controllers\API;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Company;
use App\Facades\FirebaseFMC;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChatController extends Controller
{

    /*
        * Firebase Notification   
        * Notify a user if someone is initiated a new Chat conversation
    */
    function newChat(Request $request){
        $sender = User::find($request->user_id);
        $contact_person = User::find($request->contact_person_id);
        $firebaseArray = array();

        foreach($contact_person->firebases as $firebaseAccount){
            array_push($firebaseArray, $firebaseAccount->firebase_id);
        }

        $firebase = new FirebaseFMC;
        $firebase->sendSingle(
                $firebaseArray, 
                array(
                    'title'         => $sender->company ? $sender->company->name : $sender->name . " is requesting a conversation",//$inspiration->title,  // title
                    'sender_id'     => $sender->id,
                    'sender_type'   => $sender->type,
                    'sender_name'   => $sender->company ? $sender->company->name : $sender->name ,
                    'avatar'        => $sender->company ? $sender->company->image('logo')->path('logos', '/images/placeholders/placeholder.png') :$sender->getProfilePicture(), //$inspiration->description,  // message,
                    'message'       => '',
                    'channel_id'    => $request->channel_id,
                    'notif_type'    => 'new_chat',
                    'item_id'       => '',
                    'link'          => '',
                    'image'         => '',  // image
                    'timestamp'     => Carbon::now()->toDateTimeString() // timestamp
                )
            );

        return [
            'status'     =>  200,
            'message'   =>  'success',
        ];
    }

    /*
        * Firebase Notification   
        * Notify a user if someone is send a new Chat
    */
    function newMessage(Request $request){
        $user = User::find($request->sender_id);
        $receiver = User::find($request->receiver_id);

        $firebaseArray = array();
        foreach($receiver->firebases as $firebaseAccount){
            array_push($firebaseArray, $firebaseAccount->firebase_id);
        }
    
        $firebase = new FirebaseFMC;
        $firebase->sendSingle(
                $firebaseArray, 
                array(
                    'title'         => $user->company ? $user->company->name : $user->name . " sent you a message",//$inspiration->title,  // title
                    'sender_id'     => $user->id,
                    'sender_type'   => $user->type,
                    'sender_name'   => $user->company ? $user->company->name : $user->name ,
                    'avatar'        => $user->company ? $user->company->image('logo')->path('logos', '/images/placeholders/placeholder.png') :$user->getProfilePicture(), //$inspiration->description,  // message,
                    'message'       => $request->message, // chat message of the sender
                    'channel_id'    => $request->channel_id,
                    'notif_type'    => 'new_message',
                    'item_id'       => '',
                    'link'          => '',
                    'image'         => '',  
                    'timestamp'     => Carbon::now()->toDateTimeString() // timestamp
                )
            );

        return [
                'status'     =>  200,
                'message'   =>  'success', 
        ];
    }



    function searchSupplier(Request $request){
        $companies = Company::where('name', 'LIKE', '%' . $request->key . '%')->get();
        $data = array();
        
        foreach($companies as $company){
            $user = User::whereCompanyId($company->id)->first();
            $com = [
                'id'        => $company->id,
                'name'      => $company->name,
                'logo'      => $company->image('logo')->path('logos', '/images/placeholders/placeholder.png'),
                'roles_list'=> $company->roleList(), // string,
                'contact_person' => $user ?  [
                    'id'        => $user->id,
                    'name'      => $user->name,
                    'avatar'      => $user->getProfilePicture(),
                    ] : (object) []
                ];
            array_push($data, $com);
        }

        return ['data' => $data];
    }
}
