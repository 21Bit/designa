<?php

namespace App\Http\Controllers\API;

use App\Models\User;
use App\Models\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    function registerSupplier(Request $request){
         $validator = Validator::make($request->all(), [
            "name" =>  "required|string",
            "email" =>  "email|required|unique:users,email",
            "company_name" =>  "required|string",
            "role" =>  "required",
            "town" =>  "required",
            "state" =>  "required",
            "post_code" =>  "required",
            "telephone_number" =>  "required",
            // 'firebase'  => 'required',
            'device'    => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                    'message' => $validator->errors()->all()[0],
                    'errors' => $validator->errors()
            ], 422);
        }


        $company = new Company;
        $company->name = $request->company_name;
        // $company->description = $request->description;

        $company->town = $request->town;
        $company->state = $request->state;
        $company->country = $request->country;
        $company->post_code = $request->post_code;

        $company->email = $request->email;
        $company->telephone_number = $request->telephone_number;

        $company->save();
        $company->categories()->sync($request->caters);
        $company->roles()->sync([$request->role]);

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->type = "supplier";
        $user->company_id = $company->id;
        $user->save();
        $user->sendEmailVerificationNotification();
        $user->createAsStripeCustomer();

        $company->asyncRoleToUsers();
        // create firebase id
        $user->updateFirebaseId($request);
        $user->sendEmailVerificationNotification();

        return response()->json([
            'status'  => 200,
            'message' => 'Registration Success',
            'link'    => route('site.supplier.show', $company->slug)
        ]);

    }


    function registerCustomer(Request $request){
        $validator = Validator::make($request->all(), [
            "name" =>  "required|string",
            "email" =>  "email|required|unique:users,email",
            'device'    => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                    'message' => $validator->errors()->all()[0],
                    'errors' => $validator->errors()
            ], 422);
        }

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->type = "customer";
        $user->save();

        $user->sendEmailVerificationNotification();

        if($request->picture){
            $image = $request->picture;
            $user->uploadPicture($request, true, $image);
        }

        // create firebase id
        User::whereId($user->id)->first()->updateFirebaseId($request);

        return response()->json([
            'status' => 200,
            'message' => 'Registration Success'
        ]);

    }
}
