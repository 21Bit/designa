<?php

namespace App\Http\Controllers\API;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\SupplierInspiration;
use App\Http\Controllers\Controller;
use App\Http\Resources\Inspiration\SupplierInspiration as SupplierInspirationResource;

class TaggedInspirationController extends Controller
{
    function pending(Request $request){
        $data = SupplierInspiration::whereCompanyId($request->user()->company_id)->where('status', '!=', 'confirmed')->where('status', '!=', 'rejected')->paginate(15);
        return SupplierInspirationResource::collection($data);
    }

    function confirmed(Request $request){
        $data = SupplierInspiration::whereCompanyId($request->user()->company_id)->where('status', 'confirmed')->paginate(15);
        return SupplierInspirationResource::collection($data);
    }

    function rejected(Request $request){
        $data = SupplierInspiration::whereCompanyId($request->user()->company_id)->where('status', 'rejected')->paginate(15);
        return SupplierInspirationResource::collection($data);
    }


    function respond(Request $request){

        $tagged = SupplierInspiration::find($request->id);
        $tagged->status = $request->status;
        $tagged->save();

        return response()->json([
            'message' => 'success',
            'tagged_status' => $request->status, // confirmed // rejected 
            'status'  => 200
        ],200);
    }

    function productOptions(Request $request){
        $tag = SupplierInspiration::find($request->id);
        $inspiration = $tag->inspiration;

        $products = Product::whereCompanyId($request->user()->company_id)->get();
        // $products = Product::whereCompanyId($request->user()->company_id)->sortBy(function($q){
        //     return $q->products()
        // });

        $array = array();
        foreach($products as $product){
            array_push($array, [
                'id'            => $product->id,
                'name'          => $product->name,
                'thumbnail'     => $product->image('thumbnail')->path('thumbnails',''),
                'selected'      => in_array($product->id, $inspiration->products()->pluck('products.id')->toArray()) ? true : false
            ]);
        }

        return [
            'data' => $array
        ];

    }


    function productSelected(Request $request){
        $tag = SupplierInspiration::find($request->id);
        $inspiration = $tag->inspiration;

        $array = array();
        foreach($inspiration->products()->where('company_id', $request->user()->company_id)->get() as $product){
            array_push($array, [
                'id'            => $product->id,
                'name'          => $product->name,
                'thumbnail'     => $product->image('thumbnail')->path('thumbnails',''),
                'selected'      => in_array($product->id, $inspiration->products()->pluck('products.id')->toArray())
            ]);
        }

       return [
            'data' => $array
        ];
    }


     function updateProduct(Request $request){

        $this->validate($request, [
            'products' => 'required',
            'id'        => 'required'
        ]);

        $tag = SupplierInspiration::find($request->id);
        $inspiration = $tag->inspiration;

        //clearing first all attach products
        $relatedProducts = $inspiration->products()->whereCompanyId($request->user()->company_id)->get();
        $nonRelatedProducts = $inspiration->products()->where('company_id', '!=', $request->user()->company_id)->get();

        foreach($relatedProducts as $related){
            $inspiration->products()->detach($related->id);
        }
        // $inspiration->products()->whereCompanyId($request->user()->company_id)->dettach();

        // actual adding of product in inspiration
        foreach($request->products as $product){
            $inspiration->products()->attach($product);
        }

        foreach($nonRelatedProducts as $nonRelated){
            $inspiration->products()->attach($nonRelated->id);
        }

        return  response()->json([
            'message' => "success"
        ], 200);
    }

    // function removeProduct(Request $request, $id, $product){
    //     $inspiration = Inspiration::find($id);
    //     if($request->product){
    //         $inspiration->products()->detach($product);
    //     }

    //     return back();
    // }

}



