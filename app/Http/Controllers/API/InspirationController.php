<?php

namespace App\Http\Controllers\API;

use DB;
use Cache;
use Carbon\Carbon;
use App\Models\Heart;
use App\Models\Review;
use App\Models\Company;
use App\Models\Inspiration;
use App\Facades\FirebaseFMC;
use Illuminate\Http\Request;
use App\Models\SupplierInspiration;
use App\Http\Controllers\Controller;
use App\Notifications\Supplier\Tag\ReverseTagNotification;
use App\Http\Resources\Inspiration\Inspiration as InspirationResource;
use App\Http\Resources\Inspiration\Location\Inspiration as InspirationLocationsResource;

class InspirationController extends Controller
{


    function list(Request $request){
        $key = 'api.inspiration.' . serialize($request->all());
        return Cache::remember($key, Carbon::now()->addMinutes(2), function() use($request){
            $event_types = $this->sanitizeCategories($request->event_types);
            $styles      = $this->sanitizeCategories($request->styles);
            $settings    = $this->sanitizeCategories($request->settings);
            $colors      = $this->sanitizeCategories($request->colors);
            
            $list = Inspiration::query();
            
            
            if($request->latitude && $request->longitude){
                $list->addSelect(['address_distance' => function($query) use ($request){
                            $query->selectRaw("( 6367 * acos( cos( radians($request->latitude) ) 
                                * cos( radians( inspirations.latitude ) ) 
                                * cos( radians( inspirations.longitude ) - radians($request->longitude) ) 
                                + sin( radians($request->latitude) ) 
                                * sin( radians( inspirations.latitude ) ) ))");
                        }
                    ])
                    ->whereNotNull('latitude')
                    ->whereNotNull('longitude')
                    ->orderBy('address_distance');

                if($request->distance != null){
                    $list->having('address_distance', '<=', $request->distance);
                }
            }

                
            if($event_types){
                $list->whereHas('categories',function($q) use ($event_types){
                    $q->whereType('cater')->whereIn('id', $event_types);
                }); 
            }
            
            if($settings){
                $list->whereHas('categories',function($q) use ($settings){
                    $q->whereType('setting')->whereIn('categories.id', $settings);
                });
            }
            
            if($styles){
                $list->whereHas('categories',function($q) use ($styles){
                    $q->whereType('style')->whereIn('categories.id', $styles);
                });
            }   

            if($colors){
                $list->whereHas('categories',function($q) use ($colors){
                    $q->whereType('style')->whereIn('categories.id', $colors);
                });
            }   

      
            if($request->supplier){
                $list->whereHas('company', function($q) use ($request){
                    $q->where('name', 'LIKE', '%' . $request->supplier . '%');
                });
            }

            $inspirations = $list->WithActiveCompany()->where('is_published', 1)->orderBy('created_at', 'DESC')->paginate(10);
            return InspirationResource::collection($inspirations);
        });
        
    }

    

    function sanitizeCategories($categories){
        if($categories){

            $cleanedbrackets = explode(',', str_replace(['[', ']'],'', $categories));
            
            if(!$cleanedbrackets){
                return [];
            }
            
            $array = array();
            foreach($cleanedbrackets as $id){
                array_push($array, trim($id));
            }
            
            return $array;
        }else{
            return [];
        }
    }

    function viewed(Request $request){
        $inspiration = Inspiration::find($request->inspiration_id);
        return $inspiration->viewed($request, '', 'mobile');
    }

    function shared(Request $request){
        $inspiration = Inspiration::find($request->id);
        return $inspiration->shared($request, '', 'mobile');
    }


    function requestTag(Request $request){

        if(SupplierInspiration::where('company_id', $request->user()->company_id)->where("inspiration_id", $request->inspiration_id)->where('role_id', $request->role_id)->count()){
               
            return response()->json([
                'status' => 200,
                'message' => 'Exist'
            ],200);

        }else{
                // $company = Company::find($request->user()->company_id);
                // $inspiration = Inspiration::find($request->inspiration_id);
                // $role = Role::find($request->role_id);

                $supplier = new SupplierInspiration;
                $supplier->role_id = $request->role_id;
                $supplier->inspiration_id = $request->inspiration_id;
                $supplier->request_by = $request->user()->company_id; // this might optional
                $supplier->company_id = $request->user()->company_id; // this might optional
                $supplier->name = $request->user()->company->supplier; // this might optional
                $supplier->status = 'pending';
                $supplier->save();

                return response()->json([
                        'status' => 200,
                        'message' => 'Request Sent!'
                    ],200 
                );

        }
    }


    function hearted(Request $request){

        $inspiration = Inspiration::find($request->id);
        $supplier  = Company::find($inspiration->company_id);
        $hearted = $inspiration->hearts()->whereUserId($request->user()->id)->first();
        if($hearted){
            $heartedStatus = 0;
            $inspiration->hearts()->whereUserId($request->user()->id)->first()->delete();
        }else{
            $heart = new Heart;
            $heart->user_id = $request->user()->id;
            $heart->origin = "mobile";
            $inspiration->hearts()->save($heart);
            $heartedStatus = 1;
        }

        return [
            'status' => 200,
            'hearted' => $heartedStatus
        ];
    }


    function inspirationLocations(Request $request){
        if($request->location){
            $inspirations =  Inspiration::where('location', 'LIKE', '%' . $request->location. '%')->take(20)->groupBy('location')->get();  
        }else{
            $inspirations =  Inspiration::groupBy('location')->take(20)->get();    
        
        }

        return InspirationLocationsResource::collection($inspirations);
       
    }


    function inspirationStatisticList(Request $request){
        $inspirations = Inspiration::whereCompanyId($request->user()->company_id)->get();
        $month = $request->month ? $request->month : date('m');
        $year = $request->year ? $request->year : date('Y');
        $data = array();
        foreach($inspirations as $inspiration){
            $array = [
                'id' => $inspiration->id,
                'thumbnail' => $inspiration->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),
                'name'      => $inspiration->name,
                'saves'     => $inspiration->hearts()->whereMonth('created_at', $month)->whereYear('created_at', $year)->count(),//$inspiration->dailyHearts($month, $year),
                'views'     => $inspiration->views()->whereMonth('created_at', $month)->whereYear('created_at', $year)->count(),//$inspiration->dailyViews($month, $year),
                'shares'    => $inspiration->shares()->whereMonth('created_at', $month)->whereYear('created_at', $year)->count(),//$inspiration->dailyShares($month, $year)
            ];

            array_push($data, $array);
        }

        return [ 'list' => $data];
    }

    function inspirationStatisticsShow(Request $request){
        $inspiration = Inspiration::find($request->id);
        $month = $request->month ? $request->month : date('m');
        $year = $request->year ? $request->year : date('Y');
       
        return [
            "data" => [
                    'saves'     => $inspiration->dailyHearts($month, $year),
                    'views'     => $inspiration->dailyViews($month, $year),
                    'shares'    => $inspiration->dailyShares($month, $year),
                    // 'posts'     => $inspiration->dailyInspirations($month,$year)
                ]
        ];
    }


    function inspirationStatistics(Request $request){
        $company = Company::find($request->user()->company_id);
        $month = $request->month ? $request->month : date('m');
        $year = $request->year ? $request->year : date('Y');
       
        
        return [
            'month' => $month,
            'year'  => $year,
            'saved' =>  $company->inspirationHeartsCounts($month, $year),
            'shares' => $company->inspirationSharesCounts($month, $year),
            'post'   => $company->inspirations()->whereMonth('created_at', $month)->whereYear('created_at', $year)->count(),
            'views'  => $company->inspirationViewsCounts($month, $year),

            'stats' => [
                'saved'     => $this->dailySave($month, $year, $company),
                'shares'    => $this->dailyShares($month, $year, $company),
                'views'     => $this->dailyViews($month, $year, $company),
                'post'      => $this->dailyInspirations($month, $year, $company)
            ]
        ];
    }
    



    function dailyShares($month, $year, $source){  
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);  

        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $date = date('Y-m-d', strtotime($year .'-' . $month . '-'. $i));
            $array = [
                'date'      => $i ."-" . $month,
                'web'       => $source->inspirationSharesCounts(null, null, $date, 'web'),
                'mobile'       => $source->inspirationSharesCounts(null, null, $date, 'mobile'),
                // 'mobile'    => $type == "post" ? $sourceData->where('origin', '')->whereMonth('created_at', $month)->whereYear('created_at', $year)->count() : 0
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }

    function dailySave($month, $year, $source){  
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);  
        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $date = date('Y-m-d', strtotime($year .'-' . $month . '-'. $i));
            $array = [
                'date'      => $i ."-" . $month,
                'web'       => $source->inspirationHeartsCounts(null, null, $date, 'web'),
                'mobile'       => $source->inspirationHeartsCounts(null, null, $date, 'mobile'),
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }
    
    function dailyViews($month, $year, $source){  
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);  
        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $array = [
                'date'      => $i ."-" . $month,
                'web'       => $source->inspirationViewsCounts($month, $year, $year .'-' . $month . '-'. $i, 'web'),
                'mobile'       => $source->inspirationViewsCounts($month, $year, $year .'-' . $month . '-'. $i, 'mobile'),
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }



    function dailyInspirations($month, $year, $source){  
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);  
        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $array = [
                'date'      => $i ."-" . $month,
                'web'       => $source->inspirations()->whereDate('created_at', $year .'-' . $month . '-'. $i)->count(),
                'mobile'    => 0
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }


    

    function inspirationStatistic(Request $request){
        $company = Company::find($request->user()->company_id);

        $month = $request->month ? $request->month : date('m');
        $year = $request->year ? $request->year : date('Y');
        $yearMonth = $year . '-' . $month;

        return [
            'month' => $month,
            'year'  => $year,
            'saved' => $company->inspirations()->whereHas('hearts', function($q) use ($year, $month){
                        $q->whereMonth('created_at', $month);
                        $q->whereYear('created_at', $year);
                    })->count(),
            'shares' => random_int(1,55),
            'posts'   => $company->inspirations()->whereMonth('created_at', $month)->count(),
            'views'  => $company->inspirations()->whereHas('views', function($q) use ($year, $month){
                        $q->whereMonth('created_at', $month);
                        $q->whereYear('created_at', $year);
                    })->count(),

            'stats' => [
                'saved'     => '',
                'shares'    => '',
                'views'     => '',
                'posts'      => ''
            ]
        ];
    }

    function getSavedPerDay(Company $company, $type, $month, $year){
   

        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);
      
        $dateData = array();
        for($i=1; $numberOrDays + 1 > $i; $i++){
            $array = [
                'date'      => $i ."-" . $month,
                'web'       => random_int(1,50),
                'mobile'    => random_int(1,50)
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }

    function getStats(Company $company, $type, $for, $month, $year){
        if($type == "inspiration"){
            $items = $company->inspiration;
        }

        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);

        $dateData = array();
        for($i=1; $numberOrDays + 1 > $i; $i++){
            $array = [
                'date'      => $i ."-" . $month,
                'web'       => $items->where,
                'mobile'    => ''
            ];

            array_push($dateData, $array);
        }

        return $dateData;
        
    }
}
