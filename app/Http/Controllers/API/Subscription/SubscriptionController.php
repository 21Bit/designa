<?php

namespace App\Http\Controllers\API\Subscription;

use Auth;
use App\Models\Access;
use Stripe\StripeClient;
use Illuminate\Http\Request;
use App\Models\SubscriptionPlan;
use App\Http\Controllers\Controller;
use App\Services\SubscriptionService;

class SubscriptionController extends Controller
{
    protected $stripe;

    public function __construct()
    {
        $this->stripe = new StripeClient(env('STRIPE_SECRET'));
    }


    public function index(Request $request){
//        return Auth::user()->subscriptions()->latest()->first();
        $user = $request->user();
        if($user->subscribed('Designa Plan')){
            $plan = Auth::user()->current_plan;
        }else{
            $plan = SubscriptionPlan::where('name', 'Beginner')->first();
        }

        $accesses = $plan->accesses;
        $accessData = array();

        foreach($accesses as $access){
            $name = \Str::camel($access->display_name);
            array_push($accessData, $access->display_name);
        }

        return [
            'plan' => $plan->name,
            'price' => $plan->price,
            'ends_at' =>  $user->subscriptions()->first() ? $user->subscriptions()->first()->ends_at : null,
            'stripe_status' => $user->subscriptions()->first() ? $user->subscriptions()->first()->stripe_status : null,
            'next_billing' => $user->nextBillingDate(),
            'invoices' => $user->invoices()->map(function($q){
                return [
                        'date' => $q->date()->toFormattedDateString(),
                        'price' => $q->total(),
                        'link' =>  "/user/invoice/$q->id"
                ];
            }),
            'accesses' => $accessData
        ];
    }

    function acceses(Request $request){
        $accesses = Access::all();
        $data = array();

        foreach($accesses as $access){
            $name =  \Str::camel($access->display_name);
            $data[$name] = (new SubscriptionService)->userHasAccess($request->user(), $access->name)
                            ? 1 : ((new SubscriptionService)->trailAccessValidator($request->user(),$access->name) ? 1 : 0);
        }

        return ['data' => $data];
    }


    function subscriptionList(Request $request) : array {
        if($request->user()->subscriptions->count()){
            $plan = $request->user()->current_plan;
        }else{
            $plan = SubscriptionPlan::where('name', 'Beginner')->first();
        }

        return [
            'data' =>  SubscriptionPlan::where('id', '!=', $plan->id)->with('accesses')->get()->map(function($q){
                return [
                    'id' => $q->id,
                    'name' => $q->name,
                    'stripe_plan' => $q->stripe_plan,
                    'price' => $q->price,
                    'accesses' => $q->accesses->map(function($q){
                        return $q->display_name;
                    })
                ];
            })
        ];
    }

    function invoices(Request $request): array
    {
        // return $request->user()->invoices()->plan();
        return [
            'data' => $request->user()->invoices()->map(function($q){
                return [
                    'date' => $q->date()->toFormattedDateString(),
                    'price' => $q->total(),
                    'link' => "/user/invoice/$q->id"
                ];
            }),
        ];
    }

    function update(Request $request): \Illuminate\Http\JsonResponse
    {
        $plan = SubscriptionPlan::findOrFail($request->plan);
        $user = $request->user();

        try {
             if($user->subscriptions()->count()){
                $current = $user->stripeSubscription();
                $user->subscription("Designa Plan")->swapAndInvoice($plan->stripe_plan);
            }else{
                $paymentMethod = $request->paymentMethod;
                $user->newSubscription("Designa Plan", $plan->stripe_plan)
                    ->create($paymentMethod, [
                        'email' => $user->email,
                    ]);
            }

            return response()->json([
                'status' => 200,
                'message' => 'Subscription saved!'
            ], 200);

        }catch(\Stripe\Exception\CardException $e) {
            return response()->json([
                'status' => 422,
                'message' => $e->getError()->message
            ], 422);
        } catch (\Stripe\Exception\RateLimitException $e) {
            // Too many requests made to the API too quickly
            return response()->json([
                'status' => 422,
                'message' => 'Too many requests made to the API too quickly'
            ], 422);
        } catch (\Stripe\Exception\InvalidRequestException $e) {
            // Invalid parameters were supplied to Stripe's API
            return response()->json([
                'status' => 422,
                'message' => "Invalid parameters were supplied to Stripe's API"
            ], 422);
        } catch (\Stripe\Exception\AuthenticationException $e) {
            // Authentication with Stripe's API failed
            return response()->json([
                'status' => 422,
                'message' => "Authentication with Stripe's API failed"
            ], 422);
            // (maybe you changed API keys recently)
        } catch (\Stripe\Exception\ApiConnectionException $e) {
            // Network communication with Stripe failed
            return response()->json([
                'status' => 422,
                'message' => "Network communication with Stripe failed"
            ], 422);
        } catch (\Stripe\Exception\ApiErrorException $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            return response()->json([
                'status' => 422,
                'message' => $e->getMessage()
            ], 422);
        } catch (Exception $e) {
            return response()->json([
                'status' => 422,
                'message' => $e->getMessage()
            ], 422);
        }

    }

     function cancelSubscription(Request $request): \Illuminate\Http\JsonResponse
     {
         try {
             if($request->effectivity == "now"){
                $request->user()->subscription("Designa Plan")->cancelNow();
            }else{
                $request->user()->subscription("Designa Plan")->cancel();
            }

            return response()->json([
                'status' => 200,
                'message' => 'Subscription canceled!'
            ], 200);

         }catch (\Exception $exception){
             return response()->json([
                    'status' => 422,
                    'message' => $exception->getMessage()
                ], 422);
        }
    }


    function resumeSubscription(Request $request): \Illuminate\Http\JsonResponse
    {
        try {
         $request->user()->subscription("Designa Plan")->resume();
         return response()->json([
                'status' => 200,
                'message' => 'Subscription resumed!'
            ]);

        }catch (\Exception $exception){
             return response()->json([
                    'status' => 422,
                    'message' => $exception->getMessage()
                ], 422);
        }
    }
}
