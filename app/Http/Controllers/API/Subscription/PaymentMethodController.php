<?php

namespace App\Http\Controllers\API\Subscription;

use Auth;
use Cache;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PaymentMethodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $data = array();
        foreach($user->user_payment_methods as $paymentMethod){
            array_push($data, [
                'id'                => $paymentMethod->id,
                'card_type'         => $paymentMethod->card->brand,
                'last_4'            => $paymentMethod->card->last4,
                'is_default'        => Auth::user()->default_payment_method->id  == $paymentMethod->id ? 1 : 0,
                'billing_name'      => $paymentMethod->billing_details->name,
                'billing_details'   => $paymentMethod->billing_details,
                'created_at'        => $paymentMethod->created,
                'card'              => $paymentMethod->card,
                'expiration'        => date('m', ($paymentMethod->card->exp_month)) . '/' . $paymentMethod->card->exp_year
            ]);
        }

        return [
            'data' => $data
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $request->user()->createOrGetStripeCustomer();
        $request->user()->addPaymentMethod($request->payment_method);
        if(count($request->user()->paymentMethods()) < 2){
            $request->user()->updateDefaultPaymentMethod($request->payment_method);
        }
        Cache::forget($request->user()->id . '-payment_methods');

        return response()->json([
            'status' => 200,
            'message' => 'Payment Methods Saved!'
        ], 200);
    }

    public function setDefault(Request $request, $id): \Illuminate\Http\JsonResponse
    {
        $request->user()->updateDefaultPaymentMethod($id);
        $key = $request->user()->id . '-default-payment_methods';
        Cache::forget($key);
        return response()->json([
            'status' => 200,
            'message' => 'Default payment method updated!'
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($key)
    {
        try {
            $paymentMethod = Auth::user()->findPaymentMethod($key);
            $paymentMethod->delete();
            Cache::forget(Auth::user()->id . '-payment_methods');

            return response()->json([
                'status' => 200,
                'message' => 'Payment Method Deleted!'
            ], 200);

        }catch (\Exception $exception){
            return response()->json([
                'status' => 422,
                'message' => $exception->getMessage()
            ], 422);
        }
    }
}
