<?php

namespace App\Http\Controllers\API;

use DB;
use Auth;
use Notification;
use Carbon\Carbon;
use App\Models\Qoute;
use App\Models\Company;
use App\Models\Product;
use App\Models\Category;
use App\Models\Location;
use App\Models\QouteProduct;
use Illuminate\Http\Request;
use App\Facades\FirebaseFMC;
use App\Models\PricingComponent;
use App\Models\QouteProductPrice;
use App\Http\Controllers\Controller;
use App\Http\Resources\Qoute\SupplierQoute;
use App\Http\Resources\Qoute\Qoute as QouteResource;
use App\Http\Resources\Review\Comment\Review as Comment;
use App\Notifications\Customer\QuoteResponseNotification;
use App\Notifications\Supplier\Qoute\NewQouteNotification;
use App\Http\Resources\Qoute\Product as ProductQouteResource;

class QuoteController extends Controller
{

    function quoteList(Request $request){
        $quotes = QouteResource::collection(Qoute::whereUserId($request->user()->id)->orderBy('created_at', 'DESC')->paginate(6));
        return $quotes;
    }

    function savedDecor(Request $request){

        $data = array();

        // $all = Product::whereHas('company', function($q) use ($request) {
        //     // $q->selectRaw('ST_Distance_Sphere(
        //     //     point(latitude, longitude),
        //     //     point(?, ?)
        //     // ) * .000621371192 AS distance ',  [$request->latitude, $request->longitude])
        //     // ->havingRaw("distance < ?", [100])->orWhereHas('locations', function($q) use ($request){
        //     //     $q->selectRaw('ST_Distance_Sphere(
        //     //         point(latitude, longitude),
        //     //         point(?, ?)
        //     //     ) * .000621371192 AS distance ',  [$request->latitude, $request->longitude])
        //     //     ->havingRaw("distance < ?", [100]);
        //     // });

        //       $q->whereHas('locations', function($q) use ($request){
        //                         $q->select(DB::raw('*, ( 6367 * acos( cos( radians('.$request->longitude.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$request->latitude.') ) + sin( radians('.$request->longitude.') ) * sin( radians( latitude ) ) ) ) AS distance'))
        //                             ->having('distance', '<', 50);
        //                     });
        // })
        // ->whereHas('hearts', function($q) use ($request) {
        //     $q->where('user_id', $request->user()->id);
        // })->whereHas('categories', function($q) use ($request){
        //     $q->where('id', $request->event_type);
        // });
        $all = Product::whereHas('hearts', function($q) use ($request) {
            $q->where('user_id', $request->user()->id);
        })->whereHas('categories', function($q) use ($request){
            $q->where('id', $request->event_type);
        });

        $decors = $all->take(15)->get();

        foreach($decors as $decor){
            $array = [
                'id'                => $decor->id,
                'name'              => $decor->name,
                'thumbnail'         => $decor->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder250x200.png'),
                'company'           => $decor->company->name,
                'product_type'      => $decor->categories()->whereType('product_type')->first()->name,
                'quantity'          => 1,
                'hearted'           => $request->user ? $this->hearts()->whereUserId($request->user()->id)->first() ? 1 : 0 : 0
            ];
            array_push($data, $array);
        }

        return [
            'data' => $data
        ];
    }

    function allDecor(Request $request){

        $data = array();

        $all =  Product::query();
        // $all =  Product::whereHas('company', function($q) use ($request) {
        //             $q->selectRaw('ST_Distance_Sphere(
        //                 point(latitude, longitude),
        //                 point(?, ?)
        //             ) * .000621371192 AS distance ', [$request->latitude, $request->longitude])
        //             ->havingRaw("distance < ?", [100])->orWhereHas('locations', function($q) use ($request){
        //                 $q->selectRaw('ST_Distance_Sphere(
        //                     point(latitude, longitude),
        //                     point(?, ?)
        //                 ) * .000621371192 AS distance', [$request->latitude, $request->longitude])
        //                 ->havingRaw("distance < ?", [100]);
        //             });
        //         });

        if($request->categories){
            $ids = $this->sanitizeCategories($request->categories);

            $all->whereHas('categories', function($q) use ($ids){
                $q->whereIn('categories.id', $ids);
            });

        }

        if($request->company_id){
            $all->whereHas('company',function($q) use ($request) {
                $q->where('id', $request->company_id);
            });
        }

        if($request->search){
            $all->where('name', 'LIKE', '%' . $request->search . '%'); /// applicable for product name
        }

        return ProductQouteResource::collection($all->paginate(15));
        // return $all->get()->map(function($decor) use ($request){
        //     return  [
        //         'id'            => $decor->id,
        //         'name'          => $decor->name,
        //         'thumbnail'     => $decor->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder250x200.png'),
        //         'company'       => $decor->company->name,
        //         'product_type'  => $decor->categories()->whereType('product_type')->first()->name,
        //         'quantity'      => 1,
        //         'hearted'       => $request->user ? $this->hearts()->whereUserId($request->user()->id)->first() ? 1 : 0 : 0
        //     ];
        // })->paginate(15);


        // $decors = $all->paginate(15);

        // foreach($decors as $decor){
        //     $array = [
        //         'id'            => $decor->id,
        //         'name'          => $decor->name,
        //         'thumbnail'     => $decor->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder250x200.png'),
        //         'company'       => $decor->company->name,
        //         'product_type'  => $decor->categories()->whereType('product_type')->first()->name,
        //         'quantity'      => 1,
        //         'hearted'       => $request->user ? $this->hearts()->whereUserId($request->user()->id)->first() ? 1 : 0 : 0
        //     ];
        //     array_push($data, $array);
        // }

        // return [
        //     'data' => $data
        // ];
    }



    function saveQuote(Request $request){

        $this->validate($request, [
            'event_type' => 'required',
            'location' => 'required',
            'event_date' => 'required',
            'time_start' => 'required',
            'time_end' => 'required',
            'number_of_guests' => 'required',
        ]);

        $quote = new Qoute;
        $quote->reference_number = time() . rand(10*45, 100*98);
        $quote->user_id = $request->user()->id;
        $quote->event_type = $request->event_type;
        $quote->event_name = $request->event_name;
        $quote->location = $request->location;
        $quote->event_date = $request->event_date;
        $quote->time_start = $request->time_start;
        $quote->time_end = $request->time_end;
        $quote->number_of_guests = $request->number_of_guests;

        $quote->save();

        $counter = 0;
        foreach($request->decors as $decor){
            $quoteproduct = new QouteProduct;
            $quoteproduct->product_id = $decor;
            $quoteproduct->qoute_id = $quote->id;
            $quoteproduct->quantity = $request->quantity[$counter];
            $quoteproduct->save();
            $counter++;
        }

        $companies = $quote->suppliers();
        foreach($companies as $company){
            $company->users->map(function($q) use ($quote){
                    $q->mobileNotify(
                        [
                        'title'         => 'New Qoute',  // title,
                        'avatar'        => $quote->user->getProfilePicture(),
                        'message'       => $quote->user->name . ' had new qoute entitled <b>' . $quote->event_name . '</b>',  // message,
                        'notif_type'    => 'new_qoute',
                        'item_id'       => $quote->id,
                        'link'          => route('dashboard.quote.show', $quote->reference_number),
                        'image'         => '',  // image
                        'read_at'       => '',
                        'timestamp'     => Carbon::now()->format("Y-m-d H:i:s") // timestamp
                    ]);
            });

            Notification::send($company->users, new NewQouteNotification($quote));
        }

        return [
            'status' => 200,
            'reference_number' => $quote->reference_number,
            'message' => 'saving success'
        ];

    }

    function show($id){
        $qoute = Qoute::whereReferenceNumber($id)->firstOrFail();

        $supplierIds = Array();
        $data  = array();


        foreach($qoute->decors as $decor){
            if($decor->product){
                $company_id = $decor->product->company_id;
                if(!in_array($company_id, $supplierIds)){
                    array_push($supplierIds, $company_id);
                }
            }
        }

            // for($i = 0; $supplierIds > $i; $i++){
            foreach($supplierIds as $supplierId){
                $isCompleted = true;
                $supplier  = Company::find($supplierId);
                $array = array(
                'event_name' => $qoute->event_name,
                'supplier' => [
                        'id' => $supplier->id,
                        'name' => $supplier->name,
                        'avatar' => $supplier->image('logo')->path('logos', '/images/placeholders/placeholder.png'),
                        'status' => $qoute->isSupplierPricingsComplete($supplierId) ? "completed" : "pending", //completed, pending
                        'total_price' => $qoute->supplierTotalPrice($supplierId),
                        'reference_number' => $qoute->reference_number,
                    ],
                'decor' => $qoute->decors()->whereHas('product', function($q) use ($supplierId){
                                $q->whereCompanyId($supplierId);
                            })->get()->map(function($decor){
                                return [
                                    'id' => $decor->id,
                                    'product' => $decor->product->name,
                                    'thumbnail' => $decor->product->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),
                                    'quantity' => $decor->quantity ?? 0,
                                    'responses' => PricingComponent::whereActive(1)->get()->map(function($q) use ($decor){
                                        // if($isCompleted = false){
                                        //     if($decor->getComponentPrice($q->id)
                                        // }
                                        return [
                                            'name' => $q->name,
                                            'price' => $decor->getComponentPrice($q->id),
                                            // 'subtotal' => $decor->getSubTotalPrice($q->id)
                                        ];
                                    }),
                                    'product_total_cost' => $decor->getTotal(),
                                    'comments' => Comment::collection($decor->reviews),
                                    'files' => []
                                ];
                            })
            );
            array_push($data, $array);
        }


        return [
            'data' => [
                    'quote_information' =>  [
                        'id' => $qoute->id,
                        'event_type' => Category::find($qoute->event_type)->name ?? "",
                        'event_name' => $qoute->event_name,
                        'event_date' => $qoute->event_date,
                        'location' => $qoute->location,
                        'time_start' => $qoute->time_start,
                        'time_end' => $qoute->time_end,
                        'number_of_guests' => $qoute->number_of_guests,
                        'reference_number' => $qoute->reference_number,
                        'created_at' => $qoute->created_at,
                        'overall_total' => $qoute->getOverAllPrice()
                    ],
                    'list' =>  $data,
                ]
            ];
    }

    function sendComment(Request $request){
        $quoteproduct = QouteProduct::find($request->id);
        $comment = $quoteproduct->saveReview($request, 'mobile');
        return new Comment($comment);
    }


    function supplierQouteListPending(Request $request){

        $listing = Qoute::whereHas('decors', function($q){
                $q->whereHas('product', function($q){
                        $q->where('company_id', Auth::user()->company_id);
                });
            })->orderBy('created_at', 'DESC')
            ->get()->filter(function($q) use ($request){
                    return !$q->isSupplierPricingsComplete($request->user()->company_id);
            })->paginate(15);


        return SupplierQoute::collection($listing);
    }

    function supplierQouteList(Request $request){

        $listing = Qoute::whereHas('decors', function($q){
            $q->whereHas('product', function($q){
                    $q->where('company_id', Auth::user()->company_id);
            });
        })->orderBy('created_at', 'DESC')->paginate(15);

        return SupplierQoute::collection($listing);
    }

    function supplierQouteListResponded(Request $request){

        $listing = Qoute::whereHas('decors', function($q){
            $q->whereHas('product', function($q){
                    $q->where('company_id', Auth::user()->company_id);
            });
        })->orderBy('created_at', 'DESC')->get()->filter(function($q) use ($request){
                return $q->isSupplierPricingsComplete($request->user()->company_id);
            })->paginate(15);

        return SupplierQoute::collection($listing);
    }

    function supplierQouteShow(Request $request, $id){
        $quote = Qoute::find($id);
        $decors = $quote->decors()->whereHas('product',function($q) use ($request){
                $q->where('company_id', $request->user()->company_id);
            })->get();

        $pricingcomponents = PricingComponent::whereActive(1)->get();

        $quote_info = [
               'quote_id'           => $quote->id,
               'event_name'         => $quote->event_name,
               'reference_number'   => $quote->reference_number,
               'location'           => $quote->location,
               'number_of_guests'   => $quote->number_of_guests,
               'event_date'         => $quote->event_date,
               'event_date'         => $quote->event_date,
               'time_start'         => $quote->time_start,
               'time_end'           => $quote->time_end,
               'created_at'         => $quote->created_at,
               'customer_id'         => optional($quote->user)->id,
               'avatar'             => $quote->user->getProfilePicture(),
               'name'               => $quote->user->name,
               'email'              => $quote->user->email,
        ];

        $decorsArray = array();

        foreach($decors  as $decor){

            $pricingArray = array();

            foreach($pricingcomponents as $pricing){
                if($pricing->multipliable){
                    $name = $pricing->name . ' (each)';
                }else{
                    $name = $pricing->name;
                }
                array_push($pricingArray, array(
                    'component_id' => $pricing->id,
                    'name' => $name,
                    'price' => $decor->getComponentPrice($pricing->id)
                ));
            }

            array_push($decorsArray, array(
                'product_id' => $decor->product->id,
                'name' => $decor->product->name,
                'thumbnail' => optional($decor->product)->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),
                'quantity' => $decor->quantity,
                'pricing' => $pricingArray
            ));

        }


        return array(
            'quote_info' => $quote_info,
            'decors' => $decorsArray
        );
    }


    function supplierQouteSave(Request $request){

        $quote = Qoute::find($request->quote_id);
        $pricingcomponents = PricingComponent::whereActive(1)->get();


        // foreach($quote->decors as $decor){
        //     //delete all the pring of this decor of this quote
        //     $decor->prices()->delete();
        if($quote->decors){
            $decor = $quote->decors()->where('product_id', $request->product_id)->first();
            if($decor){
                $decor->prices()->delete();
            }
        }

        $counter = 0;

        foreach($pricingcomponents as $pricingcomponent){
            $price = new QouteProductPrice;
            $price->qoute_product_id = $decor->id;
            $price->pricing_component_id = $pricingcomponent->id;
            $price->price = $request->prices[$counter];
            $price->save();
            $counter++;
        }
        // }


        if($request->notify){

            $customer = User::find($quote->user_id);
            $company = Company::find($request->user()->company_id);
            $customer->notify(new QuoteResponseNotification($quote, $company));

            $firebaseArray = array();

            foreach($customer->firebases as $firebaseAccount){
                array_push($firebaseArray, $firebaseAccount->firebase_id);
            }

            $firebase = new FirebaseFMC;
            $firebase->sendSingle(
                    $firebaseArray,
                    array(
                        'title'         => 'Qoute Pricing Update',//$inspiration->title,  // title
                        'avatar'        => $company->image('logo')->path('logos', '/images/placeholders/placeholder.png'), //$inspiration->description,  // message,
                        'message'       => $company->name . " has updated pricing for your quotation for " . $quote->event_name, //$inspiration->description,  // message,
                        'notif_type'    => 'quote_pricing_update',
                        'item_id'       => $quote->reference_number,
                        'link'          => '',//route('site.inspiration.show', $inspiration->slug),
                        'image'         => $company->image('logo')->path('logos', '/images/placeholders/placeholder.png'),  // image
                        'timestamp'     => Carbon::now()->format('Y-m-d h:i:s') // timestamp
                    )
                );
        }

        return response()->json([
            'message' => 'Prices updated',
        ], 200);
    }


    function sendSupplierReminder(Request $request){
        $quote = Qoute::find($request->quote_id);
        $company = Company::find($request->supplier_id);

        $company->users->map(function($q) use ($quote){
                $q->mobileNotify(
                    [
                    'title'         => 'New Quote',  // title,
                    'avatar'        => $quote->user->getProfilePicture(),
                    'message'       => $quote->user->name . ' wants a quotation update for <b>' . $quote->event_name . '</b>',  // message,
                    'notif_type'    => 'notify_quote',
                    'item_id'       => $quote->id,
                    'link'          => route('dashboard.quote.show', $quote->reference_number),
                    'image'         => '',  // image
                    'read_at'       => '',
                    'timestamp'     => Carbon::now()->format("Y-m-d H:i:s") // timestamp
                ]);
        });

        Notification::send($company->users, new NewQouteNotification($quote));

        return response()->json([
            'message' => 'Notification Sent',
        ], 200);
    }
}

