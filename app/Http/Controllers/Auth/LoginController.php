<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\Models\User;
use App\Models\ActivityLog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    use AuthenticatesUsers;


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';


      /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    public function loginWebApi(Request $request){
        $request->validate([
            'email'     => 'required|string|email',
            'password'  => 'required|string',
        ]);

        $credentials = request(['email', 'password']);

        if(!Auth::attempt($credentials, $request->remember)){
            $user = User::whereEmail($request->email)->first();
            if($user){
                return response()->json([
                    'message' => 'Invalid email and password combination'
                ], 401);
            }else{
                return response()->json([
                    'message' => 'No account found'
                ], 401);
            }
        }else{
            $user = Auth::user();
            if($user->email_verified_at == "") {
                Auth::logout();
                return response()->json([
                    'message' => 'Your email is not verified yet.'
                ], 401);
            }
        }
        
        $activity = new ActivityLog;
        $activity->details = "Login in the system";
        $activity->type ="LOGIN";
        $activity->origin = "web";

        Auth::user()->activities()->save($activity);

        return response()->json([
            'user_type' => Auth::user()->type,
            'message' => 'success'
        ], 200);

    }

    protected function authenticated(Request $request, $user)
    {
        if($user->email_verified_at == "") {
            Auth::logout();
            if($request->ajax()){
                return response()->json([
                    'errors' => [
                        'email' => ['Your email is not yet verified'] 
                        ]
                    ],
                    422);
            }else{
                return back()->withErrors(['email' => ["Your email is not yet verified"]]);
            }
        }else{
            if($user->type != "customer"){
                return redirect()->route('dashboard.home'); 
            }
           
        }
      
    }



    

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
