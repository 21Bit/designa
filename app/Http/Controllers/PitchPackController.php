<?php

namespace App\Http\Controllers;

use PDF;
use File;
use Response;
use App\Models\Project;
use Illuminate\Http\Request;

class PitchPackController extends Controller
{
    function showPdf(Request $request, $filename){

        // Geting the Project Model
        $slug = str_replace("",".pdf",$filename);
        $project = Project::whereSlug($slug)->firstOrFail();
        
        // Checking the file if exist and create new if doesn't
        $pdfFile = public_path('pdf/'. $project->slug . '.pdf');
        if (!File::exists($pdfFile)) {
            $pdf = PDF::loadView('dashboard.event-board.pdf', compact('project'));
            return $pdf->save($pdfFile)->stream($pdfFile);
        }

        // Preparing the response
        $file = File::get($pdfFile);
        $type = File::mimeType($pdfFile);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    }
}
