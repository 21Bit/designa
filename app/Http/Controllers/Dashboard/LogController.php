<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Middleware\SuperAdminMiddleware;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class LogController extends Controller
{
    public function __construct()
    {
        $this->middleware([SuperAdminMiddleware::class]);
    }

    function index(Request $request){
        $date = new Carbon($request->get('date', today()));
        $filePath = storage_path("logs/laravel-{$date->format('Y-m-d')}.log");
        $data = [];

        if(File::exist($filePath)){
            $data = [
                'lastModified' => new Carbon(File::lastModified($filePath)),
                'size'          =>File::size($filePath),
                'file'          =>File::get($filePath),
            ];
        }

         return view('dashboard.log.index', compact('date', 'data'));
    }

}
