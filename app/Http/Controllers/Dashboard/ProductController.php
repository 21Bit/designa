<?php

namespace App\Http\Controllers\Dashboard;

use Auth;
use Cache;
use App\Models\Color;
use App\Models\Image;
use App\Models\Company;
use App\Models\Product;
use App\Models\Category;
use App\Models\ModelFile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\HasImageIntervention;

class ProductController extends Controller
{
    use HasImageIntervention;

    public function __construct()
    {
        $this->middleware('access:load-product');
    }


    public function index(Request $request)
    {
        if(!Auth::user()->isAbleTo('product')){
            abort(403);
        }

        $list = Product::query();

        if($request->category){
            $list->whereHas('categories', function($q) use ($request) {
                $q->whereId($request->category);
            });
        }
        if($request->type){
            $list->whereHas('categories', function($q) use ($request) {
                $q->whereId($request->type);
            });
        }
        if($request->search){
            $list->where("name", 'LIKE', '%' . $request->search . '%');
        }

        if($request->feature){
              $list->where('featured', $request->feature == 2 ? 1 : 0);
        }

        if($request->status){
            $list->where('is_published', $request->status == 2 ? 1 : 0);
        }

        if($request->lock){
            $list->where('is_lock', $request->lock == 2 ? 1 : 0);
        }
        if($request->supplier){
            $list->where('company_id', $request->supplier);
        }
        if(!Auth::user()->is_superadmin){
            $list->where('company_id', Auth::user()->company_id);
        }

        $products = $list
                ->with('colors')
                ->with('images')
                ->with('categories')
                ->withCount(['reviews','hearts', 'images'])
                ->orderBy('created_at', 'DESC')
                ->paginate(20);

//        return $products;
        $categories = Category::where('type', 'cater')->get();
        $types = Category::whereType('product_type')->with('subCategories')->get();

        if(Auth::user()->is_superadmin){
            $suppliers = Company::whereIsActive(1)->get();
            return view('dashboard.product.index', compact('products', 'categories', 'types', 'suppliers'));
        }

        return view('dashboard.product.index', compact('products', 'categories', 'types'));
    }


    public function create(Request $request)
    {
        if(!Auth::user()->isAbleTo('product')){
            abort(403);
        }

        $categories = Auth::user()->caters;
        $types = Category::where('type', 'product_type')->get();
        $colors = Category::whereType('color')->get();
        return view('dashboard.product.create', compact('categories', 'types', 'colors'));
    }


    public function store(Request $request)
    {
        if(!Auth::user()->isAbleTo('product')){
            abort(403);
        }
        // check if pro. If not then limit only to 10 items
        if($request->user()->productLimitReached()){
            return back()
                    ->withInput($request->input())
                    ->with('warning', 'You already reached your limit, please upgrade your plan to Pro to continue adding your products.');
        }

        $this->validate($request, [
            'name' => 'required',
            'product_type' => 'required|integer',
            'thumbnail' => 'required|mimes:'. config('filesystems.accepted_image_types'),
        ]);

        $product = new Product;
        $product->name = $request->name;
        $product->description = $request->description;
        $product->quantity = $request->quantity;
        $product->product_identifier = $request->product_identifier;
        $product->product_size = $request->product_size;
        $product->is_published = $request->is_published ? 1 : 0;
        $product->company_id = Auth::user()->company_id;

        $product->save();

        $product->categories()->whereType('color')->attach($request->colors);
        $product->categories()->whereType('cater')->attach($request->categories);
        $product->categories()->whereType('product_type')->attach($request->product_type);

        if($request->style){
            $product->categories()->whereType('style')->attach($request->style);
        }

        if($request->hasFile('thumbnail')){
            $thumbnail = $product->addImage("thumbnail", $request->file('thumbnail'), public_path('images/thumbnails'));

            // resize image if the image exceed 800px then reduce to 800 ^_^
            list($width, $height) = getimagesize(public_path('images/thumbnails/' . $thumbnail->path));

            if($width > 800){
                $this->imageResize(public_path('images/thumbnails/' . $thumbnail->path), 800, null, true);
            }

            $product->images()->save($thumbnail);
        }


        if($request->hasFile('model')){
            $file = $request->file('model');
            //Move Uploaded File
            $destinationPath = public_path('models/');

            $filename = time() . '.' . $file->getClientOriginalExtension();

            $path = $file->move($destinationPath, $filename);
            // $path = $file->move($destinationPath, $file->getClientOriginalName());

            $product->saveModelFile([
                'model_name' => $product->name,
                'path'      =>  'models/' . $filename,
                'is_public'      => 1,
            ]);

        }



        if($request->proceed){
            return redirect()->route('dashboard.product.show', $product->slug);
        }else{
            return redirect()->route('dashboard.product.index');
        }
    }


    public function show(Request $request, $id)
    {
        if(!Auth::user()->isAbleTo('product')){
            abort(403);
        }

        $product = Product::where('slug',$id)->firstOrFail();
        $gallery = $product->images()->where('type', 'gallery')->get();
        $caters = Auth::user()->caters;

        if($request->tab == "reviews"){
            return view('dashboard.product.reviews', compact('product', 'gallery', 'caters'));
        }else if($request->tab == "hearts"){
            $hearts = $product->hearts;
            return view('dashboard.product.hearts', compact('product', 'gallery', 'caters', 'hearts'));
        }else if($request->tab == "3d"){
            $hearts = $product->hearts;
            return view('dashboard.product.3dmodel', compact('product', 'gallery', 'caters', 'hearts'));
        }else{
            return view('dashboard.product.show', compact('product', 'gallery', 'caters'));
        }
    }


    public function uploadAssetBundle(Request $request, $id){

        $this->validate($request,[
            'type'                  => 'required',
            'asset_bundle_file'     => 'required',
        ]);

        $product = Product::find($id);

        if($request->hasFile('asset_bundle_file')){

            // deleting old Android file
            if($request->type == 'web'){
                $old = $product->asset_bundle_web;
            }else if($request->type == "android"){
                $old = $product->asset_bundle_android;
            }else{
                $old = $product->asset_bundle_ios;
            }


            if($old){
                $old->delete();
            }

            $destinationPath = public_path('3d-models/');

            $file = $request->file('asset_bundle_file');
            $filename = time() . '_' . $request->type . $request->file('asset_bundle_file')->getClientOriginalExtension();

            $webPath        = $file->move($destinationPath, $filename);


            // saving web
            $product->saveModelFile([
                'model_name'        => $request->file('asset_bundle_file')->getClientOriginalName(),
                'path'              => 'models/' . $filename,
                'type'              => 'asset_bundle_' . $request->type,
                'is_public'         =>  0,
            ]);


        }


        return back()->with('message' , 'Uploading complete');
    }

    public function deleteModel($id){

        $modelfile = ModelFile::find($id);

        $model = public_path($modelfile->path);
        if(file_exists($model)){
            unlink($model);
        }

        $modelfile->delete();

        if(request()->ajax()){
            return response([
                'status' => 'success',
                'message' => 'Item Deleted'
            ], 200);
        }else{
            return back();
        }
    }

    public function uploadgallery(Request $request, $id){
        $this->validate($request, [
            'file' => 'required'
        ]);

        $product = Product::find($id);

        if($request->hasFile('file')){
            //add and upload
            $gallery = $product->addImage("gallery", $request->file('file'), public_path('images/gallery'));

            // resize images for gallery
            list($width, $height) = getimagesize(public_path('images/gallery/' . $gallery->path));
            if($width > 800){
                $this->imageResize(public_path('images/gallery/' . $gallery->path), 800, null, true);
            }

            $product->images()->save($gallery);
        }

        if(!$request->ajax()){
            return back();
        }else{
            return [
                'status' => 200,
                'message' => 'success',
                'image' =>  $gallery
            ];
        }
    }



    public function edit($id){
        if(!Auth::user()->isAbleTo('product')){
            abort(403);
        }

        $product = Product::findOrFail($id);
        $categories = Auth::user()->caters;
        $types = Category::where('type', 'product_type')->get();
        $colors = Category::whereType('color')->get();
        if(Auth::user()->is_superadmin){
            $companies = Company::all();
            return view('dashboard.product.edit', compact('categories', 'types', 'colors', 'product', 'companies'));
        }
        return view('dashboard.product.edit', compact('categories', 'types', 'colors', 'product'));
    }



    public function update(Request $request, $id)
    {
        if(!Auth::user()->isAbleTo('product')){
            abort(403);
        }

        $this->validate($request, [
            'name' => 'required',
            'product_type' => 'required|integer',
            'thumbnail' => 'nullable|mimes:' . config('filesystems.accepted_image_types'),
        ]);

        $product = Product::findOrFail($id);
        $product->name = $request->name;
        $product->description = $request->description;
        $product->quantity = $request->quantity;
        $product->product_identifier = $request->product_identifier;
        $product->product_size = $request->product_size;
        $product->is_published = $request->is_published ? 1 : 0;

        $product->is_lock = $request->is_lock ? 1 : 0;
        $product->featured = $request->featured ? 1 : 0;

        if($request->company_id){
            $product->company_id = $request->company_id;
        }

        $product->save();

        $product->clearAttachCategory(['cater', 'product_type' ,'color', 'style']);
        $product->categories()->whereType('color')->attach($request->colors);
        $product->categories()->whereType('cater')->attach($request->categories);
        $product->categories()->whereType('product_type')->attach($request->product_type);

        if($request->style){
            $product->categories()->whereType('style')->attach($request->style);
        }

        if($request->hasFile('thumbnail')){

            $product->clearImage('thumbnail');

            $thumbnail = $product->addImage("thumbnail", $request->file('thumbnail'), public_path('images/thumbnails'));

            // resize image if the image exceed 800px then reduce to 800 ^_^
            list($width, $height) = getimagesize(public_path('images/thumbnails/' . $thumbnail->path));

            if($width > 800){
                $this->imageResize(public_path('images/thumbnails/' . $thumbnail->path), 800, null);
            }

            $product->images()->save($thumbnail);

        }

        // for the update observer to run after all process
        $product->touch();

        if($request->proceed){
            return redirect()->route('dashboard.product.show', $product->slug);
        }else{
            return redirect()->route('dashboard.product.index');
        }
    }

    public function deletegallery(Request $request, $id){
        if(!Auth::user()->isAbleTo('product')){
            abort(403);
        }

        $image = Image::find($id);
        removeImage($image->path, 'gallery');
        $image->delete();

        if(!$request->ajax()){
            return back();
        }
    }

    public function destroy($id)
    {
        if(!Auth::user()->isAbleTo('product')){
            abort(403);
        }

        $product = Product::find($id);

//            //clearing all the images and picture
//            $product->clearImage('thumbnail');
//            $product->clearImage('gallery');
//
//            $product->hearts()->delete();
//            $product->views()->delete();
//            $product->reviews()->delete();
//
//            //clearning all the categories that attach to it
//            $product->clearAttachCategory(['cater', 'product_type' ,'color']);

        //actual deletion
        $product->delete();

        if(!request()->ajax()){
            return back();
        }
    }
}
