<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Heart;
use Auth;
use Mail;
use Cache;
use Carbon\Carbon;
use App\Models\Role;
use App\Models\Image;
use App\Models\Venue;
use App\Models\Video;
use App\Models\Company;
use App\Models\Category;
use App\Models\ActivityLog;
use App\Models\Inspiration;
use App\Facades\FirebaseFMC;
use Illuminate\Http\Request;
use App\Models\SupplierInspiration;
use App\Http\Controllers\Controller;
use App\Mail\Dashboard\ItemLockMail;
use App\Traits\HasImageIntervention;
use App\Http\Resources\Venue as VenueResource;

class InspirationController extends Controller
{
    use HasImageIntervention;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = Category::whereType('cater')->get();
        $query = Inspiration::query();

        if($request->category){
            $query->whereHas('categories', function($q)
             use ($request){
                $q->where('id', $request->category);
            });
        }


        if($request->search){
            $query->where('name', 'LIKE', '%' . $request->search . '%');
        }

        if($request->supplier){
            $query->where('company_id', $request->supplier);
        }

        if($request->feature){
              $query->where('featured', $request->feature == 2 ? 1 : 0);
        }

        if($request->status){
            $query->where('is_published', $request->status == 2 ? 1 : 0);
        }

        if($request->lock){
            $query->where('is_lock', $request->lock == 2 ? 1 : 0);
        }

        if(!Auth::user()->is_superadmin){
            $query->where('company_id', Auth::user()->company_id);
        }

       $inspirations = $query
                    ->with('company')
                    ->with('colors')
                    ->with('categories')
                    ->orderBy('created_at', 'DESC')
                    ->paginate(32);

        if(Auth::user()->is_superadmin){
            $suppliers = Company::whereIsActive(1)->get();
            return view('dashboard.inspiration.index', compact('inspirations', 'categories', 'suppliers'));
        }

        return view('dashboard.inspiration.index', compact('inspirations', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $categories = Category::whereType('cater')->with('subCategories')->orderBy('name', 'ASC')->get();
        $colors = Category::whereType('color')->get();
        $roles = Role::whereNotIn('name', ['venues'])->get();
        // $company = Auth::user()->company;
        $currentYear = date('Y');
        $years = array();
        for($i = $currentYear; $currentYear >= 2019; $currentYear--){
            array_push($years, $currentYear);
        }


        return view('dashboard.inspiration.create', compact('categories', 'colors', 'roles', 'years'));
    }

    function getVenues($id){
        $venues = Company::find($id)->venues;
        $data = [];

        foreach($venues as $venue){
            $array = [
                'id' => $venue->id,
                'name' => $venue->name,
                'thumbnail' => $venue->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png')
            ];
            array_push($data, $array);
        }

        return $data;
    }

    function getProducts($id){
        $products = Company::find($id)->products;
        $data = [];

        foreach($products as $product){
            $array = [
                'id' => $product->id,
                'name' => $product->name,
                'thumbnail' => $product->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png')
            ];
            array_push($data, $array);
        }

        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $imageTypes = config('filesystems.accepted_image_types');
        $this->validate($request, [
            'name' => 'required',
            'thumbnail' => "required|max:120000|image",
            'cover'     => "required|image",
            'colors'   => 'required',
            'category' => 'required',
            'style'    => 'required',
            'setting'  => 'required',
            'description' => 'required',
        ]);

        $inspiration = new Inspiration;
        $inspiration->company_id = $request->user()->company_id;
        $inspiration->name = $request->name;
        $inspiration->project_year = $request->project_year;
        $inspiration->location = $request->location;
        $inspiration->description = $request->description;
        $inspiration->is_published = $request->is_published ? 1 : 0;
        $inspiration->images_by = $request->images_by;

        if($request->venue_id){
            $inspiration->venue_id = $request->venue_id;
            $venue = Venue::whereId($request->venue_id)->first();
            $inspiration->location = $venue->name ?? "";
        }

        if($request->latitude){
            $inspiration->location = $request->location;
            $inspiration->latitude = $request->latitude;
            $inspiration->longitude = $request->longitude;
        }

        $inspiration->save();

        $inspiration->categories()->whereType('color')->attach($request->colors);
        $inspiration->categories()->whereType('cater')->attach($request->category);
        $inspiration->categories()->whereType('style')->attach($request->style);
        $inspiration->categories()->whereType('setting')->attach($request->setting);

        if($request->hasFile('thumbnail')){
            $thumbnail = $inspiration->addImage("thumbnail", $request->file('thumbnail'), public_path('images/thumbnails'));
            list($width, $height) = getimagesize(public_path('images/thumbnails/' . $thumbnail->path));
            if($width > 900){
                $this->imageResize(public_path('images/thumbnails/' . $thumbnail->path), 900, null, true);
            }

            $this->imageResize(public_path('images/thumbnails/' . $thumbnail->path), 32, 32, false, public_path('images/32x32/' . $thumbnail->path));

            $inspiration->images()->save($thumbnail);
        }

        if($request->hasFile('cover')){
            $coverimage = $inspiration->addImage("cover", $request->file('cover'), public_path('images/covers'));
            $inspiration->images()->save($coverimage);
        }

        $activity = new ActivityLog;
        $activity->details = "Create Inspiration entitled " . $inspiration->name;
        $activity->type ="inspiration";
        $activity->origin = "web";


        Auth::user()->activities()->save($activity);

        if($request->proceed){
            return redirect()->route('dashboard.inspiration.show', $inspiration->slug);
        }else{
            return redirect()->route('dashboard.inspiration.index');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $inspiration = Inspiration::whereSlug($id)
                        ->firstOrFail();
        if($request->tab == "supplier"){
            $suppliers = $inspiration->supplierRoles()->has('role')->get()->map(function($q){
                if($q->company){
                    return [
                        'id' => $q->id,
                        'is_lock' => $q->is_lock,
                        'is_published' => $q->is_published,
                        'role' => $q->role->display_name,
                        'company' => optional($q->company)->name,
                        'thumbnail' => optional($q->company)->image('logo')->path('logos', '/images/placeholders/company-logo-32x32.png'),
                        'status' => $q->status,
                        'request_by' => $q->request_by,
                        'featured' => $q->featured
                    ];
                }else{
                    return [
                        'id' => $q->id,
                        'is_lock' => $q->is_lock,
                        'is_published' => $q->is_published,
                        'role' => optional($q->role)->display_name,
                        'company' => $q->name,
                        'thumbnail' => "",
                        'status' => $q->status,
                        'request_by' => '',
                        'featured' => $q->featured
                    ];
                }
            });
            return view('dashboard.inspiration.supplier', compact('inspiration', 'suppliers'));
        }else if($request->tab == "videos"){
            $videos = $inspiration->videos()->where('type', 'inspiration_video')->get();
            return view('dashboard.inspiration.video', compact('inspiration', 'videos'));
        }else if($request->tab == "hearts"){
            $hearts = $inspiration->hearts;
            return view('dashboard.inspiration.hearts', compact('inspiration', 'hearts'));
        }else{
            $gallery = $inspiration->images()->where('type', 'gallery')->latest()->get();
            return view('dashboard.inspiration.show', compact('inspiration', 'gallery'));
        }
    }



    public function addSupplier(Request $request, $id){

        $company = Company::find($request->company_id);
        $inspiration = Inspiration::find($id);
        $owner = Company::find($request->user()->company_id);

        $supplier = new SupplierInspiration;
        $supplier->role_id = $request->role_id;
        $supplier->company_id = $request->company_id; // this might optional
        $supplier->request_by = Auth::user()->company_id; // this might optional
        $supplier->name = $request->supplier; // this might optional
        $supplier->email = $request->email; // this might optional

        if($inspiration->company_id == Auth::user()->company_id){
            $supplier->status = 'confirmed';
        }else{
            if($request->email){
                $supplier->status =  "confirmed";
            }else{
                $supplier->status =  "pending";
            }
            // $supplier->status = $request->supplier ? "confirmed" : "pending";
        }

        $activity = new ActivityLog;
        $supplierName = $company ? $company->name : $request->supplier;
        $activity->details = "Add Supplier " . $supplierName . " in inspiration entitled " . $inspiration->name;
        $activity->type ="inspiration";
        $activity->origin = "web";

        Auth::user()->activities()->save($activity);

        $supplier->inspiration_id = $id;
        $supplier->save();

        // send notification

        // $firebase = new FirebaseFMC;
        // $firebaseArray = array();

        // if($request->company_id){
        //     foreach($company->users()->has('firebases')->get() as $user){
        //         foreach($user->firebases as $firebaseAccount){
        //             array_push($firebaseArray, $firebaseAccount->firebase_id);
        //         }
        //     }

        //     $firebase->sendSingle(
        //         $firebaseArray,
        //         array(
        //             'title'         => 'Inspiration Tagged',  // title,
        //             'avatar'        => $company->image('logo')->path('32x32', '/images/placeholders/company-logo-32x32.png'),
        //             'message'       => $company->name . ' has tagged you in inspiration entitled ' . $inspiration->name,  // message,
        //             'notif_type'    => 'inspiration_tagged',
        //             'item_id'       => $inspiration->id,
        //             'link'          => route('site.inspiration.show', $inspiration->slug),
        //             'image'         => $inspiration->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),  // image
        //             'timestamp'     => Carbon::now()->toDateTimeString() // timestamp
        //         )
        //     );
        // }

        if(!$request->ajax()){
            return back();
        }
    }

    public function deleteSupplier($id){
        $supplier = SupplierInspiration::find($id);

        $activity = new ActivityLog;
        $supplierName = $supplier->company ? $supplier->company->name  : $supplier->name;
        $activity->details = "Deleted Inspiration Supplier " . $supplierName ." in inspiration entitled " . $supplier->inspiration->name;
        $activity->type ="inspiration";
        $activity->origin = "web";

        Auth::user()->activities()->save($activity);
        $supplier->delete();
        if(!request()->ajax()){
            return back();
        }
    }


    // function generate32(){
    //     $inspirations= Inspiration::all();
    //     foreach($inspirations as $inspiration){
    //         //for thumbnail
    //         $thumbnail = $inspiration->images('thumbnail')->first();
    //         if(!file_exists(public_path('/images/32x32/'. $thumbnail->path))){
    //             if(file_exists(public_path('/images/thumbnails/' . $thumbnail->path))){
    //                 $this->imageResize(public_path('/images/thumbnails/' . $thumbnail->path), 32, 32, false, public_path('/images/32x32/' . $thumbnail->path));
    //             }
    //         }

    //         // for gallery
    //         $galleries = $inspiration->images('gallery')->get();
    //         foreach($galleries as $gallery){
    //             if(!file_exists(public_path('/images/32x32/'. $gallery->path))){
    //                 if(file_exists(public_path('/images/gallery/'. $gallery->path))){
    //                     $this->imageResize(public_path('/images/gallery/'. $gallery->path), 32, 32, false, public_path('/images/32x32/' . $gallery->path));
    //                 }
    //             }
    //         }
    //     }

    //     return "Done";
    // }

    public function uploadgallery(Request $request, $id){
        ini_set('max_execution_time', 0);
        $this->validate($request, [
            'file' => "mimes:" . config('filesystems.accepted_image_types')
        ]);

        $inspiration = Inspiration::find($id);
        if($request->hasFile('file')){
            //add and upload
            $gallery = $inspiration->addImage("gallery", $request->file('file'), public_path('images/gallery'));

            list($width, $height) = getimagesize(public_path('images/gallery/' . $gallery->path));
            if($width > 900){
                $this->imageResize(public_path('images/gallery/' . $gallery->path), 900, null, true);
            }else if($height > 800){
                $this->imageResize(public_path('images/gallery/' . $gallery->path), null, 800, true);
            }

            $this->imageResize(public_path('images/gallery/' . $gallery->path), 32, 32, false, public_path('images/32x32/' . $gallery->path));
            $this->imageResize(public_path('images/gallery/' . $gallery->path), 250, 250, false, public_path('images/sm/' . $gallery->path));

            $inspiration->images()->save($gallery);
        }

        $activity = new ActivityLog;
        $activity->details = "Upload Inspiration Gallery entitled " . $inspiration->name;
        $activity->type ="inspiration";
        $activity->origin = "web";

        Auth::user()->activities()->save($activity);
        Cache::forget('inspiration.' . $inspiration->slug . '.gallery');

        if(!$request->ajax()){
            return back()->with('gallery-upload-success', 'Uploading success!');
        }
    }

    public function uploadVideo(Request $request, $id){
        $this->validate($request, [
            'video' => "max:120000|mimes:mp4"
        ]);

        $inspiration = Inspiration::find($id);

        if($request->hasFile('video')){
            //add and upload
            $video = $inspiration->addVideo($request->title, "inspiration_video", $request->file('video'), public_path('videos'));
            $inspiration->videos()->save($video);
        }

        $activity = new ActivityLog;
        $activity->details = "Upload video in Inspiration entitled " . $inspiration->name;
        $activity->type ="inspiration";
        $activity->origin = "web";

        Auth::user()->activities()->save($activity);
        Cache::forget('inspiration.' . $inspiration->slug . '.videos');
        if(!$request->ajax()){
            return back();
        }
    }

    public function deletegallery(Request $request, $id){
        $image = Image::find($id);
        $inspiration = $image->imagable;
        removeImage($image->path, 'gallery');
        $image->delete();

        // $activity = new ActivityLog;
        // $activity->details = "Deleted Inspiration Gallery entitled " . $inspiration->name;
        // $activity->type ="inspiration";
        // $activity->origin = "web";

        // Auth::user()->activities()->save($activity);
        Cache::forget('inspiration.' . $inspiration->slug . '.gallery');
        if(!$request->ajax()){
            return back();
        }
    }

    public function deletevideo(Request $request, $id){
        $video = Video::find($id);
        removeVideo($video->path, 'videos');
        $video->delete();

        // $activity = new ActivityLog;
        // $activity->details = "Deleted Inspiration video entitled " . $inspiration->name;
        // $activity->type ="inspiration";
        // $activity->origin = "web";

        // Auth::user()->activities()->save($activity);

        if(!$request->ajax()){
            return back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $inspiration = Cache::remember('dashboard-inspiration-' . $id, 60 * 60, function() use ($id){
                        return Inspiration::findOrFail($id);
                });

        $colors = Cache::remember('dashboard-colors', 60 * 60, function(){
                   return Category::whereType('color')->get();
                });

        $currentYear = date('Y');

        $years = array();
        for($i = $currentYear; $currentYear >= 2019; $currentYear--){
            array_push($years, $currentYear);
        }

        $lockingReasons = config('lockingreasons');


        return view('dashboard.inspiration.edit', compact('inspiration','colors', 'years', 'lockingReasons'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'thumbnail' => 'nullable|max:120000|mimes:' . config('filesystems.accepted_image_types'),
            'cover'    => 'nullable|max:120000|mimes:' . config('filesystems.accepted_image_types'),
            'colors'   => 'required',
            'category' => 'required',
            'style'    => 'required',
            'setting'  => 'required',
            'description' => 'required',
        ]);

        $inspiration = Inspiration::find($id);
        $inspiration->name = $request->name;
        $inspiration->project_year = $request->project_year;
        $inspiration->description = $request->description;
        $inspiration->is_published = $request->is_published ? 1 : 0;
        $inspiration->featured = $request->featured ? 1 : 0;
        $inspiration->is_lock = $request->is_lock ? 1 : 0;
        $inspiration->images_by = $request->images_by;


        if($request->venue_id){
            $inspiration->venue_id = $request->venue_id;
            $venue = Venue::Find($request->venue_id);
            $inspiration->location = $venue->name ?? "";

            //clearing geolocation items
            $inspiration->latitude = "";
            $inspiration->longitude = "";
        }

        if($request->is_lock){
            if($request->sendLockEmail){
                Mail::to($inspiration->company->email)
                // ->cc(['michael@designa.studio', 'nick@designa.studio', 'hello@designa.studio', 'jbdev21@gmail.com']) // remove in prod
                ->send(new ItemLockMail($inspiration->name, $inspiration->company->name,  $request->reason));
            }
        }

        if($request->latitude){
            $inspiration->location = $request->location;
            $inspiration->latitude = $request->latitude;
            $inspiration->longitude = $request->longitude;

            //clearing venue item connection via id
            $inspiration->venue_id = "";
        }

        $inspiration->save();

        //clear first all the attached categories
        $inspiration->clearAttachCategory(['color', 'cater', 'style', 'setting']);

        $inspiration->categories()->whereType('color')->attach($request->colors);
        $inspiration->categories()->whereType('cater')->attach($request->category);
        $inspiration->categories()->whereType('style')->attach($request->style);
        $inspiration->categories()->whereType('setting')->attach($request->setting);

        if($request->hasFile('thumbnail')){
            $inspiration->clearImage('thumbnail');
            $thumbnail = $inspiration->addImage("thumbnail", $request->file('thumbnail'), public_path('images/thumbnails'));
            list($width, $height) = getimagesize(public_path('images/thumbnails/' . $thumbnail->path));
            if($width > 900){
                $this->imageResize(public_path('images/thumbnails/' . $thumbnail->path), 900, null, true);
            }
            $inspiration->images()->save($thumbnail);
        }

        // if($request->cropped_image_cover){
        if($request->hasFile('cover')){
            $inspiration->clearImage('cover');
            $cover = $inspiration->addImage("cover", $request->file('cover'), public_path('images/covers'));
            $inspiration->images()->save($cover);
        }

        $activity = new ActivityLog;
        $activity->details = "Update Inspiration entitled " . $inspiration->name;
        $activity->type ="inspiration";
        $activity->origin = "web";

        Auth::user()->activities()->save($activity);


        $inspiration->clearCache();

        if($request->proceed){
            return redirect()->route('dashboard.inspiration.show', $inspiration->slug);
        }else{
            return back()->with('success', 'Changes applied!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $inspiration = Inspiration::find($id);

        //clear images
        $inspiration->clearImage('cover');
        $inspiration->clearImage('thumbnail');
        $inspiration->clearImage('gallery');
        $inspiration->clearVideo('inspiration_video');
        $inspiration->hearts()->delete();
        $inspiration->views()->delete();
        $inspiration->reviews()->delete();

        // inspiration
        $inspiration->clearAttachCategory(['color', 'cater', 'style', 'setting']);

        $activity = new ActivityLog;
        $activity->details = "Deleted Inspiration entitled " . $inspiration->name;
        $activity->type ="inspiration";
        $activity->origin = "web";

        Auth::user()->activities()->save($activity);

        $inspiration->delete();

        if(!request()->ajax()){
            return back();
        }
    }


    // Ajax calls
    public function caters()
    {
        return Auth::user()->caters()->with('settings')->with('styles')->get();
    }


    public function companies(Request $request){
        return Company::whereHas('categories', function($q) use ($request) {
            $q->where('id', $request->category);
        })->get();
    }

    public function venues(Request $request){
        return Venue::whereCompanyId($request->company)->whereHas('caters', function($q) use ($request){
            $q->where('id', $request->category);
        })->get();
    }

    public function roles(Request $request){
        return Company::where('name', 'LIKE', '%' . $request->name . '%')->whereHas('roles', function($q) use ($request) {
            $q->where('id', $request->role);
        })->get();
    }

}
