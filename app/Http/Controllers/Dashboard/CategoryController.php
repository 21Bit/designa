<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Middleware\SuperAdminMiddleware;
use App\Models\Category;
use App\Services\CategoryService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware([SuperAdminMiddleware::class]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $categories = Category::where('type', $request->type)->orderBy('name')->paginate(12);

        if($request->ajax()){
            return Category::where('type', $request->type)->orderBy('name')->with('settings')->with('styles')->get();
        }

        return view('dashboard.category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string',
            'type'  => 'required'
        ]);

        Category::create($request->except('_token'));

        return redirect()->route('dashboard.category.index', ['type' => $request->type, 'module' => $request->module]);
    }

    public function storeSubCategory(Request $request){
        $this->validate($request, [
            'category_id'   => 'required',
            'name'          => 'required',
            'module'        => 'required',
            'type'          => 'required'
        ]);

        $category = new Category;
        $category->name = $request->name;
        $category->description = $request->description;
        $category->parent_id = $request->category_id;
        $category->module = $request->module;
        $category->type = $request->type;

        if($request->credit_by){
            $category->credit_by = $request->credit_by;
            $category->credit_location = $request->credit_location;
        }

        $category->save();

        if($request->ajax()){
            return $category;
        }else{
            return back();
        }
    }
    public function updateSubCategory(Request $request){
        $this->validate($request, [
            'id'   => 'required',
            'name'          => 'required',
        ]);

        $category = Category::find($request->id);
        $category->name = $request->name;
        $category->description = $request->description;
        $category->save();

        if($request->ajax()){
            return $category;
        }else{
            return back();
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function subCategories($id){
        $category = Category::findOrFail($id);
        return $category->subCategories;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view('dashboard.category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|string'
        ]);

        Category::findOrFail($id)->update($request->except('_token'));

        return redirect()->route('dashboard.category.index', ['type' => Category::find($id)->type, 'module' => Category::find($id)->module, 'credits' => Category::find($id)->credit_by ? 1 : 0 ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();

        if(!request()->ajax()){
            return back();
        }
    }
}
