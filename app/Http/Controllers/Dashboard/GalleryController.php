<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GalleryController extends Controller
{
    function destroy(Request $request, $id){
        $image = Image::find($id);
        removeImage($image->path, 'gallery');
        $image->delete();


        if(!$request->ajax()){
            return back();
        }else{
            return response()->json([
                'message' => 'delete completed'
            ], 200);
        }
    }
}
