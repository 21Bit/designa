<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Middleware\SuperAdminMiddleware;
use DataTables;
use App\Models\Role;
use App\Models\Permission;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Builder;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware([SuperAdminMiddleware::class]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Builder $builder)
    {
        $roles = Role::all();
        return view('dashboard.role.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.role.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255|unique:roles,display_name',
            'description' => 'nullable|max:255'
        ]);

        $role = new Role;
        $role->name = snake_case($request->name);
        $role->display_name = $request->name;
        $role->description = $request->description;
        $role->credit_by = $request->credit_by;
        $role->credit_location = $request->credit_location;
        $role->save();

        return back();
        // return redirect()->route('dashboard.role.show', $role->id)->with('message', 'New Role Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::find($id);
        $permissions = Permission::all();
        return view('dashboard.role.show', compact('role', 'permissions'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);
        $permissions = Permission::all();
        return view('dashboard.role.edit', compact('role', 'permissions'));
    }

    public function assignPermission(Request $request, $id)
    {
        $role = Role::findOrFail($id);

        if($request->permissions){
            $role->syncPermissions($request->permissions);
        }else{
            $role->detachPermissions();
        }

        //async to user based on thier company role and permissions;
        $this->asyncToUser($role);

        return back();
    }

    public function asyncToUser(Role $role){
        foreach($role->companies()->get() as $company){
             $company->asyncRoleToUsers();
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:255|unique:roles,display_name,' . $id,
            // 'description' => 'required|max:255'
        ]);

        $role = Role::find($id);
        $role->name = snake_case($request->name);
        $role->display_name = $request->name;
        $role->description = $request->description;
        $role->credit_by = $request->credit_by;
        $role->credit_location = $request->credit_location;
        $role->save();

       // $role->syncPermissions($request->permissions);
        return redirect()->route('dashboard.role.show', $role->id)->with('message', 'New Role Created');
    }

}
