<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Blog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\HasImageIntervention;

class BlogController extends Controller
{
    use HasImageIntervention;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $blogs = Blog::orderBy('created_at', 'DESC')->paginate(5);
        $list = Blog::query();
        if($request->status){
            $list->where('is_published', $request->status == 2 ? 1 : 0);
        }
        if ($request->search){
            $list->where('title', 'LIKE', '%' . $request->search . '%');
        }
        $blogs = $list->orderBy('created_at', 'DESC')->paginate(5);

        return view('dashboard.blog.index', compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'category'          => 'required',
            'title'             => 'required',
            'description'       => 'required',
            'thumbnail'         => 'required|mimes:'. config('filesystems.accepted_image_types'),
        ]);

        $blog = new Blog;
        $blog->category = $request->category;
        $blog->title = $request->title;
        $blog->description = $request->description;
        $blog->is_published = $request->is_published;
        $blog->save();

        if($request->hasFile('thumbnail')){
            $thumbnail = $blog->addImage("thumbnail", $request->file('thumbnail'), public_path('images/blogs'));
            
            // resize image if the image exceed 800px then reduce to 800 ^_^
            list($width, $height) = getimagesize(public_path('images/blogs/' . $thumbnail->path));

            if($width > 1920){
                $this->imageResize(public_path('images/blogs/' . $thumbnail->path), 1920, null, true);
            }

            $blog->images()->save($thumbnail);
        }

        // return redirect()->route('dashboard.blog.index');
        return redirect()->route('dashboard.blog.show', $blog->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $blog = Blog::where('id',$id)->firstOrFail();
        $image = $blog->images()->where('type', 'thumbnail')->first();
        return view('dashboard.blog.show', compact('blog','image'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = Blog::findOrFail($id);
        return view('dashboard.blog.edit', compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'category'          => 'required',
            'title'             => 'required',
            'description'       => 'required',
            'thumbnail'         => 'nullable|mimes:'. config('filesystems.accepted_image_types'),
        ]);

        $blog = Blog::findOrFail($id);
        $blog->category = $request->category;
        $blog->title = $request->title;
        $blog->description = $request->description;
        $blog->is_published = $request->is_published;
        $blog->save();

        if($request->hasFile('thumbnail')){
            $blog->clearImage('thumbnail');
            
            $thumbnail = $blog->addImage("thumbnail", $request->file('thumbnail'), public_path('images/blogs'));
            
            // resize image if the image exceed 800px then reduce to 800 ^_^
            list($width, $height) = getimagesize(public_path('images/blogs/' . $thumbnail->path));

            if($width > 1920){
                $this->imageResize(public_path('images/blogs/' . $thumbnail->path), 1920, null, true);
            }

            $blog->images()->save($thumbnail);
        }

        // return redirect()->route('dashboard.blog.index');
        return redirect()->route('dashboard.blog.show', $blog->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog = Blog::find($id);
        $blog->delete();

        if(!request()->ajax()){
            return back();
        }
    }
}
