<?php

namespace App\Http\Controllers\Dashboard;

use Auth;
use App\Models\Venue;
use App\Models\ModelFile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FloorPlanModelController extends Controller
{
    public function __construct()
    {
        $this->middleware('access:create-your-own-event-3d-using-template');
    }

    public function index(Request $request)
    {
        $query = ModelFile::where('type', 'floor_plan')->has('modellable')->orderBy('created_at', 'DESC');
        if($request->tab == "my-floorplans") {
            $query->where('modellable_id', $request->user()->id);
        }elseif($request->tab == "all-floorplans"){
            if(!Auth::user()->is_superadmin){
                $query->where('modellable_id', $request->user()->id);
            }
        }else{
            $query->where('is_public', 1);
        }

        $models = $query->latest()->paginate(12);
        return view('dashboard.3d.floorplan', compact('models', ));
    }


    public function create()
    {
        //
    }



    public function generateTemplate($id)
    {
        $token =  Auth::user()->createToken('Designa Password Grant Client')->accessToken;
        $pagetitle = "Generate Template";
        $backURL = route('dashboard.floorplan.index');
        $params = "0%0%$id%venue_owner%floor_plan%$token%$backURL";
        return view('dashboard.3d.render', compact('params', 'pagetitle'));
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        $model = ModelFile::find($id);
        $decoration = ModelFile::find($model->decoration_template_id);
        $event_space_id =  ModelFile::find($decoration->base_layer_id)->modellable->id;
        $token =  Auth::user()->createToken('Designa Password Grant Client')->accessToken;
        $backURL = route('dashboard.floorplan.index');
        $pagetitle = $model->name;
        $params = "$event_space_id%$decoration->id%$id%venue_owner%floor_plan%$token%$backURL";
        return view('dashboard.3d.render', compact('params', 'pagetitle'));
    }

    public function edit($id)
    {
        $modelFile = ModelFile::find($id);
        $baseLayerId = ModelFile::find($modelFile->base_layer_id)->modellable_id;
        $decorationTemplateId = $modelFile->decoration_template_id;
        $pagetitle = $modelFile->name;
        $token =  Auth::user()->createToken('Designa Password Grant Client')->accessToken;
        $backURL = route('dashboard.floorplan.index');
        $params = "$baseLayerId%$decorationTemplateId%$id%venue_owner%floor_plan%$token%$backURL";
        return view('dashboard.3d.render', compact('params', 'pagetitle'));
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
         $user = Auth::user();
        return $user->deleteModel([
            'id' => $id
        ]);
    }
}
