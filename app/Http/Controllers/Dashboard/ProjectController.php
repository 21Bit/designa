<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Resources\Inspiration\Web\EventBoardInspirationSelection;
use App\Http\Resources\Inspiration\Web\EventBoardInspirationSelectionCollection;
use PDF;
use Auth;
use Cache;
use Storage;
use Carbon\Carbon;
use App\Models\Company;
use App\Models\Product;
use App\Models\Project;
use App\Models\Category;
use App\Models\Inspiration;
use Illuminate\Http\Request;
use App\Jobs\MakePitchPackPDFJob;
use Spatie\Browsershot\Browsershot;
use App\Http\Controllers\Controller;
use App\Http\Resources\Color\Category as ColorResource;
use App\Http\Resources\MoodBoard\Project as ProjectResouce;
use App\Http\Resources\MoodBoard\Web\Product as ProductWebResource;
use App\Http\Resources\MoodBoard\Web\Inspiration as InspirationWebResource;
use App\Http\Resources\Product\Web\EventBoardProductSelection;

class ProjectController extends Controller
{

    public function __construct()
    {
        $this->middleware('access:create-pitch-packs-in-designa');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $categories = Category::whereType('cater')->get();
        $key = 'dashboard.event-board.list.' . Auth::user()->id;
        $projects = Project::whereUserId(Auth::user()->id)
                ->withCount(['inspirationImages', 'productImages'])
                ->orderBy('created_at', 'DESC')->paginate(15);
        return view('dashboard.event-board.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function inspirations(Request $request){

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'description' => 'required'
        ]);

        $project = new Project;
        $project->title = $request->title;
        $project->description = $request->description;
        $project->user_id = $request->user()->id;
        $project->save();

        return redirect()->route('dashboard.event-board.edit.categories', $project->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = Project::whereSlug($id)->firstOrFail();
        $pitchpack_url = Storage::disk('pdf')->exists($project->slug . '.pdf') ? Storage::disk('pdf')->url($project->slug . '.pdf') : '';
        return view('dashboard.event-board.show', compact('project', 'pitchpack_url'));
    }

    public function preview($id){
        $project = Project::find($id);
        return view('dashboard.event-board.edit.preview', compact('project'));
    }

    public function generatePDf($id){
        $project = Project::find($id);
        $key = 'pitchpack-queue-' . $project->id;

        if(!Cache::get($key)){
            Cache::put('pitchpack-queue-' . $project->id, 'redis');
            MakePitchPackPDFJob::dispatch($project);
        }

        return back()->with('success', 'Your pitchpack is now on queue. We will notify you once your pitchpack is ready. Thank you.');
    }

    public function previewApi($id)
    {
        $project = Project::with(['categories', 'inspirationImages', 'productImages'])->findOrFail($id);
        return new ProjectResouce($project);
    }

    public function suppliers($id){
        $project = Project::find($id);
        $supplierListKey = 'dashboard.event-board.supplier.list.' . $project->id;
        $suppliers = Cache::rememberForever($supplierListKey, function() use($project) {
            $inspirations = $project->inspirationImages()->whereImagableType('App\Models\Inspiration')->groupBy('imagable_id')->pluck('imagable_id')->toArray();
            $products = $project->inspirationImages()->whereImagableType('App\Models\Product')->groupBy('imagable_id')->pluck('imagable_id')->toArray();

           return $suppliers =  Company::whereHas('inspirations', function($q) use ($inspirations){
                    $q->whereIn('id', $inspirations);
                })
                ->orWhereHas('products', function($q) use ($products){
                    $q->whereIn('id', $products);
                })
                ->get();

        });

        // dispatch(new MakePitchPackPDFJob($project));
        return view('dashboard.event-board.edit.supplier', compact('project', 'suppliers'));
    }

    /**
     * Show the form for editing the specified resource..
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    public function editCategories(Request $request){
        $project = Project::find($request->id);

        if(!$project){
            return redirect()->route('dashboard.event-board.index');
        }

        $event_types = Category::whereType('cater')->get();
        $colors = Category::whereType('color')->get();
        $settings = Category::whereType('setting')->get();
        $styles = Category::whereType('style')->get();

        $category_ids =  $project->categories()->pluck('id')->toArray();
        return view('dashboard.event-board.edit.categories', compact('event_types', 'colors', 'settings', 'styles', 'project', 'category_ids'));
    }

    public function editInspirations(Request $request, $id){
        $project = Project::find($id);
        if(!$project){
            return redirect()->route('dashboard.event-board.index');
        }

        if($request->ajax()){

            if($request->saved){
                $inspirationsList = $request->user()->heartedInspirations();
            }else{
                $inspirationsList = Inspiration::doesntHave('hearts')->orWhereHas('hearts', function($q){
                    $q->where('user_id', '!=',  Auth::user()->id);
                });
            }

            $event_types = $project->categories()->whereType('cater')->pluck('id')->toArray();
            $styles      = $project->categories()->whereType('style')->pluck('id')->toArray();
            $settings    = $project->categories()->whereType('setting')->pluck('id')->toArray();
            $colors      = $project->categories()->whereType('color')->pluck('id')->toArray();


            if($event_types){
                $inspirationsList->whereHas('categories',function($q) use ($event_types){
                    $q->whereType('cater')->whereIn('id', $event_types);
                });
            }

            // For Event Setting
            if($settings){
                $inspirationsList->whereHas('categories',function($q) use ($settings){
                    $q->whereType('setting')->whereIn('categories.id', $settings);
                });
            }

            // For Event Style
            if($styles){
                $inspirationsList->whereHas('categories',function($q) use ($styles){
                    $q->whereType('style')->whereIn('categories.id', $styles);
                });
            }


            // For Event Style
            if($colors){
                $inspirationsList->whereHas('categories',function($q) use ($colors){
                    $q->whereType('color')->whereIn('categories.id', $colors);
                });
            }

            $inspirations = $inspirationsList->orderBy('name', 'ASC')->paginate(10);
            return EventBoardInspirationSelection::collection($inspirations);
        }

        if($request->saved){
            return view('dashboard.event-board.edit.inspirations', compact('project'));
        }else{
            return view('dashboard.event-board.edit.inspiration-suggestion', compact('project'));
        }

    }


    public function editProducts(Request $request, $id){
        $project = Project::find($id);

        if($request->saved){
            return view('dashboard.event-board.edit.product-selection', compact('project'));
        }else{
            return view('dashboard.event-board.edit.product-selection-suggested', compact('project'));
        }

    }

    public function productsListing(Request $request, $id){
        $project = Project::find($id);

        if($request->saved){
            $productList = $request->user()->heartedProducts();
        }else{
            $productList = Product::doesnthave('hearts')->orWhereHas('hearts', function($q){
                $q->where('user_id', '!=',  Auth::user()->id);
            });
        }

        $event_types = $project->categories()->whereType('cater')->pluck('id')->toArray();
        $styles      = $project->categories()->whereType('style')->pluck('id')->toArray();
        $settings    = $project->categories()->whereType('setting')->pluck('id')->toArray();
        $colors      = $project->categories()->whereType('color')->pluck('id')->toArray();

        $products  = $productList->where('is_published', 1)->orderBy('name', 'ASC')->paginate(10);
        return EventBoardProductSelection::collection($products);
    }

    public function editCategoriesPost(Request $request, $id){
        $project = Project::find($id);
        $project->clearAttachCategory(['color', 'cater', 'style', 'setting']);
        $project->clearCache();

        $project->categories()->whereType('color')->attach($request->colors);
        $project->categories()->whereType('cater')->attach($request->event_type);
        $project->categories()->whereType('style')->attach($request->style);
        $project->categories()->whereType('setting')->attach($request->setting);
        $project->save();
        // dispatch(new MakePitchPackPDFJob($project));
        return redirect($request->redirect . '?saved=1');
    }

    public function attachImage(Request $request){

        $project = Project::find($request->project_id);
        if($request->attaching){
            $project->inspirationImages()->attach($request->image_id);
        }else{
            $project->inspirationImages()->detach($request->image_id);
        }

        Cache::forget('event-board.imagecount.inspiration.' . '.'. $project->id);
        Cache::forget('event-board.imagecount.product' . '.'. $project->id);
        Cache::forget('event-board.thumbnail.' . $project->id);

        return response()->json([
            'data' => [
                    'status' => 1,
                    'message'  => 'update complete',
                    'type'     => $request->attaching ? "attached" : 'detached'
                ]
        ], 200);
    }




    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = Project::find($id)->delete();
        return back()->with('message', 'Event Board Deleted!');
    }
}
