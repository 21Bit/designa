<?php

namespace App\Http\Controllers\Dashboard;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Notification\Dashboard\Notification;

class NotificationController extends Controller
{
    function index(Request $request){
       

        $notifications = Auth::user()->notifications()->paginate(15);
        return view("dashboard.notification.index", compact("notifications"));
    }

    function apiList(){
        return [
            'unread' => Auth::user()->unreadNotifications()->count(),
            'notifications' => Notification::collection(Auth::user()->notifications()->take(6)->get())
        ];
    }
}
