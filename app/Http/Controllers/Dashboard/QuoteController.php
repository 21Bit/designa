<?php

namespace App\Http\Controllers\Dashboard;

use DB;
use Auth;
use DataTables;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Qoute;
use App\Models\Company;
use App\Facades\FirebaseFMC;
use App\Models\QouteProduct;
use Illuminate\Http\Request;
use App\Models\PricingComponent;
use App\Models\QouteProductPrice;
use Yajra\DataTables\Html\Builder;
use App\Http\Controllers\Controller;
use App\Http\Resources\Attachment\Attachment;
use App\Notifications\Customer\QuoteResponseNotification;

class QuoteController extends Controller
{
    function index(Request $request){
        $pricingComponentsCount = DB::table('pricing_components')->count();
        $quotes = DB::table('qoutes')
                    ->select(
                            'qoutes.id as id',
                            'users.name as customer',
                            'qoutes.event_name as event_name',
                            'qoutes.created_at as created_at',
                            'qoutes.reference_number as reference_number',
                            DB::raw("(select count(*) from qoute_products join products on products.id = qoute_products.product_id where qoute_products.qoute_id = qoutes.id and products.company_id = companies.id) as product_counts"),
                            DB::raw("(select COUNT(*) from qoute_product_prices where qoute_product_id = qoute_products.id) as priced_component")
                            )
                    ->join('users', 'users.id', '=', 'qoutes.user_id')
                    ->join('qoute_products', 'qoute_products.qoute_id', '=', 'qoutes.id')
                    ->join('products', 'products.id', '=', 'qoute_products.product_id')
                    ->join('companies', 'companies.id', '=', 'products.company_id')
                    ->where('companies.id', Auth::user()->company_id)
                    ->orderBy('created_at', 'DESC')
                    ->paginate(20);

        return view('dashboard.quote.index', compact('quotes', 'pricingComponentsCount'));
    }



    function show(Request $request, $ref){
        $pricingcomponents = PricingComponent::whereActive(1)->get();
        $quote = Qoute::whereReferenceNumber($ref)->firstOrFail();
        $decors = $quote->decors()->whereHas('product',function($q){
                        $q->where('company_id', Auth::user()->company_id);
                    })->get();
         return view('dashboard.quote.show', compact('quote', 'decors','pricingcomponents'));
    }


    function savePricing(Request $request, $id){

        $quote = Qoute::find($id);
        $pricingcomponents = PricingComponent::whereActive(1)->get();

        foreach($quote->decors as $decor){
            //delete all the pring of this decor of this quote
            $decor->prices()->delete();

            foreach($pricingcomponents as $pricingcomponent){
                if($request->input('price-' . $decor->id .'-'. $pricingcomponent->id )){
                    $price = new QouteProductPrice;
                    $price->qoute_product_id = $decor->id;
                    $price->pricing_component_id = $pricingcomponent->id;
                    $price->price = $request->input('price-' . $decor->id .'-'. $pricingcomponent->id );
                    $price->save();
                }
            }
        }


        if($request->notify){
            $customer = User::find($quote->user_id);
            $company = Company::find(Auth::user()->company_id);
            $customer->notify(new QuoteResponseNotification($quote, $company));

            $firebaseArray = array();
            foreach($customer->firebases as $firebaseAccount){
                array_push($firebaseArray, $firebaseAccount->firebase_id);
            }

            $firebase = new FirebaseFMC;
            $firebase->sendSingle(
                    $firebaseArray,
                    array(
                        'title'         => 'Qoute Pricing Update',//$inspiration->title,  // title
                        'avatar'        => $company->image('logo')->path('logos', '/images/placeholders/placeholder.png'), //$inspiration->description,  // message,
                        'message'       => $company->name . " has updated pricing for your quotation for " . $quote->event_name, //$inspiration->description,  // message,
                        'notif_type'    => 'quote_pricing_update',
                        'item_id'       => $quote->reference_number,
                        'link'          => '',//route('site.inspiration.show', $inspiration->slug),
                        'image'         => $company->image('logo')->path('logos', '/images/placeholders/placeholder.png'),  // image
                        'timestamp'     => Carbon::now()->format('Y-m-d h:i:s') // timestamp
                    )
                );
        }

        \Session::flash('success', 'Changes Saved..');
        return back()->with('success', 'Changes Saved..');
    }



    function sendReview(Request $request){
        $quoteproduct = QouteProduct::find($request->id);
        $quoteproduct->saveReview($request, 'web');

        return response()->json([
            'status'    => 200,
            'message'   => 'review created',
        ]);

    }

    function getAttachment($id){
        $quoteproduct = QouteProduct::find($id);
        return Attachment::collection($quoteproduct->attachments()->orderBy("created_at")->get());
    }

    function uploadAttachment(Request $request){
        $quoteproduct = QouteProduct::find($request->id);
        $data = array(
            'filename' => $request->filename,
            'path'     => $request->file('file')->store('attachments'),
        );

        $file = $quoteproduct->saveAttachment($data);

        if($request->ajax()){
            return new Attachment($file);
        }else{
            return "yeah";
        }

    }
}
