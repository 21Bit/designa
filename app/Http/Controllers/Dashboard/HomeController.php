<?php

namespace App\Http\Controllers\Dashboard;

use Auth;
use App\Models\Venue;
use App\Models\Company;
use App\Models\Product;
use App\Models\Inspiration;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      
        if(request()->ajax()){
            $month          = $request->month ? $request->month : date('m');
            $year           = $request->year ? $request->year : date('Y');
            $monthname      = date('F',strtotime($year . '-' . 1));
            $company = Company::find($request->user()->company_id);
            
            // Get Years from 2019
            $currentYear = date('Y');
            $years = array();
            for($i = $currentYear; $currentYear >= 2019; $currentYear--){
                array_push($years, $currentYear);
            }
            

            if($request->datacategory == "profile"){
                return [           
                    'month'     =>  $month,
                    'year'      =>  $year,
                    'years'     =>  $years,

                    'components' => [
                        [
                            'id'    => 1,
                            'label' => 'Saved',
                            'icon'  => 'fa fa-heart',
                            'count' => $company->hearts()->whereMonth('created_at',$month)->whereYear('created_at', $year)->count(),
                        ],
                        [
                            'id'    => 2,
                            'label' => 'Views',
                            'icon'  => 'fa fa-eye',
                            'count' => $company->views()->whereMonth('created_at', $month)->whereYear('created_at', $year)->count(),
                        ],
                        [
                            'id'    => 3,
                            'label' => 'Reviews',
                            'icon'  => 'fa fa-comment',
                            'count' => $company->reviews()->whereMonth('created_at',$month)->whereYear('created_at', $year)->count(),
                        ],
                    ],
        
                    'stats' => [
                        'saved'     => $this->profileDailySaves($month, $year, $company),
                        // 'shares'    => $this->dailyShares($month, $year, $company),
                        'views'     => $this->profileDailyViews($month, $year, $company),
                        'reviews'   => $this->profileDailyReviews($month, $year, $company)
                    ]
                ];
            }elseif($request->datacategory == "inspiration"){
                $inspirations = Inspiration::where('company_id', Auth::user()->company_id)->get();
                return [
                    'month' => $month,
                    'year'  => $year,
                    'years'     =>  $years,
                    'components' => [
                        [
                            'id'    => 1,
                            'label' => 'Saved',
                            'icon'  => 'fa fa-heart',
                            'count' => $company->inspirationHeartsCounts($month, $year),
                        ],
                        [
                            'id'    => 2,
                            'label' => 'Shares',
                            'icon'  => 'fa fa-share',
                            'count' =>  $company->inspirationSharesCounts($month, $year),
                        ],
                        [
                            'id'    => 3,
                            'label' => 'Post',
                            'icon'  => 'fa fa-list',
                            'count' => $company->inspirations()->whereMonth('created_at', $month)->whereYear('created_at', $year)->count(),
                        ],
                        [
                            'id'    => 4,
                            'label' => 'Views',
                            'icon'  => 'fa fa-eye',
                            'count' => $company->inspirationViewsCounts($month, $year),
                        ],
                    ],
        
                    'stats' => [
                        'saved'     => $this->inspirationDailySave($month, $year, $company),
                        'shares'    => $this->inspirationDailyShares($month, $year, $company),
                        'post'      => $this->dailyInspirations($month, $year, $company),
                        'views'     => $this->inspirationDailyViews($month, $year, $company),
                    ],
                    'summary'   => $inspirations->map(function($q) use ($month, $year) {
                        return [
                            'id' => $q->id,
                            'name'  => $q->name,
                            'thumbnail'   => $q->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),
                            'components' => [
                                [ // for hearts
                                    'name' => 'saved',
                                    'icon' => 'fa fa-heart-o',
                                    'counts' => $q->hearts()->whereMonth('created_at',$year . '-' .$month)->whereYear('created_at', $year)->count(),
                                ],
                                [ // for shares
                                    'name' => 'shares',
                                    'icon' => 'fa fa-share',
                                    'counts' => $q->shares()->whereMonth('created_at',$year . '-' .$month)->whereYear('created_at', $year)->count(),
                                ],
                                [ // for views
                                    'name' => 'views',
                                    'icon' => 'fa fa-eye',
                                    'counts' => $q->views()->whereMonth('created_at',$year . '-' .$month)->whereYear('created_at', $year)->count(),
                                ],
                                
                            ]
                        ];
                    })  
                ];

            }elseif($request->datacategory == "venue"){
                $venues = Venue::where('company_id', Auth::user()->company_id)->get();
                return [
                    'month' => $month,
                    'year'  => $year,
                    'years'     =>  $years,
                    'components' => [
                        [
                            'id'    => 1,
                            'label' => 'Saved',
                            'icon'  => 'fa fa-eye',
                            'count' => $company->venueHeartsCounts($month, $year),
                        ],
                        [
                            'id'    => 2,
                            'label' => 'Shares',
                            'icon'  => 'fa fa-share',
                            'count' => $company->venueSharesCounts($month, $year),
                        ],
                        [
                            'id'    => 3,
                            'label' => 'Views',
                            'icon'  => 'fa fa-eye',
                            'count' => $company->venueViewsCounts($month, $year),
                        ],
                        [
                            'id'    => 4,
                            'label' => 'Reviews',
                            'icon'  => 'fa fa-comment',
                            'count' => $company->venueReviewsCounts($month, $year),
                        ],

                    ],
                    'stats' => [
                        'saved'     => $this->venueDailySaves($month, $year, $company),
                        'shares'    => $this->venueDailyShares($month, $year, $company),
                        'views'     => $this->venueDailyViews($month, $year, $company),
                        'reviews'   => $this->venueDailyReviews($month, $year, $company)
                    ],
                    'summary'   => $venues->map(function($q) use ($month, $year) {
                        return [
                            'id' => $q->id,
                            'name'  => $q->name,
                            'thumbnail'   => $q->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),
                            'components' => [
                                [ // for hearts
                                    'name' => 'saved',
                                    'icon' => 'fa fa-heart-o',
                                    'counts' => $q->hearts()->whereMonth('created_at',$year . '-' .$month)->whereYear('created_at', $year)->count(),
                                ],
                                [ // for shares
                                    'name' => 'shares',
                                    'icon' => 'fa fa-share',
                                    'counts' => $q->shares()->whereMonth('created_at',$year . '-' .$month)->whereYear('created_at', $year)->count(),
                                ],
                                [ // for views
                                    'name' => 'views',
                                    'icon' => 'fa fa-eye',
                                    'counts' => $q->views()->whereMonth('created_at',$year . '-' .$month)->whereYear('created_at', $year)->count(),
                                ],
                                [ // for reviews
                                    'name' => 'reviews',
                                    'icon' => 'fa fa-comment',
                                    'counts' => $q->reviews()->whereMonth('created_at',$year . '-' .$month)->whereYear('created_at', $year)->count(),
                                ],
                                
                            ]
                        ];
                    })  
                ];
            }elseif($request->datacategory == "decor"){
                $decors = Product::where('company_id', Auth::user()->company_id)->get();
                return [
                    'month' => $month,
                    'year'  => $year,
                    'years'     =>  $years,
                    'components' => [
                        [
                            'id'    => 1,
                            'label' => 'Saved',
                            'icon'  => 'fa fa-heart',
                            'count' => $company->decorHeartsCounts($month, $year),
                        ],
                        [
                            'id'    => 2,
                            'label' => 'Views',
                            'icon'  => 'fa fa-eye',
                            'count' => $company->decorViewsCounts($month, $year),
                        ],
                        [
                            'id'    => 3,
                            'label' => 'Reviews',
                            'icon'  => 'fa fa-comment',
                            'count' => $company->decorReviewsCounts($month, $year),
                        ],
                    ],
                    'stats' => [
                        'saved'     => $this->decorDailySaves($month, $year, $company),
                        'views'     => $this->decorDailyViews($month, $year, $company),
                        'reviews'   => $this->decorDailyReviews($month, $year, $company)
                    ],
                    'summary'   => $decors->map(function($q) use ($month, $year) {
                        return [
                            'id' => $q->id,
                            'name'  => $q->name,
                            'thumbnail'   => $q->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),
                            'components' => [
                                [ // for hearts
                                    'name' => 'saved',
                                    'icon' => 'fa fa-heart-o',
                                    'counts' => $q->hearts()->whereMonth('created_at',$year . '-' .$month)->whereYear('created_at', $year)->count(),
                                ],
                                [ // for views
                                    'name' => 'views',
                                    'icon' => 'fa fa-eye',
                                    'counts' => $q->views()->whereMonth('created_at',$year . '-' .$month)->whereYear('created_at', $year)->count(),
                                ],
                                [ // for reviews
                                    'name' => 'reviews',
                                    'icon' => 'fa fa-comment',
                                    'counts' => $q->reviews()->whereMonth('created_at',$year . '-' .$month)->whereYear('created_at', $year)->count(),
                                ],
                            ]
                        ];
                    })  
                ];
            }

        }

        return view('dashboard.index');

    }


    /*
     * For Profiling
     * 
     * 
    */

    function profileDailySaves($month, $year, $source){  
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);  

        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $date = date('Y-m-d', strtotime($year .'-' . $month . '-'. $i));
            $array = [
                'date'      => $i,
                'web'       => $source->hearts()->whereDate('created_at', $date)->whereOrigin('web')->count(),
                'mobile'    => $source->hearts()->whereDate('created_at', $date)->whereOrigin('mobile')->count(),
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }

    function profileDailyViews($month, $year, $source){  
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);  

        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $date = date('Y-m-d', strtotime($year .'-' . $month . '-'. $i));
            $array = [
                'date'      => $i,
                'web'       => $source->views()->whereDate('created_at', $date)->whereOrigin('web')->count(),
                'mobile'    => $source->views()->whereDate('created_at', $date)->whereOrigin('mobile')->count(),
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }


    function profileDailyReviews($month, $year, $source){  
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);  

        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $date = date('Y-m-d', strtotime($year .'-' . $month . '-'. $i));
            $array = [
                'date'      => $i,
                'web'       => $source->reviews()->whereOrigin('web')->whereDate('created_at',$date)->count(),
                'mobile'    => $source->reviews()->whereOrigin('mobile')->whereDate('created_at',$date)->count(),
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }


    /*
    * End of Profile
    */


    
    /*
     * For Inspiration
     * 
     * 
    */

    function inspirationDailyShares($month, $year, $source){  
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);  

        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $date = date('Y-m-d', strtotime($year .'-' . $month . '-'. $i));
            $array = [
                'date'      => $i,
                'web'       => $source->inspirationSharesCounts(null, null, $date, 'web') ,
                'mobile'    => $source->inspirationSharesCounts(null, null, $date, 'mobile'),
                // 'mobile'    => $type == "post" ? $sourceData->where('origin', '')->whereMonth('created_at', $month)->whereYear('created_at', $year)->count() : 0
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }

    function inspirationDailySave($month, $year, $source){  
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);  
        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $date = date('Y-m-d', strtotime($year .'-' . $month . '-'. $i));
            $array = [
                'date'      => $i,
                'web'       => $source->inspirationHeartsCounts(null, null, $date, 'web'),
                'mobile'       => $source->inspirationHeartsCounts(null, null, $date, 'mobile'),
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }
    
    function inspirationDailyViews($month, $year, $source){  
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);  
        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $array = [
                'date'      => $i,
                'web'       => $source->inspirationViewsCounts($month, $year, $year .'-' . $month . '-'. $i, 'web'),
                'mobile'       => $source->inspirationViewsCounts($month, $year, $year .'-' . $month . '-'. $i, 'mobile'),
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }



    function dailyInspirations($month, $year, $source){  
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);  
        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $array = [
                'date'      => $i,
                'web'       => $source->inspirations()->whereDate('created_at', $year .'-' . $month . '-'. $i)->count(),
                'mobile'    => 0
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }

    /*
    * End of Profile
    */


    /*
     * For Venue
     * 
     * 
    */
    function venueDailyShares($month, $year, $source){  
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);  

        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $date = date('Y-m-d', strtotime($year .'-' . $month . '-'. $i));
            $array = [
                'date'      => $i,
                'web'       => $source->venueSharesCounts(null, null, $date, 'web'),
                'mobile'       => $source->venueSharesCounts(null, null, $date, 'mobile'),
                // 'mobile'    => $type == "post" ? $sourceData->where('origin', '')->whereMonth('created_at', $month)->whereYear('created_at', $year)->count() : 0
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }

    function venueDailySaves($month, $year, $source){  
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);  

        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $date = date('Y-m-d', strtotime($year .'-' . $month . '-'. $i));
            $array = [
                'date'      => $i,
                'web'       => $source->venueHeartsCounts(null, null, $date, 'web'),
                'mobile'       => $source->venueHeartsCounts(null, null, $date, 'mobile'),
                // 'mobile'    => $type == "post" ? $sourceData->where('origin', '')->whereMonth('created_at', $month)->whereYear('created_at', $year)->count() : 0
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }

    function venueDailyReviews($month, $year, $source){  
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);  

        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $date = date('Y-m-d', strtotime($year .'-' . $month . '-'. $i));
            $array = [
                'date'      => $i,
                'web'       => $source->venueReviewsCounts(null, null, $date, 'web'),
                'mobile'       => $source->venueReviewsCounts(null, null, $date, 'mobile'),
                // 'mobile'    => $type == "post" ? $sourceData->where('origin', '')->whereMonth('created_at', $month)->whereYear('created_at', $year)->count() : 0
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }
    function venueDailyViews($month, $year, $source){  
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);  

        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $date = date('Y-m-d', strtotime($year .'-' . $month . '-'. $i));
            $array = [
                'date'      => $i,
                'web'       => $source->venueViewsCounts(null, null, $date, 'web'),
                'mobile'       => $source->venueViewsCounts(null, null, $date, 'mobile'),
                // 'mobile'    => $type == "post" ? $sourceData->where('origin', '')->whereMonth('created_at', $month)->whereYear('created_at', $year)->count() : 0
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }


       /*
    * End of Venue
    */


    /*
     * For Decor/Product
     * 
     * 
    */

    // function decorDailyShares($month, $year, $source){  
    //     $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);  

    //     $dateData = array();

    //     for($i=1; $numberOrDays + 1 > $i; $i++){
    //         $date = date('Y-m-d', strtotime($year .'-' . $month . '-'. $i));
    //         $array = [
    //             'date'      => $i,
    //             'web'       => $source->decorSharesCounts(null, null, $date, 'web'),
    //             'mobile'       => $source->decorSharesCounts(null, null, $date, 'mobile'),
    //             // 'mobile'    => $type == "post" ? $sourceData->where('origin', '')->whereMonth('created_at', $month)->whereYear('created_at', $year)->count() : 0
    //         ];

    //         array_push($dateData, $array);
    //     }

    //     return $dateData;
    // }

    function decorDailySaves($month, $year, $source){  
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);  

        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $date = date('Y-m-d', strtotime($year .'-' . $month . '-'. $i));
            $array = [
                'date'      => $i,
                'web'       => $source->decorHeartsCounts(null, null, $date, 'web'),
                'mobile'       => $source->decorHeartsCounts(null, null, $date, 'mobile'),
                // 'mobile'    => $type == "post" ? $sourceData->where('origin', '')->whereMonth('created_at', $month)->whereYear('created_at', $year)->count() : 0
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }

    function decorDailyViews($month, $year, $source){  
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);  

        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $date = date('Y-m-d', strtotime($year .'-' . $month . '-'. $i));
            $array = [
                'date'      => $i,
                'web'       => $source->decorViewsCounts(null, null, $date, 'web'),
                'mobile'       => $source->decorViewsCounts(null, null, $date, 'mobile'),
                // 'mobile'    => $type == "post" ? $sourceData->where('origin', '')->whereMonth('created_at', $month)->whereYear('created_at', $year)->count() : 0
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }

    function decorDailyReviews($month, $year, $source){  
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);  

        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $date = date('Y-m-d', strtotime($year .'-' . $month . '-'. $i));
            $array = [
                'date'      => $i,
                'web'       => $source->decorReviewsCounts(null, null, $date, 'web'),
                'mobile'       => $source->decorReviewsCounts(null, null, $date, 'mobile'),
                // 'mobile'    => $type == "post" ? $sourceData->where('origin', '')->whereMonth('created_at', $month)->whereYear('created_at', $year)->count() : 0
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }



    function loggedUser(){
        $user = Auth::user();
        return [
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'company_id' => $user->company_id,
            'company' => $user->company,
            'avatar' => $user->getProfilePicture(),
        ];
    }
}
