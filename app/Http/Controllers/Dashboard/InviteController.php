<?php

namespace App\Http\Controllers\Dashboard;

use Auth;
use Mail;
use App\Models\ActivityLog;
use Illuminate\Http\Request;
use App\Mail\Dashboard\InviteMail;
use App\Http\Controllers\Controller;

class InviteController extends Controller
{
    function invite(){
        return view('dashboard.invite.supplier');
    }

    function inviteSend(Request $request){

        $this->validate($request,[
            'first_name'    => 'required',
            'last_name'     => 'required',
            'email'         => 'required|email'
        ]);


        $data = [
            'owner_company_name' => optional($request->user()->company)->name,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
        ];

        Mail::to($request->email)
            ->send(new InviteMail($data));

            // check for failures
        if (Mail::failures()) {
            return back()->withErrors();
        }else{
            
            $text = "Invite " . $request->firs_name . ' ' . $request->last_name . ' with email: ' .  $request->email;

            if($request->company){
                $text .= ' and company: ' . $request->company;
            }

            $activity = new ActivityLog;
            $activity->details = $text;
            $activity->type ="INVITE";
            $activity->origin = "web";

            Auth::user()->activities()->save($activity);
            
            if($request->ajax()){
                return response()->json([
                    'message' => 'Email invitation sent'
                ], 200);
            }else{
                return back()->with('success', 'Email sent!');
            }
        }
    }
}
