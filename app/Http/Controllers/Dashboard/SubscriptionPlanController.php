<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Middleware\SuperAdminMiddleware;
use DB;
use Cache;
use App\Models\Access;
use Stripe\StripeClient;
use Illuminate\Http\Request;
use App\Models\SubscriptionPlan;
use App\Http\Controllers\Controller;
use App\Models\SubscriptionPlanPrice;

class SubscriptionPlanController extends Controller
{
    protected $stripe;

    public function __construct()
    {
        $this->middleware([SuperAdminMiddleware::class]);
        $this->stripe = new StripeClient(env('STRIPE_SECRET'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $accesses = Access::all();
        $plans = SubscriptionPlan::where('type', 'basic')->with('prices')->get();
        $eventSpaces = SubscriptionPlan::where('type', 'event-spaces')->with('prices')->get();
        $venues = SubscriptionPlan::where('type', 'venues')->with('prices')->get();
        return view('dashboard.subscription-plan.index', compact('plans', 'accesses', 'eventSpaces', 'venues'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $accesses = Access::all();
        return view('dashboard.subscription-plan.create', compact('accesses'));
    }


    public function addPrice(Request $request){
        return DB::transaction(function() use ($request){
            try{
                if($request->stripe_id){
                    $stripeId = $request->stripe_id;
                }else{
                    $price = $this->stripe->price->create([
                        'unit_amount' => $request->price * 100,
                        'currency' => 'aud',
                        'interval' => $request->billing_period,
                        'recurring' => ['interval' => $request->billing_period], //  it can be day,week,month or year
                        'product' => $request->product_stripe_id,
                    ]);
                    $stripeId = $price->id;
                }

                SubscriptionPlanPrice::create([
                    'subscription_plan_id' => $request->plan_id,
                    'stripe_id' => $stripeId,
                    'billing_period' => $request->billing_period,
                    'price' => $request->price
                ]);

                return back()->with('success', 'Price saved successfully!');
            }catch (\Exception $exception){
                return back()->with('error', $exception->getMessage());
            }
        });

    }

    public function updatePrice(Request $request, $id){
        return DB::transaction(function() use ($request, $id){
            try{
                $price = SubscriptionPlanPrice::find($id);
                if($request->stripe_id){
                    $stripeId = $request->stripe_id;
                }else{
                    $price = $this->stripe->plans->create([
                        'amount' => $request->price * 100,
                        'currency' => 'aud',
                        'interval' => $request->billing_period, //  it can be day,week,month or year
                        'product' => $request->product_stripe_id,
                    ]);
                    $stripeId = $price->id;
                }

                $price->update([
                    'subscription_plan_id' => $request->plan_id,
                    'stripe_id' => $stripeId,
                    'billing_period' => $request->billing_period,
                    'price' => $request->price
                ]);

                return back()->with('success', 'Price updated successfully!');
            }catch (\Exception $exception){
                return back()->with('error', $exception->getMessage());
            }
        });

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return DB::transaction(function() use ($request){
            //create stripe product
            if($request->stripe_plan){
                $id = $request->stripe_id;
            }else{
                //Stripe Product Creation
                $product = $this->stripe->products->create([
                    'name' => $request->name,
                ]);

                $id = $product->id;
            }

            $subPlan = SubscriptionPlan::create([
                'stripe_id' => $id,
                'name' => $request->name,
                'type' => $request->type,
                'description'   => $request->description
            ]);

            if($request->accesses){
                $subPlan->accesses()->attach($request->accesses);
            }

            return redirect()->route('dashboard.subscription-plan.index')->with('message', $request->name . ' is created!');
        });


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SubscriptionPlan $plan)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $plan = SubscriptionPlan::find($id);
        $accesses = Access::all();
        return view('dashboard.subscription-plan.edit', compact('plan', 'accesses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

       return \DB::transaction(function() use ($request, $id){
            $subPlan = SubscriptionPlan::find($id);
            if($request->stripe_id){
                $stripeId = $request->stripe_id;
            }else{
                // Create Product in Stripe
                $product = $this->stripe->products->create([
                    'name' => $request->name,
                ]);

                $stripeId = $product->id;
            }

            $subPlan->stripe_id = $stripeId;
            $subPlan->name = $request->name;
            $subPlan->description = $request->description;
            $subPlan->save();

            if($request->accesses){
                $subPlan->accesses()->detach();
                $subPlan->accesses()->attach($request->accesses);
                Cache::flush('subscription.access.*');
            }


            return back()->with('success', $request->name . ' is updated!');
        });
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subPlan = SubscriptionPlan::find($id);
        return DB::transaction(function() use ($subPlan){
            try {
                if($this->stripe->plans->delete($subPlan->stripe_plan)){
                    $subPlan->delete();
                }

                $subPlan->delete();
                if(!request()->ajax()){
                    return back()->with('message', 'Plan Deleted');
                }

                return response()->json([
                    'status' => 200,
                    'message' => 'Plan is Deleted!'
                ], 200);
            } catch (\Exception $e) {

                if(!request()->ajax()){
                    return back()->with('error', $e->getMessage());
                }

                return response()->json([
                    'status' => 500,
                    'message' => $e->getMessage()
                ], 500);
            }

        });

    }
}
