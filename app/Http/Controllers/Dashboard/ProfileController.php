<?php

namespace App\Http\Controllers\Dashboard;

use Auth;
use Hash;
use App\Models\User;
use App\Models\Image;
use App\Models\Company;
use App\Models\Location;
use App\Models\AreaLocation;
use Illuminate\Http\Request;
use App\Models\SubscriptionPlan;
use App\Http\Controllers\Controller;
use App\Traits\HasImageIntervention;
use Carbon\Carbon;
use Stripe\Exception\ApiErrorException;

class ProfileController extends Controller
{
    use HasImageIntervention;

    function index(Request $request){
        if($request->tab == "business"){
            $company = Company::find(Auth::user()->company_id);
            $user = Auth::user();
            $arealocations = $company->locations;
            return view('dashboard.profile.index', compact('company','user', 'arealocations'));
        }elseif($request->tab == "account"){
            $user = Auth::user();
            if(Auth::user()->type == "administrator"){
                $companies = Company::all();
                return view('dashboard.profile.account', compact('user', 'companies'));
            }
            return view('dashboard.profile.account', compact('user'));
        }elseif ($request->tab == "subscription"){
            return $this->subscription();
        }else{
            $user = Auth::user();
            return view('dashboard.profile.change-password', compact('user'));
        }
    }

    function subscription(){
        $user = Auth::user();
//        $invoices = $user->invoices();
        $paymentMethods = $user->user_payment_methods;

        $intent = $user->createSetupIntent();
        $plans = SubscriptionPlan::all();
        return view('dashboard.profile.subscription', compact('user', 'paymentMethods', 'intent', 'plans'));
    }


    function getLocations($id){
        $company = Company::find($id);
        return $company->locations->map(function($e){
            return [
                'id' => $e->id,
                'address' => $e->address,
                'longitude' => $e->longitude,
                'latitude' => $e->latitude,
            ];
        });
    }

    function modules(){
        $company = Company::find(Auth::user()->id);
        Auth::user()->allPermissions()->where('name', '!=', 'setting')->where('name', '!=', 'company') // disregard the setting and company
        ->map(function($q){
            return [
                'id' => $q->id,
                'name' => $q->name == 'product' ? "decor" : $q->name
            ];
        });
    }

    function addAreaLocation(Request $request, $id){
        $location = new Location();
        $location->address = $request->address;
        $location->latitude = $request->latitude;
        $location->longitude = $request->longitude;
        $company = Company::find($id);
        $company->locations()->save($location);
        return $location;
    }

    function deleteLocation($id){
        $location = Location::find($id);
        $location->delete();
        return response()->json([],200);
    }

    function update(Request $request){

        if($request->account_information)
        {

            $this->validate($request, [
                // 'will_travel'        => 'required',
                'services'              => 'required',
                // 'recognition'        => 'required',
                // 'cropped_image_cover'   => 'required',
                // 'cropped_image_logo'    => 'required',
                'town'                  => 'required',
                'state'                 => 'required',
                'country'               => 'required',
                'post_code'             => 'required',
                // 'telephone_number'      => 'required',
                'email'                 => 'nullable|email|unique:companies,email,' . $request->user()->company->id,
                'facebook_link'         => 'url|nullable',
                'twitter_link'          => 'url|nullable',
                'instagram_link'        => 'url|nullable',
                'youtube_link'          => 'url|nullable',
            ]);

            $company = Company::find($request->user()->company_id);
            $company->will_travel = $request->will_travel ? 1 : 0;
            $company->services = $request->services;
            $company->recognition = $request->recognition;
            $company->description = $request->description;
            $company->is_active = $request->is_active ? 1 : 0 ;
            $company->single_venue = $request->single_venue ? 1 : 0 ;

            $company->email = $request->email;
            $company->telephone_number = $request->telephone_number;
            $company->facebook_link = $request->facebook_link;
            $company->twitter_link = $request->twitter_link;
            $company->instagram_link = $request->instagram_link;
            $company->youtube_link = $request->youtube_link;

            //
            $company->town = $request->town;
            $company->state = $request->state;
            $company->country = $request->country;
            $company->post_code = $request->post_code;

        }

        $company->save();


        if($request->cropped_image_logo){
             // clear company logo
            $company->clearImage('logo');
            $image = $request->cropped_image_logo;

            list($type, $image) = explode(';', $image);
            list(, $image)      = explode(',', $image);
            $image = base64_decode($image);

            $logo = time().'.png';
            $logo_path = public_path('images/logos/'. $logo);
            file_put_contents($logo_path, $image);

            $this->imageResize($logo_path, 32, 32, true, public_path('images/32x32/'. $logo));
            $logoimage = new Image;
            $logoimage->path = $logo;
            $logoimage->type = "logo";

            $company->images()->save($logoimage);

        }

        if($request->hasFile('cover')){
            //clear cover images of company
            $company->clearImage('cover');

            //Upload and add image
            $cover = $company->addImage("cover", $request->file('cover'), public_path('images/covers'));

            $company->images()->save($cover);


             // clear company logo
            // $company->clearImage('cover');
            // $image = $request->cropped_image_cover;

            // list($type, $image) = explode(';', $image);
            // list(, $image)      = explode(',', $image);
            // $image = base64_decode($image);


            // $cover = time().'.png';
            // $cover_path = public_path('images/covers/'. $cover);
            // file_put_contents($cover_path, $image);

            // $coverimage = new Image;
            // $coverimage->path = $cover;
            // $coverimage->type = "cover";

            // $company->images()->save($coverimage);
        }

        return back()->with('success', 'Changes Saved..');
    }

    function updateAccount(Request $request){
        $user = User::find($request->user()->id);
        $user->name = $request->name;
        $user->email = $request->email;

        if($request->company){
            $user->company_id = $request->company;
        }

        $user->save();

        if($request->picture){
            $image = $request->picture;
            $user->uploadPicture($request, true, $image);
        }

        return back()->with('success', 'Account Changes Saved..');

    }

    function changePassword(Request $request){
        $this->validate($request,[
            'password' => 'required|string|min:6|confirmed',
        ]);


        if (Hash::check($request->current_password, Auth::user()->password))
        {
            $user = User::find(Auth::user()->id);
            $user->password = bcrypt($request->password);
            $user->save();
            return back()->with('success', 'Password successfuly changed');
        }else{
            return back()->with('warning', 'Current password is incorrect');
        }
    }



}
