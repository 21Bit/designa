<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Product;
use Auth;
use Notification;
use Carbon\Carbon;
use App\Models\Qoute;
use App\Models\Category;
use App\Models\ModelFile;
use App\Models\QouteProduct;
use Illuminate\Http\Request;
use App\Models\FunctionSpace;
use App\Http\Controllers\Controller;
use App\Notifications\Supplier\Qoute\NewQouteNotification;

class TemplateModelController extends Controller
{

    public function __construct()
    {
        $this->middleware('access:create-your-own-event-3d-using-template');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index(Request  $request)
    {
        $query = ModelFile::where('type', 'decoration')
            ->has('modellable')
            ->orderBy('created_at', 'DESC');
        if($request->tab == "my-templates") {
            $query->where('modellable_id', $request->user()->id);
        }elseif($request->tab == "all-templates"){
            if(!Auth::user()->is_superadmin){
                $query->where('modellable_id', $request->user()->id);
            }
        }else{
            $query->where('is_public', 1);
        }

        if($request->q){
            $query->where('model_name', 'LIKE', "%$request->q%");
        }

        $models = $query->latest()->paginate(20);
        $eventTypes = Category::where('type', 'cater')->get();
        return view('dashboard.3d.template', compact('models', 'eventTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }


    function getProducts($id){
        $model = ModelFile::find($id);
        $modelIds = collect($model->product_list)->pluck('id');
        $products =  Product::whereIn('id', $modelIds)->orderBy('name')->get()->map(function($q) use ($model) {
            return [
                'id' => $q->id,
                'name' => $q->name,
                'categories' => $q->categoryList('style', '/'),
                'thumbnail' => $q->thumbnail,
                'quantity'  => array_search($q->id,array_column($model->product_list, 'id')),
                'company'   => [
                    'name' => $q->company->name,
                    'thumbnail' => $q->company->image('logo')->path('32x32', '/images/placeholders/placeholder.png'),
                    'url' => route('site.supplier.show', $q->company->slug)
                ]
            ];
        });

        return [
            'model' => $model,
            'products' => $products
        ];
    }


    function generateQuote(Request $request){

        $modelFile = ModelFile::find($request->model_id);

        $quote = new Qoute;
        $quote->reference_number    = time() . rand(10*45, 100*98);
        $quote->user_id             = $request->user()->id;
        $quote->event_name          = $request->event_name;
        $quote->event_type          = $request->event_type;
        $quote->location            = $request->location;
        $quote->event_date          = $request->event_date;
        $quote->time_start          = $request->time_start;
        $quote->time_end            = $request->time_end;
        $quote->number_of_guests    = $request->number_of_guests;
        $quote->save();

        for($i = 0; count($request->products) > $i; $i++){
            $quoteProduct = new QouteProduct;
            $quoteProduct->product_id = $request->products[$i];
            $quoteProduct->qoute_id = $quote->id;
            $quoteProduct->quantity = $request->quantity[$i];
            $quoteProduct->save();
        }
        // foreach($modelFile->product_list as $decor){
        //     $quoteProduct = new QouteProduct;
        //     $quoteProduct->product_id = $decor['id'];
        //     $quoteProduct->qoute_id = $quote->id;
        //     $quoteProduct->quantity = $decor['count'];
        //     $quoteProduct->save();
        //     $counter++;
        // }

        // $companies = $quote->suppliers();
        // foreach($companies as $company){
        //     $company->users->map(function($q) use ($quote){
        //             $q->mobileNotify(
        //                 [
        //                 'title'         => 'New Quote',  // title,
        //                 'avatar'        => $quote->user->getProfilePicture(),
        //                 'message'       => $quote->user->name . ' had new quote entitled <b>' . $quote->event_name . '</b>',  // message,
        //                 'notif_type'    => 'new_qoute',
        //                 'item_id'       => $quote->id,
        //                 'link'          => route('dashboard.quote.show', $quote->reference_number),
        //                 'image'         => '',  // image
        //                 'read_at'       => '',
        //                 'timestamp'     => Carbon::now()->toIso8601String() // timestamp
        //             ]);
        //     });
        //     Notification::send($company->users, new NewQouteNotification($quote));
        // }


        if($request->ajax()){
            return  response()->json(
                [
                'data' => '',
                'message' => 'Quotation saved successfully'
            ], 200);
        }else{
            return back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $functionspace = FunctionSpace::find($id);
        $decoration = ModelFile::find($id);
        $event_space_id =  ModelFile::withTrashed()->find($decoration->base_layer_id)->modellable->id;
        $token =  Auth::user()->createToken('Designa Password Grant Client')->accessToken;
        $module = 'decoration_layer';
        $backURL = route('dashboard.template.index');
        $params = "$event_space_id%$id%0%venue_owner%$module%$token%$backURL";
        return view('dashboard.3d.render', compact('params'));
    }

    public function generateFloorplan(Request $request, $id){
        $decoration = ModelFile::find($id);
        $event_space_id =  ModelFile::find($decoration->base_layer_id)->modellable->id;
        $token =  Auth::user()->createToken('Designa Password Grant Client')->accessToken;
        $backURL = route('dashboard.floorplan.index');
        $pagetitle = "Generate Floor Plan";
        $params = "$event_space_id%$id%0%venue_owner%floor_plan%$token%$backURL";
        return view('dashboard.3d.render', compact('params', 'pagetitle'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit($id)
    {
        $decoration = ModelFile::find($id);
        $baseLayerId = ModelFile::find($decoration->base_layer_id)->modellable_id;
        $token =  Auth::user()->createToken('Designa Password Grant Client')->accessToken;
        $backURL = route('dashboard.template.index');
        $pagetitle = $decoration->name;
        $params = "$baseLayerId%$id%0%venue_owner%decoration_layer%$token%$backURL";
        return view('dashboard.3d.render', compact("params", "pagetitle"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return "update";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $beenUsed = ModelFile::where('decoration_template_id', $id)
                ->withTrashed()->count();

        if($beenUsed){
            ModelFile::find($id)->delete();
        }else{
            ModelFile::find($id)->forceDelete();
        }

        if(!request()->ajax()){
            return back();
        }
    }
}
