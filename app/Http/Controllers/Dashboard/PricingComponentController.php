<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Middleware\SuperAdminMiddleware;
use Illuminate\Http\Request;
use App\Models\PricingComponent;
use App\Http\Controllers\Controller;
use App\Models\QouteProductPrice;

class PricingComponentController extends Controller
{
    public function __construct()
    {
        $this->middleware([SuperAdminMiddleware::class]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pricings = PricingComponent::orderBy('created_at', 'DESC')->get();
        return view('dashboard.pricing-component.index', compact('pricings'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
        ]);

        $pricing = new PricingComponent;
        $pricing->name = $request->name;
        $pricing->active = $request->active ? 1 : 0;
        $pricing->multipliable = $request->multipliable ? 1 : 0;
        $pricing->save();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pricing = PricingComponent::find($id);
        return view('dashboard.pricing-component.edit', compact('pricing'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required',
        ]);

        $pricing = PricingComponent::find($id);
        $pricing->name = $request->name;
        $pricing->active = $request->active ? 1 : 0;
        $pricing->multipliable = $request->multipliable ? 1 : 0;
        $pricing->save();

        return redirect()->route('dashboard.pricing-component.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pricing = PricingComponent::find($id);
        QouteProductPrice::where('pricing_component_id', $id)->delete();
        $pricing->delete();
        return back();
    }
}
