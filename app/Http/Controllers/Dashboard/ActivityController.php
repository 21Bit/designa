<?php

namespace App\Http\Controllers\Dashboard;

use DataTables;
use App\Models\ActivityLog;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Builder;
use App\Http\Controllers\Controller;

class ActivityController extends Controller
{
    function index(Builder $builder){

        if (request()->ajax()) {
            $logs = ActivityLog::query();
            return DataTables::of($logs)
                ->addColumn('user', function ($log) {
                    return $log->user->name;
                })
                ->rawColumns(['user'])
                ->make();
        }

        $html = $builder
            ->parameters([
                    'order' => [ [1, 'ASC'] ],
                    'pageLength' => 10,
                ])
            ->columns([
            [
                'data' => 'user',
                'name' => 'user',
                'title' => 'User',
                'orderable' => true,
                'searchable' => true
            ],
            [
                'data' => 'type',
                'name' => 'type',
                'title' => 'Type',
                'orderable' => true,
                'searchable' => true
            ],
            [
                'data' => 'details',
                'name' => 'details',
                'title' => 'Details',
                'orderable' => true,
                'searchable' => true
            ],
            [
                'data' => 'origin',
                'name' => 'origin',
                'title' => 'Origin',
                'orderable' => true,
                'searchable' => true
            ],
            [
                'data' => 'created_at',
                'name' => 'created_at',
                'title' => 'Date/Time',
                'orderable' => true,
                'searchable' => true
            ],

        ]);

        return view('dashboard.activity-log.index', compact('html'));
    }

    public function statistics(Request $request)
    {
        $month = $request->month ? $request->month : date('m');
        $year   = $request->year ? $request->year : date('Y');
        $type = $request->activity_type ? $request->activity_type : 'LOGIN';

        $currentYear = date('Y');
        $years = array();
        for($i = $currentYear; $currentYear >= 2019; $currentYear--){
            array_push($years, $currentYear);
        }

        $day_count = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $workdays = array();


        for ($i = 1; $i <= $day_count; $i++) {
            $date = date('Y-m-d', strtotime($year .'-' . $month . '-'. $i));
            $monthly_post_data_array = [
                'date'      => $i,
                'web'       => ActivityLog::whereDate( 'created_at',  $date )->whereType($type)->whereOrigin('web')->count(),
                'mobile'    => ActivityLog::whereDate( 'created_at',  $date )->whereType($type)->whereOrigin('mobile')->count()
            ];
            array_push($workdays, $monthly_post_data_array);
        }

        return [
            'stats' => $workdays,
            'years' => ['year' => $years],
            'month' => ['month' => $month]
        ];
    }



}
