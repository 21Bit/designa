<?php

namespace App\Http\Controllers\Dashboard;

use Carbon\Carbon;
use DB;
use Cache;
use Illuminate\Http\Request;
use App\Models\SubscriptionPlan;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\SubscriptionPlanPrice;
use App\Services\HtmlMailerService;
use App\Services\SubscriptionService;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        return DB::transaction(function() use ($request){
            try{
                $user = User::find($request->user()->id);
                if($request->user()->subscribed('Designa Plan')){

                    if(config('subscription.september_promo') == 1) {
                        $user
                            ->subscription("Designa Plan")
                            ->skipTrial()
                            ->swapAndInvoice($request->price);

                        // extend trial
                        $user->subscription("Designa Plan")
                                ->extendTrial(now()->addDays(config('subscription.september_promo_days_trial')));

                    }else{
                        $user
                            ->subscription("Designa Plan")
                            ->skipTrial()
                            ->swapAndInvoice($request->price);
                    }
                }else{
                    $request->user()
                        ->newSubscription("Designa Plan", $request->price)
                        ->create($request->paymentMethod, [
                            'email' => $request->user()->email,
                        ]);
                }

                if($request->user()->company->is_active = 0){
                    $subscriptionPlan = SubscriptionPlan::whereHas('prices', function($q) use ($request){
                        $q->where('stripe_id', $request->price);
                    });

                    if($subscriptionPlan->name != 'Beginner'){
                        Company::where('id', $request->user()->company_id)->update(['is_active' => 1]);
                    }
                }

                Cache::flush("subscription.access." . $request->user()->company_id);
                return redirect()->route('dashboard.profile.index',['tab' => 'subscription']);
            }catch(\Exception $exception){
                return $exception->getMessage();
                return back()->with('warning', $exception->getMessage());
            }
        });

    }

    public function sendAmendment(Request $request){
        $html = "<h3>New Amendments</h3>
                <table>
                    <tr>
                        <td>Supplier</td>
                        <td>: ". $request->user()->company->name . "</td>
                    </tr>
                    <tr>
                        <td>Supplier login link (make to open on other browser)</td>
                        <td>: ". $request->user()->company->login_link . "</td>
                    </tr>
                    <tr>
                        <td>Amendment Message</td>
                        <td>: ". $request->message . "</td>
                    </tr>
                </table>
            ";
        return (new HtmlMailerService())
            ->subject('New Amendments')
            ->html($html)
            ->send();
    }

    function downloadInvoice(Request $request, $invoiceId){
        return $request->user()->downloadInvoice($invoiceId, [
            'vendor' => 'Designa Studio',
            'product' => 'Designa Plan',
            'street' => 'Main Str. 1',
            'location' => '2000 Antwerp, Belgium',
            'phone' => '+32 499 00 00 00',
            'email' => 'info@example.com',
            'url' => 'https://example.com',
            'vendorVat' => 'BE123456789',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $plan = SubscriptionPlan::findOrFail($id);
        $user = $request->user();
        $paymentMethod = $request->paymentMethod;

        $user->newSubscription($plan->name, $plan->stripe_plan)->quantity(1)
            ->create($paymentMethod, [
                'email' => $user->email,
            ]);
        $user->subscription('Designa Plan')->swap('provider-plan-id');
        return back();
    }

    function cancelSubscription(Request $request, $id): \Illuminate\Http\RedirectResponse
    {
        if($request->type == "complete"){
            $request->user()->subscription("Designa Plan")->cancelNow();
        }else{
            $request->user()->subscription("Designa Plan")->cancel();
        }
        return back();
    }


    function resumeSubscription(Request $request, $id): \Illuminate\Http\RedirectResponse
    {
        $request->user()->subscription("Designa Plan")->resume();
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }





    // Steps
    public function plans(Request $request){
        $billing_period = $request->period ?? 'monthly';
        $only = $request->only ? decrypt($request->only) : '';
        $plans = SubscriptionPlan::where('type', 'basic')->with('prices')->get();
        return view('dashboard.subscription.select-plan', compact('plans', 'only'));
    }


    public function methods(Request $request){
        try{
            $intent = $request->user()->createSetupIntent();
            $price = SubscriptionPlanPrice::where('stripe_id', decrypt($request->plan))->firstOrFail();
            $paymentMethods = $request->user()->paymentMethods();
            return view('dashboard.subscription.select-payment-method', compact('price','paymentMethods', 'intent'));
        }catch(\Exception $exception){
            return back()->with('warning', $exception->getMessage());
        }
    }


}
