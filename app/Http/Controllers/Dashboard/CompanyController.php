<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Middleware\SuperAdminMiddleware;
use Mail;
use App\Models\Role;
use App\Models\User;
use App\Models\Venue;
use App\Models\Company;
use App\Models\Product;
use App\Models\Category;
use App\Models\Inspiration;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\HasImageIntervention;
use Illuminate\Support\Facades\Cache;
use App\Services\SubscriptionService;


class CompanyController extends Controller
{
    use HasImageIntervention;


    public function __construct()
    {
        $this->middleware([SuperAdminMiddleware::class]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Company::with(['images', 'roles', 'categories']);
        $roles = Role::all();
        if($request->q){
            $query->where('name', 'LIKE', '%' . $request->q  .'%');
        }

         if($request->role){
            $query->whereHas('roles', function($q)
             use ($request){
                $q->where('id', $request->role);
            });
        }

        $companies = $query->orderBy('created_at', 'DESC')->paginate(20);
        return view('dashboard.company.index', compact('companies', 'roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::whereType('cater')->get();
        $roles = Role::all();
        return view('dashboard.company.create', compact('categories', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'              => 'required',
            'will_travel'       => 'required',
            'services'          => 'required',
            'recognition'       => 'required',
            'town'              => 'required',
            'state'             => 'required',
            'country'           => 'required',
            'post_code'         => 'required',
            'telephone_number'  => 'required',
            'categories'        => 'required|array',
            'offers'        => 'required|array',
            'email'             => 'nullable|email|unique:users',
            'password'          => 'required',
        ]);

        $company = new Company;
        $company->name = $request->name;
        $company->will_travel = $request->will_travel;
        $company->services = $request->services;
        $company->recognition = $request->recognition;
        $company->description = $request->description;

        $company->town = $request->town;
        $company->state = $request->state;
        $company->country = $request->country;
        $company->post_code = $request->post_code;

        $company->email = $request->email;
        $company->telephone_number = $request->telephone_number;
        $company->facebook_link = $request->facebook_link;
        $company->twitter_link = $request->twitter_link;
        $company->instagram_link = $request->instagram_link;
        $company->youtube_link = $request->youtube_link;
        $company->is_active = $request->is_active ? 1 : 0;

        $company->save();

        User::create([
            'name'  => $request->name,
            'email' => $request->email,
            'company_id' => $company->id,
            'password' => bcrypt($request->password),
            'type' => 'supplier',
            'email_verified_at' => now()
        ]);

        $company->categories()->sync($request->categories);
        $company->roles()->sync($request->offers);
        $company->asyncRoleToUsers();
        if($request->hasFile('company_logo')){
            //upload image and add
            $logo = $company->addImage("logo", $request->file('company_logo'),  public_path('images/logos'));

            //resize images
            $this->imageResize(public_path('images/logos/' . $logo->path), 250, 250, true);
            $this->imageResize(public_path('images/logos/' . $logo->path), 32, 32, true, public_path('images/32x32/'. $logo->path));

            $company->images()->save($logo);
        }

        if($request->hasFile('company_cover_picture')){
            //Upload and add image
            $cover = $company->addImage("cover", $request->file('company_cover_picture'), public_path('images/covers'));
            $company->images()->save($cover);
        }

        return redirect()->route('dashboard.company.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::findOrFail($id);
        $categories = Category::whereType('cater')->get();
        $roles = Role::all();
        return view('dashboard.company.edit', compact('company', 'categories', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'              => 'required',
            'will_travel'       => 'required',
            'services'          => 'required',
            // 'recognition'       => 'required',
            'town'              => 'required',
            'state'             => 'required',
            'country'           => 'required',
            'post_code'         => 'required',
            'telephone_number'  => 'required',
            'email'             => 'nullable|email|unique:companies,email,' . $id
        ]);

        $company = Company::find($id);
        $company->name = $request->name;
        $company->will_travel = $request->will_travel;
        $company->services = $request->services;
        $company->recognition = $request->recognition;
        $company->description = $request->description;
        $company->is_lock = $request->is_lock ? 1 : 0;

        $company->town = $request->town;
        $company->state = $request->state;
        $company->country = $request->country;
        $company->post_code = $request->post_code;

        $company->email = $request->email;
        $company->telephone_number = $request->telephone_number;
        $company->facebook_link = $request->facebook_link;
        $company->twitter_link = $request->twitter_link;
        $company->instagram_link = $request->instagram_link;
        $company->youtube_link = $request->youtube_link;
        $company->is_active = $request->is_active ? 1 : 0;

        $company->save();

        $company->categories()->sync($request->categories);
        $company->roles()->sync($request->roles);
        $company->asyncRoleToUsers();

        if($request->hasFile('company_logo')){
             // clear company logo
            $company->clearImage('logo');

            //upload image and add
            $logo = $company->addImage("logo", $request->file('company_logo'),  public_path('images/logos'));

            //resize images
            $this->imageResize(public_path('images/logos/' . $logo->path), 250, 250, true);
            $this->imageResize(public_path('images/logos/' . $logo->path), 32, 32, true, public_path('images/32x32/'. $logo->path));

            $company->images()->save($logo);

        }

        if($request->hasFile('company_cover_picture')){
            //clear cover images of company
            $company->clearImage('cover');

            //Upload and add image
            $cover = $company->addImage("cover", $request->file('company_cover_picture'), public_path('images/covers'));

            $company->images()->save($cover);
        }


        return back()->with('success', 'Changes saved!');
    }

    public function addTrial(Request $request,  SubscriptionService $subscriptionService){
        Cache::forget('company-trials-' . $request->company_id);
        $subscriptionService->clearCompanyAccessCache($request->company_id);
        try {
            $company = Company::findOrFail($request->company_id);
            $subscriptionService->addTrialInCompany($company, $request->days, $request->sendEmail ? true : false);
            return back()->with('success',  $request->days . ' of trials is added to ' . $company->name);
        }catch(\Exception $exception){
            return back()->with('error', $exception->getMessage());
        }

    }

    function cancelTrial(Request $request, SubscriptionService $subscriptionService){
         try {
             $company = Company::findOrFail($request->company_id);
             $subscriptionService->cancelTrialInCompany($company);
             $subscriptionService->clearCompanyAccessCache($request->company_id);

             return back()->with('message', 'Trial is cancelled for ' . $company->name);
//            return response([
//                'message' => 'Trial is cancelled for ' . $company->name
//            ], 200);
        }catch(\Exception $exception){
             return back()->with('message', $exception->getMessage());
//           return response([
//                'message' => $exception->getMessage()
//            ], 200);
        }

    }

    public function liftUsers($id){
        $company = Company::find($id);
        if($company){
            $users = $company->users;
            foreach($users as $user){
                $user->email_verified_at = date('Y-m-d h:i:s');
                $user->save();
            }
            return back()->with('success', 'Company lifted');
        }else{
            return back()->with('error', 'No company/supplier found');
        }
    }


    function sendActiveLink(Request $request){
        $id = $request->id;
        if(is_array($id)){
            foreach($id as $id){
                $company = Company::find($id);
                $company->sendActiveLink();
            }
        }else{
            $company = Company::find($id);
            $company->sendActiveLink();
        }

        return back()->with('success', 'Mail sent!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, $id)
    {

        $ids = $request->item_checked;
        if($ids){
           if(count($ids)){
                foreach($ids as $id){
                    $company = Company::find($id);
                    User::whereCompanyId($id)->delete();
                    Inspiration::where('company_id', $id)->delete();
                    Venue::where('company_id', $id)->delete();
                    Product::where('company_id', $id)->delete();
                    $company->clearImage('logo');
                    $company->clearImage('cover');
                    $company->delete();
                }
            }
        }

        return redirect()->back()->with('message', Company::whereIn('id', $ids)->get()->pluck('name')->implode(', ') . ' deleted successfully');
    }
}
