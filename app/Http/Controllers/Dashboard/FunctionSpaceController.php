<?php

namespace App\Http\Controllers\Dashboard;

use DB;
use Auth;
use App\Models\Venue;
use App\Models\ModelFile;
use Illuminate\Http\Request;
use App\Models\FunctionSpace;
use App\Http\Controllers\Controller;
use App\Services\SubscriptionService;
use App\Traits\HasImageIntervention;
use Laravel\Cashier\Exceptions\SubscriptionUpdateFailure;

class FunctionSpaceController extends Controller
{
     use HasImageIntervention;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if($request->user()->eventSpaceLimitReached()){
            return back()->with('warning', 'Please upgrade to pro to add more event spaces.');
        }
        try{
            DB::transaction(function() use ($request){
                $fs = new FunctionSpace;
                $fs->venue_id = $request->venue_id;
                $fs->name = $request->name;
                $fs->max_capacity_seated = $request->max_capacity_seated;
                $fs->max_capacity_standing = $request->max_capacity_standing;
                $fs->description = $request->description;
                $fs->save();

                if($request->hasFile('thumbnail')){
                    $thumbnail = $fs->addImage("thumbnail", $request->file('thumbnail'), public_path('images/thumbnails'));
                    $this->imageResize(public_path('images/thumbnails/' . $thumbnail->path), 350, 300, true);
                    $fs->images()->save($thumbnail);
                }

                if($request->hasFile('floor_plan')){
                    $floorPlan = $fs->addImage("floorplan", $request->file('floor_plan'), public_path('images/floorplans'));
                    $this->imageResize(public_path('images/floorplans/' . $floorPlan->path), 500, 800 ,true);
                    $fs->images()->save($floorPlan);
                }

                if($request->hasFile('base_layer')){
                    $file = $request->file('base_layer');
                    //Move Uploaded File
                    $destinationPath = public_path('models/');
                    $filename = time() . '.' . $file->getClientOriginalExtension();
                    $path = $file->move($destinationPath, $filename);
                    // $path = $file->move($destinationPath, $file->getClientOriginalName());
                    $fs->saveModelFile([
                        'model_name' => $fs->name,
                        'path'      =>  'models/' . $filename,
                        'is_public'      => 1,
                    ]);
                }
            });

            (new SubscriptionService)->subscribeToEventSpaceCharge(Auth::user());
        }catch(\Exception $exception){
            \Log::error($exception);
            return back()->with('warning', 'Sorry we have some problem saving your function space. We will contact you soon.');
        }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $venue = Venue::whereSlug($request->slug)->firstOrFail();
        $functionspace = FunctionSpace::find($id);
        return view('dashboard.venue.function-space-edit', compact('venue', 'functionspace'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => ['required'],
            'max_capacity_seated' => ['required'],
            'max_capacity_standing' => ['required'],
        ]);

        $fs = FunctionSpace::find($id);
        $fs->name = $request->name;
        $fs->max_capacity_seated = $request->max_capacity_seated;
        $fs->max_capacity_standing = $request->max_capacity_standing;
        $fs->description = $request->description;
        $fs->save();

        if($request->hasFile('thumbnail')){
            $fs->clearImage('thumbnail');
            $thumbnail = $fs->addImage("thumbnail", $request->file('thumbnail'), public_path('images/thumbnails'));
            $this->imageResize(public_path('images/thumbnails/' . $thumbnail->path), 350, 280);
            $fs->images()->save($thumbnail);
        }

        if($request->hasFile('floor_plan')){
            $fs->clearImage('floorplan');
            $floorPlan = $fs->addImage("floorplan", $request->file('floor_plan'), public_path('images/floorplans'));
            $this->imageResize(public_path('images/floorplans/' . $floorPlan->path), 500, 800 ,true);
            $fs->images()->save($floorPlan);
        }

        return redirect()->route('dashboard.venue.show', $fs->venue->slug);
    }


    public function assetBundles($id){
        $functionspace = FunctionSpace::find($id);
        return view('dashboard.venue.function-space-3d', compact('venue', 'functionspace'));
    }

    public function uploadAssetBundle(Request $request, $id){

        $this->validate($request,[
            'type'                  => 'required',
            'asset_bundle_file'     => 'required',
        ]);

        $product = FunctionSpace::find($id);

        if($request->hasFile('asset_bundle_file')){

            // deleting old Android file
            if($request->type == 'web'){
                $old = $product->asset_bundle_web;
            }else if($request->type == "android"){
                $old = $product->asset_bundle_android;
            }else{
                $old = $product->asset_bundle_ios;
            }


            if($old){
                $old->delete();
            }

            $destinationPath = public_path('3d-models/');

            $file = $request->file('asset_bundle_file');
            $filename = time() . '_' . $request->type . $request->file('asset_bundle_file')->getClientOriginalExtension();

            $webPath        = $file->move($destinationPath, $filename);


            // saving web
            $product->saveModelFile([
                'model_name'        => $request->file('asset_bundle_file')->getClientOriginalName(),
                'path'              => 'models/' . $filename,
                'type'              => 'asset_bundle_' . $request->type,
                'is_public'         =>  0,
            ]);

        }

        return back()->with('message' , 'Uploading complete');
    }

    public function deleteModel($id){

        $modelFile = ModelFile::find($id);

        try{
            $model = public_path($modelFile->path);

            if(file_exists($model)){
                unlink($model);
            }

            $modelFile->delete();

            if(request()->ajax()){
                return response([
                    'status' => 'success',
                    'message' => 'Item Deleted'
                ], 200);
            }else{
                return back();
            }
        }catch(\Exception $exception){
            return back()->with('warning', 'Sorry we have some problem saving your function space. We will contact you soon.');
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $funtionspace = FunctionSpace::find($id);
        $funtionspace->clearImage('thumbnail');
        $funtionspace->clearImage('floorplan');
        $funtionspace->delete();

        (new SubscriptionService)->subscribeToEventSpaceCharge(Auth::user());


        if(!request()->ajax()){
            return back();
        }
    }
}
