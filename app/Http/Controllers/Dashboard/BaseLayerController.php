<?php

namespace App\Http\Controllers\Dashboard;

use Auth;
use App\Models\Venue;
use App\Models\ModelFile;
use Illuminate\Http\Request;
use App\Models\FunctionSpace;
use App\Http\Controllers\Controller;

class BaseLayerController extends Controller
{


    public function __construct()
    {
        $this->middleware('access:create-your-own-event-3d-using-template');
    }


    /**
     * Display a listing of the resource. gleen
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $modelQuery  = ModelFile::whereType('base_layer')->with('modellable')->has('modellable');
        $venueQuery = Venue::with('functionspaces')->has('functionspaces');

        if(Auth::user()->is_superadmin){
            $venues = $venueQuery->get();
        }else{
            $venues = $venueQuery->whereCompanyId(Auth::user()->company_id)->get();
        }

        // put conditions here
        if($request->q){
            $modelQuery->where('model_name', 'LIKE', "%$request->q%");
        }


        if($request->tab == "my-baselayers"){
            $models = $modelQuery->latest()->get()->filter(function($q){
               return $q->modellable->venue->company_id == Auth::user()->company_id;
            })->paginate(12);
        }else{
            if(!Auth::user()->is_superadmin){
                $models = $modelQuery->where('is_public', 1);
            }else{
                $models = $modelQuery;
            }
            $models = $models->latest()->paginate(12);
        }


        return view('dashboard.3d.baselayer', compact('models', 'venues'));
    }


    public function create()
    {
        $event_space_id = $id;
        $token =  Auth::user()->createToken('Designa Password Grant Client')->accessToken;
        $pagetitle = "Create Base Layer";
        $backURL = route('dashboard.baselayer.index');
        $params = "$id%0%0%venue_owner%base_layer%$token%$backURL";
        return view('dashboard.3d.render', compact('params', 'pagetitle'));
    }

    public function generateTemplate($id)
    {
        $token =  Auth::user()->createToken('Designa Password Grant Client')->accessToken;
        $backURL = route('dashboard.baselayer.index');
        $pagetitle = "Generate Template";
        $params = "$id%0%0%venue_owner%decoration_layer%$token%$backURL";
        return view('dashboard.3d.render', compact('params', 'pagetitle'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $token =  Auth::user()->createToken('Designa Password Grant Client')->accessToken;
        $module = 'base_layer';
        $eventSpace = FunctionSpace::find(1);
        $pagetitle = "Base layer: $eventSpace->name" ;
        $backURL = route('dashboard.baselayer.index');
        $params = "$id%0%0%venue_owner%$module%$token%$backURL";
        return view('dashboard.3d.render', compact('params','pagetitle'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $functionSpace = FunctionSpace::find($id);
        $pagetitle = $functionSpace->name;
        $token =  Auth::user()->createToken('Designa Password Grant Client')->accessToken;
        $backURL = route('dashboard.baselayer.index');
        $params = "$id%0%0%venue_owner%base_layer%$token%$backURL";
        return view('dashboard.3d.render', compact('params', 'pagetitle'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $beenUsed = ModelFile::where('base_layer_id', $id)
                ->withTrashed()->count();

        if($beenUsed){
            ModelFile::find($id)->delete();
        }else{
            ModelFile::find($id)->forceDelete();
        }

        if(!request()->ajax()){
            return back();
        }
    }
}
