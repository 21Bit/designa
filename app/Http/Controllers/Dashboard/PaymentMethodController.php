<?php

namespace App\Http\Controllers\Dashboard;

use Carbon\Carbon;
use DB;
use Auth;
use Cache;
use Illuminate\Http\Request;
use Laravel\Cashier\PaymentMethod;
use App\Http\Controllers\Controller;
use App\Models\SubscriptionPlanPrice;
use App\Services\SubscriptionService;

class PaymentMethodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        try {
            return DB::transaction(function() use ($request){
                $request->user()->createOrGetStripeCustomer();
                $request->user()->addPaymentMethod($request->payment_method);
                if(count($request->user()->paymentMethods()) < 2){
                    $request->user()->updateDefaultPaymentMethod($request->payment_method);

                    // Get beginner Plans
                    if(config('subscription.september_promo')){
                        $price = SubscriptionPlanPrice::whereHas('subscriptionPlan', function($q){
                            $q->where('name', 'Beginner');
                        })->where('billing_period', 'yearly')->first();

                        if($price){
                            $request->user()->newSubscription("Designa Plan", $price->stripe_id)
                                ->quantity(1)
                                ->trialUntil(Carbon::now()->addDays(config('subscription.september_promo_days_trial')))
                                ->create($request->payment_method);
                        }
                    }else{
                        $price = SubscriptionPlanPrice::whereHas('subscriptionPlan', function($q){
                            $q->where('name', 'Beginner');
                        })->where('billing_period', 'yearly')->first();

                        if($price){
                            (new SubscriptionService)->subscribeToPlan($request->user(), $price, $request->payment_method);
                        }
                    }
                }

                Cache::forget($request->user()->id . '-payment_methods');
                Cache::flush("subscription.access." . $request->user()->company_id);
                if(!$request->ajax()){
                    return back();
                }
            });
        }catch (\Exception $exception){
            return $exception->getMessage();
            return back()->with('error', $exception->getMessage());
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    public function setDefault(Request $request, $id){
        $request->user()->updateDefaultPaymentMethod($id);
        $key = $request->user()->id . '-default-payment_methods';
        Cache::forget($key);
        return back()->with('message', 'Default Payment Method updated!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($key)
    {
        try {
            $paymentMethod = Auth::user()->findPaymentMethod($key);
            $paymentMethod->delete();
        }catch (\Exception $exception){
            return $exception->getMessage();
        }
        Cache::forget(Auth::user()->id . '-payment_methods');
        return back();
    }
}
