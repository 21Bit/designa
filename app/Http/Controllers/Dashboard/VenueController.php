<?php

namespace App\Http\Controllers\Dashboard;

use Auth;
use App\Models\Color;
use App\Models\Image;
use App\Models\Venue;
use App\Models\Company;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\SubscriptionService;
use App\Traits\HasImageIntervention;

class VenueController extends Controller
{
    use HasImageIntervention;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(in_array('venue', Auth::user()->company->permissions()) || Auth::user()->is_superadmin){

            if(!Auth::user()->is_superadmin){
                $list = Venue::where('company_id', Auth::user()->company_id);
            }else{
                $list = Venue::query();
            }

            if(Auth::user()->company->single_venue){
                $venue = Venue::where('company_id', Auth::user()->company_id);
                if($venue->count()){
                    return redirect()->route('dashboard.venue.show', $venue->first()->slug);
                }else{
                    return redirect()->route('dashboard.venue.create');
                }
            }else{
                $categories = Category::whereType('cater')->get();

                if($request->category){
                    $list->whereHas('categories', function($q)
                        use ($request){
                        $q->where('id', $request->category);
                    });
                }
                
                if ($request->search){
                    $list->where('name', 'LIKE', '%' . $request->search . '%');
                }
                
                if($request->feature){
                    $list->where('featured', $request->feature);                   
                }

                if($request->status){
                    $list->where('featured', $request->feature)
                        ->where('is_published', $request->status == 2 ? 0 : $request->status);
                }

                if($request->lock){
                    $list->where('featured', $request->feature)
                        ->where('is_published', $request->status == 2 ? 0 : $request->status)
                        ->where('is_lock', $request->lock);
                }
                

                $venues = $list->with('company')
                            ->with('caters')
                            ->with('colors')
                            ->orderBy('created_at', 'DESC')
                            ->paginate(8);
                
                if(Auth::user()->is_superadmin){
                    $suppliers = Company::whereIsActive(1)->get();
                    return view('dashboard.venue.index', compact('venues', 'categories', 'suppliers'));
                }

                return view('dashboard.venue.index', compact('venues','categories'));
            }
        }else{
            return abort(404);
        }
        
        // filter by company
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $colors = Category::whereType('color')->get();
        $categories = Category::whereType('cater')->orderBy('name', 'ASC')->get();
        $types = Category::whereType('venue_type')->orderBy('name', 'ASC')->get();
        $amenities = Category::whereType('amenity')->orderBy('name', 'ASC')->get();
        $catering_options = Category::whereType('catering_option')->orderBy('name', 'ASC')->get();
        $beverage_options = Category::whereType('beverage_option')->orderBy('name', 'ASC')->get();
        $average_cost_per_heads = Category::whereType('average_cost_per_head')->orderBy('name', 'ASC')->get();
        return view('dashboard.venue.create', compact('colors', 'categories', 'types', 'amenities','catering_options','beverage_options','average_cost_per_heads'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $operationHourse = '';
        $counter = 0;
        foreach($request->timein as $timein){
            if($timein != null){
                $operationHourse .= $timein . " - " . $request->timeout[$counter];
                if(count($request->timein) >= $counter + 1){
                    $operationHourse .= ",";
                }
            }
            else{
                $operationHourse .= "null";
                if(count($request->timein) > $counter + 1){
                    $operationHourse .= ",";
                }
            }
            $counter++;
        }

        $this->validate($request, [
            'name' => 'required',
            'location' => 'required',
            'capacity' => 'required|integer',
            'cropped_image' => 'required'
        ]);

        
        $venue = new Venue;
        $venue->name = $request->name;
        $venue->description = $request->description;
        $venue->location = $request->location;
        $venue->latitude = $request->latitude;
        $venue->longitude = $request->longitude;
        $venue->email = $request->email;
        $venue->contact_number = $request->contact_number;
        $venue->operating_hours = $operationHourse;
        $venue->max_seated = $request->max_seated;
        $venue->max_standing = $request->max_standing;
        $venue->capacity = $request->capacity;
        $venue->price = $request->price;
        $venue->company_id = Auth::user()->company_id;
        $venue->is_published = $request->is_published ? 1 : 0;


        $venue->save();

        $venue->categories()->whereType('cater')->attach($request->categories);
        $venue->categories()->whereType('venue_type')->attach($request->types);
        $venue->categories()->whereType('amenity')->attach($request->amenities);
        $venue->categories()->whereType('catering_option')->attach($request->catering_options);
        $venue->categories()->whereType('beverage_option')->attach($request->beverage_options);
        $venue->categories()->whereType('average_cost_per_head')->attach($request->average_cost_per_heads);

        if($request->cropped_image){
            $image = $request->cropped_image;

            list($type, $image) = explode(';', $image);
            list(, $image)      = explode(',', $image);
            $image = base64_decode($image);

            $thumbnail = time().'.png';
            $thumbnail_path = public_path('images/thumbnails/'. $thumbnail);
            file_put_contents($thumbnail_path, $image);
            
            // $this->imageResize(public_path('images/thumbnails/' . $thumbnail), 250, 200);
            
            $thumbnailimage = new Image;
            $thumbnailimage->path = $thumbnail;
            $thumbnailimage->type = "thumbnail";
            
            $venue->images()->save($thumbnailimage);
        }

        if($request->hasFile('base_layer')){
            $file = $request->file('base_layer');
            //Move Uploaded File
            $destinationPath = public_path('models/');
            $path = $file->move($destinationPath, $file->getClientOriginalName());

            $venue->saveModelFile([
                'model_name'     => $venue->name,
                'path'           => $path,
                'is_public'      => 1,
            ]);

        }


        if($request->cropped_image_cover){

            $image = $request->cropped_image_cover;

            list($type, $image) = explode(';', $image);
            list(, $image)      = explode(',', $image);
            $image = base64_decode($image);
            
            $cover = time().'.png';
            $cover_path = public_path('images/covers/'. $cover);
            file_put_contents($cover_path, $image);
            
            $coverimage = new Image;
            $coverimage->path = $cover;
            $coverimage->type = "cover";
            
            $venue->images()->save($coverimage);

        }

        (new SubscriptionService)->subscribeToVenueCharge(Auth::user());
        
        if($request->proceed){
            return redirect()->route('dashboard.venue.show', $venue->slug);
        }else{
            return redirect()->route('dashboard.venue.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $venue = Venue::where('slug',$id)->firstOrFail();
        $explodedTimes = explode(',', $venue->operating_hours);
        
        if($request->tab == "gallery"){
            $gallery = $venue->images()->where('type', 'gallery')->get();
            return view('dashboard.venue.gallery-tab', compact('venue', 'gallery', 'explodedTimes'));
        }else if($request->tab == "reviews"){
            return view('dashboard.venue.reviews', compact('venue', 'gallery', 'explodedTimes'));
        }else if($request->tab == "heart"){
            $hearts = $venue->hearts;
            return view('dashboard.venue.hearts', compact('venue', 'explodedTimes', 'hearts'));
        }else{
            $functionspaces = $venue->functionSpaces()->paginate(9);
            return view('dashboard.venue.function-space', compact('venue', 'functionspaces', 'explodedTimes'));
        }

    }

    public function uploadgallery(Request $request, $id){
        $venue = Venue::find($id);
        if($request->hasFile('file')){
            $gallery = $venue->addImage("gallery", $request->file('file'), public_path('images/gallery'));

            list($width, $height) = getimagesize(public_path('images/gallery/' . $gallery->path));
            if($width > 900){
                $this->imageResize(public_path('images/gallery/' . $gallery->path), 900, null, true);
            }else if($height > 800){
                $this->imageResize(public_path('images/gallery/' . $gallery->path), null, 800, true);
            }
          
            $venue->images()->save($gallery);
        }

        if(!$request->ajax()){
            return back();
        }
    }

    


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $venue = Venue::findOrFail($id);
        $colors = Category::whereType('color')->get();
        $categories = Category::whereType('cater')->orderBy('name', 'ASC')->get();
        $types = Category::whereType('venue_type')->orderBy('name', 'ASC')->get();
        $amenities = Category::whereType('amenity')->orderBy('name', 'ASC')->get();
        $catering_options = Category::whereType('catering_option')->orderBy('name', 'ASC')->get();
        $beverage_options = Category::whereType('beverage_option')->orderBy('name', 'ASC')->get();
        $average_cost_per_heads = Category::whereType('average_cost_per_head')->orderBy('name', 'ASC')->get();
        $suppliers = Company::all();
        $operationHours = operationHoursForEdit($venue->operating_hours);
        return view('dashboard.venue.edit', compact('operationHours','colors', 'categories', 'venue', 'types', 'amenities', 'catering_options','beverage_options','average_cost_per_heads', 'suppliers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'location' => 'required',
            'capacity' => 'required|integer',
            'thumbnail' => 'nullable|mimes:jpeg,jpg,JPG,png,PNG'
        ]);

        $operationHourse = '';
        $counter = 0;
        foreach($request->timein as $timein){
            if($timein != null){
                $operationHourse .= $timein . " - " . $request->timeout[$counter];
                if(count($request->timein) >= $counter + 1){
                    $operationHourse .= ",";
                }
            }
            else{
                $operationHourse .= "null";
                if(count($request->timein) > $counter + 1){
                    $operationHourse .= ",";
                }
            }
            $counter++;
        }
        
        $venue = Venue::find($id);
        $venue->name = $request->name;
        $venue->description = $request->description;
        $venue->location = $request->location;
        $venue->latitude = $request->latitude;
        $venue->longitude = $request->longitude;
        $venue->email = $request->email;
        $venue->contact_number = $request->contact_number;
        $venue->price = $request->price;
        $venue->max_seated = $request->max_seated;
        $venue->max_standing = $request->max_standing;
        $venue->capacity = $request->capacity;
        $venue->operating_hours = $operationHourse;
        $venue->is_published = $request->is_published ? 1 : 0;
        $venue->is_lock = $request->is_lock ? 1 : 0;
        $venue->featured = $request->featured ? 1 : 0;

        if($request->company_id){
            $venue->company_id = $request->company_id;
        }

        $venue->save();

        // Clear all category first
        $venue->clearAttachCategory(['cater', 'venue_type', 'catering_option', 'beverage_option','amenity','average_cost_per_head']);
        
        $venue->categories()->where('type','cater')->attach($request->categories);
        $venue->categories()->where('type','venue_type')->attach($request->types);
        $venue->categories()->where('type','catering_option')->attach($request->catering_options);
        $venue->categories()->where('type','beverage_option')->attach($request->beverage_options);
        $venue->categories()->where('type','amenity')->attach($request->amenities);
        $venue->categories()->where('type','average_cost_per_head')->attach($request->average_cost_per_heads);


        if($request->hasFile('thumbnail')){
            //remove existing thumbnail
            $venue->clearImage('thumbnail');

            $image = $request->cropped_image;

            list($type, $image) = explode(';', $image);
            list(, $image)      = explode(',', $image);
            $image = base64_decode($image);

            $thumbnail = time().'.png';
            $thumbnail_path = public_path('images/thumbnails/'. $thumbnail);
            file_put_contents($thumbnail_path, $image);
            
            // $this->imageResize(public_path('images/thumbnails/' . $thumbnail), 250, 200);
            
            $thumbnailimage = new Image;
            $thumbnailimage->path = $thumbnail;
            $thumbnailimage->type = "thumbnail";
            
            $venue->images()->save($thumbnailimage);
        }

         if($request->cropped_image_cover){
             $venue->clearImage('cover');
            $image = $request->cropped_image_cover;

            list($type, $image) = explode(';', $image);
            list(, $image)      = explode(',', $image);
            $image = base64_decode($image);
            
            $cover = time().'.png';
            $cover_path = public_path('images/covers/'. $cover);
            file_put_contents($cover_path, $image);
            
            $coverimage = new Image;
            $coverimage->path = $cover;
            $coverimage->type = "cover";
            
            $venue->images()->save($coverimage);

        }

        if($request->hasFile('base_layer')){

            // delete old base layer
            $old = $venue->base_layer;
            if($old){
                $old->delete();
            }

            $file = $request->file('base_layer');
            //Move Uploaded File
            $destinationPath = public_path('models/');

            $filename = time() . '.' . $file->getClientOriginalExtension();

            $path = $file->move($destinationPath, $filename);
            // $path = $file->move($destinationPath, $file->getClientOriginalName());

            $venue->saveModelFile([
                'model_name' => $venue->name,
                'path'      =>  'models/' . $filename,
                'is_public'      => 1,
            ]);

        }

      

        return back();
        if($request->proceed){
            return redirect()->route('dashboard.venue.show', $venue->slug);
        }else{
            return redirect()->route('dashboard.venue.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $venue = Venue::find($id);
        $venue->clearImage('cover');
        $venue->clearImage('thumbnail');
        $venue->hearts()->delete();
        $venue->views()->delete();
        $venue->reviews()->delete();
        
        // Clear all category first
        $venue->clearAttachCategory(['cater', 'venue_type', 'catering_option', 'beverage_option','amenity','average_cost_per_head']);
        
        $functionspaces = $venue->functionspaces;

        foreach($functionspaces as $functionspace){
            $functionspace->delete();
        }

        $venue->delete();
        (new SubscriptionService)->subscribeToVenueCharge(Auth::user());

        if(!request()->ajax()){
            return back();
        }
    }
}
