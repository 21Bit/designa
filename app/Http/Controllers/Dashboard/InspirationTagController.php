<?php

namespace App\Http\Controllers\Dashboard;

use Auth;
use App\Models\Product;
use App\Models\Inspiration;
use Illuminate\Http\Request;
use App\Models\SupplierInspiration;
use App\Http\Controllers\Controller;

class InspirationTagController extends Controller
{
    function index(Request $request){
        $company = Auth::user()->company;
        $roles = $company->roles;
        $query = SupplierInspiration::whereCompanyId($company->id);

        if($request->q){
            $query->whereHas('company', function($q) use ($request){
                    $q->where('name', 'LIKE', '%' . $request->q . '%');
                })
                ->orWhereHas('inspiration', function($q) use ($request){
                    $q->where('name', 'LIKE', '%' . $request->q . '%');
                });
        }

        $inspirationtags = $query->with(['inspiration', 'company','role'])
                            ->paginate(15);
        return view('dashboard.inspiration-tag.index', compact('inspirationtags', 'roles'));
    }

    function show($id){
        $inspirationtag = SupplierInspiration::findOrFail($id);
        $inspiration = $inspirationtag->inspiration;
        $gallery = $inspiration->images()->where('type', 'gallery')->get();
        $products = Product::whereCompanyId(Auth::user()->company_id)->get();
        return view('dashboard.inspiration-tag.show', compact('inspirationtag', 'inspiration', 'gallery', 'products'));
    }

    function changeStatus(Request $request, $id){
        $inspirationtag = SupplierInspiration::findOrFail($id);
        $inspirationtag->status = $request->status;
        $inspirationtag->save();
        return back();
    }

  

    function addProduct(Request $request, $id){
        $inspiration = Inspiration::find($id);
        if($request->product){
            $inspiration->products()->attach($request->product);
        }

        return back();
    }

    function removeProduct(Request $request, $id, $product){
        $inspiration = Inspiration::find($id);
        if($request->product){
            $inspiration->products()->detach($product);
        }

        return back();
    }
}
