<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Color;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $colors = Color::paginate(15);
        $colors = Category::whereType('color')->get();
        return view('dashboard.color.index', compact('colors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'  => 'required|unique:colors',
            'rgb_code'  => 'required'
        ]);

        $color = Color::create($request->except('_token'));

        if($request->ajax()){
            return $color;
        }else{
            return redirect()->route('dashboard.color.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $color = Color::findOrFail($id);
        return view('dashboard.color.edit', compact('color'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'  => 'required|unique:colors,name,' . $id,
            'rgb_code'  => 'required'
        ]);

        $color = Color::findOrFail($id)->update($request->except('_token'));
        
        if($request->ajax()){
            return $color;
        }else{
            return redirect()->route('dashboard.color.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        Color::find($id)->delete();
        if(!$request->ajax()){
            return back();   
        }
    }

}
