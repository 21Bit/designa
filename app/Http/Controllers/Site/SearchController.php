<?php

namespace App\Http\Controllers\Site;

use Cache;
use App\Models\Venue;
use App\Models\Company;
use App\Models\Product;
use App\Models\Inspiration;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Collection;

class SearchController extends Controller
{
    function search(Request $request){
        
        // Cache::remember('general-search-' . json_encode($request->all()), 60, function() use ($request){
            $data = array();
            $inspirations = Inspiration::where('name', 'LIKE', '%' . $request->q . '%')->whereIsPublished(1)->limit(5)->get()->map(function($q){
                                return [
                                        'id' => $q->id,
                                        'name' => $q->name,
                                        'url'   => route('site.inspiration.show', $q->slug),
                                        'type' => 'inspiration',
                                    ];
                            });
            

            $decors = Product::where('name', 'LIKE', '%' . $request->q . '%')->whereIsPublished(1)->limit(5)->get()->map(function($q){
                                    return  [
                                        'id' => $q->id,
                                        'name' => $q->name,
                                        'url'   => route('site.decor.index',['show' => $q->slug ] ),
                                        'type' => 'decor',
                                    ];
                            });

            // $inspirations->merge($decors);

            $venues = Venue::where('name', 'LIKE', '%' . $request->q . '%')->whereIsPublished(1)->limit(5)->get()->map(function($q){
                                    return [
                                        'id' => $q->id,
                                        'name' => $q->name,
                                        'url'   => route('site.venue.show', $q->slug ),
                                        'type' => 'venue',
                                    ];
                            });

            // $inspirations->merge($venues);

            $suppliers = Company::where('name', 'LIKE', '%' . $request->q . '%')->where('is_active', 1)->limit(5)->get()->map(function($q){
                                    return  [
                                        'id' => $q->id,
                                        'name' => $q->name,
                                        'url'   => route('site.supplier.show', $q->slug),
                                        'type' => 'supplier',
                                    ];
                            });


            $collection = collect($inspirations)->merge($venues)->merge($decors)->merge($suppliers);
            return response()->json($collection, 200);
        // });
        
    }
}
