<?php

namespace App\Http\Controllers\Site;

use App\Models\Venue;
use App\Models\Review;
use App\Models\Company;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Review\Review as ReviewResource;
use App\Models\QouteProduct;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request){

    }

    public function list(Request $request)
    {
        $type = $request->model;
        $id = $request->id;
        
        if($type == "product"){
            $model = Product::find($id);
        }else if($type == "venue"){
            $model = Venue::find($id);
        }else if($type == "supplier"){
            $model = Company::find($id);
        }else if($type == "quote-product"){
            $model = QouteProduct::find($id);
        }

        return ReviewResource::collection($model->reviews()->orderBy('created_at', 'DESC')->paginate(10));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $type = $request->model;
        $id = $request->id;
        $rating = $request->rating;
        $comment = $request->comment;
        
        if($type == "product"){
            $model = Product::find($id);
        }else if($type == "venue"){
            $model = Venue::find($id);
        }else if($type == "supplier"){
            $model = Company::find($id);
        }else if($type == "quote-product"){
            $model = QouteProduct::find($id);
        }

        $review = new Review;
        $review->user_id = $request->user()->id;
        $review->comment = $comment;
        $review->rating = $rating;
        
        $model->reviews()->save($review);

        return new ReviewResource($review);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
