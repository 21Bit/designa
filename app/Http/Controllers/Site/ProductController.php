<?php

namespace App\Http\Controllers\Site;

use DB;
use Auth;
use Cache;
use Carbon\Carbon;
use App\Models\Heart;
use App\Models\Company;
use App\Models\Product;
use App\Models\Category;
use App\Facades\FirebaseFMC;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Product\Web\Product as ProductResource;

class ProductController extends Controller
{
    function index(Request $request){
        $category = Category::whereSlug($request->cat)->first();

        if($category){
            if($category->parent_id){
                // for child category
                $defaultCategoryParent = $category->parent_id;
                $defaultCategory = $category->id;

            }else{
                // for parent category
                $defaultCategory = $category->id;
                $defaultCategoryParent = $category->id;
            }
        }else{
            $defaultCategory = '';
            $defaultCategoryParent = '';
        }


        return view('site.product.index', compact('defaultCategory', 'defaultCategoryParent'));
    }

    function list(Request $request){
        $keys = json_encode($request->all());
        // return Cache::remember('decor-' . json_encode($request->all()), Carbon::now()->addMinutes(2), function() use($request){
           $list = Product::query()
                    ->join('categorizables', function($join){
                        $join->on('products.id', '=', 'categorizables.categorizable_id');
                    })
                    ->join('categories', 'categories.id', '=', 'categorizables.category_id')
                    ->join('companies', function($join){
                        $join->on('products.company_id', '=', 'companies.id')
                            ->where('companies.is_active', 1);
                    });

            // if($request->caters){
            //     $list->where(function($query) use ($request) {
            //         $query->where('categories.type', 'cater')
            //                 ->whereIn('categories.id', $request->caters);
            //     });
            // }

            if($request->types && count($request->styles) < 1){
                $list->where(function($query) use ($request) {
                    $query->where('categories.type', 'product_type')
                        ->whereIn('categories.id', $request->types);
                });
            }

            if($request->styles && $request->types){
                $list->where(function($query) use ($request) {
                    $query->whereIn('categories.type', ['style', 'product_type'])
                        ->whereIn('categories.id', $request->styles);
                });
            }

            if($request->colors){
                $list->whereHas('categories',function($q) use ($request){
                    $q->whereType('color')->whereIn('id', $request->colors);
                });
            }


            if($request->supplier){
                $list->where('companies.id', $request->supplier);
            }

            if($request->search){
                $list->where('products.name', 'LIKE', '%' . $request->search . '%');
            }

            if($request->company){
                $list->where('companies.name',  'LIKE', '%' . $request->company . '%');
            }

            if($request->latitude && $request->longitude){
                $list->addSelect(['address_distance' => function($query) use ($request){
                            $query->selectRaw("MIN(( 6367 * acos( cos( radians($request->latitude) )
                                                * cos( radians( `locations`.`latitude` ) )
                                                * cos( radians( `locations`.`longitude` ) - radians($request->longitude) )
                                                + sin( radians($request->latitude) )
                                                * sin( radians( `locations`.`latitude` ) ) )))
                                                from `locations`
                                                WHERE `products`.`company_id` = `locations`.`locationable_id`
                                                and `locations`.`locationable_type` = 'App\\\Models\\\Company'");
                        }
                    ])
                    ->orderBy('address_distance');

                if($request->distance != null){
                    $list->having('address_distance', '<=', $request->distance);
                }

            }

            $products = $list
                        ->groupBy('products.id')
                        ->latest()
                        ->withCount(['hearts', 'reviews', 'shares'])
                        ->where('is_published', 1)->paginate(12);


            return ProductResource::collection($products);
        // });
    }

    function relatedItem($id){
        $product = Product::find($id);
        $relatedItems = Product::where("company_id", $product->company_id)
                    ->whereHas('categories', function($q) use ($product){
                        $q->whereIn('id', $product->categories()->pluck('id')->toArray());
                    })->where('is_published', 1)->inRandomOrder()->limit(12)->get();

        return ProductResource::collection($relatedItems);
    }

    function show($id){
        $product = Cache::remember('product.' . $id, 5, function() use($id){
            return Product::find($id);
        });
        return new ProductResource($product);

        if(request()->ajax()){

        }
    }

     function viewed(Request $request){
        $product = Product::find($request->id);
        return $product->viewed($request);
     }


     function hearted(Request $request){

        $product = Product::find($request->id);
        $supplier  = Company::find($product->company_id);

        if(!$request->user()){
            return response()->json([
                'message' => 'User not authentecated',
                'status'    => '401'
            ], 200);
        }

        if(Auth::user()){
            $hearted = $product->hearts()->whereUserId($request->user()->id)->first();
            if($hearted){
                $heartedStatus = 0;
                $product->hearts()->whereUserId($request->user()->id)->first()->delete();
            }else{
                $heart = new Heart;
                $heart->user_id = $request->user()->id;
                $product->hearts()->save($heart);
                $heartedStatus = 1;

                // send notification
                $firebaseArray = array();
                foreach($supplier->users()->has('firebases')->get() as $user){
                    foreach($user->firebases as $firebaseAccount){
                        array_push($firebaseArray, $firebaseAccount->firebase_id);
                    }
                }

                $firebase = new FirebaseFMC;
                $firebase->sendSingle(
                    $firebaseArray,
                    array(
                        'title'         => 'Hearted Product',  // title,
                        'avatar'        => '/dashboard/img/avatar04.png',
                        'message'       => $request->user()->name . ' has saved your product entitled ' . $product->name,  // message,
                        'notif_type'    => 'hearted_product',
                        'item_id'       => $product->id,
                        'link'          => '/',//route('site.decor.show', $product->slug),
                        'image'         => $product->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),  // image
                        'timestamp'     => Carbon::now()->toDateTimeString() // timestamp
                    )
                );
            }

            return [
                'status' => 200,
                'hearted' => $heartedStatus
            ];
        }else{
            return [
                'status' => 401,
                'hearted' => 0
            ];
        }
    }
}
