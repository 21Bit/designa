<?php

namespace App\Http\Controllers\Site;

use App\Models\Company;
use App\Models\Product;
use Mail;
use Cache;
use Storage;
use Notification;
use Carbon\Carbon;
use App\Models\Inspiration;
use Jenssegers\Agent\Agent;
use Illuminate\Http\Request;
use App\Mail\Site\InquiryMail;
use App\Models\SupplierInspiration;
use App\Http\Controllers\Controller;
use App\Mail\Site\EventRegistration;
use App\Notifications\Customer\ContestRegigrationNotification;
use App\Services\HtmlMailerService;
use Illuminate\Notifications\Messages\MailMessage;

class HomeController extends Controller
{
    function index(Request $request){
        $data = Cache::remember('homepage', Carbon::now()->addDays(7), function(){
            $data['wedding'] = $this->getInspirationPhotos('Wedding');
            $data['corporate'] = $this->getInspirationPhotos('Corporate');
            $data['cultural'] = $this->getInspirationPhotos('Cultural');
            $data['special_event'] = $this->getInspirationPhotos('Special Events');
            $data['outdoor'] = $this->getInspirationPhotos('Outdoor');
            $data['bar_restaurant'] = $this->getInspirationPhotos('Bar / Restaurant');
            $data['boats_cruises'] = $this->getInspirationPhotos('Boats & Cruises');
            $data['destination'] = $this->getInspirationPhotos('Destination');
            $data['event_space'] = $this->getInspirationPhotos('Event Space');
            $data['hotel'] = $this->getInspirationPhotos('Hotel');
            $data['reception'] = $this->getInspirationPhotos('Reception');
            $data['suppliers'] = Company::where('is_active', 1)->take(5)->get();
            $data['decors'] = Product::where('is_published', 1)->take(5)->get();
            $data['first_outdoor'] = Inspiration::where('is_published', 1)->with('images')->first();
            return $data;
        });

        return view('site.homepage', $data);
    }

    function home(){
        $data['wedding'] = $this->getInspirationPhotos('Wedding');
        $data['corporate'] = $this->getInspirationPhotos('Corporate');
        $data['cultural'] = $this->getInspirationPhotos('Cultural');
        $data['special_event'] = $this->getInspirationPhotos('Special Events');
        $data['outdoor'] = $this->getInspirationPhotos('Outdoor');
        $data['bar_restaurant'] = $this->getInspirationPhotos('Bar / Restaurant');
        $data['boats_cruises'] = $this->getInspirationPhotos('Boats & Cruises');
        $data['destination'] = $this->getInspirationPhotos('Destination');
        $data['event_space'] = $this->getInspirationPhotos('Event Space');
        $data['hotel'] = $this->getInspirationPhotos('Hotel');
        $data['reception'] = $this->getInspirationPhotos('Reception');

        return view('site.homepage', $data);
    }


    function accept($id){
        $inspirationtag = SupplierInspiration::findOrFail($id);
        $inspirationtag->status = 'confirmed';
        $inspirationtag->save();
        return redirect()->route('site.inspiration.tags.accepted');
    }

    function accepted(){
        $agent = new Agent;
        $browser = $agent->browser();

        if($browser == "Chrome"){
            $appLink = "designa://designa.studio";
            // $appLink = "intent://splash/#Intent;scheme=designa;package=com.globalenablement.designa;S.browser_fallback_url=http%3A%2F%2Fdesigna.studio;end";
        }else if($browser == "Firefox"){
            $appLink = "designa://designa.studio";
        }else{
            $appLink = "designa://designa.studio";
        }
        return view('tool.inspiration-tag-accepted', compact('agent', 'appLink'));
    }


    function getInspirationPhotos($category){
        $inspirations =  Inspiration::whereHas('categories', function($q) use ($category) {
                $q->where('name', $category);
            })->get();

        $imageCount = 0;
        foreach($inspirations as $item){
            $imageCount += $item->images->count();
        }

        return $imageCount;

    }


    function privacyPolicy(){
        return view('site.misc.privacy-policy');
    }


    function termCondition(){
        return view('site.misc.term-condition');
    }


     function mobileLanding(){
        $agent = new Agent;
        $browser = $agent->browser();

        // if($browser == "Chrome"){
        //     $appLink = "designa://designa.studio";
        //     // $appLink = "intent://splash/#Intent;scheme=designa;package=com.globalenablement.designa;S.browser_fallback_url=http%3A%2F%2Fdesigna.studio;end";
        // }else if($browser == "Firefox"){
        //     $appLink = "designa://designa.studio";
        // }else{
        //     $appLink = "designa://designa.studio";
        // }

        if($agent->is('iPhone')){
            $appLink = 'https://apps.apple.com/us/app/designa-studio/id1495137130';
        }else{
            $appLink = "https://play.google.com/store/apps/details?id=com.globalenablement.designa";
        }

        return view('site.landing', compact('appLink', 'agent'));
     }

     function mobileEmailVerified(){
        $agent = new Agent;
        $browser = $agent->browser();

        if($agent->is('iPhone')){
            $appLink = 'https://apps.apple.com/us/app/designa-studio/id1495137130';
        }else{
            $appLink = "https://play.google.com/store/apps/details?id=com.globalenablement.designa";
        }

        return view('site.misc.email-verified', compact('appLink', 'agent'));
     }


     function mobileProfileLiveLanding(){
        $agent = new Agent;
        $browser = $agent->browser();

        // if($browser == "Chrome"){
        //     $appLink = "designa://designa.studio";
        //     // $appLink = "intent://splash/#Intent;scheme=designa;package=com.globalenablement.designa;S.browser_fallback_url=http%3A%2F%2Fdesigna.studio;end";
        // }else if($browser == "Firefox"){
        //     $appLink = "designa://designa.studio";
        // }else{
        //     $appLink = "designa://designa.studio";
        // }

        if($agent->is('iPhone')){
            $appLink = 'https://apps.apple.com/us/app/designa-studio/id1495137130';
        }else{
            $appLink = "https://play.google.com/store/apps/details?id=com.globalenablement.designa";
        }

        return view('site.live-profile-landing', compact('appLink', 'agent'));
     }


    function assistanceForm(Request $request){
        return view('site.supplier-assistance-form');
    }


    function eventRegistration(){
        return view('site.event-registration.index');
    }



    function eventRegistrationPost(Request $request){
        for($i = 0; count($request->name) > $i; $i++){

        }


        //for the mean time it sends only to admins


        Mail::to([$request->email[0]])
            ->send(new EventRegistration($request));


        // Mail::to(['jbdev21@gmail.com'])
        //     ->send(new EventRegistration($request));
        Mail::to(['michael@designa.studio', 'nick@designa.studio', 'hello@designa.studio'])
            ->cc('mildred@globalenablement.com')
            ->send(new EventRegistration($request));
    }

    function assistanceFormStore(Request $request){
        if($request->hasFile('business_profile_image')){
            $business_profile_image = $request->business_profile_image->store('public');
        }

        if($request->hasFile('business_cover_image')){
            $business_cover_image = $request->business_cover_image->store('public');
        }

        if($request->hasFile('business_logo')){
            $business_logo = $request->business_logo->store('public');
        }

        $categories = "";

        foreach($request->categories as $category){
            $categories .= $category . '<br>';
        }

        $html = "
            <h1>Business Profile</h1>

            <h3>Personal Details</h3>
            <table>
                    <tr>
                        <td>Full Name</td>
                        <td>: $request->name</td>
                    </tr>
                    <tr>
                        <td>Email Address</td>
                        <td>: $request->email </td>
                    </tr>
                    <tr>
                        <td>Mobile Number</td>
                        <td>: $request->mobile_number</td>
                    </tr>
            </table>

            <h3>Business Details</h3>
            <table>
                    <tr>
                        <td>Businesss Name</td>
                        <td>: $request->business_name</td>
                    </tr>
                    <tr>
                        <td>Businesss Address</td>
                        <td>: $request->business_address</td>
                    </tr>
                    <tr>
                        <td>Brief Businesss Description.</td>
                        <td>: $request->description</td>
                    </tr>
                    <tr>
                        <td valign='top'>Categories</td>
                        <td>:  $categories </td>
                    </tr>
                    <tr>
                        <td>Business Profile Image</td>
                        <td>: see attachment below </td>
                    </tr>
                    <tr>
                        <td>Business Banner Image</td>
                        <td>: see attachment below</td>
                    </tr>
                    <tr>
                        <td>Business Logo</td>
                        <td>: see attachment below </td>
                    </tr>
                    <tr>
                        <td>Specific areas you service</td>
                        <td>: $request->areas_of_services</td>
                    </tr>
                    <tr>
                        <td>Type of service you offer</td>
                        <td>: $request->offers</td>
                    </tr>
                    <tr>
                        <td>Achievements or industry recognition</td>
                        <td>: $request->achievements</td>
                    </tr>
            </table>
        ";
        Mail::html($html, function($message) use($request, $business_profile_image, $business_cover_image , $business_logo) {
            $message->subject('Designa Business Profile')
                ->from('no-reply@designa.studio')->to(['michael@designa.studio', 'hello@designa.studio'])
                ->cc(['mildred@globalenablement.com'])
                ->attach(storage_path('app/' . $business_profile_image),['as' => 'profile-image.jpg'])
                ->attach(storage_path('app/' . $business_cover_image), ['as' => 'business-cover.jpg'])
                ->attach(storage_path('app/' . $business_logo), ['as' => 'business-logo.jpg']);
        });

        Storage::delete([$business_profile_image, $business_cover_image, $business_logo]);

        return back()->with('message', 'Thank you for submitting your business profile.');

    }


     function sendInquiry(Request $request){
        Mail::to(['michael@designa.studio', 'nick@designa.studio', 'hello@designa.studio'])
            ->cc('mildred@globalenablement.com')
            ->send(new InquiryMail($request));

        if(Mail::failures()){
            return 0;
        }
     }

     function registerEmail(Request $request){

        Mail::raw('Someone wants to get started email: ' .$request->email, function($message) use($request) {
            $message->subject('Designa Email Registration')->from('no-reply@designa.studio')->to(['michael@designa.studio', 'hello@designa.studio'])->cc(['mildred@globalenablement.com', 'jofie@globalenablement.com']);
        });

        if(Mail::failures()){
            return 0;
        }
     }

     function registerNewsLetterEmail(Request $request){

        Mail::raw('New Email for newsletter: ' .$request->email, function($message) use($request) {
            $message->subject('Designa Newsletter Email Registration')->from('no-reply@designa.studio')->to(['michael@designa.studio', 'hello@designa.studio'])->cc(['mildred@globalenablement.com', 'jofie@globalenablement.com']);
        });

        if(Mail::failures()){
            return 0;
        }
     }

     function requestWhiteGloveService(Request $request){

        Mail::raw('Someone Request for White Globe Service: ' .$request->email, function($message) use($request) {
            $message->subject('Designa Request for White Globe Service')->from('no-reply@designa.studio')->to(['michael@designa.studio', 'hello@designa.studio'])->cc(['mildred@globalenablement.com', 'jofie@globalenablement.com']);
        });

        if(Mail::failures()){
            return 0;
        }
     }

     function registerWebinar(Request $request){
        $data = "<h3>Someone register for Webinar</h3>";
        $data .= "<table>
                    <tr>
                        <td>First Name</td>
                        <td>: $request->first_name</td>
                    </tr>
                    <tr>
                        <td>Last Name</td>
                        <td>: $request->last_name</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>: $request->email</td>
                    </tr>
                    <tr>
                        <td>Company</td>
                        <td>: $request->company</td>
                    </tr>

                    <tr>
                        <td>How did you know about the webinar</td>
                        <td>: $request->webinar_option</td>
                    </tr>
                </table>";

        Mail::html($data, function($message) use($request) {
            $message->subject('Designa Webinar Registration')->from('no-reply@designa.studio')->to(['michael@designa.studio', 'hello@designa.studio'])->cc(['mildred@globalenablement.com', 'jofie@globalenablement.com']);
        });

        if(Mail::failures()){
            return 0;
        }
     }

     function requestDemo(Request $request){
        $subject = $request->type == "demo" ? 'Request a Demo' : 'Request White Glove Service';
        $data = "<h3>Someone " .  $subject . "</h3>";
        $data .= "<table>
                    <tr>
                        <td>Name</td>
                        <td>: $request->name</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>: $request->email</td>
                    </tr>
                    <tr>
                        <td>Company</td>
                        <td>: $request->company</td>
                    </tr>
                </table>";

        Mail::html($data, function($message) use($request, $subject) {
            $message->subject($subject)->from('no-reply@designa.studio')->to(['michael@designa.studio', 'hello@designa.studio'])->cc(['mildred@globalenablement.com', 'jofie@globalenablement.com']);
        });

        if(Mail::failures()){
            return 0;
        }
     }

     function requestDemoCovid(Request $request){
        $subject = 'Request a Demo';
        $address = "Address";
        $addressInfo = $request->address;
        if($request->type){
            if($request->type == "venue-directory"){
                $address = "Business Name";
                $addressInfo = $request->business_name;
            }
        }
        $data = "<h3>Someone " .  $subject . "</h3>";
        $data .= "<table>
                    <tr>
                        <td> Business Name</td>
                        <td>:$addressInfo</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>: $request->email</td>
                    </tr>
                    <tr>
                        <td>Contact</td>
                        <td>: $request->contact</td>
                    </tr>
                </table>";

        Mail::html($data, function($message) use($request, $subject) {
            $message->subject($subject)->from('no-reply@designa.studio')->to(['michael@designa.studio', 'hello@designa.studio', 'nick@designa.studio'])->cc(['mildred@globalenablement.com']);
        });

        if(!$request->ajax()){
            return back();
        }
        if(Mail::failures()){
            return 0;
        }
     }

     function venueMarketingUsaSendEmail(Request $request){
        $html = "
                <h3>Market Leader from USA</h3>
                <table>
                    <tr>
                        <td>Name</td>
                        <td>: $request->name</td>
                    </tr>
                    <tr>
                        <td>Business Name</td>
                        <td>: $request->bisname</td>
                    </tr>
                    <tr>
                        <td>Business type</td>
                        <td>: $request->bistype</td>
                    </tr>
                    <tr>
                        <td>Email Address</td>
                        <td>: $request->email</td>
                    </tr>
                    <tr>
                        <td>Contact</td>
                        <td>: $request->contact</td>
                    </tr>
                </table>";



        return (new HtmlMailerService())
            ->subject('From market leader form USA')
            ->html($html)
            ->send();
     }


    function landing1(){
        return view('site.landingpage.index');
    }

    function landing2(){
        return view('site.landingpage.3d-technology');
    }

    function landing3(){
        return view('site.landingpage.cost-effective');
    }

    function venueDirectory(){
        return view('site.landingpage.venue-directory');
    }

    function venueMarketingUsaPreregister(){
        return view('site.landingpage.venue-marketing-usa-preregister');
    }

    function venueMarketingUsa(){
        return view('site.landingpage.venue-marketing-usa');
    }

    function venueTours(){
        return view('site.landingpage.venue-tours');
    }

    function contestRegistration(){
        return view('site.landingpage.contest-registration');
    }

    function contestRegistrationSend(Request $request){
        $html = "
            <h3>Registration from 3d Contest</h3>
            <table>
                <tr>
                    <td>Name</td>
                    <td>: $request->name</td>
                </tr>
                <tr>
                    <td>Business Name</td>
                    <td>: $request->company</td>
                </tr>
                <tr>
                    <td>Email Address</td>
                    <td>: $request->email</td>
                </tr>
                <tr>
                    <td>Contact</td>
                    <td>: $request->contact</td>
                </tr>
            </table>";



        (new HtmlMailerService())
            ->subject('Registration from 3d Contest')
            ->html($html)
            ->send();

        $forUser = "
            Hello! <br><br>

            We thank you for your interest in joining the competition!<br><br>

            You have successfully registered in our online system, and we’re excited to see your beautiful design in 3D.<br><br>

            Should you have any questions, email us at hello@designa.studio<br><br>

            Cheers,<br>
            Designa Team
            ";

        Notification::route('mail', [
            $request->email => $request->email,
        ])->notify(new ContestRegigrationNotification());

        // (new HtmlMailerService())
        //     ->reciepients([$request->email])
        //     ->subject('Designa - Successfully Registration')
        //     ->html($forUser)
        //     ->send();

    }
}
