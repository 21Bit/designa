<?php

namespace App\Http\Controllers\Site;

use DB;
use Carbon\Carbon;
use App\Models\Heart;
use App\Models\Venue;
use App\Models\Company;
use App\Models\Category;
use App\Models\Inspiration;
use App\Facades\FirebaseFMC;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Venue\Web\Venue as VenueResource;

class VenueController extends Controller
{
    function index(Request $request){
        $defaultCategory = $request->cat ? Category::whereSlug($request->cat)->firstOrFail()->id : '';
        return view('site.venue.index', compact('defaultCategory'));
    }

    function list(Request $request){
        $key = 'venue.' . serialize($request->all());
//         return Cache::remember($key, Carbon::now()->addSeconds(15), function() use($request){
            $list = Venue::query()
                    ->addSelect('venues.id as id')
                    ->addSelect('venues.name as name')
                    ->addSelect('venues.slug as slug')
                    ->addSelect('venues.location as location')
                    ->addSelect('companies.slug as company_slug')
                    ->addSelect('venues.description as description')
                    ->join('categorizables', function($join){
                        $join->on('venues.id', '=', 'categorizables.categorizable_id')
                            ->where('categorizables.categorizable_type', "App\Models\Venue");
                    })
                    ->join('categories',function($join){
                            $join->on('categorizables.category_id', '=', 'categories.id');
                    })
                    ->join('companies',function($join){
                            $join->on('venues.company_id', '=', 'companies.id');
                    });
            // return $list->get();
           if($request->latitude && $request->longitude){
               $list->addSelect(
                       DB::raw('( 6367 * acos( cos( radians(' . $request->latitude . ') )
                       * cos( radians( venues.latitude ) )
                       * cos( radians( venues.longitude ) - radians('. $request->longitude . ') )
                       + sin( radians('. $request->latitude .') )
                       * sin( radians( venues.latitude ) ) ) ) AS address_distance')
                   )
                   ->whereNotNull('venues.latitude')
                   ->whereNotNull('venues.longitude')
                   ->orderBy('address_distance');

               if($request->distance != null){
                   $list->having('address_distance', '<=', $request->distance);
               }
           }

            if($request->categories){
                $list->whereIn('categories.id', $request->categories);
            }

            if($request->search){
                $list = $list->where('name', 'LIKE', '%'. $request->search . '%');
            }


            if($request->supplier_id){
                $list->where('company_id', $request->supplier_id);
            }

            if($request->supplier){
                $list->where('companies.name','LIKE', '%' . $request->supplier . '%');
            }

            if($request->capacity){
                $list->where('capacity','>=', $request->capacity);
            }

            if($request->venue_id){
                $list->where('venues.id', $request->venue_id);
            }

            $venues = $list->withCount(['reviews', 'hearts', 'images', 'shares'])
                        ->with('categories')
                        ->where('is_published', 1)
                        ->groupBy('venues.id')
                        ->paginate(12);

            return VenueResource::collection($venues);

//         });
    }


    function relatedList(Request $request){
        $venues = Venue::query()
                        ->addSelect('venues.id as id')
                        ->addSelect('venues.name as name')
                        ->addSelect('venues.slug as slug')
                        ->addSelect('venues.location as location')
                        ->addSelect('companies.slug as company_slug')
                        ->addSelect('venues.description as description')
                        ->join('companies',function($join){
                            $join->on('venues.company_id', '=', 'companies.id');
                    })
                    ->where('company_id', $request->supplier_id)
                    ->where('venues.id', '!=', $request->current_id)->paginate(12);
        return VenueResource::collection($venues);
    }


    function show(Request $request, $slug){
        $venue = Venue::whereSlug($slug)->firstOrFail();
        $venue->viewed($request);
        $functionspaces = $venue->functionspaces()->get();
        $gallery = $venue->images()->where('type', 'gallery')->get();
        $explodedTimes = explode(',', $venue->operating_hours);
        $otherVenues = Venue::whereCompanyId($venue->company_id)->where('id', '!=', $venue->id)->count();
        $inspirations = Inspiration::whereVenueId($venue->company_id)->take(10)->get();
        return view('site.venue.show', compact("venue", "functionspaces", "gallery","otherVenues", "explodedTimes",'inspirations', 'otherVenues'));
    }

    function hearted(Request $request){

        $venue = Venue::find($request->id);
        $supplier  = Company::find($venue->company_id);

        if(!$request->user()){
            return response()->json([
                'message' => 'User not authentecated',
                'status'    => '401'
            ], 200);
        }

        $hearted = $venue->hearts()->whereUserId($request->user()->id)->first();
        if($hearted){

            $heartedStatus = 0;
            $venue->hearts()->whereUserId($request->user()->id)->first()->delete();

        }else{

            $heart = new Heart;
            $heart->user_id = $request->user()->id;
            $venue->hearts()->save($heart);
            $heartedStatus = 1;

            // send notification
            $firebaseArray = array();
            foreach($supplier->users()->has('firebases')->get() as $user){
                foreach($user->firebases as $firebaseAccount){
                    array_push($firebaseArray, $firebaseAccount->firebase_id);
                }
            }

            $firebase = new FirebaseFMC;
            $firebase->sendSingle(
                $firebaseArray,
                array(
                    'title'         => 'Hearted product',  // title,
                    'avatar'        => '/dashboard/img/avatar04.png',
                    'message'       => $request->user()->name . ' has saved your inspiration entitled ' . $venue->name,  // message,
                    'notif_type'    => 'hearted_product',
                    'item_id'       => $venue->id,
                    'link'          => '/',//route('site.decor.show', $venue->slug),
                    'image'         => $venue->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),  // image
                    'timestamp'     => Carbon::now()->toDateTimeString() // timestamp
                )
            );
        }

        return [
            'status' => 200,
            'hearted' => $heartedStatus
        ];
    }
}
