<?php

namespace App\Http\Controllers\Site;

use DB;
use Auth;
use Cache;
use App\Services\CategoryService;
use Carbon\Carbon;
use App\Models\Heart;
use App\Models\Company;
use App\Models\Category;
use App\Models\Inspiration;
use App\Facades\FirebaseFMC;
use Illuminate\Http\Request;
use App\Models\SupplierInspiration;
use App\Http\Controllers\Controller;
use App\Http\Resources\Cater\CategoryCollection as CaterCategories;
use App\Http\Resources\Color\CategoryCollection as ColorCategories;
use App\Http\Resources\Inspiration\Web\Inspiration as InspirationResource;

class InspirationController extends Controller
{
    function index(Request $request){
        $category = $request->cat ? Category::whereSlug($request->cat)->firstOrFail() : '';

        if($category){
            if($category->parent_id){
                // for child category
                $defaultCategoryParent = $category->parent_id;
                $defaultCategory = $category->id;

            }else{
                // for parent category
                $defaultCategory = '';
                $defaultCategoryParent = $category->id;
            }
        }else{
            $defaultCategory = '';
            $defaultCategoryParent = '';
        }

        return view('site.inspiration.index', compact('defaultCategory', 'defaultCategoryParent'));

    }

    function list(Request $request){
        $key = 'inspiration.' . serialize($request->all());
        return Cache::remember($key, Carbon::now()->addMinutes(3), function() use($request){
            $list = Inspiration::query();

            $event_types = $request->event_types;
            $styles = $request->styles;
            $settings = $request->settings;
            $colors = $request->colors;

            if($request->latitude && $request->longitude){

                $list->addSelect(['address_distance' => function($query) use ($request){
                            $query->selectRaw("( 6367 * acos( cos( radians($request->latitude) )
                                * cos( radians( inspirations.latitude ) )
                                * cos( radians( inspirations.longitude ) - radians($request->longitude) )
                                + sin( radians($request->latitude) )
                                * sin( radians( inspirations.latitude ) ) ))");
                        }
                    ])
                    ->whereNotNull('latitude')
                    ->whereNotNull('longitude')
                    ->orderBy('address_distance');

                if($request->distance != null){
                    $list->having('address_distance', '<=', $request->distance);
                }
            }


            // For Event Setting
            if($event_types){
                $list->whereHas('categories',function($q) use ($event_types){
                    $q->whereType('cater')->whereIn('id', $event_types);
                });
            }
            // For Event Setting
            if($settings){
                $list->whereHas('categories',function($q) use ($settings){
                    $q->whereType('setting')->whereIn('id', $settings);
                });
            }

            // For Event Style
            if($styles){
                $list->whereHas('categories',function($q) use ($styles){
                    $q->whereType('style')->whereIn('id', $styles);
                });
            }

            if($colors){
                $list->whereHas('categories',function($q) use ($colors){
                    $q->whereType('color')->whereIn('id', $colors);
                });
            }

            if($request->venue_id){
                $list->where('venue_id', $request->venue_id);
            }

            if($request->supplier){
                $company  = Company::find($request->supplier);

                if($request->scope == "tagged"){
                    $list->whereIn('venue_id', $company->venues->map(function($q){
                                    return  $q->id;
                                })->toArray())
                                ->orWhereHas('supplierRoles', function($q) use ($request){
                                    $q->where('company_id', $request->supplier);
                                });
                }else if($request->scope  == "owned"){
                    $list->whereCompanyId($request->supplier);
                }else{
                    $list->whereCompanyId($request->supplier)
                                ->orWhereIn('venue_id', $company->venues->map(function($q){
                                    return $q->id;
                                })->toArray())
                                ->orWhereHas('supplierRoles', function($q) use ($request){
                                    $q->where('company_id', $request->supplier);
                                });
                }

                $inspirations = $list->with('company')
                                    ->WithActiveCompany()
                                    ->with('venue')
                                    ->withCount(['hearts', 'images', 'reviews'])
                                    ->where('is_published', 1)->orderBy('created_at', 'DESC')->paginate(20);

            }else{
                $inspirations = $list->with('company')
                                    ->groupBy('inspirations.id')
                                    ->WithActiveCompany()
                                    ->withCount(['hearts', 'reviews', 'shares'])
                                    ->where('is_published', 1)->orderBy('created_at', 'DESC')->paginate(20);
            }

            return InspirationResource::collection($inspirations);

        });

    }

    function filterCategories()
    {
        $data = [];
        $data['events'] = new CaterCategories((new CategoryService)->with('styles')->type('cater')->get());
        $data['colors'] = new ColorCategories((new CategoryService)->type('color')->get());
        return $data;
    }

    function show(Request $request, $slug){

         $inspiration = Cache::remember('inspiration.' . $slug, Carbon::now()->addDays(1) , function() use ($slug) {
                 return Inspiration::whereSlug($slug)
                        ->with('images')
                        ->with('categories')
                        ->with('videos')
                        ->with('company')
                        ->where('is_published', 1)->with('images')->firstOrFail();
             });

        //initiate View function
        $inspiration->viewed($request);

        $colors =  Cache::remember('inspiration.' . $slug . '.colors', Carbon::now()->addDays(1) , function() use ($inspiration){
                    return $inspiration->categories()->where('type', 'color')->get();
                });

        $setting =  Cache::remember('inspiration.' . $slug . '.setting', Carbon::now()->addDays(1) , function() use ($inspiration){
                    return optional($inspiration->categories()->whereType('setting')->first())->name;
                });

        $gallery = Cache::remember('inspiration.' . $slug . '.gallery', Carbon::now()->addMinutes(5) , function() use ($inspiration){
                    return $inspiration->images()->where('type', 'gallery')->get();
                });

        $videos = Cache::remember('inspiration.' . $slug . '.videos', Carbon::now()->addDays(1) , function() use ($inspiration){
                    return $inspiration->videos()->where('type', 'inspiration_video')->get();
                });

        $suppliers = Cache::remember('inspiration.' . $slug . '.suppliers', Carbon::now()->addMinutes(3) , function() use ($inspiration){
                        return $inspiration->supplierRoles()->where('status', 'confirmed')->has('role')->get()->map(function($q){
                            if(Auth::user()){
                                if($q->company){
                                    $isHearted = $q->company->hearts()->whereUserId(Auth::user()->id)->first() ? 1 : 0;
                                }else{
                                    $isHearted = 0;
                                }
                            }else{
                                $isHearted = 0;
                            }

                            return [
                                'company_id' => $q->company ? $q->company->id : '',
                                'isCompany' => $q->company ? 1 : 0,
                                'name' => $q->company ? $q->company->name : $q->name,
                                'slug' => $q->company ? $q->company->slug : '',
                                'role'  => $q->role->display_name,
                                'isHearted' => $isHearted
                            ];
                        });
                    });


        return view('site.inspiration.show', compact('inspiration', 'gallery', 'videos', 'colors', 'setting', 'suppliers'));
    }

    function requestTag(Request $request){

        if(SupplierInspiration::where('company_id', $request->user()->company_id)->where("inspiration_id", $request->inspiration_id)->where('role_id', $request->role_id)->count()){

            if($request->ajax()){
                return response()->json([
                    'status' => 200,
                    'message' => 'Request is already exist!'
                ],200
            );
            }else{
                return back();
            }

        }else{
                // $company = Company::find($request->user()->company_id);
                // $inspiration = Inspiration::find($request->inspiration_id);
                // $role = Role::find($request->role_id);

                $supplier = new SupplierInspiration;
                $supplier->role_id = $request->role_id;
                $supplier->inspiration_id = $request->inspiration_id;
                $supplier->request_by = $request->user()->company_id; // this might optional
                $supplier->company_id = $request->user()->company_id; // this might optional
                $supplier->name = $request->user()->company->supplier; // this might optional
                $supplier->status = 'pending';
                $supplier->save();


                if($request->ajax()){
                    return response()->json([
                        'status' => 200,
                        'message' => 'Request Sent!'
                    ],200
                );
                }else{
                   return  back();
                }

        }
    }




    function hearted(Request $request){
        $inspiration = Inspiration::find($request->id);
        $supplier  = Company::find($inspiration->company_id);

        if(!$request->user()){
            return response()->json([
                'message' => 'User not authentecated',
                'status'    => '401'
            ], 200);
        }

        $hearted = $inspiration->hearts()->whereUserId($request->user()->id)->first();
        if($hearted){
            $heartedStatus = 0;
            $inspiration->hearts()->whereUserId($request->user()->id)->first()->delete();
        }else{
            $heart = new Heart;
            $heart->user_id = $request->user()->id;
            $inspiration->hearts()->save($heart);
            $heartedStatus = 1;

            // send notification
            $firebaseArray = array();
            foreach($supplier->users()->has('firebases')->get() as $user){
                foreach($user->firebases as $firebaseAccount){
                    array_push($firebaseArray, $firebaseAccount->firebase_id);
                }
            }

            $firebase = new FirebaseFMC;
            $firebase->sendSingle(
                $firebaseArray,
                array(
                    'title'         => 'Hearted Product',  // title,
                    'avatar'        => '/dashboard/img/avatar04.png',
                    'message'       => $request->user()->name . ' has saved your product entitled ' . $inspiration->name,  // message,
                    'notif_type'    => 'hearted_product',
                    'item_id'       => $inspiration->id,
                    'link'          => '/',//route('site.decor.show', $inspiration->slug),
                    'image'         => $inspiration->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),  // image
                    'timestamp'     => Carbon::now()->toDateTimeString() // timestamp
                )
            );
        }

        return [
            'status' => 200,
            'hearted' => $heartedStatus
        ];
    }
}
