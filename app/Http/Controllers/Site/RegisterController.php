<?php

namespace App\Http\Controllers\Site;

use App\Models\Role;
use App\Models\User;
use App\Models\Company;
use App\Models\Category;
use Jenssegers\Agent\Agent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegisterController extends Controller
{

    function type(){
        return view('site.registration.type-selection');
    }

    function registerSupplier(){
        $caters = Category::whereType('cater')->get();
        $roles = Role::orderBy('name')->get();
        return view('site.registration.register-supplier', compact('caters', 'roles'));
    }

    function registerCustomer(){
        return view('site.registration.register-customer');
    }

    function registerSupplierStore(Request $request){

        // Verify
        $this->validate($request, [
            "email" =>  "email|nullable|unique:users",
            "company_name" =>  "required|string",
            "categories" =>  "required",
            "event_types" =>  "required",
            "town" =>  "required",
            "state" =>  "required",
            "post_code" =>  "required",
            "telephone_number" =>  "required",
            // "password" =>  "confirmed",
        ]);

        $company = new Company;
        $company->name = $request->company_name;
        $company->description = $request->description;

        $company->town = $request->town;
        $company->state = $request->state;
        $company->country = 'australia'; //$request->country;
        $company->post_code = $request->post_code;

        $company->email = $request->email;
        $company->telephone_number = $request->telephone_number;

        $company->save();
        $company->categories()->sync($request->event_types);
        $company->roles()->sync($request->categories);

        $user = new User;
        $user->name = $request->company_name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->type = "supplier";
        $user->company_id = $company->id;

//        if(config('subscription.september_promo')){
//            $user->trial_ends_at = now()->addDays(config('subscription.september_promo_days_trial'));
//        }

        $user->save();
        $company->asyncRoleToUsers();
        $user->sendEmailVerificationNotification();
        $user->createAsStripeCustomer();
        return redirect()->route('site.register.supplier.success', ['t' => 's']);
    }


    function registerCustomerStore(Request $request){

        $this->validate($request, [
            "name" =>  "required|string",
            "email" =>  "email|unique:users",
            "password" =>  "required|confirmed",
        ]);

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->type = "customer";
        $user->save();

        $user->sendEmailVerificationNotification();

        if(!$request->ajax()){
            return redirect()->route('site.register.supplier.success', ['t' => 'c']);
        }
    }


    function success(Request $request){
        $agent = new Agent;
        $browser = $agent->browser();

        if($browser == "Chrome"){
            $appLink = "designa://designa.studio";
            // $appLink = "intent://splash/#Intent;scheme=designa;package=com.globalenablement.designa;S.browser_fallback_url=http%3A%2F%2Fdesigna.studio;end";
        }else if($browser == "Firefox"){
            $appLink = "designa://designa.studio";
        }else{
            $appLink = "designa://designa.studio";
        }
        return view('site.registration.success', compact('agent', 'appLink'));
    }
}
