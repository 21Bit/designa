<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Models\SubscriptionPlan;
use App\Http\Controllers\Controller;

class SubscriptionController extends Controller
{
    function index(Request $request){
        // $request()->user->subscription('Intermediate')->cancel();
    
        $plans = SubscriptionPlan::orderBy('price')->get();
        return view('site.subscription.prepare', compact('plans'));
    }

    function store(Request $request){
        $plan = SubscriptionPlan::findOrFail($request->plan);
        $user = $request->user();
        
        if($user->subscriptions()->count()){
            $current = $user->stripeSubscription();
            $currentPlanId = $current['plan']['id'];
            $user->subscription("Designa Plan")->noProrate()->swap($plan->stripe_plan);
        }else{
            $paymentMethod = $request->paymentMethod;
            $user->newSubscription("Designa Plan", $plan->stripe_plan)
                ->create($paymentMethod, [
                    'email' => $user->email,
                ]);
        }
        
        if($request->ajax()){
            return response()->json([
                'status' => 200,
                'message' => 'Subscription saved!'
            ], 200);
        }else{
            return redirect()->route('dashboard.profile.index', ['tab' => 'subscription']);
        }
    }
}
