<?php

namespace App\Http\Controllers\Site\User;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificationController extends Controller
{
    function index(Request $request){

        $notifications = Auth::user()->notifications()->paginate(15);
        return view("site.user.notification.index", compact("notifications"));

    }

    function destroy($id){
        Auth::user()->notifications()->whereId($id)->delete();
        if(!request()->ajax()){
            return back();
        }
    }

    function deletelAll(){
        Auth::user()->notifications()->delete();
        if(!request()->ajax()){
            return back();
        }
    }
}
