<?php

namespace App\Http\Controllers\Site\User;

use Auth;
use App\Models\Venue;
use App\Models\Inspiration;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;

class SaveItemController extends Controller
{
    function inspirations(){
        $inspirations = Inspiration::whereHas('hearts', function($q){
            $q->where('user_id', Auth::user()->id);
        })->paginate(15);

        return view('site.user.saved.inspirations', compact('inspirations'));
    }


    function venues(){
        $venues = Venue::whereHas('hearts', function($q){
            $q->where('user_id', Auth::user()->id);
        })->paginate(15);

        return view('site.user.saved.venues', compact('venues'));
    }

    function decors(){
        $decors = Product::whereHas('hearts', function($q){
            $q->where('user_id', Auth::user()->id);
        })->paginate(15);

        return view('site.user.saved.products', compact('decors'));

    }
}
