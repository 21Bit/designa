<?php

namespace App\Http\Controllers\Site\User;


use Auth;
use App\Models\Project;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Inspiration\Web\EventBoardInspirationSelection;
use App\Http\Resources\Product\Web\EventBoardProductSelection;
use App\Models\Category;
use App\Models\Inspiration;
use App\Models\Product;
use Illuminate\Support\Facades\Cache;
use App\Http\Resources\MoodBoard\Project as ProjectResouce;

class EventBoardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::whereUserId(Auth::user()->id)->orderBy('created_at', 'DESC')->paginate(15);
        return view('site.user.event-board.index', compact('projects'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'description' => 'required'
        ]);

        $project = Project::create([
            'title' => $request->title,
            'description' => $request->description,
            'user_id' => $request->user()->id,
        ]);

        return redirect()->route('user.event-board.edit.categories', $project->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function editCategories(Project $project)
    {
        $colors = Category::whereType('color')->get();
        $settings = Category::whereType('setting')->get();
        $styles = Category::whereType('style')->whereModule('category')->get();
        $themes = $settings->merge($styles);
        $product_types = Category::whereType('product_type')->get();
        $selectedCategories = $project->categories()->pluck('id')->toArray();
        return view('site.user.event-board.categories', compact('project', 'colors', 'themes', 'product_types', 'selectedCategories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $project Project
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateCategories(Request $request, Project $project)
    {
        $this->validate($request, [
            'themes' => ['required','array'],
            'product_types' => ['required', 'array'],
            'colours' => ['required', 'array']
        ]);

        $project->categories()->detach();
        $project->categories()->attach([...$request->themes, ...$request->product_types, ...$request->colours]);
        $project->clearCache();

        if($request->redirect){
            return redirect($request->redirect);
        }

        return back()->with('message', 'Categories saved!');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function editInspirations(Project $project)
    {
        return view('site.user.event-board.inspirations', compact('project'));
    }

    public function inspirationList(Request $request, Project $project): \Illuminate\Http\Resources\Json\AnonymousResourceCollection
    {
        if($request->saved){
            $query = $request->user()->heartedInspirations();
        }else{
            $query = Inspiration::whereDoesntHave('hearts', function($q){
                $q->where('user_id', request()->user()->id);
            });
        }

        $categories = $project->categories()->pluck('id')->toArray();
        // For Event Setting
        if($categories){
            $query->whereHas('categories',function($q) use ($categories){
                $q->whereIn('id', $categories);
            });
        }

        $query->whereHas('images', function($q){
            $q->where('type', 'gallery');
        });

        return EventBoardInspirationSelection::collection($query->paginate(10));
    }


     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function editDecors(Project $project)
    {
        return view('site.user.event-board.decors', compact('project'));
    }

    public function decorList(Request $request, Project $project): \Illuminate\Http\Resources\Json\AnonymousResourceCollection
    {
        if($request->saved){
            $query = $request->user()->heartedProducts();
        }else{
            $query = Product::query();
        }

        $categories = $project->categories()->pluck('id')->toArray();
        if($categories){
            $query->whereHas('categories',function($q) use ($categories){
                $q->whereIn('id', $categories);
            });
        }

        return EventBoardProductSelection::collection($query->with('company')->paginate(12));
    }


    public function preview($id){
        $project = Project::find($id);
        return view('site.user.event-board.preview', compact('project'));
    }

    public function previewApi($id)
    {
        $project = Project::with(['categories', 'inspirationImages', 'productImages'])->findOrFail($id);
        return new ProjectResouce($project);
    }

    public function attachImage(Request $request): \Illuminate\Http\JsonResponse
    {

        $project = Project::find($request->project_id);

        if($request->attaching){
            $project->inspirationImages()->attach($request->image_id);
        }else{
            $project->inspirationImages()->detach($request->image_id);
        }

        Cache::forget('event-board.imagecount.inspiration.' . '.'. $project->id);
        Cache::forget('event-board.imagecount.product' . '.'. $project->id);
        Cache::forget('event-board.thumbnail.' . $project->id);

        return response()->json([
            'data' => [
                    'status' => 1,
                    'message'  => 'update complete',
                    'type'     => $request->attaching ? "attached" : 'detached'
                ]
        ], 200);
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
