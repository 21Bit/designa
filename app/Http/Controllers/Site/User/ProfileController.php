<?php

namespace App\Http\Controllers\Site\User;

use Auth;
use Hash;
use Session;
use App\Models\User;
use App\Models\Qoute;
use App\Models\Venue;
use App\Models\Product;
use App\Models\Inspiration;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    function index(){
        $notifications = Auth::user()->notifications()->orderBy("created_at", 'DESC')->take(5)->get();
        $saveinspirations = Inspiration::whereHas('hearts', function($q){
            $q->where('user_id', Auth::user()->id);
        })->count();
        $savevenues = Venue::whereHas('hearts', function($q){
            $q->where('user_id', Auth::user()->id);
        })->count();
        $savedecors = Product::whereHas('hearts', function($q){
            $q->where('user_id', Auth::user()->id);
        })->count();

        $quotes = Qoute::whereUserId(Auth::user()->id)->orderBy('created_at', 'DESC')->take(5)->get();
        return view('site.user.profile.index', compact('notifications', 'savedecors', 'saveinspirations' ,'savevenues', 'quotes'));
    }

    function edit(){
        $user = Auth::user();
        return view('site.user.profile.edit' , compact('user'));
    }

    function update(Request $request){
        $user = Auth::user();
        $user->name = $request->name;
        $user->save();

       
        if($request->picture){
            $image = $request->picture;
            $user->uploadPicture($request, true, $image);
        }

        \Session::flash('success', 'Changes Saved..');
        return back();
    }
    
    function changePassword(){
        return view('site.user.profile.changepassword');
    }

    function changePasswordPost(Request $request){
        $this->validate($request,[
            'current_password' => 'required',
            'password' => 'required|string|min:6|confirmed',
        ]);


        if (Hash::check($request->current_password, Auth::user()->password))
        {
            $user = User::find(Auth::user()->id);
            $user->password = bcrypt($request->password);
            $user->save();

            Session::flash('message', 'Password successfully Changed!');
            Session::flash('alert-class', 'alert-success');
        }else{
            Session::flash('message', 'Current Password not Match!');
            Session::flash('alert-class', 'alert-danger');
        }

        return back();
    }
}
