<?php

namespace App\Http\Controllers\Site\User;

use DB;
use Auth;
use Notification;
use Carbon\Carbon;
use App\Models\Qoute;
use App\Models\Company;
use App\Models\Product;
use App\Models\Location;
use App\Models\QouteProduct;
use Illuminate\Http\Request;
use App\Models\PricingComponent;
use App\Http\Controllers\Controller;
use App\Notifications\Supplier\Qoute\NewQouteNotification;

class QouteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $quotes = Qoute::whereUserId(Auth::user()->id)->orderBy('created_at', 'DESC')->paginate(15);
        return view('site.user.qoute.index', compact('quotes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function list(Request $request){
        $data = array();

        $query = Product::whereHas('company', function($q) use ($request) {
                        // $q->selectRaw('ST_Distance_Sphere(
                        //     point(latitude, longitude),
                        //     point(?, ?)
                        // ) * .000621371192 AS distance ', [$request->longitude, $request->latitude])
                        // ->havingRaw("distance < ?", [200])->orWhereHas('locations', function($q) use ($request){
                        //     $q->selectRaw('ST_Distance_Sphere(
                        //         point(latitude, longitude),
                        //         point(?, ?)
                        //     ) * .000621371192 AS distance', [$request->longitude, $request->latitude])
                        //     ->havingRaw("distance < ?", [200]);
                        // });
                        // $q->select(DB::raw('*, ( 6367 * acos( cos( radians('.$request->latitude.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$request->longitude.') ) + sin( radians('.$request->latitude.') ) * sin( radians( latitude ) ) ) ) AS distance'))
                        //     ->orWhereHas('locations', function($q) use ($request){
                        //         $q->select(DB::raw('*, ( 6367 * acos( cos( radians('.$request->latitude.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$request->longitude.') ) + sin( radians('.$request->latitude.') ) * sin( radians( latitude ) ) ) ) AS distance'))
                        //             ->having('distance', '<', 50);
                        //     })->having('distance', '<', 50);

                         $q->select(DB::raw('*, ( 6367 * acos( cos( radians('. $request->latitude. ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$request->longitude.') ) + sin( radians('.$request->latitude.') ) * sin( radians( latitude ) ) ) ) AS distance'))
                            ->whereHas('locations', function($q) use ($request){
                                 $q->select(DB::raw('*, ( 6367 * acos( cos( radians('. $request->latitude. ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$request->longitude.') ) + sin( radians('.$request->latitude.') ) * sin( radians( latitude ) ) ) ) AS distance'))
                                   ->having('distance', '<=', 50);
                            })
                            ->orderBy('distance')->having('distance', '<=', 50);
                    });
        // $query = Product::select(DB::raw('*, ( 6367 * acos( cos( radians('.$request->latitude.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$request->longitude.') ) + sin( radians('.$request->latitude.') ) * sin( radians( latitude ) ) ) ) AS distance'))
        //         ->having('distance', '<', 50);

        if($request->categories){
                $query->whereHas('categories',function($q) use ($request){
                    $q->whereIn('id', $request->categories);
                });
            // foreach($request->categories as $category){
            //     $query->whereHas('categories',function($q) use ($category){
            //         $q->whereIn('id', $category);
            //     });
            // }
        }

        if($request->search){
            $query->where('name', 'LIKE', '%' . $request->search . '%')->orWhereHas('company', function($q) use ($request){
                $q->where('name', 'LIKE', '%' . $request->search . '%');
            });
        }

        if($request->savedDecors){
            $query->whereHas('hearts',function($q) use($request){
                $q->where('user_id', $request->user()->id);
            });
        }
        // else{
        //      $query->doesntHave('hearts',function($q) use($request){
        //         $q->where('user_id', $request->user()->id);
        //     });
        // }

        $decors = $query->take(15)->get();

        foreach($decors as $decor){
            $array = [
                'id'        => $decor->id,
                'name'      => $decor->name,
                'thumbnail' => $decor->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder250x200.png'),
                'company'   => $decor->company->name,
                'quantity'  => 1,
                'hearted'   => $request->user ? $this->hearts()->whereUserId($request->user()->id)->first() ? 1 : 0 : 0
            ];
            array_push($data, $array);
        }

        return $data;

    }
    public function create(Request $request)
    {
        $data = array();

        if($request->ajax()){


            if($request->type == "all"){

                $all = Product::whereHas('company', function($q) use ($request) {
                        $q->selectRaw('ST_Distance_Sphere(
                            point(latitude, longitude),
                            point(?, ?)
                        ) * .000621371192 AS distance ', [$request->longitude, $request->latitude])
                        ->havingRaw("distance < ?", [200])->orWhereHas('locations', function($q) use ($request){
                            $q->selectRaw('ST_Distance_Sphere(
                                point(latitude, longitude),
                                point(?, ?)
                            ) * .000621371192 AS distance', [$request->longitude, $request->latitude])
                            ->havingRaw("distance < ?", [200]);
                        });
                    })
                    ->whereHas('categories', function($q) use ($request){
                        $q->where('id', $request->cater);
                    })
                    ->doesntHave('hearts');

                    if($request->search){
                        if($request->searchtype == "name"){
                            $all->where('name', 'LIKE', '%' . $request->search . '%');
                        }else{
                            $all->whereHas('company', function($q)use ($request){
                                $q->where('name', 'LIKE', '%' . $request->search . '%');
                            });
                        }
                    }

            }else{

                $all = Product::whereHas('company', function($q) use ($request) {
                    $q->selectRaw('ST_Distance_Sphere(
                        point(latitude, longitude),
                        point(?, ?)
                    ) * .000621371192 AS distance ',  [$request->longitude, $request->latitude])
                    ->havingRaw("distance < ?", [100])->orWhereHas('locations', function($q) use ($request){
                        $q->selectRaw('ST_Distance_Sphere(
                            point(latitude, longitude),
                            point(?, ?)
                        ) * .000621371192 AS distance ',  [$request->longitude, $request->latitude])
                        ->havingRaw("distance < ?", [100]);
                    });
                })
                ->whereHas('hearts', function($q){
                    $q->where('user_id', Auth::user()->id);
                })->whereHas('categories', function($q) use ($request){
                    $q->where('id', $request->cater);
                });

            }

            $decors = $all->take(15)->get();

            foreach($decors as $decor){
                $array = [
                    'id'        => $decor->id,
                    'name'      => $decor->name,
                    'thumbnail' => $decor->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder250x200.png'),
                    'company'   => $decor->company->name,
                    'quantity'  => 1,
                    'hearted'   => $request->user ? $this->hearts()->whereUserId($request->user()->id)->first() ? 1 : 0 : 0
                ];
                array_push($data, $array);
            }

            return $data;
        }
        return view('site.user.qoute.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'event_name' => 'required',
            'event_type' => 'required',
            'location' => 'required',
            'event_date' => 'required',
            'time_start' => 'required',
            'time_end' => 'required',
            'number_of_guests' => 'required',
        ]);

        $quote = new Qoute;
        $quote->reference_number    = time() . rand(10*45, 100*98);
        $quote->user_id             = $request->user()->id;
        $quote->event_name          = $request->event_name;
        $quote->event_type          = $request->event_type;
        $quote->location            = $request->location;
        $quote->event_date          = $request->event_date;
        $quote->time_start          = $request->time_start;
        $quote->time_end            = $request->time_end;
        $quote->number_of_guests    = $request->number_of_guests;

        $quote->save();

        $counter = 0;
        foreach($request->decors as $decor){
            $quoteproduct = new QouteProduct;
            $quoteproduct->product_id = $decor;
            $quoteproduct->qoute_id = $quote->id;
            $quoteproduct->quantity = $request->quantity[$counter];
            $quoteproduct->save();
            $counter++;
        }

        $companies = $quote->suppliers();
        foreach($companies as $company){
            $company->users->map(function($q) use ($quote){
                    $q->mobileNotify(
                        [
                        'title'         => 'New Qoute',  // title,
                        'avatar'        => $quote->user->getProfilePicture(),
                        'message'       => $quote->user->name . ' had new qoute entitled <b>' . $quote->event_name . '</b>',  // message,
                        'notif_type'    => 'new_qoute',
                        'item_id'       => $quote->id,
                        'link'          => route('dashboard.quote.show', $quote->reference_number),
                        'image'         => '',  // image
                        'read_at'       => '',
                        'timestamp'     => Carbon::now()->format("Y-m-d H:i:s") // timestamp
                    ]);
            });
            Notification::send($company->users, new NewQouteNotification($quote));
        }


        if($request->ajax()){
            return [
                'status' => 200,
                'reference_id' => $quote->reference_number,
                'message' => 'saving success'
            ];
        }else{
            return redirect()->route('user.qoute.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $qoute = Qoute::whereReferenceNumber($id)->firstOrFail();

        $supplierIds = Array();
        $decorlist  = array();


        foreach($qoute->decors as $decor){
            if($decor->product){
                $company_id = $decor->product->company_id;
                if(!in_array($company_id, $supplierIds)){
                    array_push($supplierIds, $company_id);
                }
            }
        }

        // for($i = 0; $supplierIds > $i; $i++){
        foreach($supplierIds as $supplierId){
            $array = array(
            'supplier' => Company::find($supplierId),
            'decor' => $qoute->decors()->whereHas('product', function($q) use ($supplierId){
                            $q->whereCompanyId($supplierId);
                        })->get(),
                        //->map(function($q){
                        //     return [
                        //         'id'        => $q->id,
                        //         'product'   => $q->product->name,
                        //         ' thumbnail' => $q->product->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),
                        //         'quantity'  => $q->quantity ?? 0,
                        //         'price'     => 1
                        //     ];
                        // })
        );

            array_push($decorlist, $array);
        }

        $pricingcomponents = PricingComponent::whereActive(1)->get();

        return view('site.user.qoute.show', compact('qoute', 'decorlist', 'pricingcomponents'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $quote = Qoute::find($id);
        $qoute->delete();

        if(!request()->ajax()){
            return back();
        }
    }
}
