<?php

namespace App\Http\Controllers\Site;

use File;
use Mail;
use Storage;
use ZipArchive;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Jobs\Request3dZipperAndMailer;
use App\Mail\Site\Request3d\AutoReplyMail;

class Request3dController extends Controller
{
    private $html;
    private $recipient = ['nick.fairall@designa.studio', 'michael@designa.studio', 'hello@designa.studio'];

    function index(Request $request){
        return view('site.request3d.index');
    }


    function submit(Request $request){
        
        if($request->type == "event-space"){
            $this->sendEventSpace($request);
        }elseif($request->type == "candleware"){
            $this->sendCandleware($request);
        }elseif($request->type == "glassware"){
            $this->sendGlassware($request);
        }elseif($request->type == "chandelier"){
            $this->sendChandelier($request);
        }elseif($request->type == "tableware"){
            $this->sendTableware($request);
        }elseif($request->type == "furniture-chair"){
            $this->sendFurnitureChair($request);
        }elseif($request->type == "furniture-table-rectangular"){
            $this->sendFurnitureTableRectangular($request);
        }elseif($request->type == "furniture-table-rounded"){
            $this->sendFurnitureTableRectangular($request);
        }elseif($request->type == "quick-form"){
            $this->sendQuickForm($request);
        }
        
        Mail::to($request->email_address)->send(new AutoReplyMail());
        return back()->with('message', 'Request Sent!');
    }


    function sendQuickForm(Request $request){

        $folder = md5(uniqid(rand(), true));

        if($request->file('images')){
            foreach($request->file('images') as $image){
                $image->store('public/temp/' . $folder. '/images');
            }
        }

        $mailHtml = "
                    Company/Business Name: $request->company_name <br>
                    Email Address: $request->email_address <br>
                    Product Type: $request->product_type
                    <br>
                    <br>
                    Item Name : $request->name <br><br>";

        dispatch(new Request3dZipperAndMailer($folder, $mailHtml, $this->recipient, "Quick Form 3D Request"));
    }

    function sendCandleware(Request $request){
        $height_measurement_amount     = $request->height_measurement_amount;
        $height_measurement              = $request->height_measurement;
        $diameter_measurement_amount   = $request->diameter_measurement_amount;
        $diameter_measurement            = $request->diameter_measurement;

        $folder = md5(uniqid(rand(), true));

        if($request->hasFile('front_view_photo')){
            $front_view_photo = $request->front_view_photo->store('public/temp/' . $folder . '/front_view_photo');
        }

        if($request->hasFile('side_view_photo')){
            $side_view_photo = $request->side_view_photo->store('public/temp/' . $folder . '/side_view_photo');
        }

        $mailHtml = "
                Company/Business Name: $request->company_name <br>
                Email Address: $request->email_address
                <br>
                <br>
                Item Name : $request->name <br><br>
                Height : $height_measurement_amount $height_measurement
                <br>
                Diameter: $diameter_measurement_amount $diameter_measurement <br>";

        dispatch(new Request3dZipperAndMailer($folder, $mailHtml, $this->recipient, "Request for Candleware Model"));

    }

    function sendGlassware(Request $request){
        $height_measurement_amount     = $request->height_measurement_amount;
        $height_measurement              = $request->height_measurement;
        $diameter_measurement_amount   = $request->diameter_measurement_amount;
        $diameter_measurement            = $request->diameter_measurement;

        $folder = md5(uniqid(rand(), true));

        if($request->hasFile('front_view_photo')){
            $front_view_photo = $request->front_view_photo->store('public/temp/' . $folder . '/front_view_photo');
        }

        $mailHtml = "
            Company/Business Name: $request->company_name <br>
            Email Address: $request->email_address
            <br>
            <br>
            Item Name : $request->name <br><br>
            Height : $height_measurement_amount $height_measurement
            <br>
            Diameter: $diameter_measurement_amount $diameter_measurement
            <br>";

        dispatch(new Request3dZipperAndMailer($folder, $mailHtml, $this->recipient, "Request for Glassware Model"));

    }



    function sendChandelier(Request $request){
        $height_measurement_amount     = $request->height_measurement_amount;
        $height_measurement              = $request->height_measurement;
        $diameter_measurement_amount   = $request->diameter_measurement_amount;
        $diameter_measurement            = $request->diameter_measurement;

        $folder = md5(uniqid(rand(), true));

        if($request->hasFile('front_view_photo')){
            $front_view_photo = $request->front_view_photo->store('public/temp/' . $folder . '/front_view_photo');
        }

        $mailHtml = "
                Company/Business Name: $request->company_name <br>
                Email Address: $request->email_address
                <br>
                <br>
                Item Name : $request->name <br><br>
                Height : $height_measurement_amount $height_measurement
                <br>
                Diameter: $diameter_measurement_amount $diameter_measurement <br>";

        dispatch(new Request3dZipperAndMailer($folder, $mailHtml, $this->recipient, "Request for Chandelier Model"));  
    }


    function sendTableware(Request $request){
        $height_measurement_amount     = $request->height_measurement_amount;
        $height_measurement              = $request->height_measurement;
        $diameter_measurement_amount   = $request->diameter_measurement_amount;
        $diameter_measurement            = $request->diameter_measurement;

        $folder = md5(uniqid(rand(), true));

        if($request->hasFile('front_view_photo')){
            $front_view_photo = $request->front_view_photo->store('public/temp/' . $folder . '/front_view_photo');
        }

        $mailHtml = "
                Company/Business Name: $request->company_name <br>
                Email Address: $request->email_address
                <br>
                <br>
                Item Name : $request->name <br><br>
                Height : $height_measurement_amount $height_measurement
                <br>
                Diameter: $diameter_measurement_amount $diameter_measurement <br>";

        dispatch(new Request3dZipperAndMailer($folder, $mailHtml, $this->recipient, "Request for Chandelier Model"));
    }

    function sendFurnitureChair(Request $request){
        $top_rail_height_amount     = $request->top_rail_height_amount;
        $top_rail_height     = $request->top_rail_height;

        $height_measurement_amount     = $request->height_measurement_amount;
        $height_measurement     = $request->height_measurement;

        $width_measurement_amount     = $request->width_measurement_amount;
        $width_measurement     = $request->width_measurement;

        $depth_measurement_amount     = $request->depth_measurement_amount;
        $depth_measurement     = $request->depth_measurement;

        $chair_leg_height_measurement_amount     = $request->chair_leg_height_measurement_amount;
        $chair_leg_height_measurement     = $request->chair_leg_height_measurement;

        $folder = md5(uniqid(rand(), true));

        if($request->hasFile('front_view_photo')){
            $front_view_photo = $request->front_view_photo->store('public/temp/' . $folder . '/front_view_photo');
        }

        if($request->hasFile('side_view_photo')){
            $side_view_photo = $request->side_view_photo->store('public/temp/' . $folder . '/side_view_photo');
        }

        if($request->hasFile('back_photo')){
            $back_photo = $request->back_photo->store('public/temp/' . $folder . '/back_photo');
        }

        if($request->hasFile('video')){
            $video = $request->video->store('public/temp/' . $folder . '/video');
        }

        $mailHtml = "
            Company/Business Name: $request->company_name <br>
            Email Address: $request->email_address
            <br>    
            <br>
            Item Name : $request->name <br><br>
            
            Top rail's height: $top_rail_height_amount $top_rail_height <br>

            Seat/Padding dimensions <br>
                - Height: $height_measurement_amount $height_measurement <br>
                - Width: $width_measurement_amount  $width_measurement  <br>
                - Depth: $depth_measurement_amount $$depth_measurement 
                - Chairs Legs height : $chair_leg_height_measurement_amount $chair_leg_height_measurement <br>";

        dispatch(new Request3dZipperAndMailer($folder, $mailHtml, $this->recipient, "Request for Furniture Chair Model"));
        
    }

    function sendFurnitureTableRectangular(Request $request){
     
        $height_measurement_amount     = $request->height_measurement_amount;
        $height_measurement     = $request->height_measurement;

        $width_measurement_amount     = $request->width_measurement_amount;
        $width_measurement     = $request->width_measurement;

        $length_measurement_amount     = $request->length_measurement_amount;
        $length_measurement     = $request->length_measurement;

    
        $folder = md5(uniqid(rand(), true));

        if($request->hasFile('top_image')){
            $top_image = $request->top_image->store('public/temp/' . $folder . '/top_image');
        }

        if($request->hasFile('side_image')){
            $side_image = $request->side_image->store('public/temp/' . $folder . '/side_image');
        }

        $html = "
            Company/Business Name: $request->company_name <br>
            Email Address: $request->email_address
            <br>
            <br>
            Item Name : $request->name <br><br>

            Dimensions <br>
                - Height: $height_measurement_amount $height_measurement <br>
                - Width: $width_measurement_amount  $width_measurement  <br>
                - Lenght: $length_measurement_amount $length_measurement 
            <br>";

        dispatch(new Request3dZipperAndMailer($folder, $html, $this->recipient, "Request for Furniture Chair Model"));
        
    }

    function sendFurnitureTableRouned(Request $request){
     
        $height_measurement_amount     = $request->height_measurement_amount;
        $height_measurement     = $request->height_measurement;

        $width_measurement_amount     = $request->width_measurement_amount;
        $width_measurement     = $request->width_measurement;

        $length_measurement_amount     = $request->length_measurement_amount;
        $length_measurement     = $request->length_measurement;

    
        $folder = md5(uniqid(rand(), true));

        if($request->hasFile('top_image')){
            $top_image = $request->top_image->store('public/temp/' . $folder . '/top_image');
        }

        if($request->hasFile('side_image')){
            $side_image = $request->side_image->store('public/temp/' . $folder . '/side_image');
        }

        $html = "
            Company/Business Name: $request->company_name <br>
            Email Address: $request->email_address
            <br>
            <br>
            Item Name : $request->name <br><br>

            Dimensions <br>
                - Height: $height_measurement_amount $height_measurement <br>
                - Width: $width_measurement_amount  $width_measurement  <br>
                - Lenght: $length_measurement_amount $length_measurement <br>";

        dispatch(new Request3dZipperAndMailer($folder, $html, $this->recipient, "Request for Table Rounded Model"));
        

    }
    
    function sendEventSpace(Request $request){
        $folder = md5(uniqid(rand(), true));

        if($request->hasFile('floor_plan')){
            $floor_plan = $request->floor_plan->store('temp/' . $folder . '/floor_plan');
        }

        if($request->file('side_photos')){
            foreach($request->file('side_photos') as $sidePhoto){
                $sidePhoto->store('public/temp/' . $folder . '/side_photos');
            }
        }

        if($request->file('cieling_angle_photos')){
            foreach($request->file('cieling_angle_photos') as $cielingPhoto){
                $cielingPhoto->store('public/temp/' . $folder. '/cieling_angle_photos');
            }
        }
        
        if($request->hasFile('floor_photo')){
            $floorPhoto = $request->file("floor_photo")->store('public/temp/' . $folder  . '/floor_photo');
        }

        if($request->hasFile('cieling_photo')){
            $floorPhoto = $request->file("cieling_photo")->store('public/temp/' . $folder . '/cieling_photo');
        }

        if($request->hasFile('video')){
            $floorPhoto = $request->file("video")->store('public/temp/' . $folder . '/video');
        }
        
      

        $mailHtml = "
        Company/Business Name: $request->company_name <br>
        Email Address: $request->email_address
        <br>
        <br>
        Item Name : $request->name <br><br>";

        dispatch(new Request3dZipperAndMailer($folder, $mailHtml, $this->recipient, "Request for Event Space Model", true));
        

    }


    
}
