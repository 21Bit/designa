<?php

namespace App\Http\Controllers\Site;

use DB;
use Cache;
use Carbon\Carbon;
use App\Models\Role;
use App\Models\Heart;
use App\Models\Company;
use App\Models\Inspiration;
use App\Facades\FirebaseFMC;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Company\Web\Company as CompanyResource;
use App\Http\Resources\Inspiration\Web\Inspiration as InspirationResource;

class SupplierController extends Controller
{
    public function index(Request $request){
        $defaultCategory = $request->cat ? Role::whereName($request->cat)->firstOrFail()->id : '';
        return view('site.supplier.index', compact('defaultCategory'));
    }

    public function show(Request $request, $slug){
        $supplier = Company::whereSlug($slug)->firstOrFail();
        $supplier->viewed($request);
        return view('site.supplier.show', compact('supplier'));
    }

    function list(Request $request){
        // return Cache::remember('supplier-' . json_encode($request->all()), Carbon::now()->addMinutes(5), function() use($request){

            $list = Company::query();
            
            if($request->latitude){
                $list->addSelect(
                        DB::raw('*, ( 6367 * acos( cos( radians(' . $request->latitude . ') ) 
                        * cos( radians( latitude ) ) 
                        * cos( radians( longitude ) - radians('. $request->longitude . ') ) 
                        + sin( radians('. $request->latitude .') ) 
                        * sin( radians( latitude ) ) ) ) AS address_distance')
                    )
                    ->whereNotNull('latitude')
                    ->whereNotNull('longitude')
                    ->orderBy('address_distance');

                if($request->distance != null){
                    $list->having('address_distance', '<=', $request->distance);
                }
            }

        
            if($request->roles){
                $list->whereHas('roles', function($q) use ($request) {
                    $q->whereIn('id', $request->roles);
                });
            }

            if($request->caters){
                $list->whereHas('categories',function($q) use ($request){
                    $q->whereIn('id', $request->caters)->where('type', 'cater');
                });
            }

            if($request->productTypes){
                $list->whereHas('products', function($q) use ($request){
                    $q->whereHas('categories', function($q) use ($request) {
                        $q->whereIn('category_id', $request->productTypes)->where('type', 'product_type');
                    });
                });
            }
            if($request->productStyles){
                $list->whereHas('products', function($q) use ($request){
                    $q->whereHas('categories', function($q) use ($request) {
                        $q->whereIn('category_id', $request->productStyles)->where('type', 'style');
                    });
                });
            }

            if($request->search){
                $list = $list->where('name', 'LIKE', '%'. $request->search . '%');
            }

            $companies = $list->withCount(['hearts', 'products', 'shares'])->where('is_active', 1)->paginate(12);
            return CompanyResource::collection($companies);
        // });
    }

    function hearted(Request $request){
        if(!$request->user()){
            return response()->json([
                'message' => 'User not authentecated',
                'status'    => '401'
            ], 200);
        }

        $company = Company::whereId($request->id)->first();
        if(!$company){
            return [
                'status' =>  404,
                'message' => 'No company Found'
            ];
        }

        $hearted = $company->hearts()->whereUserId($request->user()->id)->first();
        if($hearted){
            $heartedStatus = 0;
            $company->hearts()->whereUserId($request->user()->id)->first()->delete();
        }else{
            $heart = new Heart;
            $heart->user_id = $request->user()->id;
            $heart->origin = "web";
            $company->hearts()->save($heart);
            $heartedStatus = 1;
        }

        return [
            'status' => 200,
            'hearted' => $heartedStatus
        ];
    }


    function taggedInspiration(Request $request){
        $company  = Company::find($request->supplier);
        $taggedInspirations = Inspiration::whereHas('supplierRoles', function($q) use ($company){
            $q->whereCompanyId($company->id)->whereStatus('confirmed');
        })->paginate();

        return InspirationResource::collection($taggedInspirations);
    }
}

