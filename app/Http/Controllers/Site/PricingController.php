<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Models\SubscriptionPlan;
use App\Http\Controllers\Controller;

class PricingController extends Controller
{
    function index(){
        $plans = SubscriptionPlan::orderBy('price')->get();;
        return view('site.pricing.index', compact('plans'));
    }
}
