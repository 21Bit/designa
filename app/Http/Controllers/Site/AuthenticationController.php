<?php

namespace App\Http\Controllers\Site;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthenticationController extends Controller
{
    function login(Request $request){
        $email = $request->email;
        $password = $request->password;
        $rememberToken = $request->remember;

        if(Auth::attempt(['email' => $email, 'password' => $password], $rememberToken)){
            $message = [
                'status' => 'success',
                'message' => 'Login Success'
            ];
            return response()->json($message);
        }else{
            $message = [
                'status' => 'error',
                'message' => ''
            ];
            return response()->json($message);
        }
    }
}
