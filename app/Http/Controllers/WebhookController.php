<?php

namespace App\Http\Controllers;

use App\Traits\StripeWebhooks\HasChargeWebhook;
use App\Traits\StripeWebhooks\HasSubscriptionWebhook;
use Laravel\Cashier\Http\Controllers\WebhookController as CashierController;

class WebhookController extends CashierController
{
    use HasSubscriptionWebhook, HasChargeWebhook;
     /**
     * Handle invoice payment succeeded.
     *
     * @param  array  $payload
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handleInvoicePaymentSucceeded($payload)
    {
        // \Log::emergency($payload);
    }


    public function handleInvoiceCreated($payload){
        \Log::info($payload);
    }

    // public function handelChargeFailed($payload){
    //     \Log::info('charge failed');
    //     \Log::info($payload);
    // }
}
