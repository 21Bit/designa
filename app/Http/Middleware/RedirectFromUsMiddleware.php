<?php

namespace App\Http\Middleware;

use Cache;
use Closure;
use Illuminate\Http\Request;
use Stevebauman\Location\Facades\Location;

class RedirectFromUsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $ip = $request->ip();

        $location = Cache::rememberForever($ip, function() use ($ip){
            return Location::get($ip);
        });
        
        if($location){
            if($location->countryCode == "US"){
                return redirect('venue-marketing-usa-preregister');
            }
        }

        return $next($request);
    }
}
