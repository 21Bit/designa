<?php

namespace App\Http\Middleware;

use Closure;

class MarkNotificationAsRead
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->has('notif')) {
            if($request->user()->notifications()->whereId($request->notif)->first()){
                $request->user()->notifications()->whereId($request->notif)->first()->markAsRead();
            }
            // foreach($notifications as $notification) {
            //     $notification->markAsRead();
            // }
        }
        return $next($request);
    }
    
}
