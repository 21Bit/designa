<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class SupplierMiddleware
{
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }


    public function handle($request, Closure $next)
    {
        if ($this->auth->guest()) {
            if($request->ajax()) {
                return response('Unauthorize.', 401);
            }else {
                return redirect()->guest('/');
            }
        }else{
            if($this->auth->user()->type == "supplier" || $this->auth->user()->type == "administrator"){
                return $next($request);
            }else{
                //die('Not Accreditor');
                if($this->auth->user()->type == "supplier") {
                    return redirect()->route('dashboard.home');
                }else{
                    return redirect()->guest('/');
                }   
            }
        }
    }
}
