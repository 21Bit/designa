<?php

namespace App\Http\Middleware;

use Closure;
use Jenssegers\Agent\Agent;
use Illuminate\Support\Facades\Route;

class CheckMobileMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $agent = new Agent;
        if($agent->isMobile()){
            // request for activation 
            // if(Route::current()->getName() != 'verification.verify'  || Route::current()->getName() != 'site.for-supplier'){
            if(!Route::is([
                    'verification.verify', 
                    'site.for-supplier', 
                    'site.register.supplier', 
                    'site.register.supplier.store', 
                    'site.register.customer', 
                    'site.register.customer.store',
                    'site.register.supplier.success',
                    'site.supplier.assistance.form',
                    'site.inspiration.tags.accepted',
                    'site.eventRegister',
                    'site.eventRegisterPost',
                    'site.request-3d-form.submit',
                    'site.request-3d-form',
                    'site.blog.index',
                    'site.blog.show',
                    'site.email.verified'
                    ])){
                return redirect('/landing');
            }
        }
        return $next($request);
    }
}
