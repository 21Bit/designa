<?php 
namespace App\Traits\StripeWebhooks;

trait HasSubscriptionWebhook{
    
    public function handleCustomerSubscriptionCreated($payload){
        \Log::info($payload);
    }

    public function handleCustomerSubscriptionUpdated($payload){
        \Log::info('Subs updated');
        \Log::info($payload);
    }
}