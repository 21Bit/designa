<?php 
namespace App\Traits\StripeWebhooks;

trait HasChargeWebhook{
    
    public function handleChargeSucceeded($payload){
        \Log::info($payload);
    }

    public function handleChargeFailed($payload){
        \Log::info('charge failed');
        \Log::info($payload);
    }
}