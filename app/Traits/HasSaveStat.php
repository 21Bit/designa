<?php
namespace App\Traits;

use Image;
use Illuminate\Http\Request;

trait HasSaveStat
{
    function monthlySavedStats($month, $year){
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);  
        $dateData = array();
        for($i=1; $numberOrDays + 1 > $i; $i++){
            $array = [
                'date'      => $i,
                'web'       => $this->dailySaved($year . "-" . $month . '-'. $i, 'web'),    
                'mobile'    => $this->dailySaved($year . "-" . $month . '-'. $i, 'mobile'),    
            ];

            array_push($dateData, $array);
        }

        return $dateData;

    }

    // function statTest(Request $request){
    //     $company = Company::find($request->user()->company_id);

    //     $month = $request->month ? $request->month : date('m');
    //     $year = $request->year ? $request->year : date('Y');

    //     return [
    //         'month' => $month,
    //         'year'  => $year,
    //         'saved' => random_int(1,50),
    //         'shares' => random_int(1,55),
    //         'post'   => random_int(1,50),
    //         'views'  => random_int(1,50),

    //         'stats' => [
    //             'saved'     => $this->testDataDay($month, $year),
    //             'shares'    => $this->testDataDay($month, $year),
    //             'views'     => $this->testDataDay($month, $year),
    //             'post'      => $this->testDataDay($month, $year)
    //         ]
    //     ];
    // }

    private function dailySaved($date, $origin){
        return $this->hearts()->whereDate($date)->whereOrigin($origin)->count();
    }

    private function monthOverall($month, $origin = ""){
        if($origin){
            $this->hearts()->whereMonth($month)->whereOrigin($origin)->count();
        }else{
            $this->hearts()->whereMonth($month)->count();
        }
    }


}