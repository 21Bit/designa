<?php
namespace App\Traits;

use App\Models\AreaLocation;


/**
 * Developed by: Jofie Bernas
 *
 */
trait HasAreaLocation
{
    public function arealocationable(){
        return $this->morphMany(AreaLocation::class, 'arealocationable');
    }
}