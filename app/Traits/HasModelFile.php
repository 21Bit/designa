<?php
namespace App\Traits;

use App\Models\ModelFile;
use App\Http\Resources\ModelFile\BaseLayerFile;
use App\Http\Resources\ModelFile\ModelDecoration;
use App\Http\Resources\ModelFile\ModelFileJSONFormat;

trait HasModelFile{

public function modelFiles(){
        return $this->morphMany(ModelFile::class, 'modellable');
    }

    public function scopeWithAssetBundle($query)
    {
        return $query->whereHas('modelFiles', function($q){
            return $q->whereType('asset_bundle_web');
        });
    }

    public function scopeOwnedBaseLayer($query)
    {
       return $q->whereType('base_layer')->where('modellable_type', 'App\\Models\\FunctionSpace')->whereHas('');
    }

    function getBaseLayerAttribute(){
        $first = $this->modelFiles()->withTrashed()->whereType('base_layer')->first();
        if($first){
            return $first;
        }
        return null;
    }

    function getFloorPlanAttribute(){
        $first = $this->modelFiles()->withTrashed()->whereType('floor_plan')->first();
        if($first){
            return $first;
        }
        return null;
    }

    function getAssetBundleWebAttribute(){
        $first = $this->modelFiles()->whereType('asset_bundle_web')->first();
        if($first){
            return $first;
        }
        return null;
    }

    function getAssetBundleAndroidAttribute(){
        $first = $this->modelFiles()->whereType('asset_bundle_android')->first();
        if($first){
            return $first;
        }
        return null;
    }

    function getAssetBundleIosAttribute(){
        $first = $this->modelFiles()->whereType('asset_bundle_ios')->first();
        if($first){
            return $first;
        }
        return null;
    }

    function getAssetBundleAttribute(){
        $first = $this->modelFiles()->whereType('asset_bundle')->first();
        if($first){
            return $first;
        }

        return null;
    }

    function getBaseLayerFileAttribute(){
        $first = $this->modelFiles()->withTrashed()->first();
        if($first){
            return new BaseLayerFile($first);
        }

        return null;
    }


    public function model_file_list($paginated = false, $pagination = 0){
        $query = $this->modelFiles;

        if($paginated){
            $query->paginate($pagination);
        }

        return ModelDecoration::collection($query);
    }

    public function saveModelFile($data){

        $modelfile = new ModelFile;
        $modelfile->model_name = $data['model_name'];
        $modelfile->path = $data['path'];
        $modelfile->is_public = $data['is_public'];

        if(isset($data['number_of_guest'])){
            $modelfile->number_of_guest = $data['number_of_guest'];
        }

        if(isset($data['decoration_template_id'])){
            $modelfile->decoration_template_id = $data['decoration_template_id'];
        }

        if(isset($data['product_list'])){
            $modelfile->product_list = json_decode($data['product_list']);
        }

        $modelfile->base_layer_id = $data['base_layer_id'] ?? '';

        if(isset($data['type'])){
            $modelfile->type = $data['type'];
        }

        $this->modelFiles()->save($modelfile);

        return $modelfile;
        return new ModelFileJSONFormat($modelfile);
    }

    public function updateModelFile($data){
        $modelfile = ModelFile::find($data['id']);

        $modelfile->is_public = $data['is_public'];

        // if model_name is exist
         if(isset($data['model_name'])) {
             $modelfile->model_name = $data['model_name'];
         }

        // if none base-layer file
        $modelfile->base_layer_id = $data['base_layer_id'] ?? '';

        if(isset($data['type'])){
            $modelfile->type = $data['type'];
        }

        if(isset($data['product_list'])){
            $modelfile->product_list = json_decode($data['product_list']);
        }

        if(isset($data['number_of_guest'])){
            $modelfile->number_of_guest = $data['number_of_guest'];
        }

        if(isset($data['decoration_template_id'])){
            $modelfile->decoration_template_id = $data['decoration_template_id'];
        }

        if(isset($data['path'])){
            $modelfile->path = $data['path'];
        }

        $modelfile->save();
        return new ModelFileJSONFormat($modelfile);
    }

    public function deleteModel($data){
        if(is_array($data)){
            $id = $data['id'];
        }else{
            $id = $data;
        }

        $modelfile = ModelFile::find($id);

        $model = public_path(str_replace('models', '3d-models', $modelfile->path));
        if(file_exists($model)){
            unlink($model);
        }

        $modelfile->delete();

        if(request()->ajax()){
            return response([
                'status' => 'success',
                'message' => 'Item Deleted'
            ], 200);
        }else{
            return back();
        }
    }
}
