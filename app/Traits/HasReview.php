<?php
namespace App\Traits;

use App\Models\Review;
use Illuminate\Http\Request;

trait HasReview
{
    public function reviews(){
        return $this->morphMany(Review::class, 'reviewable');
    }

    public function dailyReviews($month, $year){
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);  

        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $date = date('Y-m-d', strtotime($year .'-' . $month . '-'. $i));
            $array = [
                'date'      => $i,
                'web'       => $this->reviews()->whereDate('created_at', $date)->whereOrigin('web')->count(),
                'mobile'       => $this->reviews()->whereDate('created_at', $date)->whereOrigin('mobile')->count(),
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }

    public function getOverallRatingAttribute(){
        return $this->reviews()->avg('rating');
    }

    public function saveReview(Request $request, $type){
     
        $review = new Review;
        $review->user_id = $request->user()->id;
        $review->comment = $request->comment;
        $review->origin = $type;
        $review->rating = $request->rating;

        $this->reviews()->save($review);

        return $review;
    }

    
}
