<?php
namespace App\Traits;

use App\Models\ActivityLog;


trait HasActivityLog{
        
        function getDistanceAttribute($request){
                $this->select(
                        DB::raw("*, round(
                            (
                                (
                                    acos(
                                        sin(( $request->latitude * pi() / 180))
                                        *
                                        sin(( `latitude` * pi() / 180)) + cos(( $request->latitude * pi() /180 ))
                                        *
                                        cos(( `latitude` * pi() / 180)) * cos((( $request->longitude - `longitude`) * pi()/180)))
                                ) * 180/pi()
                            ) * 60 * 1.1515 * 1.609344
                        ) as distance"))
                    ->orderBy('distance');
        }


        function activities(){
            return $this->hasMany(ActivityLog::class); 
        }


}