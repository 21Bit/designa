<?php
namespace App\Traits;

use App\Models\Video;

trait HasVideoFunction{
    

    private $Video;
    private $uploadedVideo; 
    /**
     * Videos of a model.
     *
     * @return \App\Models\Models
     */
    public function videos(){
        return $this->morphMany(Video::class, 'videoable');
    }


     /**
     * Display a Video of the models based on type and count.
     *
     * @return \App\Models\Models
     */
   
    public function video($type){
        $this->video = $this->videos()->where('type', $type);
        return $this;
    }

    public function videoPath($folder,  $return = null){
        if(!$this->videos->first()){
            return $return;
        }

        $file = public_path('videos/' . $folder . '/' . $this->videos->first()->path);
        if(file_exists($file)){
            return '/videos/' . $folder . '/' . $this->videos->first()->path;
        }else{
            return $return;
        }
    }
    

    /**
     * Add Video to a model
     */
    public function addVideo($title, $type, $file, $folder, $exist = false){
        $video_video = $exist ? $file : $this->uploadVideo($file, $folder);
        $video = new Video;
        $video->title = $title;
        $video->path = $video_video;
        $video->type = $type;
        return $video;
    }

    public function uploadVideo($file, $folder){
        $fileExtension = $file->getClientOriginalExtension();
        $filename = time() . uniqid() . ".". $fileExtension;
        $file->move($folder, $filename);
        return $filename;
    }



     /**
     * Display a Videos of the models based on type and count.
     */
    public function clearVideo($type){
        $Videos = $this->videos()->where('type', $type)->get();
        foreach($Videos as $Video){
            $this->removeVideo($Video->path, $type);
            $Video->delete();
        }
    }


      /**
     * Remove Video based on its type
     */
    function removeVideo($Video, $type){
        $file = public_path('Videos/logos/' . $Video);

        if(file_exists($file)){
            //delete main file
            unlink($file);
        }
    }
}