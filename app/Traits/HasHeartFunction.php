<?php
namespace App\Traits;

use App\Models\Heart;
use Illuminate\Http\Client\Request;
use Illuminate\Support\Str;

trait HasHeartFunction{

    public function hearts(){
        return $this->morphMany(Heart::class, 'heartable');
    }

    public function scopeHasHeartCounts($query){
        $model = class_basename($this);
        return $query->addSelect(
            ['heart_counts' => Heart::select(\DB::raw('count(*)'))
                    ->whereColumn('heartable_id', Str::plural(strtolower($model)). '.id')
                    ->where('heartable_type', "App\\Models\\" . $model)
                ]);
    }

    public function hasHeartedOfLoggedUser(){
            if(!request()->user()){
                return 0;
            }

            return $this->hearts()->where('user_id', request()->user()->id)->count() ?? 0;
    }


    public function dailyHearts($month, $year){
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);

        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $date = date('Y-m-d', strtotime($year .'-' . $month . '-'. $i));
            $array = [
                'date'      => $i,
                'web'       => $this->hearts()->whereDate('created_at', $date)->whereOrigin('web')->count(),
                'mobile'    => $this->hearts()->whereDate('created_at', $date)->whereOrigin('mobile')->count(),
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }

    function hearted(Request $request){
        $supplier  = Company::find($this->company_id);
        $hearted = $this->hearts()->whereUserId($request->user()->id)->first();

        if($hearted){
            $heartedStatus = 0;
            $this->hearts()->whereUserId($request->user()->id)->first()->delete();
        }else{
            $heart = new Heart;
            $heart->user_id = $request->user()->id;
            $this->hearts()->save($heart);
            $heartedStatus = 1;

            // send notification
            $firebaseArray = array();
            foreach($supplier->users()->has('firebases')->get() as $user){
                foreach($user->firebases as $firebaseAccount){
                    array_push($firebaseArray, $firebaseAccount->firebase_id);
                }
            }

            $firebase = new FirebaseFMC;
            $firebase->sendSingle(
                $firebaseArray,
                array(
                    'title'         => 'Hearted Inspiration',  // title,
                    'avatar'        => '/dashboard/img/avatar04.png',
                    'message'       => $request->user()->name . ' has saved your inspiration entitled ' . $this->name,  // message,
                    'notif_type'    => $request->type,
                    'item_id'       => $this->id,
                    'link'          => route('site.supplier.show', $this->slug),
                    'image'         => $this->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),  // image
                    'timestamp'     => Carbon::now()->format('Y-m-d h:i:s') // timestamp
                )
            );
        }

        return [
            'status' => 200,
            'hearted' => $heartedStatus
        ];
    }

}
