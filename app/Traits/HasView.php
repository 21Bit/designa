<?php
namespace App\Traits;

use App\Models\View;
use Illuminate\Http\Request;

trait HasView
{
    public function views(){
        return $this->morphMany(View::class, 'viewable');
    }

    public function viewed(Request $request, $data = null, $origin = "web"){
        if($request->user()){
            
            $viewed = $this->views()->whereUserId($request->user()->id)->whereDate('created_at', date('Y-m-d'))->first();

            if(!$viewed){
                //create view
                $view = new View;
                $view->user_id = $request->user()->id;
                $view->data = $data;
                $view->origin = $origin;
        
                //actual saving of view
                $this->views()->save($view);
                $viewStatus = 1;
            }

            return [
                'status' => 200,
                'viewed' => "View Saved"
            ];
        }

    }

    public function dailyViews($month, $year){
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);  

        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $date = date('Y-m-d', strtotime($year .'-' . $month . '-'. $i));
            $array = [
                'date'      => $i,
                'web'       => $this->views()->whereDate('created_at', $date)->whereOrigin('web')->count(),
                'mobile'       => $this->views()->whereDate('created_at', $date)->whereOrigin('mobile')->count(),
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }

    // private function dailyViews($date, $origin){
    //     return $this->views()->whereDate($date)->whereOrigin($origin)->count();
    // }

    private function monthOverallHearth($month, $origin = ""){
        if($origin){
            $this->views()->whereMonth($month)->whereOrigin($origin)->count();
        }else{
            $this->views()->whereMonth($month)->count();
        }
    }
}