<?php
namespace App\Traits;

use App\Models\Category;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

/**
 * Developed by: Jofie Bernas
 *
 */
trait HasCategories
{
    public function categories()
    {
        return $this->morphToMany(Category::class, 'categorizable');
    }

//    public function caters(){
//        return $this->categories()->where('type', 'cater');
//    }

    public function caters() : MorphToMany
    {
        return $this->morphToMany(Category::class, 'categorizable')
            ->where('type', 'cater');
    }

    public function colors() : MorphToMany
    {
        return $this->morphToMany(Category::class, 'categorizable')
            ->where('type', 'color');
    }

    public function productType() : MorphToMany
    {
        return $this->morphToMany(Category::class, 'categorizable')
            ->where('type', 'product_type');
    }

    public function productStyle() : MorphToMany
    {
        return $this->morphToMany(Category::class, 'categorizable')
            ->where('type', 'style')
            ->where('module', 'product');
    }


//    public function colors(){
//        return $this->categories()->whereType('color');
//    }

    public function categoryType($type){
        return $this->categories()->where('type', $type);
    }



    public function categoryList($type, $separator = null, $lastSeperator = " and "){
        $categories = $this->categories()->where('type', $type)->get();
        $list = '';
        $counter = 1;
        foreach($categories as $category){
            $list .= $category->name;
            if(count($categories) > $counter){
                if(count($categories) == $counter+1){
                    $list .= $lastSeperator ? $lastSeperator : " and ";
                }else{
                    $list .= $separator ? $separator : ", ";
                }
            }

            $counter++;
        }

        return $list;
    }

    public function clearAttachCategory($type){
        if(is_array($type)){
            for($i=0; count($type) > $i; $i++){
                $this->categories()->where('type', $type)->detach();
            }
        }else{
            $this->categories()->where('type', $type)->detach();
        }
    }
}
