<?php
namespace App\Traits;

use App\Models\Attachment;

trait HasAttachment{

    public function attachments(){
        return $this->morphMany(Attachment::class, 'attachable');
    }


    public function saveAttachment($data){
     
        $attachment = new Attachment;
        $attachment->filename = $data['filename'];
        $attachment->path = $data['path'];

        if(isset($data['type'])){
            $attachment->type = $data['type'];
        }

        $this->attachments()->save($attachment);

        return $attachment;
    }
}