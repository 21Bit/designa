<?php
namespace App\Traits;

use App\Models\Image;
use Illuminate\Http\Request;
use App\Traits\HasImageIntervention;

trait HasImageFunction{

    use HasImageIntervention;

    private $image;
    private $uploadedImage;
    /**
     * Images of a model.
     *
     * @return \App\Models\Models
     */
    public function images(){
        return $this->morphMany(Image::class, 'imagable');
    }


     /**
     * Display a image of the models based on type and count.
     *
     * @return \App\Models\Models
     */

    public function image($type){
        $this->image = $this->images()->where('type', $type);
        return $this;
    }

    public function path($folder,  $return = null){
        if(!$this->image->first()){
            return $return;
        }

        $file = $folder . '/' . $this->image->first()->path;
        if(file_exists(public_path('images/'. $file))){
            return '/images/' . $folder . '/' . $this->image->first()->path;
        }else{
            return $return;
        }
    }


    /**
     * Add Image to a model
     */
    public function addImage($imagetype, $file, $folder, $exist = false, $is_base64 = false){
        if($is_base64){
            $folder = public_path('images/profiles');
            $image = $file;  // your base64 encoded
            list($type, $image) = explode(';', $image);
            list(, $image)      = explode(',', $image);
            $image_image = time().'.'.'png';
            file_put_contents($folder. '/' . $image_image, base64_decode($image));
        }else{
            $image_image = $exist ? $file : $this->upload($file, $folder);
        }

        $image = new Image;
        $image->path = $image_image;
        $image->type = $imagetype;
        return $image;
    }





     /**
     * Display a images of the models based on type and count.
     */
    public function clearImage($type){
        $images = $this->images()->where('type', $type)->get();
        foreach($images as $image){
            $this->removeImage($image->path, $type);
            $image->delete();
        }
    }

      /**
     * Remove image based on its type
     */
    function removeImage($image, $type){

        switch ($type) {
            case 'logo':
                $file = public_path('images/logos/' . $image);
                break;
            case 'thumbnail':
                $file = public_path('images/thumbnails/' . $image);
                break;
            case 'profile':
                $file = public_path('images/profiles/' . $image);
                break;
            case 'cover':
                $file = public_path('images/covers/' . $image);
                break;
            case 'floorplan':
                $file = public_path('images/floorplans/' . $image);
                break;
            case '32x32':
                $file = public_path('images/32x32/' . $image);
                break;
            default:
                $file = "";
        }

        if(file_exists($file)){
            //delete main file
            unlink($file);

            //delete 32x32
            // if($type != "32x32"){
                $xs = public_path('images/32x32/' . $image);
                if(file_exists($xs)){
                    unlink($xs);
                }
            // }
        }
    }
}
