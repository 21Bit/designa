<?php
namespace App\Traits;

use App\Models\Share;
use Illuminate\Http\Request;

trait HasShare
{
    public function shares(){
        return $this->morphMany(Share::class, 'sharable');
    }

    public function dailyShares($month, $year){
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);  

        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $date = date('Y-m-d', strtotime($year .'-' . $month . '-'. $i));
            $array = [
                'date'      => $i,
                'web'       => $this->shares()->whereDate('created_at', $date)->whereOrigin('web')->count(),
                'mobile'       => $this->shares()->whereDate('created_at', $date)->whereOrigin('mobile')->count(),
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }

    public function shared(Request $request, $data = null, $origin = "web"){
        if($request->user()){

            $viewed = $this->shares()->whereUserId($request->user()->id)->first();

            if(!$viewed){
                //create view
                $view = new Share;
                $view->user_id = $request->user()->id;
                $view->data = $data;
                $view->origin = $origin;
        
                //actual saving of view
                $this->shares()->save($view);
            }

            return [
                'status' => 200,
                'message' => "has been shared"
            ];
        }

    }

}