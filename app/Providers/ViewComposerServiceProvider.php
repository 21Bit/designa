<?php

namespace App\Providers;

use Cache;
use Carbon\Carbon;
use App\Models\Role;
use App\Models\Category;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
          View::composer('site.includes.menus.navbar', function ($view) {
             $caters =   Cache::remember('navbar.caters', Carbon::now()->addDays(7), function(){
                              return Category::whereType('cater')->with('styles')->get();
                         });
             $venue_types = Cache::remember('navbar.venue_types', Carbon::now()->addDays(7), function(){
                              return Category::whereType('venue_type')->get();
                         });
             $suppliers = Cache::remember('navbar.suppliers', Carbon::now()->addDays(7), function(){
                              return Role::orderBy('name', 'ASC')->get();
                         });
             $products = Cache::remember('navbar.cater', Carbon::now()->addDays(7), function(){
                              return Category::whereType('product_type')->with('styles')->get();
                         });
             
             $view->with('data', compact('caters', 'venue_types', 'suppliers', 'products'));
        });

        View::composer('site.includes.menus.footer-sitemap', function ($view) {
             $caters = Cache::remember('footer-sitemap.caters', Carbon::now()->addDays(7), function(){
                              return Category::whereType('cater')->get();
                         });
             $venue_types = Cache::remember('footer-sitemap.venue_types', Carbon::now()->addDays(7), function(){
                              return Category::whereType('venue_type')->get();
                         });
             $suppliers = Cache::remember('footer-sitemap.suppliers', Carbon::now()->addDays(7), function(){
                              return Role::orderBy('name', 'ASC')->get();
                         });
             $products = Cache::remember('footer-sitemap.products', Carbon::now()->addDays(7), function(){
                              return Category::whereType('product_type')->get();
                         });
             
             $view->with('data', compact('caters', 'venue_types', 'suppliers', 'products'));
        });


        View::composer('site.includes.category-selections.event-types', function ($view) {
             $caters =   Cache::remember('category-selections.caters', Carbon::now()->addDays(7), function(){
                              return Category::whereType('cater')->get();
                         });

             $view->with('data', $caters);
        });


        View::composer('site.includes.category-selections.suppliers', function ($view) {
             $suppliers = Cache::remember('category-selections.suppliers', Carbon::now()->addDays(7), function(){
                              return Role::orderBy('name', 'ASC')->get();
                         });
             $view->with('data', $suppliers);
        });

        View::composer('site.includes.category-selections.venue-types', function ($view) {
             $venue_types = Cache::remember('category-selections.venue_types', Carbon::now()->addDays(7), function(){
                              return Category::whereType('venue_type')->get();
                         });
             $view->with('data', $venue_types);
        });

        View::composer('site.includes.category-selections.catering-options', function ($view) {
             $data = Cache::remember('category-selections.catering-options', Carbon::now()->addDays(7), function(){
                         return Category::whereType('catering_option')->get();
                    });
             $view->with('data', $data);
        });

        View::composer('site.includes.category-selections.beverage-options', function ($view) {
             $data = Cache::remember('category-selections.beverage-options', Carbon::now()->addDays(7), function(){
                         return Category::whereType('beverage_option')->get();
                    });
             $view->with('data', $data);
        });

        View::composer('site.includes.category-selections.amenities', function ($view) {
             $data = Cache::remember('category-selections.amenities', Carbon::now()->addDays(7), function(){
                         return Category::whereType('amenity')->get();
                    });
             $view->with('data', $data);
        });

        View::composer('site.includes.category-selections.cost-per-head', function ($view) {
             $data = Cache::remember('category-selections.cost-per-head', Carbon::now()->addDays(7), function(){
                         return Category::whereType('average_cost_per_head')->get();
                    });
             $view->with('data', $data);
        });
    }
}
