<?php

namespace App\Providers;

use App\Facades\FirebaseFMC;
use Illuminate\Support\ServiceProvider;

class FirebaseFMCServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('firebasefmc',function(){
            return new FirebaseFMC();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
