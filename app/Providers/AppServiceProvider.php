<?php

namespace App\Providers;

use Arr;
use Auth;
use App\Models\Category;
use App\Models\FunctionSpace;
use App\Models\Heart;
use App\Models\Qoute;
use App\Models\Venue;
use App\Models\Review;
use App\Models\Product;
use App\Models\Project;
use App\Models\ModelFile;
use App\Models\Inspiration;
use App\Observers\CategoryObserver;
use App\Observers\HeartObserver;
use App\Observers\QouteObserver;
use App\Observers\VenueObserver;
use App\Observers\ReviewObserver;
use App\Observers\ProductObserver;
use App\Observers\ProjectObserver;
use Illuminate\Support\Collection;
use App\Models\SupplierInspiration;
use App\Observers\FunctionSpaceObserver;
use App\Observers\ModelFileObserver;
use Illuminate\Pagination\Paginator;
use App\Observers\InspirationObserver;
use Illuminate\Support\ServiceProvider;
use App\Observers\SupplierInspirationObserver;
use App\Services\SubscriptionService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if (!Collection::hasMacro('paginate')) {
            Collection::macro('paginate',
                function ($perPage = 25, $page = null, $options = []) {
                $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
                return (new LengthAwarePaginator(
                    $this->forPage($page, $perPage), $this->count(), $perPage, $page, $options))
                    ->withPath('');
            });
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // for pagination of bootstrap
        Paginator::useBootstrap();

        // Model::preventLazyLoading(! app()->isProduction());

        Heart::observe(HeartObserver::class);
        Review::observe(ReviewObserver::class);
        Inspiration::observe(InspirationObserver::class);
        SupplierInspiration::observe(SupplierInspirationObserver::class);
        Venue::observe(VenueObserver::class);
        Product::observe(ProductObserver::class);
        Qoute::observe(QouteObserver::class);
        ModelFile::observe(ModelFileObserver::class);
        Project::observe(ProjectObserver::class);
        Category::observe(CategoryObserver::class);
        FunctionSpace::observe(FunctionSpaceObserver::class);

        // Blade directives
        Blade::if('access', function ($expression) {
            if(Auth::user()->is_superadmin){
                return true;
            }
            return (new SubscriptionService)->userHasAccess(Auth::user(), $expression) ? true : false;
        });

    }
}
