<?php

namespace App\Observers;

use App\Models\Image;

class ImageObserver
{
    /**
     * Handle the image "created" event.
     *
     * @param  \App\Models\Image  $image
     * @return void
     */
    public function created(Image $image)
    {
        //
    }

    /**
     * Handle the image "updated" event.
     *
     * @param  \App\Models\Image  $image
     * @return void
     */
    public function updated(Image $image)
    {
        //
    }

    /**
     * Handle the image "deleted" event.
     *
     * @param  \App\Models\Image  $image
     * @return void
     */
    public function deleted(Image $image)
    {
        $path = $image->path;
        $type = $image->type;
        

        $table = $image->imagable->getTable();
        if($table == "inspirations"){
            $inspiration = $image->imagable;
            Cache::forget('inspiration.' . $inspiration->slug . '.gallery');
        }

        switch ($type) {
            case "logo":
                //for logo
                    $logo = public_path('/images/logos/' . $path);
                    if(file_exists($logo)){
                        unlink(public_path('/images/logos/' . $path));
                    }
                // for 32x32
                    $logo32x32 = public_path('/images/32x32/' . $path);
                    if(file_exists($logo32x32)){
                        unlink(public_path('/images/32x32/' . $path));
                    }
                break;
            case "cover":
                unlink(public_path('/images/covers/' . $path));
                break;
         
            default:
                //
        }
    }

    /**
     * Handle the image "restored" event.
     *
     * @param  \App\Models\Image  $image
     * @return void
     */
    public function restored(Image $image)
    {
        //
    }

    /**
     * Handle the image "force deleted" event.
     *
     * @param  \App\Models\Image  $image
     * @return void
     */
    public function forceDeleted(Image $image)
    {
        //
    }
}
