<?php

namespace App\Observers;

use App\Models\ModelFile;

class ModelFileObserver
{
    /**
     * Handle the model file "created" event.
     *
     * @param  \App\Models\ModelFile  $modelFile
     * @return void
     */
    public function created(ModelFile $modelFile)
    {
        //
    }

    /**
     * Handle the model file "updated" event.
     *
     * @param  \App\Models\ModelFile  $modelFile
     * @return void
     */
    public function updated(ModelFile $modelFile)
    {
        //
    }

    /**
     * Handle the model file "deleted" event.
     *
     * @param  \App\Models\ModelFile  $modelFile
     * @return void
     */
    public function deleted(ModelFile $modelFile)
    {
        $file = public_path('3d-' . $modelFile->path);
        if(file_exists($file)){
            unlink($file);
        }
    }

    /**
     * Handle the model file "restored" event.
     *
     * @param  \App\Models\ModelFile  $modelFile
     * @return void
     */
    public function restored(ModelFile $modelFile)
    {
        //
    }

    /**
     * Handle the model file "force deleted" event.
     *
     * @param  \App\Models\ModelFile  $modelFile
     * @return void
     */
    public function forceDeleted(ModelFile $modelFile)
    {
        //
    }
}
