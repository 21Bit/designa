<?php

namespace App\Observers;

use Auth;
use App\Models\FunctionSpace;
use App\Services\SubscriptionService;

class FunctionSpaceObserver
{
    /**
     * Handle the FunctionSpace "created" event.
     *
     * @param  \App\Models\FunctionSpace  $functionSpace
     * @return void
     */
    public function created(FunctionSpace $functionSpace)
    {
        // (new SubscriptionService)->subscribeToEventSpaceCharge(Auth::user());
    }

    /**
     * Handle the FunctionSpace "updated" event.
     *
     * @param  \App\Models\FunctionSpace  $functionSpace
     * @return void
     */
    public function updated(FunctionSpace $functionSpace)
    {
        //
    }

    /**
     * Handle the FunctionSpace "deleted" event.
     *
     * @param  \App\Models\FunctionSpace  $functionSpace
     * @return void
     */
    public function deleted(FunctionSpace $functionSpace)
    {
        //
    }

    /**
     * Handle the FunctionSpace "restored" event.
     *
     * @param  \App\Models\FunctionSpace  $functionSpace
     * @return void
     */
    public function restored(FunctionSpace $functionSpace)
    {
        //
    }

    /**
     * Handle the FunctionSpace "force deleted" event.
     *
     * @param  \App\Models\FunctionSpace  $functionSpace
     * @return void
     */
    public function forceDeleted(FunctionSpace $functionSpace)
    {
        //
    }
}
