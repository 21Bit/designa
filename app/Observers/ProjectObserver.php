<?php

namespace App\Observers;

use Auth;
use Cache;
use App\Models\Project;

class ProjectObserver
{
    /**
     * Handle the project "created" event.
     *
     * @param  \App\Models\Project  $project
     * @return void
     */
    public function created(Project $project)
    {
        $key = 'dashboard.event-board.list.' . Auth::user()->id;
        $keyApi = 'api.event-board.list.' . Auth::user()->id;
        Cache::forget($key);
        Cache::forget($keyApi);
    }

    /**
     * Handle the project "updated" event.
     *
     * @param  \App\Models\Project  $project
     * @return void
     */
    public function updated(Project $project)
    {
        //
    }

    /**
     * Handle the project "deleted" event.
     *
     * @param  \App\Models\Project  $project
     * @return void
     */
    public function deleted(Project $project)
    {
        // clearing cateogires
        $project->clearAttachCategory(['color', 'cater', 'style', 'setting']);

        //dettaching all the images
        $project->inspirationImages()->detach();

        // clearing all the cache 
        $project->clearCache();
    }

    /**
     * Handle the project "restored" event.
     *
     * @param  \App\Models\Project  $project
     * @return void
     */
    public function restored(Project $project)
    {
        //
    }

    /**
     * Handle the project "force deleted" event.
     *
     * @param  \App\Models\Project  $project
     * @return void
     */
    public function forceDeleted(Project $project)
    {
        //
    }
}
