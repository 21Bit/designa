<?php

namespace App\Observers;

use Notification;
use Carbon\Carbon;
use App\Models\Qoute;
use App\Notifications\Supplier\Qoute\NewQouteNotification;

class QouteObserver
{
    /**
     * Handle the qoute "created" event.
     *
     * @param  \App\App\Models\Qoute  $qoute
     * @return void
     */
    public function created(Qoute $qoute)
    {
        $companies = $qoute->suppliers();
        foreach($companies as $company){  
            $company->users->map(function($q) use ($qoute){
                $q->mobileNotify(
                 [
                    'title'         => 'New Qoute',  // title,
                    'avatar'        => $qoute->user->getProfilePicture(),
                    'message'       => $qoute->user->name . ' had new qoute entitled <b>' . $qoute->event_name . '</b>',  // message,
                    'notif_type'    => 'new_qoute',
                    'item_id'       => $qoute->id,
                    'link'          => route('dashboard.quote.show', $qoute->reference_number),
                    'image'         => '',  // image
                    'read_at'       => '',
                    'timestamp'     => Carbon::now()->format("Y-m-d H:i:s") // timestamp
                ]);
            });

            Notification::send($company->users, new NewQouteNotification($qoute));
        }
    }

    /**
     * Handle the qoute "updated" event.
     *
     * @param  \App\App\Models\Qoute  $qoute
     * @return void
     */
    public function updated(Qoute $qoute)
    {
        //
    }

    /**
     * Handle the qoute "deleted" event.
     *
     * @param  \App\App\Models\Qoute  $qoute
     * @return void
     */
    public function deleted(Qoute $qoute)
    {
        //
    }

    /**
     * Handle the qoute "restored" event.
     *
     * @param  \App\App\Models\Qoute  $qoute
     * @return void
     */
    public function restored(Qoute $qoute)
    {
        //
    }

    /**
     * Handle the qoute "force deleted" event.
     *
     * @param  \App\App\Models\Qoute  $qoute
     * @return void
     */
    public function forceDeleted(Qoute $qoute)
    {
        //
    }
}
