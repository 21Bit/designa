<?php

namespace App\Observers;

use Cache;
use Carbon\Carbon;
use App\Models\Company;
use App\Models\Inspiration;
use App\Facades\FirebaseFMC;
use App\Notifications\Supplier\NewItem\NewInspirationNotification;

class InspirationObserver
{
    /**
     * Handle the inspiration "created" event.
     *
     * @param  \App\Models\Inspiration  $inspiration
     * @return void
     */
    public function created(Inspiration $inspiration)
    {
        $company = Company::find($inspiration->company_id);

        if($inspiration->is_published){
            $hearts = $company->hearts;
            
            $firebaseArray = array();

            foreach($hearts as $heart){
                foreach($heart->user()->has('firebases')->get() as $fireuser){
                    foreach($fireuser->firebases as $firebaseAccount){
                        array_push($firebaseArray, $firebaseAccount->firebase_id);
                    }
                }

                if($heart->user){
                    if($heart->user->company_id != $company->id){
                        $heart->user->notify(new NewInspirationNotification($inspiration));
                    }
                }
                
            }

            $firebase = new FirebaseFMC;
            $firebase->sendSingle(
                    $firebaseArray, 
                    array(
                        'title'     => 'New Inspiration',//$inspiration->title,  // title
                        'avatar'   => $company->image('logo')->path('logos', '/images/placeholders/placeholder.png'), //$inspiration->description,  // message,
                        'message'   => $company->name . ' has created ' . $inspiration->name, //$inspiration->description,  // message,
                        'notif_type' => 'new_inspiration',
                        'item_id'   => $inspiration->id,
                        'link'      => '',//route('site.inspiration.show', $inspiration->slug),
                        'image'     => $inspiration->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),  // image
                        'timestamp' => Carbon::now()->format('Y-m-d h:i:s') // timestamp
                    )
                );
        }
 
    }

    /**
     * Handle the inspiration "updated" event.
     *
     * @param  \App\Models\Inspiration  $inspiration
     * @return void
     */
    public function updated(Inspiration $inspiration)
    {
            $slug = $inspiration->slug;
            Cache::forget('inspiration.' . $slug);
            Cache::forget('inspiration.' . $slug . '.colors');
            Cache::forget('inspiration.' . $slug . '.gallery');
            Cache::forget('inspiration.' . $slug . '.videos');
            Cache::forget('dashboard-inspiration-' . $inspiration->id);
    }

    /**
     * Handle the inspiration "deleted" event.
     *
     * @param  \App\Models\Inspiration  $inspiration
     * @return void
     */
    public function deleted(Inspiration $inspiration)
    {
        $slug = $inspiration->slug;
        Cache::forget('inspiration.' . $slug);
        Cache::forget('inspiration.' . $slug . '.colors');
        Cache::forget('inspiration.' . $slug . '.gallery');
        Cache::forget('inspiration.' . $slug . '.videos');
    }

    /**
     * Handle the inspiration "restored" event.
     *
     * @param  \App\Models\Inspiration  $inspiration
     * @return void
     */
    public function restored(Inspiration $inspiration)
    {
        //
    }

    /**
     * Handle the inspiration "force deleted" event.
     *
     * @param  \App\Models\Inspiration  $inspiration
     * @return void
     */
    public function forceDeleted(Inspiration $inspiration)
    {
        //
    }
}
