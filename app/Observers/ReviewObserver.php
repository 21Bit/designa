<?php

namespace App\Observers;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Venue;
use App\Models\Review;
use App\Models\Company;
use App\Models\Product;
use App\Models\Inspiration;
use App\Facades\FirebaseFMC;
use App\Notifications\Supplier\Review\ReviewedVenueNotification;
use App\Notifications\Supplier\Review\ReviewedProductNotification;
use App\Notifications\Supplier\Review\ReviewedSupplierNotification;
use App\Notifications\Supplier\Review\ReviewedInspirationNotification;

class ReviewObserver
{
    /**
     * Handle the review "created" event.
     *
     * @param  \App\Models\Review  $review
     * @return void
     */
    public function created(Review $review)
    {
        $type = $this->type($review->reviewable_type);

        if($type == "inspiration"){
            $this->sendInspiration($review);
        }else if($type == "company"){
            $this->sendCompany($review);
        }else if($type == "venue"){
            $this->sendVenue($review);
        }else if($type == "product"){
            $this->sendProduct($review);
        }
    }

    private function type($type){

        if($type == "App\Models\Inspiration"){
            return 'inspiration';
        }else if($type == "App\Models\Venue"){
            return 'venue';
        }else if($type == "App\Models\Product"){
            return 'product';
        }else if($type == "App\Models\Company"){
            return 'company';
        }

    }

    private function sendInspiration(Review $heart){
        $user           = User::find($heart->user_id);
        $inspiration    = Inspiration::find($heart->reviewable_id);
        $company        = Company::find($inspiration->company_id);
        
        $firebaseArray = array();
        foreach($company->users as $companyuser){
            $companyuser->notify(new ReviewedInspirationNotification($inspiration, $user));
            foreach($companyuser->firebases as $firebaseId){
               array_push($firebaseArray, $firebaseId->firebase_id);
            }
        }

        $firebase = new FirebaseFMC;
        $firebase->sendSingle(
            $firebaseArray,
            array(
                'title'         => 'Hearted Inspiration',  // title,
                'avatar'        => $user->getProfilePicture(),
                'message'       => $user->name . ' has saved your inspiration entitled ' . $inspiration->name,  // message,
                'notif_type'    => 'hearted_inspiration',
                'item_id'       => $inspiration->id,
                'link'          => route('site.inspiration.show', $inspiration->slug),
                'image'         => $inspiration->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),  // image
                'timestamp'     => Carbon::now()->format('Y-m-d h:i:s') // timestamp
            )
        );
    }


    private function sendVenue(Review $heart){
        $user           = User::find($heart->user_id);
        $venue          = Venue::find($heart->reviewable_id);
        $company        = Company::find($venue->company_id);

        $firebaseArray = array();
        foreach($company->users as $companyuser){
            $companyuser->notify(new ReviewedVenueNotification($venue, $user));
            foreach($companyuser->firebases as $firebaseId){
                array_push($firebaseArray, $firebaseId->firebase_id);
             }
        }

        $firebase = new FirebaseFMC;
        $firebase->sendSingle(
            $firebaseArray,
            array(
                'title'         => 'Hearted Venue',  // title,
                'avatar'        => $user->getProfilePicture(),
                'message'       => $user->name . ' has saved your inspiration entitled ' . $venue->name ,  // message,
                'notif_type'    => 'hearted_venue',
                'item_id'       => $venue->id,
                'link'          => route('site.venue.show', $venue->slug),
                'image'         => $venue->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),  // image
                'timestamp'     => Carbon::now()->format('Y-m-d h:i:s') // timestamp
            )
        );
    }

    private function sendProduct(Review $heart){
        $user           = User::find($heart->user_id);
        $product        = Product::find($heart->reviewable_id);
        $company        = Company::find($product->company_id);

        $firebaseArray = array();
        foreach($company->users as $companyuser){
            $companyuser->notify(new ReviewedProductNotification($product, $user));
            foreach($companyuser->firebases as $firebaseId){
                array_push($firebaseArray, $firebaseId->firebase_id);
             }
        }

        $firebase = new FirebaseFMC;
        $firebase->sendSingle(
            $firebaseArray,
            array(
                'title'         => 'Hearted Product',  // title,
                'avatar'        => $user->getProfilePicture(),
                'message'       => $user->name . ' has saved your inspiration entitled ' . $product->name,  // message,
                'notif_type'    => 'hearted_product',
                'item_id'       => $product->id,
                'link'          => '', //.route('site.product.index', $product->slug),
                'image'         => $product->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),  // image
                'timestamp'     => Carbon::now()->format('Y-m-d h:i:s') // timestamp
            )
        );
    }
    
    private function sendCompany(Review $heart){
        $user           = User::find($heart->user_id);
        $company        = Company::find($heart->reviewable_id);

        $firebaseArray = array();
        foreach($company->users as $companyuser){
            $companyuser->notify(new ReviewedSupplierNotification($company, $user));
            foreach($companyuser->firebases as $firebaseId){
                array_push($firebaseArray, $firebaseId->firebase_id);
             }
        }

        $firebase = new FirebaseFMC;
        $firebase->sendSingle(
            $firebaseArray,
            array(
                'title'         => 'Hearted Supplier',  // title,
                'avatar'        => $user->getProfilePicture(),
                'message'       => $user->name . ' has saved your inspiration entitled ' . $company->name,  // message,
                'notif_type'    => 'hearted_company',
                'item_id'       => $company->id,
                'link'          => route('site.supplier.show', $company->slug),
                'image'         => $company->image('logo')->path('logos', '/images/placeholders/placeholder.png'),  // image
                'timestamp'     => Carbon::now()->format('Y-m-d h:i:s') // timestamp
            )
        );
    }

    /**
     * Handle the review "updated" event.
     *
     * @param  \App\Models\Review  $review
     * @return void
     */
    public function updated(Review $review)
    {
        //
    }

    /**
     * Handle the review "deleted" event.
     *
     * @param  \App\Models\Review  $review
     * @return void
     */
    public function deleted(Review $review)
    {
        //
    }

    /**
     * Handle the review "restored" event.
     *
     * @param  \App\Models\Review  $review
     * @return void
     */
    public function restored(Review $review)
    {
        //
    }

    /**
     * Handle the review "force deleted" event.
     *
     * @param  \App\Models\Review  $review
     * @return void
     */
    public function forceDeleted(Review $review)
    {
        //
    }
}
