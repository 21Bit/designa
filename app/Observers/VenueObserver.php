<?php

namespace App\Observers;

use Carbon\Carbon;
use App\Models\Venue;
use App\Models\Company;
use App\Facades\FirebaseFMC;
use App\Notifications\All\NewVenueNotification;

class VenueObserver
{
    /**
     * Handle the venue "created" event.
     *
     * @param  \App\Models\Venue  $venue
     * @return void
     */
    public function created(Venue $venue)
    {
        if($venue->is_published){
            $company = Company::find($venue->company_id);
            $hearts = $company->hearts;
            
            $firebaseArray = array();

            foreach($hearts as $heart){
                foreach($heart->user()->has('firebases')->get() as $fireuser){
                    foreach($fireuser->firebases as $firebaseAccount){
                        array_push($firebaseArray, $firebaseAccount->firebase_id);
                    }
                }

                $heart->user->notify(new NewVenueNotification($venue));
            }

            $firebase = new FirebaseFMC;
            $firebase->sendSingle(
                    $firebaseArray, 
                    array(
                        'title'     => 'New Venue',//$inspiration->title,  // title
                        'avatar'   => $company->image('logo')->path('logos', '/images/placeholders/placeholder.png'), //$inspiration->description,  // message,
                        'message'   => $company->name . ' has created new Venue entitled ' . $venue->name , //$venue->description,  // message,
                        'notif_type' => 'new_venue',
                        'item_id'   => $venue->id,
                        'link'      => '',//route('site.inspiration.show', $venue->slug),
                        'image'     => $venue->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),  // image
                        'timestamp' => Carbon::now()->format('Y-m-d h:i:s') // timestamp
                    )
                );
        }
    }

    /**
     * Handle the venue "updated" event.
     *
     * @param  \App\Models\Venue  $venue
     * @return void
     */
    public function updated(Venue $venue)
    {
        //
    }

    /**
     * Handle the venue "deleted" event.
     *
     * @param  \App\Models\Venue  $venue
     * @return void
     */
    public function deleted(Venue $venue)
    {
        //
    }

    /**
     * Handle the venue "restored" event.
     *
     * @param  \App\Models\Venue  $venue
     * @return void
     */
    public function restored(Venue $venue)
    {
        //
    }

    /**
     * Handle the venue "force deleted" event.
     *
     * @param  \App\Models\Venue  $venue
     * @return void
     */
    public function forceDeleted(Venue $venue)
    {
        //
    }
}
