<?php

namespace App\Observers;

use Auth;
use Mail;
use Carbon\Carbon;
use App\Models\Role;
use App\Models\Company;
use App\Models\Inspiration;
use App\Facades\FirebaseFMC;
use Illuminate\Http\Request;
use App\Models\SupplierInspiration;
use App\Mail\Dashboard\TagInvitation;
use App\Mail\Dashboard\TagSupplierProduct;
use App\Notifications\Supplier\Tag\ReserveTagNotification;
use App\Notifications\Supplier\Tag\ReverseTagNotification;
use App\Notifications\Supplier\Tag\InspirationTagNotification;

class SupplierInspirationObserver
{
    /**
     * Handle the supplier inspiration "created" event.
     *
     * @param  \App\Models\SupplierInspiration  $supplierInspiration
     * @return void
     */
    public function created(SupplierInspiration $supplierInspiration)
    {
        if($supplierInspiration->company){
        
            $company = Company::find($supplierInspiration->company_id);
            $inspiration = Inspiration::find($supplierInspiration->inspiration_id);
            $inspirationCompany = $inspiration->company;
            $role = Role::find($supplierInspiration->role_id);
            $users = $inspirationCompany->users;
            $firebaseArray = array();

            // Determine if reverse
            if($inspiration->company_id != $supplierInspiration->request_by){
                foreach($users as $user){
                    $user->mobileNotify( [
                        'title'     => 'Request Inspiration Tagged',//$inspiration->title,  // title
                        'avatar'   =>  $company->image('logo')->path('logos', '/images/placeholders/placeholder.png'), //$inspiration->description,  // message,
                        'message'   => $company->name . ' requested for you to tagged them in ' . $inspiration->name . " as " . $role->display_name, //$inspiration->description,  // message,
                        'notif_type' => 'reverse_tagged_inspiration',
                        'item_id'   => $inspiration->id,
                        'link'      => '',//route('site.inspiration.show', $inspiration->slug),
                        'image'     => $inspiration->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),  // image
                        'timestamp' => Carbon::now()->format('Y-m-d h:i:s') // timestamp
                    ]);
                    $user->notify(new ReverseTagNotification($supplierInspiration, $inspiration, $company, $role));
                }

            }else{
                if($supplierInspiration->company_id != $inspiration->company_id){
               
                    foreach($users as $user){
                        $user->mobileNotify( [
                                'title'      => 'Inspiration Tagged',//$inspiration->title,  // title
                                'avatar'     =>  $company->image('logo')->path('logos', '/images/placeholders/placeholder.png'), //$inspiration->description,  // message,
                                'message'    => $company->name . ' has tagged you in ' . $inspiration->name . " as " . $role->display_name, //$inspiration->description,  // message,
                                'notif_type' => 'inspiration_tagged',
                                'item_id'    => $inspiration->id,
                                'link'       => '',//route('site.inspiration.show', $inspiration->slug),
                                'image'      => $inspiration->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),  // image
                                'timestamp'  => Carbon::now()->format('Y-m-d h:i:s') // timestamp
                            ]);
                            
                        if($user){
                            $user->notify(new InspirationTagNotification($supplierInspiration, $inspiration, $company, $role));
                        }
                        
                    }
                    if(request()->sendEmail){
                        Mail::to($supplierInspiration->company->email)
                            ->send(new TagSupplierProduct($supplierInspiration));
                    }
                }
            }
          
           
        }else{
            $companyInviter = Company::find($supplierInspiration->request_by);
            
            if(request()->sendEmail){
                // sending email tag inspiration invitation
                Mail::to($supplierInspiration->email)
                ->send(new TagInvitation($supplierInspiration, $companyInviter));
            }
        }

        // $supplierInspiration->delete();
    }

    /**
     * Handle the supplier inspiration "updated" event.
     *
     * @param  \App\Models\SupplierInspiration  $supplierInspiration
     * @return void
     */
    public function updated(SupplierInspiration $supplierInspiration)
    {
        //
    }

    /**
     * Handle the supplier inspiration "deleted" event.
     *
     * @param  \App\Models\SupplierInspiration  $supplierInspiration
     * @return void
     */
    public function deleted(SupplierInspiration $supplierInspiration)
    {
        //
    }

    /**
     * Handle the supplier inspiration "restored" event.
     *
     * @param  \App\Models\SupplierInspiration  $supplierInspiration
     * @return void
     */
    public function restored(SupplierInspiration $supplierInspiration)
    {
        //
    }

    /**
     * Handle the supplier inspiration "force deleted" event.
     *
     * @param  \App\Models\SupplierInspiration  $supplierInspiration
     * @return void
     */
    public function forceDeleted(SupplierInspiration $supplierInspiration)
    {
        //
    }
}
