<?php

namespace App\Observers;

use Cache;
use Carbon\Carbon;
use App\Models\Company;
use App\Models\Product;
use App\Facades\FirebaseFMC;
use App\Notifications\All\NewProductNotification;

class ProductObserver
{
    /**
     * Handle the product "created" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function created(Product $product)
    {
        if($product->is_published){

            $company = Company::find($product->company_id);
            $hearts = $company->hearts;
            
            $firebaseArray = array();

            foreach($hearts as $heart){
                
                foreach($heart->user()->has('firebases')->get() as $fireuser){
                    foreach($fireuser->firebases as $firebaseAccount){
                        array_push($firebaseArray, $firebaseAccount->firebase_id);
                    }
                }

                $heart->user->notify(new NewProductNotification($product));
                
            }

            $firebase = new FirebaseFMC;
            $firebase->sendSingle(
                    $firebaseArray, 
                    array(
                        'title'     => 'New Product',//$inspiration->title,  // title
                        'avatar'   => $company->image('logo')->path('logos', '/images/placeholders/placeholder.png'), //$inspiration->description,  // message,
                        'message'   => $company->name . ' has created new Product entitled ' . $product->name , //$product->description,  // message,
                        'notif_type' => 'new_product',
                        'item_id'   => $product->id,
                        'link'      => '',//route('site.inspiration.show', $product->slug),
                        'image'     => $product->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),  // image
                        'timestamp' => Carbon::now()->format('Y-m-d h:i:s') // timestamp
                    )
                );
        }
    }

    /**
     * Handle the product "updated" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function updated(Product $product)
    {
        Cache::forget('product.product_type.' . $product->id);
        Cache::forget('product.style.' . $product->id);
        Cache::forget('product.caters.' . $product->id);
        Cache::forget('product.thumbnail.' . $product->id);

        Cache::rememberForever('product.product_type.' . $product->id, function() use ($product){
            return $product->categories()->whereType('product_type')->first();
        });
    
        Cache::rememberForever('product.style.' . $product->id, function() use ($product){
            return $product->categories()->whereType('style')->first();
        });

        Cache::rememberForever('product.caters.' . $product->id, function() use ($product){
            return $product->categories()->whereType('cater')->get();
        });
    }

    /**
     * Handle the product "deleted" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function deleted(Product $product)
    {
        //
    }

    /**
     * Handle the product "restored" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function restored(Product $product)
    {
        //
    }

    /**
     * Handle the product "force deleted" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function forceDeleted(Product $product)
    {
        //
    }
}
