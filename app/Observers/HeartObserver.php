<?php

namespace App\Observers;

use Auth;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Heart;
use App\Models\Venue;
use App\Models\Company;
use App\Models\Product;
use App\Models\Inspiration;
use App\Facades\FirebaseFMC;
use App\Notifications\Supplier\Hearts\HeartedVenueNotification;
use App\Notifications\Supplier\Hearts\HeartedProductNotification;
use App\Notifications\Supplier\Hearts\HeartedSupplierNotification;
use App\Notifications\Supplier\Hearts\HeartedInspirationNotification;

class HeartObserver
{
    /**
     * Handle the heart "created" event.
     *
     * @param  \App\Models\Heart  $heart
     * @return void
     */
    public function created(Heart $heart)
    {
        $type = $this->type($heart->heartable_type);

        if($type == "inspiration"){
            $this->sendInspiration($heart);
        }else if($type == "venue"){
            $this->sendVenue($heart);
        }else if($type == "company"){
            $this->sendCompany($heart);
        }else if($type == "venue"){
            $this->sendVenue($heart);
        }else if($type == "product"){
            $this->sendProduct($heart);
        }

    }

    private function type($type){

        if($type == "App\Models\Inspiration"){
            return 'inspiration';
        }else if($type == "App\Models\Venue"){
            return 'venue';
        }else if($type == "App\Models\Product"){
            return 'product';
        }else if($type == "App\Models\Company"){
            return 'company';
        }

    }



    /*
     * 
     * For sending hearted inspirations
     * 
    */

    private function sendInspiration(Heart $heart){
        $user           = User::find($heart->user_id);
        $inspiration    = Inspiration::find($heart->heartable_id);
        $company        = Company::find($inspiration->company_id);
        
        $firebaseArray = array();
        foreach($company->users()->where('id', '!=', Auth::user()->id)->get() as $companyuser){
            $companyuser->notify(new HeartedInspirationNotification($inspiration, $user));
            foreach($companyuser->firebases as $firebaseId){
               array_push($firebaseArray, $firebaseId->firebase_id);
            }
        }

        $firebase = new FirebaseFMC;
        $firebase->sendSingle(
            $firebaseArray,
            array(
                'title'         => 'Hearted Inspiration',  // title,
                'avatar'        => $user->getProfilePicture(),
                'message'       => $user->name . ' has saved your inspiration entitled ' . $inspiration->name ,  // message,
                'notif_type'    => 'hearted_inspiration',
                'item_id'       => $inspiration->id,
                'link'          => route('site.inspiration.show', $inspiration->slug),
                'image'         => $inspiration->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),  // image
                'timestamp'     => Carbon::now()->format('Y-m-d h:i:s') // timestamp
            )
        );
    }


    private function sendVenue(Heart $heart){
        $user           = User::find($heart->user_id);
        $venue          = Venue::find($heart->heartable_id);
        $company        = Company::find($venue->company_id);

        $firebaseArray = array();
        foreach($company->users as $companyuser){
            $companyuser->notify(new HeartedVenueNotification($venue, $user));
            foreach($companyuser->firebases as $firebaseId){
                array_push($firebaseArray, $firebaseId->firebase_id);
             }
        }

        $firebase = new FirebaseFMC;
        $firebase->sendSingle(
            $firebaseArray,
            array(
                'title'         => 'Hearted Venue',  // title,
                'avatar'        => $user->getProfilePicture(),
                'message'       => $user->name . ' has saved your inspiration entitled ' . $venue->name,  // message,
                'notif_type'    => 'hearted_venue',
                'item_id'       => $venue->id,
                'link'          => route('site.venue.show', $venue->slug),
                'image'         => $venue->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),  // image
                'timestamp'     => Carbon::now()->format('Y-m-d h:i:s') // timestamp
            )
        );
    }

    private function sendProduct(Heart $heart){
        $user           = User::find($heart->user_id);
        $product        = Product::find($heart->heartable_id);
        $company        = Company::find($product->company_id);

        $firebaseArray = array();
        foreach($company->users as $companyuser){
            $companyuser->notify(new HeartedProductNotification($product, $user));
            foreach($companyuser->firebases as $firebaseId){
                array_push($firebaseArray, $firebaseId->firebase_id);
             }
        }

        $firebase = new FirebaseFMC;
        $firebase->sendSingle(
            $firebaseArray,
            array(
                'title'         => 'Hearted Product',  // title,
                'avatar'        => $user->getProfilePicture(),
                'message'       => $user->name . ' has saved your inspiration entitled ' . $product->name,  // message,
                'notif_type'    => 'hearted_product',
                'item_id'       => $product->id,
                'link'          => '', //route('site.product.show', $product->slug),
                'image'         => $product->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png'),  // image
                'timestamp'     => Carbon::now()->format('Y-m-d h:i:s') // timestamp
            )
        );
    }
    
    private function sendCompany(Heart $heart){
        $user           = User::find($heart->user_id);
        $company        = Company::find($heart->heartable_id);

        $firebaseArray = array();
        foreach($company->users as $companyuser){
            $companyuser->notify(new HeartedSupplierNotification($company, $user));
            foreach($companyuser->firebases as $firebaseId){
                array_push($firebaseArray, $firebaseId->firebase_id);
             }
        }

        $firebase = new FirebaseFMC;
        $firebase->sendSingle(
            $firebaseArray,
            array(
                'title'         => 'Hearted Supplier',  // title,
                'avatar'        => $user->getProfilePicture(),
                'message'       => $user->name . ' has saved your inspiration entitled' . $company->name,  // message,
                'notif_type'    => 'hearted_company',
                'item_id'       => $company->id,
                'link'          => route('site.supplier.show', $company->slug),
                'image'         => $company->image('logo')->path('logos', '/images/placeholders/placeholder.png'),  // image
                'timestamp'     => Carbon::now()->format('Y-m-d h:i:s') // timestamp
            )
        );
    }


    /**
     * Handle the heart "updated" event.
     *
     * @param  \App\Models\Heart  $heart
     * @return void
     */
    public function updated(Heart $heart)
    {
        //
    }

    /**
     * Handle the heart "deleted" event.
     *
     * @param  \App\Models\Heart  $heart
     * @return void
     */
    public function deleted(Heart $heart)
    {

    }

    /**
     * Handle the heart "restored" event.
     *
     * @param  \App\Models\Heart  $heart
     * @return void
     */
    public function restored(Heart $heart)
    {
        //
    }

    /**
     * Handle the heart "force deleted" event.
     *
     * @param  \App\Models\Heart  $heart
     * @return void
     */
    public function forceDeleted(Heart $heart)
    {
        //
    }
}
