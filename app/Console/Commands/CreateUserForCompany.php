<?php

namespace App\Console\Commands;

use DB;
use App\Models\Company;
use App\Models\User;
use Illuminate\Console\Command;

class CreateUserForCompany extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:create-company-user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for creating a user to company doesnt have';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $companyId = $this->ask("Put company ID: ");
        $company = Company::whereId($companyId)->first();
        if($company){
            $userExists = $company->users()->first();
            if(!$userExists){
                DB::transaction(function() use ($company){
                    if(!User::where('email', $company->email)->count()){
                        User::create([
                                'name' => $company->name,
                                'email' => $company->email,
                                'password' => bcrypt('password'),
                                'company_id' => $company->id,
                                'type' => 'supplier',
                                'email_verified_at' => now()
                            ]);
                        $company->asyncRoleToUsers();
                    }else{
                        $this->info(' Email '. $company->email .' is already existed!');
                    }
                });

            }else{
               $this->error('Company already have user!');
            }

        }else{
            $this->error('Company not found!');
        }

        $this->line('Thank you then. Good bye!');
    }
}
