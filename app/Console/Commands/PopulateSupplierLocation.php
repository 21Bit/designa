<?php

namespace App\Console\Commands;

use App\Jobs\PopulateSupplierLocationJob;
use App\Models\Company;
use App\Services\GoogleService;
use Illuminate\Console\Command;

class PopulateSupplierLocation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:populate-supplier-location';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $suppliers = Company::where('latitude', null)->get();

        foreach($suppliers as $supplier){
            PopulateSupplierLocationJob::dispatch($supplier);
        }
    }
}
