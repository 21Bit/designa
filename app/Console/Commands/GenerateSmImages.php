<?php

namespace App\Console\Commands;

use Storage;
use Illuminate\Console\Command;
use Illuminate\Support\LazyCollection;
use Intervention\Image\Exception\NotReadableException;
use Intervention\Image\Facades\Image;

class GenerateSmImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:generate-sm-images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $folder = $this->ask('what folder?');

        if(!file_exists(public_path('images/' . $folder))){
            $this->error('Folder not found!');
            $folder = $this->ask('Please provide a proper folder:');
        }
        $files = LazyCollection::make(function() use ($folder){
            $files = \File::files(public_path('images/' . $folder));
            yield $files;
        });

        if ($this->confirm("You are about to generate images in $folder folder. Do you wish to continue?")) {
            $bar = $this->output->createProgressBar(count($files->first()));
            $bar->start();

            // $files->each(function($file){
            //     $this->info($file);
            // });
            foreach($files->first() as $index => $file){
                if(!Storage::disk('sm-images')->exists($file->getFilename())){
                    try{
                        $image = Image::make($file->getPathname());
                        $image->resize(250, 250, function ($constraint) {
                            // $constraint->aspectRatio();
                        })->save(public_path('images/sm/' . $file->getFilename() ));
                        $bar->advance();
                    }
                    catch (NotReadableException $e)
                    {
                        $this->error($e->getMessage());
                    }
                }
                
            }

            $bar->finish();
            $this->info(' Generating sm image complete!');
        }else{
            $this->line('Thank you then. Good bye!');
        }
    }
}
