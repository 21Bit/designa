<?php

namespace App\Console\Commands;

use DB;
use Artisan;
use App\Models\SubscriptionPlan;
use App\Models\SubscriptionPlanPrice;
use Illuminate\Console\Command;

class CreateSubscriptionPricingsFromPlans extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:migrate-price-from-subscription-plans';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'From the first set-up, it will migrate the default pricing from plans to prices';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        DB::transaction(function(){
            Artisan::call('migrate', array('--path' => '/database/migrations/2021_06_29_234034_create_subscription_plan_prices_table.php'));

            $currentPlans = SubscriptionPlan::all();
            foreach($currentPlans as $plan){
                $this->info(' Migrating ' . $plan->name);
                SubscriptionPlanPrice::create([
                    'plan_id' => $plan->id,
                    'stripe_id' => $plan->stripe_plan,
                    'billing_period' => 'monthly',
                    'price' => $plan->price
                ]);

                $this->info(' Migrating ' . $plan->name . ' success!');
            }

            Artisan::call('migrate', array('--path' => '/database/migrations/2021_06_29_234541_remove_some_table_in_subscription_plans_table.php'));
        });
    }
}
