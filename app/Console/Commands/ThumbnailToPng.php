<?php

namespace App\Console\Commands;

use App\Models\Image;
use DB;
use Illuminate\Console\Command;
use Str;

class ThumbnailToPng extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:thumbnails-to-png';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $images = Image::whereType('thumbnail');

        $bar = $this->output->createProgressBar($images->count());
        $bar->start();

        foreach($images->get() as $image){
            DB::transaction(function() use($image, $bar){
                $image->update(['path' => Str::replace(['.jpg', 'JPG', 'JPEG'],'.png',$image->path)]);
                $bar->advance();
            });
        }

        $bar->finish();
        $this->info(' Converting image to PNG success!');
    }
}
