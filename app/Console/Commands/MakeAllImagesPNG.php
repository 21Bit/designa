<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Intervention\Image\Facades\Image;
use Illuminate\Support\LazyCollection;
use Intervention\Image\Exception\NotReadableException;

class MakeAllImagesPNG extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:convert-png';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Convert images to png in a folder';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $converted = array();
        $folder = $this->ask('what folder?');

        if(!file_exists(public_path('images/' . $folder))){
            $this->error('Folder not found!');
            $folder = $this->ask('Please provide a proper folder:');
        }
        $data = array();
        $files = LazyCollection::make(function() use ($folder){
            $files = \File::files(public_path('images/' . $folder));
            yield $files;
        });

        if ($this->confirm("You are about to convert all images in $folder folder. Do you wish to continue?")) {
            $bar = $this->output->createProgressBar(count($files->first()));
            $bar->start();

            foreach($files->first() as $index => $file){
                $newName = $file->getPath() . '/'. str_replace($file->getExtension(), '', $file->getFilename()) . 'png';
                $type = mime_content_type($file->getPathname());

                if(str_contains($type, "image/")){
                    try
                    {
                        Image::make($file->getPathname())->encode('png')->save($newName);
                        if($type != "image/png"){
                            unlink($file->getPathname());
                        }
                        $bar->advance();
                    }
                    catch (NotReadableException $e)
                    {
                        $this->error($e->getMessage());
                    }
                }
            }

            $bar->finish();
            $this->info(' Converting image to PNG success!');
        }else{
            $this->line('Thank you then. Good bye!');
        }
    }
}
