<?php

use Illuminate\Support\Str;
use Jenssegers\Agent\Agent;



if(! function_exists('version')){
    function version(){
        return "1.1.2";
    }
}

if(! function_exists('getSubscriptionPlanPrice')){
    function getSubscriptionPlanPrice($stripe_id){
        return optional(App\Models\SubscriptionPlanPrice::where('stripe_id', $stripe_id)->first())->price ?? null;
    }
}

if(! function_exists('getSubscriptionPlanPriceBillingPeriod')){
    function getSubscriptionPlanPriceBillingPeriod($stripe_id){
        return optional(App\Models\SubscriptionPlanPrice::where('stripe_id', $stripe_id)->first())->billing_period ?? null;
    }
}

if(! function_exists('getSubscriptionPlanName')){
    function getSubscriptionPlanName($stripe_id){
        return optional(App\Models\SubscriptionPlan::whereHas('prices', function($q) use ($stripe_id){
            $q->where('stripe_id', $stripe_id);
        })->first())->name ?? null;
    }
}





if(! function_exists('uncompleteQoutes')){
    function uncompleteQoutes($companyId){
        $pricingComponentsCount = DB::table('pricing_components')->count();
        return DB::table('qoutes')
                    ->select(
                            'qoutes.id as id',
                            'users.name as customer',
                            'qoutes.event_name as event_name',
                            'qoutes.created_at as created_at',
                            'qoutes.reference_number as reference_number',
                            DB::raw("(select count(*) from qoute_products join products on products.id = qoute_products.product_id where qoute_products.qoute_id = qoutes.id and products.company_id = companies.id) as product_counts"),
                            DB::raw("(select COUNT(*) as total_count from qoute_product_prices where qoute_product_id = qoute_products.id) as priced_component")
                            )
                    ->join('users', 'users.id', '=', 'qoutes.user_id')
                    ->join('qoute_products', 'qoute_products.qoute_id', '=', 'qoutes.id')
                    ->join('products', 'products.id', '=', 'qoute_products.product_id')
                    ->join('companies', 'companies.id', '=', 'products.company_id')
                    ->where('companies.id', $companyId)
                    ->groupBy('companies.id')
                    ->having('priced_component', '<', $pricingComponentsCount)
                    ->count();
    }
}


if(! function_exists('back_end_active_menu'))
{
    //return show
    function back_end_active_menu($label, $segment, String $return = 'menu-open'){
        if(is_array($label)){
            return  in_array(Request::segment($segment), $label) ? $return : '';
        }else{
            return  Request::segment($segment) == $label ? $return : '';
        }
    }
}

if(! function_exists('string_replacer')){
    function string_replacer($string, $withStrips = true){

        $data = [
            "\r" => ' ',
            "\n" => ' ',
            "&nbsp;" => ' ',
            "\r\n\r\n" => ' ',
            '&amp;' => '&',
            '&#39;' => "'",
            "&bull;" => "•",
            "&lsquo;" => "‘",
            "&ldquo;" => '“',
            "&rdquo;" => '”',
            "&scaron;" => "š",
            "&mdash;" => "—",
            "&#39;" => "'",
            "&hellip;" => "…",
            "&rsquo;" => "'",
        ];

        $toProcess = strip_tags($string);
        foreach($data as $key => $value){
            $toProcess = str_replace($key, $value,  strip_tags($toProcess));
        }

        return html_entity_decode($toProcess);
    }
}

if(! function_exists('notification_url')){
    function notification_url($url, $id){
        $url_parts = parse_url($url);
        if (isset($url_parts['query'])){
            return $url . '&notif=' . $id;
        } else {
            return $url . "?notif=" . $id;
        }
    }
}





if(! function_exists('back_end_active_menu_true')){
    function back_end_active_menu_true($label, $segment){
        if(is_array($label)){
            return  in_array(Request::segment($segment), $label) ? 'style="display:block"' : '';
        }else{
            return  Request::segment($segment) == $label ? 'style="display:block"' : '';
        }
    }
}



if(! function_exists('categoryTitle')){
    function categoryTitle($type, $singular = false){
        $title =  Str::title(str_replace('_', ' ', Request::get('type')));
        return $singular ? $title : Str::plural($title);
    }
}

if(! function_exists('operationHoursForEdit')){
    function operationHoursForEdit($payload){
        $array = array();
        $exploded_items = explode(',', $payload);
        foreach($exploded_items as $item){
            $explodeTime  = explode(' - ', $item);
            if(count($explodeTime)> 1){
                array_push($array, [date('H:i', strtotime($explodeTime[0])), date('H:i', strtotime($explodeTime[1]))]);
            }else{
                array_push($array, ['', '']);
            }
        }

        return $array;
    }
}

if(!function_exists('isMobile')){
    function isMobile(){
        $agent = new Agent;
        return $agent->isMobile();
    }
}

if(! function_exists('getFormatedTime')){
    function getFormatedTime($item){
        if($item){
            $explode = explode(' - ', $item);
            $opening = date('h:iA', strtotime($explode[0]));
            $closing = date('h:iA', strtotime($explode[1]));
            return $opening . " - " . $closing;
        }

        return "";
    }
}

if(! function_exists('getFormatedDay')){
    function getFormatedDay($number, $isUcFirst = true){
        switch ($number) {
            case 1:
                return $isUcFirst ? "Monday" : "monday";
                break;
            case 2:
                return $isUcFirst ? "Tuesday" : "tuesday";
                break;
            case 3:
                return $isUcFirst ? "Wednesday" : "wednesday";
                break;
            case 4:
                return $isUcFirst ? "Thursday" : "thursday";
                break;
            case 5:
                return $isUcFirst ? "Friday" : "friday";
                break;
            case 6:
                return $isUcFirst ? "Saturday" : "saturday";
                break;
            case 7:
                return $isUcFirst ? "Sunday" : "sunday";
                break;

            default:
                # code...
                break;
        }
    }
}


if(! function_exists("imagePreview")){
    function imagePreview($file, $fallback){
        if(file_exists(public_path($file))){
            return $file;
        }else{
            return  $fallback;
        }
    }
}

if(! function_exists("imageViewer")){
    function imageViewer($file, $fallback){
        if(file_exists(public_path($file))){
            return $file;
        }else{
            return $fallback;
        }
    }
}

 /**
     * Remove image based on its type
     */
    if(! function_exists("removeImage")){
        function removeImage($image, $type){

            switch ($type) {
                case 'logo':
                    $file = public_path('images/logos/' . $image);
                    break;
                case 'thumbnail':
                    $file = public_path('images/thumbnails/' . $image);
                    break;
                case 'cover':
                    $file = public_path('images/covers/' . $image);
                    break;
                case 'floorplan':
                    $file = public_path('images/floorplans/' . $image);
                    break;
                case '32x32':
                    $file = public_path('images/32x32/' . $image);
                    break;
                default:
                    $file = "";
            }

            if(file_exists($file)){
                //delete main file
                unlink($file);

                //delete 32x32
                if($type != "32x32"){
                    $xs = public_path('images/32x32/' . $image);
                    if(file_exists($xs)){
                        unlink($xs);
                    }
                }
            }
        }
    }
 /**
     * Remove video based on its type
     */
    if(! function_exists("removevideo")){
        function removeVideo($image, $type){
            $file = public_path('videos/' . $image);
            if(file_exists($file)){
                //delete main file
                unlink($file);
            }
        }
    }
