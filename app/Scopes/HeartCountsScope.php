<?php
namespace App\Scopes;

use App\Models\Heart;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class HeartCountsScope implements \Illuminate\Database\Eloquent\Scope
{

    /**
     * @inheritDoc
     */
    public function apply(Builder $builder, Model $model)
    {
         $model = class_basename($model);
         $builder->addSelect(
             ['heart_counts' => Heart::select(DB::raw('count(*)'))
                    ->whereColumn('heartable_id', Str::plural(strtolower($model)). '.id')
                    ->where('heartable_type', "App\\Models\\" . $model)
                ]);
    }
}
