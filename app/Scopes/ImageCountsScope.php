<?php


namespace App\Scopes;


use App\Models\Image;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ImageCountsScope implements \Illuminate\Database\Eloquent\Scope
{

    /**
     * @inheritDoc
     */
    public function apply(Builder $builder, Model $model)
    {
         $model = class_basename($model);
         $builder->addSelect(
             ['image_counts' => Image::select(DB::raw('count(*)'))
                    ->whereColumn('imagable_id', Str::plural(strtolower($model)). '.id')
                    ->where('imagable_type', "App\\Models\\" . $model)
                ]);
    }
}
