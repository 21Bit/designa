<?php


namespace App\Scopes;


use App\Models\Review;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ReviewCountsScope implements \Illuminate\Database\Eloquent\Scope
{

    /**
     * @inheritDoc
     */
    public function apply(Builder $builder, Model $model)
    {
        $model = class_basename($model);
         $builder->addSelect(
             ['review_counts' => Review::select(DB::raw('count(*)'))
                    ->whereColumn('reviewable_id', Str::plural(strtolower($model)). '.id')
                    ->where('reviewable_type', "App\\Models\\" . $model)
                ]);
    }
}
