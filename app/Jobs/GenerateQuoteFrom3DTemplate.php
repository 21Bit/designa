<?php

namespace App\Jobs;

use App\Models\ModelFile;
use Auth;
use Carbon\Carbon;
use Notification;
use App\Models\Qoute;
use App\Models\QouteProduct;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use App\Notifications\Supplier\Qoute\NewQouteNotification;

class GenerateQuoteFrom3DTemplate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var array
     */
    private $modelFile;
    /**
     * @var array
     */
    private $products;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(ModelFile $modelFile, Array $products)
    {
        //
        $this-$modelFile = $modelFile;
        $this->products = $products;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $modelFile = $this-$modelFile;
        $productList = $this->products;

        \DB::transaction(function() use ($modelFile, $productList){
            $quote = new Qoute;
            $quote->reference_number    = time() . rand(10*45, 100*98);
            $quote->user_id             = Auth::user()->id;
            $quote->event_name          = $modelFile->model_name;
            $quote->event_type          = 584;
            $quote->location            = $request->location;
            $quote->event_date          = $request->event_date;
            $quote->time_start          = $request->time_start;
            $quote->time_end            = $request->time_end;
            $quote->number_of_guests    = $request->number_of_guests;

            $quote->save();

            $counter = 0;
            foreach($productList as $decor){
                $quoteProduct = new QouteProduct;
                $quoteProduct->product_id = $decor['id'];
                $quoteProduct->qoute_id = $quote->id;
                $quoteProduct->quantity = $decor['count'];
                $quoteProduct->save();
                $counter++;
            }

            $companies = $quote->suppliers();
            foreach($companies as $company){
                $company->users->map(function($q) use ($quote){
                        $q->mobileNotify(
                            [
                            'title'         => 'New Quote',  // title,
                            'avatar'        => $quote->user->getProfilePicture(),
                            'message'       => $quote->user->name . ' had new quote entitled <b>' . $quote->event_name . '</b>',  // message,
                            'notif_type'    => 'new_qoute',
                            'item_id'       => $quote->id,
                            'link'          => route('dashboard.quote.show', $quote->reference_number),
                            'image'         => '',  // image
                            'read_at'       => '',
                            'timestamp'     => Carbon::now()->format("Y-m-d H:i:s") // timestamp
                        ]);
                });
                Notification::send($company->users, new NewQouteNotification($quote));
            }

        });
    }
}
