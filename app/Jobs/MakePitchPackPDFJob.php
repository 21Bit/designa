<?php

namespace App\Jobs;

use PDF;
use Cache;
use App\Models\User;
use App\Models\Project;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Notifications\Supplier\EventBoard\PitchPackCreatedNotification;

class MakePitchPackPDFJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private Project $project;

    public int $timeout = 350;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Project $project)
    {
        $this->project = $project;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        set_time_limit(300);
        $project = $this->project;
        $file = public_path('pdf/'. $project->slug . '.pdf');
        $pdf = PDF::loadView('dashboard.event-board.pdf', compact('project'));
        $pdf->save($file);

        Cache::forget('pitchpack-queue-' . $project->id);

        User::find($this->project->user_id)
                ->notify( new PitchPackCreatedNotification($project, $file));
    }
}
