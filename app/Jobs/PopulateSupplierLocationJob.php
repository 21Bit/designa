<?php

namespace App\Jobs;

use App\Models\Company;
use App\Services\GoogleService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class PopulateSupplierLocationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public Company $supplier;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Company $supplier)
    {
        $this->supplier = $supplier;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $address = $this->supplier->town . ', ' . $this->supplier->state;
        $location = (new GoogleService)->getLatLong($address);
        if($location){
            $this->supplier->update(['latitude' => $location['latitude'], 'longitude' => $location['longitude']]);
        }
    }
}
