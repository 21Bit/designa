<?php

namespace App\Jobs;

use Mail;
use File;
use Storage;
use ZipArchive;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class Request3dZipperAndMailer implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $html, $mailreciever, $mailSubject, $folder, $isEventSpace;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($folder, $html, $mailreciever, $mailSubject, $isEventSpace = false)
    {
        $this->folder = $folder;
        $this->html = $html;
        $this->mailreciever = $mailreciever;
        $this->mailSubject = $mailSubject;
        $this->isEventSpace = $isEventSpace;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->zipper()
            ->sendEmail();
    }

    function zipper(){
        $zip = new ZipArchive();
        // 
        $fileName = public_path('') .'/zipfiles/' . $this->folder . '.zip';
        $archive_folder = storage_path() . '/app/public/temp/' . $this->folder ;
        $createZip = $zip->open($fileName, ZipArchive::CREATE);

        if ($createZip === TRUE)
        {
             // $files = Storage::allFiles('temp/' . $this->folder);
                $files = preg_grep('/^([^.])/', scandir($archive_folder));
                // dd($files);

                if($this->isEventSpace){
                    foreach($files as $key => $value){
                        if (is_dir($archive_folder . DIRECTORY_SEPARATOR . $value)){
                            $anotherFolders = preg_grep('/^([^.])/', scandir($archive_folder . DIRECTORY_SEPARATOR . $value));
                            
                            foreach($anotherFolders as $key2 => $value2){
                                if (is_dir($archive_folder . DIRECTORY_SEPARATOR . $value2)){
                                    $zip->addEmptyDir($value2);
                                    foreach($anotherFolders as $key3 => $value3){
                                        $zip->addFile($archive_folder . DIRECTORY_SEPARATOR . $value3);
                                    }
                                }else{
                                    $file = $archive_folder .'/'. $value . '/' .  $value2;
                                    $zip->addFile($file, $value .'/'. $value2);
                                }
                            }
    
                        }else{
                            $zip->addFile($archive_folder . DIRECTORY_SEPARATOR . $value);
                        }
                    }      
                }else{
                    foreach($files as $key => $value){
                        if (is_dir($archive_folder . DIRECTORY_SEPARATOR . $value)){
                            $anotherFolders = preg_grep('/^([^.])/', scandir($archive_folder . DIRECTORY_SEPARATOR . $value));
                            
                            foreach($anotherFolders as $key2 => $value2){
                                if (is_dir($archive_folder . DIRECTORY_SEPARATOR . $value2)){
                                    $zip->addEmptyDir($value2);
                                    foreach($anotherFolders as $key3 => $value3){
                                        $zip->addFile($archive_folder . DIRECTORY_SEPARATOR . $value3);
                                    }
                                }else{
                                    $file = $archive_folder .'/'. $value . '/' .  $value2;
                                    $zip->addFile($file, $value .'/'. $value2);
                                    // return $file;
                                }
                            }
    
                        }else{
                            $zip->addFile($archive_folder . DIRECTORY_SEPARATOR . $value);
                        }     
                    }      
                } 

            $zip->close(); 
            Storage::deleteDirectory('public/temp/' . $this->folder);
        }

        return $this;
    }


    function sendEmail(){
        $folder = $this->folder;
        $size = filesize(public_path('zipfiles/' . $folder . '.zip'));
        if($size < 5242880){
            $this->html .= "<br>
                    <br>
                    As attached below are the images organized by folder";
                    
            Mail::html($this->html, function($message) use($folder) {
                $message->subject($this->mailSubject)
                    ->from('no-reply@designa.studio')
                    ->to($this->mailreciever)
                    // ->cc(['mildred@globalenablement.com'])
                    ->attach(public_path('zipfiles/' . $folder . '.zip'), ['as' =>  $folder . '.zip']);
            });

            // deleting zip file
            unlink(public_path('zipfiles/' . $folder . '.zip'));

        }else{
            $this->html .= "<br>
                    Here is all the files that had been submitted with this request
                    <br>
                    <a href='" . url('/zipfiles/' . $folder .'.zip')  ."'> Download files</a>";

            Mail::html($this->html, function($message) use($folder) {
                $message->subject($this->mailSubject)
                    ->from('no-reply@designa.studio')
                    ->to($this->mailreciever);
            });
        }

    }

}
