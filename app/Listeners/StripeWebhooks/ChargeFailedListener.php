<?php

namespace App\Listeners\StripeWebhooks;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Spatie\WebhookClient\Models\WebhookCall;

class ChargeFailedListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function handle(WebhookCall $webhookCall)
    {
        \Log::info($webhookCall->payload);
        // do your work here

        // you can access the payload of the webhook call with `$webhookCall->payload`
    }
}
