<?php

namespace App\Mail\Dashboard;

use App\Models\Company;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MakeSupplierLive extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    public $company;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $link = $this->company->loginlink . '?activation=true';
        return $this->subject('Make my profile live')
                ->view('mails.dashboard.make-supplier-live', compact('link') );
    }
}
