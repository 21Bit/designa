<?php

namespace App\Mail\Dashboard;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ItemLockMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    public $item, $name, $reason;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($item, $name, $reason)
    {
        $this->item = $item;
        $this->name = $name;
        $this->reason = $reason;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['item'] = $this->item;
        $data['name'] = $this->name;
        $data['reason'] = $this->reason;

        return $this->subject('Your post has been unpublished')
                ->view('mails.dashboard.lock-item',  $data);
    }
}
