<?php

namespace App\Mail\Dashboard;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use App\Models\SupplierInspiration;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TagSupplierProduct extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    public $supplierInspiration; 
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(SupplierInspiration $supplierInspiration)
    {
        $this->supplierInspiration = $supplierInspiration;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $supplierInspiration = $this->supplierInspiration; 
        $link = route('site.inspiration.tags.accept', $supplierInspiration->id);
        return $this->subject('Designa Tag Inspiration Invitation')
                ->view('mails.dashboard.inspiration.invite-tag-supplier-product', compact('supplierInspiration', 'link') );
    }
}
