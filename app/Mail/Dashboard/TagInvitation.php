<?php

namespace App\Mail\Dashboard;

use App\Models\Company;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use App\Models\SupplierInspiration;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TagInvitation extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    public $supplierInspiration, $companyInviter;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(SupplierInspiration $supplierInspiration, Company $companyInviter)
    {
        $this->companyInviter = $companyInviter;
        $this->supplierInspiration = $supplierInspiration;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $inspiration = $this->supplierInspiration->inspiration;   
        $companyInviter = $this->companyInviter;
        $supplierInspiration = $this->supplierInspiration;
        return $this->subject('Designa Tag Inspiration Invitation')
                ->view('mails.dashboard.invite-tag-inspiration', compact('supplierInspiration', 'companyInviter', 'inspiration') );
    }
}
