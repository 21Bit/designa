<?php

namespace App\Mail\Dashboard;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FreeTrialAddedMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    private $company;
    private $days;
    private $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($company, $days, $user)
    {
        $this->company = $company;
        $this->days = $days;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = [
            'company' => $this->company->name,
            'days' => $this->days,
            'user' => $this->user->name
        ];
        return $this->subject('Congratulations! You are in a free trial')
                ->view('mails.dashboard.free-trial-to-supplier', compact('data') );
    }
}
