<?php

namespace App\Models;

use DB;
use Auth;
use Illuminate\Database\Eloquent\Model;

class Qoute extends Model
{

    protected $guarded = [];

    function decors(){
        return $this->hasMany(QouteProduct::class);
    }

    function products(){
        return $this->belongsToMany(Product::class, 'qoute_products');
    }

    function companyProducts(){
        return $this->belongsToMany(Product::class, 'qoute_products');
    }

    function companyDecors(){
        return $this->hasManyThrough(Product::class, QouteProduct::class, 'product_id', 'quote_product');
    }

    function companyDecor(){
        return $this->hasMany(QouteProduct::class)
                ->whereHas('product', function($q){
                    $q->where('company_id', Auth::user()->company_id);
                });
    }


    function scopeHasPrice($query, $supplier = 0){
        $query->addSelect('has_price', function($query) use ($supplier){
            $q->select('id')
                ->from('qoute_products')
                ->join('products', function ($join) use($supplier) {
                        $join->on('qoute_products.product_id', 'products.id')
                             ->where('products.company_id', $supplier);
                    })->join('qoute_product_prices', function ($join) use($supplier) {
                        $join->on('qoute_products.id','qoute_product_prices.qoute_product_id')
                        ->where('qoute_product_prices.price', '>', 0);
                    })
                ->limit(1);
        });
        $query->addSelect(['has_price' => function($query) use($supplier){
                $query
                    ->from('qoute_products')
                     ->join('products', function ($join) use($supplier) {
                        $join->on('qoute_products.product_id', 'products.id')
                             ->where('products.company_id', $supplier);
                    })->join('qoute_product_prices', function ($join) use($supplier) {
                        $join->on('qoute_products.id','qoute_product_prices.qoute_product_id')
                        ->where('qoute_product_prices.price', '>', 0);
                    });
            }]);
//        return $this->decors()->HasPrice($supplier);
    }

    function scopeHasNoPrice($query, $supplier = 0){
        return $this->decors()->HasNoPrice($supplier);
    }

    function user(){
        return $this->belongsTo(User::class);
    }

     function category(){
         return $this->belongsTo(Category::class, 'event_type');
     }

    function getOverAllPrice(){
        $price = 0;
        $qouteproducts = $this->decors()->has('product')->get();

        foreach($qouteproducts as $qp){
           $productQuoteprice = $qp->getTotal();
           if($productQuoteprice != ""){
               $price += $productQuoteprice;
           }
        }

        return $price;
    }

    function supplierTotalPrice($id){
        $total = 0;
        $qouteproducts = $this->decors()->whereHas('product', function($q) use ($id){
            $q->whereCompanyId($id);
        })->get();

        foreach($qouteproducts as $qp){
            $productQuoteprice = $qp->getTotal();
            if($productQuoteprice != ""){
                $total += $productQuoteprice;
            }
        }

        return $total;
    }


    function suppliers(){
        $suppliersId = [];
        foreach($this->decors()->has('product')->get() as $decor){
            $company = $decor->product->company_id;
            if(!in_array($company, $suppliersId)){
                array_push($suppliersId, $company);
            }
        }

        return Company::find($suppliersId);
    }

    function scopeWithPricingStatus($query, $supplierId){
        return $query->addSelect(['pricing_status' => function($query) use($supplierId) {
                    $query->selectRaw(true)
                            ->from('qoute_products')
                            ->join('products', 'qoute_products.product_id', '=' , 'products.id')
                            ->join('qoute_product_prices', 'qoute_products.id', 'qoute_product_prices.qoute_product_id')
                            ->where('products.company_id', $supplierId)
                            ->select('*')
                            ->limit(1);
        }]);
    }

    function isSupplierPricingsComplete($supplierId){
        $pricingComponentsCount = DB::table('pricing_components')->where('active', 1)->count();
        
        $qouteproducts = $this->decors()->whereHas('product', function($q) use ($supplierId){
            $q->whereCompanyId($supplierId);
        })->pluck('id')->toArray();

        return QouteProductPrice::whereIn('qoute_product_id', $qouteproducts)
                    ->count() == PricingComponent::whereActive(1)->count();        
    }
}
