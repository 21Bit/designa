<?php

namespace App\Models;

use App\Scopes\HeartCountsScope;
use App\Scopes\ImageCountsScope;
use App\Scopes\ReviewCountsScope;
use Cache;
use Carbon\Carbon;
use App\Traits\HasView;
use App\Traits\HasShare;
use App\Traits\HasReview;
use App\Traits\HasModelFile;
use App\Traits\HasCategories;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Sluggable\HasSlug;
use App\Traits\HasHeartFunction;
use App\Traits\HasImageFunction;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory, SoftDeletes, HasModelFile, HasSlug, HasCategories, HasReview, HasImageFunction, HasView, HasHeartFunction, HasShare;

    public function company(){
        return $this->belongsTo(Company::class);
    }

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }


    function scopeWithColors($query){
        return $query->whereHas('categories', function($q){
            $q->where('type', 'color');
        }, 'colors');
    }

    public function getThumbnailAttribute(){
        $product = $this;
        return Cache::remember('product.thumbnail.' . $this->id, Carbon::now()->addDays(7), function() use ($product){
              return $product->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder.png');
        });
    }

    public function getProductTypeAttribute(){
        $key = 'product.product_type.' . $this->id;
        $product = $this;
        return Cache::rememberForever($key, function() use ($product){
            return $product->categories()->whereType('product_type')->first();
        });
    }

    public function getStyleAttribute(){
        $key = 'product.style.' . $this->id;
        $product = $this;
        return Cache::rememberForever($key, function() use ($product){
            return $product->categories()->whereType('style')->first();
        });
    }

}
