<?php

namespace App\Models;

use App\Traits\HasReview;
use App\Traits\HasAttachment;
use App\Models\PricingComponent;

use Illuminate\Database\Eloquent\Model;

class QouteProduct extends Model
{
    protected $appends = [
        'has_price',
        'has_no_price'
    ];

    use HasReview, HasAttachment;

    function qoute(){
        return $this->belongsTo(Qoute::class);
    }

    function getHasPriceAttribute(){
        return $this->prices->count() ;
    }

    function getHasNoPriceAttribute(){
        return $this->prices->count() < 1;
    }

    function scopeHasPrice($query, $supplier){
        if($supplier){
            return $query->whereHas('product', function($q) use ($supplier){
                    $q->where('company_id',$supplier);
                })
                ->has('prices');
        }else{
            return $query
                ->has('prices');
        }
    }

    function scopeHasNoPrice($query, $supplier){
        if($supplier){
            return $query->whereHas('product', function($q) use ($supplier){
                    $q->where('company_id', $supplier);
                })
                ->whereHas('prices', function($q){
                    $q->where('price', '<' , 1);
                });
        }else{
            return $query
                ->whereHas('prices', function($q){
                    $q->where('price', '<' , 1);
                });
        }
    }

    function product(){
        return $this->belongsTo(Product::class);
    }

    function prices(){
        return $this->hasMany(QouteProductPrice::class);
    }

    function getTotal(){
        $overall = 0;
        foreach(PricingComponent::whereActive(1)->get() as $component){
            if($this->getComponentPrice($component->id) != ""){
                if($component->multipliable){
                    $overall += $this->getComponentPrice($component->id) * $this->quantity;
                }else{
                    $overall += $this->getComponentPrice($component->id);
                }
            }
        }

        return $overall ?? "";
    }

    function isSupplierPricingsComplete($id){
        $complete = true;
        $qouteproducts = $this->whereHas('product', function($q) use ($id){
            $q->whereCompanyId($id);
        })->get();

        foreach($qouteproducts as $qp){
            foreach(PricingComponent::whereActive(1)->get() as $component){
                $price = $qp->getComponentPrice($component->id);
                if($price == ''){
                    return false;
                }
            }
        }

        return true;
    }


    function getComponentPrice($id){
        return $this->prices()->where('pricing_component_id', $id)->first()->price ?? 0;
    }

    function getSubTotalPrice($id){
        $component = PricingComponent::find($id);

        if($component->multipliable){
            return $this->prices()->where('pricing_component_id', $id)->first() ? $this->prices()->where('pricing_component_id', $id)->first()->price * $this->quantity : "";
        }

        return $this->prices()->where('pricing_component_id', $id)->first()->price ?? '';
    }
}
