<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use PDF;
use Auth;
use Cache;
use App\Traits\HasCategories;
use Spatie\Sluggable\HasSlug;
use App\Traits\HasImageFunction;
use App\Jobs\MakePitchPackPDFJob;
use Spatie\Sluggable\SlugOptions;
use App\Traits\HasImageIntervention;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasImageFunction, HasCategories, HasSlug, HasFactory;

    protected $guarded = [];
    protected $appends = ['created_by'];

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }


    function getCreatedByAttribute(){
        $key = 'event-board.created_by.' . $this->id;
        return Cache::rememberForever($key, function(){
            if($this->user->company){
                return $this->user->company->name;
            }
            return $this->user->name;
        });
    }


    function inspirationImages(){
        return $this->belongsToMany(Image::class, 'project_images')
                ->where('imagable_type', 'App\\Models\\Inspiration');
    }

    function galleryImages(){
        return $this->belongsToMany(Image::class, 'project_images')
                ->where('imagable_type', 'App\\Models\\Inspiration');
    }

    function productImages(){
        return $this->belongsToMany(Image::class, 'project_images')
                ->where('imagable_type', 'App\\Models\\Product');
    }

    function images(){
        return $this->belongsToMany(Image::class, 'project_images');
    }



    function generatePDF(){
        $project = $this;
        dispatch(new MakePitchPackPDFJob($project));
    }


    function user(){
        return $this->belongsTo(User::class);
    }

    function getThumbnailAttribute(){
        $key = 'event-board.thumbnail.' . $this->id;
        return Cache::rememberForever($key, function(){
            if($this->inspirationImages()->first()){
                return imageViewer('/images/gallery/'. $this->inspirationImages->first()->path, '/images/placeholders/placeholder.png');
            }else{
                return '/images/placeholders/placeholder.png';
            }
        });
    }

    function getSettingNameAttribute(){
        $key = 'event-board.setting.' . $this->id;
        return Cache::rememberForever($key, function(){
            return optional($this->categories()->whereType('setting')->first())->name;
        });
    }

    function getCaterNameAttribute(){
        $key = 'event-board.cater.' . $this->id;
        return Cache::rememberForever($key, function(){
            return optional($this->categories()->whereType('cater')->first())->name;
        });
    }

    function getStyleNameAttribute(){
        $key = 'event-board.style.' . $this->id;
        return Cache::rememberForever($key, function(){
            return optional($this->categories()->whereType('style')->first())->name;
        });
    }

    function color(){
        return $this->categories()->whereType('color');
    }


    function getColorsAttribute(){
        $key = 'event-board.colors.' . $this->id;
        return Cache::rememberForever($key, function(){
            return $this->color()->get();
        });
    }


    function products(){
        return $this->belongsToMany(Product::class);
    }


    function imageCount($type = null){
        $type = $type ? $type : 'inspiration';
        $key = 'event-board.imagecount.' . $type. '.'. $this->id;
        return Cache::rememberForever($key, function() use ($type){
        if($type == "product"){
                return $this->inspirationImages()->whereImagableType('App\Models\Product')->count();
            }else{
                return $this->inspirationImages()->whereImagableType('App\Models\Inspiration')->count();
            }
        });
    }


    function clearCache(){
        Cache::forget('event-board.colors.' . $this->id);
        Cache::forget('event-board.imagecount.inspiration.' . '.'. $this->id);
        Cache::forget('event-board.imagecount.product' . '.'. $this->id);
        Cache::forget('event-board.style.' . $this->id);
        Cache::forget('event-board.cater.' . $this->id);
        Cache::forget('event-board.setting.' . $this->id);
        Cache::forget('event-board.thumbnail.' . $this->id);
        Cache::forget('event-board.created_by.' . $this->id);

        $key = 'dashboard.event-board.list.' . Auth::user()->id;
        $keyApi = 'api.event-board.list.' . Auth::user()->id;
        Cache::forget($key);
        Cache::forget($keyApi);
    }
}
