<?php

namespace App\Models;

use Auth;
use Mail;
use Cache;
use Carbon\Carbon;
use App\Traits\HasView;
use App\Traits\HasShare;
use App\Traits\HasReview;
use Illuminate\Http\Request;
use App\Traits\HasCategories;
use Spatie\Sluggable\HasSlug;
use App\Traits\HasAreaLocation;
use App\Traits\HasImageFunction;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;
use App\Mail\Dashboard\MakeSupplierLive;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Company extends Model
{
    use HasFactory, HasSlug, HasCategories, HasImageFunction, HasReview, HasView, HasShare;

    protected $guarded = [];

    public function locations(){
        return $this->morphMany(Location::class, 'locationable');
    }

    public function getLogoAttribute(){
        $company = $this;
        return Cache::remember('company.' . $this->id . '.logo', Carbon::now()->addDays(7), function() use($company){
          return $company->image('logo')->path('logos', '/images/placeholders/placeholder250x200.png');
        });
    }

    public function hasTrial(){
        foreach($this->users as $user){
            if($user->onTrial() || $user->onTrial('Designa Plan')){
                return true;
            }
        }
        return false;
    }


    function exceededFunctionSpaces(){
      $exceeded = 0;
      foreach($this->venues as $venue){
        $exceeded += $venue->exceededFunctionSpaces() ?? 0;
      }
      return $exceeded;
    }

    function trialEnds(){
        $key = 'company-trials-' . $this->id;
        return Cache::rememberForever($key, function(){
            return $this->users()->get()->max('trial_ends_at');
        });
    }

    public function getThumbnailAttribute(){
        $company = $this;
        return Cache::remember('company.' . $this->id . '.thumbnail', Carbon::now()->addDays(7), function() use ($company){
          return $company->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder250x200.png');
        });
    }

    public function getLoginlinkAttribute(){
        $user = $this->users()->first();
        if($user){
            $url = config('autologin.prefix') . $user->id . config('autologin.suffix');
            return route('autologin', $url);
        }else{
          return "#";
        }
    }

    public function sendActiveLink(){
        $email = $this->email;
        // sending email tag inspiration invitation
        Mail::to($email)
          // ->cc(['michael@designa.studio', 'nick@designa.studio', 'hello@designa.studio', 'jbdev21@gmail.com']) // remove in prod
          ->send(new MakeSupplierLive($this));

        // if(!request()->ajax()){
        //   return back();
        // }
    }

    function getQoutesAttibute(){
      return Qoute::whereHas('decors', function($q){
                    $q->whereHas('product', function($q){
                            $q->where('company_id', $this->id);
                    });
                });
    }

    function qouteList(){
        return Qoute::whereHas('decors', function($q){
                  $q->whereHas('product', function($q){
                          $q->where('company_id', $this->id);
                  })->with('prices');
              });
    }

    function uncompleteQoutes(){
      return Cache::remember("uncompleteQoutes-$this->id", 120, function(){
           return uncompleteQoutes($this->id);
      });
    }

    public function venues(){
        return $this->hasMany(Venue::class);
    }

    public function inspirations(){
        return $this->hasMany(Inspiration::class);
    }

    public function products(){
        return $this->hasMany(Product::class);
    }

    public function users(){
        return $this->hasMany(User::class);
    }

    public function permissions(){

      // return $this->hasMany(Permission::class, $this->roles);
      $permissions =  array();
      $roles = $this->roles;
      foreach($roles as $role){
        foreach($role->permissions as $per){
          array_push($permissions, $per->name);
        }
      }
      return $permissions;
    }


    public function roles(){
        return $this->belongsToMany(Role::class, 'company_role');
    }


    public function singleVenueEventSpaces(){
        $venue = $this->venues()->first();
        if($venue){
            return $venue->functionSpaces;
        }else{
          return (object) [];
        }
    }



    public function hearts(){
        return $this->morphMany(Heart::class, 'heartable');
    }

    public function inspirationHeartsCounts($month = null, $year = null, $day = null, $origin = null){
        $inspirations = $this->inspirations;
        $total = -0;
        foreach($inspirations as $inspiration){
            $hearts = $inspiration->hearts();
            if($month){
              $hearts->whereMonth('created_at', $month);
            }
            if($year){
              $hearts->whereYear('created_at', $year);
            }
            if($day){
              $hearts->whereDate('created_at', $day);
            }

            $total += $origin ? $hearts->whereOrigin($origin)->count() : $hearts->count();
        }

        return $total;
    }



    public function venueHeartsCounts($month = null, $year = null, $day = null, $origin = null){
        $venues = $this->venues;
        $total = -0;
        foreach($venues as $venue){
            $hearts = $venue->hearts();
            if($month){
              $hearts->whereMonth('created_at', $month);
            }
            if($year){
              $hearts->whereYear('created_at', $year);
            }
            if($day){
              $hearts->whereDate('created_at', $day);
            }

            $total += $origin ? $hearts->whereOrigin($origin)->count() : $hearts->count();

        }

        return $total;
    }

    public function decorHeartsCounts($month = null, $year = null, $day = null, $origin = null){
        $decors = $this->products;
        $total = -0;
        foreach($decors as $decor){
            $hearts =  $decor->hearts();

            if($month){
              $hearts->whereMonth('created_at', $month);
            }
            if($year){
              $hearts->whereYear('created_at', $year);
            }
            if($day){
              $hearts->whereDate('created_at', $day);
            }

            $total += $origin ? $hearts->whereOrigin($origin)->count() : $hearts->count();
        }

        return $total;
    }

    public function decorReviewsCounts($month = null, $year = null, $day = null, $origin = null){
        $decors = $this->products;
        $total = -0;
        foreach($decors as $decor){
            $reviews =  $decor->reviews();
            if($month){
              $reviews->whereMonth('created_at', $month);
            }
            if($year){
              $reviews->whereYear('created_at', $year);
            }
            if($day){
              $reviews->whereDate('created_at', $day);
            }

            $total += $origin ? $reviews->whereOrigin($origin)->count() : $reviews->count();
        }

        return $total;
    }


    public function inspirationSharesCounts($month = null, $year = null, $day = null, $origin = null){

        $inspirations = $this->inspirations;
        $counts = 0;

        foreach($inspirations as $inspiration){
            $shares =  $inspiration->shares();
            if($month){
              $shares->whereMonth('created_at', $month);
            }
            if($year){
              $shares->whereYear('created_at', $year);
            }
            if($day){
              $shares->whereDate('created_at', $day);
            }

            $counts += $origin ? $shares->whereOrigin($origin)->count() : $shares->count();
        }

        return $counts;

        if(isset($shares)){
            return $origin ? $shares->whereOrigin($origin)->count() : $shares->count();
        }else{
            return 0;
        }

    }

    public function venueSharesCounts($month = null, $year = null, $day = null, $origin = null){
        $venues = $this->venues;
        foreach($venues as $venue){
            $shares =  $venue->shares();
            if($month){
              $shares->whereMonth('created_at', $month);
            }
            if($year){
              $shares->whereYear('created_at', $year);
            }
            if($day){
              $shares->whereDate('created_at', $day);
            }
        }

        if(isset($shares)){
            return $origin ? $shares->whereOrigin($origin)->count() : $shares->count();
        }else{
            return 0;
        }
    }
    public function venueReviewsCounts($month = null, $year = null, $day = null, $origin = null){
        $venues = $this->venues;
        foreach($venues as $venue){
            $reviews =  $venue->reviews();
            if($month){
              $reviews->whereMonth('created_at', $month);
            }
            if($year){
              $reviews->whereYear('created_at', $year);
            }
            if($day){
              $reviews->whereDate('created_at', $day);
            }
        }

        if(isset($reviews)){
            return $origin ? $reviews->whereOrigin($origin)->count() : $reviews->count();
        }else{
            return 0;
        }
    }

    public function decorSharesCounts($month = null, $year = null, $day = null, $origin = null){
        $products = $this->products;
        foreach($products as $product){
            $shares =  $product->shares();
            if($month){
              $shares->whereMonth('created_at', $month);
            }
            if($year){
              $shares->whereYear('created_at', $year);
            }
            if($day){
              $shares->whereDate('created_at', $day);
            }
        }

        if(isset($shares)){
            return $origin ? $shares->whereOrigin($origin)->count() : $shares->count();
        }else{
            return 0;
        }
    }

    public function inspirationViewsCounts($month = null, $year = null, $day = null, $origin = null){
        $inspirations = $this->inspirations;
        foreach($inspirations as $inspiration){
            $views =  $inspiration->views();
            if($month){
              $views->whereMonth('created_at', $month);
            }
            if($year){
              $views->whereYear('created_at', $year);
            }
            if($day){
              $views->whereDate('created_at', $day);
            }
        }

        if(isset($views)){
            return $origin ? $views->whereOrigin($origin)->count() : $views->count();
        }else{
            return 0;
        }
    }

    public function venueViewsCounts($month = null, $year = null, $day = null, $origin = null){
        $venues = $this->venues;
        foreach($venues as $venue){
            $views =  $venue->views();
            if($month){
              $views->whereMonth('created_at', $month);
            }
            if($year){
              $views->whereYear('created_at', $year);
            }
            if($day){
              $views->whereDate('created_at', $day);
            }
        }

        if(isset($views)){
            return $origin ? $views->whereOrigin($origin)->count() : $views->count();
        }else{
            return 0;
        }
    }

    public function decorViewsCounts($month = null, $year = null, $day = null, $origin = null){
        $products = $this->products;
        foreach($products as $product){
            $views =  $product->views();

            if($month){
              $views->whereMonth('created_at', $month);
            }
            if($year){
              $views->whereYear('created_at', $year);
            }
            if($day){
              $views->whereDate('created_at', $day);
            }
        }

        if(isset($views)){
            return $origin ? $views->whereOrigin($origin)->count() : $views->count();
        }else{
            return 0;
        }
    }



    public function asyncRoleToUsers(){
        $users = $this->users;
        $companyRoles = $this->roles;
        foreach($users as $user){
            //sync Roles
            $user->syncRoles($companyRoles);

            $user->detachPermissions();
            // // sync Permissions
            // foreach($companyRoles as $role){
            //     foreach($role->permissions as $permission){
            //         if(!$user->can($permission->name)){
            //             $user->attachPermission($permission);
            //         }
            //     }
            // }
        }
    }



    public function images(){
        return $this->morphMany(Image::class, 'imagable');
    }

    public function getLogoPath($isSmall = false){
        $logo = $this->images()->where('type', 'logo')->first();
        if($logo){
            return $isSmall ? url('images/32x32/'.$logo->path) : url('images/logos/'.$logo->path);
        }else {
            return $isSmall ? url('images/placeholders/company-logo-32x32.png') : url('images/placeholders/company-logo.png');
        }

    }


    public function setActiveUsers(){
      $this->users()->where('active', 1);
    }

    public function getCoverPath(){
        $cover = $this->images()->where('type', 'cover')->first();
        if($cover){
            return url('images/covers/'.$cover->path);
        }else{
            return "https://via.placeholder.com/728x90.png";
        }
    }

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }


    function roleList(){
        return Cache::get('company.' . $this->id . '.role-list', function(){
            $roles = $this->roles;
            $list = '';
            $counter = 1;
            foreach($roles as $role){
                $list .= $role->display_name;
                if(count($roles) > $counter){
                    if(count($roles) == $counter+1){
                        $list .= " and ";
                    }else{
                        $list .= ", ";
                    }
                }

                $counter++;
            }

            return $list;

        });
    }

    function caterList(){
        $caters = $this->categories()->whereType('cater')->get();
        $list = '';
        $counter = 1;
        foreach($caters as $cater){
            $list .= $cater->name;
            if(count($caters) > $counter){
                if(count($caters) == $counter+1){
                    $list .= " and ";
                }else{
                    $list .= ", ";
                }
            }

            $counter++;
        }

        return $list;
    }
}
