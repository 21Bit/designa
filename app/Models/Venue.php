<?php

namespace App\Models;

use App\Traits\HasView;
use App\Traits\HasShare;
use App\Traits\HasReview;
use App\Traits\HasModelFile;
use App\Traits\HasAttachment;
use App\Traits\HasCategories;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Spatie\Sluggable\HasSlug;
use App\Traits\HasHeartFunction;
use App\Traits\HasImageFunction;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;

class Venue extends Model
{
    use HasFactory, HasModelFile, HasSlug, HasImageFunction, HasCategories, HasReview, HasView, HasShare, HasHeartFunction, HasAttachment;


    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }


    public function functionSpaces(){
        return $this->hasMany(FunctionSpace::class);
    }


    function exceededFunctionSpaces() : int {
        if($this->functionSpaces()->count() > 2){
            $count = $this->functionSpaces()->has('modelFiles')->count() - config('subscription.free_functionspaces_count');
            return $count > 0 ?? 0;
        }

        return 0;
    }




    public function company(){
        return $this->belongsTo(Company::class);
    }

    public function getThumbnailAttribute(){
        $venue = $this;
        return Cache::remember('inspiration.' . $this->id . '.thumbnail', Carbon::now()->addDays(7), function() use ($venue){
          return $venue->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder250x200.png');
        });
    }

    public function getCompanyLogoAttribute(){
        $venue = $this;
        return Cache::remember('inspiration.' . $this->id . '.company.logo', Carbon::now()->addDays(7), function() use ($venue){
          return $venue->image('logo')->path('logos', '/images/placeholders/placeholder250x200.png');
        });
    }

    function categoryList($type){
        $categories = $this->categories()->where('type', $type)->get();
        $list = '';
        $counter = 1;
        foreach($categories as $category){
            $list .= $category->name;
            if(count($categories) > $counter){
                if(count($categories) == $counter+1){
                    $list .= " and ";
                }else{
                    $list .= ", ";
                }
            }

            $counter++;
        }

        return $list;
    }


    function operatingHoursList(){
        $data = [];
        $explodedTimes = explode(',', $this->operating_hours);
        for($i = 0; $i < count($explodedTimes); $i++){
            if($explodedTimes[$i] != "null" ){
                $item = [];
                $item['time'] = getFormatedTime($explodedTimes[$i]);  //time
                $item['day'] = getFormatedDay($i+1);// day
                array_push($data, $item);
            }
        }

        return $data;
    }


}
