<?php

namespace App\Models;

use App\Traits\HasModelFile;
use App\Traits\HasImageFunction;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FunctionSpace extends Model
{
    use HasImageFunction, HasModelFile, HasFactory;
    
    function venue(){
        return $this->belongsTo(Venue::class);
    }

    function company(){
        // return $this->hasOne(Company::class, 'venue_id');
        return $this->venue->company;
        // return $this->hasOneThrough(Company::class, Venue::class);
    }
}
