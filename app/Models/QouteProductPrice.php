<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QouteProductPrice extends Model
{
    function pricingComponent(){
        return $this->belongsTo(PricingComponent::class);
    }
}
