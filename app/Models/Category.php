<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $fillable = ['name', 'description', 'type', 'module', 'credit_by', 'credit_location'];

    use HasSlug, HasFactory;

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    public function parent(){
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public function subCategories(){
        return $this->hasMany(Category::class, 'parent_id');
    }

    public function styles(){
        return $this->subCategories()->where('type', 'style');
    }

    public function settings(){
        return $this->subCategories()->where('type', 'setting');
    }

    public function companies()
    {
        return $this->morphedByMany(Company::class, 'categorizable');
    }

    public function categories(){
        return $this->morphedByMany(Category::class, 'categorizable');
    }
}
