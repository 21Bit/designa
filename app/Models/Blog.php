<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasImageFunction;

class Blog extends Model
{
    use HasImageFunction;
}
