<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;

class SubscriptionPlan extends Model
{
    use HasSlug, HasFactory;

    protected $guarded = [];

    public function accesses(){
        return $this->belongsToMany(Access::class, 'access_subscription_plan');
    }

    public function prices(){
        return $this->hasMany(SubscriptionPlanPrice::class);
    }

    public function getPrice($billing_period = 'monthly'){
        return $this->prices()->where('billing_period', $billing_period)->first() ?? null;
    }

    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

}
