<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupplierInspiration extends Model
{
    public function inspiration(){
        return $this->belongsTo(Inspiration::class);
    }

    public function inviter(){
        return $this->belongsTo(Company::class, 'request_by');
    }

    public function role(){
        return $this->belongsTo(Role::class);
    }

    public function company(){
        return $this->belongsTo(Company::class);
    }

}
