<?php

namespace App\Models;

use DB;
use Auth;
use Cache;
use Carbon\Carbon;
use App\Models\Access;
use App\Models\Firebase;
use App\Models\FunctionSpace;
use App\Facades\FirebaseFMC;
use App\Traits\HasModelFile;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Http\Request;
use Laravel\Cashier\Billable;
use App\Traits\HasActivityLog;
use App\Models\SubscriptionPlan;
use App\Services\SubscriptionService;
use App\Traits\HasImageFunction;
use Laravel\Passport\HasApiTokens;
use App\Traits\HasImageIntervention;
use Illuminate\Notifications\Notifiable;
use Laratrust\Traits\LaratrustUserTrait;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{

    use HasModelFile, LaratrustUserTrait, Notifiable, HasApiTokens, HasImageFunction, HasActivityLog, Billable, HasFactory;

     /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'trial_ends_at'
    ];

    function heartedInspirations(): \Illuminate\Database\Eloquent\Relations\HasManyThrough
    {
        return $this->hasManyThrough(Inspiration::class, Heart::class,
            'user_id',
            'id',
            'id',
        'heartable_id')->where('hearts.heartable_type', "App\\Models\\Inspiration");
    }

    function heartedProducts(): \Illuminate\Database\Eloquent\Relations\HasManyThrough
    {
        return $this->hasManyThrough(Product::class, Heart::class,
            'user_id',
            'id',
            'id',
        'heartable_id')->where('hearts.heartable_type', "App\\Models\\Product");
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    function nextBillingDate(){
        if($this->stripeSubscription()){
            if($this->upcomingInvoice()){
                $this->upcomingInvoice()->period_end;
                return Carbon::createFromTimeStamp($this->upcomingInvoice()->period_end)->toFormattedDateString();
            }
            return null;
        }

        return null;
    }

    function getUserAccessToken(){
        return $this->createToken('Designa Password Grant Client')->accessToken;
    }

    function stripeSubscription($index = 0){
        return optional($this->asStripeCustomer()["subscriptions"]->data ?? [])[$index] ?? [];
    }

    function getCompanyRolesAttribute(){
        $company = $this->company;
        return Cache::remember('company'. $company->id . '.roles', Carbon::now()->addDays(7), function() use($company){
            return $company->roles;
        });
    }


    function getUserPaymentMethodsAttribute(){
        $key = $this->id . '-payment_methods';
        $user = $this;
        return Cache::rememberForever($key, function() use($user){
            return $user->paymentMethods();
        });
    }

    function getDefaultPaymentMethodAttribute(){
        $key = $this->id . '-default-payment_methods';
        $user = $this;
        return Cache::rememberForever($key, function() use($user){
            return (object) $user->defaultPaymentMethod() ?? [];
        });
    }


    function inSubscriptions($stripe_id){
        if($this->subscribed('Designa Plan')){
            return in_array($stripe_id, $this->subscriptions()->pluck('stripe_price')->toArray());
        }
        return false;
    }

    function getCurrentPlanAttribute(){
        if($this->subscribed("Designa Plan")){
          return  SubscriptionPlan::where('stripe_price', $this->subscriptions()->first()->stripe_price)->first() ?? null;
        }
        return SubscriptionPlan::where('name', 'Beginner')->first();
    }

    public function productLimitReached(){
        if($this->is_superadmin){
            return false;
        }

        $count = DB::table('products')->where('company_id', $this->company_id)->count();

        if($count > (config('subscription.product_limit') - 1)){
           return count(array_intersect(config('subscription.product_limit_excluded_plan'), $this->subscribedPlans())) < 1;
        }

        return false;
    }
    public function eventSpaceLimitReached() : Bool{
        if($this->is_superadmin){
            return false;
        }

        $count = FunctionSpace::whereHas('venue', function($q){
            $q->where('company_id', $this->company_id);
        })->count();

        if($count > (config('subscription.venue_limit') - 1)){
           return count(array_intersect(config('subscription.venue_limit_excluded_plan'), $this->subscribedPlans())) < 1;
        }

        return false;
    }

    function  subscriptionAccess(){
        return Cache::rememberForever("subscription.access." . $this->company_id, function(){
            return Access::select('accesses.name as name', 'accesses.display_name as display_name')
                ->join('access_subscription_plan', 'access_subscription_plan.access_id', '=', 'accesses.id')
                ->join('subscription_plans', 'subscription_plans.id', '=', 'access_subscription_plan.subscription_plan_id')
                ->join('subscription_plan_prices', 'subscription_plan_prices.subscription_plan_id', '=', 'subscription_plans.id')
                ->whereIn('subscription_plan_prices.stripe_id', Auth::user()->subscriptions()->pluck('stripe_price'))
                ->get()->pluck('display_name', 'name');
        });
    }

    function subscribedPlans(){
        return (new SubscriptionService)->subscribedPlan($this);
    }


    function hasAccess($access){
        if($this->is_superadmin){
            return true;
        }

        if(is_array($access)){
            foreach($access as $acc){
                if(!(new SubscriptionService())->userHasAccess($this, $access)){
                    return false;
                }
            }
            return true;
        }

        return (new SubscriptionService())->userHasAccess($this, $access);
    }

    function hasSubscriptionAccess($access, $ignoreSuperAdmin = false, $ignoreTrial = false){
        if($this->onTrial()){
            if(!$ignoreTrial){
                return true;
            }
        }

        if($this->is_superadmin){
            if(!$ignoreSuperAdmin){
                return true;
            }
        }

        if(is_array($access)){
            return false;
        }

        return \Arr::exists($this->subscriptionAccess(), $access);
    }


    function company(){
        return $this->belongsTo(Company::class);
    }

    function userSetupIntent(){
        return $this->createSetupIntent();
    }

    function firebases(){
        return $this->hasMany(FireBase::class);
    }

    function caters(){
        return $this->company->caters();
    }


    function caterPluck($item = 'id'){
        return $this->caters()->pluck($item)->toArray();
    }

    function hearted(){
        return $this->hasMany(Heart::class);
    }

    function mobileNotify($payload){

        $firebaseIdArray = array();

        try {

            foreach($this->firebases as $firebase){
                if($firebase->firebase_id){
                    array_push($firebaseIdArray, $firebase->firebase_id);
                }

                if($firebase->ios_device_token){
                    // return $firebase->ios_device_token;
                    //$this->sendAPN($firebase->ios_device_token, $payload['message'], $payload);
                }
            }

            $firebase = new FirebaseFMC;
            $firebase->sendSingle(
                $firebaseIdArray, $payload
            );
            // return response()->json(['message' => 'all sent'], 200);

        } catch (\Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }



    }

    public function sendAPN($token, $message, $payload){
            // $deviceToken = '6e1326c0e4758b54332fab3b3cb2f9ed92e2cd332e9ba8182f49027054b64d29'; //  iPad 5s Gold prod
            $passphrase = 'designa';
            $ctx = stream_context_create();
            stream_context_set_option($ctx, 'ssl', 'local_cert', storage_path('apns-dev-cert.pem')); // Pem file to generated // openssl pkcs12 -in pushcert.p12 -out pushcert.pem -nodes -clcerts // .p12 private key generated from Apple Developer Account
            stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
            $fp = stream_socket_client('api.sandbox.push.apple.com:443', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx); // production
            // $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx); // developement

            $msg = chr(0) . pack('n', 32) . pack('H*', $token) . pack('n', strlen(json_encode($payload))) . json_encode($payload);
            $result = fwrite($fp, $msg, strlen($msg));
            // return $result;
            fclose($fp);
    }


    public function uploadPicture(Request $request, $basecode = false, $image = ""){
        Cache::forget('user.profile.picture.' . $this->id);
        /*
        * check if basecode64
        * @return New Image Model
        */
        $this->clearImage('profile_picture');
        $picture = $this->addImage("profile_picture", $basecode ? $request->picture : $request->file('picture') , public_path('images/profiles/'), false, $basecode);

        //resize images
        $this->imageResize(public_path('/images/profiles/' . $picture->path), 64,64, false, public_path('images/profiles/36/' . $picture->path));
        $this->imageResize(public_path('/images/profiles/' . $picture->path), 165, 165);

        $this->images()->save($picture);
    }


    public function getProfilePicture($isSmall = false){

        $key = 'user.profile.picture.' . $this->id;

        return Cache::rememberForever($key, function(){
            $profile = $this->images()->where('type', 'profile_picture')->first();
            if($profile){
                $image =  public_path('images/profiles/'.$profile->path);

                if(!file_exists($image)){
                    return "/images/placeholders/avatar.jpg";
                }
                return '/images/profiles/'.$profile->path;

            }else{
                return "/images/placeholders/avatar.jpg";
            }
        });
    }



    function updateFirebaseId(Request $request){
        $device = $request->device;
        $ios_device_token = $request->ios_device_token;
        $firebase_id = $request->firebase;

        $current = $this->firebases()->whereDevice($device)->first();

        if($current){
            $current->firebase_id = $firebase_id;
            $current->ios_device_token = $ios_device_token;
            $current->save();
        }else{
            Firebase::create([
                'user_id'           => $this->id,
                'firebase_id'       => $firebase_id,
                'device'            => $device,
                'ios_device_token'  => $ios_device_token
            ]);
        }
    }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];



}
