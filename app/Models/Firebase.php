<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Firebase extends Model
{
    public $fillable    = ['user_id', 'firebase_id', 'device'];

    function user(){
        return $this->belongsTo(User::class);
    }

    
}
