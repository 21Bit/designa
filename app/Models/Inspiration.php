<?php

namespace App\Models;

use Cache;
use Carbon\Carbon;
use App\Traits\HasView;
use App\Traits\HasShare;
use App\Traits\HasReview;
use App\Traits\HasCategories;
use Spatie\Sluggable\HasSlug;
use App\Scopes\HeartCountsScope;
use App\Scopes\ImageCountsScope;
use App\Traits\HasHeartFunction;
use App\Traits\HasImageFunction;
use App\Traits\HasVideoFunction;
use App\Scopes\ReviewCountsScope;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Inspiration extends Model
{
    use HasSlug, HasImageFunction, HasCategories, HasReview, HasVideoFunction, HasShare, HasView, HasHeartFunction, HasFactory;


    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    public function clearCache(): void
    {
        \Illuminate\Support\Facades\Cache::forget('inspiration.' . $this->id);
    }

//    public function scopeHasHeartCounts($query){
//        $model = class_basename($this);
//        return $query->addSelect(
//            ['heart_counts' => Heart::select(\DB::raw('count(*)'))
//                    ->whereColumn('heartable_id', Str::plural(strtolower($model)). '.id')
//                    ->where('heartable_type', "App\\Models\\" . $model)
//                ]);
//    }

    public function getThumbnailAttribute(){
        $inspiration = $this;
        return Cache::remember('inspiration.' . $this->id . '.thumbnail', Carbon::now()->addDays(7), function() use ($inspiration){
          return $inspiration->image('thumbnail')->path('thumbnails', '/images/placeholders/placeholder250x200.png');
        });
    }

    public function getCoverAttribute(){
        $company = $this;
        return Cache::remember('inspiration.' . $this->id . '.cover', Carbon::now()->addDays(7), function() use ($company){
          return $company->image('cover')->path('covers', '/images/placeholders/placeholder250x200.png');
        });
    }

    public function getCompanyLogoAttribute(){
        $company = $this;
        return Cache::remember('inspiration.' . $this->id . '.company.logo', Carbon::now()->addDays(7), function() use ($company){
          return $company->image('logo')->path('logos', '/images/placeholders/placeholder250x200.png');
        });
    }

    public function getHeartsCountsAttribute(){
        $inspiration = $this;
        return Cache::remember('inspiration'. $this->id . '.heartcounts', Carbon::now()->addMinutes(5), function() use($inspiration){
            return $inspiration->hearts()->count();
        });
    }

    public function getSharesCountsAttribute(){
        $inspiration = $this;
        return Cache::remember('inspiration'. $this->id . '.sharecounts', Carbon::now()->addMinutes(5), function() use($inspiration){
            return $inspiration->shares()->count();
        });
    }

    public function scopeWithActiveCompany($query){
        return $query->whereHas('company', function($q){
                $q->where('is_active', 1);
            });
    }


    public function scopeCloseTo($query, $latitude, $longitude, $distance)
    {
        // $ids = $this->select(
        //         DB::raw("*, round(
        //             (
        //                 (
        //                     acos(
        //                         sin(( $request->latitude * pi() / 180))
        //                         *
        //                         sin(( `latitude` * pi() / 180)) + cos(( $request->latitude * pi() /180 ))
        //                         *
        //                         cos(( `latitude` * pi() / 180)) * cos((( $request->longitude - `longitude`) * pi()/180)))
        //                 ) * 180/pi()
        //             ) * 60 * 1.1515 * 1.609344
        //         ) as distance"))
        //     ->orderBy('distance');

        // if($request->distance){
        //     $ids->having('distance', '<=', $request->distance);
        // }

        // return $this->
    }


    public function getLocationNameAttribute(){
        $inspiration = $this;
        return Cache::remember('inspiration'. $this->id . '.location', Carbon::now()->addSeconds(5), function() use($inspiration){
            return $inspiration->venue ? $inspiration->venue->name : $inspiration->location;
        });
    }

    public function getCompanyNameAttribute(){
        $inspiration = $this;
        return optional($inspiration->company)->name ?? '';
    }

    public function decorHiresWithProducts(){
        $inspiration = $this;
        return Cache::remember('inspiration'. $this->id . '.decorHiresWithProducts', Carbon::now()->addMinutes(5), function() use($inspiration){
            return $this->supplierRoles()->where('status', 'confirmed')->whereHas('role', function($q){ $q->where('name', 'decor_hire'); })->get()->filter(function($q){
                return $this->products()->whereCompanyId($q->company_id)->count() ? true : false;
            });
        });
    }



    public function getDescriptionForMobileAttribute(){
        return string_replacer($this->description);
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function hearts(){
        return $this->morphMany(Heart::class, 'heartable');
    }



    public function venueSupplier(){
        return $this->belongsTo(Company::class, 'venue_supplier');
    }

    public function products(){
        return $this->belongsToMany(Product::class);
    }

    public function imageSlider(){
        $inspiration = $this;

        $thumbnail = $this->images()->where('type', 'thumbnail')->get();
        $gallery = $this->images()->where('type', 'gallery')->get();
        $images = $thumbnail->merge($gallery);
        return $images;
    }

    public function venues(){
        return $this->belongsToMany(Venue::class);
    }


    public function venue(){
        return $this->belongsTo(Venue::class);
    }


    public function company(){
        return $this->belongsTo(Company::class);
    }

    public function supplierRoles(){
        return $this->hasMany(SupplierInspiration::class);
    }



    public function getSuppliersAttribute(){
        $ids = $this->supplierRoles()->where('status', 'confirmed')->pluck('company_id');
        return Company::find($ids);
    }



    /*
        * For Stistics uses
    */
    public function dailyHearts($month, $year){
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);

        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $date = date('Y-m-d', strtotime($year .'-' . $month . '-'. $i));
            $array = [
                'date'      => $i,
                'web'       => $this->hearts()->whereDate('created_at', $date)->whereOrigin('web')->count(),
                'mobile'       => $this->hearts()->whereDate('created_at', $date)->whereOrigin('mobile')->count(),
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }

    public function dailyViews($month, $year){
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);

        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $date = date('Y-m-d', strtotime($year .'-' . $month . '-'. $i));
            $array = [
                'date'      => $i,
                'web'       => $this->views()->whereDate('created_at', $date)->whereOrigin('web')->count(),
                'mobile'       => $this->views()->whereDate('created_at', $date)->whereOrigin('mobile')->count(),
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }


    public function dailyShares($month, $year){
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);

        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $date = date('Y-m-d', strtotime($year .'-' . $month . '-'. $i));
            $array = [
                'date'      => $i,
                'web'       => $this->shares()->whereDate('created_at', $date)->whereOrigin('web')->count(),
                'mobile'       => $this->shares()->whereDate('created_at', $date)->whereOrigin('mobile')->count(),
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }

    function dailyInspirations($month, $year){
        $numberOrDays = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $dateData = array();

        for($i=1; $numberOrDays + 1 > $i; $i++){
            $array = [
                'date'      => $i,
                'web'       => $this->whereDate('created_at', $year .'-' . $month . '-'. $i)->count(),
                'mobile'    => 0
            ];

            array_push($dateData, $array);
        }

        return $dateData;
    }



}
