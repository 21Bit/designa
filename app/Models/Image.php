<?php

namespace App\Models;

use App\Traits\HasShare;
use App\Traits\HasHeartFunction;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasHeartFunction, HasShare;
    
    protected $guarded = [];

    public function imagable()
    {
        return $this->morphTo();
    } 
}
