<?php

namespace App\Models;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ModelFile extends Model
{

    use SoftDeletes;

    protected $casts = [
        'product_list' => 'array'
    ];

    public function baseLayer(){
        return $this->belongsTo(ModelFile::class, 'base_layer_id');
    }

    public function modellable()
    {
        return $this->morphTo();
    }


    function companyOwner(){
        $company = Company::find(User::find($this->modellable_id)->company_id);
        if($company){
            return $company->name;
        }

        return "";
    }


    public function isOwned(){
        // $company = $this->modellable->company;
        if($this->modellable instanceof FunctionSpace){
           return $this->modellable->venue->company_id == Auth::user()->company_id;
        }

        return $this->modellable->company_id  == Auth::user()->company_id;
    }


    function getBaseAssetBundleAndroidAttribute(){
        return $this->baseLayer ? $this->baseLayer->modellable->asset_bundle_android : null;
    }

    function getBaseAssetBundleIosAttribute(){
        return $this->baseLayer ? $this->baseLayer->modellable->asset_bundle_ios : null;
    }

    function getBaseAssetBundleWebAttribute(){
        return  $this->baseLayer ? $this->baseLayer->modellable->asset_bundle_web : null;
    }

}
