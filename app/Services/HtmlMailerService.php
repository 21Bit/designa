<?php 

namespace App\Services;

use Illuminate\Support\Facades\Mail;

class HtmlMailerService{

    public $html;
    public $reciepient = ['michael@designa.studio', 'hello@designa.studio', 'nick@designa.studio'];

    function html($html){
        $this->html = $html;
        return $this;
    }

    function reciepients($reciepient){
        $this->reciepient = $reciepient;
        return $this;
    }
    
    function addReciepient($reciepient){
        array_push($reciepient, $this->reciepient);
        return $this;
    }

    function subject($subject){
        $this->subject = $subject;
        return $this;
    }

    function send(){
        Mail::html($this->html, function($message) {
            $message->subject($this->subject)->from('no-reply@designa.studio')->to($this->reciepient);
        });

        if(Mail::failures()){
            return false;
        }

        return true;
    }
}