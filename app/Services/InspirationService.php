<?php

namespace App\Services;

use App\Models\Inspiration;
use App\Models\User;

class InspirationService
{
    function userHeartedInspirations(User $user) : Inspiration
    {
        return Inspiration::query()
                ->join('hearts', function($query){
                    $query->on('users.id', '=', 'hearts.heartable_id')
                            ->where('heartable_type', "App\Models\Inspirations");
                })->where('hearts.user_id', $user->id);
    }
}
