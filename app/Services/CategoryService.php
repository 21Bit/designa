<?php


namespace App\Services;

use App\Models\Category;
use Illuminate\Support\Facades\Cache;

class CategoryService
{
    protected ?string $type = null;
    protected \Illuminate\Database\Eloquent\Builder $query;
    private bool $cached = true;
    private $pagination = null;
    private $with = null;
    private $cachedKey = "";
    private bool $isPaginated = false;

    function __construct(){
        $this->query = Category::query();
        $this->cachedKey = 'categories.' . $this->type;
    }

    function type($type){
        $this->query->where('type', $type);
        return $this;
    }

    function nonCached(){
        $this->cached = false;
        return $this;
    }

    function with($field){
        $this->query->with($field);
        return $this;
    }

    function paginate($number){
        $this->query->paginate($this->pagination);
        $this->isPaginated = true;
        return $this;
    }

    function orderBy($field){
        $this->query->orderBy($field);
        return $this;
    }

    function cachedKey($key){
        $this->cachedKey = $key;
    }

    function get(){
        $results = $this->isPaginated ?
                        $this->query
                        : $this->query->get();

        if($this->cached){
            return Cache::get($this->cachedKey, function () use ($results) {
                return $results;
            });
        }

        return $results;
    }


    function deleteCache(){
        Cache::forget('categories.*');
    }


}
