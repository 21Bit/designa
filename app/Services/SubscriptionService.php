<?php
namespace App\Services;

use DB;
use Mail;
use Cache;
use App\Models\User;
use App\Models\Company;
use App\Models\SubscriptionPlan;
use App\Mail\Dashboard\FreeTrialAddedMail;
use App\Models\Access;
use App\Models\FunctionSpace;
use App\Models\SubscriptionPlanPrice;
use App\Models\Venue;
use Laravel\Cashier\Exceptions\IncompletePayment;

class SubscriptionService
{
    public function userHasAccess(User $user, $access)
    {
     
        if($user->is_superadmin){
            return true;
        }


        if(is_array($access)){
            $suffix = collect($access)->implode('.');
        }else{
            $suffix = $access;
        }

        $key = 'subscription.access.' . $user->company_id . '.' . $suffix;
        return Cache::rememberForever($key, function() use ($user, $access){

            if($user->subscribed('Designa Plan')){
                $userAccess = Access::select('accesses.name as name', 'accesses.display_name as display_name')
                    ->join('access_subscription_plan', 'access_subscription_plan.access_id', '=', 'accesses.id')
                    ->join('subscription_plans', 'subscription_plans.id', '=', 'access_subscription_plan.subscription_plan_id')
                    ->join('subscription_plan_prices', 'subscription_plan_prices.subscription_plan_id', '=', 'subscription_plans.id')
                    ->whereIn('subscription_plan_prices.stripe_id', $user->subscriptions()->pluck('stripe_price')->toArray())
                    ->get()->pluck('name')->toArray();
            }else{
                $userAccess = SubscriptionPlan::where('name', 'Beginner')->first()->accesses()->pluck('name')->toArray();
            }

            if(is_array($access)){
                return count(array_intersect($access, $userAccess)) ? true : $this->trailAccessValidator($user, $access);
            }else{
                return in_array($access, $userAccess) ? true : $this->trailAccessValidator($user, $access);
            }

        });
    }


    function clearCompanyAccessCache($companyId){
        // Cache::flush();
        foreach(Access::all() as $access){
            Cache::forget('subscription.access.' . $companyId . '.' . $access->name);
        }
    }


    public function trailAccessValidator($user, $access): bool
    {
        if($user->onTrial() || $user->onTrial('Designa Plan')){
             return true;
//            if(is_array($access)){
//                foreach($access as $accessItem){
//                    if(in_array($accessItem, config('subscription.free_trial_access'), true)){
//                        return true;
//                    }
//                }
//            }else{
//                if(in_array($access, config('subscription.free_trial_access'), true)){
//                    return true;
//                }
//            }
        }

        return false;
    }


    function subscribeToPlan(User $user, SubscriptionPlanPrice $subscriptionPlanPrice, $paymentMethod = null){
        $method = $paymentMethod ?? $user->default_payment_method->id;
        if($user->subscriptions()->count()){
            $user->subscription("Designa Plan")
                    ->swap($subscriptionPlanPrice->stripe_id);
        }else{
            $user->newSubscription("Designa Plan", $subscriptionPlanPrice->stripe_id)
                ->quantity(1)
                ->create($method);
        }
        Cache::flush("subscription.access." . $user->company_id) . '.*';
        return true;
    }

    function subscribedPlan(User $user) : Array {
        if($user->subscribed('Designa Plan')){
            return SubscriptionPlan::whereHas('prices', function($q) use ($user){
                $q->whereIn('stripe_id', $user->subscriptions()->pluck('stripe_price')->toArray());
            })->pluck('name')->toArray();
        }

        return ['Beginner'];
    }

    function subscribeToEventSpaceCharge(User $user){

        $exceeded = $user->company->exceededFunctionSpaces();

        if($exceeded > 0){
            if($user->paymentMethods()->count()){
                $paymentMethod = $user->default_payment_method->id;
                $price = SubscriptionPlanPrice::whereHas('subscriptionPlan', function($q){
                    $q->where('type', 'event-spaces');
                })->first();

                if($user->subscribed('Event Space Charges')){
                    $user->subscription('Event Space Charges')
                        ->updateQuantity($exceeded);
                }else{
                    $user->newSubscription("Event Space Charges", $price->stripe_id)
                        ->quantity($exceeded)
                        ->create($paymentMethod);
                }
                return true;
            }
        }
    }

    function subscribeToVenueCharge(User $user){
        DB::transaction(function() use ($user){
            $venues = Venue::where('company_id', $user->company_id)->count();

            if($user->paymentMethods()->count()){
                $paymentMethod = $user->default_payment_method->id;
                $price = SubscriptionPlanPrice::whereHas('subscriptionPlan', function($q){
                    $q->where('type', 'venues');
                })->first();

                if($price){
                    if($user->subscribed('Venues Charges')){
                        $user->subscription('Venues Charges')
                        ->updateQuantity($venues);
                    }else{
                        $user->newSubscription("Venues Charges", $price->stripe_id)
                        ->quantity($venues)
                        ->create($paymentMethod);
                    }
                }else{
                    \Log::error("No venue changes price from database found");
                }

                return true;
            }

        });
    }

    function addTrialInCompany(Company $company, $days, $sendMail = true){
        $users = $company->users;
        foreach($users as $user){
            $user->trial_ends_at = now()->addDays($days);
            $user->save();
            if($sendMail){
                Mail::to($user->email)
                        ->send(new FreeTrialAddedMail($company, $days, $user));
            }
        }
        Cache::forget('company-trials-' . $company->id);
    }

    function cancelTrialInCompany(Company $company){
        $users = $company->users;
        foreach($users as $user){
            $user->trial_ends_at = null;
            $user->save();
        }
        Cache::forget('company-trials-' . $company->id);
        Cache::forget('subscription.access.' . $company->id . '.*');
    }
}
