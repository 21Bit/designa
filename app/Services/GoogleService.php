<?php

namespace App\Services;

use Illuminate\Http\Client\Pool;
use Illuminate\Support\Facades\Http;

class GoogleService{

    private $api, $country;

    function __construct(){
        $this->api = config('google.google_place_api');
        $this->country = config('google.country');
    }

    function getLatLong($address): array {
        $result = $this->placeAutoComplete($address, 'city');
        if($result){
            if(count($result->predictions)){
                $placeId = $result->predictions[0]->place_id;
                $details = $this->detailsJson($placeId);
                
                if($details){
                    $location = $details->result->geometry->location;
                    return [
                        'latitude' => $location->lat,
                        'longitude' => $location->lng,
                    ];
                }
            }
        }

        return [];
    }



    function geocodeJson($latlng){

        $response = Http::get(config('google.geocode_endpoint'),[
            'latlng'    => $latlng,
            'sensor'    => true,
            "key"       => $this->api,
        ]);

        return $response->object();
    }


    function placeAutoComplete($input, $type = null){
        if($type){
            if($type == "city"){
                $types = ["administrative_area_level_3", 'locality'];
            }else{
                $types = ['establishment', 'address'];
            }
        }else{
            $types = ['establishment', 'address'];
        }

        $response = Http::get(config('google.autocomplete_endpoint'),[
            'input'         => $input,
            "key"           => $this->api,
            'types'         => $types,
            'components'    => $this->country
        ]);

        return $response->object();
    }


    function detailsJson($place_id){
        $response = Http::get(config('google.details_endpoint'), [
            'place_id'      => $place_id,
            "key"           => $this->api,
        ]);
        return $response->object();
    }

}