<?php 

use Illuminate\Support\Facades\Facade;

class FirebaseFMCFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'firebasefmc';
    }
}