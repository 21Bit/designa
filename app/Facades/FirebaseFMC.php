<?php
namespace App\Facades;

class FirebaseFMC{

    function sendSingle($firebase_ids, $data) {
        $fields = array (
            'registration_ids' => $firebase_ids,
            'data' => $data,// You can add as many keys as you want inside this array
        );

        $fields = json_encode ($fields);
    
        $headers = array (
            'Authorization: Key=' . config('firebase.key'),
            'Content-Type: application/json'
        );
    
        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, config('firebase.url'));
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
    
        $result = curl_exec ( $ch );;
        curl_close ( $ch );
        return $result;
    }

    function toAllSubscribers($topic, $data) {

        $fields = array (
            'to' => "/topics/" . $topic,  // <---This line is important
            'data' => $data
        );

        $fields = json_encode ($fields);
    
        $headers = array (
            'Authorization: key=' . config('firebase.key'),
            'Content-Type: application/json'
        );
    
        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, config('firebase.url'));
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
    
        $result = curl_exec ( $ch );
       // echo $result;
        curl_close ( $ch );
    }
}